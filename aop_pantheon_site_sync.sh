############# BEFORE YOU START ####################
# !!!!!!!!!YOU MUST UPDATE THE FILE PATHS FOR THIS SCRIPT TO WORK FOR YOU. !!!!!!!
# 1. This script needs to run from your drupal site root directory
# 2. You need to have Drush installed

#================================================

#Path to my drush install
DRUSH='/Users/lasbreyn/Sites/dev/drush/drush'

#local database name
# Drush will automatically pick your local database from withing you site root

# ===============================================
# MODIFY THIS SECTION TO YOUR LOCAL AND REMOTE(Pantheon) SETTINGS
# ===============================================

# Local Database
# ============================================
# This is just used for MYSQL since the drush_sql_sync_pipe command is not working for pantheon
LOCALBD='aophs'

#Path to mysql
MYSQLPATH='/Applications/MAMP/Library/bin/'

#Backup directory
BACKUPPATH='/Users/lasbreyn/Desktop'

# Local database password
PWD='root'

# Local database user
USER='root'

# Site alias
SITEALIAS='aophs.dev'


# Pantheon Database
# ============================================
# Host
HOST='dbserver.dev.4bc2c31e-1057-4860-8c75-be29a385c746.drush.in'


# Username
PUSER='pantheon'

# Password
PPWD='dd8b41511dbd4153a0997595249014ef'

# Port
PPORT='11127'

# Database
PDB='pantheon'

# ===============================================
# DO NOT MODIFY BEYOUND THIS UNLESS YOU KNOW WHAT YOU ARE DOING
# ===============================================

# Install Drush from http://www.drush.org/
# Install drush_sql_sync_pipe from https://drupal.org/project/drush_sql_sync_pipe
# Easy Installation: drush dl drush_sql_sync_pipe --destination=$HOME/.drush
# Synk with SFTP
# sudo sftp> -r -o Port=2222 dev.75c0137d-4d88-c481-d39f-6eb1a32a4960@appserver.dev.75c0137d-4d88-c481-d39f-6eb1a32a4960.drush.in:/srv/bindings/1100b2edb60848a990c875006416854b/files/* sites/default/files
echo Syncing files from pantheon ......... \n.
$DRUSH -r . rsync @pantheon.${SITEALIAS}:%files @self:sites/default/

echo Updating files permissions ......... \n.
find `$DRUSH dd %files` -type d -exec sudo chmod 777 {} \;

#Backup your site first
echo Backing up current database ......... \n.
#$DRUSH sql-dump > ${BACKUPPATH}/database-backup.sql
${MYSQLPATH}mysqldump --verbose -h localhost -u ${USER} -p${PWD} ${LOCALBD} > ${BACKUPPATH}/database-backup.sql

#Drop all tables from the current database
echo About to drop all your tables from your database ......... \n.
$DRUSH sql-drop

# directly sync the content of your remote database to your local environment using Drush and sql-sync-pipe
#$DRUSH sql-sync-pipe @pantheon.aop.dev @self
echo syncing database ......... \n.
sudo ${MYSQLPATH}mysqldump --verbose -u ${PUSER} -p${PPWD} -h ${HOST} -P ${PPORT} ${PDB} | ${MYSQLPATH}mysql -h localhost -u ${USER} -p${PWD} ${LOCALBD}

# You can use Drush to rsync the files over.
echo Clearing cache ......... \n.
$DRUSH @pantheon.${SITEALIAS} cc all

# Update the temp files directory
echo Updating database with temp files directory ......... \n.
$DRUSH vset file_temporary_path "/tmp"

# flush twice
echo Clearing cache ......... \n.
$DRUSH cc all
echo Clearing cache second ......... \n.
$DRUSH cc all