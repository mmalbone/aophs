<article<?php print $attributes; ?>>
  <div<?php print $content_attributes; ?>>
   <div class="aop-product-listing-wrapper">
     <div class="aop-product-image-wrapper">
        <img />
     </div><!--end of product image wrapper-->
     <div class="aop-product-item-details-wrapper">
       <div class="aop-product-item-title">
          <?php print render($content['title_field']); ?>
       </div>
       <div class="aop-product-item-short-description">
          <?php print render($content['product:field_hsc_short_description']); ?>
       </div>
       <div class="aop-product-item-sku">
          <?php print render($content['product:sku']); ?>
       </div>
       <div class="aop-product-item-price">
          <?php print render($content['product:commerce_price']); ?>
       </div>
     </div>
   </div><!--end of product listing wrapper -->
  </div>
</article>
