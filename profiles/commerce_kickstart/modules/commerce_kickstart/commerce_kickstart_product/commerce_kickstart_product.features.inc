<?php
/**
 * @file
 * commerce_kickstart_product.features.inc
 */

/**
 * Implements hook_commerce_flat_rate_default_services().
 */
function commerce_kickstart_product_commerce_flat_rate_default_services() {
  $items = array();
  return $items;
}

/**
 * Implements hook_commerce_product_default_types().
 */
function commerce_kickstart_product_commerce_product_default_types() {
  $items = array();
  return $items;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function commerce_kickstart_product_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function commerce_kickstart_product_image_default_styles() {
  $styles = array();
  return $styles;
}

/**
 * Implements hook_node_info().
 */
function commerce_kickstart_product_node_info() {
  $items = array();
  return $items;
}
