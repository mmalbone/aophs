<?php
/**
 * @file
 * rules_config.rules_mondis_communication_error.inc
 */

$api = '2.0.0';

$data = entity_import('rules_config', '{ "rules_mondis_communication_error" : {
      "LABEL" : "Mondis Communication Error",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "mimemail", "aop_assist" ],
      "ON" : { "aop_assist_mondis_error" : [] },
      "DO" : [
        { "mimemail" : {
            "key" : "Mondis Communication Error",
            "to" : "webservicesteam@glynlyon.com",
            "from_name" : "Drupal webstore",
            "from_mail" : "webservicesteam@glynlyon.com",
            "reply_to" : "webservicesteam@glynlyon.com",
            "subject" : "Unable to communicate with Mondis from Drupal Webstore",
            "body" : "The Drupal webstore was unable to connect to Mondis to retrieve activation codes.",
            "plaintext" : "The Drupal webstore was unable to connect to Mondis to retrieve activation codes.",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'entity',
  1 => 'aop_assist',
  2 => 'mimemail',
  3 => 'rules',
);
