<?php
/**
 * @file
 * vocabulary.gender.inc
 */

$api = '2.0.0';

$data = (object) array(
  'description' => '',
  'hierarchy' => '0',
  'machine_name' => 'gender',
  'module' => 'taxonomy',
  'name' => 'Grade Level',
  'vid' => '22',
  'weight' => '0',
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'taxonomy',
);
