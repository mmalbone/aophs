<?php
/**
 * @file
 * content_type.curriculum.inc
 */

$api = '2.0.0';

$data = (object) array(
  'base' => 'node_content',
  'description' => '',
  'has_title' => '1',
  'help' => '',
  'name' => 'Curriculum',
  'title_label' => 'Title',
  'type' => 'curriculum',
);

$dependencies = array();

$optional = array(
  'field.node.body.curriculum' => 'field.node.body.curriculum',
  'field.node.field_collection.curriculum' => 'field.node.field_collection.curriculum',
  'field.node.field_curriculum.curriculum' => 'field.node.field_curriculum.curriculum',
  'field.node.field_grade_level.curriculum' => 'field.node.field_grade_level.curriculum',
  'field.node.field_product.curriculum' => 'field.node.field_product.curriculum',
  'field.node.field_search_boost.curriculum' => 'field.node.field_search_boost.curriculum',
  'field.node.field_subject.curriculum' => 'field.node.field_subject.curriculum',
  'field.node.field_view_sort.curriculum' => 'field.node.field_view_sort.curriculum',
  'field.node.title_field.curriculum' => 'field.node.title_field.curriculum',
  'permission.create_curriculum_content' => 'permission.create_curriculum_content',
  'permission.delete_any_curriculum_content' => 'permission.delete_any_curriculum_content',
  'permission.delete_own_curriculum_content' => 'permission.delete_own_curriculum_content',
  'permission.edit_any_curriculum_content' => 'permission.edit_any_curriculum_content',
  'permission.edit_own_curriculum_content' => 'permission.edit_own_curriculum_content',
);

$modules = array(
  0 => 'node',
);
