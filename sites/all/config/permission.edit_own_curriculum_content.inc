<?php
/**
 * @file
 * permission.edit_own_curriculum_content.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'edit own curriculum content',
  'roles' => array(
    0 => 'administrator',
    1 => 'marketing',
  ),
);

$dependencies = array(
  'content_type.curriculum' => 'content_type.curriculum',
);

$optional = array();

$modules = array(
  0 => 'node',
);
