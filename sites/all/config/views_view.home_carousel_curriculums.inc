<?php
/**
 * @file
 * views_view.home_carousel_curriculums.inc
 */

$api = '2.0.0';

$data = $view = new view();
$view->name = 'home_carousel_curriculums';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'taxonomy_term_data';
$view->human_name = 'Home Carousel Curriculums';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'jcarousel';
$handler->display->display_options['style_options']['wrap'] = 'both';
$handler->display->display_options['style_options']['visible'] = '5';
$handler->display->display_options['style_options']['auto'] = '0';
$handler->display->display_options['style_options']['autoPause'] = 1;
$handler->display->display_options['style_options']['easing'] = '';
$handler->display->display_options['style_options']['vertical'] = 0;
$handler->display->display_options['row_plugin'] = 'fields';
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['content'] = '<h1>Discover products and explore resources</h1>
<h3>Find a curriculum and tools that best suit you and your students</h3>';
$handler->display->display_options['header']['area']['format'] = 'panopoly_wysiwyg_text';
/* Field: Taxonomy term: Curriculum Icon */
$handler->display->display_options['fields']['field_curriculum_icon']['id'] = 'field_curriculum_icon';
$handler->display->display_options['fields']['field_curriculum_icon']['table'] = 'field_data_field_curriculum_icon';
$handler->display->display_options['fields']['field_curriculum_icon']['field'] = 'field_curriculum_icon';
$handler->display->display_options['fields']['field_curriculum_icon']['label'] = '';
$handler->display->display_options['fields']['field_curriculum_icon']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_curriculum_icon']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_curriculum_icon']['settings'] = array(
  'image_style' => 'curriculum_carousel_thumb_82px_height',
  'image_link' => 'content',
);
/* Field: Taxonomy term: Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['label'] = '';
$handler->display->display_options['fields']['name']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['name']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['name']['link_to_taxonomy'] = TRUE;
/* Filter criterion: Taxonomy vocabulary: Machine name */
$handler->display->display_options['filters']['machine_name']['id'] = 'machine_name';
$handler->display->display_options['filters']['machine_name']['table'] = 'taxonomy_vocabulary';
$handler->display->display_options['filters']['machine_name']['field'] = 'machine_name';
$handler->display->display_options['filters']['machine_name']['value'] = array(
  'curriculum' => 'curriculum',
);

/* Display: Content pane */
$handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
$handler->display->display_options['defaults']['header'] = FALSE;
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['content'] = '<h2>Discover products and explore resources</h2>
<h3>Find a curriculum and tools that best suit you and your students</h3>';
$handler->display->display_options['header']['area']['format'] = 'full_html';


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'views',
  1 => 'taxonomy',
  2 => 'image',
  3 => 'views_content',
);
