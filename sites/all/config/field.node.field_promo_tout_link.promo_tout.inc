<?php
/**
 * @file
 * field.node.field_promo_tout_link.promo_tout.inc
 */

$api = '2.0.0';

$data = array(
  'field_config' => array(
    'active' => '1',
    'cardinality' => '1',
    'columns' => array(
      'attributes' => array(
        'not null' => FALSE,
        'size' => 'medium',
        'type' => 'text',
      ),
      'title' => array(
        'length' => 255,
        'not null' => FALSE,
        'sortable' => TRUE,
        'type' => 'varchar',
      ),
      'url' => array(
        'length' => 2048,
        'not null' => FALSE,
        'sortable' => TRUE,
        'type' => 'varchar',
      ),
    ),
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_promo_tout_link',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => '0',
    'module' => 'link',
    'settings' => array(
      'attributes' => array(
        'class' => '',
        'rel' => '',
        'target' => 'default',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'title' => 'optional',
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
    ),
    'storage' => array(
      'active' => '1',
      'details' => array(
        'sql' => array(
          'FIELD_LOAD_CURRENT' => array(
            'field_data_field_promo_tout_link' => array(
              'attributes' => 'field_promo_tout_link_attributes',
              'title' => 'field_promo_tout_link_title',
              'url' => 'field_promo_tout_link_url',
            ),
          ),
          'FIELD_LOAD_REVISION' => array(
            'field_revision_field_promo_tout_link' => array(
              'attributes' => 'field_promo_tout_link_attributes',
              'title' => 'field_promo_tout_link_title',
              'url' => 'field_promo_tout_link_url',
            ),
          ),
        ),
      ),
      'module' => 'field_sql_storage',
      'settings' => array(),
      'type' => 'field_sql_storage',
    ),
    'translatable' => '0',
    'type' => 'link_field',
  ),
  'field_instance' => array(
    'bundle' => 'promo_tout',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => '',
    'display' => array(
      'aop_subscription_list' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'above',
        'module' => 'link',
        'settings' => array(),
        'type' => 'link_default',
        'weight' => 1,
      ),
      'product_carousel' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'product_list' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => '',
    'field_name' => 'field_promo_tout_link',
    'label' => 'Promo Tout Link',
    'required' => 0,
    'settings' => array(
      'attributes' => array(
        'class' => '',
        'configurable_title' => 0,
        'rel' => '',
        'target' => 'default',
        'title' => '',
      ),
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'display' => array(
        'url_cutoff' => '80',
      ),
      'enable_tokens' => 1,
      'rel_remove' => 'default',
      'title' => 'none',
      'title_maxlength' => '128',
      'title_value' => '',
      'url' => 0,
      'user_register_form' => FALSE,
      'validate_url' => 1,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'link',
      'settings' => array(),
      'type' => 'link_field',
      'weight' => '5',
    ),
  ),
);

$dependencies = array(
  'content_type.promo_tout' => 'content_type.promo_tout',
);

$optional = array();

$modules = array(
  0 => 'field_sql_storage',
  1 => 'link',
);
