<?php
/**
 * @file
 * page_manager_handlers.page__lighthouse_christian_academy_vs_horizons_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page__lighthouse_christian_academy_vs_horizons_panel_context';
$handler->task = 'page';
$handler->subtask = '_lighthouse_christian_academy_vs_horizons';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '18144b94-999a-4309-b0b0-cb63a0894980';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-66a103c2-6060-4bc3-81df-b438fcb8afbc';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Lighthouse Christian Academy vs. Horizons',
    'body' => '<p>Investigating the differences between A.C.E.\'s Lighthouse Christian Academy and Horizons curriculum from Alpha Omega Publications? We have just what you need! Find short overviews of each curriculum below, an easy-to-view comparison chart, and additional comparison options!</p><h3><strong>A.C.E.\'s Lighthouse Christian Academy</strong></h3><p>Lighthouse Christian Academy (LCA) is the provider of distance learning for Accelerated Christian Education (A.C.E.). Lighthouse Christian Academy is a fully accredited (MSA-CESS and Ai) program designed to assist parents in providing a quality Christian education to homeschooled students. Lighthouse Christian Academy exclusively utilizes the A.C.E. curriculum for students in grades K-12 and provides coursework in all core subjects as well as electives.</p><p>Lighthouse Christian Academy provides its students with diagnostic testing to determine placement within the A.C.E. curriculum program. Academic advising assists families in designing a course of study appropriate for each student\'s interests and goals. Honors, college-prep, general, and vocational courses are offered to all high school students. In addition to diagnostic testing, LCA provides progress reports, transcripts, standardized testing for all enrolled students. LCA provides graduation services and a diploma for eligible seniors. Enrollment and administrative fees include all provided services, but the purchase of A.C.E. curricular materials is the responsibility of the parents. Parents are responsible for supervision of all student coursework, as well as all lesson planning and grading of daily work. Completed coursework is submitted to the Lighthouse Christian Academy office for evaluation and reporting.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>Are you wondering how to find the most effective homeschooling method for your family? Keep in mind that there is no one perfect homeschool curriculum. A blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Instead of feeling torn between curriculum formats, parents should be open to mixing and matching materials and resources to customize their child\'s education. Remember, each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3><strong>Horizons from Alpha Omega Publications</strong></h3><p>Horizons is a preK-12 print-based curriculum offered by Alpha Omega Publications, a Christian-based publisher that provides curriculum and educational services to homeschool families. Filled with engaging, full-color illustrations, Horizons is a child-friendly workbook-based curriculum with consumable lessons and hands-on activities. Horizons curriculum encourages concept mastery through a spiral-based learning process of skill/concept presentation, review, and reinforcement. Teacher\'s guides offer clear, step-by-step lesson plans and activities that are suitable for both homeschool and classroom settings.</p><p>Winner of Cathy Duffy\'s 100 Top Picks for Homeschool Curriculum, Horizons integrates Scripture and Christian living throughout all subjects. Horizons workbooks provide removable student pages for ease of student use. Horizons curriculum includes courses in math (K-8), penmanship (1-5), health (K-8), physical education (PreK-12), phonics and reading (K-3), and spelling and vocabulary (1-3). Health and physical education electives were designed for classroom use and may need to be adapted to homeschool use. Complete Horizons Preschool Curriculum Sets and Multimedia Sets are also available. Horizons curriculum sets include all student and teacher materials. Student and teacher materials can also be purchased separately.</p><h3><strong>Benefits and Features of Horizons from Alpha Omega Publications</strong></h3><p class="list_heading">Horizons</p><ul><li>brightly illustrated, hands-on, consumable curriculum</li><li>offers fun lessons and manipulative-based activities</li><li>teaches concepts through a spiral learning process</li><li>encourages concept mastery through quick concept introduction, review, and reinforcement</li></ul>

<h3>Take a Closer Look at A.C.E.\'s Lighthouse Christian Academy and Horizons</h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">Horizons</th><th class="compare">Lighthouse Christian Academy</th></tr>
</thead>
<tbody>
<tr>
<td>K-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Core subjects and electives available</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Instruction provided by parent</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Interactive, one-on-one learning format</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Readiness tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Flexible scheduling and pacing</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Manageable amount of daily lesson material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Easy-to-use teacher\'s materials</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Material designed for homeschoolers</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Requires materials to be returned</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Ability to pick and choose student &amp; teacher materials</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr><th class="features">Cost</th><th class="compare">Alpha Omega Publications</th><th class="compare">A.C.E.</th></tr>
</thead>
<tbody>
<tr class="alt">
<td>Average yearly cost per grade level**</td>
<td class="tac">N/A</td>
<td class="tac">$850</td>
</tr>
<tr class="alt">
<td>Average subject cost per grade level**</td>
<td class="tac">$79.95</td>
<td class="tac">N/A</td>
</tr>
</tbody>
</table>
</div>

<h4>Compare Offerings: A.C.E. versus Alpha Omega Publications</h4>
<p class="list_heading_links">A.C.E. PACE</p>
<ul>
<li><a href="/ace-pace-vs-monarch">A.C.E. PACE vs. Monarch</a></li>
<li><a href="/ace-pace-vs-horizons">A.C.E. PACE vs. Horizons</a></li>
<li><a href="/ace-pace-vs-lifepac">A.C.E. PACE vs. LIFEPAC</a></li>
<li><a href="/ace-pace-vs-aoa">A.C.E. PACE vs. Alpha Omega Academy</a></li>
<li><a href="/ace-pace-vs-sos">A.C.E. PACE vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">A.C.E. Lighthouse Christian Academy</p>
<ul>
<li><a href="/ace-lca-vs-monarch">A.C.E. Lighthouse Christian Academy vs. Monarch</a></li>
<li><a href="/ace-lca-vs-horizons">A.C.E. Lighthouse Christian Academy vs. Horizons</a></li>
<li><a href="/ace-lca-vs-lifepac">A.C.E. Lighthouse Christian Academy vs. LIFEPAC</a></li>
<li><a href="/ace-lca-vs-aoa">A.C.E. Lighthouse Christian Academy vs. Alpha Omega Academy</a></li>
<li><a href="/ace-lca-vs-sos">A.C.E. Lighthouse Christian Academy vs. Switched-On Schoolhouse</a></li>
</ul>
<p>*A.C.E.® and PACE® are the registered trademarks and subsidiaries of A.C.E.®. Alpha Omega Publications is not in any way affiliated with A.C.E.. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by A.C.E.®.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p><p>**Prices obtained February 2013 from Accelerated Christian Education Homeschool and A.C.E. Ministries websites and from Alpha Omega Publications 2013 Homeschool Catalog. Cost comparisons are based on the single subject price for the 3rd grade math Horizons curriculum and Lighthouse Christian Academy full year administrative costs for K-8 (does not include registration fees). Costs do not reflect any additional fees or shipping and handling charges. Prices for other Accelerated Christian Education and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '66a103c2-6060-4bc3-81df-b438fcb8afbc';
  $display->content['new-66a103c2-6060-4bc3-81df-b438fcb8afbc'] = $pane;
  $display->panels['left'][0] = 'new-66a103c2-6060-4bc3-81df-b438fcb8afbc';
  $pane = new stdClass();
  $pane->pid = 'new-e59c564d-a57d-435e-8782-78ae612878fc';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'e59c564d-a57d-435e-8782-78ae612878fc';
  $display->content['new-e59c564d-a57d-435e-8782-78ae612878fc'] = $pane;
  $display->panels['right'][0] = 'new-e59c564d-a57d-435e-8782-78ae612878fc';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = 'new-66a103c2-6060-4bc3-81df-b438fcb8afbc';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
