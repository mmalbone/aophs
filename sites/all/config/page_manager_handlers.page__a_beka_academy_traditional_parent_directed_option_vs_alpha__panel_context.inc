<?php
/**
 * @file
 * page_manager_handlers.page__a_beka_academy_traditional_parent_directed_option_vs_alpha__panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page__a_beka_academy_traditional_parent_directed_option_vs_alpha__panel_context';
$handler->task = 'page';
$handler->subtask = '_a_beka_academy_traditional_parent_directed_option_vs_alpha_';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '2b293e62-8ebc-4372-9040-c55a68dea9d5';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-0923609a-ab13-4272-85e4-f46354a33c37';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'A Beka Academy Traditional Parent-Directed Option vs. Alpha Omega Academy',
    'body' => '<p>Looking for the differences between A Beka Academy Traditional Parent-Directed Option and Alpha Omega Academy? Look no further! Find short overviews of each curriculum below, an easy-to-view comparison chart, and additional comparison options!</p><h3><strong>A Beka Academy Traditional Parent-Directed Option</strong></h3><p>A Beka Academy Traditional Parent-Directed Option for homeschooling families is available for students in grades K5-6 and provides a blending of an accredited (FACCS and SACS CASI) distance learning option with a more traditional parent-directed homeschool program. The A Beka Academy Traditional Parent-Directed Option uses structured, print-based curriculum that was originally written for teachers in a Christian school setting. A Beka Book\'s Traditional Parent-Directed Program requires homeschoolers to follow a highly structured, parent-supervised program with all lessons taught by a parent. Teacher\'s guides include lesson plans that are often more suited to a classroom setting and may at times be difficult to adapt to homeschool situations.</p><p>The A Beka Traditional Parent-Directed Option provides all necessary textbooks, tests, and keys, as well as a parent manual with daily lessons plans that follow a 170-day school year. Students must meet age requirements and placement criterion to be enrolled. A Beka Academy provides progress reports for record keeping, evaluation of student work, a report card for each grading period, and one copy of the transcript. Parental responsibilities include assuring compliance with state laws, showing proof of previously completed work, providing supervision for all work, and sending in all required graded work in accordance with the provided academic calendar. Coursework must be completed within ten months of the assigned start date. All A Beka coursework is Bible-based and provides instruction from a Christian worldview. Science courses are strongly creation-based. Courses are available in all core subjects: Bible, English, mathematics, science, and history and geography. Electives are also available.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>Wondering which distance learning program is right for your child? Keep in mind that there is no one perfect homeschool program. A blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Rather than feeling torn between curriculum formats, parents should be open to mixing and matching materials and resources to customize their child\'s education. Remember, each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3><strong>Alpha Omega Academy</strong></h3><p>Alpha Omega Academy (AOA) is an accredited distance learning program for homeschool children in grades K-12. Fully accredited by NCA CASI, AOA offers an at-home education with the convenience of distance learning. Providing qualified teachers, administrative services, and multiple curriculum choices, AOA has enrolled thousands of students from all 50 states and over 42 countries. Great for homeschoolers who are looking for the assurance of accredited courses, AOA offers a high school diploma program and the security of course credits that are readily transferred to other schools and programs. AOA provides families with flexibility in scheduling and completion of assigned coursework. Students must complete coursework within ten months of the start date.</p><p>Alpha Omega Academy allows families to choose between Alpha Omega Publications\' print-based or computer-based formats. Both curriculum options can be perfectly customized to fit your child\'s learning needs and educational goals. Both options provide student-paced lessons that can be completed independently with parental supervision. The print-based program requires a parent to grade daily work. All AOP curriculum promotes a solid, Christian worldview. Core courses include Bible, language arts, math, science, and history and geography. Elementary and high school electives are also available. AOA offers both college-prep and general studies diploma tracks. AOA offers a complete line of over 140 academically challenging courses for students in grades K-12. Placement testing, summer school options, academic support, and achievement testing are all available. Full-time or part-time enrollment is offered. A National Honor Society membership (by invitation) and high school graduation ceremony are also available to AOA students.</p><h3><strong>Benefits and Features of Alpha Omega Academy</strong></h3><p class="list_heading">Alpha Omega Academy</p><ul><li>accredited K-12 distance learning program for homeschoolers</li><li>flexible, at-home learning environment with the convenience of distance learning</li><li>choice of print- or computer-based, Christ-centered curriculum options</li><li>complete high school graduation program</li></ul>

<h3 >Take a Closer Look at A Beka Academy Traditional Parent-Directed Option and Alpha Omega Academy</h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">Alpha Omega Academy (print-based curriculum)</th><th class="compare">A Beka Academy Traditional Parent-Directed Option (with A Beka Books)</th></tr>
</thead>
<tbody>
<tr>
<td>K-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Fully accredited courses</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Parent-directed curriculum</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Flexible scheduling</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Placement tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Self-paced and individualized</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Manageable amount of daily lesson material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Promotes critical thinking and cognitive reasoning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Requires student work to be mailed for evaluation</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Allows for part-time student status</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Provides official school records</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: A Beka versus Alpha Omega Publications</h4>
<p class="list_heading_links">A Beka Book</p>
<ul>
<li><a href="/abeka-book-vs-monarch">A Beka Book vs. Monarch</a></li>
<li><a href="/abeka-book-vs-horizons">A Beka Book vs. Horizons</a></li>
<li><a href="/abeka-book-vs-lifepac">A Beka Book vs. LIFEPAC</a></li>
<li><a href="/abeka-book-vs-aoa">A Beka Book vs. Alpha Omega Academy</a></li>
<li><a href="/abeka-book-vs-sos">A Beka Book vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">A Beka Academy (DVD)</p>
<ul>
<li><a href="/abeka-dvd-vs-monarch">A Beka Academy (DVD) vs. Monarch</a></li>
<li><a href="/abeka-dvd-vs-horizons">A Beka Academy (DVD) vs. Horizons</a></li>
<li><a href="/abeka-dvd-vs-lifepac">A Beka Academy (DVD) vs. LIFEPAC</a></li>
<li><a href="/abeka-dvd-vs-aoa">A Beka Academy (DVD) vs. Alpha Omega Academy</a></li>
<li><a href="/abeka-dvd-vs-sos">A Beka Academy (DVD) vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">A Beka Academy (Parent-Directed)</p>
<ul>
<li><a href="/abeka-parent-directed-vs-monarch">A Beka Academy (Parent-Directed) vs. Monarch</a></li>
<li><a href="abeka-parent-directed-vs-horizons">A Beka Academy (Parent-Directed) vs. Horizons</a></li>
<li><a href="/abeka-parent-directed-vs-lifepac">A Beka Academy (Parent-Directed) vs. LIFEPAC</a></li>
<li><a href="/abeka-parent-directed-vs-aoa">A Beka Academy (Parent-Directed) vs. Alpha Omega Academy</a></li>
<li><a href="/abeka-parent-directed-vs-sos">A Beka Academy (Parent-Directed) vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">A Beka Video-based Internet Streaming</p>
<ul>
<li><a href="/abeka-academy-streaming-video-vs-monarch">A Beka Video-based Internet Streaming vs. Monarch</a></li>
<li><a href="/abeka-academy-streaming-video-vs-horizons">A Beka Video-based Internet Streaming vs. Horizons</a></li>
<li><a href="/abeka-academy-streaming-video-vs-lifepac">A Beka Video-based Internet Streaming vs. LIFEPAC</a></li>
<li><a href="/abeka-academy-streaming-video-vs-aoa">A Beka Video-based Internet Streaming vs. Alpha Omega Academy</a></li>
<li><a href="/abeka-academy-streaming-video-vs-sos">A Beka Video-based Internet Streaming vs. Switched-On Schoolhouse</a></li>
</ul>
<p>*A Beka Book® and A Beka Academy® are the registered trademarks and subsidiaries of Pensacola Christian College. Alpha Omega Publications is not in any way affiliated with A Beka®. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by A Beka Book or A Beka Academy.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '0923609a-ab13-4272-85e4-f46354a33c37';
  $display->content['new-0923609a-ab13-4272-85e4-f46354a33c37'] = $pane;
  $display->panels['left'][0] = 'new-0923609a-ab13-4272-85e4-f46354a33c37';
  $pane = new stdClass();
  $pane->pid = 'new-8fa4a0cb-7080-4387-90a8-0efb6ffc5d3d';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '8fa4a0cb-7080-4387-90a8-0efb6ffc5d3d';
  $display->content['new-8fa4a0cb-7080-4387-90a8-0efb6ffc5d3d'] = $pane;
  $display->panels['right'][0] = 'new-8fa4a0cb-7080-4387-90a8-0efb6ffc5d3d';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-0923609a-ab13-4272-85e4-f46354a33c37';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
