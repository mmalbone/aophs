<?php
/**
 * @file
 * field.node.field_view_sort.curriculum.inc
 */

$api = '2.0.0';

$data = array(
  'field_config' => array(
    'active' => '1',
    'cardinality' => '1',
    'columns' => array(
      'value' => array(
        'not null' => FALSE,
        'type' => 'int',
      ),
    ),
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_view_sort',
    'field_permissions' => array(
      'type' => '0',
    ),
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => '0',
    'module' => 'number',
    'settings' => array(),
    'storage' => array(
      'active' => '1',
      'details' => array(
        'sql' => array(
          'FIELD_LOAD_CURRENT' => array(
            'field_data_field_view_sort' => array(
              'value' => 'field_view_sort_value',
            ),
          ),
          'FIELD_LOAD_REVISION' => array(
            'field_revision_field_view_sort' => array(
              'value' => 'field_view_sort_value',
            ),
          ),
        ),
      ),
      'module' => 'field_sql_storage',
      'settings' => array(),
      'type' => 'field_sql_storage',
    ),
    'translatable' => '0',
    'type' => 'number_integer',
  ),
  'field_instance' => array(
    'bundle' => 'curriculum',
    'default_value' => array(
      0 => array(
        'value' => '100',
      ),
    ),
    'deleted' => '0',
    'description' => 'Used for Sorting views of product other than search results which uses search boost',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => '11',
      ),
      'product_carousel' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'product_list' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => '',
    'field_name' => 'field_view_sort',
    'label' => 'view_sort',
    'required' => 0,
    'settings' => array(
      'max' => '100',
      'min' => '0',
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'select_or_other',
      'settings' => array(
        'apply_chosen' => '',
        'available_options' => '0|0
1|1
2|2
3|3
4|4
5|5
6|6
7|7
8|8
9|9
10|10
11|11
12|12
13|13
14|14
15|15
16|16
17|17
18|18
19|19
20|20
21|21
22|22
23|23
24|24
25|25
26|26
27|27
28|28
29|29
30|30
31|31
32|32
33|33
34|34
35|35
36|36
37|37
38|38
39|39
40|40
41|41
42|42
43|43
44|44
45|45
46|46
47|47
48|48
49|49
50|50
51|51
52|52
53|53
54|54
55|55
56|56
57|57
58|58
59|59
60|60
61|61
62|62
63|63
64|64
65|65
66|66
67|67
68|68
69|69
70|70
71|71
72|72
73|73
74|74
75|75
76|76
77|77
78|78
79|79
80|80
81|81
82|82
83|83
84|84
85|85
86|86
87|87
88|88
89|89
90|90
91|91
92|92
93|93
94|94
95|95
96|96
97|97
98|98
99|99
100|100',
        'available_options_php' => '',
        'markup_available_options_php' => '&lt;none&gt;',
        'other' => 'Other',
        'other_size' => '60',
        'other_title' => '',
        'other_unknown_defaults' => 'other',
        'sort_options' => 0,
      ),
      'type' => 'select_or_other',
      'weight' => '41',
    ),
  ),
);

$dependencies = array(
  'content_type.curriculum' => 'content_type.curriculum',
);

$optional = array();

$modules = array(
  0 => 'field_sql_storage',
  1 => 'number',
  2 => 'select_or_other',
);
