<?php
/**
 * @file
 * page_manager_pages.abeka.inc
 */

$api = '2.0.0';

$data = $page = new stdClass();
$page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
$page->api_version = 1;
$page->name = 'abeka';
$page->task = 'page';
$page->admin_title = 'Abeka';
$page->admin_description = '';
$page->path = 'abeka';
$page->access = array();
$page->menu = array(
  'type' => 'none',
  'title' => '',
  'weight' => '0',
  'name' => 'navigation',
  'parent' => array(
    'type' => 'none',
    'title' => '',
    'weight' => 0,
    'name' => 'navigation',
  ),
);
$page->arguments = array();
$page->conf = array();


$dependencies = array(
  'page_manager_handlers.page_abeka_panel_context' => 'page_manager_handlers.page_abeka_panel_context',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
