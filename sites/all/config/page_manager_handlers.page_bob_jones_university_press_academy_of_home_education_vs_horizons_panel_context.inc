<?php
/**
 * @file
 * page_manager_handlers.page_bob_jones_university_press_academy_of_home_education_vs_horizons_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_bob_jones_university_press_academy_of_home_education_vs_horizons_panel_context';
$handler->task = 'page';
$handler->subtask = 'bob_jones_university_press_academy_of_home_education_vs_horizons';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '3dd7699a-b1b7-405d-83cf-14a3335852e5';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-4e2a5a59-97b6-4ad3-b9bc-5967455933f3';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Bob Jones University Press Academy of Home Education vs. Horizons',
    'body' => '<p>Investigating the differences between Bob Jones University Press Academy of Home Education and Horizons curriculum from Alpha Omega Publications? We have what you\'re looking for! Find short overviews of each curriculum below, an easy-to-view comparison chart, and additional comparison options!</p><h3><strong>Bob Jones University Press Academy of Home Education</strong></h3><p>The Bob Jones University Press Academy of Home Education is designed to help homeschool parents by providing a recommended course of study to be completed under the direction of the parents. Enrollment in the Academy of Home Education is open to all 1st-12th grade students who agree to carry out a program of home education utilizing BJU Press curriculum in all academic areas in which these materials are available. All Bob Jones University Press curriculum is Bible-based, and presents instruction from a Christian worldview. Coursework is available in Bible, heritage studies, math, language (English, phonics, reading, handwriting, and spelling), and science. Families enrolled in the Academy of Home Education are provided with a variety of materials and services including, but not limited to, customer service and curriculum consultation, forms for recording student evaluation/grades, report cards reflecting parent evaluations, annual standardized testing, high school diploma and graduation ceremony, and formal transcript detailing student\'s academic record.</p><p>Students enrolled in the Academy of Home Education are required to complete a predetermined course of study over an enrollment period of at least 180 days. Some calendar requirements may apply to enrolled high school students. Fees for enrollment include all provided services, but the purchase of BJU Press curricular materials (textbooks, DVDs, hard drive, and/or online program) is the responsibility of the parents. Parents are also responsible for supervising of all student coursework, as well as all lesson planning and grading of daily work. Self-reported grades are sent to the Academy of Home Education office to be included in the student\'s permanent record.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>Are you wondering how to find the most effective homeschooling method for your family? Keep in mind that there is no one perfect homeschool curriculum. A blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Rather than feeling torn between curriculum formats, parents should be open to mixing and matching materials and resources to customize their child\'s education. Remember, each child has his own learning style so it\'s important to choose materials that best fit his needs.</p><h3><strong>Horizons from Alpha Omega Publications</strong></h3><p>Horizons is a preK-12 print-based curriculum offered by Alpha Omega Publications, a Christian-based publisher that provides curriculum and educational services to homeschool families. Filled with engaging, full-color illustrations, Horizons is a child-friendly workbook-based curriculum with consumable lessons and hands-on activities. Horizons curriculum encourages concept mastery through a spiral-based learning process of skill/concept presentation, review, and reinforcement. Teacher\'s guides offer clear, step-by-step lesson plans and activities that are suitable for both homeschool and classroom settings.</p><p>Winner of Cathy Duffy\'s 100 Top Picks for Homeschool Curriculum, Horizons integrates Scripture and Christian living throughout all subjects. Horizons workbooks provide removable student pages for ease of student use. Horizons curriculum includes courses in math (K-8), penmanship (1-5), health (K-8), physical education (PreK-12), phonics and reading (K-3), and spelling and vocabulary (1-3). Health and physical education electives were designed for classroom use and may need to be adapted to homeschool use. Complete Horizons Preschool Curriculum Sets and Multimedia Sets are also available. Horizons curriculum sets include all student and teacher materials. Student and teacher materials can also be purchased separately.</p><h3><strong>Benefits and Features of Horizons from Alpha Omega Publications</strong></h3><p class="list_heading">Horizons</p><ul><li>brightly illustrated, hands-on, consumable curriculum</li><li>offers fun lessons and manipulative-based activities</li><li>teaches concepts through a spiral learning process</li><li>encourages concept mastery through quick concept introduction, review, and reinforcement</li></ul><h3 class="para-start"><strong>Take a Closer Look at Bob Jones University Press Academy of Home Education and Horizons</strong></h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">Horizons</th><th class="compare">BJU Press Academy of Home Education</th></tr>
</thead>
<tbody>
<tr>
<td>K-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Instruction provided by parent</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Interactive, one-on-one learning format</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Offers a variety of instructional options</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Readiness tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Flexible scheduling and pacing</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Manageable amount of daily lesson material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Easy to use teacher\'s guides</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Material designed for homeschoolers</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Requires materials to be returned</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Ability to pick and choose student and teacher materials</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr><th class="features">Cost</th><th class="compare">Alpha Omega Publications</th><th class="compare">Bob Jones University Press</th></tr>
</thead>
<tbody>
<tr>
<td>Average yearly cost per grade level**</td>
<td class="tac">N/A</td>
<td class="tac">$250-300 (plus curriculum)</td>
</tr>
<tr class="alt">
<td>Average subject cost per grade level**</td>
<td class="tac">$79.95</td>
<td class="tac">N/A</td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: Bob Jones University Press versus Alpha Omega Publications</h4>
<p class="list_heading_links">Bob Jones University Press Textbooks</p>
<ul>
<li><a href="/bju-textbook-vs-monarch">Bob Jones University Press Textbooks vs. Monarch</a></li>
<li><a href="/bju-textbook-vs-horizons">Bob Jones University Press Textbooks vs. Horizons</a></li>
<li><a href="/bju-textbook-vs-lifepac">Bob Jones University Press Textbooks vs. LIFEPAC</a></li>
<li><a href="/bju-textbook-vs-aoa">Bob Jones University Press Textbooks vs. Alpha Omega Academy</a></li>
<li><a href="/bju-textbook-vs-sos">Bob Jones University Press Textbooks vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">Bob Jones University Press (DVD)</p>
<ul>
<li><a href="/bju-dvd-vs-monarch">Bob Jones University Press (DVD) vs. Monarch</a></li>
<li><a href="/bju-dvd-vs-horizons">Bob Jones University Press (DVD) vs. Horizons</a></li>
<li><a href="/bju-dvd-vs-lifepac">Bob Jones University Press (DVD) vs. LIFEPAC</a></li>
<li><a href="/bju-dvd-vs-aoa">Bob Jones University Press (DVD) vs. Alpha Omega Academy</a></li>
<li><a href="/bju-dvd-vs-sos">Bob Jones University Press (DVD) vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">Bob Jones University Press Academy (Hard Drive)</p>
<ul>
<li><a href="/bju-hard-drive-vs-monarch">Bob Jones University Press Academy (Hard Drive) vs. Monarch</a></li>
<li><a href="/bju-hard-drive-vs-horizons">Bob Jones University Press Academy (Hard Drive) vs. Horizons</a></li>
<li><a href="/bju-hard-drive-vs-lifepac">Bob Jones University Press Academy (Hard Drive) vs. LIFEPAC</a></li>
<li><a href="/bju-hard-drive-vs-aoa">Bob Jones University Press Academy (Hard Drive) vs. Alpha Omega Academy</a></li>
<li><a href="/bju-hard-drive-vs-sos">Bob Jones University Press Academy (Hard Drive) vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">Bob Jones University Press Academy (Distance Learning Online)</p>
<ul>
<li><a href="/bju-online-vs-monarch">Bob Jones University Press Academy (Distance Learning Online) vs. Monarch</a></li>
<li><a href="/bju-online-vs-horizons">Bob Jones University Press Academy (Distance Learning Online) vs. Horizons</a></li>
<li><a href="/bju-online-vs-lifepac">Bob Jones University Press Academy (Distance Learning Online) vs. LIFEPAC</a></li>
<li><a href="/bju-online-vs-aoa">Bob Jones University Press Academy (Distance Learning Online) vs. Alpha Omega Academy</a></li>
<li><a href="/bju-online-vs-sos">Bob Jones University Press Academy (Distance Learning Online) vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">Bob Jones University Press Academy (Academy of Home Education)</p>
<ul>
<li><a href="/bju-academy-home-education-vs-monarch">Bob Jones University Press Academy (Academy of Home Education) vs. Monarch</a></li>
<li><a href="/bju-academy-home-education-vs-horizons">Bob Jones University Press Academy (Academy of Home Education) vs. Horizons</a></li>
<li><a href="/bju-academy-home-education-vs-lifepac">Bob Jones University Press Academy (Academy of Home Education) vs. LIFEPAC</a></li>
<li><a href="/bju-academy-home-education-vs-aoa">Bob Jones University Press Academy (Academy of Home Education) vs. Alpha Omega Academy</a></li>
<li><a href="/bju-academy-home-education-vs-sos">Bob Jones University Press Academy (Academy of Home Education) vs. Switched-On Schoolhouse</a></li>
</ul>
<p>*BJU Press® is the registered trademark and subsidiary of Bob Jones University®. Alpha Omega Publications is not in any way affiliated with Bob Jones University Press or its trademark owner Bob Jones University. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by BJU Press.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p><p>**Prices obtained February 2013 from BJU Press Total Homeschool Solutions and BJUP websites and from Alpha Omega Publications 2013 Homeschool Catalog. Yearly prices are based on comparison between Horizons 3rd grade full year cost (not available) and BJU Press Academy of Home Education enrollment for a full year (cost of curriculum not included). Individual course prices based on comparison between Horizons 3rd Grade Math Set (including all student and teacher material, along with supplements) and BJU Press Academy of Home Education individual course fees (not available). Costs do not reflect any additional fees or shipping and handling charges. Prices for other Bob Jones University Press and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '4e2a5a59-97b6-4ad3-b9bc-5967455933f3';
  $display->content['new-4e2a5a59-97b6-4ad3-b9bc-5967455933f3'] = $pane;
  $display->panels['left'][0] = 'new-4e2a5a59-97b6-4ad3-b9bc-5967455933f3';
  $pane = new stdClass();
  $pane->pid = 'new-90376c6c-b626-4fb9-ad07-f71c9ea6318d';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '90376c6c-b626-4fb9-ad07-f71c9ea6318d';
  $display->content['new-90376c6c-b626-4fb9-ad07-f71c9ea6318d'] = $pane;
  $display->panels['right'][0] = 'new-90376c6c-b626-4fb9-ad07-f71c9ea6318d';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-4e2a5a59-97b6-4ad3-b9bc-5967455933f3';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
