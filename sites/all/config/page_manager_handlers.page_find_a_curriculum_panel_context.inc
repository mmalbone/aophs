<?php
/**
 * @file
 * page_manager_handlers.page_find_a_curriculum_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_find_a_curriculum_panel_context';
$handler->task = 'page';
$handler->subtask = 'find_a_curriculum';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => 'find-a-curriculum',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'flexible';
$display->layout_settings = array(
  'items' => array(
    'canvas' => array(
      'type' => 'row',
      'contains' => 'column',
      'children' => array(
        0 => 'main',
      ),
      'parent' => NULL,
    ),
    'main' => array(
      'type' => 'column',
      'width' => 100,
      'width_type' => '%',
      'children' => array(
        0 => 'main-row',
        1 => 1,
        2 => 2,
      ),
      'parent' => 'canvas',
    ),
    'main-row' => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'center',
      ),
      'parent' => 'main',
    ),
    'center' => array(
      'type' => 'region',
      'title' => 'Center',
      'width' => 100,
      'width_type' => '%',
      'parent' => 'main-row',
    ),
    1 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'center_',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    'center_' => array(
      'type' => 'region',
      'title' => 'Center',
      'width' => 100,
      'width_type' => '%',
      'parent' => '1',
      'class' => '',
    ),
    2 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'left',
        1 => 'right',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    'left' => array(
      'type' => 'region',
      'title' => 'Left',
      'width' => 50,
      'width_type' => '%',
      'parent' => '2',
      'class' => '',
    ),
    'right' => array(
      'type' => 'region',
      'title' => 'Right',
      'width' => 50,
      'width_type' => '%',
      'parent' => '2',
      'class' => '',
    ),
  ),
);
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'center' => NULL,
    'center_' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'a10d1b9a-c709-479a-b8a6-453637f7d86c';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-892697ed-3788-4a26-b6ca-298781ef2581';
  $pane->panel = 'center';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Find Your Curriculum',
    'body' => '<p>With print-based, computer-based, and online formats, you can quickly determine the perfect educational option for your homeschool family.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'find-your-curriculum-banner',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '892697ed-3788-4a26-b6ca-298781ef2581';
  $display->content['new-892697ed-3788-4a26-b6ca-298781ef2581'] = $pane;
  $display->panels['center'][0] = 'new-892697ed-3788-4a26-b6ca-298781ef2581';
  $pane = new stdClass();
  $pane->pid = 'new-2305599f-39d9-4064-8093-385096643ad0';
  $pane->panel = 'center';
  $pane->type = 'block';
  $pane->subtype = 'aop_curriculum_guide-aop_curriculum_guide';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '2305599f-39d9-4064-8093-385096643ad0';
  $display->content['new-2305599f-39d9-4064-8093-385096643ad0'] = $pane;
  $display->panels['center'][1] = 'new-2305599f-39d9-4064-8093-385096643ad0';
  $pane = new stdClass();
  $pane->pid = 'new-1ea21924-ba77-4e8e-94ff-e08bbbabc708';
  $pane->panel = 'center_';
  $pane->type = 'block';
  $pane->subtype = 'block-8';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '1ea21924-ba77-4e8e-94ff-e08bbbabc708';
  $display->content['new-1ea21924-ba77-4e8e-94ff-e08bbbabc708'] = $pane;
  $display->panels['center_'][0] = 'new-1ea21924-ba77-4e8e-94ff-e08bbbabc708';
  $pane = new stdClass();
  $pane->pid = 'new-f3844093-b147-45d7-a92a-64d381f9faec';
  $pane->panel = 'center_';
  $pane->type = 'block';
  $pane->subtype = 'block-10';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => 'Switched-On',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'f3844093-b147-45d7-a92a-64d381f9faec';
  $display->content['new-f3844093-b147-45d7-a92a-64d381f9faec'] = $pane;
  $display->panels['center_'][1] = 'new-f3844093-b147-45d7-a92a-64d381f9faec';
  $pane = new stdClass();
  $pane->pid = 'new-ac77cdff-940d-4a01-bbd2-370cb098fb4a';
  $pane->panel = 'center_';
  $pane->type = 'block';
  $pane->subtype = 'block-9';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'ac77cdff-940d-4a01-bbd2-370cb098fb4a';
  $display->content['new-ac77cdff-940d-4a01-bbd2-370cb098fb4a'] = $pane;
  $display->panels['center_'][2] = 'new-ac77cdff-940d-4a01-bbd2-370cb098fb4a';
  $pane = new stdClass();
  $pane->pid = 'new-913dac8b-af10-449a-9909-cb9aa6b2177a';
  $pane->panel = 'center_';
  $pane->type = 'block';
  $pane->subtype = 'block-11';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = '913dac8b-af10-449a-9909-cb9aa6b2177a';
  $display->content['new-913dac8b-af10-449a-9909-cb9aa6b2177a'] = $pane;
  $display->panels['center_'][3] = 'new-913dac8b-af10-449a-9909-cb9aa6b2177a';
  $pane = new stdClass();
  $pane->pid = 'new-664ff2d1-534e-44bf-b538-b6761d1ff0e4';
  $pane->panel = 'center_';
  $pane->type = 'block';
  $pane->subtype = 'block-7';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = '664ff2d1-534e-44bf-b538-b6761d1ff0e4';
  $display->content['new-664ff2d1-534e-44bf-b538-b6761d1ff0e4'] = $pane;
  $display->panels['center_'][4] = 'new-664ff2d1-534e-44bf-b538-b6761d1ff0e4';
  $pane = new stdClass();
  $pane->pid = 'new-a4b0e5b2-6b10-489d-9596-c5e8264ae3a1';
  $pane->panel = 'center_';
  $pane->type = 'block';
  $pane->subtype = 'block-23';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 5;
  $pane->locks = array();
  $pane->uuid = 'a4b0e5b2-6b10-489d-9596-c5e8264ae3a1';
  $display->content['new-a4b0e5b2-6b10-489d-9596-c5e8264ae3a1'] = $pane;
  $display->panels['center_'][5] = 'new-a4b0e5b2-6b10-489d-9596-c5e8264ae3a1';
  $pane = new stdClass();
  $pane->pid = 'new-08754ec4-07a8-4ac6-996c-85de067cdee6';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'block-17';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '08754ec4-07a8-4ac6-996c-85de067cdee6';
  $display->content['new-08754ec4-07a8-4ac6-996c-85de067cdee6'] = $pane;
  $display->panels['left'][0] = 'new-08754ec4-07a8-4ac6-996c-85de067cdee6';
  $pane = new stdClass();
  $pane->pid = 'new-e5e7e088-53e9-4d8d-ad77-af0001663ac2';
  $pane->panel = 'right';
  $pane->type = 'block';
  $pane->subtype = 'block-21';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'e5e7e088-53e9-4d8d-ad77-af0001663ac2';
  $display->content['new-e5e7e088-53e9-4d8d-ad77-af0001663ac2'] = $pane;
  $display->panels['right'][0] = 'new-e5e7e088-53e9-4d8d-ad77-af0001663ac2';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = '0';
$handler->conf['display'] = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
