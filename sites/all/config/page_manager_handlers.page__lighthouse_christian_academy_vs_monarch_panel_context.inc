<?php
/**
 * @file
 * page_manager_handlers.page__lighthouse_christian_academy_vs_monarch_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page__lighthouse_christian_academy_vs_monarch_panel_context';
$handler->task = 'page';
$handler->subtask = '_lighthouse_christian_academy_vs_monarch';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '4a70e5e5-8a8f-4a66-ac9a-f5f0007824e4';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-9d1f35da-d984-4b97-85d6-a5fb6886294f';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Lighthouse Christian Academy vs. Monarch',
    'body' => '<p>Are you investigating the differences between A.C.E.\'s Lighthouse Christian Academy and the Monarch curriculum from Alpha Omega Publications? You just found the information you need! Find short overviews of each curriculum below, an easy-to-view comparison chart, and additional comparison options!</p><h3><strong>A.C.E.\'s Lighthouse Christian Academy</strong></h3><p>Lighthouse Christian Academy (LCA) is the provider of distance learning for Accelerated Christian Education (A.C.E.). Lighthouse Christian Academy is a fully accredited (MSA-CESS and Ai) program designed to assist parents in providing a quality Christian education to homeschooled students. Lighthouse Christian Academy exclusively utilizes the A.C.E. curriculum for students in grades K-12 and provides coursework in all core subjects as well as electives.</p><p>Lighthouse Christian Academy provides its students with diagnostic testing to determine placement within the A.C.E. curriculum program. Academic advising assists families in designing a course of study appropriate for each student\'s interests and goals. Honors, college-prep, general, and vocational courses are offered to all high school students. In addition to diagnostic testing, LCA provides progress reports, transcripts, standardized testing for all enrolled students. LCA provides graduation services and a diploma for eligible seniors. Enrollment and administrative fees include all provided services, but the purchase of A.C.E. curricular materials is the responsibility of the parents. Parents are responsible for supervision of all student coursework, as well as all lesson planning and grading of daily work. Completed coursework is submitted to the Lighthouse Christian Academy office for evaluation and reporting.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>Wondering whether you should use a distance learning approach to homeschooling? Keep in mind there is no one perfect homeschool curriculum. A blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Instead of feeling torn between curriculum formats, parents should be open to mixing and matching materials and resources to customize their child\'s education. Remember, each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3><strong>Monarch from Alpha Omega Publications</strong></h3><p>Monarch from Alpha Omega Publications is an interactive, Internet-based curriculum for grades 3-12. Alpha Omega Publications, a trusted publisher of high quality educational products, provides Christian curriculum and educational services to homeschool families. Transforming the way students learn, Monarch is an in-depth, online curriculum that makes use of today\'s 21st century technology. This innovative, cutting-edge AOP curriculum presents Christ-centered lessons using eye-catching animations, learning games, video and audio clips, and more! Unlike traditional textbooks, Monarch has a diverse mix of text-based instruction and engaging multimedia enrichment. Time-saving teaching tools provide automatic grading, lesson planning, customizable lessons, a built-in calendar, printing options, message center, text-to-speech, and more!</p><p>Compatible with both Windows® and Macintosh® operating systems, Monarch integrates Scripture and a biblical worldview into all subjects. Monarch offers the flexibility to log on and learn anytime, anywhere with 18-month, single subscriptions using an individualized, student driven approach. Diagnostic tests are available for placement in math and language arts. Core coursework includes Bible (3-12), language arts (3-12), math (3-12), science (3-12), and history and geography (3-12). Over 35 electives are also available. Monarch may be purchased as individual subjects or as complete five-subject sets.</p><h3><strong>Benefits and Features of Monarch from Alpha Omega Publications</strong></h3><p class="list_heading">Monarch</p><ul><li>online interactive lessons with anytime, anywhere access</li><li>compatible with both Windows® and Mac® operating systems</li><li>offers customizable student learning options and time-saving teaching tools</li><li>features dynamic, media-rich lessons; real-time content updates; and data storage</li></ul>
<h3>Take a Closer Look at Lighthouse Christian Academy and Monarch</h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">Monarch</th><th class="compare">Lighthouse Christian Academy</th></tr>
</thead>
<tbody>
<tr>
<td>3-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Accredited program</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Internet-based curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Interactive, engaging multimedia tools</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Customizable lessons and calendar</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Flexible and individualized</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Manageable amount of daily lesson material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Automatic grading and lesson planning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Message center</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr class="alt"><th class="features">Cost</th><th class="compare">Monarch</th><th class="compare">A.C.E.</th></tr>
</thead>
<tbody>
<tr>
<td>Average yearly cost per grade level**</td>
<td class="tac">$449.95</td>
<td class="tac">$950</td>
</tr>
<tr class="alt">
<td>Average subject cost per grade level**</td>
<td class="tac">$99.95</td>
<td class="tac">N/A</td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: A.C.E. versus Alpha Omega Publications</h4>
<p class="list_heading_links">A.C.E. PACE</p>
<ul>
<li><a href="/ace-pace-vs-monarch">A.C.E. PACE vs. Monarch</a></li>
<li><a href="/ace-pace-vs-horizons">A.C.E. PACE vs. Horizons</a></li>
<li><a href="/ace-pace-vs-lifepac">A.C.E. PACE vs. LIFEPAC</a></li>
<li><a href="/ace-pace-vs-aoa">A.C.E. PACE vs. Alpha Omega Academy</a></li>
<li><a href="/ace-pace-vs-sos">A.C.E. PACE vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">A.C.E. Lighthouse Christian Academy</p>
<ul>
<li><a href="/ace-lca-vs-monarch">A.C.E. Lighthouse Christian Academy vs. Monarch</a></li>
<li><a href="/ace-lca-vs-horizons">A.C.E. Lighthouse Christian Academy vs. Horizons</a></li>
<li><a href="/ace-lca-vs-lifepac">A.C.E. Lighthouse Christian Academy vs. LIFEPAC</a></li>
<li><a href="/ace-lca-vs-aoa">A.C.E. Lighthouse Christian Academy vs. Alpha Omega Academy</a></li>
<li><a href="/ace-lca-vs-sos">A.C.E. Lighthouse Christian Academy vs. Switched-On Schoolhouse</a></li>
</ul>
<p>*A.C.E.® and PACE® are the registered trademarks and subsidiaries of A.C.E.®. Alpha Omega Publications is not in any way affiliated with A.C.E.. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by A.C.E.®.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p><p>**Prices obtained February 2013 from Accelerated Christian Education Homeschool and A.C.E. Ministries websites and from Alpha Omega Publications 2013 Homeschool Catalog. Yearly prices based on comparison between Monarch 9th Grade 5-Subject Set and A.C.E. Lighthouse Christian Academy full-time administrative costs for a full time high school student (not including registration fees). Costs do not reflect any additional fees or shipping and handling charges. Prices for other Accelerated Christian Education and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '9d1f35da-d984-4b97-85d6-a5fb6886294f';
  $display->content['new-9d1f35da-d984-4b97-85d6-a5fb6886294f'] = $pane;
  $display->panels['left'][0] = 'new-9d1f35da-d984-4b97-85d6-a5fb6886294f';
  $pane = new stdClass();
  $pane->pid = 'new-fbeaefb9-be40-460a-8228-993c9d181f59';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'fbeaefb9-be40-460a-8228-993c9d181f59';
  $display->content['new-fbeaefb9-be40-460a-8228-993c9d181f59'] = $pane;
  $display->panels['right'][0] = 'new-fbeaefb9-be40-460a-8228-993c9d181f59';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-9d1f35da-d984-4b97-85d6-a5fb6886294f';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
