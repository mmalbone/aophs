<?php
/**
 * @file
 * page_manager_handlers.page_a_beka_academy_dvd_vs_monarch_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_a_beka_academy_dvd_vs_monarch_panel_context';
$handler->task = 'page';
$handler->subtask = 'a_beka_academy_dvd_vs_monarch';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '125d033c-dba7-4962-afc3-2a6ed1ecc407';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-ffc59976-8f54-4527-bd76-57aa7ca7289b';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'A Beka Academy DVD vs. Monarch',
    'body' => '<p>Are you investigating the difference between the A Beka Academy DVD program and the online Monarch curriculum from Alpha Omega Publications? You just found the information you need! Find short overviews of each curriculum below, an easy-to-view comparison chart, and additional comparison options!</p><h3><strong>A Beka Academy DVDs</strong></h3><p>Available for homeschoolers in grades K5-12, the A Beka Academy DVD Program features recorded lessons presented in the classroom by Pensacola Christian Academy teachers. The A Beka curriculum is Bible-based and presents instruction from a Christian worldview. A Beka Academy offers coursework in Bible, reading, history, mathematics, science, health, and English. Electives are also offered. A Beka Academy DVD Program combines recorded classroom lectures with A Beka Book textbooks and teacher materials. When enrolled in this program, students watch recorded lesson presentations and complete student work in A Beka curriculum textbooks and workbooks.</p><p>Students can select A Beka Academy enrollment in either an accredited (FACCS, SACS) or a non-accredited DVD program. Families enrolled in the accredited program are provided with all DVDs, books, tests, and keys for selected courses, teacher\'s manual with daily lesson plans, a pre-set academic calendar, a plan of study, evaluation of student work, report cards, high school diploma, transcripts, prepaid return labels for DVDs and graded student work, and access to academic assistance. Parents provide supervision as well as grading of completed work. Families enrolled in the non-accredited program are provided with all DVDs, books, tests, and keys, teacher\'s manual with lesson plans, progress reports for parental record-keeping, prepaid return labels for DVD\'s, and access to academic assistance. Parents are responsible for all supervision, grading, and record-keeping. A Beka Academy requires all DVDs and student coursework to be returned within 12 months of start date, or late fees will be charged.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>Wondering whether you should use a computer-based or Internet-based approach to homeschooling? Keep in mind that there is no one perfect homeschool curriculum. A blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Instead of feeling torn between curriculum formats, parents should be open to mixing and matching materials and resources to customize their child\'s education. Remember, each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3><strong>Monarch from Alpha Omega Publications</strong></h3><p>Monarch from Alpha Omega Publications is an interactive, Internet-based curriculum for grades 3-12. Alpha Omega Publications, a trusted publisher of high quality educational products, provides Christian curriculum and educational services to homeschool families. Transforming the way students learn, Monarch is an in-depth, online curriculum that makes use of today\'s 21st century technology. This innovative, cutting-edge AOP curriculum presents Christ-centered lessons using eye-catching animations, learning games, video and audio clips, and more! Unlike traditional textbooks, Monarch has a diverse mix of text-based instruction and engaging multimedia enrichment. Time-saving teaching tools provide automatic grading, lesson planning, customizable lessons, a built-in calendar, printing options, message center, text-to-speech, and more!</p><p>Compatible with both Windows® and Macintosh® operating systems, Monarch integrates Scripture and a biblical worldview into all subjects. Monarch offers the flexibility to log on and learn anytime, anywhere with 18-month, single subscriptions using an individualized, student driven approach. Diagnostic tests are available for placement in math and language arts. Core coursework includes Bible (3-12), language arts (3-12), math (3-12), science (3-12), and history and geography (3-12). Over 35 electives are also available. Monarch may be purchased as individual subjects or as complete five-subject sets.</p><h3><strong>Benefits and Features of Monarch from Alpha Omega Publications</strong></h3><p class="list_heading">Monarch</p><ul><li>online interactive lessons with anytime, anywhere access</li><li>compatible with both Windows® and Mac® operating systems</li><li>offers customizable student learning options and time-saving teaching tools</li><li>features dynamic, media-rich lessons; real-time content updates; and data storage</li></ul>
<h3 class="para-start"><strong>Take a Closer Look at A Beka Academy DVDs and Monarch</strong></h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">Monarch</th><th class="compare">A Beka Academy DVD</th></tr>
</thead>
<tbody>
<tr>
<td>3-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Accredited program</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark">(available)</td>
</tr>
<tr class="alt">
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Internet-based curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Interactive, engaging multimedia tools</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Instruction provided via recorded lesson sessions</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Customizable lessons and calendar</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Flexible and individualized</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Manageable amount of daily lesson material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Automatic grading and lesson planning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Requires material to be returned and charges fees if late</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Message and resource center</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr><th class="features">Cost</th><th class="compare">Monarch</th><th class="compare">Academy DVDs (with A Beka Books)</th></tr>
</thead>
<tbody>
<tr>
<td>Average yearly cost per grade level**</td>
<td class="tac">$449.95</td>
<td class="tac">$895</td>
</tr>
<tr class="alt">
<td>Average subject cost per grade level**</td>
<td class="tac">$99.95</td>
<td class="tac">$395</td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: A Beka versus Alpha Omega Publications</h4>
<p class="list_heading_links">A Beka Book</p>
<ul>
<li><a href="/abeka-book-vs-monarch">A Beka Book vs. Monarch</a></li>
<li><a href="/abeka-book-vs-horizons">A Beka Book vs. Horizons</a></li>
<li><a href="/abeka-book-vs-lifepac">A Beka Book vs. LIFEPAC</a></li>
<li><a href="/abeka-book-vs-aoa">A Beka Book vs. Alpha Omega Academy</a></li>
<li><a href="/abeka-book-vs-sos">A Beka Book vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">A Beka Academy (DVD)</p>
<ul>
<li><a href="/abeka-dvd-vs-monarch">A Beka Academy (DVD) vs. Monarch</a></li>
<li><a href="/abeka-dvd-vs-horizons">A Beka Academy (DVD) vs. Horizons</a></li>
<li><a href="/abeka-dvd-vs-lifepac">A Beka Academy (DVD) vs. LIFEPAC</a></li>
<li><a href="/abeka-dvd-vs-aoa">A Beka Academy (DVD) vs. Alpha Omega Academy</a></li>
<li><a href="/abeka-dvd-vs-sos">A Beka Academy (DVD) vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">A Beka Academy (Parent-Directed)</p>
<ul>
<li><a href="/abeka-parent-directed-vs-monarch">A Beka Academy (Parent-Directed) vs. Monarch</a></li>
<li><a href="abeka-parent-directed-vs-horizons">A Beka Academy (Parent-Directed) vs. Horizons</a></li>
<li><a href="/abeka-parent-directed-vs-lifepac">A Beka Academy (Parent-Directed) vs. LIFEPAC</a></li>
<li><a href="/abeka-parent-directed-vs-aoa">A Beka Academy (Parent-Directed) vs. Alpha Omega Academy</a></li>
<li><a href="/abeka-parent-directed-vs-sos">A Beka Academy (Parent-Directed) vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">A Beka Video-based Internet Streaming</p>
<ul>
<li><a href="/abeka-academy-streaming-video-vs-monarch">A Beka Video-based Internet Streaming vs. Monarch</a></li>
<li><a href="/abeka-academy-streaming-video-vs-horizons">A Beka Video-based Internet Streaming vs. Horizons</a></li>
<li><a href="/abeka-academy-streaming-video-vs-lifepac">A Beka Video-based Internet Streaming vs. LIFEPAC</a></li>
<li><a href="/abeka-academy-streaming-video-vs-aoa">A Beka Video-based Internet Streaming vs. Alpha Omega Academy</a></li>
<li><a href="/abeka-academy-streaming-video-vs-sos">A Beka Video-based Internet Streaming vs. Switched-On Schoolhouse</a></li>
</ul>
<p>*A Beka Book® and A Beka Academy® are the registered trademarks and subsidiaries of Pensacola Christian College. Alpha Omega Publications is not in any way affiliated with A Beka®. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by A Beka Book or A Beka Academy.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p><p>**Prices obtained February 2013 from A Beka Academy\'s website and from Alpha Omega Publications 2013 Homeschool Catalog. Yearly prices based on comparison between Monarch 3rd Grade 5-Subject Set and A Beka Academy 3rd Grade DVD Video Complete pricing for full year. Subject prices based on comparison between Monarch 3rd Grade Math (including all student and teacher material, along with supplements) and A Beka Academy Grade 1-6 Subject Combination for DVD Video Complete option. Costs do not reflect any additional fees or shipping and handling charges. Prices for other A Beka and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'ffc59976-8f54-4527-bd76-57aa7ca7289b';
  $display->content['new-ffc59976-8f54-4527-bd76-57aa7ca7289b'] = $pane;
  $display->panels['left'][0] = 'new-ffc59976-8f54-4527-bd76-57aa7ca7289b';
  $pane = new stdClass();
  $pane->pid = 'new-bc027ef6-2f91-491e-8d9a-41c7458246b2';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'bc027ef6-2f91-491e-8d9a-41c7458246b2';
  $display->content['new-bc027ef6-2f91-491e-8d9a-41c7458246b2'] = $pane;
  $display->panels['right'][0] = 'new-bc027ef6-2f91-491e-8d9a-41c7458246b2';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-ffc59976-8f54-4527-bd76-57aa7ca7289b';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
