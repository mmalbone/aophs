<?php
/**
 * @file
 * content_type.curated_product_list.inc
 */

$api = '2.0.0';

$data = (object) array(
  'base' => 'node_content',
  'description' => 'A list of products to be displayed on the product listing pages.   ',
  'has_title' => '1',
  'help' => '',
  'name' => 'Curated Product List',
  'title_label' => 'Curated List Title',
  'type' => 'curated_product_list',
);

$dependencies = array();

$optional = array(
  'field.node.field_curated_product.curated_product_list' => 'field.node.field_curated_product.curated_product_list',
  'field.node.field_curriculum_associated_with.curated_product_list' => 'field.node.field_curriculum_associated_with.curated_product_list',
  'field.node.title_field.curated_product_list' => 'field.node.title_field.curated_product_list',
  'permission.create_curated_product_list_content' => 'permission.create_curated_product_list_content',
  'permission.delete_any_curated_product_list_content' => 'permission.delete_any_curated_product_list_content',
  'permission.delete_own_curated_product_list_content' => 'permission.delete_own_curated_product_list_content',
  'permission.edit_any_curated_product_list_content' => 'permission.edit_any_curated_product_list_content',
  'permission.edit_own_curated_product_list_content' => 'permission.edit_own_curated_product_list_content',
);

$modules = array(
  0 => 'node',
);
