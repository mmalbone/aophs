<?php
/**
 * @file
 * page_manager_handlers.page_a_beka_book_vs_monarch_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_a_beka_book_vs_monarch_panel_context';
$handler->task = 'page';
$handler->subtask = 'a_beka_book_vs_monarch';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'bfeff031-83ad-4d25-897b-4a2b9c1238a8';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-9eba7f28-462d-4132-a619-858204508e55';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'A Beka Book vs. Monarch',
    'body' => '<p>Looking to understand the difference between A Beka Book textbooks and Monarch curriculum from Alpha Omega Publications? We have the information you need. Find short overview of each curriculum below, an easy-to-view comparison chart, and additional comparison options!</p><h3><strong>A Beka Book</strong></h3><p>A Beka Book, an in-house publisher, provides nursery-12th grade Christian curriculum and educational materials to Christian schools and homeschoolers. A Beka curriculum materials were designed for use in a Christian classroom setting and as such, tend to be teacher-intensive and often less friendly in a homeschool situation. This teacher-driven, structured approach requires homeschool parents to complete many time-consuming administrative tasks, including lesson-planning and grading. A Beka Book teacher\'s guides are well-suited to a school setting and include many activities that may be difficult to adapt to homeschool situations.</p><p>Written from a Christian worldview, all A Beka coursework is completely Bible-based. All science instruction is based on a biblical view of creation and the origin of life. A Beka Book coursework offerings include Bible, reading, history, mathematics, science, health, English, and a number of electives. Parents are responsible for all administrative tasks, including grading, record-keeping, testing, and reporting.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>Trying to decide whether a print-based or Internet-based curriculum will work best for your child? Keep in mind that there is no one perfect homeschool curriculum. Giving the flexibility of using multiple resources instead of one set curriculum, a blended approach to homeschooling is often chosen by parents. Instead of feeling torn between different curricula, parents should feel free to mix and match materials and resources to customize their child\'s education. Each child is unique and has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3><strong>Monarch from Alpha Omega Publications</strong></h3><p>Monarch from Alpha Omega Publications is an interactive, Internet-based curriculum for grades 3-12. Alpha Omega Publications, a trusted publisher of high quality educational products, provides Christian curriculum and educational services to homeschool families. Transforming the way students learn, Monarch is an in-depth, online curriculum that makes use of today\'s 21st century technology. This innovative, cutting-edge AOP curriculum presents Christ-centered lessons using eye-catching animations, learning games, video and audio clips, and more! Unlike traditional textbooks, Monarch has a diverse mix of text-based instruction and engaging multimedia enrichment. Time-saving teaching tools provide automatic grading, lesson planning, customizable lessons, a built-in calendar, printing options, message center, text-to-speech, and more!</p><p>Compatible with both Windows® and Macintosh® operating systems, Monarch integrates Scripture and a biblical worldview into all subjects. Monarch offers the flexibility to log on and learn anytime, anywhere with 18-month, single subscriptions using an individualized, student driven approach. Diagnostic tests are available for placement in math and language arts. Core coursework includes Bible (3-12), language arts (3-12), math (3-12), science (3-12), and history and geography (3-12). Over 35 electives are also available. Monarch may be purchased as individual subjects or as complete five-subject sets.</p><h3><strong>Benefits and Features of Monarch from Alpha Omega Publications</strong></h3><p class="list_heading">Monarch</p><ul><li>online interactive lessons with anytime, anywhere access</li><li>compatible with both Windows® and Mac® operating systems</li><li>offers customizable student learning options and time-saving teaching tools</li><li>features dynamic, media-rich lessons; real-time content updates; and data storage</li></ul>
<h3 class="para-start"><strong>Take a Closer Look at A Beka Book and Monarch</strong></h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">Monarch</th><th class="compare">A Beka Book</th></tr>
</thead>
<tbody>
<tr>
<td>3-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Parent-directed curriculum</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Internet-based options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Interactive, engaging multimedia tools</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Customizable lessons and calendar</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Placement tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Automatic grading and lesson planning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Automatic updated material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Flexible and individualized</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Mastery-based learning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Message center</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr><th class="features">Cost</th><th class="compare">Monarch</th><th class="compare">A Beka Book</th></tr>
</thead>
<tbody>
<tr>
<td>Average yearly cost per grade level**</td>
<td class="tac">$449.95</td>
<td class="tac">$478.40</td>
</tr>
<tr class="alt">
<td>Average subject cost per grade level**</td>
<td class="tac">$99.95</td>
<td class="tac">$120.10</td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: A Beka versus Alpha Omega Publications</h4>
<p class="list_heading_links">A Beka Book</p>
<ul>
<li><a href="/abeka-book-vs-monarch">A Beka Book vs. Monarch</a></li>
<li><a href="/abeka-book-vs-horizons">A Beka Book vs. Horizons</a></li>
<li><a href="/abeka-book-vs-lifepac">A Beka Book vs. LIFEPAC</a></li>
<li><a href="/abeka-book-vs-aoa">A Beka Book vs. Alpha Omega Academy</a></li>
<li><a href="/abeka-book-vs-sos">A Beka Book vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">A Beka Academy (DVD)</p>
<ul>
<li><a href="/abeka-dvd-vs-monarch">A Beka Academy (DVD) vs. Monarch</a></li>
<li><a href="/abeka-dvd-vs-horizons">A Beka Academy (DVD) vs. Horizons</a></li>
<li><a href="/abeka-dvd-vs-lifepac">A Beka Academy (DVD) vs. LIFEPAC</a></li>
<li><a href="/abeka-dvd-vs-aoa">A Beka Academy (DVD) vs. Alpha Omega Academy</a></li>
<li><a href="/abeka-dvd-vs-sos">A Beka Academy (DVD) vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">A Beka Academy (Parent-Directed)</p>
<ul>
<li><a href="/abeka-parent-directed-vs-monarch">A Beka Academy (Parent-Directed) vs. Monarch</a></li>
<li><a href="abeka-parent-directed-vs-horizons">A Beka Academy (Parent-Directed) vs. Horizons</a></li>
<li><a href="/abeka-parent-directed-vs-lifepac">A Beka Academy (Parent-Directed) vs. LIFEPAC</a></li>
<li><a href="/abeka-parent-directed-vs-aoa">A Beka Academy (Parent-Directed) vs. Alpha Omega Academy</a></li>
<li><a href="/abeka-parent-directed-vs-sos">A Beka Academy (Parent-Directed) vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">A Beka Video-based Internet Streaming</p>
<ul>
<li><a href="/abeka-academy-streaming-video-vs-monarch">A Beka Video-based Internet Streaming vs. Monarch</a></li>
<li><a href="/abeka-academy-streaming-video-vs-horizons">A Beka Video-based Internet Streaming vs. Horizons</a></li>
<li><a href="/abeka-academy-streaming-video-vs-lifepac">A Beka Video-based Internet Streaming vs. LIFEPAC</a></li>
<li><a href="/abeka-academy-streaming-video-vs-aoa">A Beka Video-based Internet Streaming vs. Alpha Omega Academy</a></li>
<li><a href="/abeka-academy-streaming-video-vs-sos">A Beka Video-based Internet Streaming vs. Switched-On Schoolhouse</a></li>
</ul>
<p>*A Beka Book® and A Beka Academy® are the registered trademarks and subsidiaries of Pensacola Christian College. Alpha Omega Publications is not in any way affiliated with A Beka®. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by A Beka Book or A Beka Academy.</p><p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p><p>**Prices obtained February 2013 from A Beka 2013 Homeschool Catalog and from Alpha Omega Publications 2011 Homeschool Catalog. Yearly prices based on comparison between Monarch 3rd Grade 5-Subject Set and A Beka 3rd Grade child and parent kits for 3rd grade (totaled). Subject prices based on comparison between Monarch 3rd Grade Math (including all student and teacher material, along with supplements) and A Beka Book 3rd Grade Math (including all student and teacher material, along with supplements). Costs do not reflect any additional fees or shipping and handling charges. Prices for other A Beka and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '9eba7f28-462d-4132-a619-858204508e55';
  $display->content['new-9eba7f28-462d-4132-a619-858204508e55'] = $pane;
  $display->panels['left'][0] = 'new-9eba7f28-462d-4132-a619-858204508e55';
  $pane = new stdClass();
  $pane->pid = 'new-2673056b-cd24-43a5-ac51-4231f73400b1';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '2673056b-cd24-43a5-ac51-4231f73400b1';
  $display->content['new-2673056b-cd24-43a5-ac51-4231f73400b1'] = $pane;
  $display->panels['right'][0] = 'new-2673056b-cd24-43a5-ac51-4231f73400b1';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-9eba7f28-462d-4132-a619-858204508e55';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
