<?php
/**
 * @file
 * variable.rate_widgets.inc
 */

$api = '2.0.0';

$data = $strongarm = new stdClass();
$strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
$strongarm->api_version = 1;
$strongarm->name = 'rate_widgets';
$strongarm->value = array(
  1 => (object) array(
    'name' => 'rate',
    'tag' => 'rate',
    'title' => 'Rate',
    'node_types' => array(),
    'comment_types' => array(
      0 => 'curriculum',
      1 => 'dvd',
      2 => 'resource',
    ),
    'options' => array(
      0 => array(
        0 => 1,
        1 => 'up',
      ),
      1 => array(
        0 => -1,
        1 => 'down',
      ),
    ),
    'template' => 'thumbs_up_down',
    'node_display' => '1',
    'teaser_display' => FALSE,
    'comment_display' => '1',
    'node_display_mode' => '1',
    'teaser_display_mode' => '1',
    'comment_display_mode' => '2',
    'roles' => array(
      2 => '2',
      3 => 0,
      1 => 0,
      6 => 0,
      8 => 0,
      4 => 0,
      7 => 0,
      5 => 0,
    ),
    'allow_voting_by_author' => 1,
    'noperm_behaviour' => '1',
    'displayed' => '2',
    'displayed_just_voted' => '2',
    'description' => '',
    'description_in_compact' => TRUE,
    'delete_vote_on_second_click' => '1',
    'use_source_translation' => TRUE,
    'value_type' => 'points',
    'theme' => 'rate_template_thumbs_up_down',
    'css' => 'sites/all/modules/rate/templates/thumbs-up-down/thumbs-up-down.css',
    'translate' => TRUE,
  ),
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'strongarm',
  1 => 'ctools',
  2 => 'system',
);
