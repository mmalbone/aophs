<?php
/**
 * @file
 * permission.edit_any_panel_pane_node_content.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'edit any panel_pane_node content',
  'roles' => array(),
);

$dependencies = array(
  'content_type.panel_pane_node' => 'content_type.panel_pane_node',
);

$optional = array();

$modules = array(
  0 => 'node',
);
