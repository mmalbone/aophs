<?php
/**
 * @file
 * content_type.carousel_item.inc
 */

$api = '2.0.0';

$data = (object) array(
  'base' => 'node_content',
  'description' => 'Populates the carousel with business-intelligent items. ',
  'has_title' => '1',
  'help' => '',
  'name' => 'Carousel Item',
  'title_label' => 'Title',
  'type' => 'carousel_item',
);

$dependencies = array();

$optional = array(
  'field.node.body.carousel_item' => 'field.node.body.carousel_item',
  'field.node.field_carousel_button_text.carousel_item' => 'field.node.field_carousel_button_text.carousel_item',
  'field.node.field_carousel_hover_text.carousel_item' => 'field.node.field_carousel_hover_text.carousel_item',
  'field.node.field_carousel_item_hover_logo.carousel_item' => 'field.node.field_carousel_item_hover_logo.carousel_item',
  'field.node.field_carousel_item_logo.carousel_item' => 'field.node.field_carousel_item_logo.carousel_item',
  'field.node.field_carousel_item_title.carousel_item' => 'field.node.field_carousel_item_title.carousel_item',
  'field.node.field_carousel_item_url.carousel_item' => 'field.node.field_carousel_item_url.carousel_item',
  'field.node.field_carousel_short_description.carousel_item' => 'field.node.field_carousel_short_description.carousel_item',
  'field.node.title_field.carousel_item' => 'field.node.title_field.carousel_item',
  'permission.create_carousel_item_content' => 'permission.create_carousel_item_content',
  'permission.delete_any_carousel_item_content' => 'permission.delete_any_carousel_item_content',
  'permission.delete_own_carousel_item_content' => 'permission.delete_own_carousel_item_content',
  'permission.edit_any_carousel_item_content' => 'permission.edit_any_carousel_item_content',
  'permission.edit_own_carousel_item_content' => 'permission.edit_own_carousel_item_content',
);

$modules = array(
  0 => 'node',
);
