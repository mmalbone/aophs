<?php
/**
 * @file
 * vocabulary.content_placement.inc
 */

$api = '2.0.0';

$data = (object) array(
  'description' => 'This vocabulary is used to place content, e.g. promo touts, on specific pages.',
  'hierarchy' => '0',
  'machine_name' => 'content_placement',
  'module' => 'taxonomy',
  'name' => 'Content Placement',
  'vid' => '12',
  'weight' => '0',
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'taxonomy',
);
