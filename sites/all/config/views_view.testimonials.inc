<?php
/**
 * @file
 * views_view.testimonials.inc
 */

$api = '2.0.0';

$data = $view = new view();
$view->name = 'testimonials';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'testimonials';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = '<span>Testimonials</span>';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'time';
$handler->display->display_options['cache']['results_lifespan'] = '3600';
$handler->display->display_options['cache']['results_lifespan_custom'] = '0';
$handler->display->display_options['cache']['output_lifespan'] = '3600';
$handler->display->display_options['cache']['output_lifespan_custom'] = '0';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '1';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* Field: Content: Story */
$handler->display->display_options['fields']['field_story']['id'] = 'field_story';
$handler->display->display_options['fields']['field_story']['table'] = 'field_data_field_story';
$handler->display->display_options['fields']['field_story']['field'] = 'field_story';
$handler->display->display_options['fields']['field_story']['label'] = '';
$handler->display->display_options['fields']['field_story']['element_type'] = '0';
$handler->display->display_options['fields']['field_story']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_story']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_story']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_story']['field_api_classes'] = TRUE;
/* Field: Content: Testimonial By */
$handler->display->display_options['fields']['field_testimonial_by']['id'] = 'field_testimonial_by';
$handler->display->display_options['fields']['field_testimonial_by']['table'] = 'field_data_field_testimonial_by';
$handler->display->display_options['fields']['field_testimonial_by']['field'] = 'field_testimonial_by';
$handler->display->display_options['fields']['field_testimonial_by']['label'] = '';
$handler->display->display_options['fields']['field_testimonial_by']['element_type'] = '0';
$handler->display->display_options['fields']['field_testimonial_by']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_testimonial_by']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_testimonial_by']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_testimonial_by']['field_api_classes'] = TRUE;
/* Field: Content: State */
$handler->display->display_options['fields']['field_state']['id'] = 'field_state';
$handler->display->display_options['fields']['field_state']['table'] = 'field_data_field_state';
$handler->display->display_options['fields']['field_state']['field'] = 'field_state';
$handler->display->display_options['fields']['field_state']['label'] = '';
$handler->display->display_options['fields']['field_state']['element_type'] = '0';
$handler->display->display_options['fields']['field_state']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_state']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_state']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_state']['field_api_classes'] = TRUE;
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Sort criterion: Global: Random */
$handler->display->display_options['sorts']['random']['id'] = 'random';
$handler->display->display_options['sorts']['random']['table'] = 'views';
$handler->display->display_options['sorts']['random']['field'] = 'random';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'testimonial' => 'testimonial',
);
/* Filter criterion: Content: Has taxonomy term */
$handler->display->display_options['filters']['tid']['id'] = 'tid';
$handler->display->display_options['filters']['tid']['table'] = 'taxonomy_index';
$handler->display->display_options['filters']['tid']['field'] = 'tid';
$handler->display->display_options['filters']['tid']['value'] = array(
  99 => '99',
);
$handler->display->display_options['filters']['tid']['type'] = 'select';
$handler->display->display_options['filters']['tid']['vocabulary'] = 'testimonial_category';
$handler->display->display_options['filters']['tid']['hierarchy'] = 1;

/* Display: Competitor Comparison Testimonials */
$handler = $view->new_display('panel_pane', 'Competitor Comparison Testimonials', 'panel_pane_1');
$handler->display->display_options['display_description'] = 'Support Testimonials For Competitor Comparison pages';
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '1';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: Global: Random */
$handler->display->display_options['sorts']['random']['id'] = 'random';
$handler->display->display_options['sorts']['random']['table'] = 'views';
$handler->display->display_options['sorts']['random']['field'] = 'random';

/* Display: Support Pages Testimonials */
$handler = $view->new_display('panel_pane', 'Support Pages Testimonials', 'panel_pane_2');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = '<span>Testimonials</span>';
$handler->display->display_options['defaults']['css_class'] = FALSE;
$handler->display->display_options['css_class'] = 'testimonial-sidebar-header';
$handler->display->display_options['display_description'] = 'Support Testimonials for Support Pages';
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '1';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: Global: Random */
$handler->display->display_options['sorts']['random']['id'] = 'random';
$handler->display->display_options['sorts']['random']['table'] = 'views';
$handler->display->display_options['sorts']['random']['field'] = 'random';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'testimonial' => 'testimonial',
);

/* Display: Block Monarch Subscriptions Tesimonials */
$handler = $view->new_display('block', 'Block Monarch Subscriptions Tesimonials', 'block_1');
$handler->display->display_options['display_description'] = 'Testimonials for Monarch subscriptions';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'testimonial' => 'testimonial',
);
/* Filter criterion: Content: Has taxonomy term */
$handler->display->display_options['filters']['tid']['id'] = 'tid';
$handler->display->display_options['filters']['tid']['table'] = 'taxonomy_index';
$handler->display->display_options['filters']['tid']['field'] = 'tid';
$handler->display->display_options['filters']['tid']['value'] = array(
  762 => '762',
  763 => '763',
);
$handler->display->display_options['filters']['tid']['type'] = 'select';
$handler->display->display_options['filters']['tid']['vocabulary'] = 'testimonial_category';
$handler->display->display_options['filters']['tid']['hierarchy'] = 1;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'views',
  1 => 'node',
  2 => 'text',
  3 => 'views_content',
);
