<?php
/**
 * @file
 * field.user.field_billing_state.user.inc
 */

$api = '2.0.0';

$data = array(
  'field_config' => array(
    'active' => '1',
    'cardinality' => '1',
    'columns' => array(
      'value' => array(
        'length' => 255,
        'not null' => FALSE,
        'type' => 'varchar',
      ),
    ),
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_billing_state',
    'field_permissions' => array(
      'type' => '0',
    ),
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => '0',
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'AB' => 'Alberta',
        'AK' => 'Alaska',
        'AL' => 'Alabama',
        'AR' => 'Arkansas',
        'AS' => 'American Samoa',
        'AZ' => 'Arizona',
        'BC' => 'British Columbia',
        'CA' => 'California',
        'CO' => 'Colorado',
        'CT' => 'Connecticut',
        'DC' => 'District Of Columbia',
        'DE' => 'Delaware',
        'FL' => 'Florida',
        'FM' => 'Federated States Of Micronesia',
        'GA' => 'Georgia',
        'GU' => 'Guam GU',
        'HI' => 'Hawaii',
        'IA' => 'Iowa',
        'ID' => 'Idaho',
        'IL' => 'Illinois',
        'IN' => 'Indiana',
        'KS' => 'Kansas',
        'KY' => 'Kentucky',
        'LA' => 'Louisiana',
        'MA' => 'Massachusetts',
        'MB' => 'Manitoba',
        'MD' => 'Maryland',
        'ME' => 'Maine',
        'MH' => 'Marshall Islands',
        'MI' => 'Michigan',
        'MN' => 'Minnesota',
        'MO' => 'Missouri',
        'MP' => 'Northern Mariana Islands',
        'MS' => 'Mississippi',
        'MT' => 'Montana',
        'NB' => 'New Brunswick',
        'NC' => 'North Carolina',
        'ND' => 'North Dakota',
        'NE' => 'Nebraska',
        'NH' => 'New Hampshire',
        'NJ' => 'New Jersey',
        'NL' => 'Newfoundland and Labrador',
        'NM' => 'New Mexico',
        'NS' => 'Nova Scotia',
        'NT' => 'Northwest Territories',
        'NU' => 'Nunavut',
        'NV' => 'Nevada',
        'NY' => 'New York',
        'OH' => 'Ohio',
        'OK' => 'Oklahoma',
        'ON' => 'Ontario',
        'OR' => 'Oregon',
        'PA' => 'Pennsylvania',
        'PE' => 'Prince Edward Island',
        'PR' => 'Puerto Rico',
        'PW' => 'Palau',
        'QC' => 'Quebec',
        'RI' => 'Rhode Island',
        'SC' => 'South Carolina',
        'SD' => 'South Dakota',
        'SK' => 'Saskatchewan',
        'TN' => 'Tennessee',
        'TX' => 'Texas',
        'UT' => 'Utah',
        'VA' => 'Virginia',
        'VI' => 'Virgin Islands',
        'VT' => 'Vermont',
        'WA' => 'Washington',
        'WI' => 'Wisconsin',
        'WV' => 'West Virginia',
        'WY' => 'Wyoming',
        'YT' => 'Yukon',
      ),
      'allowed_values_function' => '',
    ),
    'storage' => array(
      'active' => '1',
      'details' => array(
        'sql' => array(
          'FIELD_LOAD_CURRENT' => array(
            'field_data_field_billing_state' => array(
              'value' => 'field_billing_state_value',
            ),
          ),
          'FIELD_LOAD_REVISION' => array(
            'field_revision_field_billing_state' => array(
              'value' => 'field_billing_state_value',
            ),
          ),
        ),
      ),
      'module' => 'field_sql_storage',
      'settings' => array(),
      'type' => 'field_sql_storage',
    ),
    'translatable' => '0',
    'type' => 'list_text',
  ),
  'field_instance' => array(
    'bundle' => 'user',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => '4',
      ),
    ),
    'entity_type' => 'user',
    'fences_wrapper' => '',
    'field_name' => 'field_billing_state',
    'label' => 'State/Prov',
    'required' => 0,
    'settings' => array(
      'user_register_form' => 0,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'apply_chosen' => '',
      ),
      'type' => 'options_select',
      'weight' => '6',
    ),
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'field_sql_storage',
  1 => 'list',
  2 => 'options',
);
