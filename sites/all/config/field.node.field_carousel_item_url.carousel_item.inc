<?php
/**
 * @file
 * field.node.field_carousel_item_url.carousel_item.inc
 */

$api = '2.0.0';

$data = array(
  'field_config' => array(
    'active' => '1',
    'cardinality' => '1',
    'columns' => array(
      'format' => array(
        'length' => 255,
        'not null' => FALSE,
        'type' => 'varchar',
      ),
      'value' => array(
        'length' => '300',
        'not null' => FALSE,
        'type' => 'varchar',
      ),
    ),
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_carousel_item_url',
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => '0',
    'module' => 'text',
    'settings' => array(
      'max_length' => '300',
    ),
    'storage' => array(
      'active' => '1',
      'details' => array(
        'sql' => array(
          'FIELD_LOAD_CURRENT' => array(
            'field_data_field_carousel_item_url' => array(
              'format' => 'field_carousel_item_url_format',
              'value' => 'field_carousel_item_url_value',
            ),
          ),
          'FIELD_LOAD_REVISION' => array(
            'field_revision_field_carousel_item_url' => array(
              'format' => 'field_carousel_item_url_format',
              'value' => 'field_carousel_item_url_value',
            ),
          ),
        ),
      ),
      'module' => 'field_sql_storage',
      'settings' => array(),
      'type' => 'field_sql_storage',
    ),
    'translatable' => '0',
    'type' => 'text',
  ),
  'field_instance' => array(
    'bundle' => 'carousel_item',
    'default_value' => array(
      0 => array(
        'value' => 'www.aophomeschooling.com',
      ),
    ),
    'deleted' => '0',
    'description' => 'URL to the item-specific page. ex "aophomeschooling.com/monarch_overview"',
    'display' => array(
      'aop_subscription_list' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 3,
      ),
      'product_carousel' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'product_list' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => '',
    'field_name' => 'field_carousel_item_url',
    'label' => 'carousel item url',
    'required' => 1,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'text_processing' => '0',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => '300',
      ),
      'type' => 'text_textfield',
      'weight' => '33',
    ),
  ),
);

$dependencies = array(
  'content_type.carousel_item' => 'content_type.carousel_item',
);

$optional = array();

$modules = array(
  0 => 'field_sql_storage',
  1 => 'text',
);
