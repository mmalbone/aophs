<?php
/**
 * @file
 * page_manager_handlers.page_webinars_switched_on_schoolhouse_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_webinars_switched_on_schoolhouse_panel_context';
$handler->task = 'page';
$handler->subtask = 'webinars_switched_on_schoolhouse';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'flexible';
$display->layout_settings = array(
  'items' => array(
    'canvas' => array(
      'type' => 'row',
      'contains' => 'column',
      'children' => array(
        0 => 'main',
      ),
      'parent' => NULL,
    ),
    'main' => array(
      'type' => 'column',
      'width' => 100,
      'width_type' => '%',
      'children' => array(
        0 => 'main-row',
      ),
      'parent' => 'canvas',
    ),
    'main-row' => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'center',
        1 => 'right',
      ),
      'parent' => 'main',
    ),
    'center' => array(
      'type' => 'region',
      'title' => 'Center',
      'width' => '70.04165501943186',
      'width_type' => '%',
      'parent' => 'main-row',
    ),
    'right' => array(
      'type' => 'region',
      'title' => 'Right',
      'width' => '29.958344980568143',
      'width_type' => '%',
      'parent' => 'main-row',
      'class' => '',
    ),
  ),
);
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'center' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '5af9bbf2-bac6-4845-9bed-fa5652c5b905';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-f923a48c-e6b0-4c5c-82f3-d87b6692115f';
  $pane->panel = 'center';
  $pane->type = 'block';
  $pane->subtype = 'orangebox-list_sos_series';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'f923a48c-e6b0-4c5c-82f3-d87b6692115f';
  $display->content['new-f923a48c-e6b0-4c5c-82f3-d87b6692115f'] = $pane;
  $display->panels['center'][0] = 'new-f923a48c-e6b0-4c5c-82f3-d87b6692115f';
  $pane = new stdClass();
  $pane->pid = 'new-d3ad8283-16d0-4025-a518-b82397550a00';
  $pane->panel = 'right';
  $pane->type = 'block';
  $pane->subtype = 'orangebox-list_owls';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'd3ad8283-16d0-4025-a518-b82397550a00';
  $display->content['new-d3ad8283-16d0-4025-a518-b82397550a00'] = $pane;
  $display->panels['right'][0] = 'new-d3ad8283-16d0-4025-a518-b82397550a00';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-d3ad8283-16d0-4025-a518-b82397550a00';
$handler->conf['display'] = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
