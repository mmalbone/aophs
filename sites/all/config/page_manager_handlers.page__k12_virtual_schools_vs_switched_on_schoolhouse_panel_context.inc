<?php
/**
 * @file
 * page_manager_handlers.page__k12_virtual_schools_vs_switched_on_schoolhouse_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page__k12_virtual_schools_vs_switched_on_schoolhouse_panel_context';
$handler->task = 'page';
$handler->subtask = '_k12_virtual_schools_vs_switched_on_schoolhouse';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '4408d87c-d4de-49ec-858a-43a7b27ee546';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-93038304-3d4d-4889-9196-ef20f7f5411a';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'K12 Virtual Schools vs. Switched-On Schoolhouse',
    'body' => '<p>Do you need to compare the online virtual school programs offered by K12 to the Switched-On Schoolhouse program from Alpha Omega Publications? You\'re in the right spot. Find a short overview of each curriculum and service below, an easy-to-view comparison chart, and additional comparison options!</p><h3><strong>K12 Virtual Schools</strong></h3><p>K12 is a large secular provider of kindergarten through 12th grade online curriculum currently being used in virtual public schools, an international online academy, as well as in homeschools worldwide. Because K12 is secular curriculum provider, parents from various faiths and cultural backgrounds may find K12 very appropriate for their families\' homeschool needs. Through its Virtual School programs, K12 offers kindergarten through 12th grade curriculum to families who desire to teach their children at home but prefer a public school program. The K12 Virtual school program is a partnership with local public schools systems providing a high quality online education to children in their homes.</p><p>The K12 curriculum covers six core subjects: language arts/English, math, science, history, art, and music and is based on time-tested and research-based methods of instruction. Students enrolled in a virtual school take their academic courses online with support from a certified public school teacher. The parent acts as a learning coach helping to keep the student successfully engaged in the learning process. Because they are part of the public school system, K12 Virtual Schools are tuition free to all students. This also means that K12 Virtual School families are 100% accountable to the public school system in which they are enrolled. K12 Virtual Schools are not available in all areas.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>Trying to decide between a secular virtual school program and a Christian computer-based curriculum? Remember that there is no one perfect homeschool curriculum. A blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Instead of feeling torn between different curriculums, parents should be open to mixing and matching materials and resources to customize their child\'s education. Each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3><strong>Switched-On Schoolhouse from Alpha Omega Publications</strong></h3><p>Switched-On Schoolhouse (SOS) from Alpha Omega Publications is an engaging, computer-based curriculum for grades 3-12. Alpha Omega Publications, a trusted publisher of high quality educational products, provides Christian curriculum and educational services to homeschool families. Revolutionizing the way students learn, SOS is a comprehensive, CD-ROM-based curriculum that is entirely technology based. This innovative, state-of-the-art curriculum presents biblically-based lessons using interactive multimedia, eye-catching animation, learning games, video clips, and more! Unlike traditional textbooks, SOS has a diverse mix of text-based instruction and engaging multimedia enrichment. Unsurpassed administrative tools include time-saving features like automatic grading and lesson planning with built-in calendar, customizable lessons, flexible printing options, and message center.</p><p>Winner of Cathy Duffy\'s 100 Top Picks for Homeschool Curriculum, SOS integrates Scripture and a Christian worldview into all subjects. SOS offers the flexibility of an individualized, student-driven approach, helpful text-to-speech option, and diagnostic tests for correct placement. Switched-On Schoolhouse courses are available for 3rd – 12th grade students in Bible, language arts, math, science, and history and geography. Electives are also available for students of all ages. SOS may be purchased as individual subjects or as complete five-subject sets. The program includes all required student and parent materials.</p><h3><strong>Benefits and Features of Switched-On Schoolhouse from Alpha Omega Publications</strong></h3><p class="list_heading">Switched-On Schoolhouse</p><ul><li>interactive, innovative, computer-based curriculum</li><li>offers flexible, engaging learning environment</li><li>contains multimedia tools, educational games, and automatic grading options</li><li>can reduce teacher planning and grading by 20 hours per week</li></ul><h3 class="para-start"><strong>Take a Closer Look at K12 Virtual Schools and Switched-on Schoolhouse</strong></h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">Switched-On Schoolhouse</th><th class="compare">K12 Virtual Schools</th></tr>
</thead>
<tbody>
<tr>
<td>3-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Student-directed curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Computer-based curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"> (partial)</td>
</tr>
<tr class="alt">
<td>Interactive, engaging multimedia tools</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Customizable lessons and calendar</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Placement tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Automatic grading and lesson planning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">Of some material</td>
</tr>
<tr class="alt">
<td>Automatic updated material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Flexible and individualized</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Mastery-based learning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Message and resource center</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr><th class="features">Cost</th><th class="compare">Alpha Omega Publications</th><th class="compare">K12</th></tr>
</thead>
<tbody>
<tr>
<td>Average yearly cost per grade level**</td>
<td class="tac">$449.95</td>
<td class="tac" rowspan="2">Tuition-free</td>
</tr>
<tr class="alt">
<td>Average subject cost per grade level**</td>
<td class="tac">$99.95</td>
</tr>
</tbody>
</table>
</div>
<h4><strong>Compare Offerings: K12 versus Alpha Omega Publications</strong></h4>
<p class="list_heading_links">K12 Independent Consumer Direct</p>
<ul>
<li><a href="/k12-independent-vs-monarch">K12 Independent Consumer Direct vs. Monarch</a></li>
<li><a href="/k12-independent-vs-aoa">K12 Independent Consumer Direct vs. Alpha Omega Academy</a></li>
<li><a href="/k12-independent-vs-horizons">K12 Independent Consumer Direct vs. Horizons</a></li>
<li><a href="/k12-independent-vs-lifepac">K12 Independent Consumer Direct vs. LIFEPAC</a></li>
<li><a href="/k12-independent-vs-sos">K12 Independent Consumer Direct vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">K12 Teacher-Supported Consumer Direct</p>
<ul>
<li><a href="/k12-teacher-supported-vs-monarch">K12 Teacher-Supported Consumer Direct vs. Monarch</a></li>
<li><a href="/k12-teacher-supported-vs-aoa">K12 Teacher-Supported Consumer Direct vs. Alpha Omega Academy</a></li>
<li><a href="/k12-teacher-supported-vs-horizons">K12 Teacher-Supported Consumer Direct vs. Horizons</a></li>
<li><a href="/k12-teacher-supported-vs-lifepac">K12 Teacher-Supported Consumer Direct vs. LIFEPAC</a></li>
<li><a href="/k12-teacher-supported-vs-sos">K12 Teacher-Supported Consumer Direct vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">K12 Virtual Schools</p>
<ul>
<li><a href="/k12-virtual-schools-vs-monarch">K12 Virtual Schools vs. Monarch</a></li>
<li><a href="/k12-virtual-schools-vs-aoa">K12 Virtual Schools vs. Alpha Omega Academy</a></li>
<li><a href="/k12-virtual-schools-vs-horizons">K12 Virtual Schools vs. Horizons</a></li>
<li><a href="/k12-virtual-schools-vs-lifepac">K12 Virtual Schools vs. LIFEPAC</a></li>
<li><a href="/k12-virtual-schools-vs-sos">K12 Virtual Schools vs. Switched-On Schoolhouse</a></li>
</ul>
<p>* K12 is the registered trademark of K12 Inc. Alpha Omega Publications is not in any way affiliated with K12. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by K12.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p><p>**Prices obtained February 2013 from the K12 website and from Alpha Omega Publications 2013 Homeschool Catalog. Yearly prices based on comparison between Switched-On Schoolhouse 5th Grade 5-subject boxed set and full time enrollment in a K12 Virtual School. Individual subject prices are based on comparison between costs for Switched-On Schoolhouse 5th Grade Math boxed set and enrollment in a K12 Virtual School. Costs do not reflect any additional fees or shipping and handling charges. Prices for other K12 and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '93038304-3d4d-4889-9196-ef20f7f5411a';
  $display->content['new-93038304-3d4d-4889-9196-ef20f7f5411a'] = $pane;
  $display->panels['left'][0] = 'new-93038304-3d4d-4889-9196-ef20f7f5411a';
  $pane = new stdClass();
  $pane->pid = 'new-5d0ea163-c28e-4c35-acec-052b634ff913';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '5d0ea163-c28e-4c35-acec-052b634ff913';
  $display->content['new-5d0ea163-c28e-4c35-acec-052b634ff913'] = $pane;
  $display->panels['right'][0] = 'new-5d0ea163-c28e-4c35-acec-052b634ff913';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-93038304-3d4d-4889-9196-ef20f7f5411a';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
