<?php
/**
 * @file
 * page_manager_handlers.page_how_to_get_started_homeschooling_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_how_to_get_started_homeschooling_panel_context';
$handler->task = 'page';
$handler->subtask = 'how_to_get_started_homeschooling';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'e86eb68c-7ebb-4933-b05f-b535372da23d';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-1869b08b-1c9d-440f-ba1b-dba6d5fed10e';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'How to Get Started Homeschooling',
    'title' => 'How to Get Started Homeschooling',
    'body' => '<h3>Take the first steps toward homeschooling with this top ten list of things to do for first-time homeschoolers.</h3><ol class="circles-list"><li><strong>PRAY</strong>&nbsp;and ask for God\'s leading. Homeschooling is a full-time commitment.</li><li><strong>DISCUSS</strong>&nbsp;and be in agreement with your spouse or supportive family member.</li><li><strong>CHECK</strong>&nbsp;out your state\'s homeschooling laws, and if required, file appropriate paperwork prior to the beginning of your school year. (Filing paperwork may take up to 30 – 60 days in most states).</li><li><strong>FIND</strong>&nbsp;appropriate curriculum for your child\'s grade level. Get informed. Visit homeschooling conventions and homeschool websites to familiarize yourself with products available. With print-based, computer-based, and online formats, Alpha Omega Publications offers Christian homeschool curriculum for grades PreK-12, including Monarch, Switched-On Schoolhouse, LIFEPAC, Horizons, and The Weaver Curriculum.</li><li><strong>TEST</strong>&nbsp;your child\'s skills in the main subject areas such as math and reading if you are starting a child who has already been attending school. AOP offers&nbsp;free placement tests&nbsp;for Monarch, LIFEPAC, and Horizons.Switched-On Schoolhouse placement tests&nbsp;are also available for purchase.</li><li><strong>DETERMINE</strong>&nbsp;your school year schedule and set up an area in your home to teach your children. Realize that tables and chairs should be at appropriate heights for writing or doing calculations. Consult your children and name your school together.</li><li><strong>CONTACT</strong>&nbsp;homeschool support groups in your area or start your own. You will need help and assistance as you begin and the expertise of others will encourage you. For your convenience, AOP offers a&nbsp;state-by-state listing of homeschool groups.</li><li><strong>PURCHASE</strong>&nbsp;art and other school supplies appropriate to your child\'s grade.</li><li><strong>RESEARCH</strong>&nbsp;opportunities for extracurricular activities like sports and music.</li><li><strong>SCHEDULE</strong>&nbsp;your personal activities and appointments around your school day, so they do not interrupt teaching time. Realize this is a life changing endeavor. From now on, you will be doing more things together as a family along with the cooking, cleaning, and playing.</li></ol>',
    'format' => 'markdown',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '1869b08b-1c9d-440f-ba1b-dba6d5fed10e';
  $display->content['new-1869b08b-1c9d-440f-ba1b-dba6d5fed10e'] = $pane;
  $display->panels['left'][0] = 'new-1869b08b-1c9d-440f-ba1b-dba6d5fed10e';
  $pane = new stdClass();
  $pane->pid = 'new-6a8a267a-b219-4be1-b202-e38cb9856221';
  $pane->panel = 'right';
  $pane->type = 'block';
  $pane->subtype = 'block-34';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '6a8a267a-b219-4be1-b202-e38cb9856221';
  $display->content['new-6a8a267a-b219-4be1-b202-e38cb9856221'] = $pane;
  $display->panels['right'][0] = 'new-6a8a267a-b219-4be1-b202-e38cb9856221';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-1869b08b-1c9d-440f-ba1b-dba6d5fed10e';
$handler->conf['display'] = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
