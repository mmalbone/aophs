<?php
/**
 * @file
 * page_manager_handlers.page__rod_and_staff_vs_switched_on_schoolhouse_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page__rod_and_staff_vs_switched_on_schoolhouse_panel_context';
$handler->task = 'page';
$handler->subtask = '_rod_and_staff_vs_switched_on_schoolhouse';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '735dffab-cfed-49dc-b690-e0ccf8d2b02b';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-7ff28a06-66a0-4ad9-8d90-8438db8a490b';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Rod and Staff vs. Switched-On Schoolhouse',
    'body' => '<p>Are you ready to compare the curriculum option offered by Rod and Staff Curriculum to the Switched-On Schoolhouse program from Alpha Omega Publications? You\'re in the right spot. Find a short overview of each curriculum and service below, an easy-to-view comparison chart, and additional comparison options!</p><h3><strong>Rod and Staff Curriculum</strong></h3><p>Rod and Staff Publishers, Inc. is a Mennonite publishing company which supplies Christian homes and schools with Bible-based textbooks and literature useful for academic instruction and guidance in Christian discipline. Rod and Staff publishes curriculum designed for a traditional method of instruction that encourages strong thinking and communication skills. Textbooks, workbooks, worksheets, tests, and teacher materials are available for preschool through 10th grade. Coursework includes Bible, reading, English, arithmetic, spelling, science, health, history/geography, penmanship, music, art, typing, and special education.</p><p>Designed to instill biblical values and standards, all Rod and Staff curriculum is steeped in biblical truth. Rod and Staff does not sell kindergarten curriculum. As an alternative to kindergarten, Rod and Staff provides a series of preschool resources designed to prepare young students for school both in attitude and action. Parents who choose Rod and Staff are responsible for all administrative tasks, including all required lesson planning, grading, record-keeping, and reporting. Because the Rod and Staff teacher\'s manuals are designed for the beginning teacher (or parent), they are easily used by anyone who desires to homeschool their children. Telephone consultations are available for Rod and Staff customers. Curriculum can be purchased in grade level kits or as individual components.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>As you consider your homeschool curriculum options, remember there is no one perfect homeschool curriculum. A blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Instead of feeling torn between different curricula, parents should be open to mixing and matching materials and resources to customize their child\'s education. Remember, each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3><strong>Switched-On Schoolhouse from Alpha Omega Publications</strong></h3><p>Switched-On Schoolhouse (SOS) from Alpha Omega Publications is an engaging, computer-based curriculum for grades 3-12. Alpha Omega Publications, a trusted publisher of high quality educational products, provides Christian curriculum and educational services to homeschool families. Revolutionizing the way students learn, SOS is a comprehensive, CD-ROM-based curriculum that is entirely technology based. This innovative, state-of-the-art curriculum presents biblically-based lessons using interactive multimedia, eye-catching animation, learning games, video clips, and more! Unlike traditional textbooks, SOS has a diverse mix of text-based instruction and engaging multimedia enrichment. Unsurpassed administrative tools include time-saving features like automatic grading and lesson planning with built-in calendar, customizable lessons, flexible printing options, and message center.</p><p>Winner of Cathy Duffy\'s 100 Top Picks for Homeschool Curriculum, SOS integrates Scripture and a Christian worldview into all subjects. SOS offers the flexibility of an individualized, student-driven approach, helpful text-to-speech option, and diagnostic tests for correct placement. Switched-On Schoolhouse courses are available for 3rd – 12th grade students in Bible, language arts, math, science, and history and geography. Electives are also available for students of all ages. SOS may be purchased as individual subjects or as complete five-subject sets. The program includes all required student and parent materials.</p><h3><strong>Benefits and Features of Switched-On Schoolhouse from Alpha Omega Publications</strong>&nbsp;</h3><p class="list_heading">Switched-On Schoolhouse</p><ul><li>interactive, innovative, computer-based curriculum</li><li>offers flexible, engaging learning environment</li><li>contains multimedia tools, educational games, and automatic grading options</li><li>can reduce teacher planning and grading by 20 hours per week</li></ul>
<h3 class="para-start"><strong>Take a Closer Look at Rod and Staff and Switched-on Schoolhouse</strong></h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">Switched-On Schoolhouse</th><th class="compare">Rod and Staff</th></tr>
</thead>
<tbody>
<tr>
<td>K-12 curriculum options</td>
<td class="tac">3-12th grade</td>
<td class="tac">Preschool-10th grade</td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Teacher-intensive curriculum</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Computer-based curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Interactive, engaging multimedia tools</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Customizable lessons and calendar</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Placement tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Automatic grading and lesson planning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Automatic updated material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Flexible and individualized</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Mastery-based learning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr><th class="features">Cost</th><th class="compare">Alpha Omega Publications</th><th class="compare">Rod and Staff</th></tr>
</thead>
<tbody>
<tr>
<td>Average yearly cost per grade level**</td>
<td class="tac">$449.95</td>
<td class="tac">$228.40</td>
</tr>
<tr class="alt">
<td>Average subject cost per grade level**</td>
<td class="tac">$99.95</td>
<td class="tac">$45.60</td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: Rod and Staff Curriculum versus Alpha Omega Publications</h4>
<ul class="comp_list">
<li><a href="/rod-and-staff-vs-monarch">Rod and Staff Curriculum vs. Monarch</a></li>
<li><a href="/rod-and-staff-vs-aoa">Rod and Staff Curriculum vs. Alpha Omega Academy</a></li>
<li><a href="/rod-and-staff-vs-horizons">Rod and Staff Curriculum vs. Horizons</a></li>
<li><a href="/rod-and-staff-vs-lifepac">Rod and Staff Curriculum vs. LIFEPAC</a></li>
<li><a href="/rod-and-staff-vs-sos">Rod and Staff Curriculum vs. Switched-On Schoolhouse</a></li>
</ul>
<p>*Rod and Staff Publishers, Inc. is a Christian Publisher of textbooks and literature. Alpha Omega Publications is not in any way affiliated with Rod and Staff. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by Rod and Staff.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in March 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p><p>**Prices obtained March 2013 from the Milestone Ministries website, independent vendor of Rod &amp; Staff materials, and from Alpha Omega Publications 2013 Homeschool Catalog. Yearly prices based on a comparison between the Switched-On Schoolhouse 6th Grade Complete grade level set and the Rod &amp; Staff 6th Grade Basic Packaged Program. Individual subject prices are based on comparison between Switched-On Schoolhouse 6th grade math and Rod &amp; Staff 6th Grade Math Set. Costs do not reflect any additional fees or shipping and handling charges. Prices for other Rod &amp; Staff and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '7ff28a06-66a0-4ad9-8d90-8438db8a490b';
  $display->content['new-7ff28a06-66a0-4ad9-8d90-8438db8a490b'] = $pane;
  $display->panels['left'][0] = 'new-7ff28a06-66a0-4ad9-8d90-8438db8a490b';
  $pane = new stdClass();
  $pane->pid = 'new-65822a97-707d-470f-b0b8-6f6b8280d949';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '65822a97-707d-470f-b0b8-6f6b8280d949';
  $display->content['new-65822a97-707d-470f-b0b8-6f6b8280d949'] = $pane;
  $display->panels['right'][0] = 'new-65822a97-707d-470f-b0b8-6f6b8280d949';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-7ff28a06-66a0-4ad9-8d90-8438db8a490b';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
