<?php
/**
 * @file
 * permission.administer_xmlsitemap.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'administer xmlsitemap',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'xmlsitemap',
);
