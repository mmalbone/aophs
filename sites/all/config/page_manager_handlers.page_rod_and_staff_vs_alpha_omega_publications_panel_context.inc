<?php
/**
 * @file
 * page_manager_handlers.page_rod_and_staff_vs_alpha_omega_publications_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_rod_and_staff_vs_alpha_omega_publications_panel_context';
$handler->task = 'page';
$handler->subtask = 'rod_and_staff_vs_alpha_omega_publications';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'd171c55a-4c14-4e05-b216-0227fa1306d3';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-838a117c-9bcb-47fd-ac30-0f254060c165';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Rod and Staff vs. Alpha Omega Publications',
    'body' => '<p>Thinking about homeschooling with the Bible-based curriculum offered by Rod &amp; Staff Publishers? Wondering what the differences are between Rod &amp; Staff and Alpha Omega Publications? You\'re in the right place. Many parents just like you are also searching for a homeschool curriculum and teaching approach that best fits their child\'s academic needs. With so many homeschooling options, resources, and curricula to choose from, selecting homeschool materials for your child can often be a daunting task, but we\'re here to help!</p><h3><strong>Rod and Staff\'s Approach</strong></h3><p>Rod and Staff Publishers, Inc. is a Mennonite publishing company which supplies Christian homes and schools with Bible-based textbooks and literature useful for academic instruction and guidance in Christian discipline. Rod and Staff publishes curriculum designed for a traditional method of instruction that encourages strong thinking and communication skills. Textbooks, workbooks, worksheets, tests, and teacher materials are available for preschool through 10th grade in all core subject areas.</p><p>Designed to instill biblical values and standards, all Rod and Staff curriculum is steeped in biblical truth. Parents who choose Rod and Staff are responsible for all administrative tasks, including all required lesson-planning, grading, record-keeping, and reporting. Rod and Staff provides curriculum in grade level kits or as individual components.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>Most parents discover that their child\'s educational needs are best met by blending several different homeschool curriculums and teaching approaches. Because there is no one perfect homeschool curriculum, a blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Instead of feeling torn between curricula, parents should be open to mixing and matching materials and resources to customize their child\'s education. Remember, each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3><strong>Curriculum from Alpha Omega Publications</strong></h3><p>Alpha Omega Publications is a Christian-based, PreK-12 publisher that provides engaging, interactive curriculum and educational services to homeschool families. Offering proven, easy-to-teach homeschool curriculum, AOP offers four main curriculum offerings: Internet-based Monarch (3-12), computer-based Switched-On Schoolhouse (3-12), worktext-based LIFEPAC (K-12), and workbook-based Horizons (PreK-12). Main core subjects offered include Bible, language arts, math, science, and history and geography, along with various electives. AOP also offers Alpha Omega Academy, a distance learning academy for grades K-12. Understanding that each child learns differently, AOP offers diverse curriculum and services in different formats to fit multiple learning styles, ensuring you can find what fits your child\'s needs.</p><h3><strong>Benefits and Features of Alpha Omega Publications\' Curriculum</strong>&nbsp;</h3><p class="list_heading">Monarch</p><ul><li>online interactive lessons with anytime, anywhere access</li><li>compatible with both Windows® and Mac® operating systems</li><li>offers customizable student learning options and time-saving teaching tools</li><li>features dynamic, media-rich lessons; real-time content updates; and data storage</li></ul><p class="list_heading">Switched-On Schoolhouse</p><ul><li>interactive, computer-based curriculum</li><li>offers a flexible, engaging learning environment</li><li>contains multimedia tools, educational games, and automatic grading options</li><li>can reduce teacher planning and grading by 20 hours per week</li></ul><p class="list_heading">LIFEPAC</p><ul><li>full-color, print-based curriculum</li><li>offers progressive, self-paced individual unit worktexts</li><li>promotes concept mastery and cognitive reasoning skills</li><li>encourages academic independence in your children</li></ul><p class="list_heading">Horizons</p><ul><li>brightly illustrated, hands-on, consumable curriculum</li><li>offers fun lessons and manipulative-based activities</li><li>teaches concepts through a spiral learning process</li><li>encourages concept mastery through quick concept introduction, review, and reinforcement</li></ul><h3 class="para-start"><strong>Take a Closer Look at Rod and Staff and Alpha Omega Publications</strong></h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">Alpha Omega Publications</th><th class="compare">Rod and Staff</th></tr>
</thead>
<tbody>
<tr>
<td>PreK-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">Preschool-10th grade</td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Flexible and individualized curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Customizable curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Teacher-intensive curriculum</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Placement tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Interactive lesson material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Automatic curriculum updates available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Automatic grading and lesson planning available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Appeals to students with multiple learning styles</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr><th class="features">Cost</th><th class="compare">Alpha Omega Publications</th><th class="compare">Rod and Staff</th></tr>
</thead>
<tbody>
<tr>
<td>Average yearly cost per grade level**</td>
<td class="tac">$449.95 for Monarch</td>
<td class="tac">$228.40</td>
</tr>
<tr class="alt">
<td>Average subject cost per grade level**</td>
<td class="tac">$99.95 for Monarch</td>
<td class="tac">$45.60</td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: Rod and Staff Curriculum versus Alpha Omega Publications</h4>
<ul class="comp_list">
<li><a href="/rod-and-staff-vs-monarch">Rod and Staff Curriculum vs. Monarch</a></li>
<li><a href="/rod-and-staff-vs-aoa">Rod and Staff Curriculum vs. Alpha Omega Academy</a></li>
<li><a href="/rod-and-staff-vs-horizons">Rod and Staff Curriculum vs. Horizons</a></li>
<li><a href="/rod-and-staff-vs-lifepac">Rod and Staff Curriculum vs. LIFEPAC</a></li>
<li><a href="/rod-and-staff-vs-sos">Rod and Staff Curriculum vs. Switched-On Schoolhouse</a></li>
</ul>
<p>*Rod and Staff Publishers, Inc. is a Christian Publisher of textbooks and literature. Alpha Omega Publications is not in any way affiliated with Rod and Staff. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by Rod and Staff.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in March 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p><p>**Prices obtained March 2013 from the Milestone Ministries website, independent vendor of Rod &amp; Staff materials, and from Alpha Omega Publications 2013 Homeschool Catalog. Yearly prices based on a comparison between the Monarch 6th Grade 5-Subject Set and the Rod &amp; Staff 6th Grade Basic Packaged Program. Individual subject prices are based on comparison between the Monarch 6th grade math and Rod &amp; Staff 6th Grade Math Set. Costs do not reflect any additional fees or shipping and handling charges. Prices for other Rod &amp; Staff and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '838a117c-9bcb-47fd-ac30-0f254060c165';
  $display->content['new-838a117c-9bcb-47fd-ac30-0f254060c165'] = $pane;
  $display->panels['left'][0] = 'new-838a117c-9bcb-47fd-ac30-0f254060c165';
  $pane = new stdClass();
  $pane->pid = 'new-9a8b67ff-458e-4028-86b3-4348503ce23e';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '9a8b67ff-458e-4028-86b3-4348503ce23e';
  $display->content['new-9a8b67ff-458e-4028-86b3-4348503ce23e'] = $pane;
  $display->panels['right'][0] = 'new-9a8b67ff-458e-4028-86b3-4348503ce23e';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-838a117c-9bcb-47fd-ac30-0f254060c165';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
