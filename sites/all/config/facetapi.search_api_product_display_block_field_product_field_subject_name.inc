<?php
/**
 * @file
 * facetapi.search_api_product_display_block_field_product_field_subject_name.inc
 */

$api = '2.0.0';

$data = $facet = new stdClass();
$facet->disabled = FALSE; /* Edit this to true to make a default facet disabled initially */
$facet->api_version = 1;
$facet->name = 'search_api@product_display:block:field_product:field_subject:name';
$facet->searcher = 'search_api@product_display';
$facet->realm = 'block';
$facet->facet = 'field_product:field_subject:name';
$facet->enabled = TRUE;
$facet->settings = array(
  'weight' => 0,
  'widget' => 'facetapi_checkbox_links',
  'filters' => array(),
  'active_sorts' => array(
    'active' => 'active',
    'count' => 'count',
    'display' => 'display',
    'indexed' => 0,
  ),
  'sort_weight' => array(
    'active' => '-50',
    'count' => '-49',
    'display' => '-48',
    'indexed' => '0',
  ),
  'sort_order' => array(
    'active' => '3',
    'count' => '3',
    'display' => '4',
    'indexed' => '4',
  ),
  'empty_behavior' => 'none',
  'soft_limit' => '0',
  'nofollow' => 1,
  'show_expanded' => 0,
  'name' => '',
  'prefix' => '',
  'suffix' => '',
  'auto-submit-delay' => '1500',
  'range_simple' => 10,
  'range_advanced' => '',
  'display_count' => 1,
  'empty_text' => array(
    'value' => '',
    'format' => 'filtered_html',
  ),
  'submit_realm' => 'Save and go back to realm settings',
  'facet_more_text' => 'Show more',
  'facet_fewer_text' => 'Show fewer',
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'facetapi',
);
