<?php
/**
 * @file
 * vocabulary.curriculum.inc
 */

$api = '2.0.0';

$data = (object) array(
  'description' => '',
  'hierarchy' => '0',
  'machine_name' => 'curriculum',
  'module' => 'taxonomy',
  'name' => 'Homeschool Curriculum',
  'vid' => '6',
  'weight' => '0',
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'taxonomy',
);
