<?php
/**
 * @file
 * permission.use_text_format_commerce_order_message.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'use text format commerce_order_message',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array(
  'text_format.commerce_order_message' => 'text_format.commerce_order_message',
);

$optional = array();

$modules = array(
  0 => 'filter',
);
