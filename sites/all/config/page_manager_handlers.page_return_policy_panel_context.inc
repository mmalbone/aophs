<?php
/**
 * @file
 * page_manager_handlers.page_return_policy_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_return_policy_panel_context';
$handler->task = 'page';
$handler->subtask = 'return_policy';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'ee8fae89-f2d8-4330-8591-74515e2aa592';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-e130a024-9532-4b08-a47a-df08045033aa';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Return Policy',
    'body' => '<p>Items must be returned in like-new condition (no marks, tears, erasures, missing pages on printed product). Software and media must be sealed in original packaging for refund. Only exchanges for same items will be done for defective software, DVDs, and CDs.</p>

<p>All returns require a return authorization number. To obtain a return authorization number, please contact customer service at 800-622-3070.</p>

<p>For 100% refund, items must be returned within 30 days of the invoice date.</p>

<p>After 30 days, a 20% restocking fee is deducted from your refund. An additional 5% fee is applied to orders when box sets are returned without all items. No refunds are given 60 days after the invoice date. Shipping charges are non-refundable.</p>

<p>An unauthorized refusal or return of an order is automatically assessed a $20.00 restocking fee.</p>

<p>Monarch can be returned within 30 days for a full refund. After 30 days, a 20% fee is applied. No refunds of Monarch are given after 60 days.</p>

<p>Please allow 4-6 weeks (6-8 weeks during peak times) for your return to be processed.</p>

<p>Return credits are refunded in the same form of payment used for the original purchase.</p>
',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'e130a024-9532-4b08-a47a-df08045033aa';
  $display->content['new-e130a024-9532-4b08-a47a-df08045033aa'] = $pane;
  $display->panels['middle'][0] = 'new-e130a024-9532-4b08-a47a-df08045033aa';
  $pane = new stdClass();
  $pane->pid = 'new-5dbd0297-e481-4692-9ded-3247836b215e';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Payment Methods',
    'body' => '<p>Full payment must be received with all orders. Orders can be paid by check, money order, or credit card (Visa, MasterCard, or Discover).&nbsp; A minimum $20.00 service fee is charged for all returned checks.</p>
',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '5dbd0297-e481-4692-9ded-3247836b215e';
  $display->content['new-5dbd0297-e481-4692-9ded-3247836b215e'] = $pane;
  $display->panels['middle'][1] = 'new-5dbd0297-e481-4692-9ded-3247836b215e';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-e130a024-9532-4b08-a47a-df08045033aa';
$handler->conf['display'] = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
