<?php
/**
 * @file
 * page_manager_handlers.page_rod_and_staff_vs_lifepac_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_rod_and_staff_vs_lifepac_panel_context';
$handler->task = 'page';
$handler->subtask = 'rod_and_staff_vs_lifepac';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'd0204a21-f51a-423d-9326-53e94551c5ac';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-d4dce7b8-6033-4b8c-a1fd-dfcbfb86ef34';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Rod and Staff vs. LIFEPAC',
    'body' => '<p>Are you looking for a comparison of the Bible-based homeschool programs offered by Rod and Staff Publishers and Alpha Omega Publications? You\'re in the right spot. Find a short overview of each curriculum and service below, an easy-to-view comparison chart, and additional comparison options!</p><h3><strong>Rod and Staff Curriculum</strong></h3><p>Rod and Staff Publishers, Inc. is a Mennonite publishing company which supplies Christian homes and schools with Bible-based textbooks and literature useful for academic instruction and guidance in Christian discipline. Rod and Staff publishes curriculum designed for a traditional method of instruction that encourages strong thinking and communication skills. Textbooks, workbooks, worksheets, tests, and teacher materials are available for preschool through 10th grade. Coursework includes Bible, reading, English, arithmetic, spelling, science, health, history/geography, penmanship, music, art, typing, and special education.</p><p>Designed to instill biblical values and standards, all Rod and Staff curriculum is steeped in biblical truth. Rod and Staff does not sell kindergarten curriculum. As an alternative to kindergarten, Rod and Staff provides a series of preschool resources designed to prepare young students for school both in attitude and action. Parents who choose Rod and Staff are responsible for all administrative tasks, including all required lesson planning, grading, record-keeping, and reporting. Because the Rod and Staff teacher\'s manuals are designed for the beginning teacher (or parent), they are easily used by anyone who desires to homeschool their children. Telephone consultations are available for Rod and Staff customers. Curriculum can be purchased in grade level kits or as individual components.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>No matter what type of homeschooling methods or curriculum you are considering, remember there is no one perfect homeschool curriculum. A blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Instead of feeling torn between different curriculums, parents should be open to mixing and matching materials and resources to customize their child\'s education. Remember, each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3><strong>LIFEPAC from Alpha Omega Publications</strong></h3><p>LIFEPAC is a K-12 print-based curriculum offered by Alpha Omega Publications, a Christian-based publisher that provides curriculum and educational services to Christian schools and homeschool families. Full-color and consumable, LIFEPAC is an integrated, Bible-based curriculum developed by a team of accomplished educators and teachers. Using a mastery-based learning approach, each LIFEPAC course is comprised of ten student-directed worktexts that provide a full year of instruction. Individual worktexts may also be used to supplement the homeschool curriculum or to fill in learning gaps. LIFEPAC worktexts include self tests and unit tests to provide assessment of student progress. Teacher\'s guides offer practical teaching tips, additional learning activities, and complete answer keys.</p><p>An economical, proven homeschool curriculum, LIFEPAC integrates Scripture and a Christian worldview throughout all subjects. LIFEPAC offers the flexibility of an individualized, student-driven learning approach which encourages critical thinking and academic independence. Self-paced coursework for K-12 students includes Bible (1-12), language arts (K-12), math (K-12), science (K-12), and history and geography (1-12). Electives are also available for elementary, middle, and high school students. LIFEPAC subject kits include all required student and teacher materials. LIFEPAC curriculum can be purchased as complete grade level sets or as individual subjects. Student and teacher materials and supplemental materials may be purchased separately.</p><h3><strong>Benefits and Features of LIFEPAC from Alpha Omega Publications</strong></h3><p class="list_heading">LIFEPAC</p><ul><li>full-color, print-based curriculum</li><li>offers progressive, self-paced individual unit worktexts</li><li>promotes concept mastery and cognitive reasoning skills</li><li>encourages academic independence in your children</li></ul><h3 class="para-start"><strong>Take a Closer Look at Rod and Staff and LIFEPAC</strong></h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">LIFEPAC</th><th class="compare">Rod and Staff</th></tr>
</thead>
<tbody>
<tr>
<td>K-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Accredited program</td>
<td class="tac">&nbsp;</td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Student-directed curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Self-paced and individualized</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Placement tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Economical</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Promotes strong thinking and communication skills</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Manageable amount of daily lesson material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Material designed for homeschoolers</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr><th class="features">Cost</th><th class="compare">Alpha Omega Publications</th><th class="compare">Rod and Staff</th></tr>
</thead>
<tbody>
<tr>
<td>Average yearly cost per grade level**</td>
<td class="tac">$286.95</td>
<td class="tac">$228.40</td>
</tr>
<tr class="alt">
<td>Average subject cost per grade level**</td>
<td class="tac">$58.95</td>
<td class="tac">$45.60</td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: Rod and Staff Curriculum versus Alpha Omega Publications</h4>
<ul class="comp_list">
<li><a href="/rod-and-staff-vs-monarch">Rod and Staff Curriculum vs. Monarch</a></li>
<li><a href="/rod-and-staff-vs-aoa">Rod and Staff Curriculum vs. Alpha Omega Academy</a></li>
<li><a href="/rod-and-staff-vs-horizons">Rod and Staff Curriculum vs. Horizons</a></li>
<li><a href="/rod-and-staff-vs-lifepac">Rod and Staff Curriculum vs. LIFEPAC</a></li>
<li><a href="/rod-and-staff-vs-sos">Rod and Staff Curriculum vs. Switched-On Schoolhouse</a></li>
</ul>
<p>*Rod and Staff Publishers, Inc. is a Christian Publisher of textbooks and literature. Alpha Omega Publications is not in any way affiliated with Rod and Staff. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by Rod and Staff.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in March 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p><p>**Prices obtained March 2013 from the Milestone Ministries website, independent vendor of Rod &amp; Staff materials, and from Alpha Omega Publications 2013 Homeschool Catalog. Yearly prices based on a comparison between the LIFEPAC 6th Grade Complete grade level set and the Rod &amp; Staff 6th Grade Basic Packaged Program. Individual subject prices are based on comparison between the LIFEPAC 6th Grade Mathematics Set and Rod &amp; Staff 6th Grade Math Set. Costs do not reflect any additional fees or shipping and handling charges. Prices for other Rod &amp; Staff and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'd4dce7b8-6033-4b8c-a1fd-dfcbfb86ef34';
  $display->content['new-d4dce7b8-6033-4b8c-a1fd-dfcbfb86ef34'] = $pane;
  $display->panels['left'][0] = 'new-d4dce7b8-6033-4b8c-a1fd-dfcbfb86ef34';
  $pane = new stdClass();
  $pane->pid = 'new-ce8b9e84-0172-4c98-ab4d-02fcdc2bbd39';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'ce8b9e84-0172-4c98-ab4d-02fcdc2bbd39';
  $display->content['new-ce8b9e84-0172-4c98-ab4d-02fcdc2bbd39'] = $pane;
  $display->panels['right'][0] = 'new-ce8b9e84-0172-4c98-ab4d-02fcdc2bbd39';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-d4dce7b8-6033-4b8c-a1fd-dfcbfb86ef34';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
