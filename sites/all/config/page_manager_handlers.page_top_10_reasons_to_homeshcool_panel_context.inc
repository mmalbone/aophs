<?php
/**
 * @file
 * page_manager_handlers.page_top_10_reasons_to_homeshcool_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_top_10_reasons_to_homeshcool_panel_context';
$handler->task = 'page';
$handler->subtask = 'top_10_reasons_to_homeshcool';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'flexible';
$display->layout_settings = array(
  'items' => array(
    'canvas' => array(
      'type' => 'row',
      'contains' => 'column',
      'children' => array(
        0 => 'main',
      ),
      'parent' => NULL,
    ),
    'main' => array(
      'type' => 'column',
      'width' => 100,
      'width_type' => '%',
      'children' => array(
        0 => 'main-row',
        1 => 1,
      ),
      'parent' => 'canvas',
    ),
    'main-row' => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'center',
      ),
      'parent' => 'main',
    ),
    'center' => array(
      'type' => 'region',
      'title' => 'Center',
      'width' => 100,
      'width_type' => '%',
      'parent' => 'main-row',
    ),
    1 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'left',
        1 => 'right',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    'left' => array(
      'type' => 'region',
      'title' => 'Left',
      'width' => '70.04168670835338',
      'width_type' => '%',
      'parent' => '1',
      'class' => '',
    ),
    'right' => array(
      'type' => 'region',
      'title' => 'Right',
      'width' => '29.95831329164662',
      'width_type' => '%',
      'parent' => '1',
      'class' => '',
    ),
  ),
);
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'center' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '18c4be78-26bd-4c38-a64e-a747ee4e5475';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-a9ae2285-81ab-45d4-8f1a-ba75fcabfbb2';
  $pane->panel = 'center';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Top 10 Reasons to Homeschool',
    'body' => '<h2>Things to Do When Considering Homeschooling for the First Time</h2>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'top-10-reasons-to-homeschool-banner',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'a9ae2285-81ab-45d4-8f1a-ba75fcabfbb2';
  $display->content['new-a9ae2285-81ab-45d4-8f1a-ba75fcabfbb2'] = $pane;
  $display->panels['center'][0] = 'new-a9ae2285-81ab-45d4-8f1a-ba75fcabfbb2';
  $pane = new stdClass();
  $pane->pid = 'new-8a414d23-68dd-4642-992b-f4c234e151e5';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => '',
    'body' => '<h2>Take the first steps toward homeschooling with this top ten list of things to do for first-time homeschoolers.</h2><ol class="circles-list"><li><strong>PRAY</strong>&nbsp;and ask for God\'s leading. Homeschooling is a full-time commitment.</li><li><strong>DISCUSS</strong>&nbsp;and be in agreement with your spouse or supportive family member.</li><li><strong>CHECK</strong>&nbsp;out your state\'s homeschooling laws, and if required, file appropriate paperwork prior to the beginning of your school year. (Filing paperwork may take up to 30 – 60 days in most states).</li><li><strong>FIND</strong>&nbsp;appropriate curriculum for your child\'s grade level. Get informed. Visit homeschooling conventions and homeschool websites to familiarize yourself with products available. With print-based, computer-based, and online formats, Alpha Omega Publications offers Christian homeschool curriculum for grades PreK-12, including Monarch, Switched-On Schoolhouse, LIFEPAC, Horizons, and The Weaver Curriculum.</li><li><strong>TEST</strong>&nbsp;your child\'s skills in the main subject areas such as math and reading if you are starting a child who has already been attending school. AOP offers&nbsp;free placement tests&nbsp;for Monarch, LIFEPAC, and Horizons.Switched-On Schoolhouse placement tests&nbsp;are also available for purchase.</li><li><strong>DETERMINE</strong>&nbsp;your school year schedule and set up an area in your home to teach your children. Realize that tables and chairs should be at appropriate heights for writing or doing calculations. Consult your children and name your school together.</li><li><strong>CONTACT</strong>&nbsp;homeschool support groups in your area or start your own. You will need help and assistance as you begin and the expertise of others will encourage you. For your convenience, AOP offers a&nbsp;state-by-state listing of homeschool groups.</li><li><strong>PURCHASE</strong>&nbsp;art and other school supplies appropriate to your child\'s grade.</li><li><strong>RESEARCH</strong>&nbsp;opportunities for extracurricular activities like sports and music.</li><li><strong>SCHEDULE</strong>&nbsp;your personal activities and appointments around your school day, so they do not interrupt teaching time. Realize this is a life changing endeavor. From now on, you will be doing more things together as a family along with the cooking, cleaning, and playing.</li></ol>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '8a414d23-68dd-4642-992b-f4c234e151e5';
  $display->content['new-8a414d23-68dd-4642-992b-f4c234e151e5'] = $pane;
  $display->panels['left'][0] = 'new-8a414d23-68dd-4642-992b-f4c234e151e5';
  $pane = new stdClass();
  $pane->pid = 'new-69ae8e4a-53bd-4831-8476-cdb17482f312';
  $pane->panel = 'right';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => '',
    'body' => '<div class="get-started-sidebar">
<p style="text-align: center;"><a href="/get-started/" class="tools"><em>Tools to get started</em><br><strong>HELPFUL RESOURCES</strong></a></p>
<p style="text-align: center;"><a href="/find-a-curriculum/" class="foryourfamily"><em>Find the right curriculum</em><br><strong>FOR YOUR FAMILY</strong></a></p>
<p style="text-align: center;"><a href="/get-started/" class="free-resources"><em>Free resources</em><br><strong>SUPPORT ALONG THE WAY</strong></a></p>
</div>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '69ae8e4a-53bd-4831-8476-cdb17482f312';
  $display->content['new-69ae8e4a-53bd-4831-8476-cdb17482f312'] = $pane;
  $display->panels['right'][0] = 'new-69ae8e4a-53bd-4831-8476-cdb17482f312';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-a9ae2285-81ab-45d4-8f1a-ba75fcabfbb2';
$handler->conf['display'] = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
