<?php
/**
 * @file
 * panels_layout.aop_banner_70_30.inc
 */

$api = '2.0.0';

$data = $layout = new stdClass();
$layout->disabled = FALSE; /* Edit this to true to make a default layout disabled initially */
$layout->api_version = 1;
$layout->name = 'aop_banner_70_30';
$layout->admin_title = 'AOP_Banner_70_30';
$layout->admin_description = '';
$layout->category = 'AOP';
$layout->plugin = 'flexible';
$layout->settings = array(
  'items' => array(
    'canvas' => array(
      'type' => 'row',
      'contains' => 'column',
      'children' => array(
        0 => 'main',
      ),
      'parent' => NULL,
      'class' => '',
      'column_class' => '',
      'row_class' => '',
      'region_class' => '',
      'no_scale' => TRUE,
      'fixed_width' => '',
      'column_separation' => '0.5em',
      'region_separation' => '0.5em',
      'row_separation' => '0.5em',
    ),
    'main' => array(
      'type' => 'column',
      'width' => 100,
      'width_type' => '%',
      'children' => array(
        0 => 'main-row',
        1 => 1,
      ),
      'parent' => 'canvas',
    ),
    'main-row' => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'center',
      ),
      'parent' => 'main',
    ),
    'center' => array(
      'type' => 'region',
      'title' => 'Full Width Landing Page Banner',
      'width' => 100,
      'width_type' => '%',
      'parent' => 'main-row',
      'class' => 'landing-page-banner',
    ),
    1 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => '70',
        1 => '30',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    70 => array(
      'type' => 'region',
      'title' => '70',
      'width' => '69.68134957825679',
      'width_type' => '%',
      'parent' => '1',
      'class' => '',
    ),
    30 => array(
      'type' => 'region',
      'title' => '30',
      'width' => '30.31865042174321',
      'width_type' => '%',
      'parent' => '1',
      'class' => '',
    ),
  ),
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'panels',
);
