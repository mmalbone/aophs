<?php
/**
 * @file
 * page_manager_handlers.page_academic_support_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_academic_support_panel_context';
$handler->task = 'page';
$handler->subtask = 'academic_support';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'a1b0382f-ba0e-419d-8f47-5b3d8852046a';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-5eeb0dc2-a555-4359-a4b5-5838c8762dc2';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Outstanding Technical Support',
    'body' => '<p>If your technology is on the fritz, give our tech support team a call. Our experts will guide you through the steps to get you back on track.
<a href="" title="Monarch Technical Support">Monarch Technical Support: </a>888-881-4958
<a href="" title="Switched-On Schoolhouse Technical Support">Switched-On Schoolhouse Technical Support: </a>866-444-4498',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '5eeb0dc2-a555-4359-a4b5-5838c8762dc2';
  $display->content['new-5eeb0dc2-a555-4359-a4b5-5838c8762dc2'] = $pane;
  $display->panels['middle'][0] = 'new-5eeb0dc2-a555-4359-a4b5-5838c8762dc2';
  $pane = new stdClass();
  $pane->pid = 'new-468f8ab3-17fa-4674-89d1-b20ed5f7b615';
  $pane->panel = 'middle';
  $pane->type = 'block';
  $pane->subtype = 'block-20';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '468f8ab3-17fa-4674-89d1-b20ed5f7b615';
  $display->content['new-468f8ab3-17fa-4674-89d1-b20ed5f7b615'] = $pane;
  $display->panels['middle'][1] = 'new-468f8ab3-17fa-4674-89d1-b20ed5f7b615';
  $pane = new stdClass();
  $pane->pid = 'new-2355f300-a356-4b7d-8a76-19af2a694c94';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'AOP Academic Support',
    'body' => '<p>Best of all, academic support from AOP is as flexible as the needs of your child. Available in three different packages, one-on-one sessions last up to 30 minutes and can be scheduled at any time on Monday through Friday from 8 a.m. to 4:30 p.m. (CT). Plus, purchased minutes are guaranteed because any unused minutes can be carried over to another session and they can be used by another student in your home.<br />Three academic support packages</p>
<div class="academic-support-packages-wrapper">
<div class="academic-support-package">
1 session (30 minutes): $30
</div>
<div class="academic-support-package">
3 sessions (90 minutes): $85
</div>
<div class="academic-support-package">
8 sessions (240 minutes): $216
</div>
</div>
<p>Eliminate the educational roadblocks hindering your child\'s learning with academic support sessions for your Christian homeschool curriculum. To schedule a session or learn more information, call 800-622-3070 today or verify your information below to have one of AOP\'s dedicated representatives call you!</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '2355f300-a356-4b7d-8a76-19af2a694c94';
  $display->content['new-2355f300-a356-4b7d-8a76-19af2a694c94'] = $pane;
  $display->panels['middle'][2] = 'new-2355f300-a356-4b7d-8a76-19af2a694c94';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-5eeb0dc2-a555-4359-a4b5-5838c8762dc2';
$handler->conf['display'] = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
