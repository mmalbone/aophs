<?php
/**
 * @file
 * content_type.promo_tout.inc
 */

$api = '2.0.0';

$data = (object) array(
  'base' => 'node_content',
  'description' => 'A content type used to display various calls to action or promotions on the site.',
  'has_title' => '1',
  'help' => '',
  'name' => 'Promo Tout',
  'title_label' => 'Promo Tout Title',
  'type' => 'promo_tout',
);

$dependencies = array();

$optional = array(
  'field.node.field_content_placement.promo_tout' => 'field.node.field_content_placement.promo_tout',
  'field.node.field_curriculum.promo_tout' => 'field.node.field_curriculum.promo_tout',
  'field.node.field_promo_display_title.promo_tout' => 'field.node.field_promo_display_title.promo_tout',
  'field.node.field_promo_tout_background_imag.promo_tout' => 'field.node.field_promo_tout_background_imag.promo_tout',
  'field.node.field_promo_tout_featured_image.promo_tout' => 'field.node.field_promo_tout_featured_image.promo_tout',
  'field.node.field_promo_tout_link.promo_tout' => 'field.node.field_promo_tout_link.promo_tout',
  'field.node.field_promo_tout_text.promo_tout' => 'field.node.field_promo_tout_text.promo_tout',
  'field.node.title_field.promo_tout' => 'field.node.title_field.promo_tout',
  'permission.create_promo_tout_content' => 'permission.create_promo_tout_content',
  'permission.delete_any_promo_tout_content' => 'permission.delete_any_promo_tout_content',
  'permission.delete_own_promo_tout_content' => 'permission.delete_own_promo_tout_content',
  'permission.edit_any_promo_tout_content' => 'permission.edit_any_promo_tout_content',
  'permission.edit_own_promo_tout_content' => 'permission.edit_own_promo_tout_content',
);

$modules = array(
  0 => 'node',
);
