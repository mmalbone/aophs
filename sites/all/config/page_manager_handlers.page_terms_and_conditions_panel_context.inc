<?php
/**
 * @file
 * page_manager_handlers.page_terms_and_conditions_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_terms_and_conditions_panel_context';
$handler->task = 'page';
$handler->subtask = 'terms_and_conditions';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '4350d154-31f1-497c-9a76-f2e0b5a72d71';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-1e7fe875-5263-4945-a71c-6b7a77a4a028';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'AOP Homeschooling Legal Terms & Conditions',
    'body' => '<p>1. <a href="/terms-and-conditions">Legal Terms &amp; Conditions</a><br />
2. <a href="/monarch-eula">Monarch EULA</a><br />
3. <a href="/sos-eula">SOS EULA</a></p>

<p>PLEASE READ THESE TERMS OF USE CAREFULLY BEFORE USING THIS SITE. IF YOU DO NOT AGREE TO THESE TERMS, PLEASE DO NOT USE THE SITE.</p>

<p>By using or allowing others to use the software, materials, interactive features, and website associated with AOP Homeschooling ("Website"), you (the "User") are agreeing to be bound by these legal terms and conditions ("Agreement"). Any person interacting with the Website in any way, including but not limited to students, teachers, administrators, and parents, are Users for the purposes of this Agreement. If you do not agree to the terms and conditions of this Agreement, do not use this Website. If you are dissatisfied with this Website, any Website content, or the terms and conditions of this Agreement, you agree that your sole and exclusive remedy is to discontinue your use of this Website. You acknowledge and accept that your use of this Website is at your sole risk. You represent you have the legal capacity and authority to accept these Legal Terms and Conditions on behalf of yourself or any party you represent. Certain terms of this Agreement may not apply to your use of the Website however all applicable terms are nonetheless binding. As the rightful owner of the Website, Alpha Omega Publications, a division of Glynlyon, Inc., ("AOP") reserves the right to change or terminate the terms of this Agreement at any time and from time to time without any notice to you by posting said changes on the <a href="/">AOP Homeschooling website</a>. Any such changes are hereby incorporated into this Agreement by reference as though fully set forth herein.</p>

<p><strong><u>A. General Provisions</u></strong></p>

<p><strong>1. Indemnification.</strong> You, the User of this Website, agree to indemnify and hold AOP, its subsidiaries, affiliates, and assigns, and each of their directors, officers, agents, contractors, partners and employees, harmless from and against any loss, liability, claim, demand, damages, costs, and expenses, including reasonable attorneys\' fees, arising out of this Agreement or in connection with any use of the Website including but not limited to any damages, losses, or liabilities whatsoever with respect to damage to any property or loss of any data arising from the possession, use, or operation of the Website by the User or any customers, users, students, or others, or arising from transmission of information or the lack thereof connected with the Website described in this Agreement.</p>

<p><strong>2. Termination.</strong> This Agreement shall remain in effect until terminated. This Agreement may be terminated at AOP\'s sole discretion and without prior notice, by mutual written agreement between the parties, but not by the User. AOP may suspend or terminate your access to the Website without prior notice and in AOP\'s sole discretion and AOP shall not be liable for any such suspension, termination, or deletion or its effects, including but not limited to interruption of business or education, loss of data or property, property damage, or any other hardship, losses, or damages. AOP may unilaterally and without notice terminate this Agreement and/or your access to the Website if you or any other person or entity using the Website violates any provision of this Agreement. AOP shall not be liable to you or to any third party for any termination. Upon termination you or any other person or party using the Website shall cease to use the Website at your sole cost and expense.</p>

<p><strong>3. Updates.</strong> At its option, from time to time, AOP may create updated versions of the Website and may make such updates available to you either for a fee or for free. Unless explicitly stated otherwise, any such updates will be subject to the terms of this Agreement including any amendments to this Agreement, to be determined in AOP\'s sole discretion.</p>

<p><strong>4. Proprietary Materials</strong></p>

<p>4a. All content available through the Website, including designs, text, graphics, pictures, video, information, applications, software, music, sound, and other files, and their selection and arrangement ("Site Content"), as well as all software and materials contained in or related to the Website are protected by copyrights, trademarks, service marks, patents, trade secrets, or other proprietary rights and laws. You hereby agree not to sell, license, rent, modify, distribute, copy, reproduce, transmit, publicly display, publicly perform, publish, adapt, edit, or create derivative works from such content or materials. Systematic retrieval of data or other content from the Website to create or compile, directly or indirectly, a collection, compilation, recreation, database, or directory of Website materials is prohibited except as provided for herein. Use of Website content or materials for any purpose not expressly provided for herein is prohibited.</p>

<p>4b. AOP may use third-party advertising companies to serve ads on certain pages of the Website. These companies may use information (not including your name, address, email address, or telephone number) about your visits to this Website and other websites in order to provide advertisements about goods and services than may be of interest to you. If you would like more information about his practice or to opt out of having this information used by these companies, contact AOP via the contract information listed below.</p>

<p><strong>5. Disclaimer of Warranty.</strong> The Website is provided "as is", with all faults and without warranty of any kind. AOP hereby disclaims all warranties with respect to the WEBSITE, either express, implied, or statutory, including but not limited to the implied warranties of merchantability, of satisfactory quality, of fitness for a particular purpose, of accuracy, of quiet enjoyment, and non-infringement of third party rights. AOP does not warrant, guarantee, or make any representations THAT THE CONTENT IS ACCURATE, RELIABLE, OR CORRECT OR THAT IT WILL MEET YOUR NEEDS OR REQUIREMENTS, THAT THe WEBSITE WILL BE AVAILABLE AT ANY PARTICULAR TIME OR LOCATION, THAT ANY DEFECTS OR ERRORS WILL BE CORRECTED, OR THAT THE CONTENT IS FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. You assume the entire risk as to the quality, results, and performance of the Website as well as the entire risk and cost of all service, repair, or correction. No oral or written information, advice, suggestions, or recommendations given by AOP, its representatives, dealers, distributors, agents, or employees shall create a warranty or in any way increase the scope of this Agreement and you may not rely on any such information, advice, suggestions, or recommendations. Some jurisdictions do not allow the exclusion or limitation of certain warranties or consumer rights so some exclusions and limitations may not apply to you.</p>

<p><strong>6. Limitation of Liability.</strong> You hereby agree that AOP, its subsidiaries, affiliates, and assigns, and each of their directors, officers, agents, contractors, partners, and employees, shall not be liable to you or any third party for any indirect, special, consequential, or incidental damages including but not limited to damages for loss of funds or property, business interruption, loss of business opportunity, loss of data, or any other hardship, damages, or losses arising out of or related to: the use or inability to use the Website, however caused; unauthorized or accidental access to or alteration of data; statements or conduct of any third party; or any matter relating to the use of the Website; and even if AOP has been advised of the possibility of such damages. Some jurisdictions do not allow the exclusion or limitation of certain remedies or damages so some exclusions and limitations may not apply to you.</p>

<p><strong>7. Severability.</strong> If any provision of this Agreement is held to be ineffective, unenforceable, or illegal for any reason, AOP may reform such provision to the extent necessary to make it effective, enforceable, and legal or such provision may be deemed severed and in either case this Agreement with such provision reformed or severed shall remain in full force and effect to the fullest extent permitted by law. AOP\'s failure to enforce any part or portion of this Agreement shall not be considered a waiver by AOP.</p>

<p><strong>8. Controlling Law and Controversies.</strong> This Agreement shall be governed by the laws of the State of Arizona and of the United States. You understand and agree that use of the Website may involve interstate data transmissions which may be considered a transaction in interstate commerce under federal law. If any controversy or claim arising out of or relating to this Agreement cannot be solved by negotiation between the parties, the parties hereby agree to attempt in good faith to settle the dispute through mediation administered by a mutually agreed upon mediator in Phoenix, Arizona and in accordance with the Rules of Procedure for Christian Conciliation of the Institute for Christian Conciliation, a division of Peacemaker® Ministries. If mediation fails to resolve the dispute, the parties hereby agree that the dispute shall be settled through arbitration administered by a mutually agreed upon arbitrator in Phoenix, Arizona and in accordance with the Rules of Procedure for Christian Conciliation of the Institute for Christian Conciliation, a division of Peacemaker® Ministries. Any decision may award reasonable attorneys\' fees to the prevailing party and judgment upon an arbitration decision may be entered in any court otherwise having jurisdiction.</p>

<p><strong>9. Entire Agreement.</strong> This Agreement constitutes the entire agreement between AOP and the User relating to the subject matter hereof and supersedes all prior understandings, promises, and undertakings, if any, made orally or in writing with respect to the subject matter hereof. You may not assign any portion of this Agreement without AOP\'s written permission. AOP may assign all or any portion of this Agreement in AOP\'s sole discretion. No modification, amendment, waiver, termination, or discharge of any portion of this Agreement shall be binding unless executed and confirmed in writing by AOP.</p>

<p><strong>10. Export Prohibitions.</strong> Any export or attempt to export the software either partially or in its entirety, related to the Website is governed by United States law and the laws of the jurisdiction in which you reside. Any export of software related to the Website or any portion thereof in any way prohibited by law or regulations issued by agencies of the United States federal government is hereby prohibited. Portions of the Website may include restricted computer software. Neither the Website nor any portion thereof nor the underlying information or technology may be downloaded or otherwise exported or re-exported: (a) into (or to a national or resident of) any U.S. embargoed country; (b) to anyone on the U.S. Treasury Department\'s list of Specially Designated Nationals; or (c) to the U.S. Commerce Department\'s Denied Persons or Entities List or Table of Denial Orders. You hereby represent and warrant that you are not located in or the resident of any such country or on any such list.</p>

<p><strong><u>B. End User Terms and Conditions</u></strong></p>

<p><strong>1. Ownership. </strong>This Website and all associated materials provided by AOP are the solely owned or appropriately licensed property of AOP. The Website is licensed, not sold, to you under the terms of this Agreement. AOP does not sell any title, ownership right, or interest in or to the Website. By using this Website, you are agreeing only to a non-exclusive, nontransferable license to use, according to the terms of this Agreement, the Website and any software programs or other proprietary material of third parties that are incorporated into the Website. AOP reserves and retains all applicable right, title, and interest (including but not limited to copyrights, patents, trademarks, and service marks and other intellectual property rights) in and to the Website and all associated materials. Any remuneration paid for this product constitutes a license fee for the use of the Website.</p>

<p><strong>2. Use.</strong></p>

<p>(a) The copying, reproduction, duplication, translation, reverse engineering, adaptation, decompilation, disassembly, reverse assembly, modification, or alteration of the Website or any portion thereof is expressly prohibited without the prior written consent of AOP except as provided for herein. The merger or inclusion of the Website or any portion thereof with any computer program, and the creation of derivative works or programs from the Website or any portion thereof, is also expressly prohibited without the prior written consent of AOP.</p>

<p>(b) Requests for permission to reproduce, duplicate, adapt, or otherwise exploit any portion of the Website should be submitted in writing to the AOP address listed at the bottom of this Agreement. Any permissions granted shall be in the sole and exclusive discretion of AOP.</p>

<p>(c) Neither the Website nor any part thereof may be rented, leased, sold, assigned, transferred, re-licensed, sub-licensed, or conveyed for any purpose. Any attempted rental, lease, sale, assignment, transfer, re-license, sub-license, conveyance, gift, or other disposition of the Website in violation of this Agreement is null and void. Any act or failure to prevent an act in violation of this Agreement may result in civil and/or criminal prosecution.</p>

<p>(d) Programs or software developed and/or owned by entities other than AOP and included with or incorporated into the Website ("Third Party Software") is subject to and its use is governed by this Agreement. The use of Third Party Software except as for any purpose other than its intended use in conjunction with the Website is prohibited.</p>

<p><strong>3. Registration and Identifying Information.</strong> You hereby represent and warrant that any and all information provided by you to AOP shall be complete, true, accurate, and current in all respects and that you shall update any changes to information as soon as such changes occur. As related to your use of the Website, you are responsible for maintaining the confidentiality of your account and password and for restricting access to your computer. You agree to accept responsibility for all activities that occur under your account and password. When providing any identifying information about students or minors, you hereby represent and warrant that you are authorized to provide such information and that you have read and agreed to the terms of the Privacy Policy included in this Agreement and associated with this Website.</p>

<p><strong>4. Hosting Policy.</strong></p>

<p>(a) AOP may but is not obligated to provide database services to manage student records solely related to the use and application of the Website ("Hosting Services").</p>

<p>(b) Hosting Services provided by AOP in connection with the purchase and use of the Website are included in any price paid for the Website and AOP is not responsible nor will AOP provide or offer any discounts or credits if you do not have adequate facilities or equipment to utilize the Hosting Services.</p>

<p>(c) You agree to exercise the utmost vigilance and care in protecting all information to be transmitted via AOP\'s Hosting Services. AOP is not responsible for any lost, stolen, or otherwise mismanaged data transmitted pursuant to this Agreement.</p>

<p>(d) Any and all information transmitted pursuant to this Agreement shall be subject to and covered by the indemnifications, liability limitations, and Privacy Policy included herein.</p>

<p>(e) AOP reserves the right to modify or discontinue, temporarily or permanently, at any time and from time to time, the Hosting Services (or any part thereof) with or without notice. AOP shall not be liable to you, the User, or to any third party for any modification, suspension, or discontinuance of the Hosting Services, for your or any third party\'s use of the Hosting Services, or for any damages originating therefrom. In no event shall you be entitled to receive a rebate, refund, credit or reduction of any costs or fees which you agreed to pay for the Website.</p>

<p><strong>5. Third Party Sites and Content.</strong> The Website may contain (or may send you through or to) links to other websites ("Third Party Sites") as well as articles, photographs, text, graphics, pictures, designs, music, sound, video, information, applications, software, and other content or items belonging to or originating from third parties ("Third Party Content"). AOP does not check such Third Party Sites and Third Party Content for accuracy, appropriateness, or completeness and AOP is not responsible for any Third Party Sites accessed through use of the Website or for any Third Party Content posted on, available through, or installed from the Website, including the content, accuracy, offensiveness, opinions, reliability, privacy practices, or other policies of or contained in the Third Party Sites or the Third Party Content. Inclusion of, linking to, or permitting the use or installation of any Third Party Site or any Third Party Content does not imply approval or endorsement thereof by AOP. Although some computers may employ filtering software to prevent access to certain Third Party Sites, AOP shall have no responsibility or liability whatsoever for any Third Party Sites or Third Party Content accessed through use of the Website.</p>

<p><strong>6. User Conduct.</strong> You represent, warrant, and agree that no materials of any kind submitted through your account or otherwise created, used, posted, transmitted, or shared by you or others through you on or through the Website will violate or infringe upon the rights of any third party, including copyright, trademark, privacy, publicity, or other personal or proprietary rights; or contain libelous, defamatory, or otherwise unlawful material. You further agree not to use the Website to:</p>

<p>(a) collect email addresses or other contact information of other users from the Website;</p>

<p>(b) send unsolicited communications to other users of the Website;</p>

<p>(c) take any unlawful or unauthorized actions or in any way damage, disable, overburden, or impair the Website or the intellectual property rights owned or licensed by AOP as described elsewhere herein;</p>

<p>(d) upload, post, transmit, share, store, or otherwise make available any content that AOP deems harmful, threatening, unlawful, defamatory, infringing, abusive, inflammatory, harassing, vulgar, obscene, () fraudulent, invasive of privacy or publicity rights, hateful, or racially, ethnically, or otherwise objectionable in AOP\'s sole discretion;</p>

<p>(e) misrepresent yourself, your age, or your affiliation with any person or entity;</p>

<p>(f) upload, post, transmit, share, or otherwise make available any unsolicited or unauthorized advertising, solicitations, promotional materials, "junk mail," "spam," "chain letters," "pyramid schemes," or any other form of solicitation;</p>

<p>(g) upload, post, transmit, share, store, or otherwise make publicly available through the Website any private information of any third party;</p>

<p>(h) solicit personal information from anyone under 18 or solicit passwords or personally identifying information for commercial, unauthorized, or unlawful purposes;</p>

<p>(i) upload, post, transmit, share, or otherwise make available any material that contains software viruses or any other computer code, files, or programs designed to interrupt, destroy, or limit the () functionality of any computer software or hardware or telecommunications equipment;</p>

<p>(k) intimidate or harass another;</p>

<p>(k) upload, post, transmit, share, store, or otherwise make available content that would constitute, encourage, or provide instructions for a criminal offense, violate the rights of any party, or that would otherwise create liability or violate any local, state, national, or international law;</p>

<p>(l) use or attempt to use another\'s account, service, or system or create a false identity on the Website;</p>

<p>(m) interfere with or disrupt the Website or servers or networks connected to the Website, or disobey any requirements, procedures, policies, or regulations of networks connected to the Website;</p>

<p>(n) upload, post, transmit, share, store, or otherwise make available content that infringes any proprietary rights of any party or defames, slanders, or libels any party, or otherwise violates any law of the United States or the jurisdiction in which you reside;</p>

<p>(o) upload, post, transmit, share, store, or otherwise make available content that, in the sole judgment of AOP, is objectionable or which restricts or inhibits any other person from using or enjoying the Website, or which may expose AOP or its users to any harm or liability of any type;</p>

<p>(p) facilitate or encourage any violations of this Agreement.</p>

<p><strong>7. User Content. </strong>You are solely responsible for the profiles (including any name, image, or likeness), messages, notes, text, information, listings, and other content that you upload, publish, or display on or through the Website ("User Content"). Posting, transmitting, or sharing User Content through the Website that you did not create, that you do not have the rights to, or that you do not have permission to post is prohibited. You understand and agree that AOP may, but is not obligated to, review and may delete or remove (without notice) any User Content in its sole discretion, for any reason or no reason, including User Content that in AOP\'s sole judgment violates this Agreement or which might be offensive, illegal, or that might violate the rights, harm, or threaten the safety of users or others. You are solely responsible at your sole cost and expense for creating backup copies and replacing any User Content you post or store on the Site or provide to AOP. When you post User Content, you authorize and direct AOP to make such copies thereof as AOP deems necessary in order to facilitate the posting, storage, and use of the User Content. By posting User Content through any part of the Website, you automatically grant, and you represent and warrant that you have the right to grant, to AOP an irrevocable, perpetual, non-exclusive, transferable, fully paid, worldwide license (with the right to sublicense) to use, copy, publicly perform, publicly display, reformat, translate, excerpt (in whole or in part), and distribute such User Content for any purpose, commercial, advertising, or otherwise, on or in connection with the Website or the promotion thereof, to prepare derivative works of, or incorporate into other works, such User Content, and to grant and authorize sublicenses of the foregoing.</p>

<p><strong>8. Customization Tool.</strong> Through your use of the Website, AOP may provide you access to an application allowing for the creation, modification, and deletion of portions of the Website and its related curriculum ("Customization Tool"). Your use of the Customization Tool is wholly governed by this Agreement. You shall not use the Customization Tool to create any materials which infringe any proprietary rights of any party or defames, slanders, or libels any party, or any content that AOP deems in its sole discretion to be harmful, threatening, unlawful, defamatory, infringing, abusive, inflammatory, harassing, vulgar, obscene, fraudulent, invasive of privacy or publicity rights, hateful, or racially, ethnically, or otherwise objectionable, or otherwise violates any law of the United States or the jurisdiction in which you reside. AOP shall retain all right, title, and interest in and to all materials originally provided as part of the Website. You shall not own any right, title, or interest in or to any material created through the use of the Customization Tool nor may you rent, lease, sell, assign, transfer, re-license, sub-license, convey, gift, or otherwise dispose of any material created through the use of the Customization Tool. AOP shall own all right, title, and interest in and to any material created through the use of the Customization Tool unless such materials defame, libel, slander, or infringe or otherwise violate the rights of any third party or are unauthorized in AOP\'s sole discretion. AOP hereby disclaims for all purposes and in all circumstances any responsibility or liability for any materials created through the use of the Customization Tool.</p>

<p><strong><u>C. Privacy Policy</u></strong></p>

<p>AOP institutes the terms of this privacy policy ("Privacy Policy") to ensure that Users\' personally identifiable information ("PII") is maintained in a safe, secure, and responsible manner. The following describes how AOP collects, uses, and shares the PII obtained from and about individuals who use the Website. This policy does not apply to information that AOP may collect outside the use of the Website, such as over the phone, by fax, or through conventional mail. All children\'s educational records are protected as required by the Family Educational Rights and Privacy Act (FERPA), the Children\'s Online Privacy Protection Act (COPPA), and other applicable federal and state laws.</p>

<p><strong>1. Information Collected.</strong></p>

<p>(a) AOP may collect PII for purposes of administering educational programs, to improve the content of the Website and AOP services, or for any other commercial purpose. The only PII collected from you has been voluntarily submitted and provided to AOP through your use of the Website, in response to surveys, or in response to other requests for information.</p>

<p>(b) The types of information that AOP may collect include but are not limited to: name, address, email address, services requested, student registration, and enrollment information.</p>

<p>(c) AOP maintains all student data, student educational information, and student files as confidential in order to protect the privacy of students and schools. Strict security procedures ensure that student and school files and information are not disclosed to unauthorized parties.</p>

<p><strong>2. AOP Rights.</strong></p>

<p>(a) AOP reserves the right to use "cookies" to personalize the online experience.</p>

<p>(b) AOP reserves the right to collect and log non-personal information related to your use of the Website.</p>

<p>(c) AOP reserves the right to contact you for any reason related to the Website in AOP\'s sole discretion and without any opt-out option.</p>

<p><strong>3. Children. </strong>AOP takes all steps necessary to protect children and to comply with the Children\'s Online Privacy Protection Act (COPPA).</p>

<p>(a) The only student PII collected by AOP is first and last name, username, and password, and is supplied by you through student enrollment. Student PII is limited to what is required to identify the user and is only used to identify the user. AOP may not require disclosure of more student PII than is reasonably necessary to utilize the Website.</p>

<p>(b) Student PII is not disclosed to third parties. If AOP were to disclose any student PII to third parties, the User or the student\'s parent or guardian would have the option to agree to the collection and use of the student PII without consenting to the disclosure of the PII to third parties.</p>

<p>(c) Parents or guardians have the right to review their student\'s PII collected by AOP, request that their child\'s PII collected by AOP be revised or deleted, or refuse to allow further PII to be collected by AOP by making a request in writing to the address listed below.</p>

<p><strong>4. Third Party Web Sites.</strong></p>

<p>(a) This Privacy Policy applies only to the Website and not to the websites of partners, affiliates, or Third Party Sites. You may be required to accept additional policies prior to your use of links accessed through the Website. AOP is not responsible for the privacy policies of partners, affiliates, or Third Party Sites.</p>

<p>(b) AOP may use third-party advertising companies to serve you ads on the internet. These companies may use information (not including your name, address, email address, or telephone number) about your visits to this Website and other websites in order to provide advertisements about goods and services that may be of interest to you. If you would like more information about this practice or to opt out of having this information used by these companies, you may visit the <a href="http://www.networkadvertising.org/managing/opt_out.asp">Network Advertising Initiative</a>.</p>

<p><strong>5. Third Party Servers.</strong> Certain information stored or transmitted by AOP hereunder, including PII, may be stored, managed, or otherwise handled by third parties. You understand and agree that AOP is not liable, accountable, or otherwise responsible for information communicated to or through third party servers and you hereby release AOP from any associated liability or responsibility consistent with the indemnifications, liability limitations, and Privacy Policy included herein.</p>

<p><strong>6. User Generated Content.</strong> You participate in any message boards, chat rooms, blogs, or other interactive features of the Website at your own risk. User generated content posted to the Website is the sole and exclusive responsibility of the User and AOP has no responsibility or liability for or association or involvement with such content. AOP may but is not obligated to monitor message boards, chat rooms, blogs or other interactive features of the Website but AOP cannot and will not guarantee the safety or security of any User generated content nor does AOP offer any assurances that User generated content will appear on the Website accurately or in its entirety.</p>

<p><strong>7. Access to Records and Other Personal Data. </strong>AOP takes all steps necessary to protect student privacy and to comply with the Family Educational Rights and Privacy Act (FERPA).</p>

<p>(a) The User has the right to inspect and review the student\'s education records maintained by AOP.</p>

<p>(b) The User the right to request that AOP correct records maintained by AOP which the User believes to be inaccurate or misleading.</p>

<p>(c) AOP may release information from a student\'s educational record to the following parties or under the following conditions:</p>

<p>i. School officials with legitimate educational interest;<br />
ii. Other schools to which a student is transferring;<br />
iii. Specified officials for audit or evaluation purposes;<br />
iv. Appropriate parties in connection with financial aid to a student;<br />
v. Organizations conducting certain studies for or on behalf of the school;<br />
vi. Accrediting organizations;<br />
vii. To comply with a judicial order or lawfully issued subpoena;<br />
viii. Appropriate officials in cases of health and safety emergencies; and<br />
ix. State and local authorities, within a juvenile justice system, pursuant to specific state law.</p>

<p><strong>8. Access and Corrections to PII Collected by AOP and Opting Out of Further Communications.</strong> Use the contact information below to request:</p>

<p>(a) Corrections or updates of any PII in AOP\'s database that you believe to be erroneous;</p>

<p>(b) An opt-out of future communications from AOP; or</p>

<p>(c) AOP to make reasonable efforts to remove PII from its online database. You acknowledge and accept that it may be impossible to delete personal information entirely because of backups and records of deletions.</p>

<p>(d) You may access or correct your PII in AOP\'s database or opt out of further communications by writing to:<br />
Alpha Omega Publications<br />
A division of Glynlyon, Inc.<br />
Attn: Legal Department<br />
300 N. McKemy Ave.<br />
Chandler, AZ 85226</p>

<p><strong>9. Changes in Corporate Structure. </strong>If all or part of AOP or any of its affiliates are sold, merged, or otherwise transferred to another entity, the PII provided through use of the Website may be transferred as part of that transaction or process. The Family Educational Rights and Privacy Act (FERPA) protects children\'s educational records in the event of such a transaction or transfer.</p>

<p><strong>10. Contact. </strong>If you have any questions regarding this Privacy Policy, please contact:<br />
Alpha Omega Publications<br />
A division of Glynlyon, Inc.<br />
Attn: Legal Department<br />
300 N. McKemy Ave.<br />
Chandler, AZ 85226</p>

<p><strong><u>D. Third Party Notices</u></strong></p>

<p>The Website may incorporate or have been created with the use of and in conjunction with Third Party Software. This Third Party Software may only be used in conjunction with the Website and you may not use this Third Party Software for any other purpose or with any other product or service at any time or for any reason.</p>

<p><strong>New American Standard Bible Updates</strong><br />
© 1960, 1962, 1963, 1968, 1971, 1972, 1973, 1975, 1977, 1995<br />
by The Lockman Foundation<br />
All rights reserved<br />
<u>http://www.lockman.org</u></p>

<p>The "NASB", "NAS", "New American Standard Bible" and "New American Standard" trademarks are registered in the United States Patent and Trademark Office by the Lockman Foundation. Use of these trademarks requires the permission of the Lockman Foundation.</p>

<p>The text of the New American Standard Bible® may be quoted and/or reprinted up to and inclusive of five hundred (500) verses without express written permission of The Lockman Foundation, providing the verses do not amount to a complete book of the Bible nor do the verses quoted account for more than twenty-five percent (25%) of the total work in which they are quoted.</p>

<p>Notice of copyright must appear on the title or copyright page of the work as follows:</p>

<p>Scripture taken from the New American Standard Bible,<br />
© The Lockman Foundation 1960, 1962, 1963, 1968, 1971,<br />
1972, 1973, 1975, 1977, 1995<br />
Used by permission. <u>http://www.Lockman.org</u></p>

<p>When quotations from the NASB® text are used in not-for-sale media, such as church bulletins, orders of service, posters, transparencies, or similar media, the abbreviation (NASB) may be used at the end of the quotation. This permission to quote is limited to material which is wholly manufactured in compliance with the provisions of the copyright laws of the United States and all applicable international conventions and treaties. Quotations and/or reprints in excess of the above limitations or other permission requests must be directed to and approved in writing by The Lockman Foundation, P.O. Box 2279, La Habra, CA 90632-2279, (714) 879-3055, http://www.lockman.org.</p>

<p>Copying, duplicating, altering, or otherwise modifying the NASB databases is prohibited.</p>

<p><strong>AOP Contact Information</strong><br />
Alpha Omega Publications<br />
A division of Glynlyon, Inc.<br />
Attn: Legal Department<br />
300 N. McKemy Ave.<br />
Chandler, AZ 85226</p>

<p>Alpha Omega Publications and its logo are registered trademarks of Alpha Omega Publications, a division of Glynlyon, Inc.&nbsp;All trademarks that appear on this website are the property of their respective owners. All rights reserved.</p>

<p>© 2012 Alpha Omega Publications<br />
All rights reserved.</p>

<p><a href="#">Top of Page</a></p>
',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '1e7fe875-5263-4945-a71c-6b7a77a4a028';
  $display->content['new-1e7fe875-5263-4945-a71c-6b7a77a4a028'] = $pane;
  $display->panels['middle'][0] = 'new-1e7fe875-5263-4945-a71c-6b7a77a4a028';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = 'new-1e7fe875-5263-4945-a71c-6b7a77a4a028';
$handler->conf['display'] = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
