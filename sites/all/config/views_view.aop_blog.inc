<?php
/**
 * @file
 * views_view.aop_blog.inc
 */

$api = '2.0.0';

$data = $view = new view();
$view->name = 'aop_blog';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'AOP Blog';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = '<span>Recent Stories</span>';
$handler->display->display_options['use_more_always'] = TRUE;
$handler->display->display_options['use_more_text'] = 'Read More Stories';
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '2';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'blog_post' => 'blog_post',
);
/* Filter criterion: Content: Promote as Popular (field_promote_as_popular) */
$handler->display->display_options['filters']['field_promote_as_popular_value']['id'] = 'field_promote_as_popular_value';
$handler->display->display_options['filters']['field_promote_as_popular_value']['table'] = 'field_data_field_promote_as_popular';
$handler->display->display_options['filters']['field_promote_as_popular_value']['field'] = 'field_promote_as_popular_value';
$handler->display->display_options['filters']['field_promote_as_popular_value']['value'] = array(
  1 => '1',
);

/* Display: Homepage Recent Stories */
$handler = $view->new_display('panel_pane', 'Homepage Recent Stories', 'panel_pane_1');
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['row_plugin'] = 'node';
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: Vote */
$handler->display->display_options['fields']['field_vote']['id'] = 'field_vote';
$handler->display->display_options['fields']['field_vote']['table'] = 'field_data_field_vote';
$handler->display->display_options['fields']['field_vote']['field'] = 'field_vote';
$handler->display->display_options['fields']['field_vote']['label'] = '';
$handler->display->display_options['fields']['field_vote']['element_type'] = '0';
$handler->display->display_options['fields']['field_vote']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_vote']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_vote']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_vote']['click_sort_column'] = 'rating';
$handler->display->display_options['fields']['field_vote']['settings'] = array(
  'widget' => array(
    'fivestar_widget' => 'default',
  ),
  'expose' => 1,
  'style' => 'average',
  'text' => 'average',
);
$handler->display->display_options['fields']['field_vote']['field_api_classes'] = TRUE;
/* Field: Content: Blog Body */
$handler->display->display_options['fields']['field_blog_body']['id'] = 'field_blog_body';
$handler->display->display_options['fields']['field_blog_body']['table'] = 'field_data_field_blog_body';
$handler->display->display_options['fields']['field_blog_body']['field'] = 'field_blog_body';
$handler->display->display_options['fields']['field_blog_body']['label'] = '';
$handler->display->display_options['fields']['field_blog_body']['element_type'] = '0';
$handler->display->display_options['fields']['field_blog_body']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_blog_body']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_blog_body']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_blog_body']['type'] = 'text_trimmed';
$handler->display->display_options['fields']['field_blog_body']['settings'] = array(
  'trim_length' => '600',
);
$handler->display->display_options['fields']['field_blog_body']['field_api_classes'] = TRUE;
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'blog_post' => 'blog_post',
);
/* Filter criterion: Content: Promote as Popular (field_promote_as_popular) */
$handler->display->display_options['filters']['field_promote_as_popular_value']['id'] = 'field_promote_as_popular_value';
$handler->display->display_options['filters']['field_promote_as_popular_value']['table'] = 'field_data_field_promote_as_popular';
$handler->display->display_options['filters']['field_promote_as_popular_value']['field'] = 'field_promote_as_popular_value';
$handler->display->display_options['filters']['field_promote_as_popular_value']['value'] = array(
  0 => '0',
);

/* Display: Homepage Popular Articles */
$handler = $view->new_display('panel_pane', 'Homepage Popular Articles', 'panel_pane_2');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = '<span>Popular Articles</span>';
$handler->display->display_options['defaults']['css_class'] = FALSE;
$handler->display->display_options['css_class'] = 'pop-articles';
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '3';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Path */
$handler->display->display_options['fields']['path']['id'] = 'path';
$handler->display->display_options['fields']['path']['table'] = 'node';
$handler->display->display_options['fields']['path']['field'] = 'path';
$handler->display->display_options['fields']['path']['label'] = '';
$handler->display->display_options['fields']['path']['exclude'] = TRUE;
$handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_type'] = 'h2';
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
/* Field: Content: Blog Body */
$handler->display->display_options['fields']['field_blog_body']['id'] = 'field_blog_body';
$handler->display->display_options['fields']['field_blog_body']['table'] = 'field_data_field_blog_body';
$handler->display->display_options['fields']['field_blog_body']['field'] = 'field_blog_body';
$handler->display->display_options['fields']['field_blog_body']['label'] = '';
$handler->display->display_options['fields']['field_blog_body']['alter']['max_length'] = '250';
$handler->display->display_options['fields']['field_blog_body']['alter']['more_link'] = TRUE;
$handler->display->display_options['fields']['field_blog_body']['alter']['more_link_text'] = 'MORE';
$handler->display->display_options['fields']['field_blog_body']['alter']['more_link_path'] = '[path]';
$handler->display->display_options['fields']['field_blog_body']['alter']['trim'] = TRUE;
$handler->display->display_options['fields']['field_blog_body']['alter']['html'] = TRUE;
$handler->display->display_options['fields']['field_blog_body']['element_type'] = '0';
$handler->display->display_options['fields']['field_blog_body']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_blog_body']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_blog_body']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_blog_body']['field_api_classes'] = TRUE;

/* Display: Latest News */
$handler = $view->new_display('panel_pane', 'Latest News', 'panel_pane_3');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['defaults']['use_more'] = FALSE;
$handler->display->display_options['use_more'] = TRUE;
$handler->display->display_options['defaults']['use_more_always'] = FALSE;
$handler->display->display_options['defaults']['use_more_always'] = FALSE;
$handler->display->display_options['use_more_always'] = TRUE;
$handler->display->display_options['defaults']['use_more_text'] = FALSE;
$handler->display->display_options['use_more_text'] = 'Read More ';
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '0';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['quantity'] = '9';
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['row_plugin'] = 'node';
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['footer'] = FALSE;
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'blog_post' => 'blog_post',
);
$handler->display->display_options['filters']['type']['group'] = 1;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'views',
  1 => 'node',
  2 => 'views_content',
  3 => 'fivestar',
  4 => 'text',
);
