<?php
/**
 * @file
 * page_manager_handlers.page__k12_independent_consumer_direct_vs_horizons_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page__k12_independent_consumer_direct_vs_horizons_panel_context';
$handler->task = 'page';
$handler->subtask = '_k12_independent_consumer_direct_vs_horizons';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'eba9c8bc-2279-4762-b2ba-d7191bec6120';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-fef27e50-344a-4eb7-b44b-bf5a8f91aa6b';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'K12 Independent Consumer Direct vs. Horizons',
    'body' => '<p>Do you need to compare the independent online curriculum offered by K12 to the Horizons program from Alpha Omega Publications? You\'re in the right spot. Find a short overview of each curriculum and service below, an easy-to-view comparison chart, and additional comparison options!</p><h3><strong>K12 Independent Consumer Direct</strong></h3><p>K12 is a large secular provider of kindergarten through 12th grade online curriculum currently being used in virtual public and private schools, an international online academy, as well as in homeschools worldwide. Because K12 is a secular curriculum provider, parents from various faiths and cultural backgrounds may find K12 very appropriate for their families\' homeschool needs. K12 offers curriculum to homeschoolers via Consumer Direct purchase. Families who purchase the K12 courses directly are not officially enrolled in a K12 school. Independent Consumer Direct is a kindergarten through 8th grade online school program available for purchase by families interested in independently administrated coursework. K12 Independent Consumer Direct provides all the essential tools needed to provide children with a quality education at home.</p><p>The K12 curriculum covers six core subjects: language arts/English, math, science, history, art, and music, and is based on time-tested and research-based methods of instruction. K12 delivers lessons, progress and planning tools, teacher\'s guides, assessments, and learning aids over the Internet. Required supplemental learning materials, such as books, workbooks, CDs, and videos are NOT included in course tuition and must be purchased separately. Independent Consumer Direct courses are not accredited. Purchase of Consumer Direct courses does not include graduation or diploma services.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>If you\'re considering an online approach to homeschooling, remember there is no one perfect homeschool curriculum. A blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Instead of feeling torn between different curricula, parents should be open to mixing and matching materials and resources to customize their child\'s education. Remember, each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3><strong>Horizons from Alpha Omega Publications</strong></h3><p>Horizons is a preK-12 print-based curriculum offered by Alpha Omega Publications, a Christian-based publisher that provides curriculum and educational services to homeschool families. Filled with engaging, full-color illustrations, Horizons is a child-friendly workbook-based curriculum with consumable lessons and hands-on activities. Horizons curriculum encourages concept mastery through a spiral-based learning process of skill/concept presentation, review, and reinforcement. Teacher\'s guides offer clear, step-by-step lesson plans and activities that are suitable for both homeschool and classroom settings.</p><p>Winner of Cathy Duffy\'s 100 Top Picks for Homeschool Curriculum, Horizons integrates Scripture and Christian living throughout all subjects. Horizons workbooks provide removable student pages for ease of student use. Horizons curriculum includes courses in math (K-8), penmanship (1-5), health (K-8), physical education (PreK-12), phonics and reading (K-3), and spelling and vocabulary (1-3). Health and physical education electives were designed for classroom use and may need to be adapted to homeschool use. Complete Horizons Preschool Curriculum Sets and Multimedia Sets are also available. Horizons curriculum sets include all student and teacher materials. Student and teacher materials can also be purchased separately.</p><h3><strong>Benefits and Features of Horizons from Alpha Omega Publications</strong></h3><p class="list_heading">Horizons</p><ul><li>brightly illustrated, hands-on, consumable curriculum</li><li>offers fun lessons and manipulative-based activities</li><li>teaches concepts through a spiral learning process</li><li>encourages concept mastery through quick concept introduction, review, and reinforcement</li></ul><h3 class="para-start"><strong>Take a Closer Look at K12 Independent Consumer Direct and Horizons</strong></h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">Horizons</th><th class="compare">K12 Independent Consumer Direct</th></tr>
</thead>
<tbody>
<tr>
<td>K-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Complete curriculum</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Instruction provided by parents</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Teacher-intensive</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Readiness tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Easy to use consumable workbooks</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Material designed for homeschoolers</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Manageable amount of daily lesson material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr><th class="features">Cost</th><th class="compare">Alpha Omega Publications</th><th class="compare">K12</th></tr>
</thead>
<tbody>
<tr>
<td>Average subject cost per grade level**</td>
<td class="tac">$79.95</td>
<td class="tac">$289</td>
</tr>
</tbody>
</table>
</div>
<h4><strong>Compare Offerings: K12 versus Alpha Omega Publications</strong></h4>
<p class="list_heading_links">K12 Independent Consumer Direct</p>
<ul>
<li><a href="/k12-independent-vs-monarch">K12 Independent Consumer Direct vs. Monarch</a></li>
<li><a href="/k12-independent-vs-aoa">K12 Independent Consumer Direct vs. Alpha Omega Academy</a></li>
<li><a href="/k12-independent-vs-horizons">K12 Independent Consumer Direct vs. Horizons</a></li>
<li><a href="/k12-independent-vs-lifepac">K12 Independent Consumer Direct vs. LIFEPAC</a></li>
<li><a href="/k12-independent-vs-sos">K12 Independent Consumer Direct vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">K12 Teacher-Supported Consumer Direct</p>
<ul>
<li><a href="/k12-teacher-supported-vs-monarch">K12 Teacher-Supported Consumer Direct vs. Monarch</a></li>
<li><a href="/k12-teacher-supported-vs-aoa">K12 Teacher-Supported Consumer Direct vs. Alpha Omega Academy</a></li>
<li><a href="/k12-teacher-supported-vs-horizons">K12 Teacher-Supported Consumer Direct vs. Horizons</a></li>
<li><a href="/k12-teacher-supported-vs-lifepac">K12 Teacher-Supported Consumer Direct vs. LIFEPAC</a></li>
<li><a href="/k12-teacher-supported-vs-sos">K12 Teacher-Supported Consumer Direct vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">K12 Virtual Schools</p>
<ul>
<li><a href="/k12-virtual-schools-vs-monarch">K12 Virtual Schools vs. Monarch</a></li>
<li><a href="/k12-virtual-schools-vs-aoa">K12 Virtual Schools vs. Alpha Omega Academy</a></li>
<li><a href="/k12-virtual-schools-vs-horizons">K12 Virtual Schools vs. Horizons</a></li>
<li><a href="/k12-virtual-schools-vs-lifepac">K12 Virtual Schools vs. LIFEPAC</a></li>
<li><a href="/k12-virtual-schools-vs-sos">K12 Virtual Schools vs. Switched-On Schoolhouse</a></li>
</ul>
<p>* K12 is the registered trademark of K12 Inc. Alpha Omega Publications is not in any way affiliated with K12. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by K12.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p><p>**Prices obtained February 2013 from the K12 Website and from Alpha Omega Publications 2013 Homeschool Catalog. Individual course prices are based on a comparison between the use of the 5th grade Horizons math program and direct purchase of K12 5th grade math plus required materials. Costs do not reflect any additional fees or shipping and handling charges. Prices for other K12 and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'fef27e50-344a-4eb7-b44b-bf5a8f91aa6b';
  $display->content['new-fef27e50-344a-4eb7-b44b-bf5a8f91aa6b'] = $pane;
  $display->panels['left'][0] = 'new-fef27e50-344a-4eb7-b44b-bf5a8f91aa6b';
  $pane = new stdClass();
  $pane->pid = 'new-4144c572-57b6-46aa-b871-149b9de45e08';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '4144c572-57b6-46aa-b871-149b9de45e08';
  $display->content['new-4144c572-57b6-46aa-b871-149b9de45e08'] = $pane;
  $display->panels['right'][0] = 'new-4144c572-57b6-46aa-b871-149b9de45e08';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-fef27e50-344a-4eb7-b44b-bf5a8f91aa6b';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
