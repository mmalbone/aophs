<?php
/**
 * @file
 * image_style.curriculum_carousel_thumb_82px_height.inc
 */

$api = '2.0.0';

$data = array(
  'effects' => array(
    0 => array(
      'data' => array(
        'height' => '82',
        'upscale' => 1,
        'width' => '',
      ),
      'dimensions callback' => 'image_scale_dimensions',
      'effect callback' => 'image_scale_effect',
      'form callback' => 'image_scale_form',
      'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
      'label' => 'Scale',
      'module' => 'image',
      'name' => 'image_scale',
      'summary theme' => 'image_scale_summary',
      'weight' => '1',
    ),
  ),
  'label' => 'Curriculum Carousel Thumb 82px height',
  'name' => 'curriculum_carousel_thumb_82px_height',
  'storage' => 1,
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'image',
);
