<?php
/**
 * @file
 * page_manager_handlers.page_sos_tech_support_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_sos_tech_support_panel_context';
$handler->task = 'page';
$handler->subtask = 'sos_tech_support';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'flexible';
$display->layout_settings = array(
  'items' => array(
    'canvas' => array(
      'type' => 'row',
      'contains' => 'column',
      'children' => array(
        0 => 'main',
      ),
      'parent' => NULL,
    ),
    'main' => array(
      'type' => 'column',
      'width' => 100,
      'width_type' => '%',
      'children' => array(
        0 => 'main-row',
      ),
      'parent' => 'canvas',
    ),
    'main-row' => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'center',
        1 => 'right',
      ),
      'parent' => 'main',
    ),
    'center' => array(
      'type' => 'region',
      'title' => 'Center',
      'width' => '70.04168670835338',
      'width_type' => '%',
      'parent' => 'main-row',
    ),
    'right' => array(
      'type' => 'region',
      'title' => 'Right',
      'width' => '29.95831329164662',
      'width_type' => '%',
      'parent' => 'main-row',
      'class' => '',
    ),
  ),
);
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'center' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'f077d586-2914-4689-8dfd-c813ef2c38fd';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-9c7e89e0-752a-493b-aa88-1689a6fc0ce5';
  $pane->panel = 'center';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => '',
    'body' => '<div class="box">
<h2>SOS Support Database</h2>
<p>Alpha Omega Publications provides a technical support database to help you use our Christian homeschool curriculum with ease. Our collection of resources is regularly updated as new issues and solutions are discovered for Switched-On Schoolhouse.</p>
</div>
<div class="box_contents"><strong> <a href="/tech-support/technical_support_issue_1">Bible Builder</a> </strong> <br /> <strong> <a href="/tech-support/technical_support_issue_2">Calendar Issues</a> </strong> <br /> <strong> <a href="/tech-support/technical_support_issue_3">Captain Bible</a> </strong> <br /> <strong> <a href="/tech-support/technical_support_issue_3_1">Color Phonics</a> </strong> <br /> <strong> <a href="/tech-support/technical_support_issue_3_2">Curriculum Issues</a> </strong> <br /> <strong> <a href="/tech-support/technical_support_issue_4">Database and Backup Issues</a> </strong> <br /> <strong> <a href="/tech-support/technical_support_issue_5">Error Solutions</a> </strong> <br /> <strong> <a href="/tech-support/browse_techsupport_db_144">Exploring Time on CD-ROM</a> </strong> <br /> <strong> <a href="/tech-support/technical_support_issue_7">Free Placement Test</a> </strong> <br /> <strong> <a href="/tech-support/sos_tutorials">General Issues</a> </strong> <br /> <strong> <a href="/tech-support/technical_support_issue_9">Installation Issues</a> </strong> <br /> <strong> <a href="/tech-support/technical_support_issue_10">Networking Issues</a> </strong> <br /> <strong> <a href="/tech-support/technical_support_issue_11">Printing Issues</a> </strong> <br /> <strong> <a href="/tech-support/technical_support_issue_12">Products from Alfred Publishing Co.</a> </strong> <br /> <strong> <a href="/tech-support/technical_support_issue_13">Products from Individual Software Inc</a> </strong> <br /> <strong> <a href="/tech-support/technical_support_issue_14">Rev-up for (Reading, Writing and Arithmetic)</a> </strong> <br /> <strong> <a href="/tech-support/technical_support_issue_15">SOS 1.0 (1998-2002)</a> </strong> <br /> <strong> <a href="/tech-support/technical_support_issue_16">Sound and Audio Issues</a> </strong> <br /> <strong> <a href="/tech-support/technical_support_issue_17">Spanish/French Issues</a> </strong> <br /> <strong> <a href="/tech-support/technical_support_issue_18">Startup / Login Issues</a> </strong> <br /> <strong> <a href="/tech-support/technical_support_issue_19">Uninstalling Issues</a> </strong> <br /> <strong> <a href="/tech-support/technical_support_issue_20">Use Issues</a> </strong></div>
</div></div>                </div>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '9c7e89e0-752a-493b-aa88-1689a6fc0ce5';
  $display->content['new-9c7e89e0-752a-493b-aa88-1689a6fc0ce5'] = $pane;
  $display->panels['center'][0] = 'new-9c7e89e0-752a-493b-aa88-1689a6fc0ce5';
  $pane = new stdClass();
  $pane->pid = 'new-ecfad297-81d9-459c-984b-b361d58d2403';
  $pane->panel = 'right';
  $pane->type = 'block';
  $pane->subtype = 'block-22';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Contact Tech Support',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'ecfad297-81d9-459c-984b-b361d58d2403';
  $display->content['new-ecfad297-81d9-459c-984b-b361d58d2403'] = $pane;
  $display->panels['right'][0] = 'new-ecfad297-81d9-459c-984b-b361d58d2403';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-9c7e89e0-752a-493b-aa88-1689a6fc0ce5';
$handler->conf['display'] = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
