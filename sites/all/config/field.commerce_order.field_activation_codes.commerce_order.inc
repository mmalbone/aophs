<?php
/**
 * @file
 * field.commerce_order.field_activation_codes.commerce_order.inc
 */

$api = '2.0.0';

$data = array(
  'field_config' => array(
    'active' => '1',
    'cardinality' => '-1',
    'columns' => array(
      'revision_id' => array(
        'description' => 'The field collection item revision id.',
        'not null' => FALSE,
        'type' => 'int',
      ),
      'value' => array(
        'description' => 'The field collection item id.',
        'not null' => FALSE,
        'type' => 'int',
      ),
    ),
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_activation_codes',
    'foreign keys' => array(),
    'indexes' => array(
      'revision_id' => array(
        0 => 'revision_id',
      ),
    ),
    'locked' => '0',
    'module' => 'field_collection',
    'settings' => array(
      'hide_blank_items' => 1,
      'path' => '',
    ),
    'storage' => array(
      'active' => '1',
      'details' => array(
        'sql' => array(
          'FIELD_LOAD_CURRENT' => array(
            'field_data_field_activation_codes' => array(
              'revision_id' => 'field_activation_codes_revision_id',
              'value' => 'field_activation_codes_value',
            ),
          ),
          'FIELD_LOAD_REVISION' => array(
            'field_revision_field_activation_codes' => array(
              'revision_id' => 'field_activation_codes_revision_id',
              'value' => 'field_activation_codes_value',
            ),
          ),
        ),
      ),
      'module' => 'field_sql_storage',
      'settings' => array(),
      'type' => 'field_sql_storage',
    ),
    'translatable' => '0',
    'type' => 'field_collection',
  ),
  'field_instance' => array(
    'bundle' => 'commerce_order',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => 'This is only for manual orders where the subscription is already active.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'field_collection',
        'settings' => array(
          'view_mode' => 'full',
        ),
        'type' => 'field_collection_fields',
        'weight' => 10,
      ),
    ),
    'entity_type' => 'commerce_order',
    'fences_wrapper' => '',
    'field_name' => 'field_activation_codes',
    'label' => 'Activation Codes',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'field_collection',
      'settings' => array(),
      'type' => 'field_collection_embed',
      'weight' => '1',
    ),
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'field_sql_storage',
  1 => 'field_collection',
);
