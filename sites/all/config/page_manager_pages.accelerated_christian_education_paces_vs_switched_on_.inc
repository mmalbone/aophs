<?php
/**
 * @file
 * page_manager_pages.accelerated_christian_education_paces_vs_switched_on_.inc
 */

$api = '2.0.0';

$data = $page = new stdClass();
$page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
$page->api_version = 1;
$page->name = 'accelerated_christian_education_paces_vs_switched_on_';
$page->task = 'page';
$page->admin_title = 'Accelerated Christian Education PACEs vs. Switched-On ';
$page->admin_description = '';
$page->path = 'ace-pace-vs-sos';
$page->access = array();
$page->menu = array();
$page->arguments = array();
$page->conf = array(
  'admin_paths' => FALSE,
);


$dependencies = array(
  'page_manager_handlers.page_accelerated_christian_education_paces_vs_switched_on__panel_context' => 'page_manager_handlers.page_accelerated_christian_education_paces_vs_switched_on__panel_context',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
