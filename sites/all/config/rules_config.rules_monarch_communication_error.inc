<?php
/**
 * @file
 * rules_config.rules_monarch_communication_error.inc
 */

$api = '2.0.0';

$data = entity_import('rules_config', '{ "rules_monarch_communication_error" : {
      "LABEL" : "Monarch Communication Error",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "mimemail", "aop_assist" ],
      "ON" : { "aop_assist_monarch_error" : [] },
      "DO" : [
        { "mimemail" : {
            "key" : "Monarch Communication Error",
            "to" : "webservicesteam@glynlyon.com",
            "from_name" : "Drupal Webstore",
            "from_mail" : "webservicesteam@glynlyon.com",
            "reply_to" : "webservicesteam@glynlyon.com",
            "subject" : "Webstore cannot reach Monarch",
            "body" : "The Drupal webstore is unable to communicate with Monarch",
            "plaintext" : "The Drupal webstore is unable to communicate with Monarch",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'entity',
  1 => 'aop_assist',
  2 => 'mimemail',
  3 => 'rules',
);
