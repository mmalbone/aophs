<?php
/**
 * @file
 * panelizer_defaults.taxonomy_term_curriculum_default.inc
 */

$api = '2.0.0';

$data = $panelizer = new stdClass();
$panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
$panelizer->api_version = 1;
$panelizer->name = 'taxonomy_term:curriculum:default';
$panelizer->title = 'Default';
$panelizer->panelizer_type = 'taxonomy_term';
$panelizer->panelizer_key = 'curriculum';
$panelizer->no_blocks = FALSE;
$panelizer->css_id = '';
$panelizer->css = '';
$panelizer->pipeline = 'ipe';
$panelizer->contexts = array();
$panelizer->relationships = array();
$panelizer->access = array();
$panelizer->view_mode = 'page_manager';
$panelizer->css_class = 'homeschool-curriculum';
$panelizer->title_element = 'H2';
$panelizer->link_to_entity = TRUE;
$panelizer->extra = array();
$display = new panels_display();
$display->layout = 'brenham';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'header' => NULL,
    'sidebar' => NULL,
    'contentmain' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '1993fb3f-476e-49db-a5b6-d21d5072c890';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-6c3f60a8-2fa6-4768-ab42-ac3351da77c0';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'taxonomy_term:field_curriculum_full_title';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'text_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '6c3f60a8-2fa6-4768-ab42-ac3351da77c0';
  $display->content['new-6c3f60a8-2fa6-4768-ab42-ac3351da77c0'] = $pane;
  $display->panels['contentmain'][0] = 'new-6c3f60a8-2fa6-4768-ab42-ac3351da77c0';
  $pane = new stdClass();
  $pane->pid = 'new-8dec18e8-6556-492b-9945-9f30ba79ee73';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'taxonomy_term:field_grade_levels';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'text_plain',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '8dec18e8-6556-492b-9945-9f30ba79ee73';
  $display->content['new-8dec18e8-6556-492b-9945-9f30ba79ee73'] = $pane;
  $display->panels['contentmain'][1] = 'new-8dec18e8-6556-492b-9945-9f30ba79ee73';
  $pane = new stdClass();
  $pane->pid = 'new-44c4c086-197f-42f8-b8d7-0af710cea780';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field_extra';
  $pane->subtype = 'taxonomy_term:description';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_mode' => 'full',
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '44c4c086-197f-42f8-b8d7-0af710cea780';
  $display->content['new-44c4c086-197f-42f8-b8d7-0af710cea780'] = $pane;
  $display->panels['contentmain'][2] = 'new-44c4c086-197f-42f8-b8d7-0af710cea780';
  $pane = new stdClass();
  $pane->pid = 'new-d15e4654-6e8b-40f9-94ad-ed6899e1d6d8';
  $pane->panel = 'sidebar';
  $pane->type = 'entity_field';
  $pane->subtype = 'taxonomy_term:field_curriculum_featured_image';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'image',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'image_style' => '',
      'image_link' => '',
    ),
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'd15e4654-6e8b-40f9-94ad-ed6899e1d6d8';
  $display->content['new-d15e4654-6e8b-40f9-94ad-ed6899e1d6d8'] = $pane;
  $display->panels['sidebar'][0] = 'new-d15e4654-6e8b-40f9-94ad-ed6899e1d6d8';
  $pane = new stdClass();
  $pane->pid = 'new-2579be16-0833-4a92-a4cf-8eb0d68080e6';
  $pane->panel = 'sidebar';
  $pane->type = 'views_panes';
  $pane->subtype = 'awards_links-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '2579be16-0833-4a92-a4cf-8eb0d68080e6';
  $display->content['new-2579be16-0833-4a92-a4cf-8eb0d68080e6'] = $pane;
  $display->panels['sidebar'][1] = 'new-2579be16-0833-4a92-a4cf-8eb0d68080e6';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = '0';
$panelizer->display = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'panelizer',
);
