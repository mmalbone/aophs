<?php
/**
 * @file
 * views_view.aop_family_entertainment_dvds_and_cds.inc
 */

$api = '2.0.0';

$data = $view = new view();
$view->name = 'aop_family_entertainment_dvds_and_cds';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'AOP Family Entertainment DVDs and CDs';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['disable_sql_rewrite'] = TRUE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'jcarousel';
$handler->display->display_options['style_options']['wrap'] = 'both';
$handler->display->display_options['style_options']['visible'] = '';
$handler->display->display_options['style_options']['auto'] = '0';
$handler->display->display_options['style_options']['autoPause'] = 1;
$handler->display->display_options['style_options']['easing'] = '';
$handler->display->display_options['style_options']['vertical'] = 0;
$handler->display->display_options['row_plugin'] = 'fields';
/* Relationship: Content: Referenced products */
$handler->display->display_options['relationships']['field_product_product_id']['id'] = 'field_product_product_id';
$handler->display->display_options['relationships']['field_product_product_id']['table'] = 'field_data_field_product';
$handler->display->display_options['relationships']['field_product_product_id']['field'] = 'field_product_product_id';
/* Field: Commerce Product: Product Image */
$handler->display->display_options['fields']['field_product_image']['id'] = 'field_product_image';
$handler->display->display_options['fields']['field_product_image']['table'] = 'field_data_field_product_image';
$handler->display->display_options['fields']['field_product_image']['field'] = 'field_product_image';
$handler->display->display_options['fields']['field_product_image']['relationship'] = 'field_product_product_id';
$handler->display->display_options['fields']['field_product_image']['label'] = '';
$handler->display->display_options['fields']['field_product_image']['element_type'] = '0';
$handler->display->display_options['fields']['field_product_image']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_product_image']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_product_image']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_product_image']['type'] = 'text_plain';
$handler->display->display_options['fields']['field_product_image']['field_api_classes'] = TRUE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['max_length'] = '35';
$handler->display->display_options['fields']['title']['alter']['trim'] = TRUE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['node_in_colorbox_width'] = '600';
$handler->display->display_options['fields']['title']['node_in_colorbox_height'] = '600';
$handler->display->display_options['fields']['title']['node_in_colorbox_rel'] = '';
/* Field: Commerce Product: Price */
$handler->display->display_options['fields']['commerce_price']['id'] = 'commerce_price';
$handler->display->display_options['fields']['commerce_price']['table'] = 'field_data_commerce_price';
$handler->display->display_options['fields']['commerce_price']['field'] = 'commerce_price';
$handler->display->display_options['fields']['commerce_price']['relationship'] = 'field_product_product_id';
$handler->display->display_options['fields']['commerce_price']['label'] = '';
$handler->display->display_options['fields']['commerce_price']['element_type'] = '0';
$handler->display->display_options['fields']['commerce_price']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['commerce_price']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['commerce_price']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['commerce_price']['click_sort_column'] = 'amount';
$handler->display->display_options['fields']['commerce_price']['type'] = 'commerce_price_savings_formatter_inline';
$handler->display->display_options['fields']['commerce_price']['settings'] = array(
  'calculation' => 'calculated_sell_price',
  'prices' => array(
    'list' => 'list',
    'price' => 'price',
    'savings' => 0,
  ),
  'savings' => 'amount',
  'show_labels' => 0,
);
$handler->display->display_options['fields']['commerce_price']['field_api_classes'] = TRUE;
/* Field: Commerce Product: Add to Cart form */
$handler->display->display_options['fields']['add_to_cart_form']['id'] = 'add_to_cart_form';
$handler->display->display_options['fields']['add_to_cart_form']['table'] = 'views_entity_commerce_product';
$handler->display->display_options['fields']['add_to_cart_form']['field'] = 'add_to_cart_form';
$handler->display->display_options['fields']['add_to_cart_form']['relationship'] = 'field_product_product_id';
$handler->display->display_options['fields']['add_to_cart_form']['label'] = '';
$handler->display->display_options['fields']['add_to_cart_form']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['add_to_cart_form']['show_quantity'] = 0;
$handler->display->display_options['fields']['add_to_cart_form']['default_quantity'] = '1';
$handler->display->display_options['fields']['add_to_cart_form']['combine'] = 1;
$handler->display->display_options['fields']['add_to_cart_form']['display_path'] = 0;
$handler->display->display_options['fields']['add_to_cart_form']['line_item_type'] = 'product';
/* Field: Commerce Product: SKU */
$handler->display->display_options['fields']['sku']['id'] = 'sku';
$handler->display->display_options['fields']['sku']['table'] = 'commerce_product';
$handler->display->display_options['fields']['sku']['field'] = 'sku';
$handler->display->display_options['fields']['sku']['relationship'] = 'field_product_product_id';
$handler->display->display_options['fields']['sku']['label'] = '';
$handler->display->display_options['fields']['sku']['exclude'] = TRUE;
$handler->display->display_options['fields']['sku']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['sku']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['sku']['link_to_product'] = 0;
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'dvd' => 'dvd',
);
/* Filter criterion: Commerce Product: DVD/CD (field_dvd_cd) */
$handler->display->display_options['filters']['field_dvd_cd_value']['id'] = 'field_dvd_cd_value';
$handler->display->display_options['filters']['field_dvd_cd_value']['table'] = 'field_data_field_dvd_cd';
$handler->display->display_options['filters']['field_dvd_cd_value']['field'] = 'field_dvd_cd_value';
$handler->display->display_options['filters']['field_dvd_cd_value']['relationship'] = 'field_product_product_id';
$handler->display->display_options['filters']['field_dvd_cd_value']['value'] = array(
  'cd' => 'cd',
);

/* Display: DVDs Childrens */
$handler = $view->new_display('block', 'DVDs Childrens', 'children_dvds_carousel_block');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'Childrens DVDs';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'dvd' => 'dvd',
);
/* Filter criterion: Commerce Product: DVD/CD (field_dvd_cd) */
$handler->display->display_options['filters']['field_dvd_cd_value']['id'] = 'field_dvd_cd_value';
$handler->display->display_options['filters']['field_dvd_cd_value']['table'] = 'field_data_field_dvd_cd';
$handler->display->display_options['filters']['field_dvd_cd_value']['field'] = 'field_dvd_cd_value';
$handler->display->display_options['filters']['field_dvd_cd_value']['relationship'] = 'field_product_product_id';
$handler->display->display_options['filters']['field_dvd_cd_value']['value'] = array(
  'dvd' => 'dvd',
);
/* Filter criterion: Commerce Product: DVD Category (field_dvd_category) */
$handler->display->display_options['filters']['field_dvd_category_tid']['id'] = 'field_dvd_category_tid';
$handler->display->display_options['filters']['field_dvd_category_tid']['table'] = 'field_data_field_dvd_category';
$handler->display->display_options['filters']['field_dvd_category_tid']['field'] = 'field_dvd_category_tid';
$handler->display->display_options['filters']['field_dvd_category_tid']['relationship'] = 'field_product_product_id';
$handler->display->display_options['filters']['field_dvd_category_tid']['value'] = array(
  0 => '111',
);
$handler->display->display_options['filters']['field_dvd_category_tid']['vocabulary'] = 'dvd_categories';
$handler->display->display_options['block_description'] = 'Childrens DVDs';

/* Display: CDs */
$handler = $view->new_display('block', 'CDs', 'cds_carousel_block');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'CDs';
$handler->display->display_options['block_description'] = 'CDs';

/* Display: DVDs Documentries */
$handler = $view->new_display('block', 'DVDs Documentries', 'documentries_dvds_carousel_block');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'Documentries DVDs';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'dvd' => 'dvd',
);
/* Filter criterion: Commerce Product: DVD/CD (field_dvd_cd) */
$handler->display->display_options['filters']['field_dvd_cd_value']['id'] = 'field_dvd_cd_value';
$handler->display->display_options['filters']['field_dvd_cd_value']['table'] = 'field_data_field_dvd_cd';
$handler->display->display_options['filters']['field_dvd_cd_value']['field'] = 'field_dvd_cd_value';
$handler->display->display_options['filters']['field_dvd_cd_value']['relationship'] = 'field_product_product_id';
$handler->display->display_options['filters']['field_dvd_cd_value']['value'] = array(
  'dvd' => 'dvd',
);
/* Filter criterion: Commerce Product: DVD Category (field_dvd_category) */
$handler->display->display_options['filters']['field_dvd_category_tid']['id'] = 'field_dvd_category_tid';
$handler->display->display_options['filters']['field_dvd_category_tid']['table'] = 'field_data_field_dvd_category';
$handler->display->display_options['filters']['field_dvd_category_tid']['field'] = 'field_dvd_category_tid';
$handler->display->display_options['filters']['field_dvd_category_tid']['relationship'] = 'field_product_product_id';
$handler->display->display_options['filters']['field_dvd_category_tid']['value'] = array(
  0 => '113',
);
$handler->display->display_options['filters']['field_dvd_category_tid']['vocabulary'] = 'dvd_categories';
$handler->display->display_options['block_description'] = 'Documentries DVDs';

/* Display: DVDs Drama */
$handler = $view->new_display('block', 'DVDs Drama', 'drama_dvds_carousel_block');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'Drama DVDs';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'dvd' => 'dvd',
);
/* Filter criterion: Commerce Product: DVD/CD (field_dvd_cd) */
$handler->display->display_options['filters']['field_dvd_cd_value']['id'] = 'field_dvd_cd_value';
$handler->display->display_options['filters']['field_dvd_cd_value']['table'] = 'field_data_field_dvd_cd';
$handler->display->display_options['filters']['field_dvd_cd_value']['field'] = 'field_dvd_cd_value';
$handler->display->display_options['filters']['field_dvd_cd_value']['relationship'] = 'field_product_product_id';
$handler->display->display_options['filters']['field_dvd_cd_value']['value'] = array(
  'dvd' => 'dvd',
);
/* Filter criterion: Commerce Product: DVD Category (field_dvd_category) */
$handler->display->display_options['filters']['field_dvd_category_tid']['id'] = 'field_dvd_category_tid';
$handler->display->display_options['filters']['field_dvd_category_tid']['table'] = 'field_data_field_dvd_category';
$handler->display->display_options['filters']['field_dvd_category_tid']['field'] = 'field_dvd_category_tid';
$handler->display->display_options['filters']['field_dvd_category_tid']['relationship'] = 'field_product_product_id';
$handler->display->display_options['filters']['field_dvd_category_tid']['value'] = array(
  0 => '112',
);
$handler->display->display_options['filters']['field_dvd_category_tid']['vocabulary'] = 'dvd_categories';
$handler->display->display_options['block_description'] = 'Drama DVDs';

/* Display: DVDs Educational - Teaching */
$handler = $view->new_display('block', 'DVDs Educational - Teaching', 'educational_dvds_carousel_block');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'Educational - Teaching DVDs';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'dvd' => 'dvd',
);
/* Filter criterion: Commerce Product: DVD/CD (field_dvd_cd) */
$handler->display->display_options['filters']['field_dvd_cd_value']['id'] = 'field_dvd_cd_value';
$handler->display->display_options['filters']['field_dvd_cd_value']['table'] = 'field_data_field_dvd_cd';
$handler->display->display_options['filters']['field_dvd_cd_value']['field'] = 'field_dvd_cd_value';
$handler->display->display_options['filters']['field_dvd_cd_value']['relationship'] = 'field_product_product_id';
$handler->display->display_options['filters']['field_dvd_cd_value']['value'] = array(
  'dvd' => 'dvd',
);
/* Filter criterion: Commerce Product: DVD Category (field_dvd_category) */
$handler->display->display_options['filters']['field_dvd_category_tid']['id'] = 'field_dvd_category_tid';
$handler->display->display_options['filters']['field_dvd_category_tid']['table'] = 'field_data_field_dvd_category';
$handler->display->display_options['filters']['field_dvd_category_tid']['field'] = 'field_dvd_category_tid';
$handler->display->display_options['filters']['field_dvd_category_tid']['relationship'] = 'field_product_product_id';
$handler->display->display_options['filters']['field_dvd_category_tid']['value'] = array(
  0 => '114',
);
$handler->display->display_options['filters']['field_dvd_category_tid']['vocabulary'] = 'dvd_categories';
$handler->display->display_options['block_description'] = 'Educational - Teaching DVDs';

/* Display: DVDs Inspirational */
$handler = $view->new_display('block', 'DVDs Inspirational', 'inspirational_dvds_carousel_block');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'Inspirational DVDs';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'dvd' => 'dvd',
);
/* Filter criterion: Commerce Product: DVD/CD (field_dvd_cd) */
$handler->display->display_options['filters']['field_dvd_cd_value']['id'] = 'field_dvd_cd_value';
$handler->display->display_options['filters']['field_dvd_cd_value']['table'] = 'field_data_field_dvd_cd';
$handler->display->display_options['filters']['field_dvd_cd_value']['field'] = 'field_dvd_cd_value';
$handler->display->display_options['filters']['field_dvd_cd_value']['relationship'] = 'field_product_product_id';
$handler->display->display_options['filters']['field_dvd_cd_value']['value'] = array(
  'dvd' => 'dvd',
);
/* Filter criterion: Commerce Product: DVD Category (field_dvd_category) */
$handler->display->display_options['filters']['field_dvd_category_tid']['id'] = 'field_dvd_category_tid';
$handler->display->display_options['filters']['field_dvd_category_tid']['table'] = 'field_data_field_dvd_category';
$handler->display->display_options['filters']['field_dvd_category_tid']['field'] = 'field_dvd_category_tid';
$handler->display->display_options['filters']['field_dvd_category_tid']['relationship'] = 'field_product_product_id';
$handler->display->display_options['filters']['field_dvd_category_tid']['value'] = array(
  0 => '115',
);
$handler->display->display_options['filters']['field_dvd_category_tid']['expose']['operator_id'] = 'field_dvd_category_tid_op';
$handler->display->display_options['filters']['field_dvd_category_tid']['expose']['label'] = 'DVD Category (field_dvd_category)';
$handler->display->display_options['filters']['field_dvd_category_tid']['expose']['operator'] = 'field_dvd_category_tid_op';
$handler->display->display_options['filters']['field_dvd_category_tid']['expose']['identifier'] = 'field_dvd_category_tid';
$handler->display->display_options['filters']['field_dvd_category_tid']['vocabulary'] = 'dvd_categories';
$handler->display->display_options['block_description'] = 'Inspirational DVDs';


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'views',
  1 => 'node',
  2 => 'text',
  3 => 'commerce_price',
);
