<?php
/**
 * @file
 * permission.edit_any_aop_subscription_content.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'edit any aop_subscription content',
  'roles' => array(
    0 => 'administrator',
    1 => 'oauth',
  ),
);

$dependencies = array(
  'content_type.aop_subscription' => 'content_type.aop_subscription',
);

$optional = array();

$modules = array(
  0 => 'node',
);
