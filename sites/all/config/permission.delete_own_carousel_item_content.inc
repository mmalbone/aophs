<?php
/**
 * @file
 * permission.delete_own_carousel_item_content.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'delete own carousel_item content',
  'roles' => array(),
);

$dependencies = array(
  'content_type.carousel_item' => 'content_type.carousel_item',
);

$optional = array();

$modules = array(
  0 => 'node',
);
