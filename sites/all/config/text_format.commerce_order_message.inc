<?php
/**
 * @file
 * text_format.commerce_order_message.inc
 */

$api = '2.0.0';

$data = (object) array(
  'format' => 'commerce_order_message',
  'name' => 'Commerce Order Message',
  'cache' => '1',
  'status' => '1',
  'weight' => '-6',
  'filters' => array(
    'filter_html' => array(
      'weight' => '1',
      'status' => '1',
      'settings' => array(
        'allowed_html' => '<a> <em> <strong> <cite> <blockquote> <code> <ul> <ol> <li> <dl> <dt> <dd>',
        'filter_html_help' => 1,
        'filter_html_nofollow' => 0,
      ),
    ),
  ),
);

$dependencies = array();

$optional = array(
  'permission.use_text_format_commerce_order_message' => 'permission.use_text_format_commerce_order_message',
  'wysiwyg.commerce_order_message' => 'wysiwyg.commerce_order_message',
);

$modules = array(
  0 => 'filter',
);
