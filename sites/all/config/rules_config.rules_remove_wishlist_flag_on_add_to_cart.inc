<?php
/**
 * @file
 * rules_config.rules_remove_wishlist_flag_on_add_to_cart.inc
 */

$api = '2.0.0';

$data = entity_import('rules_config', '{ "rules_remove_wishlist_flag_on_add_to_cart" : {
      "LABEL" : "Remove Wishlist Flag on Add to Cart",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "flag", "commerce_cart" ],
      "ON" : { "commerce_cart_product_add" : [] },
      "IF" : [
        { "flag_flagged_commerce_product" : {
            "flag" : "wishlist",
            "commerce_product" : [ "commerce_product" ],
            "flagging_user" : [ "site:current-user" ]
          }
        }
      ],
      "DO" : [
        { "flag_unflagcommerce_product" : {
            "flag" : "wishlist",
            "commerce_product" : [ "commerce_product" ],
            "flagging_user" : [ "site:current-user" ],
            "permission_check" : 1
          }
        }
      ]
    }
  }');

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'entity',
  1 => 'commerce_cart',
  2 => 'flag',
  3 => 'rules',
);
