<?php
/**
 * @file
 * page_manager_handlers.page_family_entertainment_dvds_and_cds_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_family_entertainment_dvds_and_cds_panel_context';
$handler->task = 'page';
$handler->subtask = 'family_entertainment_dvds_and_cds';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => 'family-entertainment',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'flexible:aop_1_1_1_2_layout';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'center' => NULL,
    'center_' => NULL,
    'center__' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = 'Family Entertainment';
$display->uuid = 'ed9dc72e-1a3d-41cc-9050-ab02b338fd30';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-6652c333-b80d-422f-ba16-82c54aa24dab';
  $pane->panel = 'center__';
  $pane->type = 'views';
  $pane->subtype = 'aop_family_entertainment_dvds_and_cds';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 0,
    'nodes_per_page' => '0',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'cds_carousel_block',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '6652c333-b80d-422f-ba16-82c54aa24dab';
  $display->content['new-6652c333-b80d-422f-ba16-82c54aa24dab'] = $pane;
  $display->panels['center__'][0] = 'new-6652c333-b80d-422f-ba16-82c54aa24dab';
  $pane = new stdClass();
  $pane->pid = 'new-41ac89d7-fc7e-4e7b-8247-094fa525f007';
  $pane->panel = 'center__';
  $pane->type = 'views';
  $pane->subtype = 'aop_family_entertainment_dvds_and_cds';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 0,
    'nodes_per_page' => '0',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'children_dvds_carousel_block',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '41ac89d7-fc7e-4e7b-8247-094fa525f007';
  $display->content['new-41ac89d7-fc7e-4e7b-8247-094fa525f007'] = $pane;
  $display->panels['center__'][1] = 'new-41ac89d7-fc7e-4e7b-8247-094fa525f007';
  $pane = new stdClass();
  $pane->pid = 'new-1ab56e7f-65ca-417c-a47c-54377a06ab34';
  $pane->panel = 'center__';
  $pane->type = 'views';
  $pane->subtype = 'aop_family_entertainment_dvds_and_cds';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 0,
    'nodes_per_page' => '0',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'documentries_dvds_carousel_block',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '1ab56e7f-65ca-417c-a47c-54377a06ab34';
  $display->content['new-1ab56e7f-65ca-417c-a47c-54377a06ab34'] = $pane;
  $display->panels['center__'][2] = 'new-1ab56e7f-65ca-417c-a47c-54377a06ab34';
  $pane = new stdClass();
  $pane->pid = 'new-7f640e8f-2308-441a-a6e1-fe3e87084402';
  $pane->panel = 'center__';
  $pane->type = 'views';
  $pane->subtype = 'aop_family_entertainment_dvds_and_cds';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 0,
    'nodes_per_page' => '0',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'drama_dvds_carousel_block',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = '7f640e8f-2308-441a-a6e1-fe3e87084402';
  $display->content['new-7f640e8f-2308-441a-a6e1-fe3e87084402'] = $pane;
  $display->panels['center__'][3] = 'new-7f640e8f-2308-441a-a6e1-fe3e87084402';
  $pane = new stdClass();
  $pane->pid = 'new-d3514ee4-71a8-434a-8c0a-b68a0a3f4c5f';
  $pane->panel = 'center__';
  $pane->type = 'views';
  $pane->subtype = 'aop_family_entertainment_dvds_and_cds';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 0,
    'nodes_per_page' => '0',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'educational_dvds_carousel_block',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = 'd3514ee4-71a8-434a-8c0a-b68a0a3f4c5f';
  $display->content['new-d3514ee4-71a8-434a-8c0a-b68a0a3f4c5f'] = $pane;
  $display->panels['center__'][4] = 'new-d3514ee4-71a8-434a-8c0a-b68a0a3f4c5f';
  $pane = new stdClass();
  $pane->pid = 'new-11925d75-439e-4d59-bb27-87df815b979f';
  $pane->panel = 'center__';
  $pane->type = 'views';
  $pane->subtype = 'aop_family_entertainment_dvds_and_cds';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 0,
    'nodes_per_page' => '0',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'inspirational_dvds_carousel_block',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 5;
  $pane->locks = array();
  $pane->uuid = '11925d75-439e-4d59-bb27-87df815b979f';
  $display->content['new-11925d75-439e-4d59-bb27-87df815b979f'] = $pane;
  $display->panels['center__'][5] = 'new-11925d75-439e-4d59-bb27-87df815b979f';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = 'new-6652c333-b80d-422f-ba16-82c54aa24dab';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.aop_family_entertainment_dvds_and_cds' => 'views_view.aop_family_entertainment_dvds_and_cds',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
