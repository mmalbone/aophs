<?php
/**
 * @file
 * page_manager_handlers.page_christian_liberty_academy_class_plan_vs_horizons_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_christian_liberty_academy_class_plan_vs_horizons_panel_context';
$handler->task = 'page';
$handler->subtask = 'christian_liberty_academy_class_plan_vs_horizons';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '8f4050d7-23ae-4475-bcf1-59d2111f304f';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-1c51d619-761b-454d-aa8b-2ca6e7a7a328';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Christian Liberty Academy CLASS Plan vs. Horizons',
    'body' => '<p>Investigating the differences between Christian Liberty Academy\'s CLASS Plan and Horizons curriculum from Alpha Omega Publications? We have just the information you need! Find short overviews of each curriculum below, an easy-to-view comparison chart, and additional comparison options!</p><h3><strong>Christian Liberty Academy CLASS Plan</strong></h3><p>Christian Liberty Press, a publisher of Christ-centered educational materials, supplies families with materials and services created to enable parents to provide a quality Christian education at home. The Christian Liberty Academy CLASS Plan is a distance learning approach to homeschooling that ensures academic accountability while still providing a level of flexibility and independence. Families enrolled in the CLASS plan are entitled to initial placement testing, appropriate grade-level curriculum, alternative course options, all necessary guidance and administrative materials, annual basic skills testing, grading and record-keeping services, year-round phone assistance, diplomas, report cards, and transcript services.</p><p>Written from a Christian worldview, all Christian Liberty courses are completely Bible-based. Science courses for all grade levels are based on a biblical view of creation and the origin of life. Christian Liberty courses include Bible, language arts, literature, history, mathematics, science, and a variety of electives. Tuition costs include all materials and services. Students have twelve months from the time of enrollment to complete all coursework. All materials remain the property of the homeschool family. Reusable book credits are available for some materials that are able to be reused by younger siblings. Christian Liberty Academy\'s programs are accredited.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>Are you wondering how to find the most effective homeschooling method for your family? Keep in mind that there is no one perfect homeschool curriculum. A blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Instead of feeling torn between curriculum formats, parents should be open to mixing and matching materials and resources to customize their child\'s education. Remember, each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3><strong>Horizons from Alpha Omega Publications</strong></h3><p>Horizons is a preK-12 print-based curriculum offered by Alpha Omega Publications, a Christian-based publisher that provides curriculum and educational services to homeschool families. Filled with engaging, full-color illustrations, Horizons is a child-friendly workbook-based curriculum with consumable lessons and hands-on activities. Horizons curriculum encourages concept mastery through a spiral-based learning process of skill/concept presentation, review, and reinforcement. Teacher\'s guides offer clear, step-by-step lesson plans and activities that are suitable for both homeschool and classroom settings.</p><p>Winner of Cathy Duffy\'s 100 Top Picks for Homeschool Curriculum, Horizons integrates Scripture and Christian living throughout all subjects. Horizons workbooks provide removable student pages for ease of student use. Horizons curriculum includes courses in math (K-8), penmanship (1-5), health (K-8), physical education (PreK-12), phonics and reading (K-3), and spelling and vocabulary (1-3). Health and physical education electives were designed for classroom use and may need to be adapted to homeschool use. Complete Horizons Preschool Curriculum Sets and Multimedia Sets are also available. Horizons curriculum sets include all student and teacher materials. Student and teacher materials can also be purchased separately.</p><h3><strong>Benefits and Features of Horizons from Alpha Omega Publications</strong></h3><p class="list_heading">Horizons</p><ul><li>brightly illustrated, hands-on, consumable curriculum</li><li>offers fun lessons and manipulative-based activities</li><li>teaches concepts through a spiral learning process</li><li>encourages concept mastery through quick concept introduction, review, and reinforcement</li></ul><h3 class="para-start"><strong>Take a Closer Look at A Beka and Alpha Omega Publications</strong></h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">Horizons</th><th class="compare">Christian Liberty Academy CLASS Plan</th></tr>
</thead>
<tbody>
<tr>
<td>K-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Core subjects and electives available</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Instruction provided by parent</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Interactive, one-on-one learning format</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Readiness tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Flexible scheduling and pacing</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Manageable amount of daily lesson material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Easy-to-use teacher\'s materials</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Material designed for homeschoolers</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Requires materials to be returned</td>
<td class="tac">&nbsp;</td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Ability to pick and choose student and teacher materials</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr><th class="features">Cost</th><th class="compare">Alpha Omega Publications</th><th class="compare">Christian Liberty Press</th></tr>
</thead>
<tbody>
<tr>
<td>Average yearly cost per grade level**</td>
<td class="tac">N/A</td>
<td class="tac">$555</td>
</tr>
<tr class="alt">
<td>Average subject cost per grade level**</td>
<td class="tac">$79.95</td>
<td class="tac">N/A</td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: Christian Liberty Academy/Press versus Alpha Omega Publications</h4>
<p class="list_heading_links">Christian Liberty Press</p>
<ul>
<li><a href="/christian-liberty-press-vs-monarch">Christian Liberty Press vs. Monarch</a></li>
<li><a href="/christian-liberty-press-vs-aoa">Christian Liberty Press vs. Alpha Omega Academy</a></li>
<li><a href="/christian-liberty-press-vs-horizons">Christian Liberty Press vs. Horizons</a></li>
<li><a href="/christian-liberty-press-vs-lifepac">Christian Liberty Press vs. LIFEPAC</a></li>
<li><a href="/christian-liberty-press-vs-sos">Christian Liberty Press vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">Christian Liberty Academy (CLASS Plan) </p>
<ul>
<li><a href="/christian-liberty-academy-class-vs-monarch">Christian Liberty Academy (CLASS Plan) vs. Monarch</a></li>
<li><a href="/christian-liberty-academy-class-vs-aoa">Christian Liberty Academy (CLASS Plan) vs. Alpha Omega Academy</a></li>
<li><a href="/christian-liberty-academy-class-vs-horizons">Christian Liberty Academy (CLASS Plan) vs. Horizons</a></li>
<li><a href="/christian-liberty-academy-class-vs-lifepac">Christian Liberty Academy (CLASS Plan) vs. LIFEPAC</a></li>
<li><a href="/christian-liberty-academy-class-vs-sos">Christian Liberty Academy (CLASS Plan) vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">Christian Liberty Academy (Family Plan) </p>
<ul>
<li><a href="/christian-liberty-academy-family-vs-monarch">Christian Liberty Academy (Family Plan) vs. Monarch</a></li>
<li><a href="/christian-liberty-academy-family-vs-aoa">Christian Liberty Academy (Family Plan) vs. Alpha Omega Academy</a></li>
<li><a href="/christian-liberty-academy-family-vs-horizons">Christian Liberty Academy (Family Plan) vs. Horizons</a></li>
<li><a href="/christian-liberty-academy-family-vs-lifepac">Christian Liberty Academy (Family Plan) vs. LIFEPAC</a></li>
<li><a href="/christian-liberty-academy-family-vs-sos">Christian Liberty Academy (Family Plan) vs. Switched-On Schoolhouse</a></li>
</ul>
<p>Christian Liberty Press is the publishing arm of Christian Liberty Academy and Christian Liberty Academy School System (CLASS). Alpha Omega Publications is not in any way affiliated with Christian Liberty Press. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by Christian Liberty Academy.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p><p>**Prices obtained February 2013 from Christian Liberty Press and the CLASS Homeschools website and from Alpha Omega Publications 2013 Homeschool Catalog. Yearly prices based on comparison between Horizons full time enrollment (not available) and the Christian Liberty Academy CLASS plan full time enrollment for a 2nd-8th grade student. Individual subject prices are based on comparison between costs for Horizons 5th grade math set and Christian Liberty Academy single subject cost (not available). Costs do not reflect any additional fees or shipping and handling charges. Prices for other Christian Liberty Press and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '1c51d619-761b-454d-aa8b-2ca6e7a7a328';
  $display->content['new-1c51d619-761b-454d-aa8b-2ca6e7a7a328'] = $pane;
  $display->panels['left'][0] = 'new-1c51d619-761b-454d-aa8b-2ca6e7a7a328';
  $pane = new stdClass();
  $pane->pid = 'new-d3d77dfa-d2ca-4a35-af89-cee20ec5a699';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'd3d77dfa-d2ca-4a35-af89-cee20ec5a699';
  $display->content['new-d3d77dfa-d2ca-4a35-af89-cee20ec5a699'] = $pane;
  $display->panels['right'][0] = 'new-d3d77dfa-d2ca-4a35-af89-cee20ec5a699';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-1c51d619-761b-454d-aa8b-2ca6e7a7a328';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
