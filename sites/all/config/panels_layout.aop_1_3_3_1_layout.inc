<?php
/**
 * @file
 * panels_layout.aop_1_3_3_1_layout.inc
 */

$api = '2.0.0';

$data = $layout = new stdClass();
$layout->disabled = FALSE; /* Edit this to true to make a default layout disabled initially */
$layout->api_version = 1;
$layout->name = 'aop_1_3_3_1_layout';
$layout->admin_title = 'AOP 1-3-3-2-1 Layout';
$layout->admin_description = 'AOP 1-3-3-1-2 layout';
$layout->category = 'AOP';
$layout->plugin = 'flexible';
$layout->settings = array(
  'items' => array(
    'canvas' => array(
      'type' => 'row',
      'contains' => 'column',
      'children' => array(
        0 => 'main',
      ),
      'parent' => NULL,
      'class' => '',
      'column_class' => '',
      'row_class' => '',
      'region_class' => '',
      'no_scale' => TRUE,
      'fixed_width' => '',
      'column_separation' => '0.5em',
      'region_separation' => '0.5em',
      'row_separation' => '0.5em',
    ),
    'main' => array(
      'type' => 'column',
      'width' => 100,
      'width_type' => '%',
      'children' => array(
        0 => 'main-row',
        1 => 1,
        2 => 2,
        3 => 3,
        4 => 4,
      ),
      'parent' => 'canvas',
    ),
    'main-row' => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'center',
      ),
      'parent' => 'main',
    ),
    'center' => array(
      'type' => 'region',
      'title' => 'Center',
      'width' => 100,
      'width_type' => '%',
      'parent' => 'main-row',
    ),
    1 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'left',
        1 => 'center_',
        2 => 'right',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    'left' => array(
      'type' => 'region',
      'title' => 'Left',
      'width' => '33.354353433093586',
      'width_type' => '%',
      'parent' => '1',
      'class' => '',
    ),
    'center_' => array(
      'type' => 'region',
      'title' => 'Center',
      'width' => '33.34607609257872',
      'width_type' => '%',
      'parent' => '1',
      'class' => '',
    ),
    'right' => array(
      'type' => 'region',
      'title' => 'Right',
      'width' => '33.299570474327695',
      'width_type' => '%',
      'parent' => '1',
      'class' => '',
    ),
    2 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'left_',
        1 => 'center__',
        2 => 'right_',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    'left_' => array(
      'type' => 'region',
      'title' => 'Left',
      'width' => '33.35998335998336',
      'width_type' => '%',
      'parent' => '2',
      'class' => '',
    ),
    'center__' => array(
      'type' => 'region',
      'title' => 'Center',
      'width' => '33.31890423322518',
      'width_type' => '%',
      'parent' => '2',
      'class' => '',
    ),
    'right_' => array(
      'type' => 'region',
      'title' => 'Right',
      'width' => '33.32111240679146',
      'width_type' => '%',
      'parent' => '2',
      'class' => '',
    ),
    3 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'center___',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    'center___' => array(
      'type' => 'region',
      'title' => 'Center',
      'width' => 100,
      'width_type' => '%',
      'parent' => '3',
      'class' => '',
    ),
    4 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'center____',
        1 => 'right__',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    'center____' => array(
      'type' => 'region',
      'title' => 'Left',
      'width' => 50,
      'width_type' => '%',
      'parent' => '4',
      'class' => '',
    ),
    'right__' => array(
      'type' => 'region',
      'title' => 'Right',
      'width' => 50,
      'width_type' => '%',
      'parent' => '4',
      'class' => '',
    ),
  ),
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'panels',
);
