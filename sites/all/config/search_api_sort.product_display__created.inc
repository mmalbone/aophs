<?php
/**
 * @file
 * search_api_sort.product_display__created.inc
 */

$api = '2.0.0';

$data = entity_import('search_api_sort', '{
    "index_id" : "product_display",
    "field" : "created",
    "name" : "Latest",
    "enabled" : "1",
    "weight" : "20",
    "identifier" : "product_display__created",
    "default_sort" : "0",
    "default_sort_no_terms" : "0",
    "default_order" : "asc",
    "options" : { "field_name" : "Date created" }
  }');

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'entity',
  1 => 'search_api_sorts',
);
