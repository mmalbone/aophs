<?php
/**
 * @file
 * facetapi.search_api_product_display_block_field_collection.inc
 */

$api = '2.0.0';

$data = $facet = new stdClass();
$facet->disabled = FALSE; /* Edit this to true to make a default facet disabled initially */
$facet->api_version = 1;
$facet->name = 'search_api@product_display:block:field_collection';
$facet->searcher = 'search_api@product_display';
$facet->realm = 'block';
$facet->facet = 'field_collection';
$facet->enabled = TRUE;
$facet->settings = array(
  'weight' => 0,
  'widget' => 'facetapi_checkbox_links',
  'filters' => array(
    'active_items' => array(
      'status' => 1,
      'weight' => -1,
    ),
  ),
  'active_sorts' => array(
    'active' => 'active',
    'count' => 'count',
    'display' => 'display',
  ),
  'sort_weight' => array(
    'active' => -50,
    'count' => -49,
    'display' => -48,
  ),
  'sort_order' => array(
    'active' => 3,
    'count' => 3,
    'display' => 4,
  ),
  'empty_behavior' => 'none',
  'soft_limit' => 20,
  'nofollow' => 1,
  'show_expanded' => 0,
  'facet_more_text' => 'Show more',
  'facet_fewer_text' => 'Show fewer',
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'facetapi',
);
