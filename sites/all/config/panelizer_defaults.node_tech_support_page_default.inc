<?php
/**
 * @file
 * panelizer_defaults.node_tech_support_page_default.inc
 */

$api = '2.0.0';

$data = $panelizer = new stdClass();
$panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
$panelizer->api_version = 1;
$panelizer->name = 'node:tech_support_page:default';
$panelizer->title = 'Default';
$panelizer->panelizer_type = 'node';
$panelizer->panelizer_key = 'tech_support_page';
$panelizer->no_blocks = FALSE;
$panelizer->css_id = '';
$panelizer->css = '';
$panelizer->pipeline = 'standard';
$panelizer->contexts = array();
$panelizer->relationships = array();
$panelizer->access = array();
$panelizer->view_mode = 'page_manager';
$panelizer->css_class = '';
$panelizer->title_element = 'H2';
$panelizer->link_to_entity = TRUE;
$panelizer->extra = array();
$display = new panels_display();
$display->layout = 'flexible';
$display->layout_settings = array();
$display->panel_settings = array();
$display->cache = array();
$display->title = '%node:title';
$display->uuid = '34c02370-57cc-4cd2-9573-1d6524fc71b6';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-f101cbeb-94fd-4198-b360-01f7b4b89f67';
  $pane->panel = 'center';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:title_field';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'hidden',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'f101cbeb-94fd-4198-b360-01f7b4b89f67';
  $display->content['new-f101cbeb-94fd-4198-b360-01f7b4b89f67'] = $pane;
  $display->panels['center'][0] = 'new-f101cbeb-94fd-4198-b360-01f7b4b89f67';
  $pane = new stdClass();
  $pane->pid = 'new-bcd09b98-888a-4f3a-a387-6701e4043f61';
  $pane->panel = 'center';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'text_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'bcd09b98-888a-4f3a-a387-6701e4043f61';
  $display->content['new-bcd09b98-888a-4f3a-a387-6701e4043f61'] = $pane;
  $display->panels['center'][1] = 'new-bcd09b98-888a-4f3a-a387-6701e4043f61';
  $pane = new stdClass();
  $pane->pid = 'new-cb30cdad-ede7-4ed9-ad20-049a479a834d';
  $pane->panel = 'center';
  $pane->type = 'node_links';
  $pane->subtype = 'node_links';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => FALSE,
    'override_title_text' => '',
    'build_mode' => 'page_manager',
    'identifier' => '',
    'link' => TRUE,
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array(
    'css_class' => 'link-wrapper',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'cb30cdad-ede7-4ed9-ad20-049a479a834d';
  $display->content['new-cb30cdad-ede7-4ed9-ad20-049a479a834d'] = $pane;
  $display->panels['center'][2] = 'new-cb30cdad-ede7-4ed9-ad20-049a479a834d';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = 'new-cb30cdad-ede7-4ed9-ad20-049a479a834d';
$panelizer->display = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'panelizer',
);
