<?php
/**
 * @file
 * views_view.merchant_xml.inc
 */

$api = '2.0.0';

$data = $view = new view();
$view->name = 'merchant_xml';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Merchant XML';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Merchant XML';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['style_plugin'] = 'table';
/* Relationship: Content: Referenced products */
$handler->display->display_options['relationships']['field_product_product_id']['id'] = 'field_product_product_id';
$handler->display->display_options['relationships']['field_product_product_id']['table'] = 'field_data_field_product';
$handler->display->display_options['relationships']['field_product_product_id']['field'] = 'field_product_product_id';
$handler->display->display_options['relationships']['field_product_product_id']['label'] = 'Referenced Product';
$handler->display->display_options['relationships']['field_product_product_id']['required'] = TRUE;
/* Field: Commerce Product: SKU */
$handler->display->display_options['fields']['sku']['id'] = 'sku';
$handler->display->display_options['fields']['sku']['table'] = 'commerce_product';
$handler->display->display_options['fields']['sku']['field'] = 'sku';
$handler->display->display_options['fields']['sku']['relationship'] = 'field_product_product_id';
$handler->display->display_options['fields']['sku']['label'] = 'g:id';
$handler->display->display_options['fields']['sku']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['sku']['link_to_product'] = 0;
/* Field: Commerce Product: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'commerce_product';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['relationship'] = 'field_product_product_id';
$handler->display->display_options['fields']['title']['label'] = 'g:title';
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['link_to_product'] = 0;
/* Field: Commerce Product: HSC Short Description */
$handler->display->display_options['fields']['field_hsc_short_description']['id'] = 'field_hsc_short_description';
$handler->display->display_options['fields']['field_hsc_short_description']['table'] = 'field_data_field_hsc_short_description';
$handler->display->display_options['fields']['field_hsc_short_description']['field'] = 'field_hsc_short_description';
$handler->display->display_options['fields']['field_hsc_short_description']['relationship'] = 'field_product_product_id';
$handler->display->display_options['fields']['field_hsc_short_description']['label'] = 'g:description';
$handler->display->display_options['fields']['field_hsc_short_description']['alter']['trim_whitespace'] = TRUE;
$handler->display->display_options['fields']['field_hsc_short_description']['alter']['strip_tags'] = TRUE;
$handler->display->display_options['fields']['field_hsc_short_description']['element_type'] = '0';
$handler->display->display_options['fields']['field_hsc_short_description']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_hsc_short_description']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_hsc_short_description']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_hsc_short_description']['field_api_classes'] = TRUE;
/* Field: Content: Path */
$handler->display->display_options['fields']['path']['id'] = 'path';
$handler->display->display_options['fields']['path']['table'] = 'node';
$handler->display->display_options['fields']['path']['field'] = 'path';
$handler->display->display_options['fields']['path']['label'] = 'g:link';
$handler->display->display_options['fields']['path']['alter']['trim_whitespace'] = TRUE;
$handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['path']['absolute'] = TRUE;
/* Field: Commerce Product: Product Image */
$handler->display->display_options['fields']['field_product_image']['id'] = 'field_product_image';
$handler->display->display_options['fields']['field_product_image']['table'] = 'field_data_field_product_image';
$handler->display->display_options['fields']['field_product_image']['field'] = 'field_product_image';
$handler->display->display_options['fields']['field_product_image']['relationship'] = 'field_product_product_id';
$handler->display->display_options['fields']['field_product_image']['label'] = 'g:image_link';
$handler->display->display_options['fields']['field_product_image']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_product_image']['alter']['text'] = 'https://s3.amazonaws.com/glnnewmedia/media/catalog/product[field_product_image]';
$handler->display->display_options['fields']['field_product_image']['alter']['strip_tags'] = TRUE;
$handler->display->display_options['fields']['field_product_image']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_product_image']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_product_image']['type'] = 'text_plain';
$handler->display->display_options['fields']['field_product_image']['settings'] = array(
  'skip_safe' => 0,
  'skip_empty_values' => 0,
);
$handler->display->display_options['fields']['field_product_image']['field_api_classes'] = TRUE;
/* Field: Global: Custom text */
$handler->display->display_options['fields']['nothing']['id'] = 'nothing';
$handler->display->display_options['fields']['nothing']['table'] = 'views';
$handler->display->display_options['fields']['nothing']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing']['label'] = 'g:condition';
$handler->display->display_options['fields']['nothing']['alter']['text'] = 'new';
$handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
/* Field: Global: Custom text */
$handler->display->display_options['fields']['nothing_1']['id'] = 'nothing_1';
$handler->display->display_options['fields']['nothing_1']['table'] = 'views';
$handler->display->display_options['fields']['nothing_1']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing_1']['label'] = 'g:availability';
$handler->display->display_options['fields']['nothing_1']['alter']['text'] = 'in stock';
$handler->display->display_options['fields']['nothing_1']['element_label_colon'] = FALSE;
/* Field: Commerce Product: Price */
$handler->display->display_options['fields']['commerce_price']['id'] = 'commerce_price';
$handler->display->display_options['fields']['commerce_price']['table'] = 'field_data_commerce_price';
$handler->display->display_options['fields']['commerce_price']['field'] = 'commerce_price';
$handler->display->display_options['fields']['commerce_price']['relationship'] = 'field_product_product_id';
$handler->display->display_options['fields']['commerce_price']['label'] = 'g:price';
$handler->display->display_options['fields']['commerce_price']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['commerce_price']['alter']['text'] = '[commerce_price] USD';
$handler->display->display_options['fields']['commerce_price']['alter']['trim_whitespace'] = TRUE;
$handler->display->display_options['fields']['commerce_price']['alter']['strip_tags'] = TRUE;
$handler->display->display_options['fields']['commerce_price']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['commerce_price']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['commerce_price']['click_sort_column'] = 'amount';
$handler->display->display_options['fields']['commerce_price']['type'] = 'commerce_price_no_decimals';
$handler->display->display_options['fields']['commerce_price']['settings'] = array(
  'calculation' => TRUE,
  'alternative_text_for_zero_price' => '',
  'text_format' => 'plain_text',
  'raw' => 1,
);
$handler->display->display_options['fields']['commerce_price']['field_api_classes'] = TRUE;
/* Field: Commerce Product: isbn */
$handler->display->display_options['fields']['field_isbn1']['id'] = 'field_isbn1';
$handler->display->display_options['fields']['field_isbn1']['table'] = 'field_data_field_isbn1';
$handler->display->display_options['fields']['field_isbn1']['field'] = 'field_isbn1';
$handler->display->display_options['fields']['field_isbn1']['relationship'] = 'field_product_product_id';
$handler->display->display_options['fields']['field_isbn1']['label'] = 'g:gtin';
$handler->display->display_options['fields']['field_isbn1']['alter']['strip_tags'] = TRUE;
$handler->display->display_options['fields']['field_isbn1']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_isbn1']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_isbn1']['hide_empty'] = TRUE;
$handler->display->display_options['fields']['field_isbn1']['type'] = 'text_plain';
$handler->display->display_options['fields']['field_isbn1']['field_api_classes'] = TRUE;
/* Field: Commerce Product: upc */
$handler->display->display_options['fields']['field_upc1']['id'] = 'field_upc1';
$handler->display->display_options['fields']['field_upc1']['table'] = 'field_data_field_upc1';
$handler->display->display_options['fields']['field_upc1']['field'] = 'field_upc1';
$handler->display->display_options['fields']['field_upc1']['relationship'] = 'field_product_product_id';
$handler->display->display_options['fields']['field_upc1']['label'] = 'g:gtin';
$handler->display->display_options['fields']['field_upc1']['alter']['strip_tags'] = TRUE;
$handler->display->display_options['fields']['field_upc1']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_upc1']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_upc1']['hide_empty'] = TRUE;
$handler->display->display_options['fields']['field_upc1']['type'] = 'text_plain';
$handler->display->display_options['fields']['field_upc1']['field_api_classes'] = TRUE;
/* Field: Commerce Product: Physical Weight (for shipping) */
$handler->display->display_options['fields']['field_physical_weight_for_shippi']['id'] = 'field_physical_weight_for_shippi';
$handler->display->display_options['fields']['field_physical_weight_for_shippi']['table'] = 'field_data_field_physical_weight_for_shippi';
$handler->display->display_options['fields']['field_physical_weight_for_shippi']['field'] = 'field_physical_weight_for_shippi';
$handler->display->display_options['fields']['field_physical_weight_for_shippi']['relationship'] = 'field_product_product_id';
$handler->display->display_options['fields']['field_physical_weight_for_shippi']['label'] = 'g:shipping_weight';
$handler->display->display_options['fields']['field_physical_weight_for_shippi']['alter']['strip_tags'] = TRUE;
$handler->display->display_options['fields']['field_physical_weight_for_shippi']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_physical_weight_for_shippi']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_physical_weight_for_shippi']['click_sort_column'] = 'weight';
$handler->display->display_options['fields']['field_physical_weight_for_shippi']['field_api_classes'] = TRUE;
$handler->display->display_options['filter_groups']['groups'] = array(
  1 => 'AND',
  2 => 'OR',
);
/* Filter criterion: Commerce Product: Status */
$handler->display->display_options['filters']['status_1']['id'] = 'status_1';
$handler->display->display_options['filters']['status_1']['table'] = 'commerce_product';
$handler->display->display_options['filters']['status_1']['field'] = 'status';
$handler->display->display_options['filters']['status_1']['relationship'] = 'field_product_product_id';
$handler->display->display_options['filters']['status_1']['value'] = '1';
$handler->display->display_options['filters']['status_1']['group'] = 1;
/* Filter criterion: Commerce Product: isbn (field_isbn1) */
$handler->display->display_options['filters']['field_isbn1_value']['id'] = 'field_isbn1_value';
$handler->display->display_options['filters']['field_isbn1_value']['table'] = 'field_data_field_isbn1';
$handler->display->display_options['filters']['field_isbn1_value']['field'] = 'field_isbn1_value';
$handler->display->display_options['filters']['field_isbn1_value']['relationship'] = 'field_product_product_id';
$handler->display->display_options['filters']['field_isbn1_value']['operator'] = 'not empty';
$handler->display->display_options['filters']['field_isbn1_value']['group'] = 2;
/* Filter criterion: Commerce Product: upc (field_upc1) */
$handler->display->display_options['filters']['field_upc1_value']['id'] = 'field_upc1_value';
$handler->display->display_options['filters']['field_upc1_value']['table'] = 'field_data_field_upc1';
$handler->display->display_options['filters']['field_upc1_value']['field'] = 'field_upc1_value';
$handler->display->display_options['filters']['field_upc1_value']['relationship'] = 'field_product_product_id';
$handler->display->display_options['filters']['field_upc1_value']['operator'] = 'not empty';
$handler->display->display_options['filters']['field_upc1_value']['group'] = 2;

/* Display: Merchant XML */
$handler = $view->new_display('views_data_export', 'Merchant XML', 'views_data_export_1');
$handler->display->display_options['display_description'] = 'XML export view for Google Merchant';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'views_data_export_xml';
$handler->display->display_options['style_options']['provide_file'] = 0;
$handler->display->display_options['style_options']['parent_sort'] = 0;
$handler->display->display_options['style_options']['transform'] = 1;
$handler->display->display_options['style_options']['root_node'] = 'nodes';
$handler->display->display_options['style_options']['item_node'] = 'item';
$handler->display->display_options['path'] = 'merchant-xml';
$handler->display->display_options['use_batch'] = 'batch';
$handler->display->display_options['segment_size'] = '100';


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'views',
  1 => 'node',
  2 => 'text',
  3 => 'commerce_price',
  4 => 'physical',
);
