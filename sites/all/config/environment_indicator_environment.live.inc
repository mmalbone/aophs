<?php
/**
 * @file
 * environment_indicator_environment.live.inc
 */

$api = '2.0.0';

$data = $environment = new stdClass();
$environment->disabled = FALSE; /* Edit this to true to make a default environment disabled initially */
$environment->api_version = 1;
$environment->machine = 'live';
$environment->name = 'Live';
$environment->regexurl = 'live-aophs.gotpantheon.com';
$environment->settings = array(
  'color' => '#0c0b0b',
  'text_color' => '#ffffff',
  'weight' => '',
  'position' => 'top',
  'fixed' => 0,
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'environment_indicator',
);
