<?php
/**
 * @file
 * page_manager_handlers.page_sign_up_for_free_homeschool_enews__panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_sign_up_for_free_homeschool_enews__panel_context';
$handler->task = 'page';
$handler->subtask = 'sign_up_for_free_homeschool_enews_';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '79e1e565-1163-47fb-b9db-caacf4d21cad';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-29b294db-8982-46a1-8bb0-a722d38a0e9f';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Sign up for Free Homeschool eNews!',
    'body' => '<p>Looking for great ideas and tips to make your homeschooling better? Sign up for free eNews from Alpha Omega Publications (AOP). From practical homeschooling articles in the Homeschool View&trade; to fantastic sales offers in our AOP NetAlerts, you&#39;re sure to find the helpful information you need to improve your homeschooling. To subscribe, login and click on the free eNews resources you wish to receive. Join today!</p><p><strong>Homeschool View</strong> - Monthly homeschooling teaching tips, ideas, contests, and how to&#39;s</p><p><strong>Daily Focus</strong> - Inspirational daily devotions for homeschool parents</p><p><strong>AOP NetAlert</strong> - Promotional offers, discounts, and shipping savings on Christian homeschool curriculum</p>',
    'format' => 'markdown',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '29b294db-8982-46a1-8bb0-a722d38a0e9f';
  $display->content['new-29b294db-8982-46a1-8bb0-a722d38a0e9f'] = $pane;
  $display->panels['left'][0] = 'new-29b294db-8982-46a1-8bb0-a722d38a0e9f';
  $pane = new stdClass();
  $pane->pid = 'new-bdb45bfc-792c-4dad-b8c9-553129d7dea6';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'block-19';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<span>AOP Newsletter</span>',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'bdb45bfc-792c-4dad-b8c9-553129d7dea6';
  $display->content['new-bdb45bfc-792c-4dad-b8c9-553129d7dea6'] = $pane;
  $display->panels['left'][1] = 'new-bdb45bfc-792c-4dad-b8c9-553129d7dea6';
  $pane = new stdClass();
  $pane->pid = 'new-5d3884ab-3693-4d4c-87d1-f7436afdef52';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'orangebox-enews_signup';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '5d3884ab-3693-4d4c-87d1-f7436afdef52';
  $display->content['new-5d3884ab-3693-4d4c-87d1-f7436afdef52'] = $pane;
  $display->panels['left'][2] = 'new-5d3884ab-3693-4d4c-87d1-f7436afdef52';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-29b294db-8982-46a1-8bb0-a722d38a0e9f';
$handler->conf['display'] = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
