<?php
/**
 * @file
 * page_manager_handlers.page_christian_liberty_press_vs_lifepac_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_christian_liberty_press_vs_lifepac_panel_context';
$handler->task = 'page';
$handler->subtask = 'christian_liberty_press_vs_lifepac';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '0b2c2d42-7fe0-4479-936e-136b4aa2d826';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-7ec10c6c-a920-4381-a440-f3e978fc52bc';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Christian Liberty Press vs. LIFEPAC',
    'body' => '<p>Do you need to compare the independent homeschool option offered by Christian Liberty Press to the LIFEPAC program from Alpha Omega Publications? You\'re in the right spot. Find a short overview of each curriculum and service below, an easy-to-view comparison chart, and additional comparison options!</p><h3><strong>Christian Liberty Press Independent Homeschool Option</strong></h3><p>Christian Liberty Press, a publisher of Christ-centered educational materials, provides materials and service created to assist families with the home education of Pre-K-12th grade students. The Christian Liberty independent homeschooling option is a textbook-based approach which offers a completely independent learning experience for families not seeking administrative oversight or accountability. With the independent program, parents are responsible for all administrative tasks, including all required lesson planning, grading, record-keeping, and reporting. Individual course materials and educational services can be purchased separately or as part of complete grade level kits. Textbooks, workbooks, teacher\'s manuals, test packets, answer keys, and other resources are all available.</p><p>Written from a Christian worldview, all Christian Liberty courses are completely Bible-based. All science instruction is based on a biblical view of creation and the origin of life. Christian Liberty courses include Bible, language arts, literature, history, mathematics, sciences, and electives. Kits provide all essential materials for students and teachers. Curriculum recommendation services and independent achievement testing services are available.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>If you\'re considering your many homeschool curriculum options, it\'s important to remember that there is no one perfect homeschool curriculum. A blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Instead of feeling torn between different curriculums, parents should be open to mixing and matching materials and resources to customize their child\'s education. Remember, each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3><strong>LIFEPAC from Alpha Omega Publications</strong></h3><p>LIFEPAC is a K-12 print-based curriculum offered by Alpha Omega Publications, a Christian-based publisher that provides curriculum and educational services to Christian schools and homeschool families. Full-color and consumable, LIFEPAC is an integrated, Bible-based curriculum developed by a team of accomplished educators and teachers. Using a mastery-based learning approach, each LIFEPAC course is comprised of ten student-directed worktexts that provide a full year of instruction. Individual worktexts may also be used to supplement the homeschool curriculum or to fill in learning gaps. LIFEPAC worktexts include self tests and unit tests to provide assessment of student progress. Teacher\'s guides offer practical teaching tips, additional learning activities, and complete answer keys.</p><p>An economical, proven homeschool curriculum, LIFEPAC integrates Scripture and a Christian worldview throughout all subjects. LIFEPAC offers the flexibility of an individualized, student-driven learning approach which encourages critical thinking and academic independence. Self-paced coursework for K-12 students includes Bible (1-12), language arts (K-12), math (K-12), science (K-12), and history and geography (1-12). Electives are also available for elementary, middle, and high school students. LIFEPAC subject kits include all required student and teacher materials. LIFEPAC curriculum can be purchased as complete grade level sets or as individual subjects. Student and teacher materials and supplemental materials may be purchased separately.</p><h3><strong>Benefits and Features of LIFEPAC from Alpha Omega Publications</strong>&nbsp;</h3><p class="list_heading">LIFEPAC</p><ul><li>full-color, print-based curriculum</li><li>offers progressive, self-paced individual unit worktexts</li><li>promotes concept mastery and cognitive reasoning skills</li><li>encourages academic independence in your children</li></ul><h3 class="para-start"><strong>Take a Closer Look at Christian Liberty Press Independent Homeschooling Option and LIFEPAC</strong></h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">LIFEPAC</th><th class="compare">Christian Liberty Press</th></tr>
</thead>
<tbody>
<tr>
<td>K-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Accredited program</td>
<td class="tac">&nbsp;</td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Student-directed curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Self-paced and individualized</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Separate unit worktexts available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Placement tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Economical</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Manageable amount of daily lesson material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Material designed for homeschoolers</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr><th class="features">Cost</th><th class="compare">Alpha Omega Publications</th><th class="compare">Christian Liberty Press</th></tr>
</thead>
<tbody>
<tr>
<td>Average yearly cost per grade level**</td>
<td class="tac">$286.95</td>
<td class="tac">$223.53</td>
</tr>
<tr class="alt">
<td>Average subject cost per grade level**</td>
<td class="tac">$58.95</td>
<td class="tac">$51.94-$89.50</td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: Christian Liberty Academy/Press versus Alpha Omega Publications</h4>
<p class="list_heading_links">Christian Liberty Press</p>
<ul>
<li><a href="/christian-liberty-press-vs-monarch">Christian Liberty Press vs. Monarch</a></li>
<li><a href="/christian-liberty-press-vs-aoa">Christian Liberty Press vs. Alpha Omega Academy</a></li>
<li><a href="/christian-liberty-press-vs-horizons">Christian Liberty Press vs. Horizons</a></li>
<li><a href="/christian-liberty-press-vs-lifepac">Christian Liberty Press vs. LIFEPAC</a></li>
<li><a href="/christian-liberty-press-vs-sos">Christian Liberty Press vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">Christian Liberty Academy (CLASS Plan) </p>
<ul>
<li><a href="/christian-liberty-academy-class-vs-monarch">Christian Liberty Academy (CLASS Plan) vs. Monarch</a></li>
<li><a href="/christian-liberty-academy-class-vs-aoa">Christian Liberty Academy (CLASS Plan) vs. Alpha Omega Academy</a></li>
<li><a href="/christian-liberty-academy-class-vs-horizons">Christian Liberty Academy (CLASS Plan) vs. Horizons</a></li>
<li><a href="/christian-liberty-academy-class-vs-lifepac">Christian Liberty Academy (CLASS Plan) vs. LIFEPAC</a></li>
<li><a href="/christian-liberty-academy-class-vs-sos">Christian Liberty Academy (CLASS Plan) vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">Christian Liberty Academy (Family Plan) </p>
<ul>
<li><a href="/christian-liberty-academy-family-vs-monarch">Christian Liberty Academy (Family Plan) vs. Monarch</a></li>
<li><a href="/christian-liberty-academy-family-vs-aoa">Christian Liberty Academy (Family Plan) vs. Alpha Omega Academy</a></li>
<li><a href="/christian-liberty-academy-family-vs-horizons">Christian Liberty Academy (Family Plan) vs. Horizons</a></li>
<li><a href="/christian-liberty-academy-family-vs-lifepac">Christian Liberty Academy (Family Plan) vs. LIFEPAC</a></li>
<li><a href="/christian-liberty-academy-family-vs-sos">Christian Liberty Academy (Family Plan) vs. Switched-On Schoolhouse</a></li>
</ul>
<p>Christian Liberty Press is the publishing arm of Christian Liberty Academy and Christian Liberty Academy School System (CLASS). Alpha Omega Publications is not in any way affiliated with Christian Liberty Press. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by Christian Liberty Academy.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p><p>**Prices obtained February 2013 from Christian Liberty Press and the CLASS Homeschools Website and from Alpha Omega Publications 2013 Homeschool Catalog. Yearly prices based on comparison between LIFEPAC 5th Grade 5-subject boxed set and the Christian Liberty 5th Grade Curriculum Kit. Individual subject prices are based on comparison between costs for LIFEPAC 5th Grade Math boxed set and Christian Liberty Press 5th grade math materials. Costs do not reflect any additional fees or shipping and handling charges. Prices for other Christian Liberty Press and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '7ec10c6c-a920-4381-a440-f3e978fc52bc';
  $display->content['new-7ec10c6c-a920-4381-a440-f3e978fc52bc'] = $pane;
  $display->panels['left'][0] = 'new-7ec10c6c-a920-4381-a440-f3e978fc52bc';
  $pane = new stdClass();
  $pane->pid = 'new-c696d304-7794-40e3-b983-2711cc6f4e75';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'c696d304-7794-40e3-b983-2711cc6f4e75';
  $display->content['new-c696d304-7794-40e3-b983-2711cc6f4e75'] = $pane;
  $display->panels['right'][0] = 'new-c696d304-7794-40e3-b983-2711cc6f4e75';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-7ec10c6c-a920-4381-a440-f3e978fc52bc';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
