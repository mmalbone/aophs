<?php
/**
 * @file
 * page_manager_handlers.page__accelerated_christian_education_paces_vs_alpha_omega_academy_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page__accelerated_christian_education_paces_vs_alpha_omega_academy_panel_context';
$handler->task = 'page';
$handler->subtask = '_accelerated_christian_education_paces_vs_alpha_omega_academy';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'd55a48c4-17df-46d8-9eae-d64151abad3d';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-0de51e53-ffea-4ff9-b0d1-1210ce6f25bc';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Accelerated Christian Education PACEs vs. Alpha Omega Academy',
    'body' => '<p>Do you need to compare the print-based curriculum program offered by A.C.E. to the programs offered by Alpha Omega Academy? You\'re in the right spot. Find a short overview of each curriculum and service below, an easy-to-view comparison chart, and additional comparison options!</p><h3><strong>Accelerated Christian Education (School of Tomorrow) PACEs</strong></h3><p>Accelerated Christian Education, an in-house curriculum publisher, provides K-12th grade Christian curriculum and educational materials to Christian schools and homeschoolers. The A.C.E. PACEs option is a student-driven, workbook approach which provides an independent and individualized learning experience to students. Parents are responsible for all administrative tasks, including any required lesson planning, grading, record-keeping, and reporting. Accelerated Christian Education PACEs\' answer keys provide the answers and solutions to all student activities included in each PACE booklet.</p><p>Written from a Christian worldview, all A.C.E. PACEs are completely Bible-based. All science instruction is based on a biblical view of creation and the origin of life. Accelerated Christian Education courses include Bible, reading, social studies, mathematics, science, health, English, literature, fine arts, and foreign language. Coursework can be purchased in complete grade level kits or as subject kits. Kits provide all essential materials for students and teachers. Additional optional resources can be purchased separately.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>Trying to decide between a print-based, parent-directed program and a distance learning academy for your child? Keep in mind there is no one perfect answer for homeschooling. Giving the flexibility of using multiple resources instead of one set curriculum, a blended approach to homeschooling is often chosen by many parents. Instead of feeling torn between curriculum and an academy, parents should be open to mixing and matching materials and resources to customize their child\'s education. Each child is unique and has his own learning style. It\'s important to choose materials and educational services that best fit the needs of the individual child and of the homeschool family.</p><h3><strong>Alpha Omega Academy</strong></h3><p>Alpha Omega Academy (AOA) is an accredited distance learning program for homeschool children in grades K-12. Fully accredited by NCA CASI, AOA offers an at-home education with the convenience of distance learning. Providing qualified teachers, administrative services, and multiple curriculum choices, AOA has enrolled thousands of students from all 50 states and over 42 countries. Great for homeschoolers who are looking for the assurance of accredited courses, AOA offers a high school diploma program and the security of course credits that are readily transferred to other schools and programs. AOA provides families with flexibility in scheduling and completion of assigned coursework. Students must complete coursework within ten months of the start date.</p><p>Alpha Omega Academy allows families to choose between Alpha Omega Publications\' print-based or computer-based formats. Both curriculum options can be perfectly customized to fit your child\'s learning needs and educational goals. Both options provide student-paced lessons that can be completed independently with parental supervision. The print-based program requires a parent to grade daily work. All AOP curriculum promotes a solid, Christian worldview. Core courses include Bible, language arts, math, science, and history and geography. Elementary and high school electives are also available. AOA offers both college-prep and general studies diploma tracks. AOA offers a complete line of over 140 academically challenging courses for students in grades K-12. Placement testing, summer school options, academic support, and achievement testing are all available. Full-time or part-time enrollment is offered. A National Honor Society membership (by invitation) and high school graduation ceremony are also available to AOA students.</p><h3><strong>Benefits and Features of Alpha Omega Academy</strong></h3><p class="list_heading">Alpha Omega Academy</p><ul><li>accredited K-12 distance learning program for homeschoolers</li><li>flexible, at-home learning environment with the convenience of distance learning</li><li>choice of print- or computer-based, Christ-centered curriculum options</li><li>complete high school graduation program</li></ul>
<h3>Take a Closer Look at Accelerated Christian Education PACEs and Alpha Omega Academy</h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">Alpha Omega Academy</th><th class="compare"><strong>Accelerated Christian Education PACEs</strong></th></tr>
</thead>
<tbody>
<tr>
<td>K-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Student-directed curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Computer-based or print-based curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Fully accredited courses</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Convenient distance learning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Placement tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>High school graduation program</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Flexible and individualized</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Mastery-based learning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Direct teacher/student interaction</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Helpful academic support</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: A.C.E. versus Alpha Omega Publications</h4>
<p class="list_heading_links">A.C.E. PACE</p>
<ul>
<li><a href="/ace-pace-vs-monarch">A.C.E. PACE vs. Monarch</a></li>
<li><a href="/ace-pace-vs-horizons">A.C.E. PACE vs. Horizons</a></li>
<li><a href="/ace-pace-vs-lifepac">A.C.E. PACE vs. LIFEPAC</a></li>
<li><a href="/ace-pace-vs-aoa">A.C.E. PACE vs. Alpha Omega Academy</a></li>
<li><a href="/ace-pace-vs-sos">A.C.E. PACE vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">A.C.E. Lighthouse Christian Academy</p>
<ul>
<li><a href="/ace-lca-vs-monarch">A.C.E. Lighthouse Christian Academy vs. Monarch</a></li>
<li><a href="/ace-lca-vs-horizons">A.C.E. Lighthouse Christian Academy vs. Horizons</a></li>
<li><a href="/ace-lca-vs-lifepac">A.C.E. Lighthouse Christian Academy vs. LIFEPAC</a></li>
<li><a href="/ace-lca-vs-aoa">A.C.E. Lighthouse Christian Academy vs. Alpha Omega Academy</a></li>
<li><a href="/ace-lca-vs-sos">A.C.E. Lighthouse Christian Academy vs. Switched-On Schoolhouse</a></li>
</ul>
<p>*A.C.E.® and PACE® are the registered trademarks and subsidiaries of A.C.E.®. Alpha Omega Publications is not in any way affiliated with A.C.E.. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by A.C.E.®.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '0de51e53-ffea-4ff9-b0d1-1210ce6f25bc';
  $display->content['new-0de51e53-ffea-4ff9-b0d1-1210ce6f25bc'] = $pane;
  $display->panels['left'][0] = 'new-0de51e53-ffea-4ff9-b0d1-1210ce6f25bc';
  $pane = new stdClass();
  $pane->pid = 'new-5f26b5af-081f-4c31-b27f-5c6a9e5e2701';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '5f26b5af-081f-4c31-b27f-5c6a9e5e2701';
  $display->content['new-5f26b5af-081f-4c31-b27f-5c6a9e5e2701'] = $pane;
  $display->panels['right'][0] = 'new-5f26b5af-081f-4c31-b27f-5c6a9e5e2701';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-0de51e53-ffea-4ff9-b0d1-1210ce6f25bc';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
