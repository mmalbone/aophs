<?php
/**
 * @file
 * page_manager_pages.switched_on_schoolhouse_end_users_license_agreement.inc
 */

$api = '2.0.0';

$data = $page = new stdClass();
$page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
$page->api_version = 1;
$page->name = 'switched_on_schoolhouse_end_users_license_agreement';
$page->task = 'page';
$page->admin_title = 'Switched-On Schoolhouse End Users License Agreement';
$page->admin_description = '';
$page->path = 'sos-eula';
$page->access = array();
$page->menu = array();
$page->arguments = array();
$page->conf = array(
  'admin_paths' => FALSE,
);


$dependencies = array(
  'page_manager_handlers.page_switched_on_schoolhouse_end_users_license_agreement_panel_context' => 'page_manager_handlers.page_switched_on_schoolhouse_end_users_license_agreement_panel_context',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
