<?php
/**
 * @file
 * page_manager_handlers.page_webinars_monarch_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_webinars_monarch_panel_context';
$handler->task = 'page';
$handler->subtask = 'webinars_monarch';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'flexible';
$display->layout_settings = array(
  'items' => array(
    'canvas' => array(
      'type' => 'row',
      'contains' => 'column',
      'children' => array(
        0 => 'main',
      ),
      'parent' => NULL,
    ),
    'main' => array(
      'type' => 'column',
      'width' => 100,
      'width_type' => '%',
      'children' => array(
        0 => 'main-row',
      ),
      'parent' => 'canvas',
    ),
    'main-row' => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'center',
        1 => 'right',
      ),
      'parent' => 'main',
    ),
    'center' => array(
      'type' => 'region',
      'title' => 'Center',
      'width' => '70.04168670835338',
      'width_type' => '%',
      'parent' => 'main-row',
    ),
    'right' => array(
      'type' => 'region',
      'title' => 'Right',
      'width' => '29.95831329164662',
      'width_type' => '%',
      'parent' => 'main-row',
      'class' => '',
    ),
  ),
);
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'center' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '92d5592b-96e2-4417-b1ab-14bb6318faa1';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-15a48a87-ea3d-4d41-8ab5-772a94d7c40c';
  $pane->panel = 'center';
  $pane->type = 'block';
  $pane->subtype = 'orangebox-list_mon_series';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '15a48a87-ea3d-4d41-8ab5-772a94d7c40c';
  $display->content['new-15a48a87-ea3d-4d41-8ab5-772a94d7c40c'] = $pane;
  $display->panels['center'][0] = 'new-15a48a87-ea3d-4d41-8ab5-772a94d7c40c';
  $pane = new stdClass();
  $pane->pid = 'new-01a69ac3-7859-4846-b9e6-b29d2b5857e9';
  $pane->panel = 'right';
  $pane->type = 'block';
  $pane->subtype = 'orangebox-list_owls';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '01a69ac3-7859-4846-b9e6-b29d2b5857e9';
  $display->content['new-01a69ac3-7859-4846-b9e6-b29d2b5857e9'] = $pane;
  $display->panels['right'][0] = 'new-01a69ac3-7859-4846-b9e6-b29d2b5857e9';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = 'new-15a48a87-ea3d-4d41-8ab5-772a94d7c40c';
$handler->conf['display'] = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
