<?php
/**
 * @file
 * page_manager_pages._christian_liberty_academy_family_plan_vs_alpha_omega_academy.inc
 */

$api = '2.0.0';

$data = $page = new stdClass();
$page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
$page->api_version = 1;
$page->name = '_christian_liberty_academy_family_plan_vs_alpha_omega_academy';
$page->task = 'page';
$page->admin_title = ' Christian Liberty Academy Family Plan vs. Alpha Omega Academy';
$page->admin_description = '';
$page->path = 'christian-liberty-academy-family-vs-aoa';
$page->access = array();
$page->menu = array();
$page->arguments = array();
$page->conf = array(
  'admin_paths' => FALSE,
);


$dependencies = array(
  'page_manager_handlers.page__christian_liberty_academy_family_plan_vs_alpha_omega_academy_panel_context' => 'page_manager_handlers.page__christian_liberty_academy_family_plan_vs_alpha_omega_academy_panel_context',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
