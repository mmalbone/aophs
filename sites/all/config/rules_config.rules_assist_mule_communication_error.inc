<?php
/**
 * @file
 * rules_config.rules_assist_mule_communication_error.inc
 */

$api = '2.0.0';

$data = entity_import('rules_config', '{ "rules_assist_mule_communication_error" : {
      "LABEL" : "Assist-Mule Communication Error",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "mimemail", "aop_assist" ],
      "ON" : { "aop_assist_mule_error" : [] },
      "DO" : [
        { "mimemail" : {
            "key" : "Failure to Communicate with Mule",
            "to" : "webservicesteam@glynlyon.com",
            "from_name" : "Drupal Webstore",
            "from_mail" : "webservicesteam@glynlyon.com",
            "reply_to" : "webservicesteam@glynlyon.com",
            "subject" : "Unable to reach Mule\\/Assist",
            "body" : "The webstore was unable to communicate with Mule\\/Assist.",
            "plaintext" : "The webstore was unable to communicate with Mule\\/Assist.",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'entity',
  1 => 'aop_assist',
  2 => 'mimemail',
  3 => 'rules',
);
