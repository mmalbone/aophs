<?php
/**
 * @file
 * page_manager_handlers.page_schedule_a_callback_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_schedule_a_callback_panel_context';
$handler->task = 'page';
$handler->subtask = 'schedule_a_callback';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'flexible:aop_1_2_layout';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'center' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '3137f910-6fbd-450f-b611-88c9d34cfe4b';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-ef871f9f-0449-4a48-beb2-b662c4e1bd66';
  $pane->panel = 'center';
  $pane->type = 'block';
  $pane->subtype = 'webform-client-block-19032';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'ef871f9f-0449-4a48-beb2-b662c4e1bd66';
  $display->content['new-ef871f9f-0449-4a48-beb2-b662c4e1bd66'] = $pane;
  $display->panels['center'][0] = 'new-ef871f9f-0449-4a48-beb2-b662c4e1bd66';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = 'new-ef871f9f-0449-4a48-beb2-b662c4e1bd66';
$handler->conf['display'] = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
