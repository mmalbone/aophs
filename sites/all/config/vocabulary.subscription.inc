<?php
/**
 * @file
 * vocabulary.subscription.inc
 */

$api = '2.0.0';

$data = (object) array(
  'vid' => '19',
  'name' => 'Subscription',
  'machine_name' => 'subscription',
  'description' => '',
  'hierarchy' => '0',
  'module' => 'taxonomy',
  'weight' => '0',
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'taxonomy',
);
