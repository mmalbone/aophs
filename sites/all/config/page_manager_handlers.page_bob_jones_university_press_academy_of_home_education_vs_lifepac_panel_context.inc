<?php
/**
 * @file
 * page_manager_handlers.page_bob_jones_university_press_academy_of_home_education_vs_lifepac_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_bob_jones_university_press_academy_of_home_education_vs_lifepac_panel_context';
$handler->task = 'page';
$handler->subtask = 'bob_jones_university_press_academy_of_home_education_vs_lifepac';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '2da92770-a8fd-4fe0-ac9c-3ee706e285ee';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-44723b68-504f-47b8-bfed-9514287850f4';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Bob Jones University Press Academy of Home Education vs. LIFEPAC',
    'body' => '<p>Thinking about homeschooling with Bob Jones University Press? Wondering what the differences are between Bob Jones and Alpha Omega Publications? You\'re in the right place. Many parents just like you are also searching for a homeschool curriculum and teaching approach that best fits their child\'s academic needs. With so many homeschooling options, resources, and curricula to choose from, selecting homeschool materials for your child can often be a daunting task, but we\'re here to help!</p><h3><strong>Bob Jones University Press Academy of Home Education</strong></h3><p>The Bob Jones University Press Academy of Home Education is designed to help homeschool parents by providing a recommended course of study to be completed under the direction of the parents. Enrollment in the Academy of Home Education is open to all 1st-12th grade students who agree to carry out a program of home education utilizing BJU Press curriculum in all academic areas in which these materials are available. All Bob Jones University Press curriculum is Bible-based, and presents instruction from a Christian worldview. Coursework is available in Bible, heritage studies, math, language (English, phonics, reading, handwriting, and spelling), and science. Families enrolled in the Academy of Home Education are provided with a variety of materials and services including, but not limited to, customer service and curriculum consultation, forms for recording student evaluation/grades, report cards reflecting parent evaluations, annual standardized testing, high school diploma and graduation ceremony, and formal transcript detailing student\'s academic record.</p><p>Students enrolled in the Academy of Home Education are required to complete a predetermined course of study over an enrollment period of at least 180 days. Some calendar requirements may apply to enrolled high school students. Fees for enrollment include all provided services, but the purchase of BJU Press curricular materials (textbooks, DVDs, hard drive, and/or online program) is the responsibility of the parents. Parents are also responsible for supervising of all student coursework, as well as all lesson planning and grading of daily work. Self-reported grades are sent to the Academy of Home Education office to be included in the student\'s permanent record.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>Wondering whether you should use a print-based or a distance learning approach to homeschooling? Keep in mind that there is no one perfect homeschool curriculum. A blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Instead of feeling torn between curriculum formats, parents should be open to mixing and matching materials and resources to customize their child\'s education. Remember, each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3><strong>LIFEPAC from Alpha Omega Publications</strong></h3><p>LIFEPAC is a K-12 print-based curriculum offered by Alpha Omega Publications, a Christian-based publisher that provides curriculum and educational services to Christian schools and homeschool families. Full-color and consumable, LIFEPAC is an integrated, Bible-based curriculum developed by a team of accomplished educators and teachers. Using a mastery-based learning approach, each LIFEPAC course is comprised of ten student-directed worktexts that provide a full year of instruction. Individual worktexts may also be used to supplement the homeschool curriculum or to fill in learning gaps. LIFEPAC worktexts include self tests and unit tests to provide assessment of student progress. Teacher\'s guides offer practical teaching tips, additional learning activities, and complete answer keys.</p><p>An economical, proven homeschool curriculum, LIFEPAC integrates Scripture and a Christian worldview throughout all subjects. LIFEPAC offers the flexibility of an individualized, student-driven learning approach which encourages critical thinking and academic independence. Self-paced coursework for K-12 students includes Bible (1-12), language arts (K-12), math (K-12), science (K-12), and history and geography (1-12). Electives are also available for elementary, middle, and high school students. LIFEPAC subject kits include all required student and teacher materials. LIFEPAC curriculum can be purchased as complete grade level sets or as individual subjects. Student and teacher materials and supplemental materials may be purchased separately.</p><h3><strong>Benefits and Features of LIFEPAC from Alpha Omega Publications</strong></h3><p class="list_heading">LIFEPAC</p><ul><li>full-color, print-based curriculum</li><li>offers progressive, self-paced individual unit worktexts</li><li>promotes concept mastery and cognitive reasoning skills</li><li>encourages academic independence in your children</li></ul><h3 class="para-start"><strong>Take a Closer Look at Bob Jones University Press Academy of Home Education and LIFEPAC</strong></h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">LIFEPAC</th><th class="compare">BJU Press Academy of Home Education</th></tr>
</thead>
<tbody>
<tr>
<td>K-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Student-directed curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Instruction provided through text-based lessons</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Instruction provided through recorded lesson sessions</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Promotes concept mastery and cognitive reasoning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Self-paced and individualized</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Manageable amount of daily lesson material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Separate unit worktexts available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Requires material to be returned</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Economical</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Accredited program</td>
<td class="tac">&nbsp;</td>
<td class="tac">&nbsp;</td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr><th class="features">Cost</th><th class="compare">Alpha Omega Publications</th><th class="compare">Bob Jones University Press</th></tr>
</thead>
<tbody>
<tr>
<td>Average yearly cost per grade level**</td>
<td class="tac">$286.95</td>
<td class="tac">$250-300 (plus curriculum)</td>
</tr>
<tr class="alt">
<td>Average subject cost per grade level**</td>
<td class="tac">$58.95</td>
<td class="tac">N/A</td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: Bob Jones University Press versus Alpha Omega Publications</h4>
<p class="list_heading_links">Bob Jones University Press Textbooks</p>
<ul>
<li><a href="/bju-textbook-vs-monarch">Bob Jones University Press Textbooks vs. Monarch</a></li>
<li><a href="/bju-textbook-vs-horizons">Bob Jones University Press Textbooks vs. Horizons</a></li>
<li><a href="/bju-textbook-vs-lifepac">Bob Jones University Press Textbooks vs. LIFEPAC</a></li>
<li><a href="/bju-textbook-vs-aoa">Bob Jones University Press Textbooks vs. Alpha Omega Academy</a></li>
<li><a href="/bju-textbook-vs-sos">Bob Jones University Press Textbooks vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">Bob Jones University Press (DVD)</p>
<ul>
<li><a href="/bju-dvd-vs-monarch">Bob Jones University Press (DVD) vs. Monarch</a></li>
<li><a href="/bju-dvd-vs-horizons">Bob Jones University Press (DVD) vs. Horizons</a></li>
<li><a href="/bju-dvd-vs-lifepac">Bob Jones University Press (DVD) vs. LIFEPAC</a></li>
<li><a href="/bju-dvd-vs-aoa">Bob Jones University Press (DVD) vs. Alpha Omega Academy</a></li>
<li><a href="/bju-dvd-vs-sos">Bob Jones University Press (DVD) vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">Bob Jones University Press Academy (Hard Drive)</p>
<ul>
<li><a href="/bju-hard-drive-vs-monarch">Bob Jones University Press Academy (Hard Drive) vs. Monarch</a></li>
<li><a href="/bju-hard-drive-vs-horizons">Bob Jones University Press Academy (Hard Drive) vs. Horizons</a></li>
<li><a href="/bju-hard-drive-vs-lifepac">Bob Jones University Press Academy (Hard Drive) vs. LIFEPAC</a></li>
<li><a href="/bju-hard-drive-vs-aoa">Bob Jones University Press Academy (Hard Drive) vs. Alpha Omega Academy</a></li>
<li><a href="/bju-hard-drive-vs-sos">Bob Jones University Press Academy (Hard Drive) vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">Bob Jones University Press Academy (Distance Learning Online)</p>
<ul>
<li><a href="/bju-online-vs-monarch">Bob Jones University Press Academy (Distance Learning Online) vs. Monarch</a></li>
<li><a href="/bju-online-vs-horizons">Bob Jones University Press Academy (Distance Learning Online) vs. Horizons</a></li>
<li><a href="/bju-online-vs-lifepac">Bob Jones University Press Academy (Distance Learning Online) vs. LIFEPAC</a></li>
<li><a href="/bju-online-vs-aoa">Bob Jones University Press Academy (Distance Learning Online) vs. Alpha Omega Academy</a></li>
<li><a href="/bju-online-vs-sos">Bob Jones University Press Academy (Distance Learning Online) vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">Bob Jones University Press Academy (Academy of Home Education)</p>
<ul>
<li><a href="/bju-academy-home-education-vs-monarch">Bob Jones University Press Academy (Academy of Home Education) vs. Monarch</a></li>
<li><a href="/bju-academy-home-education-vs-horizons">Bob Jones University Press Academy (Academy of Home Education) vs. Horizons</a></li>
<li><a href="/bju-academy-home-education-vs-lifepac">Bob Jones University Press Academy (Academy of Home Education) vs. LIFEPAC</a></li>
<li><a href="/bju-academy-home-education-vs-aoa">Bob Jones University Press Academy (Academy of Home Education) vs. Alpha Omega Academy</a></li>
<li><a href="/bju-academy-home-education-vs-sos">Bob Jones University Press Academy (Academy of Home Education) vs. Switched-On Schoolhouse</a></li>
</ul>
<p>*BJU Press® is the registered trademark and subsidiary of Bob Jones University®. Alpha Omega Publications is not in any way affiliated with Bob Jones University Press or its trademark owner Bob Jones University. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by BJU Press.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p><p>**Prices obtained February 2013 from BJU Press Total Homeschool Solutions and BJUP websites and from Alpha Omega Publications 2013 Homeschool Catalog. Yearly prices based on comparison between LIFEPAC 3rd Grade 5-Subject Set and Bob Jones University Press Academy of Home Education enrollment for full year (cost of curriculum not included). Subject prices are based on comparison between LIFEPAC 3rd Grade Math (including all student and teacher material, along with supplements) and Bob Jones University Press Academy of Home Education individual course fees (not available). Costs do not reflect any additional fees or shipping and handling charges. Prices for other Bob Jones University Press and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '44723b68-504f-47b8-bfed-9514287850f4';
  $display->content['new-44723b68-504f-47b8-bfed-9514287850f4'] = $pane;
  $display->panels['left'][0] = 'new-44723b68-504f-47b8-bfed-9514287850f4';
  $pane = new stdClass();
  $pane->pid = 'new-f781c9ce-6761-4dd5-8979-d52d8ba69aeb';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'f781c9ce-6761-4dd5-8979-d52d8ba69aeb';
  $display->content['new-f781c9ce-6761-4dd5-8979-d52d8ba69aeb'] = $pane;
  $display->panels['right'][0] = 'new-f781c9ce-6761-4dd5-8979-d52d8ba69aeb';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-44723b68-504f-47b8-bfed-9514287850f4';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
