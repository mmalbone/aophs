<?php
/**
 * @file
 * page_manager_handlers.page_christian_liberty_vs_alpha_omega_publications_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_christian_liberty_vs_alpha_omega_publications_panel_context';
$handler->task = 'page';
$handler->subtask = 'christian_liberty_vs_alpha_omega_publications';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '053dac4b-bdac-4317-9a15-15d22d785d4a';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-1ce09321-e3c2-4fa5-a176-d10665de72ff';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Christian Liberty vs. Alpha Omega Publications',
    'body' => '<p>Thinking about homeschooling with Christian Liberty? Wondering what the differences are between Christian Liberty and Alpha Omega Publications? You\'re in the right place. Many parents just like you are also searching for a homeschool curriculum and teaching approach that best fits their child\'s academic needs. With so many homeschooling options, resources, and curricula to choose from, selecting homeschool materials for your child can often be a daunting task, but we\'re here to help!</p><h3><strong>Christian Liberty\'s Approach</strong></h3><p>Christian Liberty Academy School System is a ministry that offers a variety of educational options to Christian families. In addition to a Christian day school, the Christian Liberty Academy School System offers a large distance learning academy. Christian Liberty Press, the publishing arm of the Christian Liberty Academy School System, publishes a broad selection of K-12 curriculum and Christian literature. Families enrolled in the Christian Liberty Academy can select from two administrative options. The CLASS Plan provides families with accountability and flexibility. Record-keeping, annual standardized tests, and provision of reports cards and transcripts are among the services provided. The Family Plan provides fewer services but offers a greater degree of independence. Families may also elect to purchase homeschool textbooks and materials directly from Christian Liberty Press as a completely independent homeschool option. Christian Liberty\'s self-published curriculum is strongly Bible-based, offering homeschoolers a distinctly Christian homeschool experience.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>Most parents discover that their child\'s educational needs are best met by blending several different homeschool curriculums and teaching approaches. Because there is no one perfect homeschool curriculum, a blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Instead of feeling torn between curricula, parents should be open to mixing and matching materials and resources to customize their child\'s education. Remember, each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3><strong>Curriculum from Alpha Omega Publications</strong></h3><p>Alpha Omega Publications is a Christian-based, PreK-12 publisher that provides engaging, interactive curriculum and educational services to homeschool families. Offering proven, easy-to-teach homeschool curriculum, AOP offers four main curriculum offerings: Internet-based Monarch (3-12), computer-based Switched-On Schoolhouse (3-12), worktext-based LIFEPAC (K-12), and workbook-based Horizons (PreK-12). Main core subjects offered include Bible, language arts, math, science, and history and geography, along with various electives. AOP also offers Alpha Omega Academy, a distance learning academy for grades K-12. Understanding that each child learns differently, AOP offers diverse curricula and services in different formats to fit multiple learning styles, ensuring you can find what fits your child\'s needs.&nbsp;</p><h3><strong>Benefits and Features of Alpha Omega Publications\' Curriculum</strong>&nbsp;</h3><p class="list_heading">Monarch</p><ul><li>online interactive lessons with anytime, anywhere access</li><li>compatible with both Windows® and Mac® operating systems</li><li>offers customizable student learning options and time-saving teaching tools</li><li>features dynamic, media-rich lessons; real-time content updates; and data storage</li></ul><p class="list_heading">Switched-On Schoolhouse</p><ul><li>interactive, computer-based curriculum</li><li>offers a flexible, engaging learning environment</li><li>contains multimedia tools, educational games, and automatic grading options</li><li>can reduce teacher planning and grading by 20 hours per week</li></ul><p class="list_heading">LIFEPAC</p><ul><li>full-color, print-based curriculum</li><li>offers progressive, self-paced individual unit worktexts</li><li>promotes concept mastery and cognitive reasoning skills</li><li>encourages academic independence in your children</li></ul><p class="list_heading">Horizons</p><ul><li>brightly illustrated, hands-on, consumable curriculum</li><li>offers fun lessons and manipulative-based activities</li><li>teaches concepts through a spiral learning process</li><li>encourages concept mastery through quick concept introduction, review, and reinforcement</li></ul><h3 class="para-start"><strong>Take a Closer Look at Christian Liberty and Alpha Omega Publications</strong></h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">Alpha Omega Publications</th><th class="compare">Christian Liberty</th></tr>
</thead>
<tbody>
<tr>
<td>PreK-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Flexible and individualized curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Customizable curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Teacher-intensive curriculum</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Placement tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Interactive lesson material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Automatic curriculum updates available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Automatic grading and lesson planning available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Appeals to students with multiple learning styles</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr><th class="features">Cost</th><th class="compare">Alpha Omega Publications</th><th class="compare">Christian Liberty</th></tr>
</thead>
<tbody>
<tr>
<td>Average yearly cost per grade level**</td>
<td class="tac">$449.95 for Monarch</td>
<td class="tac">$555</td>
</tr>
<tr class="alt">
<td>Average subject cost per grade level**</td>
<td class="tac">$99.95 for Monarch</td>
<td class="tac">$51.94-$89.50</td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: Christian Liberty Academy/Press versus Alpha Omega Publications</h4>
<p class="list_heading_links">Christian Liberty Press</p>
<ul>
<li><a href="/christian-liberty-press-vs-monarch">Christian Liberty Press vs. Monarch</a></li>
<li><a href="/christian-liberty-press-vs-aoa">Christian Liberty Press vs. Alpha Omega Academy</a></li>
<li><a href="/christian-liberty-press-vs-horizons">Christian Liberty Press vs. Horizons</a></li>
<li><a href="/christian-liberty-press-vs-lifepac">Christian Liberty Press vs. LIFEPAC</a></li>
<li><a href="/christian-liberty-press-vs-sos">Christian Liberty Press vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">Christian Liberty Academy (CLASS Plan) </p>
<ul>
<li><a href="/christian-liberty-academy-class-vs-monarch">Christian Liberty Academy (CLASS Plan) vs. Monarch</a></li>
<li><a href="/christian-liberty-academy-class-vs-aoa">Christian Liberty Academy (CLASS Plan) vs. Alpha Omega Academy</a></li>
<li><a href="/christian-liberty-academy-class-vs-horizons">Christian Liberty Academy (CLASS Plan) vs. Horizons</a></li>
<li><a href="/christian-liberty-academy-class-vs-lifepac">Christian Liberty Academy (CLASS Plan) vs. LIFEPAC</a></li>
<li><a href="/christian-liberty-academy-class-vs-sos">Christian Liberty Academy (CLASS Plan) vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">Christian Liberty Academy (Family Plan) </p>
<ul>
<li><a href="/christian-liberty-academy-family-vs-monarch">Christian Liberty Academy (Family Plan) vs. Monarch</a></li>
<li><a href="/christian-liberty-academy-family-vs-aoa">Christian Liberty Academy (Family Plan) vs. Alpha Omega Academy</a></li>
<li><a href="/christian-liberty-academy-family-vs-horizons">Christian Liberty Academy (Family Plan) vs. Horizons</a></li>
<li><a href="/christian-liberty-academy-family-vs-lifepac">Christian Liberty Academy (Family Plan) vs. LIFEPAC</a></li>
<li><a href="/christian-liberty-academy-family-vs-sos">Christian Liberty Academy (Family Plan) vs. Switched-On Schoolhouse</a></li>
</ul>
<p>Christian Liberty Press is the publishing arm of Christian Liberty Academy and Christian Liberty Academy School System (CLASS). Alpha Omega Publications is not in any way affiliated with Christian Liberty Press. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by Christian Liberty Academy.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p><p>**Prices obtained February 2013 from Christian Liberty Press and the CLASS Homeschools Website and from Alpha Omega Publications 2013 Homeschool Catalog. Yearly prices based on comparison between the Monarch 5th Grade 5-subject set and the Christian Liberty Academy CLASS plan full time enrollment for a 2nd-8th grade student. Individual subject prices are based on a comparison between the Monarch 5th Grade Math and Christian Liberty Press 5th grade math materials. Costs do not reflect any additional fees or shipping and handling charges. Prices for other Christian Liberty Press and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '1ce09321-e3c2-4fa5-a176-d10665de72ff';
  $display->content['new-1ce09321-e3c2-4fa5-a176-d10665de72ff'] = $pane;
  $display->panels['left'][0] = 'new-1ce09321-e3c2-4fa5-a176-d10665de72ff';
  $pane = new stdClass();
  $pane->pid = 'new-c086220a-9117-4eab-b773-add84bfccbf6';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'c086220a-9117-4eab-b773-add84bfccbf6';
  $display->content['new-c086220a-9117-4eab-b773-add84bfccbf6'] = $pane;
  $display->panels['right'][0] = 'new-c086220a-9117-4eab-b773-add84bfccbf6';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-1ce09321-e3c2-4fa5-a176-d10665de72ff';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
