<?php
/**
 * @file
 * search_api_server.frontend.inc
 */

$api = '2.0.0';

$data = entity_import('search_api_server', '{
    "name" : "Frontend",
    "machine_name" : "frontend",
    "description" : "",
    "class" : "search_api_db_service",
    "options" : {
      "database" : "default:default",
      "min_chars" : 3,
      "indexes" : {
        "product_display" : {
          "nid" : {
            "table" : "search_api_db_product_display",
            "column" : "nid",
            "type" : "integer",
            "boost" : "1.0"
          },
          "type" : {
            "table" : "search_api_db_product_display",
            "column" : "type",
            "type" : "string",
            "boost" : "1.0"
          },
          "status" : {
            "table" : "search_api_db_product_display",
            "column" : "status",
            "type" : "integer",
            "boost" : "1.0"
          },
          "created" : {
            "table" : "search_api_db_product_display",
            "column" : "created",
            "type" : "date",
            "boost" : "1.0"
          },
          "changed" : {
            "table" : "search_api_db_product_display",
            "column" : "changed",
            "type" : "date",
            "boost" : "1.0"
          },
          "field_tags" : {
            "table" : "search_api_db_product_display_field_tags",
            "column" : "value",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "title_field" : {
            "table" : "search_api_db_product_display",
            "column" : "title_field",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_collection" : {
            "table" : "search_api_db_product_display",
            "column" : "field_collection",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_product" : {
            "table" : "search_api_db_product_display_field_product",
            "column" : "value",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "search_api_language" : {
            "table" : "search_api_db_product_display",
            "column" : "search_api_language",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_product_commerce_price_amount_decimal_asc" : {
            "table" : "search_api_db_product_display",
            "column" : "field_product_commerce_price_amount_decimal_asc",
            "type" : "decimal",
            "boost" : "1.0"
          },
          "field_product_commerce_price_amount_decimal_desc" : {
            "table" : "search_api_db_product_display",
            "column" : "field_product_commerce_price_amount_decimal_desc",
            "type" : "decimal",
            "boost" : "1.0"
          },
          "search_api_aggregation_1" : {
            "table" : "search_api_db_product_display_text",
            "type" : "text",
            "boost" : "1.0"
          },
          "search_api_aggregation_2" : {
            "table" : "search_api_db_product_display_text",
            "type" : "text",
            "boost" : "1.0"
          },
          "search_api_aggregation_3" : {
            "table" : "search_api_db_product_display_text",
            "type" : "text",
            "boost" : "1.0"
          },
          "field_collection:name" : {
            "table" : "search_api_db_product_display",
            "column" : "field_collection_name",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_product:commerce_price:amount_decimal" : {
            "table" : "search_api_db_product_display_field_product_commerce_price_amo",
            "column" : "value",
            "type" : "list\\u003Cdecimal\\u003E",
            "boost" : "1.0"
          },
          "field_product:field_subject" : {
            "table" : "search_api_db_product_display_field_product_field_subject",
            "column" : "value",
            "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
            "boost" : "1.0"
          },
          "field_product:field_grade_level" : {
            "table" : "search_api_db_product_display_field_product_field_grade_level",
            "column" : "value",
            "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
            "boost" : "1.0"
          },
          "field_product:field_subject:name" : {
            "table" : "search_api_db_product_display_field_product_field_subject_name",
            "column" : "value",
            "type" : "list\\u003Clist\\u003Cstring\\u003E\\u003E",
            "boost" : "1.0"
          },
          "field_product:field_grade_level:name" : {
            "table" : "search_api_db_product_display_field_product_field_grade_level_",
            "column" : "value",
            "type" : "list\\u003Clist\\u003Cstring\\u003E\\u003E",
            "boost" : "1.0"
          },
          "field_product:sku" : {
            "table" : "search_api_db_product_display_text",
            "type" : "list\\u003Ctext\\u003E",
            "boost" : "1.0"
          },
          "title" : {
            "table" : "search_api_db_product_display_text",
            "type" : "text",
            "boost" : "1.0"
          },
          "field_subject" : {
            "table" : "search_api_db_product_display_field_subject",
            "column" : "value",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_grade_level" : {
            "table" : "search_api_db_product_display_field_grade_level",
            "column" : "value",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_curriculum" : {
            "table" : "search_api_db_product_display",
            "column" : "field_curriculum",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_product:type" : {
            "table" : "search_api_db_product_display_field_product_type",
            "column" : "value",
            "type" : "list\\u003Cstring\\u003E",
            "boost" : "1.0"
          },
          "field_product:title" : {
            "table" : "search_api_db_product_display_text",
            "type" : "list\\u003Ctext\\u003E",
            "boost" : "1.0"
          },
          "field_product:field_product_node" : {
            "table" : "search_api_db_product_display_field_product_field_product_node",
            "column" : "value",
            "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
            "boost" : "1.0"
          },
          "field_product:title_field" : {
            "table" : "search_api_db_product_display_text",
            "type" : "list\\u003Ctext\\u003E",
            "boost" : "1.0"
          },
          "field_product:field_curriculum" : {
            "table" : "search_api_db_product_display_field_product_field_curriculum",
            "column" : "value",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_product:field_dvd_cd" : {
            "table" : "search_api_db_product_display_field_product_field_dvd_cd",
            "column" : "value",
            "type" : "list\\u003Cstring\\u003E",
            "boost" : "1.0"
          },
          "field_product:field_dvd_category" : {
            "table" : "search_api_db_product_display_field_product_field_dvd_category",
            "column" : "value",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_collection:field_grade_levels" : {
            "table" : "search_api_db_product_display_text",
            "type" : "text",
            "boost" : "1.0"
          },
          "field_product:field_subject:field_grade_levels" : {
            "table" : "search_api_db_product_display_text",
            "type" : "list\\u003Clist\\u003Ctext\\u003E\\u003E",
            "boost" : "1.0"
          }
        },
        "grade_level" : { "name" : {
            "table" : "search_api_db_grade_level_text",
            "type" : "text",
            "boost" : "1.0"
          }
        }
      }
    },
    "enabled" : "1"
  }');

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'entity',
  1 => 'search_api',
);
