<?php
/**
 * @file
 * page_manager_handlers.page_uncommon_homeschooling_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_uncommon_homeschooling_panel_context';
$handler->task = 'page';
$handler->subtask = 'uncommon_homeschooling';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'sutro';
$display->layout_settings = array(
  'items' => array(
    'canvas' => array(
      'type' => 'row',
      'contains' => 'column',
      'children' => array(
        0 => 'main',
      ),
      'parent' => NULL,
    ),
    'main' => array(
      'type' => 'column',
      'width' => 100,
      'width_type' => '%',
      'children' => array(
        0 => 'main-row',
        1 => 1,
      ),
      'parent' => 'canvas',
    ),
    'main-row' => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'center',
        1 => 'right',
      ),
      'parent' => 'main',
    ),
    'center' => array(
      'type' => 'region',
      'title' => 'Center',
      'width' => '60.065380435750804',
      'width_type' => '%',
      'parent' => 'main-row',
    ),
    'right' => array(
      'type' => 'region',
      'title' => 'Right',
      'width' => '39.934619564249196',
      'width_type' => '%',
      'parent' => 'main-row',
      'class' => '',
    ),
    1 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'center_',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    'center_' => array(
      'type' => 'region',
      'title' => 'Center',
      'width' => 100,
      'width_type' => '%',
      'parent' => '1',
      'class' => '',
    ),
  ),
);
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'center' => NULL,
    'right' => NULL,
    'center_' => NULL,
    'header' => NULL,
    'column1' => NULL,
    'column2' => NULL,
    'footer' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '4a92fe8c-cd0b-4114-8b9b-6439577064ae';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-84eeecf8-31ef-4f29-ba3c-0f6e41180de4';
  $pane->panel = 'column1';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => '',
    'body' => '<p>[[{"fid":"198","view_mode":"default","fields":{"format":"default","field_file_image_alt_text[und][0][value]":"Uncommon Homeschooling","field_file_image_title_text[und][0][value]":"Uncommon Homeschooling"},"type":"media","attributes":{"height":289,"width":620,"alt":"Uncommon Homeschooling","title":"Uncommon Homeschooling","class":"media-element file-default"}}]]</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '84eeecf8-31ef-4f29-ba3c-0f6e41180de4';
  $display->content['new-84eeecf8-31ef-4f29-ba3c-0f6e41180de4'] = $pane;
  $display->panels['column1'][0] = 'new-84eeecf8-31ef-4f29-ba3c-0f6e41180de4';
  $pane = new stdClass();
  $pane->pid = 'new-885fad98-12a1-4629-999a-b3bad614b51b';
  $pane->panel = 'column1';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => '',
    'body' => '<h2>Homeschooling rises above standards to teach children to think for themselves and learn freely in ways that suit them best.</h2><p>Just as you stand apart through your commitment to your child\'s education, we go beyond the ordinary, from our passion for homeschooling and variety of curriculum to our foundation in faith and tradition of excellence. Being uncommon is a badge of honor that we owe to you and all the homeschool families who inspire us to aim higher with each new day.</p>',
    'format' => 'markdown',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '885fad98-12a1-4629-999a-b3bad614b51b';
  $display->content['new-885fad98-12a1-4629-999a-b3bad614b51b'] = $pane;
  $display->panels['column1'][1] = 'new-885fad98-12a1-4629-999a-b3bad614b51b';
  $pane = new stdClass();
  $pane->pid = 'new-c48ebb84-93fc-4d8e-af25-e592ca3df47f';
  $pane->panel = 'column2';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => '',
    'body' => '<div class="uh-lp-elem" style="min-height:175px;"><img alt="A New Look" class="uh-lp-badge" src="https://glnmedia.s3.amazonaws.com/images/pages/hsc/uncommon/new-AOP-logo.gif"><div><h3>A NEW LOOK</h3>We share your dedication to hard work and Christian education. As a reflection of this mission, we\'ve updated our look with an image of a child committed to his studies and his relationship with Christ through prayer.</div></div>',
    'format' => 'markdown',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'c48ebb84-93fc-4d8e-af25-e592ca3df47f';
  $display->content['new-c48ebb84-93fc-4d8e-af25-e592ca3df47f'] = $pane;
  $display->panels['column2'][0] = 'new-c48ebb84-93fc-4d8e-af25-e592ca3df47f';
  $pane = new stdClass();
  $pane->pid = 'new-fa7835db-f12e-4d43-a45e-43fce6383dc4';
  $pane->panel = 'column2';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => '',
    'body' => '<div class="uh-lp-elem" style="min-height:200px;"><img alt="A New Selection" class="uh-lp-badge" src="//glnnewmedia.s3.amazonaws.com/new-selection.gif" title="A New Selection"><div><h3>A NEW SELECTION</h3>Your student will love the choices among our new courses and curriculum updates. Explore <a href="/new-for-2014" style="color: #2ab5cd;">what we\'re releasing in 2014</a>, including redesigned LIFEPAC courses and many new Career and Technical Education (CTE) electives for Monarch and Switched-On Schoolhouse.</div></div>',
    'format' => 'markdown',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'fa7835db-f12e-4d43-a45e-43fce6383dc4';
  $display->content['new-fa7835db-f12e-4d43-a45e-43fce6383dc4'] = $pane;
  $display->panels['column2'][1] = 'new-fa7835db-f12e-4d43-a45e-43fce6383dc4';
  $pane = new stdClass();
  $pane->pid = 'new-16993583-0187-48b7-a7ba-997a107cf51c';
  $pane->panel = 'column2';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => '',
    'body' => '<div class="uh-lp-elem"><img alt="A New Experience" class="uh-lp-badge" src="https://glnmedia.s3.amazonaws.com/images/pages/hsc/uncommon/new-experience.gif"><div><h3>A NEW EXPERIENCE</h3>We\'ve also rebuilt our website to better deliver the Bible-based curriculum you’ve come to expect. Simple to search and shop, the new site makes it easier to find the perfect option for your homeschool family.</div></div>',
    'format' => 'markdown',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '16993583-0187-48b7-a7ba-997a107cf51c';
  $display->content['new-16993583-0187-48b7-a7ba-997a107cf51c'] = $pane;
  $display->panels['column2'][2] = 'new-16993583-0187-48b7-a7ba-997a107cf51c';
  $pane = new stdClass();
  $pane->pid = 'new-8d7a0a21-6612-4ec1-b468-4767046967b3';
  $pane->panel = 'footer';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => '',
    'body' => '<div  style="margin:40px auto 0 auto; max-width:700px; text-align:center"><h4>Are you ready for an uncommon homeschooling experience? Be a part of our homeschool community and discover for yourself the benefits of our distinctly Christian curriculum.</h4><a href="/find-a-curriculum" style="text-decoration: none;"><img alt="Find Your Curriculum" src="https://glnmedia.s3.amazonaws.com/images/pages/hsc/uncommon/findcurriculumbutton.png"> </a><br><br><a href="/enews">Sign up for our emails</a> to stay in the loop about news and deals from AOP.</div>',
    'format' => 'markdown',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '8d7a0a21-6612-4ec1-b468-4767046967b3';
  $display->content['new-8d7a0a21-6612-4ec1-b468-4767046967b3'] = $pane;
  $display->panels['footer'][0] = 'new-8d7a0a21-6612-4ec1-b468-4767046967b3';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-885fad98-12a1-4629-999a-b3bad614b51b';
$handler->conf['display'] = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
