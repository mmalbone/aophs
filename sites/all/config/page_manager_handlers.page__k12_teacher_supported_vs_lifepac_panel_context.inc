<?php
/**
 * @file
 * page_manager_handlers.page__k12_teacher_supported_vs_lifepac_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page__k12_teacher_supported_vs_lifepac_panel_context';
$handler->task = 'page';
$handler->subtask = '_k12_teacher_supported_vs_lifepac';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '47e1b412-9a02-4bcb-86e8-b5ef6a0a3544';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-7d41e580-5b73-47fd-90ce-e5554406aed1';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'K12 Teacher-Supported vs. LIFEPAC',
    'body' => '<p>Do you need to compare the teacher-supported online curriculum offered by K12 Consumer Direct to the LIFEPAC program from Alpha Omega Publications? You\'re in the right spot. Find a short overview of each curriculum and service below, an easy-to-view comparison chart, and additional comparison options!</p><h3><strong>K12 Virtual Schools</strong></h3><p>K12 is a large secular provider of kindergarten through 12th grade online curriculum currently being used in virtual public schools, an international online academy, as well as in homeschools worldwide. Because K12 is secular curriculum provider, parents from various faiths and cultural backgrounds may find K12 very appropriate for their families\' homeschool needs. Through its Virtual School programs, K12 offers kindergarten through 12th grade curriculum to families who desire to teach their children at home but prefer a public school program. The K12 Virtual school program is a partnership with local public schools systems providing a high quality online education to children in their homes.</p><p>The K12 curriculum covers six core subjects: language arts/English, math, science, history, art, and music and is based on time-tested and research-based methods of instruction. Students enrolled in a virtual school take their academic courses online with support from a certified public school teacher. The parent acts as a learning coach helping to keep the student successfully engaged in the learning process. Because they are part of the public school system, K12 Virtual Schools are tuition free to all students. This also means that K12 Virtual School families are 100% accountable to the public school system in which they are enrolled. K12 Virtual Schools are not available in all areas.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>If you\'re considering an online approach to homeschooling, remember that there is no one perfect homeschool curriculum. A blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Instead of feeling torn between different curricula, parents should be open to mixing and matching materials and resources to customize their child\'s education. Remember, each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3><strong>LIFEPAC from Alpha Omega Publications</strong></h3><p>LIFEPAC is a K-12 print-based curriculum offered by Alpha Omega Publications, a Christian-based publisher that provides curriculum and educational services to Christian schools and homeschool families. Full-color and consumable, LIFEPAC is an integrated, Bible-based curriculum developed by a team of accomplished educators and teachers. Using a mastery-based learning approach, each LIFEPAC course is comprised of ten student-directed worktexts that provide a full year of instruction. Individual worktexts may also be used to supplement the homeschool curriculum or to fill in learning gaps. LIFEPAC worktexts include self tests and unit tests to provide assessment of student progress. Teacher\'s guides offer practical teaching tips, additional learning activities, and complete answer keys.</p><p>An economical, proven homeschool curriculum, LIFEPAC integrates Scripture and a Christian worldview throughout all subjects. LIFEPAC offers the flexibility of an individualized, student-driven learning approach which encourages critical thinking and academic independence. Self-paced coursework for K-12 students includes Bible (1-12), language arts (K-12), math (K-12), science (K-12), and history and geography (1-12). Electives are also available for elementary, middle, and high school students. LIFEPAC subject kits include all required student and teacher materials. LIFEPAC curriculum can be purchased as complete grade level sets or as individual subjects. Student and teacher materials and supplemental materials may be purchased separately.</p><h3><strong>Benefits and Features of LIFEPAC from Alpha Omega Publications</strong>&nbsp;</h3><p class="list_heading">LIFEPAC</p><ul><li>full-color, print-based curriculum</li><li>offers progressive, self-paced individual unit worktexts</li><li>promotes concept mastery and cognitive reasoning skills</li><li>encourages academic independence in your children</li></ul><h3 class="para-start"><strong>Take a Closer Look at K12 Teacher-Supported Consumer Direct and LIFEPAC</strong></h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">LIFEPAC</th><th class="compare">K12 Teacher-Supported</th></tr>
</thead>
<tbody>
<tr>
<td>K-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Accredited program</td>
<td class="tac">&nbsp;</td>
<td class="tac">for 9th -12th grades</td>
</tr>
<tr>
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Student-directed curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Self-paced and individualized</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Separate unit worktexts available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Placement tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Economical</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Manageable amount of daily lesson material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Material designed for homeschoolers</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr><th class="features">Cost</th><th class="compare">Alpha Omega Publications</th><th class="compare">K12</th></tr>
</thead>
<tbody>
<tr>
<td>Average yearly cost per grade level**</td>
<td class="tac">$286.95</td>
<td class="tac">$2200.00</td>
</tr>
<tr class="alt">
<td>Average subject cost per grade level**</td>
<td class="tac">$58.95</td>
<td class="tac">$550</td>
</tr>
</tbody>
</table>
</div>
<h4><strong>Compare Offerings: K12 versus Alpha Omega Publications</strong></h4>
<p class="list_heading_links">K12 Independent Consumer Direct</p>
<ul>
<li><a href="/k12-independent-vs-monarch">K12 Independent Consumer Direct vs. Monarch</a></li>
<li><a href="/k12-independent-vs-aoa">K12 Independent Consumer Direct vs. Alpha Omega Academy</a></li>
<li><a href="/k12-independent-vs-horizons">K12 Independent Consumer Direct vs. Horizons</a></li>
<li><a href="/k12-independent-vs-lifepac">K12 Independent Consumer Direct vs. LIFEPAC</a></li>
<li><a href="/k12-independent-vs-sos">K12 Independent Consumer Direct vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">K12 Teacher-Supported Consumer Direct</p>
<ul>
<li><a href="/k12-teacher-supported-vs-monarch">K12 Teacher-Supported Consumer Direct vs. Monarch</a></li>
<li><a href="/k12-teacher-supported-vs-aoa">K12 Teacher-Supported Consumer Direct vs. Alpha Omega Academy</a></li>
<li><a href="/k12-teacher-supported-vs-horizons">K12 Teacher-Supported Consumer Direct vs. Horizons</a></li>
<li><a href="/k12-teacher-supported-vs-lifepac">K12 Teacher-Supported Consumer Direct vs. LIFEPAC</a></li>
<li><a href="/k12-teacher-supported-vs-sos">K12 Teacher-Supported Consumer Direct vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">K12 Virtual Schools</p>
<ul>
<li><a href="/k12-virtual-schools-vs-monarch">K12 Virtual Schools vs. Monarch</a></li>
<li><a href="/k12-virtual-schools-vs-aoa">K12 Virtual Schools vs. Alpha Omega Academy</a></li>
<li><a href="/k12-virtual-schools-vs-horizons">K12 Virtual Schools vs. Horizons</a></li>
<li><a href="/k12-virtual-schools-vs-lifepac">K12 Virtual Schools vs. LIFEPAC</a></li>
<li><a href="/k12-virtual-schools-vs-sos">K12 Virtual Schools vs. Switched-On Schoolhouse</a></li>
</ul>
<p>* K12 is the registered trademark of K12 Inc. Alpha Omega Publications is not in any way affiliated with K12. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by K12.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p><p>**Prices obtained February 2013 from the K12 website and from Alpha Omega Publications 2013 Homeschool Catalog. Yearly prices based on comparison between LIFEPAC 5th Grade 5-subject boxed set and direct purchase of four K-5 teacher-supported courses (not including some required materials). Individual subject prices are based on comparison between costs for LIFEPAC 5th Grade Math boxed set and direct purchase of a single K12 teacher-supported course for grades K-5 (not including some required materials). Costs do not reflect any additional fees or shipping and handling charges. Prices for other K12 and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '7d41e580-5b73-47fd-90ce-e5554406aed1';
  $display->content['new-7d41e580-5b73-47fd-90ce-e5554406aed1'] = $pane;
  $display->panels['left'][0] = 'new-7d41e580-5b73-47fd-90ce-e5554406aed1';
  $pane = new stdClass();
  $pane->pid = 'new-b87bef15-d9e2-411b-9b7d-014309b690fd';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'b87bef15-d9e2-411b-9b7d-014309b690fd';
  $display->content['new-b87bef15-d9e2-411b-9b7d-014309b690fd'] = $pane;
  $display->panels['right'][0] = 'new-b87bef15-d9e2-411b-9b7d-014309b690fd';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-7d41e580-5b73-47fd-90ce-e5554406aed1';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
