<?php
/**
 * @file
 * panels_layout.aop_homepage.inc
 */

$api = '2.0.0';

$data = $layout = new stdClass();
$layout->disabled = FALSE; /* Edit this to true to make a default layout disabled initially */
$layout->api_version = 1;
$layout->name = 'aop_homepage';
$layout->admin_title = 'AOP Homepage';
$layout->admin_description = 'AOP Homepage ';
$layout->category = 'AOP';
$layout->plugin = 'flexible';
$layout->settings = array(
  'items' => array(
    'canvas' => array(
      'type' => 'row',
      'contains' => 'column',
      'children' => array(
        0 => 'main',
      ),
      'parent' => NULL,
      'class' => '',
      'column_class' => '',
      'row_class' => '',
      'region_class' => '',
      'no_scale' => TRUE,
      'fixed_width' => '',
      'column_separation' => '0.5em',
      'region_separation' => '0.5em',
      'row_separation' => '0.5em',
    ),
    'main' => array(
      'type' => 'column',
      'width' => 100,
      'width_type' => '%',
      'children' => array(
        0 => 'main-row',
        1 => 1,
        2 => 2,
        3 => 3,
        4 => 4,
        5 => 5,
      ),
      'parent' => 'canvas',
    ),
    'main-row' => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'center',
      ),
      'parent' => 'main',
    ),
    'center' => array(
      'type' => 'region',
      'title' => 'Center',
      'width' => 100,
      'width_type' => '%',
      'parent' => 'main-row',
    ),
    1 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'homepage_hero_left',
        1 => 'homepage_hero_right',
      ),
      'parent' => 'main',
      'class' => 'aop-homepage-hero',
    ),
    'homepage_hero_left' => array(
      'type' => 'region',
      'title' => 'Homepage Hero Left',
      'width' => '70.0034365628805',
      'width_type' => '%',
      'parent' => '1',
      'class' => 'homepage-hero-left',
    ),
    'homepage_hero_right' => array(
      'type' => 'region',
      'title' => 'Homepage Hero Right',
      'width' => '29.99656343711951',
      'width_type' => '%',
      'parent' => '1',
      'class' => 'homepage-hero-right',
    ),
    2 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'homepage_carousel',
      ),
      'parent' => 'main',
      'class' => 'homepage-carousel',
    ),
    'homepage_carousel' => array(
      'type' => 'region',
      'title' => 'Homepage Carousel',
      'width' => 100,
      'width_type' => '%',
      'parent' => '2',
      'class' => 'homepage-carousel-inner',
    ),
    3 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'homepage_recent_stories',
        1 => 'homepage_popular_articles',
      ),
      'parent' => 'main',
      'class' => 'homepage-stories-and-articles',
    ),
    'homepage_recent_stories' => array(
      'type' => 'region',
      'title' => 'Homepage Recent Stories',
      'width' => '70.0034365628805',
      'width_type' => '%',
      'parent' => '3',
      'class' => 'homepage-recent-stories',
    ),
    'homepage_popular_articles' => array(
      'type' => 'region',
      'title' => 'Homepage Popular Articles',
      'width' => '29.99656343711951',
      'width_type' => '%',
      'parent' => '3',
      'class' => 'homepage-popular-articles',
    ),
    4 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'homepage_bottom_grid_item_1',
        1 => 'homepage_bottom_grid_item_2',
      ),
      'parent' => 'main',
      'class' => 'homepage-bottom-grid-row-1',
    ),
    'homepage_bottom_grid_item_1' => array(
      'type' => 'region',
      'title' => 'Homepage Bottom Grid Item 1',
      'width' => 50,
      'width_type' => '%',
      'parent' => '4',
      'class' => 'homepage-bottom-grid-1',
    ),
    'homepage_bottom_grid_item_2' => array(
      'type' => 'region',
      'title' => 'Homepage Bottom Grid Item 2',
      'width' => 50,
      'width_type' => '%',
      'parent' => '4',
      'class' => 'homepage-bottom-grid-2',
    ),
    5 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'homepage_bottom_grid_item_3',
        1 => 'homepage_bottom_grid_item_3_',
      ),
      'parent' => 'main',
      'class' => 'homepage-bottom-grid-row-2',
    ),
    'homepage_bottom_grid_item_3' => array(
      'type' => 'region',
      'title' => 'Homepage Bottom Grid Item 3',
      'width' => 50,
      'width_type' => '%',
      'parent' => '5',
      'class' => 'homepage-bottom-grid-3',
    ),
    'homepage_bottom_grid_item_3_' => array(
      'type' => 'region',
      'title' => 'Homepage Bottom Grid Item 3',
      'width' => 50,
      'width_type' => '%',
      'parent' => '5',
      'class' => 'homepage-bottom-grid-4',
    ),
  ),
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'panels',
);
