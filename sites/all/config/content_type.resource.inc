<?php
/**
 * @file
 * content_type.resource.inc
 */

$api = '2.0.0';

$data = (object) array(
  'base' => 'node_content',
  'description' => '',
  'has_title' => '1',
  'help' => '',
  'name' => 'Resource',
  'title_label' => 'Title',
  'type' => 'resource',
);

$dependencies = array();

$optional = array(
  'field.node.field_product.resource' => 'field.node.field_product.resource',
  'field.node.field_search_boost.resource' => 'field.node.field_search_boost.resource',
  'field.node.title_field.resource' => 'field.node.title_field.resource',
  'permission.create_resource_content' => 'permission.create_resource_content',
  'permission.delete_any_resource_content' => 'permission.delete_any_resource_content',
  'permission.delete_own_resource_content' => 'permission.delete_own_resource_content',
  'permission.edit_any_resource_content' => 'permission.edit_any_resource_content',
  'permission.edit_own_resource_content' => 'permission.edit_own_resource_content',
);

$modules = array(
  0 => 'node',
);
