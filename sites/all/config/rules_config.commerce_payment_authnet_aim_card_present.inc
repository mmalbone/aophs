<?php
/**
 * @file
 * rules_config.commerce_payment_authnet_aim_card_present.inc
 */

$api = '2.0.0';

$data = entity_import('rules_config', '{ "commerce_payment_authnet_aim_card_present" : {
      "LABEL" : "Authorize.Net AIM - Card Present (Dev \\u0026 Test)",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "OWNER" : "rules",
      "TAGS" : [ "Commerce Payment" ],
      "REQUIRES" : [ "php", "rules", "commerce_payment" ],
      "ON" : { "commerce_payment_methods" : [] },
      "IF" : [
        { "php_eval" : { "code" : "if (isset($_SERVER[\\u0027PANTHEON_ENVIRONMENT\\u0027]) \\u0026\\u0026\\r\\n  $_SERVER[\\u0027PANTHEON_ENVIRONMENT\\u0027] != \\u0027live\\u0027) {\\r\\n      return true;\\r\\n  } else { \\r\\n     return false;\\r\\n}" } }
      ],
      "DO" : [ { "commerce_payment_enable_authnet_aim_card_present" : [] } ]
    }
  }');

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'entity',
  1 => 'commerce_payment',
  2 => 'php',
  3 => 'rules',
);
