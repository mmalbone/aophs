<?php
/**
 * @file
 * views_view.subscriptions.inc
 */

$api = '2.0.0';

$data = $view = new view();
$view->name = 'subscriptions';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Subscriptions';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '25';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['quantity'] = '9';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'field_customer_uid' => 'field_customer_uid',
  'field_monarch_product_id' => 'field_monarch_product_id',
  'title' => 'title',
  'field_subscription_id' => 'field_subscription_id',
  'field_activation_date' => 'field_activation_date',
  'field_date_last_billed' => 'field_date_last_billed',
  'field_subscription_expiration_da' => 'field_subscription_expiration_da',
  'field_renewal_flag' => 'field_renewal_flag',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'field_customer_uid' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_monarch_product_id' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'title' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_subscription_id' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_activation_date' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_date_last_billed' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_subscription_expiration_da' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_renewal_flag' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Footer: Global: Text area */
$handler->display->display_options['footer']['area']['id'] = 'area';
$handler->display->display_options['footer']['area']['table'] = 'views';
$handler->display->display_options['footer']['area']['field'] = 'area';
$handler->display->display_options['footer']['area']['content'] = 'Subscription status codes:  <b>A</b>: Active,  <b>I</b>: Inactive, <b>P</b>: Pending Activation';
$handler->display->display_options['footer']['area']['format'] = 'full_html';
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['empty'] = TRUE;
$handler->display->display_options['empty']['area']['content'] = 'We are sorry,  we were unable to find any subscriptions associated with your account.   Please contact customer service if you have Alpha Omega product subscriptions and wish to have them added to your account.';
$handler->display->display_options['empty']['area']['format'] = 'plain_text';
/* Field: Content: Renewal Flag */
$handler->display->display_options['fields']['field_renewal_flag']['id'] = 'field_renewal_flag';
$handler->display->display_options['fields']['field_renewal_flag']['table'] = 'field_data_field_renewal_flag';
$handler->display->display_options['fields']['field_renewal_flag']['field'] = 'field_renewal_flag';
$handler->display->display_options['fields']['field_renewal_flag']['label'] = '';
$handler->display->display_options['fields']['field_renewal_flag']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_renewal_flag']['element_type'] = '0';
$handler->display->display_options['fields']['field_renewal_flag']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_renewal_flag']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_renewal_flag']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_renewal_flag']['field_api_classes'] = TRUE;
/* Field: Content: Customer UID */
$handler->display->display_options['fields']['field_customer_uid']['id'] = 'field_customer_uid';
$handler->display->display_options['fields']['field_customer_uid']['table'] = 'field_data_field_customer_uid';
$handler->display->display_options['fields']['field_customer_uid']['field'] = 'field_customer_uid';
$handler->display->display_options['fields']['field_customer_uid']['label'] = '';
$handler->display->display_options['fields']['field_customer_uid']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_customer_uid']['element_type'] = '0';
$handler->display->display_options['fields']['field_customer_uid']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_customer_uid']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_customer_uid']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_customer_uid']['field_api_classes'] = TRUE;
/* Field: Content: Monarch Product ID */
$handler->display->display_options['fields']['field_monarch_product_id']['id'] = 'field_monarch_product_id';
$handler->display->display_options['fields']['field_monarch_product_id']['table'] = 'field_data_field_monarch_product_id';
$handler->display->display_options['fields']['field_monarch_product_id']['field'] = 'field_monarch_product_id';
$handler->display->display_options['fields']['field_monarch_product_id']['label'] = 'Product ID';
$handler->display->display_options['fields']['field_monarch_product_id']['element_type'] = '0';
$handler->display->display_options['fields']['field_monarch_product_id']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_monarch_product_id']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_monarch_product_id']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_monarch_product_id']['field_api_classes'] = TRUE;
/* Field: Content: Monarch Product Title */
$handler->display->display_options['fields']['field_monarch_product_title']['id'] = 'field_monarch_product_title';
$handler->display->display_options['fields']['field_monarch_product_title']['table'] = 'field_data_field_monarch_product_title';
$handler->display->display_options['fields']['field_monarch_product_title']['field'] = 'field_monarch_product_title';
$handler->display->display_options['fields']['field_monarch_product_title']['label'] = 'Product ';
$handler->display->display_options['fields']['field_monarch_product_title']['element_type'] = '0';
$handler->display->display_options['fields']['field_monarch_product_title']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_monarch_product_title']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_monarch_product_title']['field_api_classes'] = TRUE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = 'Activation Code';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
$handler->display->display_options['fields']['title']['node_in_colorbox_width'] = '600';
$handler->display->display_options['fields']['title']['node_in_colorbox_height'] = '600';
$handler->display->display_options['fields']['title']['node_in_colorbox_rel'] = '';
/* Field: Content: Activation Date */
$handler->display->display_options['fields']['field_activation_date']['id'] = 'field_activation_date';
$handler->display->display_options['fields']['field_activation_date']['table'] = 'field_data_field_activation_date';
$handler->display->display_options['fields']['field_activation_date']['field'] = 'field_activation_date';
$handler->display->display_options['fields']['field_activation_date']['element_type'] = '0';
$handler->display->display_options['fields']['field_activation_date']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_activation_date']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_activation_date']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_activation_date']['settings'] = array(
  'format_type' => 'short',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
);
$handler->display->display_options['fields']['field_activation_date']['field_api_classes'] = TRUE;
/* Field: Content: Date Last Billed */
$handler->display->display_options['fields']['field_date_last_billed']['id'] = 'field_date_last_billed';
$handler->display->display_options['fields']['field_date_last_billed']['table'] = 'field_data_field_date_last_billed';
$handler->display->display_options['fields']['field_date_last_billed']['field'] = 'field_date_last_billed';
$handler->display->display_options['fields']['field_date_last_billed']['label'] = 'Last Billing Date';
$handler->display->display_options['fields']['field_date_last_billed']['element_type'] = '0';
$handler->display->display_options['fields']['field_date_last_billed']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_date_last_billed']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_date_last_billed']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_date_last_billed']['settings'] = array(
  'format_type' => 'short',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
);
$handler->display->display_options['fields']['field_date_last_billed']['field_api_classes'] = TRUE;
/* Field: Content: Subscription Expiration Date */
$handler->display->display_options['fields']['field_subscription_expiration_da']['id'] = 'field_subscription_expiration_da';
$handler->display->display_options['fields']['field_subscription_expiration_da']['table'] = 'field_data_field_subscription_expiration_da';
$handler->display->display_options['fields']['field_subscription_expiration_da']['field'] = 'field_subscription_expiration_da';
$handler->display->display_options['fields']['field_subscription_expiration_da']['label'] = 'Expiration Date';
$handler->display->display_options['fields']['field_subscription_expiration_da']['element_type'] = '0';
$handler->display->display_options['fields']['field_subscription_expiration_da']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_subscription_expiration_da']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_subscription_expiration_da']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_subscription_expiration_da']['settings'] = array(
  'format_type' => 'short',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
);
$handler->display->display_options['fields']['field_subscription_expiration_da']['field_api_classes'] = TRUE;
/* Field: subscription_status */
$handler->display->display_options['fields']['field_active_flag']['id'] = 'field_active_flag';
$handler->display->display_options['fields']['field_active_flag']['table'] = 'field_data_field_active_flag';
$handler->display->display_options['fields']['field_active_flag']['field'] = 'field_active_flag';
$handler->display->display_options['fields']['field_active_flag']['ui_name'] = 'subscription_status';
$handler->display->display_options['fields']['field_active_flag']['label'] = 'Status';
$handler->display->display_options['fields']['field_active_flag']['element_type'] = '0';
$handler->display->display_options['fields']['field_active_flag']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_active_flag']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_active_flag']['field_api_classes'] = TRUE;
/* Field: Content: Monarch Product URL */
$handler->display->display_options['fields']['field_monarch_product_url']['id'] = 'field_monarch_product_url';
$handler->display->display_options['fields']['field_monarch_product_url']['table'] = 'field_data_field_monarch_product_url';
$handler->display->display_options['fields']['field_monarch_product_url']['field'] = 'field_monarch_product_url';
$handler->display->display_options['fields']['field_monarch_product_url']['label'] = '';
$handler->display->display_options['fields']['field_monarch_product_url']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_monarch_product_url']['element_type'] = '0';
$handler->display->display_options['fields']['field_monarch_product_url']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_monarch_product_url']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_monarch_product_url']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_monarch_product_url']['field_api_classes'] = TRUE;
/* Field: subscription-renewal-link */
$handler->display->display_options['fields']['nothing']['id'] = 'nothing';
$handler->display->display_options['fields']['nothing']['table'] = 'views';
$handler->display->display_options['fields']['nothing']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing']['ui_name'] = 'subscription-renewal-link';
$handler->display->display_options['fields']['nothing']['label'] = 'Renew';
$handler->display->display_options['fields']['nothing']['exclude'] = TRUE;
$handler->display->display_options['fields']['nothing']['alter']['text'] = 'Renew';
$handler->display->display_options['fields']['nothing']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['nothing']['alter']['path'] = 'subscriptions/renew/[title]';
/* Field: Global: Custom text */
$handler->display->display_options['fields']['nothing_1']['id'] = 'nothing_1';
$handler->display->display_options['fields']['nothing_1']['table'] = 'views';
$handler->display->display_options['fields']['nothing_1']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing_1']['label'] = 'Cancel';
$handler->display->display_options['fields']['nothing_1']['exclude'] = TRUE;
$handler->display->display_options['fields']['nothing_1']['alter']['text'] = 'Cancel';
$handler->display->display_options['fields']['nothing_1']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['nothing_1']['alter']['path'] = 'subscriptions/cancel/[title]?destination=user/[field_customer_uid]/subscriptions';
/* Field: subscription_action */
$handler->display->display_options['fields']['nothing_2']['id'] = 'nothing_2';
$handler->display->display_options['fields']['nothing_2']['table'] = 'views';
$handler->display->display_options['fields']['nothing_2']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing_2']['ui_name'] = 'subscription_action';
$handler->display->display_options['fields']['nothing_2']['label'] = 'Action';
/* Field: Field: Subscription Renewal Period */
$handler->display->display_options['fields']['field_subscription_renewal_perio']['id'] = 'field_subscription_renewal_perio';
$handler->display->display_options['fields']['field_subscription_renewal_perio']['table'] = 'field_data_field_subscription_renewal_perio';
$handler->display->display_options['fields']['field_subscription_renewal_perio']['field'] = 'field_subscription_renewal_perio';
$handler->display->display_options['fields']['field_subscription_renewal_perio']['label'] = '';
$handler->display->display_options['fields']['field_subscription_renewal_perio']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_subscription_renewal_perio']['element_type'] = '0';
$handler->display->display_options['fields']['field_subscription_renewal_perio']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_subscription_renewal_perio']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_subscription_renewal_perio']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_subscription_renewal_perio']['field_api_classes'] = TRUE;
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Contextual filter: Subscriber User ID */
$handler->display->display_options['arguments']['field_customer_uid_value']['id'] = 'field_customer_uid_value';
$handler->display->display_options['arguments']['field_customer_uid_value']['table'] = 'field_data_field_customer_uid';
$handler->display->display_options['arguments']['field_customer_uid_value']['field'] = 'field_customer_uid_value';
$handler->display->display_options['arguments']['field_customer_uid_value']['ui_name'] = 'Subscriber User ID';
$handler->display->display_options['arguments']['field_customer_uid_value']['default_action'] = 'empty';
$handler->display->display_options['arguments']['field_customer_uid_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_customer_uid_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_customer_uid_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_customer_uid_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_customer_uid_value']['limit'] = '0';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = '0';
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'aop_subscription' => 'aop_subscription',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'subscriptions_listing');
$handler->display->display_options['path'] = 'user/%/subscriptions';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Subscriptions';
$handler->display->display_options['menu']['weight'] = '13';
$handler->display->display_options['menu']['name'] = 'user-menu';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'views',
  1 => 'node',
  2 => 'text',
  3 => 'date',
  4 => 'list',
);
