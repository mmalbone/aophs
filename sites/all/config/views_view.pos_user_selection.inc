<?php
/**
 * @file
 * views_view.pos_user_selection.inc
 */

$api = '2.0.0';

$data = $view = new view();
$view->name = 'pos_user_selection';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'users';
$view->human_name = 'POS User Selection';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_ajax'] = TRUE;
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'access user profiles';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'name' => 'name',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'name' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Field: Broken/missing handler */
$handler->display->display_options['fields']['pos_set_user']['id'] = 'pos_set_user';
$handler->display->display_options['fields']['pos_set_user']['table'] = 'users';
$handler->display->display_options['fields']['pos_set_user']['field'] = 'pos_set_user';
$handler->display->display_options['fields']['pos_set_user']['label'] = '';
/* Field: User: Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'users';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['label'] = 'Username';
$handler->display->display_options['fields']['name']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['name']['alter']['ellipsis'] = FALSE;
/* Field: User: E-mail */
$handler->display->display_options['fields']['mail']['id'] = 'mail';
$handler->display->display_options['fields']['mail']['table'] = 'users';
$handler->display->display_options['fields']['mail']['field'] = 'mail';
$handler->display->display_options['fields']['mail']['link_to_user'] = '0';
/* Sort criterion: User: Created date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'users';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Filter criterion: Field: First Name (field_firstname) */
$handler->display->display_options['filters']['field_firstname_value']['id'] = 'field_firstname_value';
$handler->display->display_options['filters']['field_firstname_value']['table'] = 'field_data_field_firstname';
$handler->display->display_options['filters']['field_firstname_value']['field'] = 'field_firstname_value';
$handler->display->display_options['filters']['field_firstname_value']['operator'] = 'contains';
$handler->display->display_options['filters']['field_firstname_value']['group'] = 1;
$handler->display->display_options['filters']['field_firstname_value']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_firstname_value']['expose']['operator_id'] = 'field_firstname_value_op';
$handler->display->display_options['filters']['field_firstname_value']['expose']['label'] = 'First Name';
$handler->display->display_options['filters']['field_firstname_value']['expose']['operator'] = 'field_firstname_value_op';
$handler->display->display_options['filters']['field_firstname_value']['expose']['identifier'] = 'field_firstname_value';
$handler->display->display_options['filters']['field_firstname_value']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  6 => 0,
  7 => 0,
  8 => 0,
  5 => 0,
  3 => 0,
  4 => 0,
);
/* Filter criterion: Field: Last Name (field_lastname) */
$handler->display->display_options['filters']['field_lastname_value']['id'] = 'field_lastname_value';
$handler->display->display_options['filters']['field_lastname_value']['table'] = 'field_data_field_lastname';
$handler->display->display_options['filters']['field_lastname_value']['field'] = 'field_lastname_value';
$handler->display->display_options['filters']['field_lastname_value']['operator'] = 'contains';
$handler->display->display_options['filters']['field_lastname_value']['group'] = 1;
$handler->display->display_options['filters']['field_lastname_value']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_lastname_value']['expose']['operator_id'] = 'field_lastname_value_op';
$handler->display->display_options['filters']['field_lastname_value']['expose']['label'] = 'Last Name';
$handler->display->display_options['filters']['field_lastname_value']['expose']['operator'] = 'field_lastname_value_op';
$handler->display->display_options['filters']['field_lastname_value']['expose']['identifier'] = 'field_lastname_value';
$handler->display->display_options['filters']['field_lastname_value']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  6 => 0,
  7 => 0,
  8 => 0,
  5 => 0,
  3 => 0,
  4 => 0,
);
/* Filter criterion: User: E-mail */
$handler->display->display_options['filters']['mail']['id'] = 'mail';
$handler->display->display_options['filters']['mail']['table'] = 'users';
$handler->display->display_options['filters']['mail']['field'] = 'mail';
$handler->display->display_options['filters']['mail']['operator'] = 'starts';
$handler->display->display_options['filters']['mail']['group'] = 1;
$handler->display->display_options['filters']['mail']['exposed'] = TRUE;
$handler->display->display_options['filters']['mail']['expose']['operator_id'] = 'mail_op';
$handler->display->display_options['filters']['mail']['expose']['label'] = 'E-mail';
$handler->display->display_options['filters']['mail']['expose']['operator'] = 'mail_op';
$handler->display->display_options['filters']['mail']['expose']['identifier'] = 'mail';


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'views',
  1 => 'user',
);
