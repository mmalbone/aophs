<?php
/**
 * @file
 * vocabulary.grade_level.inc
 */

$api = '2.0.0';

$data = (object) array(
  'description' => '',
  'hierarchy' => '0',
  'machine_name' => 'grade_level',
  'module' => 'taxonomy',
  'name' => 'Grade Level',
  'vid' => '22',
  'weight' => '0',
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'taxonomy',
);
