<?php
/**
 * @file
 * page_manager_handlers.page_sos_webinars_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_sos_webinars_panel_context';
$handler->task = 'page';
$handler->subtask = 'sos_webinars';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '3d4b7001-085b-4ebc-8fad-0ae42bdbb565';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-26e9bd25-4af3-464e-b7a2-b95675496757';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => '',
    'body' => ' <h1>Discover the SOS Difference! Attend a Free SOS Webinar Now!</h1>

<p><strong>Who should attend SOS webinars?</strong> These educational opportunities are invaluable to both homeschool parents and children interested in learning how to use Switched-On Schoolhouse CD-ROM cu<del></del>rriculum from Alpha Omega Publications&reg;. To attend, simply pick the webinar of interest to you below and click "Register for Webinar."</p>
<p>Please note: Webinar registrations must be received at least thirty minutes prior to scheduled start times.</p>
<div id="webinar_area">
  <script language="javascript" type="text/javascript">
    function verifyWebinar() {
      for (i=0; i<document.form1.webinar_id.length; i++) {
        if (form1.webinar_id[i].checked) return true
      }
      alert(\'Please select at least one available date.\')
      return false
    }
    function toggleText(id) {
      var data = document.getElementById(id).innerHTML;
      if (data == \'More Information\') { document.getElementById(id).innerHTML = \'Less Information\'; }
      else { document.getElementById(id).innerHTML = \'More Information\'; }
    }
  </script>


  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tbody><tr>
      <td>
        <!-- Row Header Section End -->
                              <table width="100%" bgcolor="#FCFCFC" border="0" cellpadding="0" cellspacing="0">
              <tbody><tr>
                <!-- Begin Data Row -->	<!-- Begin Toolbar -->
                <!-- End Toolbar -->
                <td valign="top" bgcolor="#e1e1e1" height="1"></td>
              </tr>
              <tr>
                <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tbody><tr height="34">
                      <td width="41" height="34"><div class="fieldlabel3" align="left">
                          <div align="center"><img src="https://www.aophomeschooling.com/skin/frontend/enterprise/aophomeschooling/images/doc.gif" alt="Webinar Title" width="16" border="0" height="16"></div>
                        </div></td>
                      <td width="700" height="34"><strong>
                          Before You Buy - Preview SOS!					</strong></td>
                      <td width="221" height="34"></td>
                      <td width="42"></td>
                    </tr>
                    <tr>
                      <td colspan="4" class="fieldlabel3"><div style="padding-left:6px; padding-right:6px" align="justify">
                          <p>Before making the investment in Switched-On Schoolhouse, get a solid overview of the curriculum with this free training webinar! Hosted live by a training specialist from AOP, this 30-minute overview presents a detailed summary of the benefits and resources in SOS. Conducted via telephone as you follow along online, this webinar is sure to give you the confidence you need to choose the right homeschool curriculum for your family.</p>							<span id="24" style="display:none"><br>
											<p>This introductory webinar provides a thorough outline of the curriculum with a focus on key features and functions. This session also offers tips for easy access to areas throughout the curriculum and provides examples of lessons, tests, and quizzes. In addition, attendees explore the customizable calendar to learn how to schedule and adjust school days and work flow. Plus, you\'ll take a peek at the automatic grading system and view how students work independently.

Several time slots are available to fit your family\'s needs. To sign up for the free "Getting to Know SOS" training webinar, check the box before the date and time that\'s most convenient for you and click "Register."</p>							</span></div></td>
                    </tr>
                    <tr height="34">
                      <td width="41" height="40"><div class="fieldlabel3" align="left">
                          <div align="center"><a href="#" class="invisible" onclick="hidetoggle(\'24\'); toggleText(\'series_info24\');"><img src="https://www.aophomeschooling.com/skin/frontend/enterprise/aophomeschooling/images/info.gif" alt="More Info" width="16" border="0" height="16"></a></div>
                        </div></td>
                      <td align="left" height="40"><a href="#" class="invisible" onclick="hidetoggle(\'24\'); toggleText(\'series_info24\', \'Less Information\'); return false;"><span id="series_info24">More Information</span></a></td>
                      <td height="40"><div align="right">
                          <a id = "1" href="/webinar/index/soswebinarregform/series/1" class="invisible"><strong>Register for a Webinar</strong></a></div></td>
                      <td width="42">				  <div align="center"><img src="https://www.aophomeschooling.com/skin/frontend/enterprise/aophomeschooling/images/register.gif" alt="Register Now" width="16" border="0" height="16"></div></td>
                    </tr>
                    </tbody></table></td>
              </tr>
              <!--	</form> -->
              <!-- End Webinars -->
              <tr>
                <td valign="top" bgcolor="#e1e1e1" height="1"></td>
              </tr>
              </tbody></table>
                      <table width="100%" bgcolor="#FCFCFC" border="0" cellpadding="0" cellspacing="0">
              <tbody><tr>
                <!-- Begin Data Row -->	<!-- Begin Toolbar -->
                <!-- End Toolbar -->
                <td valign="top" bgcolor="#e1e1e1" height="1"></td>
              </tr>
              <tr>
                <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tbody><tr height="34">
                      <td width="41" height="34"><div class="fieldlabel3" align="left">
                          <div align="center"><img src="https://www.aophomeschooling.com/skin/frontend/enterprise/aophomeschooling/images/doc.gif" alt="Webinar Title" width="16" border="0" height="16"></div>
                        </div></td>
                      <td width="700" height="34"><strong>
                          New SOS Users - Getting Started!					</strong></td>
                      <td width="221" height="34"></td>
                      <td width="42"></td>
                    </tr>
                    <tr>
                      <td colspan="4" class="fieldlabel3"><div style="padding-left:6px; padding-right:6px" align="justify">
                          <p>Explore your child\'s new curriculum with a free Switched-On Schoolhouse webinar from an AOP training specialist! Now, homeschooling parents new to SOS can enroll in a live webinar hosted by a qualified AOP trainer who cheerfully introduces you to the curriculum. Conducted via telephone as you follow along online, this one-hour webinar gives you the confidence you need to find homeschool success with SOS.</p>							<span id="24" style="display:none"><br>
											<p>This webinar guides you through the basics of using your child\'s new curriculum, including how to assign subjects, select your school\'s start and end dates on the customizable calendar, generate reports that help you follow your student\'s progress, and unassign or block unwanted lessons and school days. Plus, you\'ll receive handy user tips that will save valuable time in your homeschooling, and you\'ll have an opportunity to get answers to your SOS questions!

Several time slots are available to fit your family\'s needs. To sign up for the free "Getting Started with SOS" training webinar, simply check the box before the date and time that?s most convenient for you and click "Register."</p>							</span></div></td>
                    </tr>
                    <tr height="34">
                      <td width="41" height="40"><div class="fieldlabel3" align="left">
                          <div align="center"><a href="#" class="invisible" onclick="hidetoggle(\'24\'); toggleText(\'series_info24\');"><img src="https://www.aophomeschooling.com/skin/frontend/enterprise/aophomeschooling/images/info.gif" alt="More Info" width="16" border="0" height="16"></a></div>
                        </div></td>
                      <td align="left" height="40"><a href="#" class="invisible" onclick="hidetoggle(\'24\'); toggleText(\'series_info24\', \'Less Information\'); return false;"><span id="series_info24">More Information</span></a></td>
                      <td height="40"><div align="right">
                          <a id = "2" href="/webinar/index/soswebinarregform/series/2" class="invisible"><strong>Register for a Webinar</strong></a></div></td>
                      <td width="42">				  <div align="center"><img src="https://www.aophomeschooling.com/skin/frontend/enterprise/aophomeschooling/images/register.gif" alt="Register Now" width="16" border="0" height="16"></div></td>
                    </tr>
                    </tbody></table></td>
              </tr>
              <!--	</form> -->
              <!-- End Webinars -->
              <tr>
                <td valign="top" bgcolor="#e1e1e1" height="1"></td>
              </tr>
              </tbody></table>
                      <table width="100%" bgcolor="#FCFCFC" border="0" cellpadding="0" cellspacing="0">
              <tbody><tr>
                <!-- Begin Data Row -->	<!-- Begin Toolbar -->
                <!-- End Toolbar -->
                <td valign="top" bgcolor="#e1e1e1" height="1"></td>
              </tr>
              <tr>
                <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tbody><tr height="34">
                      <td width="41" height="34"><div class="fieldlabel3" align="left">
                          <div align="center"><img src="https://www.aophomeschooling.com/skin/frontend/enterprise/aophomeschooling/images/doc.gif" alt="Webinar Title" width="16" border="0" height="16"></div>
                        </div></td>
                      <td width="700" height="34"><strong>
                          SOS Q&A					</strong></td>
                      <td width="221" height="34"></td>
                      <td width="42"></td>
                    </tr>
                    <tr>
                      <td colspan="4" class="fieldlabel3"><div style="padding-left:6px; padding-right:6px" align="justify">
                          <p>Learn how to navigate through Switched-On Schoolhouse more quickly and become more familiar with your child\'s curriculum with this free educational webinar for current SOS users. Hosted by a training specialist from AOP, this webinar provides an open forum on the areas in the curriculum you find most challenging. Conducted via telephone as you follow along online, this webinar helps you find homeschool success with SOS.</p>							<span id="24" style="display:none"><br>
											<p>Whether you want to gain a better grasp of using the customizable calendar, generating reports, understanding the grading scale, or locating the answer keys, this session helps users become more proficient with SOS through a review of the curriculum and a question and answer session that listens to your concerns and answers your specific questions.

To sign up for this weekly "SOS Q & A" training webinar, simply check the box before the date and click "Register."</p>							</span></div></td>
                    </tr>
                    <tr height="34">
                      <td width="41" height="40"><div class="fieldlabel3" align="left">
                          <div align="center"><a href="#" class="invisible" onclick="hidetoggle(\'24\'); toggleText(\'series_info24\');"><img src="https://www.aophomeschooling.com/skin/frontend/enterprise/aophomeschooling/images/info.gif" alt="More Info" width="16" border="0" height="16"></a></div>
                        </div></td>
                      <td align="left" height="40"><a href="#" class="invisible" onclick="hidetoggle(\'24\'); toggleText(\'series_info24\', \'Less Information\'); return false;"><span id="series_info24">More Information</span></a></td>
                      <td height="40"><div align="right">
                          <a id = "3" href="/webinar/index/soswebinarregform/series/3" class="invisible"><strong>Register for a Webinar</strong></a></div></td>
                      <td width="42">				  <div align="center"><img src="https://www.aophomeschooling.com/skin/frontend/enterprise/aophomeschooling/images/register.gif" alt="Register Now" width="16" border="0" height="16"></div></td>
                    </tr>
                    </tbody></table></td>
              </tr>
              <!--	</form> -->
              <!-- End Webinars -->
              <tr>
                <td valign="top" bgcolor="#e1e1e1" height="1"></td>
              </tr>
              </tbody></table>
                    <br>
          <!-- End Data Row -->
          <!-- Row Header Section End -->

              </td>
    </tr>
    </tbody></table>
  <br>      </div>


<div class="clear"></div>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '26e9bd25-4af3-464e-b7a2-b95675496757';
  $display->content['new-26e9bd25-4af3-464e-b7a2-b95675496757'] = $pane;
  $display->panels['middle'][0] = 'new-26e9bd25-4af3-464e-b7a2-b95675496757';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-26e9bd25-4af3-464e-b7a2-b95675496757';
$handler->conf['display'] = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
