<?php
/**
 * @file
 * permission.search_panel_content.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'search panel content',
  'roles' => array(
    0 => 'administrator',
    1 => 'anonymous user',
    2 => 'authenticated user',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'search_config',
);
