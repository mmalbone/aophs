<?php
/**
 * @file
 * page_manager_pages.aop_homepage.inc
 */

$api = '2.0.0';

$data = $page = new stdClass();
$page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
$page->api_version = 1;
$page->name = 'aop_homepage';
$page->task = 'page';
$page->admin_title = 'AOP Homepage';
$page->admin_description = 'AOP Homepage';
$page->path = 'home';
$page->access = array();
$page->menu = array();
$page->arguments = array();
$page->conf = array(
  'admin_paths' => FALSE,
);


$dependencies = array(
  'page_manager_handlers.page_aop_homepage_panel_context' => 'page_manager_handlers.page_aop_homepage_panel_context',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
