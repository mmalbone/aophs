<?php
/**
 * @file
 * field.fieldable_panels_pane.field_title_type.fieldable_panels_pane.inc
 */

$api = '2.0.0';

$data = array(
  'field_config' => array(
    'active' => '1',
    'cardinality' => '1',
    'columns' => array(
      'value' => array(
        'length' => 255,
        'not null' => FALSE,
        'type' => 'varchar',
      ),
    ),
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_title_type',
    'field_permissions' => array(
      'type' => '0',
    ),
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => '0',
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'h1' => 'H1',
        'h2' => 'H2',
        'h2f' => 'H2 Fancy',
        'h3' => 'H3',
        'h4' => 'H4',
        'h4b' => 'H4 Blue',
      ),
      'allowed_values_function' => '',
    ),
    'storage' => array(
      'active' => '1',
      'details' => array(
        'sql' => array(
          'FIELD_LOAD_CURRENT' => array(
            'field_data_field_title_type' => array(
              'value' => 'field_title_type_value',
            ),
          ),
          'FIELD_LOAD_REVISION' => array(
            'field_revision_field_title_type' => array(
              'value' => 'field_title_type_value',
            ),
          ),
        ),
      ),
      'module' => 'field_sql_storage',
      'settings' => array(),
      'type' => 'field_sql_storage',
    ),
    'translatable' => '0',
    'type' => 'list_text',
  ),
  'field_instance' => array(
    'bundle' => 'fieldable_panels_pane',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => '2',
      ),
    ),
    'entity_type' => 'fieldable_panels_pane',
    'fences_wrapper' => '',
    'field_name' => 'field_title_type',
    'label' => 'Title Type',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'apply_chosen' => '',
      ),
      'type' => 'options_select',
      'weight' => '-4',
    ),
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'field_sql_storage',
  1 => 'list',
  2 => 'options',
);
