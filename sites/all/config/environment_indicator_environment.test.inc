<?php
/**
 * @file
 * environment_indicator_environment.test.inc
 */

$api = '2.0.0';

$data = $environment = new stdClass();
$environment->disabled = FALSE; /* Edit this to true to make a default environment disabled initially */
$environment->api_version = 1;
$environment->machine = 'test';
$environment->name = 'Test';
$environment->regexurl = 'test-aophs.gotpantheon.com';
$environment->settings = array(
  'color' => '#d0d0d0',
  'text_color' => '#ffffff',
  'weight' => '',
  'position' => 'top',
  'fixed' => 0,
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'environment_indicator',
);
