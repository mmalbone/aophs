<?php
/**
 * @file
 * rules_config.commerce_payment_commerce_cheque.inc
 */

$api = '2.0.0';

$data = entity_import('rules_config', '{ "commerce_payment_commerce_cheque" : {
      "LABEL" : "Check",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Commerce Payment" ],
      "ON" : { "commerce_pos_order_paid" : [] },
      "DO" : [ { "commerce_payment_enable_commerce_cheque" : [] } ]
    }
  }');

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'entity',
  1 => 'rules',
);
