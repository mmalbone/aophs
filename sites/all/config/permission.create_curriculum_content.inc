<?php
/**
 * @file
 * permission.create_curriculum_content.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'create curriculum content',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array(
  'content_type.curriculum' => 'content_type.curriculum',
);

$optional = array();

$modules = array(
  0 => 'node',
);
