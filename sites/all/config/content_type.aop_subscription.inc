<?php
/**
 * @file
 * content_type.aop_subscription.inc
 */

$api = '2.0.0';

$data = (object) array(
  'base' => 'node_content',
  'description' => 'A content type used to store and process AOP Subscriptions',
  'has_title' => '1',
  'help' => '',
  'name' => 'AOP Subscription',
  'title_label' => 'Activation Code',
  'type' => 'aop_subscription',
);

$dependencies = array();

$optional = array(
  'field.node.field_activation_date.aop_subscription' => 'field.node.field_activation_date.aop_subscription',
  'field.node.field_active_flag.aop_subscription' => 'field.node.field_active_flag.aop_subscription',
  'field.node.field_assist_customer_id.aop_subscription' => 'field.node.field_assist_customer_id.aop_subscription',
  'field.node.field_customer_uid.aop_subscription' => 'field.node.field_customer_uid.aop_subscription',
  'field.node.field_date_last_billed.aop_subscription' => 'field.node.field_date_last_billed.aop_subscription',
  'field.node.field_date_product_ordered.aop_subscription' => 'field.node.field_date_product_ordered.aop_subscription',
  'field.node.field_drupal_com_orig_order_numb.aop_subscription' => 'field.node.field_drupal_com_orig_order_numb.aop_subscription',
  'field.node.field_drupal_commerce_order_numb.aop_subscription' => 'field.node.field_drupal_commerce_order_numb.aop_subscription',
  'field.node.field_grace_period.aop_subscription' => 'field.node.field_grace_period.aop_subscription',
  'field.node.field_last_assist_order_number.aop_subscription' => 'field.node.field_last_assist_order_number.aop_subscription',
  'field.node.field_last_update_from_lms.aop_subscription' => 'field.node.field_last_update_from_lms.aop_subscription',
  'field.node.field_monarch_product_id.aop_subscription' => 'field.node.field_monarch_product_id.aop_subscription',
  'field.node.field_monarch_product_title.aop_subscription' => 'field.node.field_monarch_product_title.aop_subscription',
  'field.node.field_monarch_product_url.aop_subscription' => 'field.node.field_monarch_product_url.aop_subscription',
  'field.node.field_orig_assist_order_number.aop_subscription' => 'field.node.field_orig_assist_order_number.aop_subscription',
  'field.node.field_renewal_flag.aop_subscription' => 'field.node.field_renewal_flag.aop_subscription',
  'field.node.field_sales_rep.aop_subscription' => 'field.node.field_sales_rep.aop_subscription',
  'field.node.field_subscription_expiration_da.aop_subscription' => 'field.node.field_subscription_expiration_da.aop_subscription',
  'field.node.field_subscription_id.aop_subscription' => 'field.node.field_subscription_id.aop_subscription',
  'field.node.field_subscription_renewal_perio.aop_subscription' => 'field.node.field_subscription_renewal_perio.aop_subscription',
  'field.node.title_field.aop_subscription' => 'field.node.title_field.aop_subscription',
  'permission.create_aop_subscription_content' => 'permission.create_aop_subscription_content',
  'permission.delete_any_aop_subscription_content' => 'permission.delete_any_aop_subscription_content',
  'permission.delete_own_aop_subscription_content' => 'permission.delete_own_aop_subscription_content',
  'permission.edit_any_aop_subscription_content' => 'permission.edit_any_aop_subscription_content',
  'permission.edit_own_aop_subscription_content' => 'permission.edit_own_aop_subscription_content',
);

$modules = array(
  0 => 'node',
);
