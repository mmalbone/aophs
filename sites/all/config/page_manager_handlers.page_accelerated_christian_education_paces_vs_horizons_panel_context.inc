<?php
/**
 * @file
 * page_manager_handlers.page_accelerated_christian_education_paces_vs_horizons_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_accelerated_christian_education_paces_vs_horizons_panel_context';
$handler->task = 'page';
$handler->subtask = 'accelerated_christian_education_paces_vs_horizons';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'top' => NULL,
    'left' => NULL,
    'middle' => NULL,
    'right' => NULL,
    'bottom' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'ac32098f-f288-48d9-b5e2-9854ae4eff9e';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-061b517b-aabf-4052-af1f-dfe5343fef2b';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Accelerated Christian Education PACEs vs. Horizons',
    'body' => '<p>Do you need to compare the print-based curriculum offered by A.C.E. to the Horizons program from Alpha Omega Publications? You\'re in the right spot. Find a short overview of each curriculum and service below, an easy-to-view comparison chart, and additional comparison options!</p><h3><strong>Accelerated Christian Education (School of Tomorrow) PACEs</strong></h3><p>Accelerated Christian Education, an in-house curriculum publisher, provides K-12th grade Christian curriculum and educational materials to Christian schools and homeschoolers. The A.C.E. PACEs option is a student-driven, workbook approach which provides an independent and individualized learning experience to students. Parents are responsible for all administrative tasks, including any required lesson planning, grading, record-keeping, and reporting. Accelerated Christian Education PACEs\' answer keys provide the answers and solutions to all student activities included in each PACE booklet.</p><p>Written from a Christian worldview, all A.C.E. PACEs are completely Bible-based. All science instruction is based on a biblical view of creation and the origin of life. Accelerated Christian Education courses include Bible, reading, social studies, mathematics, science, health, English, literature, fine arts, and foreign language. Coursework can be purchased in complete grade level kits or as subject kits. Kits provide all essential materials for students and teachers. Additional optional resources can be purchased separately.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>If you\'re considering a print-based approach to homeschooling, remember that there is no one perfect homeschool curriculum. A blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Rather than feeling torn between different curricula, parents should be open to mixing and matching materials and resources to customize their child\'s education. Remember, each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3><strong>Horizons from Alpha Omega Publications</strong></h3><p>Horizons is a preK-12 print-based curriculum offered by Alpha Omega Publications, a Christian-based publisher that provides curriculum and educational services to homeschool families. Filled with engaging, full-color illustrations, Horizons is a child-friendly workbook-based curriculum with consumable lessons and hands-on activities. Horizons curriculum encourages concept mastery through a spiral-based learning process of skill/concept presentation, review, and reinforcement. Teacher\'s guides offer clear, step-by-step lesson plans and activities that are suitable for both homeschool and classroom settings.</p><p>Winner of Cathy Duffy\'s 100 Top Picks for Homeschool Curriculum, Horizons integrates Scripture and Christian living throughout all subjects. Horizons workbooks provide removable student pages for ease of student use. Horizons curriculum includes courses in math (K-8), penmanship (1-5), health (K-8), physical education (PreK-12), phonics and reading (K-3), and spelling and vocabulary (1-3). Health and physical education electives were designed for classroom use and may need to be adapted to homeschool use. Complete Horizons Preschool Curriculum Sets and Multimedia Sets are also available. Horizons curriculum sets include all student and teacher materials. Student and teacher materials can also be purchased separately.</p><h3><strong>Benefits and Features of Horizons from Alpha Omega Publications</strong>&nbsp;</h3><p class="list_heading">Horizons</p><ul><li>brightly illustrated, hands-on, consumable curriculum</li><li>offers fun lessons and manipulative-based activities</li><li>teaches concepts through a spiral learning process</li><li>encourages concept mastery through quick concept introduction, review, and reinforcement</li></ul>

<h3>Take a Closer Look at Accelerated Christian Education PACEs and Horizons</h3>

<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">Horizons</th><th class="compare"><strong>A.C.E. PACES</strong></th></tr>
</thead>
<tbody>
<tr>
<td>K-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Complete curriculum</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Instruction provided by parents</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Teacher-intensive</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Readiness tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Easy to use, consumable workbooks</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Material designed for homeschoolers</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Manageable amount of daily lesson material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr><th class="features">Cost</th><th class="compare">Alpha Omega Publications</th><th class="compare">A.C.E.</th></tr>
</thead>
<tbody>
<tr>
<td>Average subject cost per grade level**</td>
<td class="tac">$79.95</td>
<td class="tac">$64.80</td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: A.C.E. versus Alpha Omega Publications</h4>
<p class="list_heading_links">A.C.E. PACE</p>
<ul>
<li><a href="/ace-pace-vs-monarch">A.C.E. PACE vs. Monarch</a></li>
<li><a href="/ace-pace-vs-horizons">A.C.E. PACE vs. Horizons</a></li>
<li><a href="/ace-pace-vs-lifepac">A.C.E. PACE vs. LIFEPAC</a></li>
<li><a href="/ace-pace-vs-aoa">A.C.E. PACE vs. Alpha Omega Academy</a></li>
<li><a href="/ace-pace-vs-sos">A.C.E. PACE vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">A.C.E. Lighthouse Christian Academy</p>
<ul>
<li><a href="/ace-lca-vs-monarch">A.C.E. Lighthouse Christian Academy vs. Monarch</a></li>
<li><a href="/ace-lca-vs-horizons">A.C.E. Lighthouse Christian Academy vs. Horizons</a></li>
<li><a href="/ace-lca-vs-lifepac">A.C.E. Lighthouse Christian Academy vs. LIFEPAC</a></li>
<li><a href="/ace-lca-vs-aoa">A.C.E. Lighthouse Christian Academy vs. Alpha Omega Academy</a></li>
<li><a href="/ace-lca-vs-sos">A.C.E. Lighthouse Christian Academy vs. Switched-On Schoolhouse</a></li>
</ul>
<p>*A.C.E.® and PACE® are the registered trademarks and subsidiaries of A.C.E.®. Alpha Omega Publications is not in any way affiliated with A.C.E.. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by A.C.E.®.</p>
<p class="note">Note: Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p>

<p>**Prices obtained February 2013 from Accelerated Christian Education Homeschool and A.C.E. Ministries websites and from Alpha Omega Publications 2013 Homeschool Catalog. Cost comparisons are based on the single subject price for the 3rd grade math Horizons curriculum and Lighthouse Christian Academy full year administrative costs for K-8 (does not include registration fees). Costs do not reflect any additional fees or shipping and handling charges. Prices for other Accelerated Christian Education and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>
',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '061b517b-aabf-4052-af1f-dfe5343fef2b';
  $display->content['new-061b517b-aabf-4052-af1f-dfe5343fef2b'] = $pane;
  $display->panels['left'][0] = 'new-061b517b-aabf-4052-af1f-dfe5343fef2b';
  $pane = new stdClass();
  $pane->pid = 'new-8db75b86-f95c-43cc-a8fa-8d29cfb76d5f';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '8db75b86-f95c-43cc-a8fa-8d29cfb76d5f';
  $display->content['new-8db75b86-f95c-43cc-a8fa-8d29cfb76d5f'] = $pane;
  $display->panels['right'][0] = 'new-8db75b86-f95c-43cc-a8fa-8d29cfb76d5f';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = '0';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
