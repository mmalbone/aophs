<?php
/**
 * @file
 * wysiwyg.commerce_order_message.inc
 */

$api = '2.0.0';

$data = FALSE;

$dependencies = array(
  'text_format.commerce_order_message' => 'text_format.commerce_order_message',
);

$optional = array();

$modules = array(
  0 => 'wysiwyg',
);
