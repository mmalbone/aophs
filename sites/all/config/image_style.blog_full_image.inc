<?php
/**
 * @file
 * image_style.blog_full_image.inc
 */

$api = '2.0.0';

$data = array(
  'effects' => array(),
  'label' => 'Blog full image',
  'name' => 'blog_full_image',
  'storage' => 1,
);

$dependencies = array();

$optional = array();

$modules = array();
