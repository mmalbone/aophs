<?php
/**
 * @file
 * menu.management.inc
 */

$api = '2.0.0';

$data = array(
  'description' => 'The <em>Management</em> menu contains links for administrative tasks.',
  'menu_name' => 'management',
  'title' => 'Management',
);

$dependencies = array();

$optional = array();

$modules = array();
