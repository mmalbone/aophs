<?php
/**
 * @file
 * page_manager_handlers.page_monarch_eula_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_monarch_eula_panel_context';
$handler->task = 'page';
$handler->subtask = 'monarch_eula';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '55b10f2a-a20f-4c65-bb55-b4c3bde778ce';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-74067c5d-5090-4e17-abaa-8c535c20020c';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => '',
    'body' => '<h1>Monarch Legal Terms and Conditions</h1>
<p>1. <a href="/terms-and-conditions">Legal Terms &amp; Conditions</a><br /> 2. <a href="/monarch-eula">Monarch EULA</a><br /> 3. <a href="/sos-eula">SOS EULA</a></p>
<p>PLEASE READ THESE TERMS OF USE CAREFULLY BEFORE USING THIS SITE. IF YOU DO NOT AGREE TO THESE TERMS, PLEASE DO NOT USE THE SITE.</p>
<p>By using or allowing others to use the software, materials, interactive features, and website associated with Monarch ("Monarch"), you (the "User") are agreeing to be bound by these legal terms and conditions ("Agreement"). Any person interacting with Monarch in any way, including but not limited to students, teachers, administrators, and parents, are Users for the purposes of this Agreement. If you do not agree to the terms and conditions of this Agreement, do not use Monarch. If you are dissatisfied with Monarch, any Monarch content, or the terms and conditions of this Agreement, you agree that your sole and exclusive remedy is to discontinue your use of Monarch. You acknowledge and accept that your use of Monarch is at your sole risk. You represent you have the legal capacity and authority to accept these Legal Terms and Conditions on behalf of yourself or any party you represent. Certain terms of this Agreement may not apply to your use of Monarch however all applicable terms are nonetheless binding. As the rightful owner of Monarch, Glynlyon, Inc. ("Glynlyon") reserves the right to change or terminate the terms of this Agreement at any time and from time to time without any notice to you by posting said changes on the Monarch website. Any such changes are hereby incorporated into this Agreement by reference as though fully set forth herein.</p>
<p><strong><a style="text-decoration: none; color: #000;" name="#General Provisions"></a>A. General Provisions</strong></p>
<p><strong>1. Indemnification.</strong> You, the User of Monarch, agree to indemnify and hold Glynlyon, its subsidiaries, affiliates, and assigns, and each of their directors, officers, agents, contractors, partners and employees, harmless from and against any loss, liability, claim, demand, damages, costs, and expenses, including reasonable attorneys" fees, arising out of this Agreement or in connection with any use of Monarch including but not limited to any damages, losses, or liabilities whatsoever with respect to damage to any property or loss of any data arising from the possession, use, or operation of Monarch by the User or any customers, users, students, or others, or arising from transmission of information or the lack thereof connected with Monarch as described in this Agreement.</p>
<p>&nbsp;</p>
<p><strong>2. Termination.</strong> This Agreement shall remain in effect until terminated. This Agreement may be terminated at Glynlyon"s sole discretion and without prior notice, by mutual written agreement between the parties, but not by the User. Glynlyon may suspend, terminate, or delete your access to Monarch without prior notice and in Glynlyon"s sole discretion and Glynlyon shall not be liable for any such suspension, termination, or deletion or its effects, including but not limited to interruption of business or education, loss of data or property, property damage, or any other hardship, losses, or damages. Glynlyon may unilaterally and without notice terminate this Agreement and/or your access to Monarch if you or any other person or entity using Monarch violates any provision of this Agreement. Glynlyon shall not be liable to you or to any third party for any termination. Upon termination you or any other person or party using Monarch shall cease to use Monarch at your sole cost and expense.</p>
<p>&nbsp;</p>
<p><strong>3. Updates.</strong> At its option, from time to time, Glynlyon may create updated versions of Monarch and may make such updates available to you either for a fee or for free. Unless explicitly stated otherwise, any such updates will be subject to the terms of this Agreement including any amendments to this Agreement, to be determined in Glynlyon"s sole discretion.</p>
<p>&nbsp;</p>
<p><strong>4. Proprietary Materials.</strong> All content available through Monarch, including designs, text, graphics, pictures, video, information, applications, software, music, sound, and other files, and their selection and arrangement ("Site Content"), as well as all software and materials contained in or related to Monarch are protected by copyrights, trademarks, service marks, patents, trade secrets, or other proprietary rights and laws. You hereby agree not to sell, license, rent, modify, distribute, copy, reproduce, transmit, publicly display, publicly perform, publish, adapt, edit, or create derivative works from such content or materials. Systematic retrieval of data or other content from Monarch to create or compile, directly or indirectly, a collection, compilation, recreation, database, or directory of Website materials is prohibited except as provided for herein. Use of Monarch content or materials for any purpose not expressly provided for herein is prohibited.</p>
<p>&nbsp;</p>
<p><strong>5. Disclaimer of Warranty.</strong> Monarch is provided "as is", with all faults and without warranty of any kind. GLYNLYON HEREBY DISCLAIMS ALL WARRANTIES WITH RESPECT TO MONARCH, EITHER EXPRESS, IMPLIED, OR STATUTORY, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, OF SATISFACTORY QUALITY, OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY, OF QUIET ENJOYMENT, AND NON-INFRINGEMENT OF THIRD PARTY RIGHTS. GLYNLYON DOES NOT WARRANT, GUARANTEE, OR MAKE ANY REPRESENTATIONS THAT THE CONTENT IS ACCURATE, RELIABLE, OR CORRECT OR THAT IT WILL MEET YOUR NEEDS OR REQUIREMENTS, THAT MONARCH WILL BE AVAILABLE AT ANY PARTICULAR TIME OR LOCATION, THAT ANY DEFECTS OR ERRORS WILL BE CORRECTED, OR THAT THE CONTENT IS FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. You assume the entire risk as to the quality, results, and performance of Monarch as well as the entire risk and cost of all service, repair, or correction. No oral or written information, advice, suggestions, or recommendations given by Glynlyon, its representatives, dealers, distributors, agents, or employees shall create a warranty or in any way increase the scope of this Agreement and you may not rely on any such information, advice, suggestions, or recommendations. Some jurisdictions do not allow the exclusion or limitation of certain warranties or consumer rights so some exclusions and limitations may not apply to you.</p>
<p>&nbsp;</p>
<p><strong>6. Limitation of Liability.</strong> You hereby agree that Glynlyon, its subsidiaries, affiliates, and assigns, and each of their directors, officers, agents, contractors, partners, and employees, shall not be liable to you or any third party for any indirect, special, consequential, or incidental damages including but not limited to damages for loss of funds or property, business interruption, loss of business opportunity, loss of data, or any other hardship, damages, or losses arising out of or related to: the use or inability to use Monarch, however caused; unauthorized or accidental access to or alteration of data; statements or conduct of any third party; or any matter relating to the use of Monarch; and even if Glynlyon has been advised of the possibility of such damages. Some jurisdictions do not allow the exclusion or limitation of certain remedies or damages so some exclusions and limitations may not apply to you.</p>
<p>&nbsp;</p>
<p><strong>7. Severability.</strong> If any provision of this Agreement is held to be ineffective, unenforceable, or illegal for any reason, Glynlyon may reform such provision to the extent necessary to make it effective, enforceable, and legal or such provision may be deemed severed and in either case this Agreement with such provision reformed or severed shall remain in full force and effect to the fullest extent permitted by law.</p>
<p>&nbsp;</p>
<p><strong>8. Controlling Law and Controversies.</strong> This Agreement shall be governed by the laws of the State of Arizona and of the United States. You understand and agree that use of Monarch may involve interstate data transmissions which may be considered a transaction in interstate commerce under federal law. If any controversy or claim arising out of or relating to this Agreement cannot be solved by negotiation between the parties, the parties hereby agree to attempt in good faith to settle the dispute through mediation administered by a mutually agreed upon mediator in Phoenix, Arizona and in accordance with the Rules of Procedure for Christian Conciliation of the Institute for Christian Conciliation, a division of Peacemaker" Ministries. If mediation fails to resolve the dispute, the parties hereby agree that the dispute shall be settled through arbitration administered by a mutually agreed upon arbitrator in Phoenix, Arizona and in accordance with the Rules of Procedure for Christian Conciliation of the Institute for Christian Conciliation, a division of Peacemaker" Ministries. Judgment upon an arbitration decision may be entered in any court otherwise having jurisdiction.</p>
<p>&nbsp;</p>
<p><strong>9. Entire Agreement.</strong> This Agreement constitutes the entire agreement between Glynlyon and the User relating to the subject matter hereof and supersedes all prior understandings, promises, and undertakings, if any, made orally or in writing with respect to the subject matter hereof. No modification, amendment, waiver, termination, or discharge of any portion of this Agreement shall be binding unless executed and confirmed in writing by Glynlyon.</p>
<p>&nbsp;</p>
<p><strong><a style="text-decoration: none; color: #000;" name="#End User Terms and Conditions"></a>B. End User Terms and Conditions</strong></p>
<p><strong>1. Ownership.</strong> Monarch and all associated materials provided by Glynlyon are the solely owned or appropriately licensed property of Glynlyon. Monarch is licensed, not sold, to you under the terms of this Agreement. Glynlyon does not sell any title, ownership right, or interest in or to Monarch. By using Monarch, you are agreeing only to a non-exclusive, nontransferable license to use, according to the terms of this Agreement, Monarch and any software programs or other proprietary material of third parties that are incorporated into Monarch. Glynlyon reserves and retains all applicable right, title, and interest (including but not limited to copyrights, patents, trademarks, and service marks and other intellectual property rights) in and to Monarch and all associated materials. Any remuneration paid for this product constitutes a license fee for the use of Monarch.</p>
<p>&nbsp;</p>
<p><strong>2. Use.</strong></p>
<p>a) The copying, reproduction, duplication, translation, reverse engineering, adaptation, decompilation, disassembly, reverse assembly, modification, or alteration of Monarch or any portion thereof is expressly prohibited without the prior written consent of Glynlyon except as provided for herein. The merger or inclusion of Monarch or any portion thereof with any computer program, and the creation of derivative works or programs from Monarch or any portion thereof, is also expressly prohibited without the prior written consent of Glynlyon.</p>
<p>b) Requests for permission to reproduce, duplicate, adapt, or otherwise exploit any portion of Monarch should be submitted in writing to the Glynlyon address listed at the bottom of this Agreement. Any permissions granted shall be in the sole and exclusive discretion of Glynlyon.</p>
<p>c) Neither Monarch nor any part thereof may be rented, leased, sold, assigned, transferred, re-licensed, sub-licensed, or conveyed for any purpose. Any attempted rental, lease, sale, assignment, transfer, re-license, sub-license, conveyance, gift, or other disposition of Monarch in violation of this Agreement is null and void. Any act or failure to prevent an act in violation of this Agreement may result in civil and/or criminal prosecution.</p>
<p>d) Programs or software developed and/or owned by entities other than Glynlyon and included with or incorporated into Monarch ("Third Party Software") is subject to and its use is governed by this Agreement. The use of Third Party Software except as for any purpose other than its intended use in conjunction with Monarch is prohibited.</p>
<p>&nbsp;</p>
<p><strong>3. Registration and Identifying Information.</strong> You hereby represent and warrant that any and all information provided by you to Glynlyon shall be complete, true, accurate, and current in all respects and that you shall update any changes to information as soon as such changes occur. As related to your use of Monarch, you are responsible for maintaining the confidentiality of your account and password and for restricting access to your computer. You agree to accept responsibility for all activities that occur under your account and password. When providing any identifying information about students or minors, you hereby represent and warrant that you are authorized to provide such information and that you have read and agreed to the terms of the Privacy Policy included in this Agreement and associated with Monarch.</p>
<p>&nbsp;</p>
<p><strong>4. Hosting Policy.</strong></p>
<p>a) Glynlyon may but is not obligated to provide database services to manage student records solely related to the use and application of Monarch ("Hosting Services").</p>
<p>b) Hosting Services provided by Glynlyon in connection with the purchase and/or use of Monarch are included in any price paid for Monarch and Glynlyon is not responsible nor will Glynlyon provide or offer any discounts or credits if you do not have adequate facilities or equipment to utilize the Hosting Services.</p>
<p>c) You agree to exercise the utmost vigilance and care in protecting all information to be transmitted via Glynlyon"s Hosting Services. Glynlyon is not responsible for any lost, stolen, or otherwise mismanaged data transmitted pursuant to this Agreement.</p>
<p>d) Any and all information transmitted pursuant to this Agreement shall be subject to and covered by the indemnifications, liability limitations, and Privacy Policy included herein.</p>
<p>e) Glynlyon reserves the right to modify or discontinue, temporarily or permanently, at any time and from time to time, the Hosting Services (or any part thereof) with or without notice. Glynlyon shall not be liable to you, the User, or to any third party for any modification, suspension, or discontinuance of the Hosting Services, for your or any third party"s use of the Hosting Services, or for any damages originating therefrom. In no event shall you be entitled to receive a rebate, refund, credit or reduction of any costs or fees which you agreed to pay for Monarch.</p>
<p>&nbsp;</p>
<p><strong>5. Third Party Sites and Content.</strong> The Website may contain (or may send you through or to) links to other websites ("Third Party Sites") as well as articles, photographs, text, graphics, pictures, designs, music, sound, video, information, applications, software, and other content or items belonging to or originating from third parties ("Third Party Content"). Glynlyon does not check such Third Party Sites and Third Party Content for accuracy, appropriateness, or completeness and Glynlyon is not responsible for any Third Party Sites accessed through use of Monarch or for any Third Party Content posted on, available through, or installed from Monarch, including the content, accuracy, offensiveness, opinions, reliability, privacy practices, or other policies of or contained in the Third Party Sites or the Third Party Content. Inclusion of, linking to, or permitting the use or installation of any Third Party Site or any Third Party Content does not imply approval or endorsement thereof by Glynlyon. Although some computers may employ filtering software to prevent access to certain Third Party Sites, Glynlyon shall have no responsibility or liability whatsoever for any Third Party Sites or Third Party Content accessed through use of Monarch.<br /> </p>
<p><strong>6. User Conduct.</strong> You represent, warrant, and agree that no materials of any kind submitted through your account or otherwise created, used, posted, transmitted, or shared by you or others through you on or through Monarch will violate or infringe upon the rights of any third party, including copyright, trademark, privacy, publicity, or other personal or proprietary rights; or contain libelous, defamatory, or otherwise unlawful material. You further agree not to use Monarch to:</p>
<p>a) collect email addresses or other contact information of other users from Monarch;</p>
<p>b) send unsolicited communications to other users of Monarch;</p>
<p>c) take any unlawful or unauthorized actions or in any way damage, disable, overburden, or impair Monarch or the intellectual property rights owned or licensed by Glynlyon as described elsewhere herein;</p>
<p>d) upload, post, transmit, share, store, or otherwise make available any content that Glynlyon deems harmful, threatening, unlawful, defamatory, infringing, abusive, inflammatory, harassing, vulgar, obscene, fraudulent, invasive of privacy or publicity rights, hateful, or racially, ethnically, or otherwise objectionable;<strong></strong></p>
<p>e) misrepresent yourself, your age, or your affiliation with any person or entity</p>
<p>f) upload, post, transmit, share, or otherwise make available any unsolicited or unauthorized advertising, solicitations, promotional materials, "junk mail," "spam," "chain letters," "pyramid schemes," or any other form of solicitation</p>
<p>g) upload, post, transmit, share, store, or otherwise make publicly available through Monarch any private information of any third party;</p>
<p>h) solicit personal information from anyone under 18 or solicit passwords or personally identifying information for commercial, unauthorized, or unlawful purposes;</p>
<p>i) upload, post, transmit, share, or otherwise make available any material that contains software viruses or any other computer code, files, or programs designed to interrupt, destroy, or limit the functionality of any computer software, hardware, or telecommunications equipment</p>
<p>j) intimidate or harass another</p>
<p>k) upload, post, transmit, share, store, or otherwise make available content that would constitute, encourage, or provide instructions for a criminal offense, violate the rights of any party, or otherwise create liability or violate any local, state, national, or international law</p>
<p>l) use or attempt to use another\'s account, service, or system or create a false identity on Monarch;</p>
<p>m) interfere with or disrupt Monarch or servers or networks connected to Monarch, or disobey any requirements, procedures, policies, or regulations of networks connected to Monarch;</p>
<p>n) upload, post, transmit, share, store, or otherwise make available content that infringes any proprietary rights of any party, defames, slanders, or libels any party, or otherwise violates any law of the United States or the jurisdiction in which you reside</p>
<p>o) upload, post, transmit, share, store, or otherwise make available content that, in the sole judgment of Glynlyon, is objectionable or which restricts or inhibits any other person from using or enjoying Monarch, or which may expose Glynlyon or its users to any harm or liability of any type.</p>
<p>&nbsp;</p>
<p><strong>7. User Content. </strong>You are solely responsible for the profiles (including any name, image, or likeness), messages, notes, text, information, listings, and other content that you upload, publish, or display on or through Monarch ("User Content"). Posting, transmitting, or sharing User Content through Monarch that you did not create, that you do not have the rights to, or that you do not have permission to post is prohibited. You understand and agree that Glynlyon may, but is not obligated to, review and may delete or remove (without notice) any User Content in its sole discretion, for any reason or no reason, including User Content that in Glynlyon"s sole judgment violates this Agreement or which might be offensive, illegal, or that might violate the rights, harm, or threaten the safety of users or others. You are solely responsible at your sole cost and expense for creating backup copies and replacing any User Content you post or store on Monarch or provide to Glynlyon. When you post User Content, you authorize and direct Glynlyon to make such copies thereof as Glynlyon deems necessary in order to facilitate the posting, storage, and use of the User Content. By posting User Content through any part of Monarch, you automatically grant, and you represent and warrant that you have the right to grant, to Glynlyon an irrevocable, perpetual, non-exclusive, transferable, fully paid, worldwide license (with the right to sublicense) to use, copy, publicly perform, publicly display, reformat, translate, excerpt (in whole or in part), and distribute such User Content for any purpose, commercial, advertising, or otherwise, on or in connection with Monarch or the promotion thereof, to prepare derivative works of, or incorporate into other works, such User Content, and to grant and authorize sublicenses of the foregoing.</p>
<p>&nbsp;</p>
<p><strong><a style="text-decoration: none; color: #000;" name="#Privacy Policy"></a>C. Privacy Policy</strong></p>
<p>Glynlyon institutes the terms of this privacy policy ("Privacy Policy") to ensure that Users" personally identifiable information ("PII") is maintained in a safe, secure, and responsible manner. The following describes how Glynlyon collects, uses, and shares the PII obtained from and about individuals who use Monarch. This policy does not apply to information that Glynlyon may collect outside the use of Monarch, such as over the phone, by fax, or through conventional mail. All children"s educational records are protected as required by the Family Educational Rights and Privacy Act (FERPA), the Children"s Online Privacy Protection Act (COPPA), and other applicable federal and state laws.</p>
<p><strong>1. Information Collected.</strong></p>
<p>a) Glynlyon may collect PII for purposes of administering educational programs, to improve the content of Monarch and Glynlyon services, or for any other commercial purpose. The only PII collected from you has been voluntarily submitted and provided to Glynlyon through your use of Monarch, in response to surveys, or in response to other requests for information.</p>
<p>b) The types of information that Glynlyon may collect include but are not limited to: name, address, email address, services requested, student registration and enrollment information, and background information.</p>
<p>c) Glynlyon maintains all student data, student educational information, and student files as confidential in order to protect the privacy of students and schools. Strict security procedures ensure that student and school files and information are not disclosed to unauthorized parties.</p>
<p>&nbsp;</p>
<p><strong>2. Glynlyon Rights.</strong></p>
<p>a) Glynlyon reserves the right to use "cookies" to personalize the online experience.</p>
<p>b) Glynlyon reserves the right to collect and log non-personal information related to your use of Monarch.</p>
<p>c) Glynlyon reserves the right to share PII with affiliated companies and third parties. You can request that your PII not be shared with independent third parties by making a request in writing to the address listed below.</p>
<p>d) Glynlyon reserves the right to contact you for any reason related to Monarch in Glynlyon"s sole discretion and without any opt-out option.</p>
<p>e) Glynlyon reserves the right to disclose PII if required to do so by law or under the belief that such action is necessary to (a) comply with the law or with a legal process, (b) protect against misuse or unauthorized use of Monarch, (c) enforce the terms of this Agreement, or (d) protect the personal safety or property of other users, the public, or Glynlyon employees.</p>
<p>&nbsp;</p>
<p><strong>3. Children. </strong>Glynlyon takes all steps necessary to protect children and to comply with the Children"s Online Privacy Protection Act (COPPA).</p>
<p>a) The only student PII collected by Glynlyon is name, address, username, and password, and is supplied by you through student enrollment. Student PII is limited to what is required to identify the user and is only used to identify the user. Glynlyon may not require disclosure of more student PII than is reasonably necessary to utilize Monarch.</p>
<p>b) Student PII is not disclosed to third parties. If Glynlyon were to disclose any student PII to third parties, the User or the student"s parent or guardian would have the option to agree to the collection and use of the student PII without consenting to the disclosure of the PII to third parties.</p>
<p>c) Parents or guardians have the right to review their student"s PII collected by Glynlyon, request that their child"s PII be revised or deleted, or refuse to allow further PII to be collected by making a request in writing to the address listed below.</p>
<p>&nbsp;</p>
<p><strong>4. Third Party Websites</strong> This Privacy Policy applies only to Monarch and not to the websites of partners, affiliates, or Third Party Sites. You may be required to accept additional policies prior to your use of links accessed through Monarch. Glynlyon is not responsible for the privacy policies of partners, affiliates, or Third Party Sites.</p>
<p><strong>5. Third Party Servers.</strong> Certain information stored or transmitted by Glynlyon hereunder, including PII, may be stored, managed, or otherwise handled by third parties. You understand and agree that Glynlyon is not liable, accountable, or otherwise responsible for information communicated to or through third parties and you hereby release Glynlyon from any associated liability or responsibility consistent with the indemnifications, liability limitations, and Privacy Policy included herein.</p>
<p>&nbsp;</p>
<p><strong>6. User Generated Content.</strong> You participate in any messaging, chats, blogs, or other interactive features of Monarch at your own risk. User generated content posted to Monarch is the sole and exclusive responsibility of the User and Glynlyon has no responsibility or liability for or association or involvement with such content. Glynlyon may but is not obligated to monitor messages, chats, blogs or other interactive features of Monarch but Glynlyon cannot and will not guarantee the safety or security of any User generated content nor does Glynlyon offer any assurances that User generated content will appear on Monarch accurately or in its entirety.<strong></strong></p>
<p>&nbsp;</p>
<p><strong>7. Access and Corrections to PII and Opting out of Further Communications.</strong> Use the contact information below to request<strong></strong></p>
<p>a) Corrections or updates of any PII in Glynlyon"s database you believe to be erroneous</p>
<p>b) An opt-out of future communications from Glynlyon</p>
<p>c) Glynlyon to make reasonable efforts to remove PII from its online database. You acknowledge and accept that it may be impossible to delete personal information entirely because of backups and records of deletions.</p>
<p>d) You may access or correct your PII or opt out of further communications by writing to</p>
<p>Glynlyon, Inc.<br /> Attn: Legal Department<br /> 300 North McKemy Ave. <br /> Chandler, AZ 85226</p>
<p><strong>8. Changes in Corporate Structure. </strong>If all or part of Glynlyon or any of its affiliates are sold, merged, or otherwise transferred to another entity, the PII provided through use of Monarch may be transferred as part of that transaction or process. The Family Educational Rights and Privacy Act (FERPA) protects children"s educational records in the event of such a transaction or transfer.</p>
<p><strong>9. Contact. </strong>If you have any questions regarding this Privacy Policy, please contact:</p>
<p>Glynlyon, Inc.<br /> Attn: Legal Department<br /> 300 North McKemy Ave. <br /> Chandler, AZ 85226</p>
<p>Monarch and its logo are registered trademarks of Glynlyon, Inc. All trademarks that appear in this product are the property of their respective owners. All rights reserved.</p>
<p>&copy; 2010 Glynlyon, Inc. All rights reserved.</p>
<p>&nbsp;</p>
<p><a href="#">Top of Page</a></p></div>                </div>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '74067c5d-5090-4e17-abaa-8c535c20020c';
  $display->content['new-74067c5d-5090-4e17-abaa-8c535c20020c'] = $pane;
  $display->panels['middle'][0] = 'new-74067c5d-5090-4e17-abaa-8c535c20020c';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-74067c5d-5090-4e17-abaa-8c535c20020c';
$handler->conf['display'] = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
