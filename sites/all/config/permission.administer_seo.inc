<?php
/**
 * @file
 * permission.administer_seo.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'administer seo',
  'roles' => array(
    0 => 'administrator',
    1 => 'marketing',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'seo',
);
