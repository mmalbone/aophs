<?php
/**
 * @file
 * page_manager_handlers.page_rod_and_staff_vs_horizons_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_rod_and_staff_vs_horizons_panel_context';
$handler->task = 'page';
$handler->subtask = 'rod_and_staff_vs_horizons';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '69d3b96e-41b5-4bae-ab05-cbe2ea05b550';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-e66976ca-5d16-4631-83d0-133e5191c93d';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Rod and Staff vs. Horizons',
    'body' => '<p>Do you need to compare Rod and Staff\'s Bible-based curriculum to the Horizons program from Alpha Omega Publications? You\'re in the right spot. Find a short overview of each curriculum and service below, an easy-to-view comparison chart, and additional comparison options!</p><h3><strong>Rod and Staff Curriculum</strong></h3><p>Rod and Staff Publishers, Inc. is a Mennonite publishing company which supplies Christian homes and schools with Bible-based textbooks and literature useful for academic instruction and guidance in Christian discipline. Rod and Staff publishes curriculum designed for a traditional method of instruction that encourages strong thinking and communication skills. Textbooks, workbooks, worksheets, tests, and teacher materials are available for preschool through 10th grade. Coursework includes Bible, reading, English, arithmetic, spelling, science, health, history/geography, penmanship, music, art, typing, and special education.</p><p>Designed to instill biblical values and standards, all Rod and Staff curriculum is steeped in biblical truth. Rod and Staff does not sell kindergarten curriculum. As an alternative to kindergarten, Rod and Staff provides a series of preschool resources designed to prepare young students for school both in attitude and action. Parents who choose Rod and Staff are responsible for all administrative tasks, including all required lesson planning, grading, record-keeping, and reporting. Because the Rod and Staff teacher\'s manuals are designed for the beginning teacher (or parent), they are easily used by anyone who desires to homeschool their children. Telephone consultations are available for Rod and Staff customers. Curriculum can be purchased in grade level kits or as individual components.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>If you\'re considering a Bible-based approach to homeschooling, remember that there is no one perfect homeschool curriculum. A blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Instead of feeling torn between different curricula, parents should be open to mixing and matching materials and resources to customize their child\'s education. Remember, each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3><strong>Horizons from Alpha Omega Publications</strong></h3><p>Horizons is a preK-12 print-based curriculum offered by Alpha Omega Publications, a Christian-based publisher that provides curriculum and educational services to homeschool families. Filled with engaging, full-color illustrations, Horizons is a child-friendly workbook-based curriculum with consumable lessons and hands-on activities. Horizons curriculum encourages concept mastery through a spiral-based learning process of skill/concept presentation, review, and reinforcement. Teacher\'s guides offer clear, step-by-step lesson plans and activities that are suitable for both homeschool and classroom settings.</p><p>Winner of Cathy Duffy\'s 100 Top Picks for Homeschool Curriculum, Horizons integrates Scripture and Christian living throughout all subjects. Horizons workbooks provide removable student pages for ease of student use. Horizons curriculum includes courses in math (K-8), penmanship (1-5), health (K-8), physical education (PreK-12), phonics and reading (K-3), and spelling and vocabulary (1-3). Health and physical education electives were designed for classroom use and may need to be adapted to homeschool use. Complete Horizons Preschool Curriculum Sets and Multimedia Sets are also available. Horizons curriculum sets include all student and teacher materials. Student and teacher materials can also be purchased separately.</p><h3><strong>Benefits and Features of Horizons from Alpha Omega Publications</strong></h3><p class="list_heading">Horizons</p><ul><li>brightly illustrated, hands-on, consumable curriculum</li><li>offers fun lessons and manipulative-based activities</li><li>teaches concepts through a spiral learning process</li><li>encourages concept mastery through quick concept introduction, review, and reinforcement</li></ul><h3 class="para-start"><strong>Take a Closer Look at Rod and Staff Curriculum and Horizons</strong></h3>

<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">Horizons</th><th class="compare">Rod and Staff</th></tr>
</thead>
<tbody>
<tr>
<td>K-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">Preschool-10th grade</td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Complete curriculum</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Instruction provided by parents</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Easy to use parent materials</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Readiness tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Easy to use consumable workbooks</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Material designed for homeschoolers</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Manageable amount of daily lesson material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Traditional approach to instruction</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr><th class="features">Cost</th><th class="compare">Alpha Omega Publications</th><th class="compare">Rod and Staff</th></tr>
</thead>
<tbody>
<tr>
<td>Average subject cost per grade level**</td>
<td class="tac">$79.95</td>
<td class="tac">$45.60</td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: Rod and Staff Curriculum versus Alpha Omega Publications</h4>
<ul class="comp_list">
<li><a href="/rod-and-staff-vs-monarch">Rod and Staff Curriculum vs. Monarch</a></li>
<li><a href="/rod-and-staff-vs-aoa">Rod and Staff Curriculum vs. Alpha Omega Academy</a></li>
<li><a href="/rod-and-staff-vs-horizons">Rod and Staff Curriculum vs. Horizons</a></li>
<li><a href="/rod-and-staff-vs-lifepac">Rod and Staff Curriculum vs. LIFEPAC</a></li>
<li><a href="/rod-and-staff-vs-sos">Rod and Staff Curriculum vs. Switched-On Schoolhouse</a></li>
</ul>
<p>*Rod and Staff Publishers, Inc. is a Christian Publisher of textbooks and literature. Alpha Omega Publications is not in any way affiliated with Rod and Staff. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by Rod and Staff.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in March 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p><p>**Prices obtained March 2013 from the Milestone Ministries website, independent vendor of Rod &amp; Staff materials, and from Alpha Omega Publications 2013 Homeschool Catalog. Individual subject prices are based on a comparison between the Horizons 6th Grade Math boxed set and Rod &amp; Staff 6th Grade Math Set. Costs do not reflect any additional fees or shipping and handling charges. Prices for other Rod &amp; Staff and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'e66976ca-5d16-4631-83d0-133e5191c93d';
  $display->content['new-e66976ca-5d16-4631-83d0-133e5191c93d'] = $pane;
  $display->panels['left'][0] = 'new-e66976ca-5d16-4631-83d0-133e5191c93d';
  $pane = new stdClass();
  $pane->pid = 'new-18dba7d9-0a44-4178-9e74-0947ecf74dec';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '18dba7d9-0a44-4178-9e74-0947ecf74dec';
  $display->content['new-18dba7d9-0a44-4178-9e74-0947ecf74dec'] = $pane;
  $display->panels['right'][0] = 'new-18dba7d9-0a44-4178-9e74-0947ecf74dec';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-e66976ca-5d16-4631-83d0-133e5191c93d';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
