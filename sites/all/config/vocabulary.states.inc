<?php
/**
 * @file
 * vocabulary.states.inc
 */

$api = '2.0.0';

$data = (object) array(
  'vid' => '20',
  'name' => 'States',
  'machine_name' => 'states',
  'description' => '',
  'hierarchy' => '0',
  'module' => 'taxonomy',
  'weight' => '0',
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'taxonomy',
);
