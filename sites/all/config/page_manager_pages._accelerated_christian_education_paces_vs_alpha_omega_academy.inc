<?php
/**
 * @file
 * page_manager_pages._accelerated_christian_education_paces_vs_alpha_omega_academy.inc
 */

$api = '2.0.0';

$data = $page = new stdClass();
$page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
$page->api_version = 1;
$page->name = '_accelerated_christian_education_paces_vs_alpha_omega_academy';
$page->task = 'page';
$page->admin_title = ' Accelerated Christian Education PACEs vs. Alpha Omega Academy';
$page->admin_description = '';
$page->path = 'ace-pace-vs-aoa';
$page->access = array();
$page->menu = array();
$page->arguments = array();
$page->conf = array(
  'admin_paths' => FALSE,
);


$dependencies = array(
  'page_manager_handlers.page__accelerated_christian_education_paces_vs_alpha_omega_academy_panel_context' => 'page_manager_handlers.page__accelerated_christian_education_paces_vs_alpha_omega_academy_panel_context',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
