<?php
/**
 * @file
 * page_manager_handlers.page__time4learning_vs_monarch_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page__time4learning_vs_monarch_panel_context';
$handler->task = 'page';
$handler->subtask = '_time4learning_vs_monarch';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '9cb89a01-bb96-47e9-92f1-b251bf2db714';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-51716a8d-f149-415b-89af-0016d32e59cc';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Time4Learning vs. Monarch',
    'body' => '<p>Are you ready to compare the online curriculum option offered by Time4Learning to the Internet-based Monarch program from Alpha Omega Publications? You\'re in the right spot. Find a short overview of each curriculum and service below, an easy-to-view comparison chart, and additional comparison options!</p><h3><strong>Time4Learning Online Curriculum</strong></h3><p>Time4Learning, an online educational program for preschool through 8th grade students, provides academic instruction through multimedia, interactive project-based activities, and worksheets. Time4Learning has many appropriate applications including its use as a core curriculum for homeschooled students. Courses include language arts, math, science, and social studies. This student-paced, flexible approach to academic instruction presents engaging interactive lessons in a suggested sequence that correlates to state standards. The math and language arts courses provide comprehensive programs that need no supplementation. The Time4Learning science (1st-6th grade) and social studies (2nd-7th grade) courses may require supplementation in order to satisfy local requirements.</p><p>Because Time4Learning is a secular homeschooling program, parents from various faiths and cultural backgrounds may find Time4Learning appropriate for their families\' homeschool needs. For parents, Time4Learning provides detailed reporting, simple record-keeping, lesson plans, teaching tools, and more. Because Time4Learning is web-based, families have complete flexibility regarding their use of the curriculum. A computer with a high speed internet connection (DSL or cable modem) is all that is needed to provide a quality homeschool experience!</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>As you consider your homeschool curriculum options, remember there is no one perfect homeschool curriculum. A blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Instead of feeling torn between different curricula, parents should be open to mixing and matching materials and resources to customize their child\'s education. Remember, each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3><strong>Monarch from Alpha Omega Publications</strong></h3><p>Monarch from Alpha Omega Publications is an interactive, Internet-based curriculum for grades 3-12. Alpha Omega Publications, a trusted publisher of high quality educational products, provides Christian curriculum and educational services to homeschool families. Transforming the way students learn, Monarch is an in-depth, online curriculum that makes use of today\'s 21st century technology. This innovative, cutting-edge AOP curriculum presents Christ-centered lessons using eye-catching animations, learning games, video and audio clips, and more! Unlike traditional textbooks, Monarch has a diverse mix of text-based instruction and engaging multimedia enrichment. Time-saving teaching tools provide automatic grading, lesson planning, customizable lessons, a built-in calendar, printing options, message center, text-to-speech, and more!</p><p>Compatible with both Windows® and Macintosh® operating systems, Monarch integrates Scripture and a biblical worldview into all subjects. Monarch offers the flexibility to log on and learn anytime, anywhere with 18-month, single subscriptions using an individualized, student driven approach. Diagnostic tests are available for placement in math and language arts. Core coursework includes Bible (3-12), language arts (3-12), math (3-12), science (3-12), and history and geography (3-12). Over 35 electives are also available. Monarch may be purchased as individual subjects or as complete five-subject sets.</p><h3><strong>Benefits and Features of Monarch from Alpha Omega Publications</strong></h3><p class="list_heading">Monarch</p><ul><li>online interactive lessons with anytime, anywhere access</li><li>compatible with both Windows® and Mac® operating systems</li><li>offers customizable student learning options and time-saving teaching tools</li><li>features dynamic, media-rich lessons; real-time content updates; and data storage</li></ul><h3 class="para-start"><strong>Take a Closer Look at Time4Learning Online Curriculum and Monarch</strong></h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">Monarch</th><th class="compare">Time4Learning</th></tr>
</thead>
<tbody>
<tr>
<td>K-12 curriculum options</td>
<td class="tac">3-12th grade</td>
<td class="tac">Preschool-8th grade</td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">No electives</td>
</tr>
<tr class="alt">
<td>Teacher-intensive curriculum</td>
<td class="tac">&nbsp;</td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Internet-based curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Interactive, engaging multimedia tools</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Customizable lessons and calendar</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Placement tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Automatic grading and lesson planning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Automatic updated material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Flexible and individualized</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Mastery-based learning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr><th class="features">Cost</th><th class="compare">Monarch</th><th class="compare">Time4Learning</th></tr>
</thead>
<tbody>
<tr>
<td>Average yearly cost per grade level**</td>
<td class="tac">$449.95</td>
<td class="tac" rowspan="2">$199.50<br>(same price for full access or single subject)</td>
</tr>
<tr class="alt">
<td>Average subject cost per grade level**</td>
<td class="tac">$99.95</td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: Time4Learning versus Alpha Omega Publications</h4>
<ul class="comp_list">
<li><a href="/time4learning-vs-monarch">Time4Learning vs. Monarch</a></li>
<li><a href="/time4learning-vs-aoa">Time4Learning vs. Alpha Omega Academy</a></li>
<li><a href="/time4learning-vs-horizons">Time4Learning vs. Horizons</a></li>
<li><a href="/time4learning-vs-lifepac">Time4Learning vs. LIFEPAC</a></li>
<li><a href="/time4learning-vs-sos">Time4Learning vs. Switched-On Schoolhouse</a></li>
</ul>
<p>*Time4Learning® is the exclusive trademark of Time4Learning. Alpha Omega Publications is not in any way affiliated with Time4Learning. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by Time4Learning.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p><p>**Prices obtained February 2013 from the Time4Learning website and from Alpha Omega Publications 2013 Homeschool Catalog. Yearly prices based on a comparison between the Monarch 6th Grade Complete grade level set and the regular fees for access to Time4Learning curriculum for a single student for 10 months. Individual subject prices are based on comparison between the Monarch 6th grade single subject cost and the regular fees for access to Time4Learning curriculum for a single student for 10 months. Costs do not reflect any additional fees or shipping and handling charges. Prices for other Time4Learning and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '51716a8d-f149-415b-89af-0016d32e59cc';
  $display->content['new-51716a8d-f149-415b-89af-0016d32e59cc'] = $pane;
  $display->panels['left'][0] = 'new-51716a8d-f149-415b-89af-0016d32e59cc';
  $pane = new stdClass();
  $pane->pid = 'new-5dba3e60-0308-4201-926b-136efe09b26a';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '5dba3e60-0308-4201-926b-136efe09b26a';
  $display->content['new-5dba3e60-0308-4201-926b-136efe09b26a'] = $pane;
  $display->panels['right'][0] = 'new-5dba3e60-0308-4201-926b-136efe09b26a';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-51716a8d-f149-415b-89af-0016d32e59cc';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
