<?php
/**
 * @file
 * views_view.curriculum_product_listing_header.inc
 */

$api = '2.0.0';

$data = $view = new view();
$view->name = 'curriculum_product_listing_header';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'taxonomy_term_data';
$view->human_name = 'Curriculum Product Listing Header';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* Footer: Global: View area */
$handler->display->display_options['footer']['view']['id'] = 'view';
$handler->display->display_options['footer']['view']['table'] = 'views';
$handler->display->display_options['footer']['view']['field'] = 'view';
$handler->display->display_options['footer']['view']['label'] = 'Curated Products';
$handler->display->display_options['footer']['view']['view_to_insert'] = 'curated_product_list:default';
/* Field: Taxonomy term: Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['label'] = '';
$handler->display->display_options['fields']['name']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['name']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['name']['element_wrapper_type'] = 'h1';
$handler->display->display_options['fields']['name']['element_wrapper_class'] = 'product_banner_title';
$handler->display->display_options['fields']['name']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['name']['link_to_taxonomy'] = TRUE;
/* Field: Taxonomy term: Banner Image */
$handler->display->display_options['fields']['field_banner_image']['id'] = 'field_banner_image';
$handler->display->display_options['fields']['field_banner_image']['table'] = 'field_data_field_banner_image';
$handler->display->display_options['fields']['field_banner_image']['field'] = 'field_banner_image';
$handler->display->display_options['fields']['field_banner_image']['label'] = '';
$handler->display->display_options['fields']['field_banner_image']['element_type'] = '0';
$handler->display->display_options['fields']['field_banner_image']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_banner_image']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_banner_image']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_banner_image']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_banner_image']['settings'] = array(
  'image_style' => '',
  'image_link' => '',
);
$handler->display->display_options['fields']['field_banner_image']['field_api_classes'] = TRUE;
/* Field: Taxonomy term: Short Description */
$handler->display->display_options['fields']['field_short_description']['id'] = 'field_short_description';
$handler->display->display_options['fields']['field_short_description']['table'] = 'field_data_field_short_description';
$handler->display->display_options['fields']['field_short_description']['field'] = 'field_short_description';
$handler->display->display_options['fields']['field_short_description']['label'] = '';
$handler->display->display_options['fields']['field_short_description']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_short_description']['element_wrapper_type'] = 'div';
$handler->display->display_options['fields']['field_short_description']['element_wrapper_class'] = 'product_banner_short_description';
$handler->display->display_options['fields']['field_short_description']['element_default_classes'] = FALSE;
/* Contextual filter: Taxonomy term: Term ID */
$handler->display->display_options['arguments']['tid']['id'] = 'tid';
$handler->display->display_options['arguments']['tid']['table'] = 'taxonomy_term_data';
$handler->display->display_options['arguments']['tid']['field'] = 'tid';
$handler->display->display_options['arguments']['tid']['default_action'] = 'default';
$handler->display->display_options['arguments']['tid']['default_argument_type'] = 'php';
$handler->display->display_options['arguments']['tid']['default_argument_options']['code'] = 'return aop_utility_product_curriculum_listing_header($_SERVER[\'REQUEST_URI\']);';
$handler->display->display_options['arguments']['tid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['tid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['tid']['summary_options']['items_per_page'] = '25';
/* Filter criterion: Taxonomy vocabulary: Machine name */
$handler->display->display_options['filters']['machine_name']['id'] = 'machine_name';
$handler->display->display_options['filters']['machine_name']['table'] = 'taxonomy_vocabulary';
$handler->display->display_options['filters']['machine_name']['field'] = 'machine_name';
$handler->display->display_options['filters']['machine_name']['value'] = array(
  'curriculum' => 'curriculum',
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'views',
  1 => 'taxonomy',
  2 => 'image',
  3 => 'text',
);
