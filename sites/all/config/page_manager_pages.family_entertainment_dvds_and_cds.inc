<?php
/**
 * @file
 * page_manager_pages.family_entertainment_dvds_and_cds.inc
 */

$api = '2.0.0';

$data = $page = new stdClass();
$page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
$page->api_version = 1;
$page->name = 'family_entertainment_dvds_and_cds';
$page->task = 'page';
$page->admin_title = 'Family Entertainment DVDs and CDs';
$page->admin_description = 'Aop family entertainment dvds and cds views';
$page->path = 'family-entertainment';
$page->access = array();
$page->menu = array();
$page->arguments = array();
$page->conf = array(
  'admin_paths' => FALSE,
);


$dependencies = array(
  'page_manager_handlers.page_family_entertainment_dvds_and_cds_panel_context' => 'page_manager_handlers.page_family_entertainment_dvds_and_cds_panel_context',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
