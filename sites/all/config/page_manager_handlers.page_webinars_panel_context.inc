<?php
/**
 * @file
 * page_manager_handlers.page_webinars_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_webinars_panel_context';
$handler->task = 'page';
$handler->subtask = 'webinars';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'flexible';
$display->layout_settings = array(
  'items' => array(
    'canvas' => array(
      'type' => 'row',
      'contains' => 'column',
      'children' => array(
        0 => 'main',
      ),
      'parent' => NULL,
    ),
    'main' => array(
      'type' => 'column',
      'width' => 100,
      'width_type' => '%',
      'children' => array(
        0 => 'main-row',
      ),
      'parent' => 'canvas',
    ),
    'main-row' => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'center',
        1 => 'right',
      ),
      'parent' => 'main',
    ),
    'center' => array(
      'type' => 'region',
      'title' => 'Center',
      'width' => '70.0738889956649',
      'width_type' => '%',
      'parent' => 'main-row',
    ),
    'right' => array(
      'type' => 'region',
      'title' => 'Right',
      'width' => '29.9261110043351',
      'width_type' => '%',
      'parent' => 'main-row',
      'class' => '',
    ),
  ),
);
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'center' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'a477cd90-9cbb-4a11-a494-fbfd5dbe5f3d';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-c758f008-1e76-48f6-8992-8fafc5bd8de9';
  $pane->panel = 'center';
  $pane->type = 'block';
  $pane->subtype = 'orangebox-webinar_series_info';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'c758f008-1e76-48f6-8992-8fafc5bd8de9';
  $display->content['new-c758f008-1e76-48f6-8992-8fafc5bd8de9'] = $pane;
  $display->panels['center'][0] = 'new-c758f008-1e76-48f6-8992-8fafc5bd8de9';
  $pane = new stdClass();
  $pane->pid = 'new-e2177aac-bb65-4abd-a7fe-348bef1d9e6b';
  $pane->panel = 'right';
  $pane->type = 'block';
  $pane->subtype = 'orangebox-list_owls';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'e2177aac-bb65-4abd-a7fe-348bef1d9e6b';
  $display->content['new-e2177aac-bb65-4abd-a7fe-348bef1d9e6b'] = $pane;
  $display->panels['right'][0] = 'new-e2177aac-bb65-4abd-a7fe-348bef1d9e6b';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = '0';
$handler->conf['display'] = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
