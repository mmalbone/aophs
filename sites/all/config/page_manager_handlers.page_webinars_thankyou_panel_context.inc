<?php
/**
 * @file
 * page_manager_handlers.page_webinars_thankyou_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_webinars_thankyou_panel_context';
$handler->task = 'page';
$handler->subtask = 'webinars_thankyou';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '294b46d1-cadf-4902-8c72-df6c796e780b';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-097c4383-a602-4b9a-afc1-6ec840aa0751';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Thank You!',
    'body' => '<p>Your registration has been recorded.</p>
',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '097c4383-a602-4b9a-afc1-6ec840aa0751';
  $display->content['new-097c4383-a602-4b9a-afc1-6ec840aa0751'] = $pane;
  $display->panels['middle'][0] = 'new-097c4383-a602-4b9a-afc1-6ec840aa0751';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = 'new-097c4383-a602-4b9a-afc1-6ec840aa0751';
$handler->conf['display'] = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
