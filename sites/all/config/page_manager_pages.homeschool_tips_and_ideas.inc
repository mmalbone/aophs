<?php
/**
 * @file
 * page_manager_pages.homeschool_tips_and_ideas.inc
 */

$api = '2.0.0';

$data = $page = new stdClass();
$page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
$page->api_version = 1;
$page->name = 'homeschool_tips_and_ideas';
$page->task = 'page';
$page->admin_title = '';
$page->admin_description = '';
$page->path = 'homeschool-tips-and-ideas';
$page->access = array();
$page->menu = array();
$page->arguments = array();
$page->conf = array(
  'admin_paths' => FALSE,
);


$dependencies = array(
  'page_manager_handlers.page_homeschool_tips_and_ideas_panel_context' => 'page_manager_handlers.page_homeschool_tips_and_ideas_panel_context',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
