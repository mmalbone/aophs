<?php
/**
 * @file
 * permission.edit_any_resource_content.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'edit any resource content',
  'roles' => array(
    0 => 'administrator',
    1 => 'marketing',
  ),
);

$dependencies = array(
  'content_type.resource' => 'content_type.resource',
);

$optional = array();

$modules = array(
  0 => 'node',
);
