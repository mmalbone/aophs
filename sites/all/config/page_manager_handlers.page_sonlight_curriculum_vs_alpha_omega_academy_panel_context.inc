<?php
/**
 * @file
 * page_manager_handlers.page_sonlight_curriculum_vs_alpha_omega_academy_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_sonlight_curriculum_vs_alpha_omega_academy_panel_context';
$handler->task = 'page';
$handler->subtask = 'sonlight_curriculum_vs_alpha_omega_academy';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '6f4758e8-0bf2-48e6-94eb-946e5646e768';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-92aef3bc-368b-453d-8637-9077ff453850';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Sonlight Curriculum vs. Alpha Omega Academy',
    'body' => '<p>Do you need to compare the literature-based curriculum program offered by Sonlight to the programs offered by Alpha Omega Academy? You\'re in the right spot. Find a short overview of each curriculum and service below, an easy-to-view comparison chart, and additional comparison options!</p><h3><strong>Sonlight Curriculum</strong></h3><p>Sonlight is a Christian company that provides literature-based homeschool courses for students in K-12th grades. Sonlight provides complete curriculum packages, resources, and materials designed to instill a love of learning in the homeschooled student. The Sonlight Curriculum consists of collections of literature designed to provide everything parents need to teach one child for an entire year. Sonlight\'s Core programs integrate history, geography, and literature into a coordinated whole. In addition, language arts, math, science, and electives can be added to the Core to provide a complete academic course load. Core instructor\'s guides are available to provide scheduling and teaching helps, study guides, answer keys, and more. Parents who use the Sonlight curriculum are responsible for all administrative tasks, including all required lesson planning, grading, record-keeping, and reporting.</p><p>Designed to instill a Christian worldview, all Sonlight curriculum utilizes literature, both fiction and non-fiction, to provide all academic content. Science instruction is based on a biblical view of creation and the origin of life. All Sonlight curriculum coursework can be purchased as Core packages (history, geography, and literature) or as individual subjects (math, science, Bible, language arts, and electives). Kits provide all essential materials for both students and teachers. Optional academic and administrative resources can be purchased separately.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>Trying to decide between a literature-based curriculum program and a distance learning academy for your child? Keep in mind that there is no one perfect answer for homeschooling. Giving the flexibility of using multiple resources instead of one set curriculum, a blended approach to homeschooling is often chosen by many parents. Instead of feeling torn between curriculum and an academy, parents should be open to mixing and matching materials and resources to customize their child\'s education. Each child is unique and has his own learning style. It\'s important to choose materials and educational services that best fit the needs of the individual child and of the homeschool family.</p><h3><strong>Alpha Omega Academy</strong></h3><p>Alpha Omega Academy (AOA) is an accredited distance learning program for homeschool children in grades K-12. Fully accredited by NCA CASI, AOA offers an at-home education with the convenience of distance learning. Providing qualified teachers, administrative services, and multiple curriculum choices, AOA has enrolled thousands of students from all 50 states and over 42 countries. Great for homeschoolers who are looking for the assurance of accredited courses, AOA offers a high school diploma program and the security of course credits that are readily transferred to other schools and programs. AOA provides families with flexibility in scheduling and completion of assigned coursework. Students must complete coursework within ten months of the start date.</p><p>Alpha Omega Academy allows families to choose between Alpha Omega Publications\' print-based or computer-based formats. Both curriculum options can be perfectly customized to fit your child\'s learning needs and educational goals. Both options provide student-paced lessons that can be completed independently with parental supervision. The print-based program requires a parent to grade daily work. All AOP curriculum promotes a solid, Christian worldview. Core courses include Bible, language arts, math, science, and history and geography. Elementary and high school electives are also available. AOA offers both college-prep and general studies diploma tracks. AOA offers a complete line of over 140 academically challenging courses for students in grades K-12. Placement testing, summer school options, academic support, and achievement testing are all available. Full-time or part-time enrollment is offered. A National Honor Society membership (by invitation) and high school graduation ceremony are also available to AOA students.</p><h3><strong>Benefits and Features of Alpha Omega Academy</strong></h3><p class="list_heading">Alpha Omega Academy</p><ul><li>accredited K-12 distance learning program for homeschoolers</li><li>flexible, at-home learning environment with the convenience of distance learning</li><li>choice of print- or computer-based, Christ-centered curriculum options</li><li>complete high school graduation program</li></ul><h3 class="para-start"><strong>Take a Closer Look at Sonlight Curriculum and Alpha Omega Academy</strong></h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">Alpha Omega Academy</th><th class="compare">Sonlight Curriculum</th></tr>
</thead>
<tbody>
<tr>
<td>K-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Student-directed curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Computer-based or print-based curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Fully accredited courses</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Convenient distance learning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Placement tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>High school graduation program</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Flexible and individualized</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Mastery-based learning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Direct teacher/student interaction</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Helpful academic support</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: Sonlight Curriculum versus Alpha Omega Publications</h4>
<ul class="comp_list">
<li><a href="/sonlight-vs-monarch">Sonlight vs. Monarch</a></li>
<li><a href="/sonlight-vs-aoa">Sonlight vs. Alpha Omega Academy</a></li>
<li><a href="/sonlight-vs-horizons">Sonlight vs. Horizons</a></li>
<li><a href="/sonlight-vs-lifepac">Sonlight vs. LIFEPAC</a></li>
<li><a href="/sonlight-vs-sos">Sonlight vs. Switched-On Schoolhouse</a></li>
</ul>
<p>*Sonlight® is the registered trademark of Sonlight Curriculum, LTD. Alpha Omega Publications is not in any way affiliated with Sonlight Curriculum. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by Sonlight.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '92aef3bc-368b-453d-8637-9077ff453850';
  $display->content['new-92aef3bc-368b-453d-8637-9077ff453850'] = $pane;
  $display->panels['left'][0] = 'new-92aef3bc-368b-453d-8637-9077ff453850';
  $pane = new stdClass();
  $pane->pid = 'new-823bf97b-1fda-4aa0-8e1b-7434b446e20f';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '823bf97b-1fda-4aa0-8e1b-7434b446e20f';
  $display->content['new-823bf97b-1fda-4aa0-8e1b-7434b446e20f'] = $pane;
  $display->panels['right'][0] = 'new-823bf97b-1fda-4aa0-8e1b-7434b446e20f';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-92aef3bc-368b-453d-8637-9077ff453850';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
