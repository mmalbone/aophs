<?php
/**
 * @file
 * page_manager_pages.schedule_a_call.inc
 */

$api = '2.0.0';

$data = $page = new stdClass();
$page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
$page->api_version = 1;
$page->name = 'schedule_a_call';
$page->task = 'page';
$page->admin_title = 'Schedule a Call';
$page->admin_description = '';
$page->path = 'support/schedule-a-call-back';
$page->access = array();
$page->menu = array();
$page->arguments = array();
$page->conf = array(
  'admin_paths' => FALSE,
);


$dependencies = array(
  'page_manager_handlers.page_schedule_a_call_panel_context' => 'page_manager_handlers.page_schedule_a_call_panel_context',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
