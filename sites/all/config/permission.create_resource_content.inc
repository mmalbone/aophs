<?php
/**
 * @file
 * permission.create_resource_content.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'create resource content',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array(
  'content_type.resource' => 'content_type.resource',
);

$optional = array();

$modules = array(
  0 => 'node',
);
