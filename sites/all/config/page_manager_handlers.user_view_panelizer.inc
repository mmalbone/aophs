<?php
/**
 * @file
 * page_manager_handlers.user_view_panelizer.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'user_view_panelizer';
$handler->task = 'user_view';
$handler->subtask = '';
$handler->handler = 'panelizer_node';
$handler->weight = -100;
$handler->conf = array(
  'title' => 'User panelizer',
  'context' => 'argument_entity_id:user_1',
  'access' => array(),
  'did' => '1025',
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
