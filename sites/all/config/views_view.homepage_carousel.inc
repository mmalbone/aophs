<?php
/**
 * @file
 * views_view.homepage_carousel.inc
 */

$api = '2.0.0';

$data = $view = new view();
$view->name = 'homepage_carousel';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'homepage-carousel';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'jcarousel';
$handler->display->display_options['style_options']['wrap'] = '0';
$handler->display->display_options['style_options']['visible'] = '';
$handler->display->display_options['style_options']['auto'] = '0';
$handler->display->display_options['style_options']['autoPause'] = 1;
$handler->display->display_options['style_options']['easing'] = '';
$handler->display->display_options['style_options']['vertical'] = 0;
$handler->display->display_options['row_plugin'] = 'fields';
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['content'] = '<h3>Match your family\'s needs to our homeschool<br> curriculum and resources.</h3>';
$handler->display->display_options['header']['area']['format'] = 'full_html';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: carousel item title */
$handler->display->display_options['fields']['field_carousel_item_title']['id'] = 'field_carousel_item_title';
$handler->display->display_options['fields']['field_carousel_item_title']['table'] = 'field_data_field_carousel_item_title';
$handler->display->display_options['fields']['field_carousel_item_title']['field'] = 'field_carousel_item_title';
$handler->display->display_options['fields']['field_carousel_item_title']['label'] = '';
$handler->display->display_options['fields']['field_carousel_item_title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_carousel_item_title']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_carousel_item_title']['type'] = 'text_plain';
/* Field: Content: carousel button text */
$handler->display->display_options['fields']['field_carousel_button_text']['id'] = 'field_carousel_button_text';
$handler->display->display_options['fields']['field_carousel_button_text']['table'] = 'field_data_field_carousel_button_text';
$handler->display->display_options['fields']['field_carousel_button_text']['field'] = 'field_carousel_button_text';
$handler->display->display_options['fields']['field_carousel_button_text']['label'] = '';
$handler->display->display_options['fields']['field_carousel_button_text']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_carousel_button_text']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_carousel_button_text']['type'] = 'text_plain';
/* Field: Content: carousel hover text */
$handler->display->display_options['fields']['field_carousel_hover_text']['id'] = 'field_carousel_hover_text';
$handler->display->display_options['fields']['field_carousel_hover_text']['table'] = 'field_data_field_carousel_hover_text';
$handler->display->display_options['fields']['field_carousel_hover_text']['field'] = 'field_carousel_hover_text';
$handler->display->display_options['fields']['field_carousel_hover_text']['label'] = '';
$handler->display->display_options['fields']['field_carousel_hover_text']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_carousel_hover_text']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_carousel_hover_text']['type'] = 'text_plain';
/* Field: Content: carousel item hover logo */
$handler->display->display_options['fields']['field_carousel_item_hover_logo']['id'] = 'field_carousel_item_hover_logo';
$handler->display->display_options['fields']['field_carousel_item_hover_logo']['table'] = 'field_data_field_carousel_item_hover_logo';
$handler->display->display_options['fields']['field_carousel_item_hover_logo']['field'] = 'field_carousel_item_hover_logo';
$handler->display->display_options['fields']['field_carousel_item_hover_logo']['label'] = '';
$handler->display->display_options['fields']['field_carousel_item_hover_logo']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_carousel_item_hover_logo']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_carousel_item_hover_logo']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_carousel_item_hover_logo']['settings'] = array(
  'image_style' => 'thumbnail',
  'image_link' => '',
);
/* Field: Content: carousel item logo */
$handler->display->display_options['fields']['field_carousel_item_logo']['id'] = 'field_carousel_item_logo';
$handler->display->display_options['fields']['field_carousel_item_logo']['table'] = 'field_data_field_carousel_item_logo';
$handler->display->display_options['fields']['field_carousel_item_logo']['field'] = 'field_carousel_item_logo';
$handler->display->display_options['fields']['field_carousel_item_logo']['label'] = '';
$handler->display->display_options['fields']['field_carousel_item_logo']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_carousel_item_logo']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_carousel_item_logo']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_carousel_item_logo']['settings'] = array(
  'image_style' => 'thumbnail',
  'image_link' => '',
);
/* Field: Content: carousel item title */
$handler->display->display_options['fields']['field_carousel_item_title_1']['id'] = 'field_carousel_item_title_1';
$handler->display->display_options['fields']['field_carousel_item_title_1']['table'] = 'field_data_field_carousel_item_title';
$handler->display->display_options['fields']['field_carousel_item_title_1']['field'] = 'field_carousel_item_title';
$handler->display->display_options['fields']['field_carousel_item_title_1']['label'] = '';
$handler->display->display_options['fields']['field_carousel_item_title_1']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_carousel_item_title_1']['type'] = 'text_plain';
/* Field: Content: carousel item url */
$handler->display->display_options['fields']['field_carousel_item_url']['id'] = 'field_carousel_item_url';
$handler->display->display_options['fields']['field_carousel_item_url']['table'] = 'field_data_field_carousel_item_url';
$handler->display->display_options['fields']['field_carousel_item_url']['field'] = 'field_carousel_item_url';
$handler->display->display_options['fields']['field_carousel_item_url']['label'] = '';
$handler->display->display_options['fields']['field_carousel_item_url']['element_type'] = '0';
$handler->display->display_options['fields']['field_carousel_item_url']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_carousel_item_url']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_carousel_item_url']['type'] = 'text_plain';
/* Field: Content: carousel short description */
$handler->display->display_options['fields']['field_carousel_short_description']['id'] = 'field_carousel_short_description';
$handler->display->display_options['fields']['field_carousel_short_description']['table'] = 'field_data_field_carousel_short_description';
$handler->display->display_options['fields']['field_carousel_short_description']['field'] = 'field_carousel_short_description';
$handler->display->display_options['fields']['field_carousel_short_description']['label'] = '';
$handler->display->display_options['fields']['field_carousel_short_description']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_carousel_short_description']['element_default_classes'] = FALSE;
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'carousel_item' => 'carousel_item',
);

/* Display: Content pane */
$handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'views',
  1 => 'node',
  2 => 'text',
  3 => 'image',
  4 => 'views_content',
);
