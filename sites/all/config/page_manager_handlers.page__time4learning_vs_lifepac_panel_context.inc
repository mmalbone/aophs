<?php
/**
 * @file
 * page_manager_handlers.page__time4learning_vs_lifepac_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page__time4learning_vs_lifepac_panel_context';
$handler->task = 'page';
$handler->subtask = '_time4learning_vs_lifepac';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '07fdd4f9-2a87-4374-9ddf-45f0c19b2337';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-799934b1-7a13-4aa4-9a6b-b72626ae901e';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => ' Time4Learning vs. LIFEPAC',
    'body' => '<p>Are you looking for a comparison of the online homeschool program offered by Time4Learning and LIFEPAC from Alpha Omega Publications? You\'re in the right spot. Find a short overview of each curriculum and service below, an easy-to-view comparison chart, and additional comparison options!</p><h3><strong>Time4Learning Online Curriculum</strong></h3><p>Time4Learning, an online educational program for preschool through 8th grade students, provides academic instruction through multimedia, interactive project-based activities, and worksheets. Time4Learning has many appropriate applications including its use as a core curriculum for homeschooled students. Courses include language arts, math, science, and social studies. This student-paced, flexible approach to academic instruction presents engaging interactive lessons in a suggested sequence that correlates to state standards. The math and language arts courses provide comprehensive programs that need no supplementation. The Time4Learning science (1st-6th grade) and social studies (2nd-7th grade) courses may require supplementation in order to satisfy local requirements.</p><p>Because Time4Learning is a secular homeschooling program, parents from various faiths and cultural backgrounds may find Time4Learning appropriate for their families\' homeschool needs. For parents, Time4Learning provides detailed reporting, simple record-keeping, lesson plans, teaching tools, and more. Because Time4Learning is web-based, families have complete flexibility regarding their use of the curriculum. A computer with a high speed internet connection (DSL or cable modem) is all that is needed to provide a quality homeschool experience!</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>No matter what type of homeschooling methods or curriculum you are considering, remember that there is no one perfect homeschool curriculum. A blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Rather than feeling torn between different curriculums, parents should be open to mixing and matching materials and resources to customize their child\'s education. Remember, each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3><strong>LIFEPAC from Alpha Omega Publications</strong></h3><p>LIFEPAC is a K-12 print-based curriculum offered by Alpha Omega Publications, a Christian-based publisher that provides curriculum and educational services to Christian schools and homeschool families. Full-color and consumable, LIFEPAC is an integrated, Bible-based curriculum developed by a team of accomplished educators and teachers. Using a mastery-based learning approach, each LIFEPAC course is comprised of ten student-directed worktexts that provide a full year of instruction. Individual worktexts may also be used to supplement the homeschool curriculum or to fill in learning gaps. LIFEPAC worktexts include self tests and unit tests to provide assessment of student progress. Teacher\'s guides offer practical teaching tips, additional learning activities, and complete answer keys.</p><p>An economical, proven homeschool curriculum, LIFEPAC integrates Scripture and a Christian worldview throughout all subjects. LIFEPAC offers the flexibility of an individualized, student-driven learning approach which encourages critical thinking and academic independence. Self-paced coursework for K-12 students includes Bible (1-12), language arts (K-12), math (K-12), science (K-12), and history and geography (1-12). Electives are also available for elementary, middle, and high school students. LIFEPAC subject kits include all required student and teacher materials. LIFEPAC curriculum can be purchased as complete grade level sets or as individual subjects. Student and teacher materials and supplemental materials may be purchased separately.</p><h3><strong>Benefits and Features of LIFEPAC from Alpha Omega Publications</strong></h3><p class="list_heading">LIFEPAC</p><ul><li>full-color, print-based curriculum</li><li>offers progressive, self-paced individual unit worktexts</li><li>promotes concept mastery and cognitive reasoning skills</li><li>encourages academic independence in your children</li></ul><h3 class="para-start"><strong>Take a Closer Look at Time4Learning and LIFEPAC</strong></h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">LIFEPAC</th><th class="compare">Time4Learning</th></tr>
</thead>
<tbody>
<tr>
<td>K-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">preK-8th</td>
</tr>
<tr class="alt">
<td>Accredited program</td>
<td class="tac">&nbsp;</td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">No electives</td>
</tr>
<tr>
<td>Student-directed curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Self-paced and individualized</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Placement tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Economical</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Promotes strong thinking and communication skills</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Manageable amount of daily lesson material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Material designed for homeschoolers</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr><th class="features">Cost</th><th class="compare">Alpha Omega Publications</th><th class="compare">Time4Learning</th></tr>
</thead>
<tbody>
<tr>
<td>Average yearly cost per grade level**</td>
<td class="tac">$286.95</td>
<td class="tac" rowspan="2">$199.50<br>(same price for full access or single subject)</td>
</tr>
<tr class="alt">
<td>Average subject cost per grade level**</td>
<td class="tac">$58.95</td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: Time4Learning versus Alpha Omega Publications</h4>
<ul class="comp_list">
<li><a href="/time4learning-vs-monarch">Time4Learning vs. Monarch</a></li>
<li><a href="/time4learning-vs-aoa">Time4Learning vs. Alpha Omega Academy</a></li>
<li><a href="/time4learning-vs-horizons">Time4Learning vs. Horizons</a></li>
<li><a href="/time4learning-vs-lifepac">Time4Learning vs. LIFEPAC</a></li>
<li><a href="/time4learning-vs-sos">Time4Learning vs. Switched-On Schoolhouse</a></li>
</ul>
<p>*Time4Learning® is the exclusive trademark of Time4Learning. Alpha Omega Publications is not in any way affiliated with Time4Learning. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by Time4Learning.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p><p>**Prices obtained February 2013 from the Time4Learning website and from Alpha Omega Publications 2013 Homeschool Catalog. Yearly prices based on a comparison between the LIFEPAC 6th Grade Complete grade level set and the regular fees for access to Time4Learning curriculum for a single student for 10 months. Individual subject prices are based on comparison between the LIFEPAC 6th grade single subject cost and the regular fees for access to Time4Learning curriculum for a single student for 10 months. Costs do not reflect any additional fees or shipping and handling charges. Prices for other Time4Learning and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '799934b1-7a13-4aa4-9a6b-b72626ae901e';
  $display->content['new-799934b1-7a13-4aa4-9a6b-b72626ae901e'] = $pane;
  $display->panels['left'][0] = 'new-799934b1-7a13-4aa4-9a6b-b72626ae901e';
  $pane = new stdClass();
  $pane->pid = 'new-49df87f5-39e7-4fb5-a814-2280558faf74';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '49df87f5-39e7-4fb5-a814-2280558faf74';
  $display->content['new-49df87f5-39e7-4fb5-a814-2280558faf74'] = $pane;
  $display->panels['right'][0] = 'new-49df87f5-39e7-4fb5-a814-2280558faf74';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-799934b1-7a13-4aa4-9a6b-b72626ae901e';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
