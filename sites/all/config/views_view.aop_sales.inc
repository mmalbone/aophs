<?php
/**
 * @file
 * views_view.aop_sales.inc
 */

$api = '2.0.0';

$data = $view = new view();
$view->name = 'aop_sales';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'commerce_line_item';
$view->human_name = 'aop_sales';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['group_by'] = TRUE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'commerce_product' => 'commerce_product',
  'quantity' => 'quantity',
);
$handler->display->display_options['style_options']['default'] = 'commerce_product';
$handler->display->display_options['style_options']['info'] = array(
  'commerce_product' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'quantity' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Field: Commerce Line item: Product */
$handler->display->display_options['fields']['commerce_product']['id'] = 'commerce_product';
$handler->display->display_options['fields']['commerce_product']['table'] = 'field_data_commerce_product';
$handler->display->display_options['fields']['commerce_product']['field'] = 'commerce_product';
$handler->display->display_options['fields']['commerce_product']['label'] = '';
$handler->display->display_options['fields']['commerce_product']['element_type'] = '0';
$handler->display->display_options['fields']['commerce_product']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['commerce_product']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['commerce_product']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['commerce_product']['type'] = 'commerce_product_reference_sku_link';
$handler->display->display_options['fields']['commerce_product']['settings'] = array(
  'show_quantity' => 0,
  'default_quantity' => '1',
  'combine' => 1,
  'show_single_product_attributes' => 0,
  'line_item_type' => 'product',
);
$handler->display->display_options['fields']['commerce_product']['field_api_classes'] = TRUE;
/* Field: SUM(Commerce Line Item: Quantity) */
$handler->display->display_options['fields']['quantity']['id'] = 'quantity';
$handler->display->display_options['fields']['quantity']['table'] = 'commerce_line_item';
$handler->display->display_options['fields']['quantity']['field'] = 'quantity';
$handler->display->display_options['fields']['quantity']['group_type'] = 'sum';
$handler->display->display_options['fields']['quantity']['set_precision'] = TRUE;
$handler->display->display_options['fields']['quantity']['precision'] = '0';
$handler->display->display_options['fields']['quantity']['separator'] = '';
/* Sort criterion: Commerce Line item: Product (commerce_product) */
$handler->display->display_options['sorts']['commerce_product_product_id_1']['id'] = 'commerce_product_product_id_1';
$handler->display->display_options['sorts']['commerce_product_product_id_1']['table'] = 'field_data_commerce_product';
$handler->display->display_options['sorts']['commerce_product_product_id_1']['field'] = 'commerce_product_product_id';
/* Filter criterion: Commerce Line Item: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'commerce_line_item';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'product' => 'product',
);
/* Filter criterion: Commerce Line Item: Created date */
$handler->display->display_options['filters']['created']['id'] = 'created';
$handler->display->display_options['filters']['created']['table'] = 'commerce_line_item';
$handler->display->display_options['filters']['created']['field'] = 'created';
$handler->display->display_options['filters']['created']['operator'] = '>=';
$handler->display->display_options['filters']['created']['value']['value'] = '-1 year';
$handler->display->display_options['filters']['created']['value']['type'] = 'offset';


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'views',
  1 => 'commerce_line_item',
  2 => 'commerce_product_reference',
);
