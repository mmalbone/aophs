<?php
/**
 * @file
 * oauth_common_context.mule.inc
 */

$api = '2.0.0';

$data = $context = new stdClass();
$context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
$context->api_version = 1;
$context->name = 'mule';
$context->title = 'mule';
$context->authorization_options = array(
  'access_token_lifetime' => NULL,
  'page_title' => NULL,
  'message' => NULL,
  'warning' => NULL,
  'deny_access_title' => NULL,
  'grant_access_title' => NULL,
  'disable_auth_level_selection' => NULL,
  'signature_methods' => array(
    0 => 'HMAC-SHA1',
  ),
  'default_authorization_levels' => array(
    0 => 'muleoauth',
  ),
);
$context->authorization_levels = array(
  'muleoauth' => array(
    'name' => 'muleoauth',
    'title' => 'muleoauth',
    'default' => 1,
    'delete' => 0,
    'description' => '',
  ),
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'oauth_common',
);
