<?php
/**
 * @file
 * facetapi.search_api_product_display__field_product_field_subject_name.inc
 */

$api = '2.0.0';

$data = $facet = new stdClass();
$facet->disabled = FALSE; /* Edit this to true to make a default facet disabled initially */
$facet->api_version = 1;
$facet->name = 'search_api@product_display::field_product:field_subject:name';
$facet->searcher = 'search_api@product_display';
$facet->realm = '';
$facet->facet = 'field_product:field_subject:name';
$facet->enabled = FALSE;
$facet->settings = array(
  'operator' => 'and',
  'hard_limit' => '50',
  'dependencies' => array(
    'bundle' => 'none',
    'bundle_selected' => array(),
  ),
  'facet_mincount' => '1',
  'facet_missing' => '0',
  'flatten' => 0,
  'query_type' => 'term',
  'default_true' => '1',
  'facet_search_ids' => array(),
  'exclude' => 0,
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'facetapi',
);
