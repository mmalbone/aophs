<?php
/**
 * @file
 * page_manager_handlers.page_about_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_about_panel_context';
$handler->task = 'page';
$handler->subtask = 'about';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => 'aop-about-page',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'flexible';
$display->layout_settings = array(
  'items' => array(
    'canvas' => array(
      'type' => 'row',
      'contains' => 'column',
      'children' => array(
        0 => 'main',
      ),
      'parent' => NULL,
    ),
    'main' => array(
      'type' => 'column',
      'width' => 100,
      'width_type' => '%',
      'children' => array(
        0 => 1,
        1 => 'main-row',
      ),
      'parent' => 'canvas',
    ),
    'main-row' => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'center',
        1 => 'right',
      ),
      'parent' => 'main',
    ),
    'center' => array(
      'type' => 'region',
      'title' => 'Center',
      'width' => '70.02345944876843',
      'width_type' => '%',
      'parent' => 'main-row',
    ),
    'right' => array(
      'type' => 'region',
      'title' => 'RIGHT',
      'width' => '29.97654055123156',
      'width_type' => '%',
      'parent' => 'main-row',
      'class' => '',
    ),
    1 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'center_',
      ),
      'parent' => 'main',
      'class' => 'aop-about-top',
    ),
    'center_' => array(
      'type' => 'region',
      'title' => 'Center',
      'width' => 100,
      'width_type' => '%',
      'parent' => '1',
      'class' => 'aop-about-top-content',
    ),
  ),
);
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'center' => NULL,
    'right' => NULL,
    'center_' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '97b51d2d-428b-4991-9718-9cc9f02d2fd7';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-e43b9f78-18b1-490f-a329-26c5d69fd161';
  $pane->panel = 'center';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'AOP About',
    'title' => '',
    'body' => '<h2>Exceptional Christian Homeschool Curriculum</h2><p>Wondering what makes Alpha Omega Publications stand apart as a provider of PreK-12 Christian homeschool curriculum? Headquartered in Rock Rapids, Iowa, with offices in Chandler, Arizona, AOP has revolutionized Christian education with over 35 years of experience in offering affordable curriculum, educational books and games, support services, and family entertainment to homeschool customers worldwide.</p><h3>Why is Alpha Omega Publications dedicated to homeschooling?</h3><p>As a leading Christian homeschool curriculum publisher, since 1977, AOP\'s award-winning educational products have enabled thousands of families to discover the amazing benefits of a Christian home education. Take a closer look and learn the top ten reasons to homeschool.</p><h3>What products does Alpha Omega Publications offer?</h3><p>Learn more about AOP\'s complete selection of Christian homeschool curriculum with print-based, computer-based, and online formats, you can quickly find the perfect educational option for your homeschool family.</p><h3>Why should you use AOP\'s homeschooling resources?</h3><p>For over 35 years, Alpha Omega Publications has listened to the needs of homeschooling parents like you. Designing quality homeschool curriculum that works, AOP has been recognized by the homeschooling industry with many prestigious awards.</p><h3>How does AOP support today\'s homeschooling community?</h3><p>More than just a curriculum provider, AOP offers valuable encouragement and information for your homeschooling and your personal walk with God. Interact with other parents on AOP\'s Homeschool View blog and monthly newsletter. You can also get connected to our community of like-minded homeschoolers on Facebook, Pinterest, Instagram, and Twitter.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'e43b9f78-18b1-490f-a329-26c5d69fd161';
  $display->content['new-e43b9f78-18b1-490f-a329-26c5d69fd161'] = $pane;
  $display->panels['center'][0] = 'new-e43b9f78-18b1-490f-a329-26c5d69fd161';
  $pane = new stdClass();
  $pane->pid = 'new-012c8a04-478c-46ba-aa21-c6b277c67e92';
  $pane->panel = 'center_';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => '',
    'body' => '<h1>About Alpha Omega Publications</h1><h2>Changing education for the glory of God.</h2>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'aop-about-alpha-omega',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '012c8a04-478c-46ba-aa21-c6b277c67e92';
  $display->content['new-012c8a04-478c-46ba-aa21-c6b277c67e92'] = $pane;
  $display->panels['center_'][0] = 'new-012c8a04-478c-46ba-aa21-c6b277c67e92';
  $pane = new stdClass();
  $pane->pid = 'new-90fdf969-69fe-471a-bce9-4abac5b543e7';
  $pane->panel = 'right';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => '',
    'body' => '<p><a href="https://www.facebook.com/aophomeschooling" onclick="window.open(this.href, \'\', \'resizable=yes,status=no,location=no,toolbar=no,menubar=no,fullscreen=no,scrollbars=no,dependent=no\'); return false;"><em>Join our growin</em>g</a></p>

<p><a href="https://www.facebook.com/aophomeschooling" onclick="window.open(this.href, \'\', \'resizable=yes,status=no,location=no,toolbar=no,menubar=no,fullscreen=no,scrollbars=no,dependent=no\'); return false;">FACEBOOK COMMUNITY</a><br />
&nbsp;</p>
',
    'format' => 'full_html',
    'substitute' => TRUE,
    'name' => 'aop_join_facebook',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'aop-about-join-facebook',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '90fdf969-69fe-471a-bce9-4abac5b543e7';
  $display->content['new-90fdf969-69fe-471a-bce9-4abac5b543e7'] = $pane;
  $display->panels['right'][0] = 'new-90fdf969-69fe-471a-bce9-4abac5b543e7';
  $pane = new stdClass();
  $pane->pid = 'new-7081a354-e7ff-4c39-9885-8c6b485a136f';
  $pane->panel = 'right';
  $pane->type = 'block';
  $pane->subtype = 'block-33';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<span>Our Mission</span>',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '7081a354-e7ff-4c39-9885-8c6b485a136f';
  $display->content['new-7081a354-e7ff-4c39-9885-8c6b485a136f'] = $pane;
  $display->panels['right'][1] = 'new-7081a354-e7ff-4c39-9885-8c6b485a136f';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = '0';
$handler->conf['display'] = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
