<?php
/**
 * @file
 * views_view.organization.inc
 */

$api = '2.0.0';

$data = $view = new view();
$view->name = 'organization';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Organization';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Homeschool Organizations';
$handler->display->display_options['use_ajax'] = TRUE;
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Location: City */
$handler->display->display_options['fields']['city']['id'] = 'city';
$handler->display->display_options['fields']['city']['table'] = 'location';
$handler->display->display_options['fields']['city']['field'] = 'city';
/* Field: Location: Province */
$handler->display->display_options['fields']['province']['id'] = 'province';
$handler->display->display_options['fields']['province']['table'] = 'location';
$handler->display->display_options['fields']['province']['field'] = 'province';
$handler->display->display_options['fields']['province']['label'] = 'State';
/* Sort criterion: Location: Province */
$handler->display->display_options['sorts']['province']['id'] = 'province';
$handler->display->display_options['sorts']['province']['table'] = 'location';
$handler->display->display_options['sorts']['province']['field'] = 'province';
/* Sort criterion: Location: City */
$handler->display->display_options['sorts']['city']['id'] = 'city';
$handler->display->display_options['sorts']['city']['table'] = 'location';
$handler->display->display_options['sorts']['city']['field'] = 'city';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'organization' => 'organization',
);

/* Display: Organization List by State */
$handler = $view->new_display('panel_pane', 'Organization List by State', 'panel_pane_1');
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['grouping'] = array(
  0 => array(
    'field' => 'province',
    'rendered' => 1,
    'rendered_strip' => 0,
  ),
);
$handler->display->display_options['style_options']['columns'] = array(
  'title' => 'title',
  'city' => 'city',
  'province' => 'province',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'title' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'city' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'province' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Location: City */
$handler->display->display_options['fields']['city']['id'] = 'city';
$handler->display->display_options['fields']['city']['table'] = 'location';
$handler->display->display_options['fields']['city']['field'] = 'city';
/* Field: Location: Province */
$handler->display->display_options['fields']['province']['id'] = 'province';
$handler->display->display_options['fields']['province']['table'] = 'location';
$handler->display->display_options['fields']['province']['field'] = 'province';
$handler->display->display_options['fields']['province']['label'] = 'State';
$handler->display->display_options['fields']['province']['exclude'] = TRUE;
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: Location: Province */
$handler->display->display_options['sorts']['province']['id'] = 'province';
$handler->display->display_options['sorts']['province']['table'] = 'location';
$handler->display->display_options['sorts']['province']['field'] = 'province';
/* Sort criterion: Location: City */
$handler->display->display_options['sorts']['city']['id'] = 'city';
$handler->display->display_options['sorts']['city']['table'] = 'location';
$handler->display->display_options['sorts']['city']['field'] = 'city';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'organization' => 'organization',
);
/* Filter criterion: Location: Province */
$handler->display->display_options['filters']['province']['id'] = 'province';
$handler->display->display_options['filters']['province']['table'] = 'location';
$handler->display->display_options['filters']['province']['field'] = 'province';
$handler->display->display_options['filters']['province']['value'] = 'AL';
$handler->display->display_options['filters']['province']['exposed'] = TRUE;
$handler->display->display_options['filters']['province']['expose']['operator_id'] = 'province_op';
$handler->display->display_options['filters']['province']['expose']['label'] = 'State';
$handler->display->display_options['filters']['province']['expose']['operator'] = 'province_op';
$handler->display->display_options['filters']['province']['expose']['identifier'] = 'province';
$handler->display->display_options['filters']['province']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  6 => 0,
  7 => 0,
  8 => 0,
  5 => 0,
  3 => 0,
  4 => 0,
);
$handler->display->display_options['filters']['province']['type'] = 'select';

/* Display: Organization Map */
$handler = $view->new_display('panel_pane', 'Organization Map', 'panel_pane_2');
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'gmap';
$handler->display->display_options['style_options']['markertype'] = 'small lblue';
$handler->display->display_options['style_options']['latfield'] = 'address';
$handler->display->display_options['style_options']['lonfield'] = 'address';
$handler->display->display_options['style_options']['markerfield'] = 'title';
$handler->display->display_options['style_options']['geofield'] = 'title';
$handler->display->display_options['style_options']['enablermt'] = 0;
$handler->display->display_options['style_options']['rmtfield'] = 'title';
$handler->display->display_options['style_options']['rmtfieldraw'] = 1;
$handler->display->display_options['style_options']['animation'] = '0';
$handler->display->display_options['style_options']['tooltipfield'] = 'title';
$handler->display->display_options['style_options']['bubbletextfield'] = 'title';
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['row_plugin'] = 'node';
$handler->display->display_options['row_options']['view_mode'] = 'full';
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['empty'] = FALSE;
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['empty'] = TRUE;
$handler->display->display_options['empty']['area']['content'] = 'No organizations found matching your criteria.';
$handler->display->display_options['empty']['area']['format'] = 'full_html';
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Location: Address */
$handler->display->display_options['fields']['address']['id'] = 'address';
$handler->display->display_options['fields']['address']['table'] = 'location';
$handler->display->display_options['fields']['address']['field'] = 'address';
$handler->display->display_options['fields']['address']['hide'] = array(
  'name' => 0,
  'street' => 0,
  'additional' => 0,
  'city' => 0,
  'province' => 0,
  'postal_code' => 0,
  'country' => 0,
  'locpick' => 0,
  'province_name' => 0,
  'country_name' => 0,
  'map_link' => 0,
  'coords' => 0,
);
$handler->display->display_options['defaults']['sorts'] = FALSE;

/* Display: Organization Map with Proximity */
$handler = $view->new_display('panel_pane', 'Organization Map with Proximity', 'panel_pane_3');
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'gmap';
$handler->display->display_options['style_options']['markertype'] = 'small red';
$handler->display->display_options['style_options']['latfield'] = 'title';
$handler->display->display_options['style_options']['lonfield'] = 'title';
$handler->display->display_options['style_options']['markerfield'] = 'title';
$handler->display->display_options['style_options']['geofield'] = 'title';
$handler->display->display_options['style_options']['enablermt'] = 0;
$handler->display->display_options['style_options']['rmtfield'] = 'title';
$handler->display->display_options['style_options']['rmtfieldraw'] = 1;
$handler->display->display_options['style_options']['center_on_proximityarg'] = 1;
$handler->display->display_options['style_options']['animation'] = '0';
$handler->display->display_options['style_options']['tooltipenabled'] = 1;
$handler->display->display_options['style_options']['tooltipfield'] = 'city';
$handler->display->display_options['style_options']['bubbletextenabled'] = 1;
$handler->display->display_options['style_options']['bubbletextfield'] = 'title';
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['sorts'] = FALSE;
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'organization' => 'organization',
);
/* Filter criterion: Location: Distance / Proximity */
$handler->display->display_options['filters']['distance']['id'] = 'distance';
$handler->display->display_options['filters']['distance']['table'] = 'location';
$handler->display->display_options['filters']['distance']['field'] = 'distance';
$handler->display->display_options['filters']['distance']['operator'] = 'dist';
$handler->display->display_options['filters']['distance']['value'] = array(
  'latitude' => '',
  'longitude' => '',
  'postal_code' => '',
  'country' => '',
  'php_code' => '',
  'nid_arg' => '',
  'nid_loc_field' => 'node',
  'uid_arg' => '',
  'search_distance' => '100',
  'search_units' => 'mile',
);
$handler->display->display_options['filters']['distance']['exposed'] = TRUE;
$handler->display->display_options['filters']['distance']['expose']['operator_id'] = 'distance_op';
$handler->display->display_options['filters']['distance']['expose']['label'] = 'Distance / Proximity';
$handler->display->display_options['filters']['distance']['expose']['operator'] = 'distance_op';
$handler->display->display_options['filters']['distance']['expose']['identifier'] = 'distance';
$handler->display->display_options['filters']['distance']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  6 => 0,
  7 => 0,
  8 => 0,
  5 => 0,
  3 => 0,
  4 => 0,
);
$handler->display->display_options['filters']['distance']['expose']['gmap_macro'] = array(
  'default' => '[gmap ]',
);
$handler->display->display_options['filters']['distance']['expose']['user_location_choose'] = array(
  'default' => FALSE,
);
$handler->display->display_options['filters']['distance']['origin'] = 'postal_default';

/* Display: Organization List by State Page */
$handler = $view->new_display('page', 'Organization List by State Page', 'page_1');
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['grouping'] = array(
  0 => array(
    'field' => 'province',
    'rendered' => 1,
    'rendered_strip' => 0,
  ),
);
$handler->display->display_options['style_options']['columns'] = array(
  'title' => 'title',
  'city' => 'city',
  'province' => 'province',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'title' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'city' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'province' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Location: City */
$handler->display->display_options['fields']['city']['id'] = 'city';
$handler->display->display_options['fields']['city']['table'] = 'location';
$handler->display->display_options['fields']['city']['field'] = 'city';
/* Field: Location: Province */
$handler->display->display_options['fields']['province']['id'] = 'province';
$handler->display->display_options['fields']['province']['table'] = 'location';
$handler->display->display_options['fields']['province']['field'] = 'province';
$handler->display->display_options['fields']['province']['label'] = 'State';
$handler->display->display_options['fields']['province']['exclude'] = TRUE;
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: Location: Province */
$handler->display->display_options['sorts']['province']['id'] = 'province';
$handler->display->display_options['sorts']['province']['table'] = 'location';
$handler->display->display_options['sorts']['province']['field'] = 'province';
/* Sort criterion: Location: City */
$handler->display->display_options['sorts']['city']['id'] = 'city';
$handler->display->display_options['sorts']['city']['table'] = 'location';
$handler->display->display_options['sorts']['city']['field'] = 'city';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'organization' => 'organization',
);
/* Filter criterion: Location: Province */
$handler->display->display_options['filters']['province']['id'] = 'province';
$handler->display->display_options['filters']['province']['table'] = 'location';
$handler->display->display_options['filters']['province']['field'] = 'province';
$handler->display->display_options['filters']['province']['value'] = 'AL';
$handler->display->display_options['filters']['province']['exposed'] = TRUE;
$handler->display->display_options['filters']['province']['expose']['operator_id'] = 'province_op';
$handler->display->display_options['filters']['province']['expose']['label'] = 'State';
$handler->display->display_options['filters']['province']['expose']['description'] = 'Select a state to narrow the list. ';
$handler->display->display_options['filters']['province']['expose']['operator'] = 'province_op';
$handler->display->display_options['filters']['province']['expose']['identifier'] = 'province';
$handler->display->display_options['filters']['province']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  6 => 0,
  7 => 0,
  8 => 0,
  5 => 0,
  3 => 0,
  4 => 0,
);
$handler->display->display_options['filters']['province']['type'] = 'select';
$handler->display->display_options['path'] = 'community/organizations-by-state';


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'views',
  1 => 'node',
  2 => 'views_content',
);
