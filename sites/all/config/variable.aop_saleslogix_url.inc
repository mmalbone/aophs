<?php
/**
 * @file
 * variable.aop_saleslogix_url.inc
 */

$api = '2.0.0';

$data = $strongarm = new stdClass();
$strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
$strongarm->api_version = 1;
$strongarm->name = 'aop_saleslogix_url';
$strongarm->value = 'https://206.169.175.177:3535/weblead.svc';


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'strongarm',
  1 => 'ctools',
  2 => 'system',
);
