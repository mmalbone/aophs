<?php
/**
 * @file
 * panelizer_defaults.node_tech_support_page_default_default.inc
 */

$api = '2.0.0';

$data = $panelizer = new stdClass();
$panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
$panelizer->api_version = 1;
$panelizer->name = 'node:tech_support_page:default:default';
$panelizer->title = 'Default';
$panelizer->panelizer_type = 'node';
$panelizer->panelizer_key = 'tech_support_page';
$panelizer->no_blocks = FALSE;
$panelizer->css_id = '';
$panelizer->css = '';
$panelizer->pipeline = 'standard';
$panelizer->contexts = array();
$panelizer->relationships = array();
$panelizer->access = array();
$panelizer->view_mode = 'default';
$panelizer->css_class = '';
$panelizer->title_element = 'H2';
$panelizer->link_to_entity = TRUE;
$panelizer->extra = array();
$display = new panels_display();
$display->layout = 'flexible';
$display->layout_settings = array();
$display->panel_settings = array();
$display->cache = array();
$display->title = '%node:title';
$display->uuid = '5182a2b5-d2e6-404f-b5a5-0d44a43ea12f';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-4eba904f-446a-4ab1-adb8-f1320ae884b0';
  $pane->panel = 'center';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:title_field';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'hidden',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '4eba904f-446a-4ab1-adb8-f1320ae884b0';
  $display->content['new-4eba904f-446a-4ab1-adb8-f1320ae884b0'] = $pane;
  $display->panels['center'][0] = 'new-4eba904f-446a-4ab1-adb8-f1320ae884b0';
  $pane = new stdClass();
  $pane->pid = 'new-fd583d6e-63ff-4dfa-b9f5-9acfd9cc8cbf';
  $pane->panel = 'center';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'text_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'fd583d6e-63ff-4dfa-b9f5-9acfd9cc8cbf';
  $display->content['new-fd583d6e-63ff-4dfa-b9f5-9acfd9cc8cbf'] = $pane;
  $display->panels['center'][1] = 'new-fd583d6e-63ff-4dfa-b9f5-9acfd9cc8cbf';
  $pane = new stdClass();
  $pane->pid = 'new-10c16726-2d4b-4122-95f6-80aa0546b6dd';
  $pane->panel = 'center';
  $pane->type = 'node_links';
  $pane->subtype = 'node_links';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => FALSE,
    'override_title_text' => '',
    'build_mode' => 'default',
    'identifier' => '',
    'link' => TRUE,
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array(
    'css_class' => 'link-wrapper',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '10c16726-2d4b-4122-95f6-80aa0546b6dd';
  $display->content['new-10c16726-2d4b-4122-95f6-80aa0546b6dd'] = $pane;
  $display->panels['center'][2] = 'new-10c16726-2d4b-4122-95f6-80aa0546b6dd';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = 'new-10c16726-2d4b-4122-95f6-80aa0546b6dd';
$panelizer->display = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'panelizer',
);
