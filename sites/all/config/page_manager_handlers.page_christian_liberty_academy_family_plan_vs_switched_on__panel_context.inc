<?php
/**
 * @file
 * page_manager_handlers.page_christian_liberty_academy_family_plan_vs_switched_on__panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_christian_liberty_academy_family_plan_vs_switched_on__panel_context';
$handler->task = 'page';
$handler->subtask = 'christian_liberty_academy_family_plan_vs_switched_on_';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '49ee7feb-e707-4181-8c43-c36ff51fdb79';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-ac24c51a-60d6-42ba-94a4-ab205b63c2bc';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => ' Christian Liberty Academy Family Plan vs. Switched-On',
    'body' => '<p>Are you investigating the differences between Christian Liberty Academy\'s Family Plan and the Switched-On Schoolhouse curriculum from Alpha Omega Publications? You just found the information you need! Find short overviews of each curriculum below, an easy-to-view comparison chart, and additional comparison options!</p><h3><strong>Christian Liberty Academy Family Plan</strong></h3><p>Christian Liberty Press, a publisher of Christ-centered educational materials, provides materials and services created to enable parents to provide a quality Christian education at home. The Christian Liberty Academy Family Plan is a distance learning approach designed to offer homeschool families time-saving services while providing a high level of independence. Families that are affiliated with the Family Plan are entitled to placement testing, online tutorials and email helpline, annual achievement testing, administrative and guidance materials, alternative course options, year-round telephone assistance, and more. Administrative tasks such as lesson planning, grading, and record-keeping are the complete responsibility of parents.</p><p>Written from a Christian worldview, all Christian Liberty courses are completely Bible-based. All science instruction is based on a biblical view of creation and the origin of life. Christian Liberty courses include Bible, language arts, literature, history, mathematics, science, and a variety of electives. Family Plan tuition includes all materials and services. With the Family Plan, there is no time limit for completion of coursework. After completion, all materials remain the property of the homeschool family. Christian Liberty Academy\'s programs are not accredited.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>Wondering which homeschooling method will work best for your family? Keep in mind that there is no one perfect homeschool curriculum. A blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Instead of feeling torn between curriculum formats, parents should be open to mixing and matching materials and resources to customize their child\'s education. Remember, each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3><strong>Switched-On Schoolhouse from Alpha Omega Publications</strong></h3><p>Switched-On Schoolhouse (SOS) from Alpha Omega Publications is an engaging, computer-based curriculum for grades 3-12. Alpha Omega Publications, a trusted publisher of high quality educational products, provides Christian curriculum and educational services to homeschool families. Revolutionizing the way students learn, SOS is a comprehensive, CD-ROM-based curriculum that is entirely technology based. This innovative, state-of-the-art curriculum presents biblically-based lessons using interactive multimedia, eye-catching animation, learning games, video clips, and more! Unlike traditional textbooks, SOS has a diverse mix of text-based instruction and engaging multimedia enrichment. Unsurpassed administrative tools include time-saving features like automatic grading and lesson planning with built-in calendar, customizable lessons, flexible printing options, and message center.</p><p>Winner of Cathy Duffy\'s 100 Top Picks for Homeschool Curriculum, SOS integrates Scripture and a Christian worldview into all subjects. SOS offers the flexibility of an individualized, student-driven approach, helpful text-to-speech option, and diagnostic tests for correct placement. Switched-On Schoolhouse courses are available for 3rd – 12th grade students in Bible, language arts, math, science, and history and geography. Electives are also available for students of all ages. SOS may be purchased as individual subjects or as complete five-subject sets. The program includes all required student and parent materials.</p><h3><strong>Benefits and Features of Switched-On Schoolhouse from Alpha Omega Publications</strong></h3><p class="list_heading">Switched-On Schoolhouse</p><ul><li>interactive, innovative, computer-based curriculum</li><li>offers flexible, engaging learning environment</li><li>contains multimedia tools, educational games, and automatic grading options</li><li>can reduce teacher planning and grading by 20 hours per week</li></ul><h3 class="para-start"><strong>Take a Closer Look at Christian Liberty Academy Family Plan and Switched-On Schoolhouse</strong></h3>

<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">Switched-On Schoolhouse</th><th class="compare">Christian Liberty Academy Family Plan</th></tr>
</thead>
<tbody>
<tr>
<td>K-12 curriculum options</td>
<td class="tac">Grade 3 - 12</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Accredited program</td>
<td class="tac">&nbsp;</td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Computer-based curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Interactive, engaging multimedia tools</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Instruction provided by innovative text-based lessons delivered via computer</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Customizable lessons and calendar</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Report cards and transcripts provided</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Manageable amount of daily lesson material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Automatic grading and lesson planning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Telephone assistance</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr><th class="features">Cost</th><th class="compare">Alpha Omega Publications</th><th class="compare">Christian Liberty Press</th></tr>
</thead>
<tbody>
<tr>
<td>Average yearly cost per grade level**</td>
<td class="tac">$449.95</td>
<td class="tac">$375</td>
</tr>
<tr class="alt">
<td>Average subject cost per grade level**</td>
<td class="tac">$99.95</td>
<td class="tac">N/A</td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: Christian Liberty Academy/Press versus Alpha Omega Publications</h4>
<p class="list_heading_links">Christian Liberty Press</p>
<ul>
<li><a href="/christian-liberty-press-vs-monarch">Christian Liberty Press vs. Monarch</a></li>
<li><a href="/christian-liberty-press-vs-aoa">Christian Liberty Press vs. Alpha Omega Academy</a></li>
<li><a href="/christian-liberty-press-vs-horizons">Christian Liberty Press vs. Horizons</a></li>
<li><a href="/christian-liberty-press-vs-lifepac">Christian Liberty Press vs. LIFEPAC</a></li>
<li><a href="/christian-liberty-press-vs-sos">Christian Liberty Press vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">Christian Liberty Academy (CLASS Plan) </p>
<ul>
<li><a href="/christian-liberty-academy-class-vs-monarch">Christian Liberty Academy (CLASS Plan) vs. Monarch</a></li>
<li><a href="/christian-liberty-academy-class-vs-aoa">Christian Liberty Academy (CLASS Plan) vs. Alpha Omega Academy</a></li>
<li><a href="/christian-liberty-academy-class-vs-horizons">Christian Liberty Academy (CLASS Plan) vs. Horizons</a></li>
<li><a href="/christian-liberty-academy-class-vs-lifepac">Christian Liberty Academy (CLASS Plan) vs. LIFEPAC</a></li>
<li><a href="/christian-liberty-academy-class-vs-sos">Christian Liberty Academy (CLASS Plan) vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">Christian Liberty Academy (Family Plan) </p>
<ul>
<li><a href="/christian-liberty-academy-family-vs-monarch">Christian Liberty Academy (Family Plan) vs. Monarch</a></li>
<li><a href="/christian-liberty-academy-family-vs-aoa">Christian Liberty Academy (Family Plan) vs. Alpha Omega Academy</a></li>
<li><a href="/christian-liberty-academy-family-vs-horizons">Christian Liberty Academy (Family Plan) vs. Horizons</a></li>
<li><a href="/christian-liberty-academy-family-vs-lifepac">Christian Liberty Academy (Family Plan) vs. LIFEPAC</a></li>
<li><a href="/christian-liberty-academy-family-vs-sos">Christian Liberty Academy (Family Plan) vs. Switched-On Schoolhouse</a></li>
</ul>
<p>Christian Liberty Press is the publishing arm of Christian Liberty Academy and Christian Liberty Academy School System (CLASS). Alpha Omega Publications is not in any way affiliated with Christian Liberty Press. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by Christian Liberty Academy.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p><p>**Prices obtained February 2013 from Christian Liberty Press and the CLASS Homeschools Website and from Alpha Omega Publications 2013 Homeschool Catalog. Yearly prices based on comparison between the Switched-On Schoolhouse 5th Grade 5-subject set and full time affiliation with the Christian Liberty Academy Family plan for a 2nd-8th grade student. Individual subject prices are based on comparison between costs for 5th Grade Switched-On Schoolhouse Math and Christian Liberty Academy single subject cost (not available). Costs do not reflect any additional fees or shipping and handling charges. Prices for other Christian Liberty Press and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'ac24c51a-60d6-42ba-94a4-ab205b63c2bc';
  $display->content['new-ac24c51a-60d6-42ba-94a4-ab205b63c2bc'] = $pane;
  $display->panels['left'][0] = 'new-ac24c51a-60d6-42ba-94a4-ab205b63c2bc';
  $pane = new stdClass();
  $pane->pid = 'new-99bb5297-e991-43b9-8772-3988d05044e8';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '99bb5297-e991-43b9-8772-3988d05044e8';
  $display->content['new-99bb5297-e991-43b9-8772-3988d05044e8'] = $pane;
  $display->panels['right'][0] = 'new-99bb5297-e991-43b9-8772-3988d05044e8';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-ac24c51a-60d6-42ba-94a4-ab205b63c2bc';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
