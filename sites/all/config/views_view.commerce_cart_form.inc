<?php
/**
 * @file
 * views_view.commerce_cart_form.inc
 */

$api = '2.0.0';

$data = $view = new view();
$view->name = 'commerce_cart_form';
$view->description = 'Display a shopping cart update form.';
$view->tag = 'commerce';
$view->base_table = 'commerce_order';
$view->human_name = 'Shopping cart form';
$view->core = 0;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Defaults */
$handler = $view->new_display('default', 'Defaults', 'default');
$handler->display->display_options['title'] = 'Shopping cart';
$handler->display->display_options['display_comment'] = 'web-1464  I set the normal "order total" footer (not output in footer) element to display:none, and used global text areas to rewrite it.';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['disable_sql_rewrite'] = TRUE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['content'] = '<div class="cart-rows-header"><div class="cart-labels"><div class="item-label">Item</div><div class="qty-label">Quantity</div><div class="price-label">Price</div></div></div>';
$handler->display->display_options['header']['area']['format'] = 'markdown';
/* Footer: Global: Text area */
$handler->display->display_options['footer']['area_1']['id'] = 'area_1';
$handler->display->display_options['footer']['area_1']['table'] = 'views';
$handler->display->display_options['footer']['area_1']['field'] = 'area';
$handler->display->display_options['footer']['area_1']['label'] = 'Cart Total';
$handler->display->display_options['footer']['area_1']['content'] = '<div class="cart-sidebar"><div class="cart-total"><div class="cart-components"><div class="component-title">Cart Total</div><div class="component-total">[commerce_order_total]test</div></div></div></div>';
$handler->display->display_options['footer']['area_1']['format'] = 'markdown';
$handler->display->display_options['footer']['area_1']['tokenize'] = TRUE;
/* Footer: Global: Text area */
$handler->display->display_options['footer']['area']['id'] = 'area';
$handler->display->display_options['footer']['area']['table'] = 'views';
$handler->display->display_options['footer']['area']['field'] = 'area';
$handler->display->display_options['footer']['area']['content'] = '<div class="cart-subtotal"><div class="component-label">Subtotal</div><div class="component-total"> [commerce_order_total]</div></div>';
$handler->display->display_options['footer']['area']['format'] = 'markdown';
$handler->display->display_options['footer']['area']['tokenize'] = TRUE;
/* Footer: Commerce Order: Order total */
$handler->display->display_options['footer']['order_total']['id'] = 'order_total';
$handler->display->display_options['footer']['order_total']['table'] = 'commerce_order';
$handler->display->display_options['footer']['order_total']['field'] = 'order_total';
/* Relationship: Commerce Order: Referenced line items */
$handler->display->display_options['relationships']['commerce_line_items_line_item_id']['id'] = 'commerce_line_items_line_item_id';
$handler->display->display_options['relationships']['commerce_line_items_line_item_id']['table'] = 'field_data_commerce_line_items';
$handler->display->display_options['relationships']['commerce_line_items_line_item_id']['field'] = 'commerce_line_items_line_item_id';
$handler->display->display_options['relationships']['commerce_line_items_line_item_id']['required'] = TRUE;
/* Relationship: Commerce Line item: Referenced products */
$handler->display->display_options['relationships']['commerce_product_product_id']['id'] = 'commerce_product_product_id';
$handler->display->display_options['relationships']['commerce_product_product_id']['table'] = 'field_data_commerce_product';
$handler->display->display_options['relationships']['commerce_product_product_id']['field'] = 'commerce_product_product_id';
$handler->display->display_options['relationships']['commerce_product_product_id']['relationship'] = 'commerce_line_items_line_item_id';
/* Relationship: Commerce Product: Referencing Content */
$handler->display->display_options['relationships']['field_product']['id'] = 'field_product';
$handler->display->display_options['relationships']['field_product']['table'] = 'commerce_product';
$handler->display->display_options['relationships']['field_product']['field'] = 'field_product';
$handler->display->display_options['relationships']['field_product']['relationship'] = 'commerce_product_product_id';
/* Field: Content: Path */
$handler->display->display_options['fields']['path']['id'] = 'path';
$handler->display->display_options['fields']['path']['table'] = 'node';
$handler->display->display_options['fields']['path']['field'] = 'path';
$handler->display->display_options['fields']['path']['relationship'] = 'field_product';
$handler->display->display_options['fields']['path']['label'] = '';
$handler->display->display_options['fields']['path']['exclude'] = TRUE;
$handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
/* Field: Commerce Line Item: Title */
$handler->display->display_options['fields']['line_item_title_1']['id'] = 'line_item_title_1';
$handler->display->display_options['fields']['line_item_title_1']['table'] = 'commerce_line_item';
$handler->display->display_options['fields']['line_item_title_1']['field'] = 'line_item_title';
$handler->display->display_options['fields']['line_item_title_1']['relationship'] = 'commerce_line_items_line_item_id';
$handler->display->display_options['fields']['line_item_title_1']['label'] = '';
$handler->display->display_options['fields']['line_item_title_1']['exclude'] = TRUE;
$handler->display->display_options['fields']['line_item_title_1']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['line_item_title_1']['alter']['text'] = '<a class="cart-form-title" href="[path]">[line_item_title_1]</a>';
$handler->display->display_options['fields']['line_item_title_1']['element_label_colon'] = FALSE;
/* Field: Commerce Line item: Product */
$handler->display->display_options['fields']['commerce_product']['id'] = 'commerce_product';
$handler->display->display_options['fields']['commerce_product']['table'] = 'field_data_commerce_product';
$handler->display->display_options['fields']['commerce_product']['field'] = 'commerce_product';
$handler->display->display_options['fields']['commerce_product']['relationship'] = 'commerce_line_items_line_item_id';
$handler->display->display_options['fields']['commerce_product']['label'] = '';
$handler->display->display_options['fields']['commerce_product']['exclude'] = TRUE;
$handler->display->display_options['fields']['commerce_product']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['commerce_product']['type'] = 'commerce_product_reference_rendered_product';
$handler->display->display_options['fields']['commerce_product']['settings'] = array(
  'view_mode' => 'product_in_cart',
  'page' => 1,
);
/* Field: Commerce Line item: Product */
$handler->display->display_options['fields']['commerce_product_1']['id'] = 'commerce_product_1';
$handler->display->display_options['fields']['commerce_product_1']['table'] = 'field_data_commerce_product';
$handler->display->display_options['fields']['commerce_product_1']['field'] = 'commerce_product';
$handler->display->display_options['fields']['commerce_product_1']['relationship'] = 'commerce_line_items_line_item_id';
$handler->display->display_options['fields']['commerce_product_1']['label'] = '';
$handler->display->display_options['fields']['commerce_product_1']['exclude'] = TRUE;
$handler->display->display_options['fields']['commerce_product_1']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['commerce_product_1']['alter']['text'] = '<div class="cart-form-sku">PRODUCT ID [commerce_product_1]</div>';
$handler->display->display_options['fields']['commerce_product_1']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['commerce_product_1']['type'] = 'commerce_product_reference_sku_plain';
$handler->display->display_options['fields']['commerce_product_1']['settings'] = array(
  'show_quantity' => 0,
  'default_quantity' => '1',
  'combine' => 1,
  'show_single_product_attributes' => 0,
  'line_item_type' => 'product',
);
/* Field: Commerce Product: Product Image */
$handler->display->display_options['fields']['field_product_image']['id'] = 'field_product_image';
$handler->display->display_options['fields']['field_product_image']['table'] = 'field_data_field_product_image';
$handler->display->display_options['fields']['field_product_image']['field'] = 'field_product_image';
$handler->display->display_options['fields']['field_product_image']['relationship'] = 'commerce_product_product_id';
$handler->display->display_options['fields']['field_product_image']['label'] = '';
$handler->display->display_options['fields']['field_product_image']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_product_image']['alter']['text'] = '<img src="https://s3.amazonaws.com/glnnewmedia/media/catalog/product[field_product_image-value]" alt="[line_item_title]">';
$handler->display->display_options['fields']['field_product_image']['element_type'] = 'div';
$handler->display->display_options['fields']['field_product_image']['element_class'] = 'cart-form-image';
$handler->display->display_options['fields']['field_product_image']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_product_image']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_product_image']['type'] = 'text_plain';
/* Field: Global: Custom text */
$handler->display->display_options['fields']['nothing']['id'] = 'nothing';
$handler->display->display_options['fields']['nothing']['table'] = 'views';
$handler->display->display_options['fields']['nothing']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing']['label'] = '';
$handler->display->display_options['fields']['nothing']['alter']['text'] = '[line_item_title_1]<br/>
[commerce_product_1]
 ';
$handler->display->display_options['fields']['nothing']['element_type'] = 'div';
$handler->display->display_options['fields']['nothing']['element_class'] = 'cart-form-product';
$handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
/* Field: Commerce Line item: Unit price */
$handler->display->display_options['fields']['commerce_unit_price']['id'] = 'commerce_unit_price';
$handler->display->display_options['fields']['commerce_unit_price']['table'] = 'field_data_commerce_unit_price';
$handler->display->display_options['fields']['commerce_unit_price']['field'] = 'commerce_unit_price';
$handler->display->display_options['fields']['commerce_unit_price']['relationship'] = 'commerce_line_items_line_item_id';
$handler->display->display_options['fields']['commerce_unit_price']['label'] = 'Price';
$handler->display->display_options['fields']['commerce_unit_price']['exclude'] = TRUE;
$handler->display->display_options['fields']['commerce_unit_price']['element_class'] = 'price';
$handler->display->display_options['fields']['commerce_unit_price']['click_sort_column'] = 'amount';
$handler->display->display_options['fields']['commerce_unit_price']['settings'] = array(
  'calculation' => FALSE,
);
/* Field: Commerce Line Item: Delete button */
$handler->display->display_options['fields']['edit_delete']['id'] = 'edit_delete';
$handler->display->display_options['fields']['edit_delete']['table'] = 'commerce_line_item';
$handler->display->display_options['fields']['edit_delete']['field'] = 'edit_delete';
$handler->display->display_options['fields']['edit_delete']['relationship'] = 'commerce_line_items_line_item_id';
$handler->display->display_options['fields']['edit_delete']['label'] = '';
$handler->display->display_options['fields']['edit_delete']['exclude'] = TRUE;
$handler->display->display_options['fields']['edit_delete']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['edit_delete']['element_label_colon'] = FALSE;
/* Field: Commerce Line Item: Quantity text field */
$handler->display->display_options['fields']['edit_quantity']['id'] = 'edit_quantity';
$handler->display->display_options['fields']['edit_quantity']['table'] = 'commerce_line_item';
$handler->display->display_options['fields']['edit_quantity']['field'] = 'edit_quantity';
$handler->display->display_options['fields']['edit_quantity']['relationship'] = 'commerce_line_items_line_item_id';
$handler->display->display_options['fields']['edit_quantity']['label'] = '';
$handler->display->display_options['fields']['edit_quantity']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['edit_quantity']['alter']['text'] = '[edit_quantity]
[edit_delete] ';
$handler->display->display_options['fields']['edit_quantity']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['edit_quantity']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['edit_quantity']['element_label_colon'] = FALSE;
/* Field: Commerce Line item: Total */
$handler->display->display_options['fields']['commerce_total']['id'] = 'commerce_total';
$handler->display->display_options['fields']['commerce_total']['table'] = 'field_data_commerce_total';
$handler->display->display_options['fields']['commerce_total']['field'] = 'commerce_total';
$handler->display->display_options['fields']['commerce_total']['relationship'] = 'commerce_line_items_line_item_id';
$handler->display->display_options['fields']['commerce_total']['label'] = '';
$handler->display->display_options['fields']['commerce_total']['element_class'] = 'price';
$handler->display->display_options['fields']['commerce_total']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['commerce_total']['click_sort_column'] = 'amount';
$handler->display->display_options['fields']['commerce_total']['settings'] = array(
  'calculation' => FALSE,
);
/* Field: Commerce Order: Order total */
$handler->display->display_options['fields']['commerce_order_total']['id'] = 'commerce_order_total';
$handler->display->display_options['fields']['commerce_order_total']['table'] = 'field_data_commerce_order_total';
$handler->display->display_options['fields']['commerce_order_total']['field'] = 'commerce_order_total';
$handler->display->display_options['fields']['commerce_order_total']['label'] = 'Cart Total';
$handler->display->display_options['fields']['commerce_order_total']['exclude'] = TRUE;
$handler->display->display_options['fields']['commerce_order_total']['element_type'] = '0';
$handler->display->display_options['fields']['commerce_order_total']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['commerce_order_total']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['commerce_order_total']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['commerce_order_total']['click_sort_column'] = 'amount';
$handler->display->display_options['fields']['commerce_order_total']['settings'] = array(
  'calculation' => FALSE,
);
/* Sort criterion: Commerce Line Item: Line item ID */
$handler->display->display_options['sorts']['line_item_id']['id'] = 'line_item_id';
$handler->display->display_options['sorts']['line_item_id']['table'] = 'commerce_line_item';
$handler->display->display_options['sorts']['line_item_id']['field'] = 'line_item_id';
$handler->display->display_options['sorts']['line_item_id']['relationship'] = 'commerce_line_items_line_item_id';
/* Contextual filter: Commerce Order: Order ID */
$handler->display->display_options['arguments']['order_id']['id'] = 'order_id';
$handler->display->display_options['arguments']['order_id']['table'] = 'commerce_order';
$handler->display->display_options['arguments']['order_id']['field'] = 'order_id';
$handler->display->display_options['arguments']['order_id']['default_action'] = 'empty';
$handler->display->display_options['arguments']['order_id']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['order_id']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['order_id']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['order_id']['summary_options']['items_per_page'] = '25';
/* Filter criterion: Commerce Line Item: Line item is of a product line item type */
$handler->display->display_options['filters']['product_line_item_type']['id'] = 'product_line_item_type';
$handler->display->display_options['filters']['product_line_item_type']['table'] = 'commerce_line_item';
$handler->display->display_options['filters']['product_line_item_type']['field'] = 'product_line_item_type';
$handler->display->display_options['filters']['product_line_item_type']['relationship'] = 'commerce_line_items_line_item_id';
$handler->display->display_options['filters']['product_line_item_type']['value'] = '1';
$handler->display->display_options['filters']['product_line_item_type']['group'] = 0;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'views',
  1 => 'commerce_order',
  2 => 'commerce_product_reference',
  3 => 'text',
  4 => 'commerce_price',
);
