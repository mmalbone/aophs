<?php
/**
 * @file
 * page_manager_handlers.page_contact_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_contact_panel_context';
$handler->task = 'page';
$handler->subtask = 'contact';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'flexible';
$display->layout_settings = array(
  'items' => array(
    'canvas' => array(
      'type' => 'row',
      'contains' => 'column',
      'children' => array(
        0 => 'main',
      ),
      'parent' => NULL,
    ),
    'main' => array(
      'type' => 'column',
      'width' => 100,
      'width_type' => '%',
      'children' => array(
        0 => 'main-row',
        1 => 1,
      ),
      'parent' => 'canvas',
    ),
    'main-row' => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'center',
      ),
      'parent' => 'main',
    ),
    'center' => array(
      'type' => 'region',
      'title' => 'Center',
      'width' => 100,
      'width_type' => '%',
      'parent' => 'main-row',
    ),
    1 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'left',
        1 => 'right',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    'left' => array(
      'type' => 'region',
      'title' => 'Left',
      'width' => '70.03236499360531',
      'width_type' => '%',
      'parent' => '1',
      'class' => '',
    ),
    'right' => array(
      'type' => 'region',
      'title' => 'Right',
      'width' => '29.967635006394687',
      'width_type' => '%',
      'parent' => '1',
      'class' => '',
    ),
  ),
);
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'center' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '6ff615d5-6245-464d-b4c5-b1c0e37f6776';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-84b25905-df67-455c-a8ea-b1c792eef4d3';
  $pane->panel = 'center';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => '',
    'body' => '<h1>Contact Us</h1>
<h2>At AOP, we&#39;re listening!&nbsp; Please share your questions and comments with us.</h2>',
    'format' => 'markdown',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'aop-contact-banner',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '84b25905-df67-455c-a8ea-b1c792eef4d3';
  $display->content['new-84b25905-df67-455c-a8ea-b1c792eef4d3'] = $pane;
  $display->panels['center'][0] = 'new-84b25905-df67-455c-a8ea-b1c792eef4d3';
  $pane = new stdClass();
  $pane->pid = 'new-a6367d81-bbdf-4d8e-a267-f2c075bbcac8';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => '',
    'body' => '<p>AOP welcomes your comments! To serve you better, we encourage you to submit questions, feedback, or testimonials on our Christian homeschool curriculum using the forms or contact information listed below.</p>

<p>For general information or customer service inquiries, please send requests to:</p>

<h2>Mailing Address:</h2>

<p>Alpha Omega Publications<br />
Attn: Homeschool Division<br />
804 N. 2nd Avenue East<br />
Rock Rapids, Iowa 51246-1759 U.S.A.</p>

<h2>Phone Numbers:&nbsp;</h2>

<p>Homeschool Orders and Customer Service Inquiries: 800-622-3070<br />
Switched-On Schoolhouse Homeschool Technical Support: 866-444-4498<br />
Monarch Homeschool Technical Support: 888-881-4958</p>

<h2>Fax:</h2>

<p>Fax Orders: 712-472-4856</p>

<h2>Contact Us:</h2>

<p>To contact our customer service department, please complete the online form below. (Please allow 1-3 business days for a response.) If you are looking for SOS Technical Support or simply want a free catalog on our Christian homeschool curriculum, please do not use the form below. Please report any errors in our Christian homeschool curriculum using our product report form. If you have forgotten your account password, please use the Forgot Password form.</p>
',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'a6367d81-bbdf-4d8e-a267-f2c075bbcac8';
  $display->content['new-a6367d81-bbdf-4d8e-a267-f2c075bbcac8'] = $pane;
  $display->panels['left'][0] = 'new-a6367d81-bbdf-4d8e-a267-f2c075bbcac8';
  $pane = new stdClass();
  $pane->pid = 'new-4d441df6-86e9-44d2-933c-42cd3fc819e1';
  $pane->panel = 'left';
  $pane->type = 'node';
  $pane->subtype = 'node';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'nid' => '17249',
    'links' => 0,
    'leave_node_title' => 0,
    'identifier' => '',
    'build_mode' => 'full',
    'link_node_title' => 0,
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'aop-contact-form',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '4d441df6-86e9-44d2-933c-42cd3fc819e1';
  $display->content['new-4d441df6-86e9-44d2-933c-42cd3fc819e1'] = $pane;
  $display->panels['left'][1] = 'new-4d441df6-86e9-44d2-933c-42cd3fc819e1';
  $pane = new stdClass();
  $pane->pid = 'new-131fb9c8-3b33-423c-ae38-8b5a35c7f1e7';
  $pane->panel = 'right';
  $pane->type = 'custom';
  $pane->subtype = 'aop_join_facebook';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => '',
    'body' => '',
    'format' => 'filtered_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'aop-contact-facebook',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '131fb9c8-3b33-423c-ae38-8b5a35c7f1e7';
  $display->content['new-131fb9c8-3b33-423c-ae38-8b5a35c7f1e7'] = $pane;
  $display->panels['right'][0] = 'new-131fb9c8-3b33-423c-ae38-8b5a35c7f1e7';
  $pane = new stdClass();
  $pane->pid = 'new-30e70afa-558a-4008-a81a-d1c7f4f82029';
  $pane->panel = 'right';
  $pane->type = 'block';
  $pane->subtype = 'block-19';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<span>AOP Newsletter</span>',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '30e70afa-558a-4008-a81a-d1c7f4f82029';
  $display->content['new-30e70afa-558a-4008-a81a-d1c7f4f82029'] = $pane;
  $display->panels['right'][1] = 'new-30e70afa-558a-4008-a81a-d1c7f4f82029';
  $pane = new stdClass();
  $pane->pid = 'new-61e0f5f4-ad0b-4b81-98d6-ac4e20178481';
  $pane->panel = 'right';
  $pane->type = 'block';
  $pane->subtype = 'orangebox-enews_signup';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '61e0f5f4-ad0b-4b81-98d6-ac4e20178481';
  $display->content['new-61e0f5f4-ad0b-4b81-98d6-ac4e20178481'] = $pane;
  $display->panels['right'][2] = 'new-61e0f5f4-ad0b-4b81-98d6-ac4e20178481';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-84b25905-df67-455c-a8ea-b1c792eef4d3';
$handler->conf['display'] = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
