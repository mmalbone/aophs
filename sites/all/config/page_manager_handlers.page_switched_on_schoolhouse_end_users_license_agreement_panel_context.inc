<?php
/**
 * @file
 * page_manager_handlers.page_switched_on_schoolhouse_end_users_license_agreement_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_switched_on_schoolhouse_end_users_license_agreement_panel_context';
$handler->task = 'page';
$handler->subtask = 'switched_on_schoolhouse_end_users_license_agreement';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'af5e158e-3316-4fc1-b3b3-d7664cf25772';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-e6750b1f-5ef8-4000-8bb4-b48184fb7ee1';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Switched-On Schoolhouse End Users License Agreement',
    'body' => '<p>1. <a href="/terms-and-conditions">Legal Terms &amp; Conditions</a><br /> 2. <a href="/monarch-eula">Monarch EULA</a><br /> 3. <a href="/sos-eula">SOS EULA</a></p>
<p><strong>Software License Agreement, Disclaimer of Warranty, and Limitation of Remedies</strong></p>
<p>If you use this program or if you allow someone else to use this program (or anything else contained in this package), you are agreeing to all of the terms and conditions of this entire agreement.&nbsp; Make sure that you read and understand all of the terms of this Agreement so that you will know what your obligations and responsibilities are.&nbsp; All parts of this Agreement apply to you, so read each provision carefully.</p>
<p><strong>Software License Agreement</strong></p>
<p>1. Alpha Omega Publications, Inc., (in this agreement, we will call it Alpha Omega) is not selling you this computer software program, or any part of it.&nbsp; When you buy this product, all you are buying is the right to use it on your own computer, in your own home, with your children. You are not buying anything exclusive, and you may transfer the product one time only without our written permission in advance and in accordance with our conditions for such a transfer as set out in this Agreement. Please note that the law normally prevents such transfers, but we want to be as fair as possible so we are allowing you a one time transfer. All terms of this Agreement apply to your purchase and use, so, please make certain that you read and understand it before you install the software on your computer.&nbsp;</p>
<p>In producing and manufacturing this computer software program, we have incorporated software programs that have been developed and are owned by other companies - this is what is known as third party software.&nbsp; You may use this third party software ONLY in conjunction with our program.&nbsp; You may NOT use this third party software, or any of the software contained within this program in any fashion other than with this program.&nbsp; By agreeing to the terms of this Agreement, you understand that all of the information and programming that is used in this software is owned by us or others, and that we are only giving you the right to use this material for yourself and your family, and not for any other purpose.</p>
<p>You understand that Alpha Omega and the third parties which have developed and produced this program are the sole owners of this material, and that they are not selling you anything except the right to use it within your home. The ownership to which we are referring includes all of the copyrights, trademarks, service marks and patents that are involved with this software and packaging.&nbsp; Therefore, you are not allowed nor are given permission to use any of these items except for yourself, without the express written permission of Alpha Omega.</p>
<p>You have paid the fee for your use of the program, as well as for the discs, booklets, box, CDs, and all of the other items of a physical nature included in the package.&nbsp; You will own the items in the package, such as the actual box, CDs, booklets, and so on. Do not mistake the ownership of the physical material with the right to use the computer software on the CDs, etc.&nbsp; These are two different things.&nbsp; Except for the one-time transfer,&nbsp;as described in Section 2.d below, you can not use the software, booklets, or anything else that you have bought for any other purpose but your own, in your own house, for your own children, for their own coursework.&nbsp;</p>
<p>2.&nbsp;&nbsp;&nbsp;&nbsp; Your use of this information is limited to the following, and nothing more:</p>
<p>a.&nbsp;&nbsp; the software can only be used as follows:<br /> i.&nbsp;&nbsp;&nbsp; on a single computer, at one location, at any given time and not to exceed five (5) students; or<br /> ii.&nbsp;&nbsp; if you use this program on a home computer network, you may not exceed (5) students in total at any given time using this program on the network (you may not electronically transfer the software over your network, or to any other computer even if you use it at home);<br /> b.&nbsp;&nbsp; You may NOT copy, reproduce, duplicate, adapt, modify, translate, disassemble, decompile, reverse engineer or reverse assemble anything that you are purchasing here without the specific written consent and approval of Alpha Omega.&nbsp; This includes the software program, booklets, instruction materials, and all other items that are included within the package that you have purchased when you bought this package.&nbsp;</p>
<p>c.&nbsp;&nbsp; You and your family are the only ones who can use this material.&nbsp; With the exception of the one-time transfer,&nbsp;as described in Section 2.d below, you may not give, rent, lease, sell, assign or in any other fashion convey any part of this package to anyone else, at any time, for any reason.&nbsp; If you convey any portion of this package more than one time, the conveyance will not be effective.&nbsp; It will, in fact, be null and void, as you have no rights to do so.&nbsp; You are the only person who has the right to use this program, others must pay Alpha Omega in order to use this material, just as you did.&nbsp; Anything else constitutes the theft of the material from Alpha Omega, and Alpha Omega will pursue those responsible for such theft in order to protect their exclusive ownership of the copyrights, trademarks, service marks and patents.&nbsp; This may include civil (money damages and injunctions against all those people who use it without authorization) as well as criminal prosecution (that could mean going to jail for a violation).&nbsp;&nbsp;&nbsp;</p>
<p>d.&nbsp;&nbsp;&nbsp;&nbsp;One Time&nbsp;Transfer. &nbsp;The initial user of the software product may make a one-time permanent transfer of this EULA and software product only directly to an end user, and only for no monetary or other compensation. This transfer must include all of the software product (including the application disc, all component parts, any back-up copies, the media and printed materials, any upgrades, this EULA, and, if applicable, the serial number). Such transfer may not be by way of consignment or any other indirect transfer. The transferee of such one-time transfer must agree to comply with the terms of this EULA, including the obligation not to further transfer this EULA and software product to any other party. Prior to transferring the software product you must remove all copies of the software product from your machine, including your portable computer.&nbsp; No technical support privileges or warranty of any kind is transferred to the transferee, and this must be conveyed by the initial user to the transferee at the time of the transfer of the software product.</p>
<p>3.&nbsp; As long as you use this software and all of the other materials for yourself, and you don\'t do any of the things that we have discussed above, you can use it for as long as you like.&nbsp; If you violate any of these terms, your right to use this material will cease immediately.&nbsp; Note that we do not have to give you any notice of the termination of your right to use it. The termination is automatic because you have violated the terms of your purchase and license for use.&nbsp; That means that if you continue to use it after violating these terms, you will subject yourself to all of the civil and criminal penalties that are described above.&nbsp;</p>
<p>In addition, if your right to use this material is terminated due to your violation of this Agreement, you must destroy all of the materials that are in your possession. If you have given any of these materials to others, all materials which you&nbsp;given to others&nbsp;you are required to obtain, and upon receipt of same, you are required to immediately destroy the material.&nbsp; You must destroy all software, CD\'s, discs, written material, and, yes, even the box that it all came in.&nbsp; This requirement to destroy all of the materials is absolute even if you paid for it.&nbsp;</p>
<p>Remember that your payment only gives you the right to use this material if you adhere to our terms.&nbsp; If this right to use is terminated, then you have to destroy your package, and we do not have to compensate you for any amounts, which you have previously paid.</p>
<p>4.&nbsp; From time to time, Alpha Omega may develop updates to make this program more efficient, more complete, or easier for you to use.&nbsp; It may also develop additional versions that contain different material.&nbsp; Your purchase of this version does not automatically entitle you to any of this updated material.&nbsp; If Alpha Omega does develop anything new or different, it may become available for you to purchase at an additional price.</p>
<p><strong>DISCLAIMER OF WARRANTY</strong></p>
<p>You are buying this package with your full consent and understanding that you will use it at your own risk and that you will not hold us responsible for any results that occur regardless of the damages that may befall you.&nbsp; Neither we, nor any of our dealers, distributors, agents or employees, creators of the material, those involved in the production of the material or its distribution or the delivery of these materials are taking any responsibility for your use, for the accuracy of the materials, for the results of your use, for the reliability of the material, for how up to date it is, or for any other aspect of this package or its contents.&nbsp; Your full risk extends to and includes all of the instructional material, the software itself, and any and all other aspects of this package (and, yes, even information that is included on the box and all other advertising or promotional material).&nbsp; In addition, you understand and agree that your risk includes the results of defective software which may or may not be your fault.&nbsp;</p>
<p>We will, of course, endeavor to assist you with technical matters and to replace anything that is defective if you tell us immediately after you purchase the software, but we are not required to do so.&nbsp; And this willingness of ours to assist you does not indicate in any way that we are assuming any of the risks of this problem. &nbsp;</p>
<p>There are two forms of a warranty -&nbsp; a legal promise.&nbsp; One form is that in which we expressly tell you what we warrant (i.e. express warranty), and the other form is that which is only implied with your purchase and use of these materials (i.e. implied warranty most notably the implied warrant of merchantability or fitness for a particular purpose).&nbsp; We are not making any express warranties to you. And you are agreeing that you are not going to hold us responsible for anything that may only be implied even if you have told us (or anyone who has been involved with the sale of this product to you) in advance what your use will be, or what your requirements are with respect to this product.&nbsp; You also agree that even if one of the people who help you with the purchase of this product says something, suggests something, recommends something, or writes something for you, that it will NOT affect this disclaimer.&nbsp; You should, therefore, <span style="text-decoration: underline;">NOT RELY ON SUCH INFORMATION</span>.&nbsp; It is only what is written in this Agreement that represents the agreement between you and Alpha Omega.&nbsp; Once again, you assume all of the risk of servicing, repairing or correcting any of this software package regardless of the cause of any problem.&nbsp;</p>
<p>You may live in a state that does not allow the exclusion or limitation of certain warranties. You may also have other rights that vary from state to state.&nbsp; If you are concerned about the particular laws of your state, you should contact someone who is an expert in this area to find out if yours is such a state.&nbsp; We are not giving you any advice on this subject whatsoever.&nbsp;</p>
<p>This disclaimer is very broad and you should understand that it eliminates or limits our liability for all direct and indirect damages, all damages that are the consequence of a defect in the program or materials (such as a loss of profits, the costs of any business interruption, or the loss of any business opportunities), as well as any damages that may be incidental to these defects such as legal fees, costs to correct problems, and so on.&nbsp; Even if you tell Alpha Omega in advance that there is a possibility of such damages, this will not make us liable in the event that you suffer any loss whether due to the use or the inability to use this product.&nbsp;</p>
<p>Unless you live in one of those states that does not allow this broad disclaimer, the maximum that we will be liable for is the amount that you paid for the materials.</p>
<p>If a court or arbitrator shall determine that any provision of this Agreement is not valid or enforceable, all of the other provisions shall continue to be fully valid and enforceable.&nbsp; In addition, if there is a question about the validity or enforceability of any provision, then this provision will be presumed to be revised so that it is enforceable and legal.&nbsp;</p>
<p>Because Alpha Omega is located in Arizona, all of the questions that arise under this Agreement (except those that are governed by Federal and/or international law or treaties like copyright and trademark issues) will be dealt with according to the laws of Arizona.&nbsp; In order to make this Agreement easy for all to understand and to resolve neither party will be required to go to court.&nbsp; The costs and time for all involved are excessive and unnecessary.&nbsp; In order to encourage a speedy and efficient resolution of any disputes between you and us, you agree that any dispute will be resolved by arbitration using the rules of the American Arbitration Association in Phoenix, Arizona.</p>
<p><strong>Acknowledgment</strong></p>
<p>By using this product, you are acknowledging that you have read and agree with all of the terms of this Agreement, understand the limitations on your use of the materials that you have purchased, and agree that you will not hold us responsible for your use of these materials in any fashion.&nbsp; In addition, you agree that whether or not you have received any verbal or written information or statements from anyone else at any time regarding this product or your use of it, THIS IS THE ONLY AGREEMENT BETWEEN YOU AND US THAT APPLIES.&nbsp;</p>
<p>You also acknowledge that the purchase price for this product would be much higher if we had to cover all of the potential problems that one may encounter with respect to the use of these materials.&nbsp; We, of course, always do our best to provide to you with the finest quality product available.&nbsp; The only way that we could sell the software for its current price, however, is that we are assuming no responsibilities for your use, or the quality of the materials that you have purchased.&nbsp;</p>
<p><strong>Export Prohibitions</strong></p>
<p>You may not export the enclosed software program or any portion thereof into any country prohibited by the United States Export Administration Act and the regulations thereunder. Portions of the enclosed software program may include Restricted Computer Software. Use, duplication, or disclosure by the U.S. Government is subject to restrictions as set forth in this Agreement and as provided in DFARS 227.7202-1(a) and 227.7202-3(a) (1995), DFARS 252.227-7013 (OCT 1988), FAR 52.227-19, or FAR 52.227-114, as applicable. You acknowledge that neither the enclosed software program nor any portion thereof nor the underlying information or technology may be downloaded or otherwise exported or re-exported: (i) into (or to a national or resident of) any other country to which the U.S. has embargoed goods; or (ii) to anyone on the U.S. Treasury Department\'s list of Specially Designated Nationals or the U.S. Commerce Department\'s Table of Denial Orders. You hereby represent and warrant that you are not located in or the resident of any such country or on any such list.</p>
<p><strong>Notices Regarding Third-Party Rights</strong></p>
<p align="center">New American Standard Bible Updates<br /> 1960, 1962, 1963, 1968, 1971, 1972,<br /> 1973, 1975, 1977, 1995 by The Lockman Foundation<br /> All rights reserved<br /> <span style="text-decoration: underline;">http://www.lockman.org</span> <br /> The NASB, NAS, New American Standard Bible and New American Standard trademarks are registered in the United States Patent and Trademark Office by the Lockman Foundation.&nbsp; Use of these trademarks requires the permission of the Lockman Foundation.</p>
<p align="center">Permission to Quote:<br /> The text of the New American Standard Bible may be quoted and/or reprinted up to and inclusive of five hundred (500) verses without express written permission of The Lockman Foundation, providing the verses do not amount to a complete book of the Bible nor do the verses quoted account for more than 25% of the total work in which they are quoted.<br /> Notice of copyright must appear on the title or copyright page of the work as follows:<br /> Scripture taken from the New American Standard Bible, <br /> The Lockman Foundation 1960, 1962, 1963, 1968, 1971, <br /> 1972, 1973, 1975, 1977, 1995<br /> Used by permission.&nbsp; <span style="text-decoration: underline;">http://www.Lockman.org</span><br /> When quotations from the NASB text are used in not-for-sale media, such as church bulletins, orders of service, posters, transparencies or similar media, the abbreviation (NASB) may be used at the end of the quotation.&nbsp; This permission to quote is limited to material which is wholly manufactured in compliance with the provisions of the copyright laws of the United States of America and all applicable international conventions and treaties.&nbsp; Quotations and/or reprints in excess of the above limitations, or other permission requests, must be directed to and approved in writing by The Lockman Foundation, PO Box 2279, La Habra, CA 90632-2279, (714) 879-3055.&nbsp; http//www.lockman.org.</p>
<p align="center">You are expressly prohibited from copying or otherwise duplicating the NASB databases, and from altering or otherwise modifying the NASB databases.</p>
<p align="center">Macromedia Trademark/ Copyright Notice: <br /> Switched-On Schoolhouse contains Adobe Shockwave Player and Adobe Flash Player software by Adobe Systems Incorporated, Copyright 1995-2007 Adobe Systems Incorporated. All rights reserved. Protected by U.S. Patent 6,879,327; Patents Pending in the United States and other countries. Adobe, Flash, Macromedia, and Shockwave are trademarks of Adobe Systems Incorporated.</p>
<p align="center">Portions Copyright ComponentOne, LLC 1991-2006. All Rights Reserved.</p>
<p>Should you have any questions concerning the above or this product, or if you desire to contact Alpha Omega for any reason, please contact, in writing:<br /> Alpha Omega Publications,&nbsp;Legal Department, 804 North 2nd Ave. East, Rock Rapids, IA 51246.</p>
<p>Alpha Omega Publications, Switched-On Schoolhouse and Switched-On&nbsp;and their logos&nbsp;are registered trademarks of Alpha Omega Publications, Inc.&nbsp; All other trademarks that appear in this product are the property of their respective owners. All rights reserved.</p>
<p align="center">2003, 2005, 2007 Alpha Omega Publications, Inc. All rights reserved.</p>
<p><a href="#">Top of Page</a></p></div>                </div>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'e6750b1f-5ef8-4000-8bb4-b48184fb7ee1';
  $display->content['new-e6750b1f-5ef8-4000-8bb4-b48184fb7ee1'] = $pane;
  $display->panels['middle'][0] = 'new-e6750b1f-5ef8-4000-8bb4-b48184fb7ee1';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-e6750b1f-5ef8-4000-8bb4-b48184fb7ee1';
$handler->conf['display'] = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
