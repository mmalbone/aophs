<?php
/**
 * @file
 * page_manager_pages.new_for_2014.inc
 */

$api = '2.0.0';

$data = $page = new stdClass();
$page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
$page->api_version = 1;
$page->name = 'new_for_2014';
$page->task = 'page';
$page->admin_title = 'New for 2014';
$page->admin_description = '';
$page->path = 'new-for-2014';
$page->access = array();
$page->menu = array();
$page->arguments = array();
$page->conf = array(
  'admin_paths' => FALSE,
);


$dependencies = array(
  'page_manager_handlers.page_new_for_2014_panel_context' => 'page_manager_handlers.page_new_for_2014_panel_context',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
