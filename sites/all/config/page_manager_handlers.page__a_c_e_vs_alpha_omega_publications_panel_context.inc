<?php
/**
 * @file
 * page_manager_handlers.page__a_c_e_vs_alpha_omega_publications_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page__a_c_e_vs_alpha_omega_publications_panel_context';
$handler->task = 'page';
$handler->subtask = '_a_c_e_vs_alpha_omega_publications';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'be10b1fc-3e2c-4382-b9d1-4da7ef27b644';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-d22692b5-f4e7-40b9-8259-fdc975f45319';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'A.C.E. vs. Alpha Omega Publications',
    'body' => '<p>Thinking about homeschooling with the homeschool programs offered by Accelerated Christian Education? Wondering what the differences are between A.C.E. and Alpha Omega Publications? You\'re in the right place. Many parents just like you are also searching for a homeschool curriculum and teaching approach that best fits their children\'s academic needs. With so many homeschooling options, resources, and curricula to choose from, selecting homeschool materials for your child can often be a daunting task, but we\'re here to help!</p><p><a href="#comparison_chart">View comparison charts for Accelerated Christian Education vs. Alpha Omega Publications curricula and services!</a></p><h3><strong>A.C.E.\'s Approach</strong></h3><p>Accelerated Christian Education, an in-house curriculum publisher, provides K-12th grade Christian curriculum and educational materials to Christian schools and homeschoolers. The A.C.E. PACEs option is a student-driven, workbook approach which provides an independent and individualized learning experience for homeschooled or Christian school students. Parents are responsible for all administrative tasks, including any required lesson-planning, grading, record-keeping, and reporting. Accelerated Christian Education PACEs answer keys provide the answers and solutions to all student activities included in each PACE booklet.</p><p>Written from a Christian worldview, all A.C.E. PACEs are completely Bible-based. All science instruction is based on a Biblical view of creation and the origin of life. Accelerated Christian Education courses include Bible, reading, social studies, mathematics, science, health, English, literature, fine arts, and foreign language. Lighthouse Christian Academy is a fully accredited (MSA-CESS and Ai) program designed to assist parents in providing a quality Christian education to homeschooled students. Lighthouse Christian Academy exclusively utilizes the A.C.E. curriculum for students in grades K-12 and provides coursework in all core subjects as well as electives.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>Most parents discover that their children\'s educational needs are best met by blending several different homeschool curriculums and teaching approaches. Because there is no one perfect homeschool curriculum, a blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Instead of feeling torn between curriculums, parents should be open to mix and match materials and resources to customize their child\'s education. Remember, each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3><strong>Curriculum from Alpha Omega Publications</strong></h3><p>Alpha Omega Publications is a Christian-based, PreK-12 publisher that provides engaging, interactive curriculum and educational services to homeschool families. Offering proven, easy-to-teach homeschool curriculum, AOP offers four main curriculum offerings: Internet-based Monarch (3-12), computer-based Switched-On Schoolhouse (3-12), worktext-based LIFEPAC (K-12), and workbook-based Horizons (PreK-12). Main core subjects offered include Bible, history and geography, language arts, math, and science, along with various electives. AOP also offers Alpha Omega Academy, a distance learning academy for grades K-12. Understanding that each child learns differently, AOP offers diverse curriculum and services in different formats to fit multiple learning styles, ensuring you can find what fits your child needs.</p><h3><strong>Benefits and Features of Alpha Omega Publications\' Curriculum</strong></h3><p class="list_heading">Monarch</p><ul><li>online interactive lessons with anytime, anywhere access</li><li>compatible with both Windows® and Mac® operating systems</li><li>offers customizable student learning options and time-saving teaching tools</li><li>features dynamic, media-rich lessons; real-time content updates; and data storage</li></ul><p class="list_heading">Switched-On Schoolhouse</p><ul><li>interactive, innovative computer-based curriculum</li><li>offers flexible, engaging learning environment</li><li>contains multimedia tools, educational games, and automatic grading options</li><li>can reduce teacher planning and grading by 20 hours per week</li></ul>
<p id="comparison_chart">LIFEPAC</p><ul><li>full-color, print-based curriculum</li><li>offers progressive, self-paced individual unit worktexts</li><li>promotes concept mastery and cognitive reasoning skills</li><li>encourages academic independence in your children</li></ul><p>Horizons</p><ul><li>brightly illustrated, hands-on, consumable curriculum</li><li>offers fun lessons and manipulative-based activities</li><li>teaches concepts through a spiral learning process</li><li>encourages concept mastery through quick concept introduction, review, and reinforcement</li></ul>

<h3>Take a Closer Look at A.C.E. and Alpha Omega Publications</h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">Alpha Omega Publications</th><th class="compare">A.C.E.</th></tr>
</thead>
<tbody>
<tr>
<td>PreK-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Flexible and individualized curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Customizable curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Student-paced curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Placement tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Interactive lesson material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Automatic curriculum updates available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Automatic grading and lesson planning available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Appeals to students with multiple learning styles</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr><th class="features">Cost</th><th class="compare">Alpha Omega Publications</th><th class="compare">A.C.E.</th></tr>
</thead>
<tbody>
<tr class="alt">
<td>Average yearly cost per grade level**</td>
<td class="tac">$449.95 for Monarch</td>
<td class="tac">$411</td>
</tr>
<tr>
<td>Average subject cost per grade level**</td>
<td class="tac">$99.95 for Monarch</td>
<td class="tac">$64.80</td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: A.C.E. versus Alpha Omega Publications</h4>
<p class="list_heading_links">A.C.E. PACE</p>
<ul>
<li><a href="/ace-pace-vs-monarch">A.C.E. PACE vs. Monarch</a></li>
<li><a href="/ace-pace-vs-horizons">A.C.E. PACE vs. Horizons</a></li>
<li><a href="/ace-pace-vs-lifepac">A.C.E. PACE vs. LIFEPAC</a></li>
<li><a href="/ace-pace-vs-aoa">A.C.E. PACE vs. Alpha Omega Academy</a></li>
<li><a href="/ace-pace-vs-sos">A.C.E. PACE vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">A.C.E. Lighthouse Christian Academy</p>
<ul>
<li><a href="/ace-lca-vs-monarch">A.C.E. Lighthouse Christian Academy vs. Monarch</a></li>
<li><a href="/ace-lca-vs-horizons">A.C.E. Lighthouse Christian Academy vs. Horizons</a></li>
<li><a href="/ace-lca-vs-lifepac">A.C.E. Lighthouse Christian Academy vs. LIFEPAC</a></li>
<li><a href="/ace-lca-vs-aoa">A.C.E. Lighthouse Christian Academy vs. Alpha Omega Academy</a></li>
<li><a href="/ace-lca-vs-sos">A.C.E. Lighthouse Christian Academy vs. Switched-On Schoolhouse</a></li>
</ul>
<p>*A.C.E.® and PACE® are the registered trademarks and subsidiaries of A.C.E.®. Alpha Omega Publications is not in any way affiliated with A.C.E.. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by A.C.E.®.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p><p>**Prices obtained February 2013 from Accelerated Christian Education Homeschool and A.C.E. Ministries websites and from Alpha Omega Publications 2013 Homeschool Catalog. Yearly prices based on comparison between Monarch 3rd Grade 5-Subject Set and ACE 3rd PACES pricing for a full year. Individual subject prices are based on comparison between Monarch 3rd Grade Math (including all student and teacher material, along with supplements) and ACE prices for individual 3rd Grade Math PACES kit. Costs do not reflect any additional fees or shipping and handling charges. Prices for other Accelerated Christian Education and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'd22692b5-f4e7-40b9-8259-fdc975f45319';
  $display->content['new-d22692b5-f4e7-40b9-8259-fdc975f45319'] = $pane;
  $display->panels['left'][0] = 'new-d22692b5-f4e7-40b9-8259-fdc975f45319';
  $pane = new stdClass();
  $pane->pid = 'new-fa6627a8-95dd-4c2a-a2fe-add5906918a0';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'fa6627a8-95dd-4c2a-a2fe-add5906918a0';
  $display->content['new-fa6627a8-95dd-4c2a-a2fe-add5906918a0'] = $pane;
  $display->panels['right'][0] = 'new-fa6627a8-95dd-4c2a-a2fe-add5906918a0';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-d22692b5-f4e7-40b9-8259-fdc975f45319';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
