<?php
/**
 * @file
 * permission.delete_own_dvd_content.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'delete own dvd content',
  'roles' => array(
    0 => 'administrator',
    1 => 'marketing',
  ),
);

$dependencies = array(
  'content_type.dvd' => 'content_type.dvd',
);

$optional = array();

$modules = array(
  0 => 'node',
);
