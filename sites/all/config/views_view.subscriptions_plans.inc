<?php
/**
 * @file
 * views_view.subscriptions_plans.inc
 */

$api = '2.0.0';

$data = $view = new view();
$view->name = 'subscriptions_plans';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Subscriptions Plans';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* Relationship: Content: Referenced products */
$handler->display->display_options['relationships']['field_product_product_id']['id'] = 'field_product_product_id';
$handler->display->display_options['relationships']['field_product_product_id']['table'] = 'field_data_field_product';
$handler->display->display_options['relationships']['field_product_product_id']['field'] = 'field_product_product_id';
/* Field: Commerce Product: Product Image */
$handler->display->display_options['fields']['field_product_image']['id'] = 'field_product_image';
$handler->display->display_options['fields']['field_product_image']['table'] = 'field_data_field_product_image';
$handler->display->display_options['fields']['field_product_image']['field'] = 'field_product_image';
$handler->display->display_options['fields']['field_product_image']['relationship'] = 'field_product_product_id';
$handler->display->display_options['fields']['field_product_image']['label'] = '';
$handler->display->display_options['fields']['field_product_image']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_product_image']['alter']['text'] = '<img src="https://s3.amazonaws.com/glnnewmedia/media/catalog/product[field_product_image]" alt="">';
$handler->display->display_options['fields']['field_product_image']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['field_product_image']['alter']['path'] = '[title]';
$handler->display->display_options['fields']['field_product_image']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_product_image']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_product_image']['settings'] = array(
  'title_style' => '_none',
  'title_link' => '',
  'title_class' => '',
);
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Commerce Product: Price */
$handler->display->display_options['fields']['commerce_price']['id'] = 'commerce_price';
$handler->display->display_options['fields']['commerce_price']['table'] = 'field_data_commerce_price';
$handler->display->display_options['fields']['commerce_price']['field'] = 'commerce_price';
$handler->display->display_options['fields']['commerce_price']['relationship'] = 'field_product_product_id';
$handler->display->display_options['fields']['commerce_price']['label'] = '';
$handler->display->display_options['fields']['commerce_price']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['commerce_price']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['commerce_price']['click_sort_column'] = 'amount';
$handler->display->display_options['fields']['commerce_price']['settings'] = array(
  'calculation' => '0',
);
$handler->display->display_options['fields']['commerce_price']['field_api_classes'] = TRUE;
/* Sort criterion: Content: Title */
$handler->display->display_options['sorts']['title_1']['id'] = 'title_1';
$handler->display->display_options['sorts']['title_1']['table'] = 'node';
$handler->display->display_options['sorts']['title_1']['field'] = 'title';
$handler->display->display_options['sorts']['title_1']['order'] = 'DESC';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Title */
$handler->display->display_options['filters']['title']['id'] = 'title';
$handler->display->display_options['filters']['title']['table'] = 'node';
$handler->display->display_options['filters']['title']['field'] = 'title';
$handler->display->display_options['filters']['title']['operator'] = 'contains';
$handler->display->display_options['filters']['title']['value'] = 'Monarch';
/* Filter criterion: Content: Title */
$handler->display->display_options['filters']['title_1']['id'] = 'title_1';
$handler->display->display_options['filters']['title_1']['table'] = 'node';
$handler->display->display_options['filters']['title_1']['field'] = 'title';
$handler->display->display_options['filters']['title_1']['operator'] = 'contains';
$handler->display->display_options['filters']['title_1']['value'] = 'Subscription';
/* Filter criterion: Commerce Product: Product ID */
$handler->display->display_options['filters']['product_id']['id'] = 'product_id';
$handler->display->display_options['filters']['product_id']['table'] = 'commerce_product';
$handler->display->display_options['filters']['product_id']['field'] = 'product_id';
$handler->display->display_options['filters']['product_id']['relationship'] = 'field_product_product_id';
$handler->display->display_options['filters']['product_id']['operator'] = 'not empty';

/* Display: Content pane */
$handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: Content: Search Boost (field_search_boost) */
$handler->display->display_options['sorts']['field_search_boost_value']['id'] = 'field_search_boost_value';
$handler->display->display_options['sorts']['field_search_boost_value']['table'] = 'field_data_field_search_boost';
$handler->display->display_options['sorts']['field_search_boost_value']['field'] = 'field_search_boost_value';
$handler->display->display_options['sorts']['field_search_boost_value']['order'] = 'DESC';


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'views',
  1 => 'node',
  2 => 'text',
  3 => 'commerce_price',
  4 => 'views_content',
);
