<?php
/**
 * @file
 * permission.delete_own_curated_product_list_content.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'delete own curated_product_list content',
  'roles' => array(),
);

$dependencies = array(
  'content_type.curated_product_list' => 'content_type.curated_product_list',
);

$optional = array();

$modules = array(
  0 => 'node',
);
