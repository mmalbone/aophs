<?php
/**
 * @file
 * page_manager_handlers.page_schedule_a_call_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_schedule_a_call_panel_context';
$handler->task = 'page';
$handler->subtask = 'schedule_a_call';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '1971c542-92d6-49eb-bfce-a39cabdc2a21';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-58e42821-9169-4e81-b722-600f97155524';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Schedule a Call Back',
    'body' => '<p>Need help or additional information?&nbsp;&nbsp; Fill out the information below and one of our customer service representatives will give you a call.</p>
',
    'format' => 'filtered_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '58e42821-9169-4e81-b722-600f97155524';
  $display->content['new-58e42821-9169-4e81-b722-600f97155524'] = $pane;
  $display->panels['middle'][0] = 'new-58e42821-9169-4e81-b722-600f97155524';
  $pane = new stdClass();
  $pane->pid = 'new-62a57647-4658-4ef3-917b-8710a64795b0';
  $pane->panel = 'middle';
  $pane->type = 'block';
  $pane->subtype = 'aop_call_scheduling-aop_call_scheduling_form';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '62a57647-4658-4ef3-917b-8710a64795b0';
  $display->content['new-62a57647-4658-4ef3-917b-8710a64795b0'] = $pane;
  $display->panels['middle'][1] = 'new-62a57647-4658-4ef3-917b-8710a64795b0';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-62a57647-4658-4ef3-917b-8710a64795b0';
$handler->conf['display'] = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
