<?php
/**
 * @file
 * page_manager_handlers.page_bob_jones_university_press_dvd_vs_lifepac_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_bob_jones_university_press_dvd_vs_lifepac_panel_context';
$handler->task = 'page';
$handler->subtask = 'bob_jones_university_press_dvd_vs_lifepac';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '4ba8c1eb-767e-45c5-b6f5-01326ce5846a';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-3a77e9e6-81ba-4472-8fd5-49fb60c3b4de';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Bob Jones University Press DVD vs. LIFEPAC',
    'body' => '<p>Trying to learn about the differences between the Bob Jones University Press DVD Program and LIFEPAC curriculum from Alpha Omega Publications? You\'re in the right place! Find short overviews of each curriculum below, an easy-to-view comparison chart, and additional comparison options!</p><h3>Bob Jones University Press DVD Program</h3><p>The Bob Jones University Press DVD Program is a distance learning program for grades K4-12. Instruction is presented on DVD via recorded sessions with experienced teachers. All Bob Jones University Press (BJU Press) curriculum is Bible-based and presents instruction from a Christian worldview. BJU Press curriculum offers coursework in Bible, history, mathematics, science, and language (including instruction in reading, grammar, spelling, and more). Electives are also offered. The BJU Press DVD Program combines recorded instruction with textbooks and teacher materials published by Bob Jones University Press. When used in a homeschool setting, students watch lesson presentations and then complete student work in the included textbooks and workbooks.</p><p>Delivery of Bob Jones University Press coursework via DVD offers homeschool families a convenient and portable approach to distance learning. BJU Press DVD kits are leased for up to13 months to homeschool families and include all DVDs, textbooks, and essential course materials for student and parent. DVD courses are available as complete grade level packages for K4-Grade 12, and as individual courses for grades 1-12. There is no per-child viewing fee, allowing families to use the program for siblings studying at the same grade level. The BJU Press DVD program allows families to customize packages as needed for students who need different grade levels for different subjects. Parents are responsible for all administrative tasks including supervision of student work, calendar management, grading, record-keeping, and reporting.</p><h3>Finding Your Approach to Homeschooling</h3><p>Wondering whether you should use a print-based or DVD-based approach to homeschooling? Keep in mind that there is no one perfect homeschool curriculum. A blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Instead of feeling torn between curriculum formats, parents should be open to mixing and matching materials and resources to customize their child\'s education. Remember, each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3>LIFEPAC from Alpha Omega Publications</h3><p>LIFEPAC is a K-12 print-based curriculum offered by Alpha Omega Publications, a Christian-based publisher that provides curriculum and educational services to Christian schools and homeschool families. Full-color and consumable, LIFEPAC is an integrated, Bible-based curriculum developed by a team of accomplished educators and teachers. Using a mastery-based learning approach, each LIFEPAC course is comprised of ten student-directed worktexts that provide a full year of instruction. Individual worktexts may also be used to supplement the homeschool curriculum or to fill in learning gaps. LIFEPAC worktexts include self tests and unit tests to provide assessment of student progress. Teacher\'s guides offer practical teaching tips, additional learning activities, and complete answer keys.</p><p>An economical, proven homeschool curriculum, LIFEPAC integrates Scripture and a Christian worldview throughout all subjects. LIFEPAC offers the flexibility of an individualized, student-driven learning approach which encourages critical thinking and academic independence. Self-paced coursework for K-12 students includes Bible (1-12), language arts (K-12), math (K-12), science (K-12), and history and geography (1-12). Electives are also available for elementary, middle, and high school students. LIFEPAC subject kits include all required student and teacher materials. LIFEPAC curriculum can be purchased as complete grade level sets or as individual subjects. Student and teacher materials and supplemental materials may be purchased separately.</p><h3>Benefits and Features of LIFEPAC from Alpha Omega Publications</h3><p class="list_heading">LIFEPAC</p><ul><li>full-color, print-based curriculum</li><li>offers progressive, self-paced individual unit worktexts</li><li>promotes concept mastery and cognitive reasoning skills</li><li>encourages academic independence in your children</li></ul>
<h3>Take a Closer Look at Bob Jones University Press DVD and LIFEPAC</h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">LIFEPAC</th><th class="compare">BJU Press DVD (with textbooks)</th></tr>
</thead>
<tbody>
<tr>
<td>K-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Student-directed curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Instruction provided through text-based lessons</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Instruction provided through recorded lesson sessions</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Promotes concept mastery and cognitive reasoning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Self-paced and individualized</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Manageable amount of daily lesson material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Separate unit worktexts available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Requires material to be returned</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Separate unit worktexts available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr><th class="features">Cost</th><th class="compare">Alpha Omega Publications</th><th class="compare">Bob Jones University Press</th></tr>
</thead>
<tbody>
<tr>
<td>Average yearly cost per grade level**</td>
<td class="tac">$286.95</td>
<td class="tac">$1049</td>
</tr>
<tr class="alt">
<td>Average subject cost per grade level**</td>
<td class="tac">$58.95</td>
<td class="tac">$339-389</td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: Bob Jones University Press versus Alpha Omega Publications</h4>
<p class="list_heading_links">Bob Jones University Press Textbooks</p>
<ul>
<li><a href="/bju-textbook-vs-monarch">Bob Jones University Press Textbooks vs. Monarch</a></li>
<li><a href="/bju-textbook-vs-horizons">Bob Jones University Press Textbooks vs. Horizons</a></li>
<li><a href="/bju-textbook-vs-lifepac">Bob Jones University Press Textbooks vs. LIFEPAC</a></li>
<li><a href="/bju-textbook-vs-aoa">Bob Jones University Press Textbooks vs. Alpha Omega Academy</a></li>
<li><a href="/bju-textbook-vs-sos">Bob Jones University Press Textbooks vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">Bob Jones University Press (DVD)</p>
<ul>
<li><a href="/bju-dvd-vs-monarch">Bob Jones University Press (DVD) vs. Monarch</a></li>
<li><a href="/bju-dvd-vs-horizons">Bob Jones University Press (DVD) vs. Horizons</a></li>
<li><a href="/bju-dvd-vs-lifepac">Bob Jones University Press (DVD) vs. LIFEPAC</a></li>
<li><a href="/bju-dvd-vs-aoa">Bob Jones University Press (DVD) vs. Alpha Omega Academy</a></li>
<li><a href="/bju-dvd-vs-sos">Bob Jones University Press (DVD) vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">Bob Jones University Press Academy (Hard Drive)</p>
<ul>
<li><a href="/bju-hard-drive-vs-monarch">Bob Jones University Press Academy (Hard Drive) vs. Monarch</a></li>
<li><a href="/bju-hard-drive-vs-horizons">Bob Jones University Press Academy (Hard Drive) vs. Horizons</a></li>
<li><a href="/bju-hard-drive-vs-lifepac">Bob Jones University Press Academy (Hard Drive) vs. LIFEPAC</a></li>
<li><a href="/bju-hard-drive-vs-aoa">Bob Jones University Press Academy (Hard Drive) vs. Alpha Omega Academy</a></li>
<li><a href="/bju-hard-drive-vs-sos">Bob Jones University Press Academy (Hard Drive) vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">Bob Jones University Press Academy (Distance Learning Online)</p>
<ul>
<li><a href="/bju-online-vs-monarch">Bob Jones University Press Academy (Distance Learning Online) vs. Monarch</a></li>
<li><a href="/bju-online-vs-horizons">Bob Jones University Press Academy (Distance Learning Online) vs. Horizons</a></li>
<li><a href="/bju-online-vs-lifepac">Bob Jones University Press Academy (Distance Learning Online) vs. LIFEPAC</a></li>
<li><a href="/bju-online-vs-aoa">Bob Jones University Press Academy (Distance Learning Online) vs. Alpha Omega Academy</a></li>
<li><a href="/bju-online-vs-sos">Bob Jones University Press Academy (Distance Learning Online) vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">Bob Jones University Press Academy (Academy of Home Education)</p>
<ul>
<li><a href="/bju-academy-home-education-vs-monarch">Bob Jones University Press Academy (Academy of Home Education) vs. Monarch</a></li>
<li><a href="/bju-academy-home-education-vs-horizons">Bob Jones University Press Academy (Academy of Home Education) vs. Horizons</a></li>
<li><a href="/bju-academy-home-education-vs-lifepac">Bob Jones University Press Academy (Academy of Home Education) vs. LIFEPAC</a></li>
<li><a href="/bju-academy-home-education-vs-aoa">Bob Jones University Press Academy (Academy of Home Education) vs. Alpha Omega Academy</a></li>
<li><a href="/bju-academy-home-education-vs-sos">Bob Jones University Press Academy (Academy of Home Education) vs. Switched-On Schoolhouse</a></li>
</ul>
<p>*BJU Press® is the registered trademark and subsidiary of Bob Jones University®. Alpha Omega Publications is not in any way affiliated with Bob Jones University Press or its trademark owner Bob Jones University. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by BJU Press.</p><p><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p><p>**Prices obtained February 2013 from BJU Press Total Homeschool Solutions and BJUP websites and from Alpha Omega Publications 2013 Homeschool Catalog. Yearly prices based on comparison between LIFEPAC 3rd Grade 5-Subject Set and Bob Jones University Press DVD program pricing for full year of curriculum (textbooks included). Subject prices based on comparison between LIFEPAC 3rd Grade Math (including all student and teacher material, along with supplements) and Bob Jones University Press DVD individual course fee for a full year. Costs do not reflect any additional fees or shipping and handling charges. Prices for other Bob Jones University Press and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '3a77e9e6-81ba-4472-8fd5-49fb60c3b4de';
  $display->content['new-3a77e9e6-81ba-4472-8fd5-49fb60c3b4de'] = $pane;
  $display->panels['left'][0] = 'new-3a77e9e6-81ba-4472-8fd5-49fb60c3b4de';
  $pane = new stdClass();
  $pane->pid = 'new-37e54895-3c58-450d-bc0d-019ceddcde60';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '37e54895-3c58-450d-bc0d-019ceddcde60';
  $display->content['new-37e54895-3c58-450d-bc0d-019ceddcde60'] = $pane;
  $display->panels['right'][0] = 'new-37e54895-3c58-450d-bc0d-019ceddcde60';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-3a77e9e6-81ba-4472-8fd5-49fb60c3b4de';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
