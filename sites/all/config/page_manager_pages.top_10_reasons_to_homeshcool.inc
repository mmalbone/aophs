<?php
/**
 * @file
 * page_manager_pages.top_10_reasons_to_homeshcool.inc
 */

$api = '2.0.0';

$data = $page = new stdClass();
$page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
$page->api_version = 1;
$page->name = 'top_10_reasons_to_homeshcool';
$page->task = 'page';
$page->admin_title = 'Top 10 Reasons to Homeshcool';
$page->admin_description = '';
$page->path = 'top-10-reasons-to-homeschool';
$page->access = array();
$page->menu = array();
$page->arguments = array();
$page->conf = array(
  'admin_paths' => FALSE,
);


$dependencies = array(
  'page_manager_handlers.page_top_10_reasons_to_homeshcool_panel_context' => 'page_manager_handlers.page_top_10_reasons_to_homeshcool_panel_context',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
