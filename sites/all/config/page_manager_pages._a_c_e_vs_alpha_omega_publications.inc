<?php
/**
 * @file
 * page_manager_pages._a_c_e_vs_alpha_omega_publications.inc
 */

$api = '2.0.0';

$data = $page = new stdClass();
$page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
$page->api_version = 1;
$page->name = '_a_c_e_vs_alpha_omega_publications';
$page->task = 'page';
$page->admin_title = ' A.C.E. vs. Alpha Omega Publications';
$page->admin_description = '';
$page->path = 'ace';
$page->access = array();
$page->menu = array();
$page->arguments = array();
$page->conf = array(
  'admin_paths' => FALSE,
);


$dependencies = array(
  'page_manager_handlers.page__a_c_e_vs_alpha_omega_publications_panel_context' => 'page_manager_handlers.page__a_c_e_vs_alpha_omega_publications_panel_context',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
