<?php
/**
 * @file
 * views_view.display_products.inc
 */

$api = '2.0.0';

$data = $view = new view();
$view->name = 'display_products';
$view->description = 'Display all the products that are available, using Search API';
$view->tag = 'default';
$view->base_table = 'search_api_index_product_display';
$view->human_name = 'Display Products';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_more_always'] = TRUE;
$handler->display->display_options['use_more_text'] = 'See Details';
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '24';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['expose']['items_per_page_options'] = '24, 100';
$handler->display->display_options['style_plugin'] = 'list';
$handler->display->display_options['style_options']['class'] = 'all-products';
$handler->display->display_options['row_plugin'] = 'entity';
$handler->display->display_options['row_options']['view_mode'] = 'product_list';
/* Header: Global: View area */
$handler->display->display_options['header']['view']['id'] = 'view';
$handler->display->display_options['header']['view']['table'] = 'views';
$handler->display->display_options['header']['view']['field'] = 'view';
$handler->display->display_options['header']['view']['view_to_insert'] = 'curriculum_product_listing_header:default';
/* No results behavior: Global: Unfiltered text */
$handler->display->display_options['empty']['area_text_custom']['id'] = 'area_text_custom';
$handler->display->display_options['empty']['area_text_custom']['table'] = 'views';
$handler->display->display_options['empty']['area_text_custom']['field'] = 'area_text_custom';
$handler->display->display_options['empty']['area_text_custom']['empty'] = TRUE;
$handler->display->display_options['empty']['area_text_custom']['content'] = '<p> </p>
<h2>No results found.</h2>
<p>Sorry we couldn\'t find any products that match that search criteria. Use our online curriculum tool to find the homeschooling curriculum that fits your needs.</p>
<a href="/find-a-curriculum">Find a Curriculum</a>';
/* Sort criterion: Indexed Content: Search Boost */
$handler->display->display_options['sorts']['field_search_boost']['id'] = 'field_search_boost';
$handler->display->display_options['sorts']['field_search_boost']['table'] = 'search_api_index_product_display';
$handler->display->display_options['sorts']['field_search_boost']['field'] = 'field_search_boost';
$handler->display->display_options['sorts']['field_search_boost']['order'] = 'DESC';
/* Filter criterion: Search: Fulltext search */
$handler->display->display_options['filters']['search_api_views_fulltext']['id'] = 'search_api_views_fulltext';
$handler->display->display_options['filters']['search_api_views_fulltext']['table'] = 'search_api_index_product_display';
$handler->display->display_options['filters']['search_api_views_fulltext']['field'] = 'search_api_views_fulltext';
$handler->display->display_options['filters']['search_api_views_fulltext']['exposed'] = TRUE;
$handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator_id'] = 'search_api_views_fulltext_op';
$handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator'] = 'search_api_views_fulltext_op';
$handler->display->display_options['filters']['search_api_views_fulltext']['expose']['identifier'] = 'search_api_views_fulltext';

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
$handler->display->display_options['exposed_block'] = TRUE;
$handler->display->display_options['defaults']['header'] = FALSE;
/* Header: Global: View area */
$handler->display->display_options['header']['view']['id'] = 'view';
$handler->display->display_options['header']['view']['table'] = 'views';
$handler->display->display_options['header']['view']['field'] = 'view';
$handler->display->display_options['header']['view']['view_to_insert'] = 'curriculum_product_listing_header:default';
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: Indexed Content: Search Boost */
$handler->display->display_options['sorts']['field_search_boost']['id'] = 'field_search_boost';
$handler->display->display_options['sorts']['field_search_boost']['table'] = 'search_api_index_product_display';
$handler->display->display_options['sorts']['field_search_boost']['field'] = 'field_search_boost';
$handler->display->display_options['sorts']['field_search_boost']['order'] = 'DESC';
/* Sort criterion: Indexed Content: Title */
$handler->display->display_options['sorts']['title_field']['id'] = 'title_field';
$handler->display->display_options['sorts']['title_field']['table'] = 'search_api_index_product_display';
$handler->display->display_options['sorts']['title_field']['field'] = 'title_field';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Search: Fulltext search */
$handler->display->display_options['filters']['search_api_views_fulltext']['id'] = 'search_api_views_fulltext';
$handler->display->display_options['filters']['search_api_views_fulltext']['table'] = 'search_api_index_product_display';
$handler->display->display_options['filters']['search_api_views_fulltext']['field'] = 'search_api_views_fulltext';
$handler->display->display_options['filters']['search_api_views_fulltext']['exposed'] = TRUE;
$handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator_id'] = 'search_api_views_fulltext_op';
$handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator'] = 'search_api_views_fulltext_op';
$handler->display->display_options['filters']['search_api_views_fulltext']['expose']['identifier'] = 'search_api_views_fulltext';
/* Filter criterion: Product variations: Type (indexed) */
$handler->display->display_options['filters']['field_product_type']['id'] = 'field_product_type';
$handler->display->display_options['filters']['field_product_type']['table'] = 'search_api_index_product_display';
$handler->display->display_options['filters']['field_product_type']['field'] = 'field_product_type';
$handler->display->display_options['filters']['field_product_type']['value'] = array(
  'curriculum' => 'curriculum',
);
$handler->display->display_options['path'] = 'products';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'All products';
$handler->display->display_options['menu']['weight'] = '50';
$handler->display->display_options['menu']['name'] = 'main-menu';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['tab_options']['weight'] = '0';

/* Display: Resources */
$handler = $view->new_display('page', 'Resources', 'page_1');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['defaults']['css_class'] = FALSE;
$handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
$handler->display->display_options['exposed_block'] = TRUE;
$handler->display->display_options['defaults']['header'] = FALSE;
/* Header: Global: View area */
$handler->display->display_options['header']['view']['id'] = 'view';
$handler->display->display_options['header']['view']['table'] = 'views';
$handler->display->display_options['header']['view']['field'] = 'view';
$handler->display->display_options['header']['view']['view_to_insert'] = 'promo_touts:resources_banner';
$handler->display->display_options['defaults']['relationships'] = FALSE;
/* Relationship: Product variations: A bridge to node referenced by field_product (indexed) */
$handler->display->display_options['relationships']['field_product_field_product_node']['id'] = 'field_product_field_product_node';
$handler->display->display_options['relationships']['field_product_field_product_node']['table'] = 'search_api_index_product_display';
$handler->display->display_options['relationships']['field_product_field_product_node']['field'] = 'field_product_field_product_node';
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: Indexed Content: Search Boost */
$handler->display->display_options['sorts']['field_search_boost']['id'] = 'field_search_boost';
$handler->display->display_options['sorts']['field_search_boost']['table'] = 'search_api_index_product_display';
$handler->display->display_options['sorts']['field_search_boost']['field'] = 'field_search_boost';
$handler->display->display_options['sorts']['field_search_boost']['order'] = 'DESC';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Search: Fulltext search */
$handler->display->display_options['filters']['search_api_views_fulltext']['id'] = 'search_api_views_fulltext';
$handler->display->display_options['filters']['search_api_views_fulltext']['table'] = 'search_api_index_product_display';
$handler->display->display_options['filters']['search_api_views_fulltext']['field'] = 'search_api_views_fulltext';
$handler->display->display_options['filters']['search_api_views_fulltext']['exposed'] = TRUE;
$handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator_id'] = 'search_api_views_fulltext_op';
$handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator'] = 'search_api_views_fulltext_op';
$handler->display->display_options['filters']['search_api_views_fulltext']['expose']['identifier'] = 'search_api_views_fulltext';
/* Filter criterion: Product variations: Type (indexed) */
$handler->display->display_options['filters']['field_product_type']['id'] = 'field_product_type';
$handler->display->display_options['filters']['field_product_type']['table'] = 'search_api_index_product_display';
$handler->display->display_options['filters']['field_product_type']['field'] = 'field_product_type';
$handler->display->display_options['filters']['field_product_type']['value'] = array(
  'resource' => 'resource',
);
$handler->display->display_options['path'] = 'resources';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'All resources';
$handler->display->display_options['menu']['weight'] = '50';
$handler->display->display_options['menu']['name'] = 'main-menu';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
$handler->display->display_options['tab_options']['weight'] = '0';

/* Display: Family Entertainment */
$handler = $view->new_display('page', 'Family Entertainment', 'page_2');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['defaults']['css_class'] = FALSE;
$handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
$handler->display->display_options['exposed_block'] = TRUE;
$handler->display->display_options['defaults']['header'] = FALSE;
$handler->display->display_options['defaults']['relationships'] = FALSE;
/* Relationship: Product variations: A bridge to node referenced by field_product (indexed) */
$handler->display->display_options['relationships']['field_product_field_product_node']['id'] = 'field_product_field_product_node';
$handler->display->display_options['relationships']['field_product_field_product_node']['table'] = 'search_api_index_product_display';
$handler->display->display_options['relationships']['field_product_field_product_node']['field'] = 'field_product_field_product_node';
/* Relationship: Product variations: DVD Category (indexed) */
$handler->display->display_options['relationships']['field_product_field_dvd_category']['id'] = 'field_product_field_dvd_category';
$handler->display->display_options['relationships']['field_product_field_dvd_category']['table'] = 'search_api_index_product_display';
$handler->display->display_options['relationships']['field_product_field_dvd_category']['field'] = 'field_product_field_dvd_category';
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: Indexed Content: Search Boost */
$handler->display->display_options['sorts']['field_search_boost']['id'] = 'field_search_boost';
$handler->display->display_options['sorts']['field_search_boost']['table'] = 'search_api_index_product_display';
$handler->display->display_options['sorts']['field_search_boost']['field'] = 'field_search_boost';
$handler->display->display_options['sorts']['field_search_boost']['order'] = 'DESC';
$handler->display->display_options['defaults']['arguments'] = FALSE;
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Search: Fulltext search */
$handler->display->display_options['filters']['search_api_views_fulltext']['id'] = 'search_api_views_fulltext';
$handler->display->display_options['filters']['search_api_views_fulltext']['table'] = 'search_api_index_product_display';
$handler->display->display_options['filters']['search_api_views_fulltext']['field'] = 'search_api_views_fulltext';
$handler->display->display_options['filters']['search_api_views_fulltext']['exposed'] = TRUE;
$handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator_id'] = 'search_api_views_fulltext_op';
$handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator'] = 'search_api_views_fulltext_op';
$handler->display->display_options['filters']['search_api_views_fulltext']['expose']['identifier'] = 'search_api_views_fulltext';
/* Filter criterion: Product variations: Type (indexed) */
$handler->display->display_options['filters']['field_product_type']['id'] = 'field_product_type';
$handler->display->display_options['filters']['field_product_type']['table'] = 'search_api_index_product_display';
$handler->display->display_options['filters']['field_product_type']['field'] = 'field_product_type';
$handler->display->display_options['filters']['field_product_type']['value'] = array(
  'dvd' => 'dvd',
);
$handler->display->display_options['path'] = 'family-entertainment';
$handler->display->display_options['menu']['title'] = 'All resources';
$handler->display->display_options['menu']['weight'] = '50';
$handler->display->display_options['menu']['name'] = 'main-menu';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
$handler->display->display_options['tab_options']['weight'] = '0';


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'views',
  1 => '',
);
