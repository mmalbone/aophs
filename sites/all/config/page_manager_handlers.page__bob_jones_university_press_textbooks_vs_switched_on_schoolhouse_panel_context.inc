<?php
/**
 * @file
 * page_manager_handlers.page__bob_jones_university_press_textbooks_vs_switched_on_schoolhouse_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page__bob_jones_university_press_textbooks_vs_switched_on_schoolhouse_panel_context';
$handler->task = 'page';
$handler->subtask = '_bob_jones_university_press_textbooks_vs_switched_on_schoolhouse';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'eb94d2c3-fad1-4755-ad37-e369401b901f';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-48b706bd-7104-452f-986f-bf16733049d3';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => ' Bob Jones University Press Textbooks vs. Switched-On Schoolhouse',
    'body' => '<p>Looking to understand the difference between Bob Jones University Press textbooks and the Switched-On Schoolhouse program from Alpha Omega Publications? Good news! We have the information that you need. Find a short overview of each curriculum below, an easy-to-view comparison chart, and additional comparison options!</p><h3><strong>Bob Jones University Press Textbooks</strong></h3><p>Bob Jones University Press, an in-house curriculum publisher, provides K4-12th grade Christian curriculum and educational materials to Christian schools and homeschoolers. The BJU Press curriculum was designed for use in a Christian classroom setting and as such, tends to be teacher-intensive and often less friendly in a homeschool situation. The BJU Press textbook option is a teacher-driven, structured approach which requires homeschool parents to teach all coursework. Parents are also responsible for all administrative tasks, including lesson planning, grading, record-keeping, and reporting. Bob Jones University Press teacher\'s guides provide many useful teaching ideas and resources but tend to be better suited for use in a classroom and may include activities that are difficult to adapt to homeschool situations.</p><p>Written from a Christian worldview, all BJU Press text-based coursework is completely Bible-based. All science instruction is based on a biblical view of creation and the origin of life. Bob Jones University Press courses include Bible, reading, history, mathematics, science, health, English, music, and foreign language. Coursework can be purchased in complete grade level kits or as subject kits. Kits provide all essential materials for students and teachers. Additional optional resources can be purchased separately.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>Trying to decide whether a print-based or computer-based curriculum will work best for your child? Keep in mind that there is no one perfect homeschool curriculum. Giving the flexibility of using multiple resources instead of one set curriculum, a blended approach to homeschooling is often chosen by parents. Instead of feeling torn between different curriculums, parents should feel free to mix and match materials and resources to customize their child\'s education. Each child is unique and has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3><strong>Switched-On Schoolhouse from Alpha Omega Publications</strong></h3><p>Switched-On Schoolhouse (SOS) from Alpha Omega Publications is an engaging, computer-based curriculum for grades 3-12. Alpha Omega Publications, a trusted publisher of high quality educational products, provides Christian curriculum and educational services to homeschool families. Revolutionizing the way students learn, SOS is a comprehensive, CD-ROM-based curriculum that is entirely technology based. This innovative, state-of-the-art curriculum presents biblically-based lessons using interactive multimedia, eye-catching animation, learning games, video clips, and more! Unlike traditional textbooks, SOS has a diverse mix of text-based instruction and engaging multimedia enrichment. Unsurpassed administrative tools include time-saving features like automatic grading and lesson planning with built-in calendar, customizable lessons, flexible printing options, and message center.</p><p>Winner of Cathy Duffy\'s 100 Top Picks for Homeschool Curriculum, SOS integrates Scripture and a Christian worldview into all subjects. SOS offers the flexibility of an individualized, student-driven approach, helpful text-to-speech option, and diagnostic tests for correct placement. Switched-On Schoolhouse courses are available for 3rd – 12th grade students in Bible, language arts, math, science, and history and geography. Electives are also available for students of all ages. SOS may be purchased as individual subjects or as complete five-subject sets. The program includes all required student and parent materials.</p><h3><strong>Benefits and Features of Switched-On Schoolhouse from Alpha Omega Publications</strong>&nbsp;</h3><p class="list_heading">Switched-On Schoolhouse</p><ul><li>interactive, innovative, computer-based curriculum</li><li>offers flexible, engaging learning environment</li><li>contains multimedia tools, educational games, and automatic grading options</li><li>can reduce teacher planning and grading by 20 hours per week</li></ul>
<h3>Take a Closer Look at Bob Jones University Press Textbooks and Switched-On Schoolhouse</h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">Switched-On Schoolhouse</th><th class="compare">BJU Press Textbooks</th></tr>
</thead>
<tbody>
<tr>
<td>3-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Parent-directed curriculum</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Computer-based curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Interactive, engaging multimedia tools</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Customizable lessons and calendar</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Placement tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Automatic grading and lesson planning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Automatic updated material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Flexible and individualized</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Mastery-based learning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Message and resource center</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr><th class="features">Cost</th><th class="compare">Alpha Omega Publications</th><th class="compare">Bob Jones University Press</th></tr>
</thead>
<tbody>
<tr>
<td>Average yearly cost per grade level**</td>
<td class="tac">$449.95</td>
<td class="tac">$809.50</td>
</tr>
<tr class="alt">
<td>Average subject cost per grade level**</td>
<td class="tac">$99.95</td>
<td class="tac">$141.50</td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: Bob Jones University Press versus Alpha Omega Publications</h4>
<p class="list_heading_links">Bob Jones University Press Textbooks</p>
<ul>
<li><a href="/bju-textbook-vs-monarch">Bob Jones University Press Textbooks vs. Monarch</a></li>
<li><a href="/bju-textbook-vs-horizons">Bob Jones University Press Textbooks vs. Horizons</a></li>
<li><a href="/bju-textbook-vs-lifepac">Bob Jones University Press Textbooks vs. LIFEPAC</a></li>
<li><a href="/bju-textbook-vs-aoa">Bob Jones University Press Textbooks vs. Alpha Omega Academy</a></li>
<li><a href="/bju-textbook-vs-sos">Bob Jones University Press Textbooks vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">Bob Jones University Press (DVD)</p>
<ul>
<li><a href="/bju-dvd-vs-monarch">Bob Jones University Press (DVD) vs. Monarch</a></li>
<li><a href="/bju-dvd-vs-horizons">Bob Jones University Press (DVD) vs. Horizons</a></li>
<li><a href="/bju-dvd-vs-lifepac">Bob Jones University Press (DVD) vs. LIFEPAC</a></li>
<li><a href="/bju-dvd-vs-aoa">Bob Jones University Press (DVD) vs. Alpha Omega Academy</a></li>
<li><a href="/bju-dvd-vs-sos">Bob Jones University Press (DVD) vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">Bob Jones University Press Academy (Hard Drive)</p>
<ul>
<li><a href="/bju-hard-drive-vs-monarch">Bob Jones University Press Academy (Hard Drive) vs. Monarch</a></li>
<li><a href="/bju-hard-drive-vs-horizons">Bob Jones University Press Academy (Hard Drive) vs. Horizons</a></li>
<li><a href="/bju-hard-drive-vs-lifepac">Bob Jones University Press Academy (Hard Drive) vs. LIFEPAC</a></li>
<li><a href="/bju-hard-drive-vs-aoa">Bob Jones University Press Academy (Hard Drive) vs. Alpha Omega Academy</a></li>
<li><a href="/bju-hard-drive-vs-sos">Bob Jones University Press Academy (Hard Drive) vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">Bob Jones University Press Academy (Distance Learning Online)</p>
<ul>
<li><a href="/bju-online-vs-monarch">Bob Jones University Press Academy (Distance Learning Online) vs. Monarch</a></li>
<li><a href="/bju-online-vs-horizons">Bob Jones University Press Academy (Distance Learning Online) vs. Horizons</a></li>
<li><a href="/bju-online-vs-lifepac">Bob Jones University Press Academy (Distance Learning Online) vs. LIFEPAC</a></li>
<li><a href="/bju-online-vs-aoa">Bob Jones University Press Academy (Distance Learning Online) vs. Alpha Omega Academy</a></li>
<li><a href="/bju-online-vs-sos">Bob Jones University Press Academy (Distance Learning Online) vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">Bob Jones University Press Academy (Academy of Home Education)</p>
<ul>
<li><a href="/bju-academy-home-education-vs-monarch">Bob Jones University Press Academy (Academy of Home Education) vs. Monarch</a></li>
<li><a href="/bju-academy-home-education-vs-horizons">Bob Jones University Press Academy (Academy of Home Education) vs. Horizons</a></li>
<li><a href="/bju-academy-home-education-vs-lifepac">Bob Jones University Press Academy (Academy of Home Education) vs. LIFEPAC</a></li>
<li><a href="/bju-academy-home-education-vs-aoa">Bob Jones University Press Academy (Academy of Home Education) vs. Alpha Omega Academy</a></li>
<li><a href="/bju-academy-home-education-vs-sos">Bob Jones University Press Academy (Academy of Home Education) vs. Switched-On Schoolhouse</a></li>
</ul>
<p>*BJU Press® is the registered trademark and subsidiary of Bob Jones University®. Alpha Omega Publications is not in any way affiliated with Bob Jones University Press or its trademark owner Bob Jones University. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by BJU Press.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p><p>**Prices obtained February 2013 from BJU Press Total Homeschool Solutions and BJUP websites and from Alpha Omega Publications 2013 Homeschool Catalog. Yearly prices based on comparison between SOS 3rd Grade 5-Subject Set and BJU Press 3rd Grade Textbook pricing for a full year. Individual subject prices are based on comparison between SOS 3rd Grade Math Curriculum Set and BJU Press 3rd Grade Math Subject Kit. Costs do not reflect any additional fees or shipping and handling charges. Prices for other Bob Jones University Press and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '48b706bd-7104-452f-986f-bf16733049d3';
  $display->content['new-48b706bd-7104-452f-986f-bf16733049d3'] = $pane;
  $display->panels['left'][0] = 'new-48b706bd-7104-452f-986f-bf16733049d3';
  $pane = new stdClass();
  $pane->pid = 'new-eb888010-3a84-4c7c-b1c2-30f76ec6762e';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'eb888010-3a84-4c7c-b1c2-30f76ec6762e';
  $display->content['new-eb888010-3a84-4c7c-b1c2-30f76ec6762e'] = $pane;
  $display->panels['right'][0] = 'new-eb888010-3a84-4c7c-b1c2-30f76ec6762e';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-48b706bd-7104-452f-986f-bf16733049d3';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
