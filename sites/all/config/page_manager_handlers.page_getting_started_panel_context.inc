<?php
/**
 * @file
 * page_manager_handlers.page_getting_started_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_getting_started_panel_context';
$handler->task = 'page';
$handler->subtask = 'getting_started';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Get Started',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'flexible';
$display->layout_settings = array(
  'items' => array(
    'canvas' => array(
      'type' => 'row',
      'contains' => 'column',
      'children' => array(
        0 => 'main',
      ),
      'parent' => NULL,
    ),
    'main' => array(
      'type' => 'column',
      'width' => 100,
      'width_type' => '%',
      'children' => array(
        0 => 'main-row',
        1 => 1,
        2 => 2,
      ),
      'parent' => 'canvas',
    ),
    'main-row' => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'center',
      ),
      'parent' => 'main',
    ),
    'center' => array(
      'type' => 'region',
      'title' => 'Center',
      'width' => 100,
      'width_type' => '%',
      'parent' => 'main-row',
      'class' => 'get-started-top-center',
    ),
    1 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'left',
        1 => 'center_',
        2 => 'right',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    'left' => array(
      'type' => 'region',
      'title' => 'Left',
      'width' => '33.29548717145616',
      'width_type' => '%',
      'parent' => '1',
      'class' => 'get-started-middle-left',
    ),
    'center_' => array(
      'type' => 'region',
      'title' => 'Center',
      'width' => '33.34466999391287',
      'width_type' => '%',
      'parent' => '1',
      'class' => 'get-started-middle-center',
    ),
    'right' => array(
      'type' => 'region',
      'title' => 'Right',
      'width' => '33.35984283463099',
      'width_type' => '%',
      'parent' => '1',
      'class' => 'get-started-middle-right',
    ),
    2 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'left_',
        1 => 'right_',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    'left_' => array(
      'type' => 'region',
      'title' => 'Left',
      'width' => 50,
      'width_type' => '%',
      'parent' => '2',
      'class' => 'get-started-bottom-left',
    ),
    'right_' => array(
      'type' => 'region',
      'title' => 'Right',
      'width' => 50,
      'width_type' => '%',
      'parent' => '2',
      'class' => 'get-started-bottom-right',
    ),
  ),
);
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'center' => NULL,
    'homepage_hero_left' => NULL,
    'homepage_hero_right' => NULL,
    'homepage_carousel' => NULL,
    'homepage_recent_stories' => NULL,
    'homepage_popular_articles' => NULL,
    'homepage_bottom_grid_item_1' => NULL,
    'homepage_bottom_grid_item_2' => NULL,
    'homepage_bottom_grid_item_3' => NULL,
    'homepage_bottom_grid_item_3_' => NULL,
    'left' => NULL,
    'center_' => NULL,
    'right' => NULL,
    'left_' => NULL,
    'right_' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'd93994bf-b7f1-4ca7-b01e-0134c8130243';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-e491439d-1097-4033-8c72-66f220d58af6';
  $pane->panel = 'center';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => '',
    'body' => '<h1>Getting Started is Easier Than You Think</h1><h2>3 ways to get you going on your homeschool journey.</h2>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'get-started-top-promo lp-banner',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'e491439d-1097-4033-8c72-66f220d58af6';
  $display->content['new-e491439d-1097-4033-8c72-66f220d58af6'] = $pane;
  $display->panels['center'][0] = 'new-e491439d-1097-4033-8c72-66f220d58af6';
  $pane = new stdClass();
  $pane->pid = 'new-56134e26-0284-4be0-a734-7a5468bcdb7d';
  $pane->panel = 'center_';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Finding your curriculum',
    'body' => '<p>Not sure which curriculum best suits you? Our curriculum tool helps you determine the best curriculum for each child’s individual needs. See what’s right for your family.</p>
<p style="text-align: center;"><a href="/find-a-curriculum/" class="foryourfamily"><em>Find the right curriculum</em><br><strong>FOR YOUR FAMILY</strong></a></p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'getting-started-2-wrapper',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '56134e26-0284-4be0-a734-7a5468bcdb7d';
  $display->content['new-56134e26-0284-4be0-a734-7a5468bcdb7d'] = $pane;
  $display->panels['center_'][0] = 'new-56134e26-0284-4be0-a734-7a5468bcdb7d';
  $pane = new stdClass();
  $pane->pid = 'new-7344178f-149a-4cb9-b848-ea5e1dee046a';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Tools to get started',
    'body' => '<p>We’ve been doing this for a while, so let us help you out. Get started on your homeschooling journey with these helpful resources for beginners.</p><ul>
<li><a href="/series">7 Steps to Starting</a></li>
<li><a href="/dedication">Top 10 Reasons to Homeschool</a></li>
<li><a href="/how-to-get-started-homeschooling">Tips for First-time Homeschoolers</a></li>
</ul>',
    'format' => 'markdown',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'getting-started-1-wrapper',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '7344178f-149a-4cb9-b848-ea5e1dee046a';
  $display->content['new-7344178f-149a-4cb9-b848-ea5e1dee046a'] = $pane;
  $display->panels['left'][0] = 'new-7344178f-149a-4cb9-b848-ea5e1dee046a';
  $pane = new stdClass();
  $pane->pid = 'new-74677c40-3929-4470-8219-fd33af7d5c62';
  $pane->panel = 'left_';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'We want to hear from you',
    'body' => '<div class="text">Get expert curriculum advice! Call us on weekdays at 800.622.3070 from 7 a.m. to 5 p.m. (CT).</p></div><div class="buttons"><p><a class="button-gray-background" href="https://secure.livechatinc.com/licence/3247562/open_chat.cgi?groups=10" target="_blank"><em>Chat</em> <strong>NOW!</strong></a> <a class="button-gray-background" href="/support/schedule-a-callback"><em>Schedule a</em> <strong>CALL</strong></a></div>',
    'format' => 'markdown',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'get-started-hear-from-you',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '74677c40-3929-4470-8219-fd33af7d5c62';
  $display->content['new-74677c40-3929-4470-8219-fd33af7d5c62'] = $pane;
  $display->panels['left_'][0] = 'new-74677c40-3929-4470-8219-fd33af7d5c62';
  $pane = new stdClass();
  $pane->pid = 'new-ca031f2f-d6bc-4b53-af7d-a2066876c445';
  $pane->panel = 'right';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Free Resources',
    'body' => '<p>Take advantage of these free homeschooling resources for encouragement to get going and support along the way.</p><ul><li><a href="/community">Local Homeschool Support Groups</a></li><li><a href="/blog">Homeschool Blog</a></li><li><a href="/blog-categories/daily-focus">Daily Homeschool Devotions</a></li><li><a href="https://www.facebook.com/aophomeschooling" target="_blank">Facebook</a></li><li><a href="https://twitter.com/homeschoolers" target="_blank">Twitter</a></li><li><a href="http://www.pinterest.com/aophomeschool/" target="_blank">Pinterest</a></li><li><a href="/enews">Free Homeschool eNews</a></li></ul><p><a class="button-blue-background" href="https://www.facebook.com/aophomeschooling" target="_blank"><em>Join us on</em> <strong>FACEBOOK</strong></a></p>',
    'format' => 'markdown',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'getting-started-3-wrapper',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'ca031f2f-d6bc-4b53-af7d-a2066876c445';
  $display->content['new-ca031f2f-d6bc-4b53-af7d-a2066876c445'] = $pane;
  $display->panels['right'][0] = 'new-ca031f2f-d6bc-4b53-af7d-a2066876c445';
  $pane = new stdClass();
  $pane->pid = 'new-f7f71f8e-6d3a-4aa1-a95a-793711b57eed';
  $pane->panel = 'right_';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Order a free catalog',
    'body' => '<div class="text">Learn more about our products and commitment to Christian education.</p></div><div class="buttons"><p><a class="button-gray-background" href="webform/request-catalog"><em>Request a</em> <strong>CATALOG</strong></a></div>',
    'format' => 'markdown',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'get-started-order-catalog',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'f7f71f8e-6d3a-4aa1-a95a-793711b57eed';
  $display->content['new-f7f71f8e-6d3a-4aa1-a95a-793711b57eed'] = $pane;
  $display->panels['right_'][0] = 'new-f7f71f8e-6d3a-4aa1-a95a-793711b57eed';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = '0';
$handler->conf['display'] = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
