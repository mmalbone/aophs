<?php
/**
 * @file
 * image_style.blog_image_full.inc
 */

$api = '2.0.0';

$data = array(
  'effects' => array(),
  'label' => 'Blog image full',
  'name' => 'blog_image_full',
  'storage' => 1,
);

$dependencies = array();

$optional = array();

$modules = array();
