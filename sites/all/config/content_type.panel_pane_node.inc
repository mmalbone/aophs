<?php
/**
 * @file
 * content_type.panel_pane_node.inc
 */

$api = '2.0.0';

$data = (object) array(
  'base' => 'node_content',
  'description' => '',
  'has_title' => '1',
  'help' => '',
  'name' => 'Panel Pane Node',
  'title_label' => 'Title',
  'type' => 'panel_pane_node',
);

$dependencies = array();

$optional = array(
  'field.node.body.panel_pane_node' => 'field.node.body.panel_pane_node',
  'field.node.field_pane_node_image.panel_pane_node' => 'field.node.field_pane_node_image.panel_pane_node',
  'field.node.field_title_type.panel_pane_node' => 'field.node.field_title_type.panel_pane_node',
  'field.node.title_field.panel_pane_node' => 'field.node.title_field.panel_pane_node',
  'permission.create_panel_pane_node_content' => 'permission.create_panel_pane_node_content',
  'permission.delete_any_panel_pane_node_content' => 'permission.delete_any_panel_pane_node_content',
  'permission.delete_own_panel_pane_node_content' => 'permission.delete_own_panel_pane_node_content',
  'permission.edit_any_panel_pane_node_content' => 'permission.edit_any_panel_pane_node_content',
  'permission.edit_own_panel_pane_node_content' => 'permission.edit_own_panel_pane_node_content',
);

$modules = array(
  0 => 'node',
);
