<?php
/**
 * @file
 * page_manager_handlers.page__a_beka_academy_traditional_parent_directed_option_vs_horizons_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page__a_beka_academy_traditional_parent_directed_option_vs_horizons_panel_context';
$handler->task = 'page';
$handler->subtask = '_a_beka_academy_traditional_parent_directed_option_vs_horizons';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '5bb93317-1c72-434c-a22f-257e4941e762';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-29886dd5-aaf5-4e50-99b6-cc354dd6ec19';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'A Beka Academy Traditional Parent-Directed Option vs. Horizons',
    'body' => '<p>Looking for the differences between A Beka Academy Traditional Parent-Directed Option and Horizons curriculum from Alpha Omega Publications? You\'re in the right place. Find short overviews of each curriculum below, an easy-to-view comparison chart, and additional comparison options!</p><p><strong>A Beka Academy Traditional Parent-Directed Option</strong></p><p>A Beka Academy Traditional Parent-Directed Option for homeschooling families is available for students in grades K5-6 and provides a blending of an accredited (FACCS and SACS CASI) distance learning option with a more traditional parent-directed homeschool program. The A Beka Academy Traditional Parent-Directed Option uses structured, print-based curriculum that was originally written for teachers in a Christian school setting. A Beka Book\'s Traditional Parent-Directed Program requires homeschoolers to follow a highly structured, parent-supervised program with all lessons taught by a parent. Teacher\'s guides include lesson plans that are often more suited to a classroom setting and may at times be difficult to adapt to homeschool situations.</p><p>The A Beka Traditional Parent-Directed Option provides all necessary textbooks, tests, and keys, as well as a parent manual with daily lessons plans that follow a 170-day school year. Students must meet age requirements and placement criterion to be enrolled. A Beka Academy provides progress reports for record keeping, evaluation of student work, a report card for each grading period, and one copy of the transcript. Parental responsibilities include assuring compliance with state laws, showing proof of previously completed work, providing supervision for all work, and sending in all required graded work in accordance with the provided academic calendar. Coursework must be completed within ten months of the assigned start date. All A Beka coursework is Bible-based and provides instruction from a Christian worldview. Science courses are strongly creation-based. Courses are available in all core subjects: Bible, English, mathematics, science, and history and geography. Electives are also available.</p><p><strong>Finding Your Approach to Homeschooling</strong></p><p>Wondering how you will find the perfect homeschool curriculum for your family? Keep in mind that there is no one perfect homeschool curriculum. A blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Instead of feeling torn between curriculum formats, parents should feel free to mix and match materials and resources to customize their child\'s education. Remember, each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><p><strong>Horizons from Alpha Omega Publications</strong></p><p>Horizons is a preK-12 print-based curriculum offered by Alpha Omega Publications, a Christian-based publisher that provides curriculum and educational services to homeschool families. Filled with engaging, full-color illustrations, Horizons is a child-friendly workbook-based curriculum with consumable lessons and hands-on activities. Horizons curriculum encourages concept mastery through a spiral-based learning process of skill/concept presentation, review, and reinforcement. Teacher\'s guides offer clear, step-by-step lesson plans and activities that are suitable for both homeschool and classroom settings.</p><p>Winner of Cathy Duffy\'s 100 Top Picks for Homeschool Curriculum, Horizons integrates Scripture and Christian living throughout all subjects. Horizons workbooks provide removable student pages for ease of student use. Horizons curriculum includes courses in math (K-8), penmanship (1-5), health (K-8), physical education (PreK-12), phonics and reading (K-3), and spelling and vocabulary (1-3). Health and physical education electives were designed for classroom use and may need to be adapted to homeschool use. Complete Horizons Preschool Curriculum Sets and Multimedia Sets are also available. Horizons curriculum sets include all student and teacher materials. Student and teacher materials can also be purchased separately.</p><p><strong>Benefits and Features of Horizons from Alpha Omega Publications</strong></p><p class="list_heading">Horizons</p><ul><li>brightly illustrated, hands-on, consumable curriculum</li><li>offers fun lessons and manipulative-based activities</li><li>teaches concepts through a spiral learning process</li><li>encourages concept mastery through quick concept introduction, review, and reinforcement</li></ul>
<h3>Take a Closer Look at A Beka Academy Traditional Parent-Directed Option and Horizons</h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">Horizons</th><th class="compare">A Beka Academy Traditional Parent-Directed Option (with A Beka Books)</th></tr>
</thead>
<tbody>
<tr>
<td>K-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Accredited</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Parent-directed curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Teacher-intensive curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Readiness tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Easily removable student pages</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Manageable amount of daily lesson material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Easy-to-use teacher\'s guides</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Material designed for homeschoolers</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Flexible scheduling and pacing</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Can be purchased as individual subjects</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Requires student work to be mailed for evaluation</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr><th class="features">Cost</th><th class="compare">Horizons</th><th class="compare">A Beka Academy Traditional Parent-Directed Option (with A Beka Books)</th></tr>
</thead>
<tbody>
<tr>
<td>Average yearly cost per grade level**</td>
<td class="tac">$262.80</td>
<td class="tac">$750</td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: A Beka versus Alpha Omega Publications</h4>
<p class="list_heading_links">A Beka Book</p>
<ul>
<li><a href="/abeka-book-vs-monarch">A Beka Book vs. Monarch</a></li>
<li><a href="/abeka-book-vs-horizons">A Beka Book vs. Horizons</a></li>
<li><a href="/abeka-book-vs-lifepac">A Beka Book vs. LIFEPAC</a></li>
<li><a href="/abeka-book-vs-aoa">A Beka Book vs. Alpha Omega Academy</a></li>
<li><a href="/abeka-book-vs-sos">A Beka Book vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">A Beka Academy (DVD)</p>
<ul>
<li><a href="/abeka-dvd-vs-monarch">A Beka Academy (DVD) vs. Monarch</a></li>
<li><a href="/abeka-dvd-vs-horizons">A Beka Academy (DVD) vs. Horizons</a></li>
<li><a href="/abeka-dvd-vs-lifepac">A Beka Academy (DVD) vs. LIFEPAC</a></li>
<li><a href="/abeka-dvd-vs-aoa">A Beka Academy (DVD) vs. Alpha Omega Academy</a></li>
<li><a href="/abeka-dvd-vs-sos">A Beka Academy (DVD) vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">A Beka Academy (Parent-Directed)</p>
<ul>
<li><a href="/abeka-parent-directed-vs-monarch">A Beka Academy (Parent-Directed) vs. Monarch</a></li>
<li><a href="abeka-parent-directed-vs-horizons">A Beka Academy (Parent-Directed) vs. Horizons</a></li>
<li><a href="/abeka-parent-directed-vs-lifepac">A Beka Academy (Parent-Directed) vs. LIFEPAC</a></li>
<li><a href="/abeka-parent-directed-vs-aoa">A Beka Academy (Parent-Directed) vs. Alpha Omega Academy</a></li>
<li><a href="/abeka-parent-directed-vs-sos">A Beka Academy (Parent-Directed) vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">A Beka Video-based Internet Streaming</p>
<ul>
<li><a href="/abeka-academy-streaming-video-vs-monarch">A Beka Video-based Internet Streaming vs. Monarch</a></li>
<li><a href="/abeka-academy-streaming-video-vs-horizons">A Beka Video-based Internet Streaming vs. Horizons</a></li>
<li><a href="/abeka-academy-streaming-video-vs-lifepac">A Beka Video-based Internet Streaming vs. LIFEPAC</a></li>
<li><a href="/abeka-academy-streaming-video-vs-aoa">A Beka Video-based Internet Streaming vs. Alpha Omega Academy</a></li>
<li><a href="/abeka-academy-streaming-video-vs-sos">A Beka Video-based Internet Streaming vs. Switched-On Schoolhouse</a></li>
</ul>
<p>*A Beka Book® and A Beka Academy® are the registered trademarks and subsidiaries of Pensacola Christian College. Alpha Omega Publications is not in any way affiliated with A Beka®. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by A Beka Book or A Beka Academy.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p><p>**Prices obtained February 2013 from A Beka Academy\'s website and from Alpha Omega Publications 2013 Homeschool Catalog. Annual prices based on comparison between Horizons 2nd Grade four subjects of math, penmanship, phonics and reading, spelling and vocabulary (including all student and teacher material, along with supplements) and A Beka Academy total tuition fee for a full elementary grade. Costs do not reflect any additional fees or shipping and handling charges. Prices for other A Beka and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '29886dd5-aaf5-4e50-99b6-cc354dd6ec19';
  $display->content['new-29886dd5-aaf5-4e50-99b6-cc354dd6ec19'] = $pane;
  $display->panels['left'][0] = 'new-29886dd5-aaf5-4e50-99b6-cc354dd6ec19';
  $pane = new stdClass();
  $pane->pid = 'new-00988ba5-1044-4201-b90c-aa9556cd95bc';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '00988ba5-1044-4201-b90c-aa9556cd95bc';
  $display->content['new-00988ba5-1044-4201-b90c-aa9556cd95bc'] = $pane;
  $display->panels['right'][0] = 'new-00988ba5-1044-4201-b90c-aa9556cd95bc';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-29886dd5-aaf5-4e50-99b6-cc354dd6ec19';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
