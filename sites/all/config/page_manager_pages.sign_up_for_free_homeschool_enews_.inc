<?php
/**
 * @file
 * page_manager_pages.sign_up_for_free_homeschool_enews_.inc
 */

$api = '2.0.0';

$data = $page = new stdClass();
$page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
$page->api_version = 1;
$page->name = 'sign_up_for_free_homeschool_enews_';
$page->task = 'page';
$page->admin_title = 'Sign up for Free Homeschool eNews!';
$page->admin_description = '';
$page->path = 'enews';
$page->access = array();
$page->menu = array();
$page->arguments = array();
$page->conf = array(
  'admin_paths' => FALSE,
);


$dependencies = array(
  'page_manager_handlers.page_sign_up_for_free_homeschool_enews__panel_context' => 'page_manager_handlers.page_sign_up_for_free_homeschool_enews__panel_context',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
