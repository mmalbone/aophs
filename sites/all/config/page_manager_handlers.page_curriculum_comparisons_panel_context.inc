<?php
/**
 * @file
 * page_manager_handlers.page_curriculum_comparisons_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_curriculum_comparisons_panel_context';
$handler->task = 'page';
$handler->subtask = 'curriculum_comparisons';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'e11ea225-5bef-42ee-8d94-0c4815093756';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-3b4ccc22-c6ce-4821-be4f-a2c645db17ee';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Curriculum Comparisons',
    'body' => '<p>At Alpha Omega Publications®, we understand that homeschooling parents want to make educated decisions when choosing the best curriculum for their children. Because of this, we have decided to make it easy for you to compare our curricula with the curricula of our major competitors.</p><p><strong>View AOP\'s curricula in comparison to</strong></p><ul><li><a href="/abeka/">A Beka</a></li><li><a href="/ace/">Accelerated Christian Education (A.C.E.) or School of Tomorrow</a></li><li><a href="/bju/">Bob Jones University Press</a></li><li><a href="/christian-liberty/">Christian Liberty</a></li><li><a href="/k12/">K12</a></li><li><a href="/rod-and-staff/">Rod and Staff</a></li><li><a href="/sonlight/">Sonlight</a></li><li><a href="/time4learning/">Time4Learning</a></li></ul><p></p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '3b4ccc22-c6ce-4821-be4f-a2c645db17ee';
  $display->content['new-3b4ccc22-c6ce-4821-be4f-a2c645db17ee'] = $pane;
  $display->panels['middle'][0] = 'new-3b4ccc22-c6ce-4821-be4f-a2c645db17ee';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = 'new-3b4ccc22-c6ce-4821-be4f-a2c645db17ee';
$handler->conf['display'] = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
