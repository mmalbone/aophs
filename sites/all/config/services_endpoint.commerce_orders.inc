<?php
/**
 * @file
 * services_endpoint.commerce_orders.inc
 */

$api = '2.0.0';

$data = $endpoint = new stdClass();
$endpoint->disabled = FALSE; /* Edit this to true to make a default endpoint disabled initially */
$endpoint->api_version = 3;
$endpoint->name = 'commerce_orders';
$endpoint->server = 'rest_server';
$endpoint->path = 'assist/orders';
$endpoint->authentication = array(
  'services_oauth' => array(
    'oauth_context' => 'mule',
    'authorization' => 'muleoauth',
    'credentials' => 'consumer',
  ),
);
$endpoint->server_settings = array(
  'formatters' => array(
    'json' => TRUE,
    'bencode' => FALSE,
    'jsonp' => FALSE,
    'php' => FALSE,
    'xml' => FALSE,
  ),
  'parsers' => array(
    'application/json' => TRUE,
    'application/vnd.php.serialized' => FALSE,
    'application/x-www-form-urlencoded' => FALSE,
    'application/xml' => FALSE,
    'multipart/form-data' => FALSE,
    'text/xml' => FALSE,
  ),
);
$endpoint->resources = array(
  'order' => array(
    'operations' => array(
      'index' => array(
        'enabled' => '1',
      ),
      'retrieve' => array(
        'enabled' => '1',
      ),
      'update' => array(
        'enabled' => '1',
      ),
      'delete' => array(
        'enabled' => '1',
      ),
    ),
    'relationships' => array(
      'line-items' => array(
        'enabled' => '1',
      ),
    ),
  ),
);
$endpoint->debug = 1;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'services',
);
