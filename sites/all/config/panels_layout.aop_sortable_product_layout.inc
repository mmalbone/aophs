<?php
/**
 * @file
 * panels_layout.aop_sortable_product_layout.inc
 */

$api = '2.0.0';

$data = $layout = new stdClass();
$layout->disabled = FALSE; /* Edit this to true to make a default layout disabled initially */
$layout->api_version = 1;
$layout->name = 'aop_sortable_product_layout';
$layout->admin_title = 'AOP Sortable Product Layout';
$layout->admin_description = '';
$layout->category = 'AOP';
$layout->plugin = 'flexible';
$layout->settings = array(
  'items' => array(
    'canvas' => array(
      'type' => 'row',
      'contains' => 'column',
      'children' => array(
        0 => 'main',
      ),
      'parent' => NULL,
      'class' => '',
      'column_class' => '',
      'row_class' => '',
      'region_class' => '',
      'no_scale' => TRUE,
      'fixed_width' => '',
      'column_separation' => '0.5em',
      'region_separation' => '0.5em',
      'row_separation' => '0.5em',
    ),
    'main' => array(
      'type' => 'column',
      'width' => 100,
      'width_type' => '%',
      'children' => array(
        0 => 1,
        1 => 'main-row',
        2 => 2,
        3 => 3,
        4 => 4,
      ),
      'parent' => 'canvas',
    ),
    'main-row' => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'center',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    1 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'full_width_landing_page_banner',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    'full_width_landing_page_banner' => array(
      'type' => 'region',
      'title' => 'Full Width Landing Page Banner',
      'width' => 100,
      'width_type' => '%',
      'parent' => '1',
      'class' => 'landing-page-banner',
    ),
    2 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'fac_product_blocks',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    'center' => array(
      'type' => 'region',
      'title' => 'FAC Wizard',
      'width' => 100,
      'width_type' => '%',
      'parent' => 'main-row',
      'class' => 'fac-wizard-wrapper',
    ),
    'fac_product_blocks' => array(
      'type' => 'region',
      'title' => 'FAC Product Blocks',
      'width' => 100,
      'width_type' => '%',
      'parent' => '2',
      'class' => 'fac-products-wrapper',
    ),
    3 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'full_width',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    'full_width' => array(
      'type' => 'region',
      'title' => 'Full Width',
      'width' => 100,
      'width_type' => '%',
      'parent' => '3',
      'class' => '',
    ),
    4 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'left',
        1 => 'right',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    'left' => array(
      'type' => 'region',
      'title' => 'Left',
      'width' => 50,
      'width_type' => '%',
      'parent' => '4',
      'class' => '',
    ),
    'right' => array(
      'type' => 'region',
      'title' => 'Right',
      'width' => 50,
      'width_type' => '%',
      'parent' => '4',
      'class' => '',
    ),
  ),
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'panels',
);
