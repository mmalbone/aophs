<?php
/**
 * @file
 * page_manager_handlers.page__k12_teacher_supported_vs_monarch_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page__k12_teacher_supported_vs_monarch_panel_context';
$handler->task = 'page';
$handler->subtask = '_k12_teacher_supported_vs_monarch';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'b5b10791-083e-42d8-a85a-eae1c244d0b3';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-b108453e-41ca-46ad-b7a6-2f4c98446ec8';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'K12 Teacher-Supported vs. Monarch',
    'body' => '<p>Do you need to compare the teacher-supported online curriculum offered by K12 Consumer Direct to the online Monarch program from Alpha Omega Publications? You\'re in the right spot. Find a short overview of each curriculum and service below, an easy-to-view comparison chart, and additional comparison options!</p><h3><strong>K12 Virtual Schools</strong></h3><p>K12 is a large secular provider of kindergarten through 12th grade online curriculum currently being used in virtual public schools, an international online academy, as well as in homeschools worldwide. Because K12 is secular curriculum provider, parents from various faiths and cultural backgrounds may find K12 very appropriate for their families\' homeschool needs. Through its Virtual School programs, K12 offers kindergarten through 12th grade curriculum to families who desire to teach their children at home but prefer a public school program. The K12 Virtual school program is a partnership with local public schools systems providing a high quality online education to children in their homes.</p><p>The K12 curriculum covers six core subjects: language arts/English, math, science, history, art, and music and is based on time-tested and research-based methods of instruction. Students enrolled in a virtual school take their academic courses online with support from a certified public school teacher. The parent acts as a learning coach helping to keep the student successfully engaged in the learning process. Because they are part of the public school system, K12 Virtual Schools are tuition free to all students. This also means that K12 Virtual School families are 100% accountable to the public school system in which they are enrolled. K12 Virtual Schools are not available in all areas.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>As you consider your homeschool curriculum options, remember there is no one perfect homeschool curriculum. A blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Instead of feeling torn between different curriculums, parents should be open to mixing and matching materials and resources to customize their child\'s education. Remember, each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3><strong>Monarch from Alpha Omega Publications</strong></h3><p>Monarch from Alpha Omega Publications is an interactive, Internet-based curriculum for grades 3-12. Alpha Omega Publications, a trusted publisher of high quality educational products, provides Christian curriculum and educational services to homeschool families. Transforming the way students learn, Monarch is an in-depth, online curriculum that makes use of today\'s 21st century technology. This innovative, cutting-edge AOP curriculum presents Christ-centered lessons using eye-catching animations, learning games, video and audio clips, and more! Unlike traditional textbooks, Monarch has a diverse mix of text-based instruction and engaging multimedia enrichment. Time-saving teaching tools provide automatic grading, lesson planning, customizable lessons, a built-in calendar, printing options, message center, text-to-speech, and more!</p><p>Compatible with both Windows® and Macintosh® operating systems, Monarch integrates Scripture and a biblical worldview into all subjects. Monarch offers the flexibility to log on and learn anytime, anywhere with 18-month, single subscriptions using an individualized, student driven approach. Diagnostic tests are available for placement in math and language arts. Core coursework includes Bible (3-12), language arts (3-12), math (3-12), science (3-12), and history and geography (3-12). Over 35 electives are also available. Monarch may be purchased as individual subjects or as complete five-subject sets.</p><h3><strong>Benefits and Features of Monarch from Alpha Omega Publications</strong></h3><p class="list_heading">Monarch</p><ul><li>online interactive lessons with anytime, anywhere access</li><li>compatible with both Windows® and Mac® operating systems</li><li>offers customizable student learning options and time-saving teaching tools</li><li>features dynamic, media-rich lessons; real-time content updates; and data storage</li></ul><h3 class="para-start"><strong>Take a Closer Look at K12 Teacher-supported Consumer Direct and Monarch</strong></h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">Monarch</th><th class="compare">K12 Teacher-Supported</th></tr>
</thead>
<tbody>
<tr>
<td>3-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Student-directed curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Internet-based curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"> (partial)</td>
</tr>
<tr class="alt">
<td>Interactive, engaging multimedia tools</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Customizable lessons &amp; calendar</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Placement tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Automatic grading and lesson planning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">Of some material</td>
</tr>
<tr class="alt">
<td>Automatic updated material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Flexible and individualized</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Mastery-based learning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Message center</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr><th class="features">Cost</th><th class="compare">Monarch</th><th class="compare">K12</th></tr>
</thead>
<tbody>
<tr>
<td>Average yearly cost per grade level**</td>
<td class="tac">$449.95</td>
<td class="tac">$2200.00</td>
</tr>
<tr class="alt">
<td>Average subject cost per grade level**</td>
<td class="tac">$99.95</td>
<td class="tac">$550</td>
</tr>
</tbody>
</table>
</div>
<h4><strong>Compare Offerings: K12 versus Alpha Omega Publications</strong></h4>
<p class="list_heading_links">K12 Independent Consumer Direct</p>
<ul>
<li><a href="/k12-independent-vs-monarch">K12 Independent Consumer Direct vs. Monarch</a></li>
<li><a href="/k12-independent-vs-aoa">K12 Independent Consumer Direct vs. Alpha Omega Academy</a></li>
<li><a href="/k12-independent-vs-horizons">K12 Independent Consumer Direct vs. Horizons</a></li>
<li><a href="/k12-independent-vs-lifepac">K12 Independent Consumer Direct vs. LIFEPAC</a></li>
<li><a href="/k12-independent-vs-sos">K12 Independent Consumer Direct vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">K12 Teacher-Supported Consumer Direct</p>
<ul>
<li><a href="/k12-teacher-supported-vs-monarch">K12 Teacher-Supported Consumer Direct vs. Monarch</a></li>
<li><a href="/k12-teacher-supported-vs-aoa">K12 Teacher-Supported Consumer Direct vs. Alpha Omega Academy</a></li>
<li><a href="/k12-teacher-supported-vs-horizons">K12 Teacher-Supported Consumer Direct vs. Horizons</a></li>
<li><a href="/k12-teacher-supported-vs-lifepac">K12 Teacher-Supported Consumer Direct vs. LIFEPAC</a></li>
<li><a href="/k12-teacher-supported-vs-sos">K12 Teacher-Supported Consumer Direct vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">K12 Virtual Schools</p>
<ul>
<li><a href="/k12-virtual-schools-vs-monarch">K12 Virtual Schools vs. Monarch</a></li>
<li><a href="/k12-virtual-schools-vs-aoa">K12 Virtual Schools vs. Alpha Omega Academy</a></li>
<li><a href="/k12-virtual-schools-vs-horizons">K12 Virtual Schools vs. Horizons</a></li>
<li><a href="/k12-virtual-schools-vs-lifepac">K12 Virtual Schools vs. LIFEPAC</a></li>
<li><a href="/k12-virtual-schools-vs-sos">K12 Virtual Schools vs. Switched-On Schoolhouse</a></li>
</ul>
<p>* K12 is the registered trademark of K12 Inc. Alpha Omega Publications is not in any way affiliated with K12. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by K12.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p><p>**Prices obtained February 2013 from the K12 website and from Alpha Omega Publications 2013 Homeschool Catalog. Yearly prices based on comparison between the Monarch 5th Grade 5-subject set and direct purchase of four K12 teacher-supported courses for grades K-5 (not including some required materials). Individual subject prices are based on comparison between costs for Monarch 5th Grade Math and direct purchase of a single K12 teacher-supported course for grades K-5 (not including some required materials). Costs do not reflect any additional fees or shipping and handling charges. Prices for other K12 and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'b108453e-41ca-46ad-b7a6-2f4c98446ec8';
  $display->content['new-b108453e-41ca-46ad-b7a6-2f4c98446ec8'] = $pane;
  $display->panels['left'][0] = 'new-b108453e-41ca-46ad-b7a6-2f4c98446ec8';
  $pane = new stdClass();
  $pane->pid = 'new-9883b927-4862-4a92-9216-1d4c5109f69a';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '9883b927-4862-4a92-9216-1d4c5109f69a';
  $display->content['new-9883b927-4862-4a92-9216-1d4c5109f69a'] = $pane;
  $display->panels['right'][0] = 'new-9883b927-4862-4a92-9216-1d4c5109f69a';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-b108453e-41ca-46ad-b7a6-2f4c98446ec8';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
