<?php
/**
 * @file
 * field.node.field_content_placement.promo_tout.inc
 */

$api = '2.0.0';

$data = array(
  'field_config' => array(
    'active' => '1',
    'cardinality' => '1',
    'columns' => array(
      'value' => array(
        'length' => 255,
        'not null' => FALSE,
        'type' => 'varchar',
      ),
    ),
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_content_placement',
    'field_permissions' => array(
      'type' => '0',
    ),
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => '0',
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        '5-Subject Set Header' => '5-Subject Set Header',
        'Family Entertainment Header' => 'Family Entertainment Header',
        'General Header' => 'General Header',
        'Homepage Featured CTA' => 'Homepage Featured CTA',
        'Horizons Header' => 'Horizons Header',
        'LIFEPAC Header' => 'LIFEPAC Header',
        'Menu Featured CTA Left' => 'Menu Featured CTA Left',
        'Menu Featured CTA Right' => 'Menu Featured CTA Right',
        'Monarch Ad' => 'Monarch Ad',
        'Monarch Header' => 'Monarch Header',
        'Resources Header' => 'Resources Header',
        'Switched-On Schoolhouse Header' => 'Switched-On Schoolhouse Header',
        'Weaver Header' => 'Weaver Header',
      ),
      'allowed_values_function' => '',
    ),
    'storage' => array(
      'active' => '1',
      'details' => array(
        'sql' => array(
          'FIELD_LOAD_CURRENT' => array(
            'field_data_field_content_placement' => array(
              'value' => 'field_content_placement_value',
            ),
          ),
          'FIELD_LOAD_REVISION' => array(
            'field_revision_field_content_placement' => array(
              'value' => 'field_content_placement_value',
            ),
          ),
        ),
      ),
      'module' => 'field_sql_storage',
      'settings' => array(),
      'type' => 'field_sql_storage',
    ),
    'translatable' => '0',
    'type' => 'list_text',
  ),
  'field_instance' => array(
    'bundle' => 'promo_tout',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => '',
    'display' => array(
      'aop_subscription_list' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 7,
      ),
      'product_carousel' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'product_list' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => '',
    'field_name' => 'field_content_placement',
    'label' => 'Content Placement',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'apply_chosen' => '',
      ),
      'type' => 'options_select',
      'weight' => '13',
    ),
  ),
);

$dependencies = array(
  'content_type.promo_tout' => 'content_type.promo_tout',
);

$optional = array();

$modules = array(
  0 => 'field_sql_storage',
  1 => 'list',
  2 => 'options',
);
