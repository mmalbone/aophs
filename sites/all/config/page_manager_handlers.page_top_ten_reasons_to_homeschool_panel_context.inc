<?php
/**
 * @file
 * page_manager_handlers.page_top_ten_reasons_to_homeschool_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_top_ten_reasons_to_homeschool_panel_context';
$handler->task = 'page';
$handler->subtask = 'top_ten_reasons_to_homeschool';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '90ea06db-50d4-40e4-b20b-1e79b48b55d8';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-f4de2148-f93c-4114-84db-ab8ea9221a95';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Top Ten Reasons to Homeschool',
    'body' => '<p>At Alpha Omega Publications®, we believe you\'ll find the following homeschooling benefits convincing reasons to start educating your child at home:</p>

<dl>
  <dt>1. Flexible Schedules</dt>
  <dd>No more crazy mornings getting your child up early and ready for school. Instead, enjoy homeschooling on your time, while your child is refreshed and relaxed.</dd>
  <dt>2. Customizable Curriculum</dt>
  <dd>Forget tedious and boring busywork. Suit the needs and individual interests of your child by letting him pursue subjects and topics that accentuate his talents.</dd>
<dt>3. Individual Attention</dt>
<dd>One-on-one teaching allows your child to learn at the pace he needs to firmly grasp a particular concept.</dd>
<dl>4. Family Togetherness</dl>
<dd>Mutual love, respect, and closeness are enhanced while homeschooling because learning together becomes an extension of the family\'s daily activities.</dd>
<dt>5. Fun-filled Learning</dt>
<dd>Rather than just making a grade, homeschooling allows your child to use the learning style that best suits him to promote a lifelong love of learning.</dd>
<dl>6. Moral and Religious Instruction</dl>
<dd>Christian beliefs and ethics can be incorporated into the curriculum, removing conflicting messages between what\'s taught and what\'s practiced.</dd>
<dt>7. Positive Social Interaction</dt>
<dd>Exposure to multiple age groups enables your child to make a variety of friendships and socialize confidently with others outside his peer group.</dd>
<dt>8. Safety</dt>
<dd>The absence of bullying, gangs, drugs, and dangerous weapons allows your child to learn better within a loving environment.</dd>
<dt>9. Self-confidence</dt>
<dd>Since your child doesn\'t have to worry about being embarrassed in front of peers, homeschooling allows him to be more creative and willing to take risks while learning.</dd>
<dt>10. Life Skills</dt>
<dd>Developing a sense of personal responsibility, your child can learn how to manage a home, handle finances, and better care for himself as an adult.</dd>
</dl>
',
    'format' => 'markdown',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'f4de2148-f93c-4114-84db-ab8ea9221a95';
  $display->content['new-f4de2148-f93c-4114-84db-ab8ea9221a95'] = $pane;
  $display->panels['left'][0] = 'new-f4de2148-f93c-4114-84db-ab8ea9221a95';
  $pane = new stdClass();
  $pane->pid = 'new-fc7dd972-975f-41cc-acb9-bb505d4e8b1b';
  $pane->panel = 'right';
  $pane->type = 'block';
  $pane->subtype = 'block-34';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'fc7dd972-975f-41cc-acb9-bb505d4e8b1b';
  $display->content['new-fc7dd972-975f-41cc-acb9-bb505d4e8b1b'] = $pane;
  $display->panels['right'][0] = 'new-fc7dd972-975f-41cc-acb9-bb505d4e8b1b';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-f4de2148-f93c-4114-84db-ab8ea9221a95';
$handler->conf['display'] = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
