<?php
/**
 * @file
 * facetapi.search_api_grade_level_block_search_api_language.inc
 */

$api = '2.0.0';

$data = $facet = new stdClass();
$facet->disabled = FALSE; /* Edit this to true to make a default facet disabled initially */
$facet->api_version = 1;
$facet->name = 'search_api@grade_level:block:search_api_language';
$facet->searcher = 'search_api@grade_level';
$facet->realm = 'block';
$facet->facet = 'search_api_language';
$facet->enabled = FALSE;
$facet->settings = array(
  'weight' => 0,
  'widget' => 'facetapi_links',
  'filters' => array(),
  'active_sorts' => array(
    'active' => 'active',
    'count' => 'count',
    'display' => 'display',
  ),
  'sort_weight' => array(
    'active' => -50,
    'count' => -49,
    'display' => -48,
  ),
  'sort_order' => array(
    'active' => 3,
    'count' => 3,
    'display' => 4,
  ),
  'empty_behavior' => 'none',
  'soft_limit' => 20,
  'nofollow' => 1,
  'show_expanded' => 0,
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'facetapi',
);
