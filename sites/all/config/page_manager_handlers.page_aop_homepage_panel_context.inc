<?php
/**
 * @file
 * page_manager_handlers.page_aop_homepage_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_aop_homepage_panel_context';
$handler->task = 'page';
$handler->subtask = 'aop_homepage';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => 'aop-homepage',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'flexible:aop_homepage';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'center' => NULL,
    'homepage_hero_left' => NULL,
    'homepage_hero_right' => NULL,
    'homepage_carousel' => NULL,
    'homepage_recent_stories' => NULL,
    'homepage_popular_articles' => NULL,
    'homepage_bottom_grid_item_1' => NULL,
    'homepage_bottom_grid_item_2' => NULL,
    'homepage_bottom_grid_item_3' => NULL,
    'homepage_bottom_grid_item_3_' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '72429591-58b7-4128-a882-b15a9afde02e';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-10431d08-df51-4655-a5b8-32c41ea46511';
  $pane->panel = 'center';
  $pane->type = 'views_panes';
  $pane->subtype = 'promo_touts-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '10431d08-df51-4655-a5b8-32c41ea46511';
  $display->content['new-10431d08-df51-4655-a5b8-32c41ea46511'] = $pane;
  $display->panels['center'][0] = 'new-10431d08-df51-4655-a5b8-32c41ea46511';
  $pane = new stdClass();
  $pane->pid = 'new-d4497f03-6299-4c07-833a-407c9a52d0c1';
  $pane->panel = 'homepage_bottom_grid_item_1';
  $pane->type = 'block';
  $pane->subtype = 'block-17';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => 'We want to hear from you',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'homepage-cta-1',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'd4497f03-6299-4c07-833a-407c9a52d0c1';
  $display->content['new-d4497f03-6299-4c07-833a-407c9a52d0c1'] = $pane;
  $display->panels['homepage_bottom_grid_item_1'][0] = 'new-d4497f03-6299-4c07-833a-407c9a52d0c1';
  $pane = new stdClass();
  $pane->pid = 'new-64e4b180-c58c-495d-8954-f0500a89f001';
  $pane->panel = 'homepage_bottom_grid_item_2';
  $pane->type = 'block';
  $pane->subtype = 'block-18';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'homepage-cta-2',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '64e4b180-c58c-495d-8954-f0500a89f001';
  $display->content['new-64e4b180-c58c-495d-8954-f0500a89f001'] = $pane;
  $display->panels['homepage_bottom_grid_item_2'][0] = 'new-64e4b180-c58c-495d-8954-f0500a89f001';
  $pane = new stdClass();
  $pane->pid = 'new-844baacc-4d46-4f6e-a091-29d89884e1c1';
  $pane->panel = 'homepage_bottom_grid_item_3';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => '',
    'body' => '<div class="aop-academy-promo-wrapper"><div class="aop-academy-promo-image"><img alt="" class="file-default media-element" src="https://glnnewmedia.s3.amazonaws.com/AOA-logo_0.gif"></div><div class="aop-academy-promo-text"><h2>Alpha Omega Academy</h2><p>Enroll your student in a fully accredited, online learning academy with the support of Christian teachers and advisors.</p><p><a class="button-blue-background" href="http://www.aopacademy.com" target="_blank" title="Visit ALPHA OMEGA ACADEMY"><em>Learn</em> MORE</a></p></div></div>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'homepage-cta-3',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '844baacc-4d46-4f6e-a091-29d89884e1c1';
  $display->content['new-844baacc-4d46-4f6e-a091-29d89884e1c1'] = $pane;
  $display->panels['homepage_bottom_grid_item_3'][0] = 'new-844baacc-4d46-4f6e-a091-29d89884e1c1';
  $pane = new stdClass();
  $pane->pid = 'new-badd55b5-ff40-4f19-b3d1-77008d8218d0';
  $pane->panel = 'homepage_bottom_grid_item_3_';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => '',
    'body' => '<div class="aop-christian-schools-wrapper"><div class="aop-christian-schools-image"><img alt="" class="file-default media-element" src="https://glnnewmedia.s3.amazonaws.com/christianschool.png"></div><div class="aop-christian-schools-text"><h2>Christian Schools</h2><p>Whether you are an existing Christian school or starting a new one, AOP’s curriculum can build a Christian worldview in your students.</p><p><a class="button-blue-background" href="http://www.aopschools.com" target="_blank" title="Visit AOP SCHOOL"><em>Visit</em> AOP SCHOOLS</a></p></div></div><p>&nbsp;</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'homepage-cta-4',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'badd55b5-ff40-4f19-b3d1-77008d8218d0';
  $display->content['new-badd55b5-ff40-4f19-b3d1-77008d8218d0'] = $pane;
  $display->panels['homepage_bottom_grid_item_3_'][0] = 'new-badd55b5-ff40-4f19-b3d1-77008d8218d0';
  $pane = new stdClass();
  $pane->pid = 'new-5f9fa7a2-a3bb-4234-a74d-573f4e4af513';
  $pane->panel = 'homepage_carousel';
  $pane->type = 'views_panes';
  $pane->subtype = 'home_carousel_curriculums-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '5f9fa7a2-a3bb-4234-a74d-573f4e4af513';
  $display->content['new-5f9fa7a2-a3bb-4234-a74d-573f4e4af513'] = $pane;
  $display->panels['homepage_carousel'][0] = 'new-5f9fa7a2-a3bb-4234-a74d-573f4e4af513';
  $pane = new stdClass();
  $pane->pid = 'new-10569f9c-5fc8-48d6-b379-bbf802bdb130';
  $pane->panel = 'homepage_hero_left';
  $pane->type = 'block';
  $pane->subtype = 'block-30';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '10569f9c-5fc8-48d6-b379-bbf802bdb130';
  $display->content['new-10569f9c-5fc8-48d6-b379-bbf802bdb130'] = $pane;
  $display->panels['homepage_hero_left'][0] = 'new-10569f9c-5fc8-48d6-b379-bbf802bdb130';
  $pane = new stdClass();
  $pane->pid = 'new-15516f5b-60ee-47ff-bd55-37fdb5130f77';
  $pane->panel = 'homepage_hero_right';
  $pane->type = 'block';
  $pane->subtype = 'block-29';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '15516f5b-60ee-47ff-bd55-37fdb5130f77';
  $display->content['new-15516f5b-60ee-47ff-bd55-37fdb5130f77'] = $pane;
  $display->panels['homepage_hero_right'][0] = 'new-15516f5b-60ee-47ff-bd55-37fdb5130f77';
  $pane = new stdClass();
  $pane->pid = 'new-547b29b1-19ed-4bc7-9651-79d18e0adadf';
  $pane->panel = 'homepage_popular_articles';
  $pane->type = 'views_panes';
  $pane->subtype = 'aop_blog-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '547b29b1-19ed-4bc7-9651-79d18e0adadf';
  $display->content['new-547b29b1-19ed-4bc7-9651-79d18e0adadf'] = $pane;
  $display->panels['homepage_popular_articles'][0] = 'new-547b29b1-19ed-4bc7-9651-79d18e0adadf';
  $pane = new stdClass();
  $pane->pid = 'new-d51d145a-fb16-4cd9-832d-66126ac7b7b9';
  $pane->panel = 'homepage_recent_stories';
  $pane->type = 'views_panes';
  $pane->subtype = 'aop_blog-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'd51d145a-fb16-4cd9-832d-66126ac7b7b9';
  $display->content['new-d51d145a-fb16-4cd9-832d-66126ac7b7b9'] = $pane;
  $display->panels['homepage_recent_stories'][0] = 'new-d51d145a-fb16-4cd9-832d-66126ac7b7b9';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = '0';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.promo_touts' => 'views_view.promo_touts',
  'views_view.home_carousel_curriculums' => 'views_view.home_carousel_curriculums',
  'views_view.aop_blog' => 'views_view.aop_blog',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
