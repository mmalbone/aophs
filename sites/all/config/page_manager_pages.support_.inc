<?php
/**
 * @file
 * page_manager_pages.support_.inc
 */

$api = '2.0.0';

$data = $page = new stdClass();
$page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
$page->api_version = 1;
$page->name = 'support_';
$page->task = 'page';
$page->admin_title = 'Support ';
$page->admin_description = 'Support page';
$page->path = 'support';
$page->access = array();
$page->menu = array();
$page->arguments = array();
$page->conf = array(
  'admin_paths' => FALSE,
);


$dependencies = array(
  'page_manager_handlers.page_support__panel_context' => 'page_manager_handlers.page_support__panel_context',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
