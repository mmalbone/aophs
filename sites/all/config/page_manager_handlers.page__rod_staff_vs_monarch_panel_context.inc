<?php
/**
 * @file
 * page_manager_handlers.page__rod_staff_vs_monarch_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page__rod_staff_vs_monarch_panel_context';
$handler->task = 'page';
$handler->subtask = '_rod_staff_vs_monarch';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'eb8674c2-e9cf-42a1-81ab-9e0bf11218ad';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-569ce30f-bf37-4ed2-95f0-1de5fe043ded';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Rod & Staff vs. Monarch',
    'body' => '<p>Are you ready to compare the curriculum option offered by Rod &amp; Staff Curriculum to the online Monarch program from Alpha Omega Publications? You\'re in the right spot. Find a short overview of each curriculum and service below, an easy-to-view comparison chart, and additional comparison options!</p><h3><strong>Rod and Staff Curriculum</strong></h3><p>Rod and Staff Publishers, Inc. is a Mennonite publishing company which supplies Christian homes and schools with Bible-based textbooks and literature useful for academic instruction and guidance in Christian discipline. Rod and Staff publishes curriculum designed for a traditional method of instruction that encourages strong thinking and communication skills. Textbooks, workbooks, worksheets, tests, and teacher materials are available for preschool through 10th grade. Coursework includes Bible, reading, English, arithmetic, spelling, science, health, history/geography, penmanship, music, art, typing, and special education.</p><p>Designed to instill biblical values and standards, all Rod and Staff curriculum is steeped in biblical truth. Rod and Staff does not sell kindergarten curriculum. As an alternative to kindergarten, Rod and Staff provides a series of preschool resources designed to prepare young students for school both in attitude and action. Parents who choose Rod and Staff are responsible for all administrative tasks, including all required lesson planning, grading, record-keeping, and reporting. Because the Rod and Staff teacher\'s manuals are designed for the beginning teacher (or parent), they are easily used by anyone who desires to homeschool their children. Telephone consultations are available for Rod and Staff customers. Curriculum can be purchased in grade level kits or as individual components.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>As you consider your homeschool curriculum options, remember there is no one perfect homeschool curriculum. A blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Instead of feeling torn between different curricula, parents should be open to mixing and matching materials and resources to customize their child\'s education. Remember, each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3><strong>Monarch from Alpha Omega Publications</strong></h3><p>Monarch from Alpha Omega Publications is an interactive, Internet-based curriculum for grades 3-12. Alpha Omega Publications, a trusted publisher of high quality educational products, provides Christian curriculum and educational services to homeschool families. Transforming the way students learn, Monarch is an in-depth, online curriculum that makes use of today\'s 21st century technology. This innovative, cutting-edge AOP curriculum presents Christ-centered lessons using eye-catching animations, learning games, video and audio clips, and more! Unlike traditional textbooks, Monarch has a diverse mix of text-based instruction and engaging multimedia enrichment. Time-saving teaching tools provide automatic grading, lesson planning, customizable lessons, a built-in calendar, printing options, message center, text-to-speech, and more!</p><p>Compatible with both Windows® and Macintosh® operating systems, Monarch integrates Scripture and a biblical worldview into all subjects. Monarch offers the flexibility to log on and learn anytime, anywhere with 18-month, single subscriptions using an individualized, student driven approach. Diagnostic tests are available for placement in math and language arts. Core coursework includes Bible (3-12), language arts (3-12), math (3-12), science (3-12), and history and geography (3-12). Over 35 electives are also available. Monarch may be purchased as individual subjects or as complete five-subject sets.</p><h3><strong>Benefits and Features of Monarch from Alpha Omega Publications</strong></h3><p class="list_heading">Monarch</p><ul><li>online interactive lessons with anytime, anywhere access</li><li>compatible with both Windows® and Mac® operating systems</li><li>offers customizable student learning options and time-saving teaching tools</li><li>features dynamic, media-rich lessons; real-time content updates; and data storage</li></ul><h3 class="para-start"><strong>Take a Closer Look at Rod and Staff and Monarch</strong></h3>

<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">Monarch</th><th class="compare">Rod and Staff</th></tr>
</thead>
<tbody>
<tr>
<td>K-12 curriculum options</td>
<td class="tac">3-12th grade</td>
<td class="tac">Preschool-10th grade</td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Teacher-intensive curriculum</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Internet-based curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Interactive, engaging multimedia tools</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Customizable lessons and calendar</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Placement tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Automatic grading and lesson planning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Automatic updated material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Flexible and individualized</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Mastery-based learning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr><th class="features">Cost</th><th class="compare">Monarch</th><th class="compare">Rod and Staff</th></tr>
</thead>
<tbody>
<tr>
<td>Average yearly cost per grade level**</td>
<td class="tac">$449.95</td>
<td class="tac">$228.40</td>
</tr>
<tr class="alt">
<td>Average subject cost per grade level**</td>
<td class="tac">$99.95</td>
<td class="tac">$45.60</td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: Rod and Staff Curriculum versus Alpha Omega Publications</h4>
<ul class="comp_list">
<li><a href="/rod-and-staff-vs-monarch">Rod and Staff Curriculum vs. Monarch</a></li>
<li><a href="/rod-and-staff-vs-aoa">Rod and Staff Curriculum vs. Alpha Omega Academy</a></li>
<li><a href="/rod-and-staff-vs-horizons">Rod and Staff Curriculum vs. Horizons</a></li>
<li><a href="/rod-and-staff-vs-lifepac">Rod and Staff Curriculum vs. LIFEPAC</a></li>
<li><a href="/rod-and-staff-vs-sos">Rod and Staff Curriculum vs. Switched-On Schoolhouse</a></li>
</ul>
<p>*Rod and Staff Publishers, Inc. is a Christian Publisher of textbooks and literature. Alpha Omega Publications is not in any way affiliated with Rod and Staff. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by Rod and Staff.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in March 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p><p>**Prices obtained March 2013 from the Milestone Ministries website, independent vendor of Rod &amp; Staff materials, and from Alpha Omega Publications 2013 Homeschool Catalog. Yearly prices based on a comparison between the Monarch 6th Grade 5-Subject Set and the Rod &amp; Staff 6th Grade Basic Packaged Program. Individual subject prices are based on comparison between the Monarch 6th grade math and Rod &amp; Staff 6th Grade Math Set. Costs do not reflect any additional fees or shipping and handling charges. Prices for other Rod &amp; Staff and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '569ce30f-bf37-4ed2-95f0-1de5fe043ded';
  $display->content['new-569ce30f-bf37-4ed2-95f0-1de5fe043ded'] = $pane;
  $display->panels['left'][0] = 'new-569ce30f-bf37-4ed2-95f0-1de5fe043ded';
  $pane = new stdClass();
  $pane->pid = 'new-1287271e-ef19-499d-ad1e-c08713e944d2';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '1287271e-ef19-499d-ad1e-c08713e944d2';
  $display->content['new-1287271e-ef19-499d-ad1e-c08713e944d2'] = $pane;
  $display->panels['right'][0] = 'new-1287271e-ef19-499d-ad1e-c08713e944d2';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-569ce30f-bf37-4ed2-95f0-1de5fe043ded';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
