<?php
/**
 * @file
 * permission.rate_content.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'rate content',
  'roles' => array(
    0 => 'administrator',
    1 => 'authenticated user',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'fivestar',
);
