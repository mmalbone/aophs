<?php
/**
 * @file
 * services_endpoint.aop_subscriptions.inc
 */

$api = '2.0.0';

$data = $endpoint = new stdClass();
$endpoint->disabled = FALSE; /* Edit this to true to make a default endpoint disabled initially */
$endpoint->api_version = 3;
$endpoint->name = 'aop_subscriptions';
$endpoint->server = 'rest_server';
$endpoint->path = 'subscriptions';
$endpoint->authentication = array(
  'services_oauth' => array(
    'oauth_context' => 'mule',
    'authorization' => 'muleoauth',
    'credentials' => 'consumer',
  ),
);
$endpoint->server_settings = array(
  'formatters' => array(
    'bencode' => TRUE,
    'json' => TRUE,
    'php' => TRUE,
    'xml' => TRUE,
    'jsonp' => FALSE,
  ),
  'parsers' => array(
    'application/json' => TRUE,
    'application/vnd.php.serialized' => TRUE,
    'application/x-www-form-urlencoded' => TRUE,
    'application/xml' => TRUE,
    'multipart/form-data' => TRUE,
    'text/xml' => TRUE,
  ),
);
$endpoint->resources = array(
  'aop_subscriptions_services' => array(
    'operations' => array(
      'retrieve' => array(
        'enabled' => '1',
        'settings' => array(
          'services_oauth' => array(
            'credentials' => 'consumer',
            'authorization' => 'muleoauth',
          ),
        ),
      ),
      'update' => array(
        'enabled' => '1',
        'settings' => array(
          'services_oauth' => array(
            'credentials' => 'consumer',
            'authorization' => 'muleoauth',
          ),
        ),
      ),
    ),
  ),
  'order' => array(
    'operations' => array(
      'retrieve' => array(
        'enabled' => '1',
        'settings' => array(
          'services_oauth' => array(
            'credentials' => 'consumer',
            'authorization' => 'muleoauth',
          ),
        ),
      ),
      'update' => array(
        'enabled' => '1',
        'settings' => array(
          'services_oauth' => array(
            'credentials' => 'consumer',
            'authorization' => 'muleoauth',
          ),
        ),
      ),
    ),
  ),
  'user' => array(
    'operations' => array(
      'retrieve' => array(
        'enabled' => '1',
        'settings' => array(
          'services_oauth' => array(
            'credentials' => 'consumer',
            'authorization' => 'muleoauth',
          ),
        ),
      ),
      'update' => array(
        'enabled' => '1',
        'settings' => array(
          'services_oauth' => array(
            'credentials' => 'consumer',
            'authorization' => 'muleoauth',
          ),
        ),
      ),
    ),
  ),
);
$endpoint->debug = 1;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'services',
);
