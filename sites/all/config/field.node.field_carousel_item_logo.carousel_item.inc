<?php
/**
 * @file
 * field.node.field_carousel_item_logo.carousel_item.inc
 */

$api = '2.0.0';

$data = array(
  'field_config' => array(
    'active' => '1',
    'cardinality' => '1',
    'columns' => array(
      'alt' => array(
        'description' => 'Alternative image text, for the image\'s \'alt\' attribute.',
        'length' => 512,
        'not null' => FALSE,
        'type' => 'varchar',
      ),
      'fid' => array(
        'description' => 'The {file_managed}.fid being referenced in this field.',
        'not null' => FALSE,
        'type' => 'int',
        'unsigned' => TRUE,
      ),
      'height' => array(
        'description' => 'The height of the image in pixels.',
        'type' => 'int',
        'unsigned' => TRUE,
      ),
      'title' => array(
        'description' => 'Image title text, for the image\'s \'title\' attribute.',
        'length' => 1024,
        'not null' => FALSE,
        'type' => 'varchar',
      ),
      'width' => array(
        'description' => 'The width of the image in pixels.',
        'type' => 'int',
        'unsigned' => TRUE,
      ),
    ),
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_carousel_item_logo',
    'foreign keys' => array(
      'fid' => array(
        'columns' => array(
          'fid' => 'fid',
        ),
        'table' => 'file_managed',
      ),
    ),
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'locked' => '0',
    'module' => 'image',
    'settings' => array(
      'default_image' => 0,
      'uri_scheme' => 's3',
    ),
    'storage' => array(
      'active' => '1',
      'details' => array(
        'sql' => array(
          'FIELD_LOAD_CURRENT' => array(
            'field_data_field_carousel_item_logo' => array(
              'alt' => 'field_carousel_item_logo_alt',
              'fid' => 'field_carousel_item_logo_fid',
              'height' => 'field_carousel_item_logo_height',
              'title' => 'field_carousel_item_logo_title',
              'width' => 'field_carousel_item_logo_width',
            ),
          ),
          'FIELD_LOAD_REVISION' => array(
            'field_revision_field_carousel_item_logo' => array(
              'alt' => 'field_carousel_item_logo_alt',
              'fid' => 'field_carousel_item_logo_fid',
              'height' => 'field_carousel_item_logo_height',
              'title' => 'field_carousel_item_logo_title',
              'width' => 'field_carousel_item_logo_width',
            ),
          ),
        ),
      ),
      'module' => 'field_sql_storage',
      'settings' => array(),
      'type' => 'field_sql_storage',
    ),
    'translatable' => '0',
    'type' => 'image',
  ),
  'field_instance' => array(
    'bundle' => 'carousel_item',
    'deleted' => '0',
    'description' => 'Upload the logo of the carousel item. 400px x 400 px',
    'display' => array(
      'aop_subscription_list' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 4,
      ),
      'product_carousel' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'product_list' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => '',
    'field_name' => 'field_carousel_item_logo',
    'label' => 'carousel item logo',
    'required' => 1,
    'settings' => array(
      'alt_field' => 0,
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '600x600',
      'media_library_include_in_library' => 0,
      'min_resolution' => '200x200',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'media',
      'settings' => array(
        'allowed_schemes' => array(
          'private' => 0,
          'public' => 0,
          's3' => 's3',
        ),
        'allowed_types' => array(
          'audio' => 0,
          'document' => 0,
          'image' => 'image',
          'video' => 0,
        ),
        'browser_plugins' => array(
          'media_default--media_browser_1' => 0,
          'media_default--media_browser_2' => 0,
          'media_default--media_browser_my_files' => 0,
          'media_internet' => 0,
          'upload' => 'upload',
        ),
        'progress_indicator' => 'throbber',
      ),
      'type' => 'media_generic',
      'weight' => '34',
    ),
  ),
);

$dependencies = array(
  'content_type.carousel_item' => 'content_type.carousel_item',
);

$optional = array();

$modules = array(
  0 => 'field_sql_storage',
  1 => 'image',
  2 => 'media',
);
