<?php
/**
 * @file
 * page_manager_handlers.page__k12_virtual_schools_vs_alpha_omega_academy_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page__k12_virtual_schools_vs_alpha_omega_academy_panel_context';
$handler->task = 'page';
$handler->subtask = '_k12_virtual_schools_vs_alpha_omega_academy';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '77093aac-87a7-489a-b273-68453e89c711';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-b5cf6f4c-ef54-489e-ac7a-283e131d3173';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => ' K12 Virtual Schools vs. Alpha Omega Academy',
    'body' => '<p>Do you need to compare the online virtual school programs offered by K12 to the programs offered by Alpha Omega Academy? You\'re in the right spot. Find a short overview of each curriculum and service below, an easy-to-view comparison chart, and additional comparison options!</p><h3><strong>K12 Virtual Schools</strong></h3><p>K12 is a large secular provider of kindergarten through 12th grade online curriculum currently being used in virtual public schools, an international online academy, as well as in homeschools worldwide. Because K12 is secular curriculum provider, parents from various faiths and cultural backgrounds may find K12 very appropriate for their families\' homeschool needs. Through its Virtual School programs, K12 offers kindergarten through 12th grade curriculum to families who desire to teach their children at home but prefer a public school program. The K12 Virtual school program is a partnership with local public schools systems providing a high quality online education to children in their homes.</p><p>The K12 curriculum covers six core subjects: language arts/English, math, science, history, art, and music and is based on time-tested and research-based methods of instruction. Students enrolled in a virtual school take their academic courses online with support from a certified public school teacher. The parent acts as a learning coach helping to keep the student successfully engaged in the learning process. Because they are part of the public school system, K12 Virtual Schools are tuition free to all students. This also means that K12 Virtual School families are 100% accountable to the public school system in which they are enrolled. K12 Virtual Schools are not available in all areas.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>Trying to decide between a secular online homeschool curriculum and a Christian distance learning academy for your child? Keep in mind that there is no one perfect answer for homeschooling. Giving the flexibility of using multiple resources instead of one set curriculum, a blended approach to homeschooling is often chosen by many parents. Instead of feeling torn between curriculum and an academy, parents should be open to mixing and matching materials and resources to customize their child\'s education. Each child is unique and has his own learning style. It\'s important to choose materials and educational services that best fit the needs of the individual child and of the homeschool family.</p><h3><strong>Alpha Omega Academy</strong></h3><p>Alpha Omega Academy (AOA) is an accredited distance learning program for homeschool children in grades K-12. Fully accredited by NCA CASI, AOA offers an at-home education with the convenience of distance learning. Providing qualified teachers, administrative services, and multiple curriculum choices, AOA has enrolled thousands of students from all 50 states and over 42 countries. Great for homeschoolers who are looking for the assurance of accredited courses, AOA offers a high school diploma program and the security of course credits that are readily transferred to other schools and programs. AOA provides families with flexibility in scheduling and completion of assigned coursework. Students must complete coursework within ten months of the start date.</p><p>Alpha Omega Academy allows families to choose between Alpha Omega Publications\' print-based or computer-based formats. Both curriculum options can be perfectly customized to fit your child\'s learning needs and educational goals. Both options provide student-paced lessons that can be completed independently with parental supervision. The print-based program requires a parent to grade daily work. All AOP curriculum promotes a solid, Christian worldview. Core courses include Bible, language arts, math, science, and history and geography. Elementary and high school electives are also available. AOA offers both college-prep and general studies diploma tracks. AOA offers a complete line of over 140 academically challenging courses for students in grades K-12. Placement testing, summer school options, academic support, and achievement testing are all available. Full-time or part-time enrollment is offered. A National Honor Society membership (by invitation) and high school graduation ceremony are also available to AOA students.</p><h3><strong>Benefits and Features of Alpha Omega Academy</strong></h3><p class="list_heading">Alpha Omega Academy</p><ul><li>accredited K-12 distance learning program for homeschoolers</li><li>flexible, at-home learning environment with the convenience of distance learning</li><li>choice of print- or computer-based, Christ-centered curriculum options</li><li>complete high school graduation program</li></ul><h3 class="para-start"><strong>Take a Closer Look at K12 Virtual Schools and Alpha Omega Academy</strong></h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">Alpha Omega Academy</th><th class="compare">K12 Virtual Schools</th></tr>
</thead>
<tbody>
<tr>
<td>K-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Student-directed curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Computer-based or print-based curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Fully accredited courses</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Convenient distance learning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Placement tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>High school graduation program</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Flexible and individualized</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Mastery-based learning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Direct teacher/student interaction</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Helpful academic support</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
</tbody>
</table>
</div>
<h4><strong>Compare Offerings: K12 versus Alpha Omega Publications</strong></h4>
<p class="list_heading_links">K12 Independent Consumer Direct</p>
<ul>
<li><a href="/k12-independent-vs-monarch">K12 Independent Consumer Direct vs. Monarch</a></li>
<li><a href="/k12-independent-vs-aoa">K12 Independent Consumer Direct vs. Alpha Omega Academy</a></li>
<li><a href="/k12-independent-vs-horizons">K12 Independent Consumer Direct vs. Horizons</a></li>
<li><a href="/k12-independent-vs-lifepac">K12 Independent Consumer Direct vs. LIFEPAC</a></li>
<li><a href="/k12-independent-vs-sos">K12 Independent Consumer Direct vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">K12 Teacher-Supported Consumer Direct</p>
<ul>
<li><a href="/k12-teacher-supported-vs-monarch">K12 Teacher-Supported Consumer Direct vs. Monarch</a></li>
<li><a href="/k12-teacher-supported-vs-aoa">K12 Teacher-Supported Consumer Direct vs. Alpha Omega Academy</a></li>
<li><a href="/k12-teacher-supported-vs-horizons">K12 Teacher-Supported Consumer Direct vs. Horizons</a></li>
<li><a href="/k12-teacher-supported-vs-lifepac">K12 Teacher-Supported Consumer Direct vs. LIFEPAC</a></li>
<li><a href="/k12-teacher-supported-vs-sos">K12 Teacher-Supported Consumer Direct vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">K12 Virtual Schools</p>
<ul>
<li><a href="/k12-virtual-schools-vs-monarch">K12 Virtual Schools vs. Monarch</a></li>
<li><a href="/k12-virtual-schools-vs-aoa">K12 Virtual Schools vs. Alpha Omega Academy</a></li>
<li><a href="/k12-virtual-schools-vs-horizons">K12 Virtual Schools vs. Horizons</a></li>
<li><a href="/k12-virtual-schools-vs-lifepac">K12 Virtual Schools vs. LIFEPAC</a></li>
<li><a href="/k12-virtual-schools-vs-sos">K12 Virtual Schools vs. Switched-On Schoolhouse</a></li>
</ul>

<p>* K12 is the registered trademark of K12 Inc. Alpha Omega Publications is not in any way affiliated with K12. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by K12.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'b5cf6f4c-ef54-489e-ac7a-283e131d3173';
  $display->content['new-b5cf6f4c-ef54-489e-ac7a-283e131d3173'] = $pane;
  $display->panels['left'][0] = 'new-b5cf6f4c-ef54-489e-ac7a-283e131d3173';
  $pane = new stdClass();
  $pane->pid = 'new-7b28eb62-b8ec-4f9f-9db6-3a315d30a6b8';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '7b28eb62-b8ec-4f9f-9db6-3a315d30a6b8';
  $display->content['new-7b28eb62-b8ec-4f9f-9db6-3a315d30a6b8'] = $pane;
  $display->panels['right'][0] = 'new-7b28eb62-b8ec-4f9f-9db6-3a315d30a6b8';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-b5cf6f4c-ef54-489e-ac7a-283e131d3173';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
