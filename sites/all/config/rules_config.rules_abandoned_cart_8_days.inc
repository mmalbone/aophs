<?php
/**
 * @file
 * rules_config.rules_abandoned_cart_8_days.inc
 */

$api = '2.0.0';

$data = entity_import('rules_config', '{ "rules_abandoned_cart_8_days" : {
      "LABEL" : "Abandoned Cart 8 days",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "abandoned", "autoresponder", "cart", "email" ],
      "REQUIRES" : [ "rules" ],
      "ON" : { "cron" : [] },
      "IF" : [
        { "data_is" : {
            "data" : [ "site:current-user:roles" ],
            "value" : { "value" : { "2" : "2" } }
          }
        },
        { "data_is" : { "data" : [ "site:current-cart-order:status" ], "value" : "cart" } },
        { "AND" : [
            { "data_is" : {
                "data" : [ "site:current-date" ],
                "op" : "\\u003E",
                "value" : {
                  "select" : "site:current-cart-order:changed",
                  "date_offset" : { "value" : 691200 }
                }
              }
            },
            { "data_is" : {
                "data" : [ "site:current-date" ],
                "op" : "\\u003C",
                "value" : {
                  "select" : "site:current-cart-order:changed",
                  "date_offset" : { "value" : 777600 }
                }
              }
            }
          ]
        }
      ],
      "DO" : []
    }
  }');

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'entity',
  1 => 'rules',
);
