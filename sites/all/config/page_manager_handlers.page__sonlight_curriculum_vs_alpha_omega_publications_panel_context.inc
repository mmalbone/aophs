<?php
/**
 * @file
 * page_manager_handlers.page__sonlight_curriculum_vs_alpha_omega_publications_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page__sonlight_curriculum_vs_alpha_omega_publications_panel_context';
$handler->task = 'page';
$handler->subtask = '_sonlight_curriculum_vs_alpha_omega_publications';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '68338636-5b7e-468a-ba5c-ced513a2a790';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-68307352-450d-498a-bc3c-3fe473c54e39';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Sonlight Curriculum vs. Alpha Omega Publications',
    'body' => '<p>Thinking about homeschooling with the literature-based approach of Sonlight Curriculum? Wondering what the differences are between Sonlight and Alpha Omega Publications? You\'re in the right place. Many parents just like you are also searching for a homeschool curriculum and teaching approach that best fits their child\'s academic needs. With so many homeschooling options, resources, and curricula to choose from, selecting homeschool materials for your child can often be a daunting task, but we\'re here to help!</p><h3><strong>Sonlight\'s Approach</strong></h3><p>Sonlight is a Christian company that provides literature-based homeschool courses for students in K-12th grades. Sonlight provides complete curriculum packages, resources and materials designed to instill a love of learning in the homeschooled student. The Sonlight Curriculum consists of collections of literature designed to provide everything parents need to teach one child for an entire year. Sonlight\'s Core programs integrate history, geography, and literature into a coordinated whole. The literature-based approach is based on the premise that children will learn and retain knowledge as they assimilate relevant literature. Designed to instill a Christian worldview, all Sonlight curriculum utilizes literature, both fiction and non-fiction, to provide all academic content. Literature-based instruction encourages student-teacher interaction that focuses on learning together rather than on students being lectured by teachers. Discussion, questions, and critical thinking are all important aspects of the literature-based approach.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>Most parents discover that their child\'s educational needs are best met by blending several different homeschool curriculums and teaching approaches. Because there is no one perfect homeschool curriculum, a blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Instead of feeling torn between curricula, parents should be open to mixing and matching materials and resources to customize their child\'s education. Remember, each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3><strong>Curriculum from Alpha Omega Publications</strong></h3><p>Alpha Omega Publications is a Christian-based, PreK-12 publisher that provides engaging, interactive curriculum and educational services to homeschool families. Offering proven, easy-to-teach homeschool curriculum, AOP offers four main curriculum offerings: Internet-based Monarch (3-12), computer-based Switched-On Schoolhouse (3-12), worktext-based LIFEPAC (K-12), and workbook-based Horizons (PreK-12). Main core subjects offered include Bible, language arts, math, science, and history and geography, along with various electives. AOP also offers Alpha Omega Academy, a distance learning academy for grades K-12. Understanding that each child learns differently, AOP offers diverse curricula and services in different formats to fit multiple learning styles, ensuring you can find what fits your child\'s needs.</p><h3><strong>Benefits and Features of Alpha Omega Publications\' Curriculum</strong></h3><p class="list_heading">Monarch</p><ul><li>online interactive lessons with anytime, anywhere access</li><li>compatible with both Windows® and Mac® operating systems</li><li>offers customizable student learning options and time-saving teaching tools</li><li>features dynamic, media-rich lessons; real-time content updates; and data storage</li></ul><p class="list_heading">Switched-On Schoolhouse</p><ul><li>interactive, computer-based curriculum</li><li>offers a flexible, engaging learning environment</li><li>contains multimedia tools, educational games, and automatic grading options</li><li>can reduce teacher planning and grading by 20 hours per week</li></ul><p class="list_heading">LIFEPAC</p><ul><li>full-color, print-based curriculum</li><li>offers progressive, self-paced individual unit worktexts</li><li>promotes concept mastery and cognitive reasoning skills</li><li>encourages academic independence in your children</li></ul><p class="list_heading">Horizons</p><ul><li>brightly illustrated, hands-on, consumable curriculum</li><li>offers fun lessons and manipulative-based activities</li><li>teaches concepts through a spiral learning process</li><li>encourages concept mastery through quick concept introduction, review, and reinforcement&nbsp;</li></ul><h3 class="para-start"><strong>Take a Closer Look at Sonlight and Alpha Omega Publications</strong></h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">Alpha Omega Publications</th><th class="compare">Sonlight Curriculum</th></tr>
</thead>
<tbody>
<tr>
<td>PreK-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Flexible and individualized curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Customizable curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Teacher intensive curriculum</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Placement tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Interactive lesson material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Automatic curriculum updates available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Automatic grading and lesson planning available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Appeals to students with multiple learning styles</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr><th class="features">Cost</th><th class="compare">Alpha Omega Publications</th><th class="compare">Sonlight</th></tr>
</thead>
<tbody>
<tr>
<td>Average yearly cost per grade level**</td>
<td class="tac">$449.95 for Monarch</td>
<td class="tac">$869</td>
</tr>
<tr class="alt">
<td>Average subject cost per grade level**</td>
<td class="tac">$99.95 for Monarch</td>
<td class="tac">$227.78</td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: Sonlight Curriculum versus Alpha Omega Publications</h4>
<ul class="comp_list">
<li><a href="/sonlight-vs-monarch">Sonlight vs. Monarch</a></li>
<li><a href="/sonlight-vs-aoa">Sonlight vs. Alpha Omega Academy</a></li>
<li><a href="/sonlight-vs-horizons">Sonlight vs. Horizons</a></li>
<li><a href="/sonlight-vs-lifepac">Sonlight vs. LIFEPAC</a></li>
<li><a href="/sonlight-vs-sos">Sonlight vs. Switched-On Schoolhouse</a></li>
</ul>
<p>*Sonlight® is the registered trademark of Sonlight Curriculum, LTD. Alpha Omega Publications is not in any way affiliated with Sonlight Curriculum. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by Sonlight.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p><p>**Prices obtained February 2013 from the Sonlight website and from Alpha Omega Publications 2013 Homeschool Catalog. Yearly prices based on comparison between the Monarch 3rd Grade Complete grade level set and the complete Sonlight 3rd grade Multi-subject Package, both of which include all required student and teacher materials for a single 3rd grader. Individual subject prices are based on comparison between the Monarch 3rd grade single subject cost and Sonlight 3rd grade science kit D costs. Costs do not reflect any additional fees or shipping and handling charges. Prices for other Sonlight and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '68307352-450d-498a-bc3c-3fe473c54e39';
  $display->content['new-68307352-450d-498a-bc3c-3fe473c54e39'] = $pane;
  $display->panels['left'][0] = 'new-68307352-450d-498a-bc3c-3fe473c54e39';
  $pane = new stdClass();
  $pane->pid = 'new-d78e3380-9844-493a-8615-575af6be2603';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'd78e3380-9844-493a-8615-575af6be2603';
  $display->content['new-d78e3380-9844-493a-8615-575af6be2603'] = $pane;
  $display->panels['right'][0] = 'new-d78e3380-9844-493a-8615-575af6be2603';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-68307352-450d-498a-bc3c-3fe473c54e39';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
