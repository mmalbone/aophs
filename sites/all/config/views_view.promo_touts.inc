<?php
/**
 * @file
 * views_view.promo_touts.inc
 */

$api = '2.0.0';

$data = $view = new view();
$view->name = 'promo_touts';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Promo Touts';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '1';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* Field: Content: Promo Tout Link */
$handler->display->display_options['fields']['field_promo_tout_link']['id'] = 'field_promo_tout_link';
$handler->display->display_options['fields']['field_promo_tout_link']['table'] = 'field_data_field_promo_tout_link';
$handler->display->display_options['fields']['field_promo_tout_link']['field'] = 'field_promo_tout_link';
$handler->display->display_options['fields']['field_promo_tout_link']['label'] = '';
$handler->display->display_options['fields']['field_promo_tout_link']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_promo_tout_link']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_promo_tout_link']['click_sort_column'] = 'url';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['title']['alter']['path'] = '[field_promo_tout_link]';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'promo_tout' => 'promo_tout',
);
/* Filter criterion: Content: Content Placement (field_content_placement) */
$handler->display->display_options['filters']['field_content_placement_value']['id'] = 'field_content_placement_value';
$handler->display->display_options['filters']['field_content_placement_value']['table'] = 'field_data_field_content_placement';
$handler->display->display_options['filters']['field_content_placement_value']['field'] = 'field_content_placement_value';
$handler->display->display_options['filters']['field_content_placement_value']['value'] = array(
  'Homepage Featured CTA' => 'Homepage Featured CTA',
);

/* Display: Homepage Featured CTA */
$handler = $view->new_display('panel_pane', 'Homepage Featured CTA', 'panel_pane_1');

/* Display: Block */
$handler = $view->new_display('block', 'Block', 'block_1');

/* Display: Product Listing Page Promo Touts */
$handler = $view->new_display('panel_pane', 'Product Listing Page Promo Touts', 'product_list_page_generic_banner');
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Promo Tout Link */
$handler->display->display_options['fields']['field_promo_tout_link']['id'] = 'field_promo_tout_link';
$handler->display->display_options['fields']['field_promo_tout_link']['table'] = 'field_data_field_promo_tout_link';
$handler->display->display_options['fields']['field_promo_tout_link']['field'] = 'field_promo_tout_link';
$handler->display->display_options['fields']['field_promo_tout_link']['label'] = '';
$handler->display->display_options['fields']['field_promo_tout_link']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_promo_tout_link']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_promo_tout_link']['click_sort_column'] = 'url';
$handler->display->display_options['fields']['field_promo_tout_link']['type'] = 'link_plain';
/* Field: Content: Promo Tout Featured Image */
$handler->display->display_options['fields']['field_promo_tout_featured_image']['id'] = 'field_promo_tout_featured_image';
$handler->display->display_options['fields']['field_promo_tout_featured_image']['table'] = 'field_data_field_promo_tout_featured_image';
$handler->display->display_options['fields']['field_promo_tout_featured_image']['field'] = 'field_promo_tout_featured_image';
$handler->display->display_options['fields']['field_promo_tout_featured_image']['label'] = '';
$handler->display->display_options['fields']['field_promo_tout_featured_image']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_promo_tout_featured_image']['alter']['text'] = '<a href=[field_promo_tout_link]>[field_promo_tout_featured_image]</a>';
$handler->display->display_options['fields']['field_promo_tout_featured_image']['alter']['path'] = '[field_promo_tout_link]';
$handler->display->display_options['fields']['field_promo_tout_featured_image']['element_type'] = '0';
$handler->display->display_options['fields']['field_promo_tout_featured_image']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_promo_tout_featured_image']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_promo_tout_featured_image']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_promo_tout_featured_image']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_promo_tout_featured_image']['settings'] = array(
  'image_style' => '',
  'image_link' => '',
);
$handler->display->display_options['fields']['field_promo_tout_featured_image']['field_api_classes'] = TRUE;
$handler->display->display_options['defaults']['arguments'] = FALSE;
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'promo_tout' => 'promo_tout',
);
/* Filter criterion: Content: Has taxonomy term */
$handler->display->display_options['filters']['tid']['id'] = 'tid';
$handler->display->display_options['filters']['tid']['table'] = 'taxonomy_index';
$handler->display->display_options['filters']['tid']['field'] = 'tid';
$handler->display->display_options['filters']['tid']['value'] = array(
  760 => '760',
);
$handler->display->display_options['filters']['tid']['type'] = 'select';
$handler->display->display_options['filters']['tid']['vocabulary'] = 'content_placement';
$handler->display->display_options['filters']['tid']['hierarchy'] = 1;

/* Display: Product List Headers */
$handler = $view->new_display('panel_pane', 'Product List Headers', 'product_list_headers');
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Promo Tout Link */
$handler->display->display_options['fields']['field_promo_tout_link']['id'] = 'field_promo_tout_link';
$handler->display->display_options['fields']['field_promo_tout_link']['table'] = 'field_data_field_promo_tout_link';
$handler->display->display_options['fields']['field_promo_tout_link']['field'] = 'field_promo_tout_link';
$handler->display->display_options['fields']['field_promo_tout_link']['label'] = '';
$handler->display->display_options['fields']['field_promo_tout_link']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_promo_tout_link']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_promo_tout_link']['click_sort_column'] = 'url';
/* Field: Content: Promo Tout Featured Image */
$handler->display->display_options['fields']['field_promo_tout_featured_image']['id'] = 'field_promo_tout_featured_image';
$handler->display->display_options['fields']['field_promo_tout_featured_image']['table'] = 'field_data_field_promo_tout_featured_image';
$handler->display->display_options['fields']['field_promo_tout_featured_image']['field'] = 'field_promo_tout_featured_image';
$handler->display->display_options['fields']['field_promo_tout_featured_image']['label'] = '';
$handler->display->display_options['fields']['field_promo_tout_featured_image']['element_type'] = '0';
$handler->display->display_options['fields']['field_promo_tout_featured_image']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_promo_tout_featured_image']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_promo_tout_featured_image']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_promo_tout_featured_image']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_promo_tout_featured_image']['settings'] = array(
  'image_style' => '',
  'image_link' => '',
);
$handler->display->display_options['fields']['field_promo_tout_featured_image']['field_api_classes'] = TRUE;
/* Field: Content: Promo Display Title */
$handler->display->display_options['fields']['field_promo_display_title']['id'] = 'field_promo_display_title';
$handler->display->display_options['fields']['field_promo_display_title']['table'] = 'field_data_field_promo_display_title';
$handler->display->display_options['fields']['field_promo_display_title']['field'] = 'field_promo_display_title';
$handler->display->display_options['fields']['field_promo_display_title']['label'] = '';
$handler->display->display_options['fields']['field_promo_display_title']['alter']['strip_tags'] = TRUE;
$handler->display->display_options['fields']['field_promo_display_title']['element_type'] = 'span';
$handler->display->display_options['fields']['field_promo_display_title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_promo_display_title']['element_wrapper_type'] = 'h1';
$handler->display->display_options['fields']['field_promo_display_title']['element_wrapper_class'] = 'product_banner_title';
$handler->display->display_options['fields']['field_promo_display_title']['element_default_classes'] = FALSE;
/* Field: Content: Promo Tout Background Image */
$handler->display->display_options['fields']['field_promo_tout_background_imag']['id'] = 'field_promo_tout_background_imag';
$handler->display->display_options['fields']['field_promo_tout_background_imag']['table'] = 'field_data_field_promo_tout_background_imag';
$handler->display->display_options['fields']['field_promo_tout_background_imag']['field'] = 'field_promo_tout_background_imag';
$handler->display->display_options['fields']['field_promo_tout_background_imag']['label'] = '';
$handler->display->display_options['fields']['field_promo_tout_background_imag']['element_type'] = '0';
$handler->display->display_options['fields']['field_promo_tout_background_imag']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_promo_tout_background_imag']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_promo_tout_background_imag']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_promo_tout_background_imag']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_promo_tout_background_imag']['settings'] = array(
  'image_style' => '',
  'image_link' => '',
);
$handler->display->display_options['fields']['field_promo_tout_background_imag']['field_api_classes'] = TRUE;
/* Field: Content: Promo Tout Text */
$handler->display->display_options['fields']['field_promo_tout_text']['id'] = 'field_promo_tout_text';
$handler->display->display_options['fields']['field_promo_tout_text']['table'] = 'field_data_field_promo_tout_text';
$handler->display->display_options['fields']['field_promo_tout_text']['field'] = 'field_promo_tout_text';
$handler->display->display_options['fields']['field_promo_tout_text']['label'] = '';
$handler->display->display_options['fields']['field_promo_tout_text']['alter']['strip_tags'] = TRUE;
$handler->display->display_options['fields']['field_promo_tout_text']['element_type'] = 'div';
$handler->display->display_options['fields']['field_promo_tout_text']['element_class'] = 'product_banner_short_description';
$handler->display->display_options['fields']['field_promo_tout_text']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_promo_tout_text']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_promo_tout_text']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_promo_tout_text']['field_api_classes'] = TRUE;
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Content: Content Placement (field_content_placement) */
$handler->display->display_options['arguments']['field_content_placement_value']['id'] = 'field_content_placement_value';
$handler->display->display_options['arguments']['field_content_placement_value']['table'] = 'field_data_field_content_placement';
$handler->display->display_options['arguments']['field_content_placement_value']['field'] = 'field_content_placement_value';
$handler->display->display_options['arguments']['field_content_placement_value']['default_action'] = 'not found';
$handler->display->display_options['arguments']['field_content_placement_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_content_placement_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_content_placement_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_content_placement_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_content_placement_value']['limit'] = '0';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'promo_tout' => 'promo_tout',
);

/* Display: Marketing Product Listing Tout */
$handler = $view->new_display('panel_pane', 'Marketing Product Listing Tout', 'marketing_product_listing_tout');
$handler->display->display_options['display_description'] = 'Marketing Content Area for Product Listing Page';
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['style_options']['default_row_class'] = FALSE;
$handler->display->display_options['style_options']['row_class_special'] = FALSE;
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['row_options']['default_field_elements'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Promo Tout Link */
$handler->display->display_options['fields']['field_promo_tout_link']['id'] = 'field_promo_tout_link';
$handler->display->display_options['fields']['field_promo_tout_link']['table'] = 'field_data_field_promo_tout_link';
$handler->display->display_options['fields']['field_promo_tout_link']['field'] = 'field_promo_tout_link';
$handler->display->display_options['fields']['field_promo_tout_link']['label'] = '';
$handler->display->display_options['fields']['field_promo_tout_link']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_promo_tout_link']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_promo_tout_link']['click_sort_column'] = 'url';
/* Field: Content: Promo Tout Featured Image */
$handler->display->display_options['fields']['field_promo_tout_featured_image']['id'] = 'field_promo_tout_featured_image';
$handler->display->display_options['fields']['field_promo_tout_featured_image']['table'] = 'field_data_field_promo_tout_featured_image';
$handler->display->display_options['fields']['field_promo_tout_featured_image']['field'] = 'field_promo_tout_featured_image';
$handler->display->display_options['fields']['field_promo_tout_featured_image']['label'] = '';
$handler->display->display_options['fields']['field_promo_tout_featured_image']['element_type'] = '0';
$handler->display->display_options['fields']['field_promo_tout_featured_image']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_promo_tout_featured_image']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_promo_tout_featured_image']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_promo_tout_featured_image']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_promo_tout_featured_image']['settings'] = array(
  'image_style' => '',
  'image_link' => '',
);
$handler->display->display_options['fields']['field_promo_tout_featured_image']['field_api_classes'] = TRUE;
/* Field: Content: Promo Display Title */
$handler->display->display_options['fields']['field_promo_display_title']['id'] = 'field_promo_display_title';
$handler->display->display_options['fields']['field_promo_display_title']['table'] = 'field_data_field_promo_display_title';
$handler->display->display_options['fields']['field_promo_display_title']['field'] = 'field_promo_display_title';
$handler->display->display_options['fields']['field_promo_display_title']['label'] = '';
$handler->display->display_options['fields']['field_promo_display_title']['alter']['strip_tags'] = TRUE;
$handler->display->display_options['fields']['field_promo_display_title']['element_type'] = 'span';
$handler->display->display_options['fields']['field_promo_display_title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_promo_display_title']['element_wrapper_type'] = 'h1';
$handler->display->display_options['fields']['field_promo_display_title']['element_wrapper_class'] = 'product_banner_title';
$handler->display->display_options['fields']['field_promo_display_title']['element_default_classes'] = FALSE;
/* Field: Content: Promo Tout Background Image */
$handler->display->display_options['fields']['field_promo_tout_background_imag']['id'] = 'field_promo_tout_background_imag';
$handler->display->display_options['fields']['field_promo_tout_background_imag']['table'] = 'field_data_field_promo_tout_background_imag';
$handler->display->display_options['fields']['field_promo_tout_background_imag']['field'] = 'field_promo_tout_background_imag';
$handler->display->display_options['fields']['field_promo_tout_background_imag']['label'] = '';
$handler->display->display_options['fields']['field_promo_tout_background_imag']['element_type'] = '0';
$handler->display->display_options['fields']['field_promo_tout_background_imag']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_promo_tout_background_imag']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_promo_tout_background_imag']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_promo_tout_background_imag']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_promo_tout_background_imag']['settings'] = array(
  'image_style' => '',
  'image_link' => '',
);
$handler->display->display_options['fields']['field_promo_tout_background_imag']['field_api_classes'] = TRUE;
/* Field: Content: Promo Tout Text */
$handler->display->display_options['fields']['field_promo_tout_text']['id'] = 'field_promo_tout_text';
$handler->display->display_options['fields']['field_promo_tout_text']['table'] = 'field_data_field_promo_tout_text';
$handler->display->display_options['fields']['field_promo_tout_text']['field'] = 'field_promo_tout_text';
$handler->display->display_options['fields']['field_promo_tout_text']['label'] = '';
$handler->display->display_options['fields']['field_promo_tout_text']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_promo_tout_text']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_promo_tout_text']['settings'] = array(
  'trim_length' => 600,
);
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Content: Content Placement (field_content_placement) */
$handler->display->display_options['arguments']['field_content_placement_value']['id'] = 'field_content_placement_value';
$handler->display->display_options['arguments']['field_content_placement_value']['table'] = 'field_data_field_content_placement';
$handler->display->display_options['arguments']['field_content_placement_value']['field'] = 'field_content_placement_value';
$handler->display->display_options['arguments']['field_content_placement_value']['default_action'] = 'not found';
$handler->display->display_options['arguments']['field_content_placement_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_content_placement_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_content_placement_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_content_placement_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_content_placement_value']['limit'] = '0';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'promo_tout' => 'promo_tout',
);

/* Display: Menu Featured CTA Left */
$handler = $view->new_display('block', 'Menu Featured CTA Left', 'menu_featured_cta');
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'promo_tout' => 'promo_tout',
);
/* Filter criterion: Content: Content Placement (field_content_placement) */
$handler->display->display_options['filters']['field_content_placement_value']['id'] = 'field_content_placement_value';
$handler->display->display_options['filters']['field_content_placement_value']['table'] = 'field_data_field_content_placement';
$handler->display->display_options['filters']['field_content_placement_value']['field'] = 'field_content_placement_value';
$handler->display->display_options['filters']['field_content_placement_value']['value'] = array(
  'Menu Featured CTA Left' => 'Menu Featured CTA Left',
);

/* Display: Menu Featured CTA Right */
$handler = $view->new_display('block', 'Menu Featured CTA Right', 'block_2');
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'promo_tout' => 'promo_tout',
);
/* Filter criterion: Content: Content Placement (field_content_placement) */
$handler->display->display_options['filters']['field_content_placement_value']['id'] = 'field_content_placement_value';
$handler->display->display_options['filters']['field_content_placement_value']['table'] = 'field_data_field_content_placement';
$handler->display->display_options['filters']['field_content_placement_value']['field'] = 'field_content_placement_value';
$handler->display->display_options['filters']['field_content_placement_value']['value'] = array(
  'Menu Featured CTA Right' => 'Menu Featured CTA Right',
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'views',
  1 => 'node',
  2 => 'link',
  3 => 'views_content',
  4 => 'image',
  5 => 'text',
);
