<?php
/**
 * @file
 * panels_layout.aop_75_25_bottom_50_50_landing_pages.inc
 */

$api = '2.0.0';

$data = $layout = new stdClass();
$layout->disabled = FALSE; /* Edit this to true to make a default layout disabled initially */
$layout->api_version = 1;
$layout->name = 'aop_75_25_bottom_50_50_landing_pages';
$layout->admin_title = 'AOP 75-25 + bottom 50-50 Landing Pages';
$layout->admin_description = '';
$layout->category = 'AOP';
$layout->plugin = 'flexible';
$layout->settings = array(
  'items' => array(
    'canvas' => array(
      'type' => 'row',
      'contains' => 'column',
      'children' => array(
        0 => 'main',
      ),
      'parent' => NULL,
      'class' => '',
      'column_class' => '',
      'row_class' => '',
      'region_class' => '',
      'no_scale' => TRUE,
      'fixed_width' => '',
      'column_separation' => '0.5em',
      'region_separation' => '0.5em',
      'row_separation' => '0.5em',
    ),
    'main' => array(
      'type' => 'column',
      'width' => 100,
      'width_type' => '%',
      'children' => array(
        0 => 1,
        1 => 'main-row',
        2 => 2,
      ),
      'parent' => 'canvas',
    ),
    'main-row' => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'center',
        1 => 'right_25',
      ),
      'parent' => 'main',
    ),
    'center' => array(
      'type' => 'region',
      'title' => 'Left 70',
      'width' => '69.99652234394019',
      'width_type' => '%',
      'parent' => 'main-row',
      'class' => 'left-70',
    ),
    'right_25' => array(
      'type' => 'region',
      'title' => 'Right 30',
      'width' => '30.003477656059818',
      'width_type' => '%',
      'parent' => 'main-row',
      'class' => 'right-30',
    ),
    1 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'full_width_landing_page_banner',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    'full_width_landing_page_banner' => array(
      'type' => 'region',
      'title' => 'Full width Landing Page Banner',
      'width' => 100,
      'width_type' => '%',
      'parent' => '1',
      'class' => 'landing-page-banner',
    ),
    2 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'bottom_left',
        1 => 'bottom_right',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    'bottom_left' => array(
      'type' => 'region',
      'title' => 'Bottom Left',
      'width' => 50,
      'width_type' => '%',
      'parent' => '2',
      'class' => 'left-50',
    ),
    'bottom_right' => array(
      'type' => 'region',
      'title' => 'Bottom Right',
      'width' => 50,
      'width_type' => '%',
      'parent' => '2',
      'class' => 'right-50',
    ),
  ),
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'panels',
);
