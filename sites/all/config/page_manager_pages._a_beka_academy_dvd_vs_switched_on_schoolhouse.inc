<?php
/**
 * @file
 * page_manager_pages._a_beka_academy_dvd_vs_switched_on_schoolhouse.inc
 */

$api = '2.0.0';

$data = $page = new stdClass();
$page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
$page->api_version = 1;
$page->name = '_a_beka_academy_dvd_vs_switched_on_schoolhouse';
$page->task = 'page';
$page->admin_title = ' A Beka Academy DVD vs. Switched-On Schoolhouse';
$page->admin_description = 'abeka-dvd-vs-sos';
$page->path = 'abeka-dvd-vs-sos';
$page->access = array();
$page->menu = array();
$page->arguments = array();
$page->conf = array(
  'admin_paths' => FALSE,
);


$dependencies = array(
  'page_manager_handlers.page__a_beka_academy_dvd_vs_switched_on_schoolhouse_panel_context' => 'page_manager_handlers.page__a_beka_academy_dvd_vs_switched_on_schoolhouse_panel_context',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
