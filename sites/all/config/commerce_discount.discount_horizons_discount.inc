<?php
/**
 * @file
 * commerce_discount.discount_horizons_discount.inc
 */

$api = '2.0.0';

$data = entity_import('commerce_discount', '{
    "name" : "discount_horizons_discount",
    "label" : "Horizons Discount",
    "type" : "product_discount",
    "component_title" : "Horizons Discount",
    "export_status" : "1",
    "commerce_discount_date" : [],
    "commerce_discount_offer" : {
      "type" : "fixed_amount",
      "commerce_fixed_amount" : { "und" : [
          {
            "amount" : "1000",
            "currency_code" : "USD",
            "data" : { "components" : [] }
          }
        ]
      }
    },
    "inline_conditions" : { "und" : [
        {
          "condition_name" : "commerce_product_has_specified_terms",
          "condition_settings" : {
            "terms" : [ { "target_id" : "75" } ],
            "condition_logic_operator" : null
          },
          "condition_negate" : 0
        }
      ]
    },
    "discount_usage_per_person" : { "und" : [ { "value" : "10" } ] },
    "discount_usage_limit" : []
  }');

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'entity',
  1 => 'commerce_discount',
);
