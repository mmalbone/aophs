<?php
/**
 * @file
 * page_manager_handlers.page_seven_steps_to_start_homeschooling_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_seven_steps_to_start_homeschooling_panel_context';
$handler->task = 'page';
$handler->subtask = 'seven_steps_to_start_homeschooling';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '340bd35f-a269-47ab-854a-dc8921557c3b';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-e774f58d-15b8-49bd-a00d-beae8f7ad4fe';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Seven Steps to Start Homeschooling',
    'title' => 'Seven Steps to Start Homeschooling',
    'body' => '<p>If you\'ve been thinking about homeschooling but aren\'t quite sure how to begin, we have good news! Because Alpha Omega Publications cares about first-time homeschoolers like you, we\'ve developed "Seven Steps to Start Homeschooling" to walk you through the homeschooling process. From choosing the perfect curriculum for your child to organizing a daily homeschooling schedule, you\'ll learn the basics to make your first year of homeschooling successful and fun. Does that sound like just what you need?</p>

<p>To get the answers you need to start your homeschooling journey, simply choose a topic(s) of interest to you and click on the link(s) below.</p>

<ol>
	<li>Make the Decision — Are you ready to start? Learn what homeschooling is, how it benefits your family, what qualifications you need to homeschool, and how homeschooling is a lifestyle.</li>
	<li>Get Informed — Cover your bases! Discover the facts you need to know to homeschool legally in your state, the everyday ins and outs of homeschooling, and case studies that prove homeschool success.</li>
	<li>Find Support — Gain strength in numbers! Get the information you need to build an effective support base with other homeschool families, homeschool co-ops, state homeschool organizations, homeschool forums, and social networking sites.</li>
	<li>Select a Method — How does one teach? Identify which homeschooling approach is best for your family as you explore popular homeschool options such as computer-based, traditional, unit study, Charlotte Mason, Montessori, classical, and more!</li>
	<li>Choose Your Curriculum — What should you use? Find out what subjects to teach, where to find homeschool curriculum, how to know what grade level is right for your child, and if your children\'s learning style is hands on, auditory, or visual.</li>
	<li>Schedule Your Day — Fit everything in – schooling, laundry, shopping, cooking, and cleaning. Find practical guidelines to plan and organize your homeschooling day with "Twelve Secrets to Scheduling Success!"</li>
	<li>Keep the Right Records — Put it in print! Learn how to document with report cards, monthly planners, transcripts, and homeschooling portfolios in order to show your child\'s progress and prove he is learning.</li>
</ol>
',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'e774f58d-15b8-49bd-a00d-beae8f7ad4fe';
  $display->content['new-e774f58d-15b8-49bd-a00d-beae8f7ad4fe'] = $pane;
  $display->panels['middle'][0] = 'new-e774f58d-15b8-49bd-a00d-beae8f7ad4fe';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-e774f58d-15b8-49bd-a00d-beae8f7ad4fe';
$handler->conf['display'] = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
