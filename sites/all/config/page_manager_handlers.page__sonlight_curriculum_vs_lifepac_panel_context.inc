<?php
/**
 * @file
 * page_manager_handlers.page__sonlight_curriculum_vs_lifepac_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page__sonlight_curriculum_vs_lifepac_panel_context';
$handler->task = 'page';
$handler->subtask = '_sonlight_curriculum_vs_lifepac';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '9c0fb1ec-5a3e-4897-ac62-5119a1819e02';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-ae183ce7-ec66-49a5-b16c-2915d99d0b79';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Sonlight Curriculum vs. LIFEPAC',
    'body' => '<p>Do you need to compare the literature-based curriculum offered by Sonlight to the LIFEPAC program from Alpha Omega Publications? You\'re in the right spot. Find a short overview of each curriculum and service below, an easy-to-view comparison chart, and additional comparison options!</p><h3><strong>Sonlight Curriculum</strong></h3><p>Sonlight is a Christian company that provides literature-based homeschool courses for students in K-12th grades. Sonlight provides complete curriculum packages, resources, and materials designed to instill a love of learning in the homeschooled student. The Sonlight Curriculum consists of collections of literature designed to provide everything parents need to teach one child for an entire year. Sonlight\'s Core programs integrate history, geography, and literature into a coordinated whole. In addition, language arts, math, science, and electives can be added to the Core to provide a complete academic course load. Core instructor\'s guides are available to provide scheduling and teaching helps, study guides, answer keys, and more. Parents who use the Sonlight curriculum are responsible for all administrative tasks, including all required lesson planning, grading, record-keeping, and reporting.</p><p>Designed to instill a Christian worldview, all Sonlight curriculum utilizes literature, both fiction and non-fiction, to provide all academic content. Science instruction is based on a biblical view of creation and the origin of life. All Sonlight curriculum coursework can be purchased as Core packages (history, geography, and literature) or as individual subjects (math, science, Bible, language arts, and electives). Kits provide all essential materials for both students and teachers. Optional academic and administrative resources can be purchased separately.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>If you\'re considering a literature-based approach to homeschooling, remember that there is no one perfect homeschool curriculum. A blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Instead of feeling torn between different curricula, parents should be open to mixing and matching materials and resources to customize their child\'s education. Remember, each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3><strong>LIFEPAC from Alpha Omega Publications</strong></h3><p>LIFEPAC is a K-12 print-based curriculum offered by Alpha Omega Publications, a Christian-based publisher that provides curriculum and educational services to Christian schools and homeschool families. Full-color and consumable, LIFEPAC is an integrated, Bible-based curriculum developed by a team of accomplished educators and teachers. Using a mastery-based learning approach, each LIFEPAC course is comprised of ten student-directed worktexts that provide a full year of instruction. Individual worktexts may also be used to supplement the homeschool curriculum or to fill in learning gaps. LIFEPAC worktexts include self tests and unit tests to provide assessment of student progress. Teacher\'s guides offer practical teaching tips, additional learning activities, and complete answer keys.</p><p>An economical, proven homeschool curriculum, LIFEPAC integrates Scripture and a Christian worldview throughout all subjects. LIFEPAC offers the flexibility of an individualized, student-driven learning approach which encourages critical thinking and academic independence. Self-paced coursework for K-12 students includes Bible (1-12), language arts (K-12), math (K-12), science (K-12), and history and geography (1-12). Electives are also available for elementary, middle, and high school students. LIFEPAC subject kits include all required student and teacher materials. LIFEPAC curriculum can be purchased as complete grade level sets or as individual subjects. Student and teacher materials and supplemental materials may be purchased separately.</p><h3><strong>Benefits and Features of LIFEPAC from Alpha Omega Publications</strong>&nbsp;</h3><p class="list_heading">LIFEPAC</p><ul><li>full-color, print-based curriculum</li><li>offers progressive, self-paced individual unit worktexts</li><li>promotes concept mastery and cognitive reasoning skills</li><li>encourages academic independence in your children</li></ul><h3 class="para-start"><strong>Take a Closer Look at Sonlight Curriculum and LIFEPAC</strong></h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">LIFEPAC</th><th class="compare">Sonlight Curriculum</th></tr>
</thead>
<tbody>
<tr>
<td>K-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Accredited program</td>
<td class="tac">&nbsp;</td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Student-directed curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Self-paced and individualized</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Placement tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Economical</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Promotes critical thinking and cognitive reasoning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Manageable amount of daily lesson material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Material designed for homeschoolers</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr><th class="features">Cost</th><th class="compare">Alpha Omega Publications</th><th class="compare">Sonlight</th></tr>
</thead>
<tbody>
<tr>
<td>Average yearly cost per grade level**</td>
<td class="tac">$286.95</td>
<td class="tac">$869</td>
</tr>
<tr class="alt">
<td>Average subject cost per grade level**</td>
<td class="tac">$58.95</td>
<td class="tac">$227.78</td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: Sonlight Curriculum versus Alpha Omega Publications</h4>
<ul class="comp_list">
<li><a href="/sonlight-vs-monarch">Sonlight vs. Monarch</a></li>
<li><a href="/sonlight-vs-aoa">Sonlight vs. Alpha Omega Academy</a></li>
<li><a href="/sonlight-vs-horizons">Sonlight vs. Horizons</a></li>
<li><a href="/sonlight-vs-lifepac">Sonlight vs. LIFEPAC</a></li>
<li><a href="/sonlight-vs-sos">Sonlight vs. Switched-On Schoolhouse</a></li>
</ul>
<p>*Sonlight® is the registered trademark of Sonlight Curriculum, LTD. Alpha Omega Publications is not in any way affiliated with Sonlight Curriculum. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by Sonlight.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p><p>**Prices obtained February 2013 from the Sonlight website and from Alpha Omega Publications 2013 Homeschool Catalog. Yearly prices based on comparison between the LIFEPAC 3rd Grade Complete grade level set and the complete Sonlight 3rd Grade Multi-subject Package, both of which include all required student and teacher materials for a single 3rd grader. Individual subject prices are based on comparison between the LIFEPAC 3rd grade science set cost and Sonlight 3rd grade science kit D costs. Costs do not reflect any additional fees or shipping and handling charges. Prices for other Sonlight and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'ae183ce7-ec66-49a5-b16c-2915d99d0b79';
  $display->content['new-ae183ce7-ec66-49a5-b16c-2915d99d0b79'] = $pane;
  $display->panels['left'][0] = 'new-ae183ce7-ec66-49a5-b16c-2915d99d0b79';
  $pane = new stdClass();
  $pane->pid = 'new-f78f762b-9d1e-495f-9e01-436dcc615577';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'f78f762b-9d1e-495f-9e01-436dcc615577';
  $display->content['new-f78f762b-9d1e-495f-9e01-436dcc615577'] = $pane;
  $display->panels['right'][0] = 'new-f78f762b-9d1e-495f-9e01-436dcc615577';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-ae183ce7-ec66-49a5-b16c-2915d99d0b79';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
