<?php
/**
 * @file
 * permission.create_dvd_content.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'create dvd content',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array(
  'content_type.dvd' => 'content_type.dvd',
);

$optional = array();

$modules = array(
  0 => 'node',
);
