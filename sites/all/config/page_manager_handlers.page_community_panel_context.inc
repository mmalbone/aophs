<?php
/**
 * @file
 * page_manager_handlers.page_community_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_community_panel_context';
$handler->task = 'page';
$handler->subtask = 'community';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'e671043d-5279-41d3-aa83-66b7554daa97';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-ee5b44e5-6ada-4f49-aaf6-e3ea417cc880';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Find Local Homeschool Support Groups',
    'body' => '<p>Alpha Omega Publications® strongly encourages homeschoolers to participate in local homeschool support groups! What is a homeschool support group? Support groups may be clubs, co-operatives, or associations that have their own, unique characteristics. By attending and interacting in your local homeschool group, you and your children can:</p><ul class="bullets"><li>Meet new people and make new friends</li><li>Share experiences, information, and ideas</li><li>Participate in group activities like sports, field trips, and volunteer projects</li><li>Gain support and encouragement from networking</li><li>Strengthen and enrich your homeschooling experience</li></ul><p>You may need to research and attend several groups before you find one that\'s a good fit for your family. Please note that Alpha Omega Publications does not endorse groups listed on this site, nor verify accuracy of submitted information.</p><p>Ready to find a homeschool support group near you?&nbsp; Enter your zip code below and click on the apply button to locate support groups near you, or click <a href="community/organizations-by-state">here</a> to see a list by state.&nbsp; To add an organization to our list click <a href="node/19019">here</a>.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'ee5b44e5-6ada-4f49-aaf6-e3ea417cc880';
  $display->content['new-ee5b44e5-6ada-4f49-aaf6-e3ea417cc880'] = $pane;
  $display->panels['middle'][0] = 'new-ee5b44e5-6ada-4f49-aaf6-e3ea417cc880';
  $pane = new stdClass();
  $pane->pid = 'new-9d5b2eb7-1f2e-46a7-ae20-a87005fedaf0';
  $pane->panel = 'middle';
  $pane->type = 'views_panes';
  $pane->subtype = 'organization-panel_pane_3';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '9d5b2eb7-1f2e-46a7-ae20-a87005fedaf0';
  $display->content['new-9d5b2eb7-1f2e-46a7-ae20-a87005fedaf0'] = $pane;
  $display->panels['middle'][1] = 'new-9d5b2eb7-1f2e-46a7-ae20-a87005fedaf0';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-ee5b44e5-6ada-4f49-aaf6-e3ea417cc880';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.organization' => 'views_view.organization',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
