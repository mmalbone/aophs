<?php
/**
 * @file
 * views_view.awards_links.inc
 */

$api = '2.0.0';

$data = $view = new view();
$view->name = 'awards_links';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'taxonomy_term_data';
$view->human_name = 'Awards Links';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['content'] = 'Awards & Recognition';
$handler->display->display_options['header']['area']['format'] = 'plain_text';
/* Field: Taxonomy term: Awards Links */
$handler->display->display_options['fields']['field_awards_links']['id'] = 'field_awards_links';
$handler->display->display_options['fields']['field_awards_links']['table'] = 'field_data_field_awards_links';
$handler->display->display_options['fields']['field_awards_links']['field'] = 'field_awards_links';
$handler->display->display_options['fields']['field_awards_links']['label'] = '';
$handler->display->display_options['fields']['field_awards_links']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_awards_links']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_awards_links']['click_sort_column'] = 'url';
$handler->display->display_options['fields']['field_awards_links']['type'] = 'link_plain';
/* Field: Taxonomy term: Awards Badges */
$handler->display->display_options['fields']['field_awards_badges']['id'] = 'field_awards_badges';
$handler->display->display_options['fields']['field_awards_badges']['table'] = 'field_data_field_awards_badges';
$handler->display->display_options['fields']['field_awards_badges']['field'] = 'field_awards_badges';
$handler->display->display_options['fields']['field_awards_badges']['label'] = '';
$handler->display->display_options['fields']['field_awards_badges']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['field_awards_badges']['alter']['path'] = '[field_awards_links]';
$handler->display->display_options['fields']['field_awards_badges']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_awards_badges']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_awards_badges']['settings'] = array(
  'image_style' => '',
  'image_link' => '',
);

/* Display: Curriculum Awards Pane */
$handler = $view->new_display('panel_pane', 'Curriculum Awards Pane', 'panel_pane_1');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['defaults']['relationships'] = FALSE;
/* Relationship: Taxonomy term: Awards (field_awards) */
$handler->display->display_options['relationships']['field_awards_tid']['id'] = 'field_awards_tid';
$handler->display->display_options['relationships']['field_awards_tid']['table'] = 'field_data_field_awards';
$handler->display->display_options['relationships']['field_awards_tid']['field'] = 'field_awards_tid';
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Taxonomy term: Awards Links */
$handler->display->display_options['fields']['field_awards_links']['id'] = 'field_awards_links';
$handler->display->display_options['fields']['field_awards_links']['table'] = 'field_data_field_awards_links';
$handler->display->display_options['fields']['field_awards_links']['field'] = 'field_awards_links';
$handler->display->display_options['fields']['field_awards_links']['relationship'] = 'field_awards_tid';
$handler->display->display_options['fields']['field_awards_links']['label'] = '';
$handler->display->display_options['fields']['field_awards_links']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_awards_links']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_awards_links']['click_sort_column'] = 'url';
$handler->display->display_options['fields']['field_awards_links']['type'] = 'link_plain';
/* Field: Taxonomy term: Awards Badges */
$handler->display->display_options['fields']['field_awards_badges']['id'] = 'field_awards_badges';
$handler->display->display_options['fields']['field_awards_badges']['table'] = 'field_data_field_awards_badges';
$handler->display->display_options['fields']['field_awards_badges']['field'] = 'field_awards_badges';
$handler->display->display_options['fields']['field_awards_badges']['relationship'] = 'field_awards_tid';
$handler->display->display_options['fields']['field_awards_badges']['label'] = '';
$handler->display->display_options['fields']['field_awards_badges']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['field_awards_badges']['alter']['path'] = '[field_awards_links]';
$handler->display->display_options['fields']['field_awards_badges']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_awards_badges']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_awards_badges']['settings'] = array(
  'image_style' => '',
  'image_link' => '',
);
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Taxonomy term: Term ID */
$handler->display->display_options['arguments']['tid']['id'] = 'tid';
$handler->display->display_options['arguments']['tid']['table'] = 'taxonomy_term_data';
$handler->display->display_options['arguments']['tid']['field'] = 'tid';
$handler->display->display_options['arguments']['tid']['default_action'] = 'default';
$handler->display->display_options['arguments']['tid']['default_argument_type'] = 'taxonomy_tid';
$handler->display->display_options['arguments']['tid']['default_argument_options']['node'] = TRUE;
$handler->display->display_options['arguments']['tid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['tid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['tid']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['argument_input'] = array(
  'tid' => array(
    'type' => 'panel',
    'context' => 'entity:comment.author',
    'context_optional' => 0,
    'panel' => '0',
    'fixed' => '',
    'label' => 'Taxonomy term: Term ID',
  ),
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'views',
  1 => 'taxonomy',
  2 => 'link',
  3 => 'image',
  4 => 'views_content',
);
