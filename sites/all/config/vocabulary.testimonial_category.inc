<?php
/**
 * @file
 * vocabulary.testimonial_category.inc
 */

$api = '2.0.0';

$data = (object) array(
  'description' => 'Testimonial Category',
  'hierarchy' => '0',
  'machine_name' => 'testimonial_category',
  'module' => 'taxonomy',
  'name' => 'Testimonial Category',
  'vid' => '15',
  'weight' => '0',
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'taxonomy',
);
