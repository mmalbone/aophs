<?php
/**
 * @file
 * page_manager_handlers.page__time4learning_vs_alpha_omega_academy_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page__time4learning_vs_alpha_omega_academy_panel_context';
$handler->task = 'page';
$handler->subtask = '_time4learning_vs_alpha_omega_academy';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'top' => NULL,
    'left' => NULL,
    'middle' => NULL,
    'right' => NULL,
    'bottom' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'cb0eed1f-e514-46b5-8334-3701039e68aa';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-d7b7bc68-e1a9-4d75-8e88-1dc11f4973ef';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Time4Learning vs. Alpha Omega Academy',
    'body' => '<p>Would you like to learn more about how the online learning program offered by Time4Learning compares to Alpha Omega Academy? You\'re in the right spot. Find a short overview of each curriculum and service below, an easy-to-view comparison chart, and additional comparison options!</p><h3><strong>Time4Learning Online Curriculum</strong></h3><p>Time4Learning, an online educational program for preschool through 8th grade students, provides academic instruction through multimedia, interactive project-based activities, and worksheets. Time4Learning has many appropriate applications including its use as a core curriculum for homeschooled students. Courses include language arts, math, science, and social studies. This student-paced, flexible approach to academic instruction presents engaging interactive lessons in a suggested sequence that correlates to state standards. The math and language arts courses provide comprehensive programs that need no supplementation. The Time4Learning science (1st-6th grade) and social studies (2nd-7th grade) courses may require supplementation in order to satisfy local requirements.</p><p>Because Time4Learning is a secular homeschooling program, parents from various faiths and cultural backgrounds may find Time4Learning appropriate for their families\' homeschool needs. For parents, Time4Learning provides detailed reporting, simple record-keeping, lesson plans, teaching tools, and more. Because Time4Learning is web-based, families have complete flexibility regarding their use of the curriculum. A computer with a high speed internet connection (DSL or cable modem) is all that is needed to provide a quality homeschool experience!</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>Trying to decide what approach to homeschooling will be best for your children? Keep in mind that there is no one perfect answer for homeschooling. Giving the flexibility of using multiple resources instead of one set curriculum, a blended approach to homeschooling is often chosen by many parents. Rather than feeling torn between curriculum and an academy, parents should be open to mixing and matching materials and resources to customize their child\'s education. Each child is unique and has his own learning style. It\'s important to choose materials and educational services that best fit the needs of the individual child and of the homeschool family.</p><h3><strong>Alpha Omega Academy</strong></h3><p>Alpha Omega Academy (AOA) is an accredited distance learning program for homeschool children in grades K-12. Fully accredited by NCA CASI, AOA offers an at-home education with the convenience of distance learning. Providing qualified teachers, administrative services, and multiple curriculum choices, AOA has enrolled thousands of students from all 50 states and over 42 countries. Great for homeschoolers who are looking for the assurance of accredited courses, AOA offers a high school diploma program and the security of course credits that are readily transferred to other schools and programs. AOA provides families with flexibility in scheduling and completion of assigned coursework. Students must complete coursework within ten months of the start date.</p><p>Alpha Omega Academy allows families to choose between Alpha Omega Publications\' print-based or computer-based formats. Both curriculum options can be perfectly customized to fit your child\'s learning needs and educational goals. Both options provide student-paced lessons that can be completed independently with parental supervision. The print-based program requires a parent to grade daily work. All AOP curriculum promotes a solid, Christian worldview. Core courses include Bible, language arts, math, science, and history and geography. Elementary and high school electives are also available. AOA offers both college-prep and general studies diploma tracks. AOA offers a complete line of over 140 academically challenging courses for students in grades K-12. Placement testing, summer school options, academic support, and achievement testing are all available. Full-time or part-time enrollment is offered. A National Honor Society membership (by invitation) and high school graduation ceremony are also available to AOA students.</p><h3><strong>Benefits and Features of Alpha Omega Academy</strong></h3><p class="list_heading">Alpha Omega Academy</p><ul><li>accredited K-12 distance learning program for homeschoolers</li><li>flexible, at-home learning environment with the convenience of distance learning</li><li>choice of print- or computer-based, Christ-centered curriculum options</li><li>complete high school graduation program</li></ul><h3 class="para-start"><strong>Take a Closer Look at Time4Learning and Alpha Omega Academy</strong></h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">Alpha Omega Academy</th><th class="compare">Time4Learning</th></tr>
</thead>
<tbody>
<tr>
<td>K-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">PreK-8th</td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">No electives</td>
</tr>
<tr class="alt">
<td>Student-directed curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Computer-based or print-based curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Fully accredited courses</td>
<td class="tac">&nbsp;</td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Convenient distance learning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Placement tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>High school graduation program</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Flexible and individualized</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td height="40">Mastery-based learning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Direct teacher/student interaction</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Helpful academic support</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: Time4Learning versus Alpha Omega Publications</h4>
<ul class="comp_list">
<li><a href="/time4learning-vs-monarch">Time4Learning vs. Monarch</a></li>
<li><a href="/time4learning-vs-aoa">Time4Learning vs. Alpha Omega Academy</a></li>
<li><a href="/time4learning-vs-horizons">Time4Learning vs. Horizons</a></li>
<li><a href="/time4learning-vs-lifepac">Time4Learning vs. LIFEPAC</a></li>
<li><a href="/time4learning-vs-sos">Time4Learning vs. Switched-On Schoolhouse</a></li>
</ul><p>*Time4Learning® is the exclusive trademark of Time4Learning. Alpha Omega Publications is not in any way affiliated with Time4Learning. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by Time4Learning.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'd7b7bc68-e1a9-4d75-8e88-1dc11f4973ef';
  $display->content['new-d7b7bc68-e1a9-4d75-8e88-1dc11f4973ef'] = $pane;
  $display->panels['left'][0] = 'new-d7b7bc68-e1a9-4d75-8e88-1dc11f4973ef';
  $pane = new stdClass();
  $pane->pid = 'new-1df26ddf-6314-47df-b2ca-6e0a352ae9a1';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '1df26ddf-6314-47df-b2ca-6e0a352ae9a1';
  $display->content['new-1df26ddf-6314-47df-b2ca-6e0a352ae9a1'] = $pane;
  $display->panels['right'][0] = 'new-1df26ddf-6314-47df-b2ca-6e0a352ae9a1';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = '0';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
