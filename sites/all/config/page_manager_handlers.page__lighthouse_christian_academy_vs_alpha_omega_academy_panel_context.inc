<?php
/**
 * @file
 * page_manager_handlers.page__lighthouse_christian_academy_vs_alpha_omega_academy_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page__lighthouse_christian_academy_vs_alpha_omega_academy_panel_context';
$handler->task = 'page';
$handler->subtask = '_lighthouse_christian_academy_vs_alpha_omega_academy';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '7221390f-f4a0-4af4-9026-3c93aa37a1ce';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-7bdbe6bf-5d76-4e1d-9fd9-47b5d8fda31d';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Lighthouse Christian Academy vs. Alpha Omega Academy',
    'body' => '<p>Looking for the differences between the Lighthouse Christian Academy and the Alpha Omega Academy? You\'re in the right spot. Find short overviews of each curriculum below, an easy-to-view comparison chart, and additional comparison options!</p>
<h3><strong>A.C.E.\'s Lighthouse Christian Academy</strong></h3>
<p>Lighthouse Christian Academy (LCA) is the provider of distance learning for Accelerated Christian Education (A.C.E.). Lighthouse Christian Academy is a fully accredited (MSA-CESS and Ai) program designed to assist parents in providing a quality Christian education to homeschooled students. Lighthouse Christian Academy exclusively utilizes the A.C.E. curriculum for students in grades K-12 and provides coursework in all core subjects as well as electives.</p><p>Lighthouse Christian Academy provides its students with diagnostic testing to determine placement within the A.C.E. curriculum program. Academic advising assists families in designing a course of study appropriate for each student\'s interests and goals. Honors, college-prep, general, and vocational courses are offered to all high school students. In addition to diagnostic testing, LCA provides progress reports, transcripts, standardized testing for all enrolled students. LCA provides graduation services and a diploma for eligible seniors. Enrollment and administrative fees include all provided services, but the purchase of A.C.E. curricular materials is the responsibility of the parents. Parents are responsible for supervision of all student coursework, as well as all lesson planning and grading of daily work. Completed coursework is submitted to the Lighthouse Christian Academy office for evaluation and reporting.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>Are you wondering how to decide between different distance learning options? Keep in mind there is no one perfect homeschool curriculum. A blended approach to homeschooling allows parents the flexibility to use multiple resources instead of one set curriculum. Instead of feeling torn between curriculum formats, parents should be open to mixing and matching materials and resources to customize their child\'s education. Remember, each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3><strong>Alpha Omega Academy</strong></h3><p>Alpha Omega Academy (AOA) is an accredited distance learning program for homeschool children in grades K-12. Fully accredited by NCA CASI, AOA offers an at-home education with the convenience of distance learning. Providing qualified teachers, administrative services, and multiple curriculum choices, AOA has enrolled thousands of students from all 50 states and over 42 countries. Great for homeschoolers who are looking for the assurance of accredited courses, AOA offers a high school diploma program and the security of course credits that are readily transferred to other schools and programs. AOA provides families with flexibility in scheduling and completion of assigned coursework. Students must complete coursework within ten months of the start date.</p><p>Alpha Omega Academy allows families to choose between Alpha Omega Publications\' print-based or computer-based formats. Both curriculum options can be perfectly customized to fit your child\'s learning needs and educational goals. Both options provide student-paced lessons that can be completed independently with parental supervision. The print-based program requires a parent to grade daily work. All AOP curriculum promotes a solid, Christian worldview. Core courses include Bible, language arts, math, science, and history and geography. Elementary and high school electives are also available. AOA offers both college-prep and general studies diploma tracks. AOA offers a complete line of over 140 academically challenging courses for students in grades K-12. Placement testing, summer school options, academic support, and achievement testing are all available. Full-time or part-time enrollment is offered. A National Honor Society membership (by invitation) and high school graduation ceremony are also available to AOA students.</p><h3><strong>Benefits and Features of Alpha Omega Academy</strong></h3><p class="list_heading">Alpha Omega Academy</p><ul><li>accredited K-12 distance learning program for homeschoolers</li><li>flexible, at-home learning environment with the convenience of distance learning</li><li>choice of print- or computer-based, Christ-centered curriculum options</li><li>complete high school graduation program</li></ul>
<h3>Take a Closer Look at A.C.E.\'s Lighthouse Christian Academy and Alpha Omega Academy</h3>
<div class="comp_chart">
<table>
<thead>
<tr>
<th class="features">Offerings</th>
<th class="compare">Alpha Omega Academy</th>
<th class="compare">A.C.E. <strong>PACES</strong></th></tr>
</thead>
<tbody>
<tr>
<td>K-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Computer-based or print-based curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Fully accredited courses</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Convenient distance learning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Requires materials to be returned</td>
<td class="tac">&nbsp;</td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>High school graduation program</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Flexible and individualized</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Mastery-based learning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Direct teacher/student interaction</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Helpful academic support</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: A.C.E. versus Alpha Omega Publications</h4>
<p class="list_heading_links">A.C.E. PACE</p>
<ul>
<li><a href="/ace-pace-vs-monarch">A.C.E. PACE vs. Monarch</a></li>
<li><a href="/ace-pace-vs-horizons">A.C.E. PACE vs. Horizons</a></li>
<li><a href="/ace-pace-vs-lifepac">A.C.E. PACE vs. LIFEPAC</a></li>
<li><a href="/ace-pace-vs-aoa">A.C.E. PACE vs. Alpha Omega Academy</a></li>
<li><a href="/ace-pace-vs-sos">A.C.E. PACE vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">A.C.E. Lighthouse Christian Academy</p>
<ul>
<li><a href="/ace-lca-vs-monarch">A.C.E. Lighthouse Christian Academy vs. Monarch</a></li>
<li><a href="/ace-lca-vs-horizons">A.C.E. Lighthouse Christian Academy vs. Horizons</a></li>
<li><a href="/ace-lca-vs-lifepac">A.C.E. Lighthouse Christian Academy vs. LIFEPAC</a></li>
<li><a href="/ace-lca-vs-aoa">A.C.E. Lighthouse Christian Academy vs. Alpha Omega Academy</a></li>
<li><a href="/ace-lca-vs-sos">A.C.E. Lighthouse Christian Academy vs. Switched-On Schoolhouse</a></li>
</ul>

<p>*A.C.E.® and PACE® are the registered trademarks and subsidiaries of A.C.E.®. Alpha Omega Publications is not in any way affiliated with A.C.E.. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by A.C.E.®.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '7bdbe6bf-5d76-4e1d-9fd9-47b5d8fda31d';
  $display->content['new-7bdbe6bf-5d76-4e1d-9fd9-47b5d8fda31d'] = $pane;
  $display->panels['left'][0] = 'new-7bdbe6bf-5d76-4e1d-9fd9-47b5d8fda31d';
  $pane = new stdClass();
  $pane->pid = 'new-13d2d960-3fd0-45da-8130-dbc55ec3e386';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '13d2d960-3fd0-45da-8130-dbc55ec3e386';
  $display->content['new-13d2d960-3fd0-45da-8130-dbc55ec3e386'] = $pane;
  $display->panels['right'][0] = 'new-13d2d960-3fd0-45da-8130-dbc55ec3e386';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = 'new-7bdbe6bf-5d76-4e1d-9fd9-47b5d8fda31d';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
