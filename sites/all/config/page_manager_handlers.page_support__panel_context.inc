<?php
/**
 * @file
 * page_manager_handlers.page_support__panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_support__panel_context';
$handler->task = 'page';
$handler->subtask = 'support_';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => 'aop-support-page',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'brenham_flipped';
$display->layout_settings = array(
  'items' => array(
    'canvas' => array(
      'type' => 'row',
      'contains' => 'column',
      'children' => array(
        0 => 'main',
      ),
      'parent' => NULL,
    ),
    'main' => array(
      'type' => 'column',
      'width' => 100,
      'width_type' => '%',
      'children' => array(
        0 => 'main-row',
        1 => 1,
      ),
      'parent' => 'canvas',
    ),
    'main-row' => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'center',
      ),
      'parent' => 'main',
    ),
    'center' => array(
      'type' => 'region',
      'title' => 'Center',
      'width' => 100,
      'width_type' => '%',
      'parent' => 'main-row',
    ),
    1 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'left',
        1 => 'right',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    'left' => array(
      'type' => 'region',
      'title' => 'Left',
      'width' => '70.04168670835338',
      'width_type' => '%',
      'parent' => '1',
      'class' => '',
    ),
    'right' => array(
      'type' => 'region',
      'title' => 'Right',
      'width' => '29.95831329164662',
      'width_type' => '%',
      'parent' => '1',
      'class' => '',
    ),
  ),
);
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'center' => NULL,
    'left' => NULL,
    'right' => NULL,
    'header' => NULL,
    'sidebar' => NULL,
    'contentmain' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '82188dc7-94b7-431b-983f-0458802abce9';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-c9fcc31b-077a-43e8-812e-dd64d0977d61';
  $pane->panel = 'contentmain';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'AOP Academic Support',
    'body' => '<p>Is your homeschooler struggling to grasp a difficult subject, or are you having trouble explaining a challenging lesson in your Christian homeschool curriculum? &nbsp;Set your mind at ease and schedule an academic support session with Alpha Omega Publications®.</p><p>With AOP\'s academic support sessions, you\'ll love seeing your child grasp the tools he or she needs to comprehend confusing coursework in language arts, world languages, advanced math, or other subjects. &nbsp;Whether you\'re teaching with Monarch<sup>TM</sup>, Switched-On-Schoolhouse®, LIFEPAC®, or Horizons, qualified academic advisors are available to give personalized instruction in any subject at any grade level of our christian homeschool curriculum.</p><p>Best of all, academic support from AOP is as flexible as the needs of your child. Available in three different packages, one-on-one sessions last up to 30 minutes and can be scheduled at any time on Monday through Friday from 8 a.m. to 4:30 p.m. (CT). PLus, purchased minutes are garunteed because any unused minutes can be carried over to another session and they can be used by another student in your home.</p><div class="support-bullets-and-graphic" style="float:left; padding: 0 3%; width: 94%;"><div class="landing-page-bullet-section" style="float:left;"><h4>Academic Support Pages</h4><ul><li>1 session (30 minutes): $30</li><li>3 sessions(90 minutes): $85</li><li>8 sessions(240 minutes): $216</li></ul></div><!--MEDIA-WRAPPER-START-3--><img style="float:right;" alt="Subject Doodles" class="media-element file-default" data-file_info="%7B%22fid%22:%22220%22,%22view_mode%22:%22default%22,%22fields%22:%7B%22format%22:%22default%22,%22field_file_image_alt_text%5Bund%5D%5B0%5D%5Bvalue%5D%22:%22Subject%20Doodles%22,%22field_file_image_title_text%5Bund%5D%5B0%5D%5Bvalue%5D%22:%22Subject%20Doodles%22%7D,%22type%22:%22media%22%7D" src="//glnnewmedia.s3.amazonaws.com/subject-doodles.gif" style="font-size: 0.923em; font-weight: 500; line-height: 1.5em;" title="Subject Doodles"><!--MEDIA-WRAPPER-END-3--></div><p>Eliminate the educational roadblocks hindering your child\'s learning with academic support sessions for your Christian homeschool curriculum. To schedule a session or learn more information, call 800-622-3070 today or verify your information below to have one of AOP\'s dedicated representatives call you!</p>',
    'format' => 'markdown',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'c9fcc31b-077a-43e8-812e-dd64d0977d61';
  $display->content['new-c9fcc31b-077a-43e8-812e-dd64d0977d61'] = $pane;
  $display->panels['contentmain'][0] = 'new-c9fcc31b-077a-43e8-812e-dd64d0977d61';
  $pane = new stdClass();
  $pane->pid = 'new-44b4afa4-c669-4340-981b-034d74dc78d3';
  $pane->panel = 'contentmain';
  $pane->type = 'block';
  $pane->subtype = 'block-20';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '44b4afa4-c669-4340-981b-034d74dc78d3';
  $display->content['new-44b4afa4-c669-4340-981b-034d74dc78d3'] = $pane;
  $display->panels['contentmain'][1] = 'new-44b4afa4-c669-4340-981b-034d74dc78d3';
  $pane = new stdClass();
  $pane->pid = 'new-9d6818f3-fe42-4a56-830a-c4f48cc5e0ab';
  $pane->panel = 'header';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => '',
    'body' => '<h1>Help is just a call away!</h1>
<h2>Connect to our community of homeschoolers dedicated to your success.</h2>',
    'format' => 'markdown',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'aop-support-banner lp-banner',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '9d6818f3-fe42-4a56-830a-c4f48cc5e0ab';
  $display->content['new-9d6818f3-fe42-4a56-830a-c4f48cc5e0ab'] = $pane;
  $display->panels['header'][0] = 'new-9d6818f3-fe42-4a56-830a-c4f48cc5e0ab';
  $pane = new stdClass();
  $pane->pid = 'new-69565efc-3111-4897-ad87-8984acedb81b';
  $pane->panel = 'sidebar';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'testimonial-sidebar-header',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '69565efc-3111-4897-ad87-8984acedb81b';
  $display->content['new-69565efc-3111-4897-ad87-8984acedb81b'] = $pane;
  $display->panels['sidebar'][0] = 'new-69565efc-3111-4897-ad87-8984acedb81b';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-9d6818f3-fe42-4a56-830a-c4f48cc5e0ab';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
