<?php
/**
 * @file
 * vocabulary.category.inc
 */

$api = '2.0.0';

$data = (object) array(
  'description' => '',
  'hierarchy' => '0',
  'machine_name' => 'category',
  'module' => 'taxonomy',
  'name' => 'Subject',
  'vid' => '5',
  'weight' => '0',
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'taxonomy',
);
