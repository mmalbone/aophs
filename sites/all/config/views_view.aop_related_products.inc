<?php
/**
 * @file
 * views_view.aop_related_products.inc
 */

$api = '2.0.0';

$data = $view = new view();
$view->name = 'aop_related_products';
$view->description = 'Related products based on curriculum, grade and subject';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'AOP Related Products';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['distinct'] = TRUE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '20';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'jcarousel';
$handler->display->display_options['style_options']['wrap'] = '0';
$handler->display->display_options['style_options']['visible'] = '';
$handler->display->display_options['style_options']['auto'] = '0';
$handler->display->display_options['style_options']['autoPause'] = 1;
$handler->display->display_options['style_options']['easing'] = '';
$handler->display->display_options['style_options']['vertical'] = 0;
$handler->display->display_options['row_plugin'] = 'node';
/* Relationship: Content: Referenced products */
$handler->display->display_options['relationships']['field_product_product_id']['id'] = 'field_product_product_id';
$handler->display->display_options['relationships']['field_product_product_id']['table'] = 'field_data_field_product';
$handler->display->display_options['relationships']['field_product_product_id']['field'] = 'field_product_product_id';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Contextual filter: Content: Has taxonomy term ID (with depth) */
$handler->display->display_options['arguments']['term_node_tid_depth']['id'] = 'term_node_tid_depth';
$handler->display->display_options['arguments']['term_node_tid_depth']['table'] = 'node';
$handler->display->display_options['arguments']['term_node_tid_depth']['field'] = 'term_node_tid_depth';
$handler->display->display_options['arguments']['term_node_tid_depth']['default_action'] = 'default';
$handler->display->display_options['arguments']['term_node_tid_depth']['default_argument_type'] = 'taxonomy_tid';
$handler->display->display_options['arguments']['term_node_tid_depth']['default_argument_options']['node'] = TRUE;
$handler->display->display_options['arguments']['term_node_tid_depth']['default_argument_options']['vocabularies'] = array(
  'dvd_categories' => 'dvd_categories',
  'grade_level' => 'grade_level',
  'curriculum' => 'curriculum',
);
$handler->display->display_options['arguments']['term_node_tid_depth']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['term_node_tid_depth']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['term_node_tid_depth']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['term_node_tid_depth']['depth'] = '0';
/* Contextual filter: Content: Nid */
$handler->display->display_options['arguments']['nid']['id'] = 'nid';
$handler->display->display_options['arguments']['nid']['table'] = 'node';
$handler->display->display_options['arguments']['nid']['field'] = 'nid';
$handler->display->display_options['arguments']['nid']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;

/* Display: Related Products Block */
$handler = $view->new_display('block', 'Related Products Block', 'related_products_block');
$handler->display->display_options['defaults']['query'] = FALSE;
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['disable_sql_rewrite'] = TRUE;
$handler->display->display_options['query']['options']['distinct'] = TRUE;
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'jcarousel';
$handler->display->display_options['style_options']['wrap'] = '0';
$handler->display->display_options['style_options']['visible'] = '2';
$handler->display->display_options['style_options']['auto'] = '0';
$handler->display->display_options['style_options']['autoPause'] = 1;
$handler->display->display_options['style_options']['easing'] = '';
$handler->display->display_options['style_options']['vertical'] = 0;
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['row_options']['inline'] = array(
  'title' => 'title',
  'field_product_image' => 'field_product_image',
  'commerce_price' => 'commerce_price',
  'add_to_cart_form' => 'add_to_cart_form',
  'field_hsc_long_description' => 'field_hsc_long_description',
  'sku' => 'sku',
  'nothing' => 'nothing',
);
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Commerce Product: Product Image */
$handler->display->display_options['fields']['field_product_image']['id'] = 'field_product_image';
$handler->display->display_options['fields']['field_product_image']['table'] = 'field_data_field_product_image';
$handler->display->display_options['fields']['field_product_image']['field'] = 'field_product_image';
$handler->display->display_options['fields']['field_product_image']['relationship'] = 'field_product_product_id';
$handler->display->display_options['fields']['field_product_image']['label'] = '';
$handler->display->display_options['fields']['field_product_image']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_product_image']['alter']['text'] = 'https://s3.amazonaws.com/glnnewmedia/media/catalog/product[field_product_image]';
$handler->display->display_options['fields']['field_product_image']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_product_image']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_product_image']['type'] = 'text_plain';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['exclude'] = TRUE;
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['node_in_colorbox_width'] = '600';
$handler->display->display_options['fields']['title']['node_in_colorbox_height'] = '600';
$handler->display->display_options['fields']['title']['node_in_colorbox_rel'] = '';
/* Field: Commerce Product: HSC Long Description */
$handler->display->display_options['fields']['field_hsc_long_description']['id'] = 'field_hsc_long_description';
$handler->display->display_options['fields']['field_hsc_long_description']['table'] = 'field_data_field_hsc_long_description';
$handler->display->display_options['fields']['field_hsc_long_description']['field'] = 'field_hsc_long_description';
$handler->display->display_options['fields']['field_hsc_long_description']['relationship'] = 'field_product_product_id';
$handler->display->display_options['fields']['field_hsc_long_description']['label'] = '';
$handler->display->display_options['fields']['field_hsc_long_description']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_hsc_long_description']['alter']['max_length'] = '40';
$handler->display->display_options['fields']['field_hsc_long_description']['alter']['trim'] = TRUE;
$handler->display->display_options['fields']['field_hsc_long_description']['alter']['html'] = TRUE;
$handler->display->display_options['fields']['field_hsc_long_description']['element_label_colon'] = FALSE;
/* Field: Commerce Product: Price */
$handler->display->display_options['fields']['commerce_price']['id'] = 'commerce_price';
$handler->display->display_options['fields']['commerce_price']['table'] = 'field_data_commerce_price';
$handler->display->display_options['fields']['commerce_price']['field'] = 'commerce_price';
$handler->display->display_options['fields']['commerce_price']['relationship'] = 'field_product_product_id';
$handler->display->display_options['fields']['commerce_price']['label'] = '';
$handler->display->display_options['fields']['commerce_price']['exclude'] = TRUE;
$handler->display->display_options['fields']['commerce_price']['element_type'] = '0';
$handler->display->display_options['fields']['commerce_price']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['commerce_price']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['commerce_price']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['commerce_price']['click_sort_column'] = 'amount';
$handler->display->display_options['fields']['commerce_price']['type'] = 'commerce_price_savings_formatter_inline';
$handler->display->display_options['fields']['commerce_price']['settings'] = array(
  'calculation' => '0',
  'prices' => array(
    'list' => 'list',
    'price' => 'price',
    'savings' => 0,
  ),
  'savings' => 'amount',
  'show_labels' => 0,
);
$handler->display->display_options['fields']['commerce_price']['field_api_classes'] = TRUE;
/* Field: Commerce Product: SKU */
$handler->display->display_options['fields']['sku']['id'] = 'sku';
$handler->display->display_options['fields']['sku']['table'] = 'commerce_product';
$handler->display->display_options['fields']['sku']['field'] = 'sku';
$handler->display->display_options['fields']['sku']['relationship'] = 'field_product_product_id';
$handler->display->display_options['fields']['sku']['label'] = '';
$handler->display->display_options['fields']['sku']['exclude'] = TRUE;
$handler->display->display_options['fields']['sku']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['sku']['link_to_product'] = 0;
/* Field: Commerce Product: Add to Cart form */
$handler->display->display_options['fields']['add_to_cart_form']['id'] = 'add_to_cart_form';
$handler->display->display_options['fields']['add_to_cart_form']['table'] = 'views_entity_commerce_product';
$handler->display->display_options['fields']['add_to_cart_form']['field'] = 'add_to_cart_form';
$handler->display->display_options['fields']['add_to_cart_form']['relationship'] = 'field_product_product_id';
$handler->display->display_options['fields']['add_to_cart_form']['label'] = '';
$handler->display->display_options['fields']['add_to_cart_form']['exclude'] = TRUE;
$handler->display->display_options['fields']['add_to_cart_form']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['add_to_cart_form']['show_quantity'] = 0;
$handler->display->display_options['fields']['add_to_cart_form']['default_quantity'] = '1';
$handler->display->display_options['fields']['add_to_cart_form']['combine'] = 0;
$handler->display->display_options['fields']['add_to_cart_form']['display_path'] = 0;
$handler->display->display_options['fields']['add_to_cart_form']['line_item_type'] = 'product';
/* Field: Global: Custom text */
$handler->display->display_options['fields']['nothing']['id'] = 'nothing';
$handler->display->display_options['fields']['nothing']['table'] = 'views';
$handler->display->display_options['fields']['nothing']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing']['label'] = '';
$handler->display->display_options['fields']['nothing']['alter']['text'] = '<div id="related-product-block" class="clearfix">
	<div class="related_product_image"><img width="150px" height="100px" alt="No Image" src="https://s3.amazonaws.com/glnnewmedia/media/catalog/product[field_product_image]"></div>
	<div class="related_product_description">
		<div class="title">[title]</div>
		<!-- [field_hsc_long_description] -->
		<div class="product-id">PRODUCT ID: [sku]</div>
		<div class="product-price">[commerce_price]</div>
                <div class="product-add-to-cart">[add_to_cart_form]</div>
	</div>
</div>';
$handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Content: Nid */
$handler->display->display_options['arguments']['nid']['id'] = 'nid';
$handler->display->display_options['arguments']['nid']['table'] = 'node';
$handler->display->display_options['arguments']['nid']['field'] = 'nid';
$handler->display->display_options['arguments']['nid']['default_action'] = 'default';
$handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
$handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['nid']['not'] = TRUE;
/* Contextual filter: Content: Has taxonomy term ID (with depth) */
$handler->display->display_options['arguments']['term_node_tid_depth']['id'] = 'term_node_tid_depth';
$handler->display->display_options['arguments']['term_node_tid_depth']['table'] = 'node';
$handler->display->display_options['arguments']['term_node_tid_depth']['field'] = 'term_node_tid_depth';
$handler->display->display_options['arguments']['term_node_tid_depth']['default_action'] = 'default';
$handler->display->display_options['arguments']['term_node_tid_depth']['default_argument_type'] = 'taxonomy_tid';
$handler->display->display_options['arguments']['term_node_tid_depth']['default_argument_options']['node'] = TRUE;
$handler->display->display_options['arguments']['term_node_tid_depth']['default_argument_options']['vocabularies'] = array(
  'dvd_categories' => 'dvd_categories',
  'grade_level' => 'grade_level',
  'curriculum' => 'curriculum',
);
$handler->display->display_options['arguments']['term_node_tid_depth']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['term_node_tid_depth']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['term_node_tid_depth']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['term_node_tid_depth']['depth'] = '0';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'curriculum' => 'curriculum',
  'dvd' => 'dvd',
  'resource' => 'resource',
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'views',
  1 => 'node',
  2 => 'text',
  3 => 'commerce_price',
);
