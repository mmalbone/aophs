<?php
/**
 * @file
 * page_manager_handlers.page__sonlight_curriculum_vs_horizons_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page__sonlight_curriculum_vs_horizons_panel_context';
$handler->task = 'page';
$handler->subtask = '_sonlight_curriculum_vs_horizons';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '02b452eb-bb57-4120-af6c-0f229e960548';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-e593e0d4-9aad-44f7-9bff-ce1f7d8e3129';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Sonlight Curriculum vs. Horizons',
    'body' => '<p>Do you need to compare Sonlight\'s literature-based curriculum to the Horizons program from Alpha Omega Publications? You\'re in the right spot. Find a short overview of each curriculum and service below, an easy-to-view comparison chart, and additional comparison options!</p><h3><strong>Sonlight Curriculum</strong></h3><p>Sonlight is a Christian company that provides literature-based homeschool courses for students in K-12th grades. Sonlight provides complete curriculum packages, resources, and materials designed to instill a love of learning in the homeschooled student. The Sonlight Curriculum consists of collections of literature designed to provide everything parents need to teach one child for an entire year. Sonlight\'s Core programs integrate history, geography, and literature into a coordinated whole. In addition, language arts, math, science, and electives can be added to the Core to provide a complete academic course load. Core instructor\'s guides are available to provide scheduling and teaching helps, study guides, answer keys, and more. Parents who use the Sonlight curriculum are responsible for all administrative tasks, including all required lesson planning, grading, record-keeping, and reporting.</p><p>Designed to instill a Christian worldview, all Sonlight curriculum utilizes literature, both fiction and non-fiction, to provide all academic content. Science instruction is based on a biblical view of creation and the origin of life. All Sonlight curriculum coursework can be purchased as Core packages (history, geography, and literature) or as individual subjects (math, science, Bible, language arts, and electives). Kits provide all essential materials for both students and teachers. Optional academic and administrative resources can be purchased separately.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>If you\'re considering a literature-based approach to homeschooling, remember that there is no one perfect homeschool curriculum. A blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Instead of feeling torn between different curricula, parents should be open to mixing and matching materials and resources to customize their child\'s education. Remember, each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3><strong>Horizons from Alpha Omega Publications</strong></h3><p>Horizons is a preK-12 print-based curriculum offered by Alpha Omega Publications, a Christian-based publisher that provides curriculum and educational services to homeschool families. Filled with engaging, full-color illustrations, Horizons is a child-friendly workbook-based curriculum with consumable lessons and hands-on activities. Horizons curriculum encourages concept mastery through a spiral-based learning process of skill/concept presentation, review, and reinforcement. Teacher\'s guides offer clear, step-by-step lesson plans and activities that are suitable for both homeschool and classroom settings.</p><p>Winner of Cathy Duffy\'s 100 Top Picks for Homeschool Curriculum, Horizons integrates Scripture and Christian living throughout all subjects. Horizons workbooks provide removable student pages for ease of student use. Horizons curriculum includes courses in math (K-8), penmanship (1-5), health (K-8), physical education (PreK-12), phonics and reading (K-3), and spelling and vocabulary (1-3). Health and physical education electives were designed for classroom use and may need to be adapted to homeschool use. Complete Horizons Preschool Curriculum Sets and Multimedia Sets are also available. Horizons curriculum sets include all student and teacher materials. Student and teacher materials can also be purchased separately.</p><h3><strong>Benefits and Features of Horizons from Alpha Omega Publications</strong></h3><p class="list_heading">Horizons</p><ul><li>brightly illustrated, hands-on, consumable curriculum</li><li>offers fun lessons and manipulative-based activities</li><li>teaches concepts through a spiral learning process</li><li>encourages concept mastery through quick concept introduction, review, and reinforcement</li></ul><h3 class="para-start"><strong>Take a Closer Look at Sonlight Curriculum and Horizons</strong></h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">Horizons</th><th class="compare">Sonlight Curriculum</th></tr>
</thead>
<tbody>
<tr>
<td>K-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Complete curriculum</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Instruction provided by parents</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Teacher-intensive</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Readiness tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Easy to use consumable workbooks</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Material designed for homeschoolers</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Manageable amount of daily lesson material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Literature-based approach</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr><th class="features">Cost</th><th class="compare">Alpha Omega Publications</th><th class="compare">Sonlight</th></tr>
</thead>
<tbody>
<tr>
<td>Average subject cost per grade level**</td>
<td class="tac">$104.95</td>
<td class="tac">$127.81</td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: Sonlight Curriculum versus Alpha Omega Publications</h4>
<ul class="comp_list">
<li><a href="/sonlight-vs-monarch">Sonlight vs. Monarch</a></li>
<li><a href="/sonlight-vs-aoa">Sonlight vs. Alpha Omega Academy</a></li>
<li><a href="/sonlight-vs-horizons">Sonlight vs. Horizons</a></li>
<li><a href="/sonlight-vs-lifepac">Sonlight vs. LIFEPAC</a></li>
<li><a href="/sonlight-vs-sos">Sonlight vs. Switched-On Schoolhouse</a></li>
</ul>
<p>*Sonlight® is the registered trademark of Sonlight Curriculum, LTD. Alpha Omega Publications is not in any way affiliated with Sonlight Curriculum. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by Sonlight.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p><p>**Prices obtained February 2013 from the Sonlight Curriculum website and from Alpha Omega Publications 2013 Homeschool Catalog. Individual course prices are based on a comparison between the use of the Horizons 2nd grade Phonics &amp; Reading program and Sonlight Grade 2 Readers with Language Arts 2. Costs do not reflect any additional fees or shipping and handling charges. Prices for other Sonlight and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'e593e0d4-9aad-44f7-9bff-ce1f7d8e3129';
  $display->content['new-e593e0d4-9aad-44f7-9bff-ce1f7d8e3129'] = $pane;
  $display->panels['left'][0] = 'new-e593e0d4-9aad-44f7-9bff-ce1f7d8e3129';
  $pane = new stdClass();
  $pane->pid = 'new-4a893c3b-e92a-4079-b74e-550568e5818c';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '4a893c3b-e92a-4079-b74e-550568e5818c';
  $display->content['new-4a893c3b-e92a-4079-b74e-550568e5818c'] = $pane;
  $display->panels['right'][0] = 'new-4a893c3b-e92a-4079-b74e-550568e5818c';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-e593e0d4-9aad-44f7-9bff-ce1f7d8e3129';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
