<?php
/**
 * @file
 * panels_layout.subscription_plans_landing_page_layout.inc
 */

$api = '2.0.0';

$data = $layout = new stdClass();
$layout->disabled = FALSE; /* Edit this to true to make a default layout disabled initially */
$layout->api_version = 1;
$layout->name = 'subscription_plans_landing_page_layout';
$layout->admin_title = 'Subscription Plans Landing Page layout';
$layout->admin_description = '';
$layout->category = 'AOP';
$layout->plugin = 'flexible';
$layout->settings = array(
  'items' => array(
    'canvas' => array(
      'type' => 'row',
      'contains' => 'column',
      'children' => array(
        0 => 'main',
      ),
      'parent' => NULL,
    ),
    'main' => array(
      'type' => 'column',
      'width' => 100,
      'width_type' => '%',
      'children' => array(
        0 => 'main-row',
        1 => 1,
        2 => 2,
        3 => 3,
        4 => 4,
        5 => 5,
        6 => 6,
        7 => 7,
        8 => 8,
        9 => 9,
      ),
      'parent' => 'canvas',
    ),
    'main-row' => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'center',
      ),
      'parent' => 'main',
    ),
    'center' => array(
      'type' => 'region',
      'title' => 'Center',
      'width' => 100,
      'width_type' => '%',
      'parent' => 'main-row',
    ),
    1 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'left_60',
        1 => 'right_40',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    'left_60' => array(
      'type' => 'region',
      'title' => 'left 60',
      'width' => 50,
      'width_type' => '%',
      'parent' => '1',
      'class' => 'left-60',
    ),
    'right_40' => array(
      'type' => 'region',
      'title' => 'right 40',
      'width' => 50,
      'width_type' => '%',
      'parent' => '1',
      'class' => 'right-40',
    ),
    2 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'left_40',
        1 => 'right_60',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    'left_40' => array(
      'type' => 'region',
      'title' => 'left 40',
      'width' => 50,
      'width_type' => '%',
      'parent' => '2',
      'class' => 'left-40',
    ),
    'right_60' => array(
      'type' => 'region',
      'title' => 'right 60',
      'width' => 50,
      'width_type' => '%',
      'parent' => '2',
      'class' => 'right-60',
    ),
    3 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'left_60_',
        1 => 'right_40_',
      ),
      'parent' => 'main',
      'class' => 'left 60',
    ),
    'right_40_' => array(
      'type' => 'region',
      'title' => 'right 40',
      'width' => 50,
      'width_type' => '%',
      'parent' => '3',
      'class' => 'right-40',
    ),
    4 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'left_40_',
        1 => 'right_40__',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    'left_40_' => array(
      'type' => 'region',
      'title' => 'left 40',
      'width' => 50,
      'width_type' => '%',
      'parent' => '4',
      'class' => 'left-40',
    ),
    'left_60_' => array(
      'type' => 'region',
      'title' => 'left 60',
      'width' => 50,
      'width_type' => '%',
      'parent' => '3',
      'class' => 'left-60',
    ),
    'right_40__' => array(
      'type' => 'region',
      'title' => 'right 60',
      'width' => 50,
      'width_type' => '%',
      'parent' => '4',
      'class' => 'right-60',
    ),
    5 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'left_60__',
        1 => 'right_40___',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    'left_60__' => array(
      'type' => 'region',
      'title' => 'left 60',
      'width' => 50,
      'width_type' => '%',
      'parent' => '5',
      'class' => 'left-60',
    ),
    'right_40___' => array(
      'type' => 'region',
      'title' => 'right 40',
      'width' => 50,
      'width_type' => '%',
      'parent' => '5',
      'class' => 'right-40',
    ),
    6 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'left_40__',
        1 => 'right_60_',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    'left_40__' => array(
      'type' => 'region',
      'title' => 'left 40',
      'width' => 50,
      'width_type' => '%',
      'parent' => '6',
      'class' => 'left-40',
    ),
    'right_60_' => array(
      'type' => 'region',
      'title' => 'right 60',
      'width' => 50,
      'width_type' => '%',
      'parent' => '6',
      'class' => 'right-60',
    ),
    7 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'left_60___',
        1 => 'right_40____',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    'left_60___' => array(
      'type' => 'region',
      'title' => 'left 60',
      'width' => 50,
      'width_type' => '%',
      'parent' => '7',
      'class' => 'left-60',
    ),
    'right_40____' => array(
      'type' => 'region',
      'title' => 'right 40',
      'width' => 50,
      'width_type' => '%',
      'parent' => '7',
      'class' => 'right-40',
    ),
    8 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'left_40___',
        1 => 'right_60__',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    'left_40___' => array(
      'type' => 'region',
      'title' => 'left 40',
      'width' => 50,
      'width_type' => '%',
      'parent' => '8',
      'class' => 'left-40',
    ),
    'right_60__' => array(
      'type' => 'region',
      'title' => 'right 60',
      'width' => 50,
      'width_type' => '%',
      'parent' => '8',
      'class' => 'right-60',
    ),
    9 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'full_bottom',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    'full_bottom' => array(
      'type' => 'region',
      'title' => 'full bottom',
      'width' => 100,
      'width_type' => '%',
      'parent' => '9',
      'class' => '',
    ),
  ),
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'panels',
);
