<?php
/**
 * @file
 * page_manager_handlers.page_sonlight_curriculum_vs_switched_on_schoolhouse_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_sonlight_curriculum_vs_switched_on_schoolhouse_panel_context';
$handler->task = 'page';
$handler->subtask = 'sonlight_curriculum_vs_switched_on_schoolhouse';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '8f627c66-3d35-49f8-9673-4de366d4c9a5';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-e0d8e08b-92f8-4e94-b1af-de7e0f5e0877';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => ' Sonlight Curriculum vs. Switched-On Schoolhouse',
    'body' => '<p>Ready to compare the literature-based option offered by Sonlight Curriculum to the Switched-On Schoolhouse program from Alpha Omega Publications? You\'re in the right spot. Find a short overview of each curriculum and service below, an easy-to-view comparison chart, and additional comparison options!</p><h3><strong>Sonlight Curriculum</strong></h3><p>Sonlight is a Christian company that provides literature-based homeschool courses for students in K-12th grades. Sonlight provides complete curriculum packages, resources, and materials designed to instill a love of learning in the homeschooled student. The Sonlight Curriculum consists of collections of literature designed to provide everything parents need to teach one child for an entire year. Sonlight\'s Core programs integrate history, geography, and literature into a coordinated whole. In addition, language arts, math, science, and electives can be added to the Core to provide a complete academic course load. Core instructor\'s guides are available to provide scheduling and teaching helps, study guides, answer keys, and more. Parents who use the Sonlight curriculum are responsible for all administrative tasks, including all required lesson planning, grading, record-keeping, and reporting.</p><p>Designed to instill a Christian worldview, all Sonlight curriculum utilizes literature, both fiction and non-fiction, to provide all academic content. Science instruction is based on a biblical view of creation and the origin of life. All Sonlight curriculum coursework can be purchased as Core packages (history, geography, and literature) or as individual subjects (math, science, Bible, language arts, and electives). Kits provide all essential materials for both students and teachers. Optional academic and administrative resources can be purchased separately.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>If you\'re considering a literature-based approach to homeschooling, remember that there is no one perfect homeschool curriculum. A blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Instead of feeling torn between different curricula, parents should be open to mixing and matching materials and resources to customize their child\'s education. Remember, each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3><strong>Switched-On Schoolhouse from Alpha Omega Publications</strong></h3><p>Switched-On Schoolhouse (SOS) from Alpha Omega Publications is an engaging, computer-based curriculum for grades 3-12. Alpha Omega Publications, a trusted publisher of high quality educational products, provides Christian curriculum and educational services to homeschool families. Revolutionizing the way students learn, SOS is a comprehensive, CD-ROM-based curriculum that is entirely technology based. This innovative, state-of-the-art curriculum presents biblically-based lessons using interactive multimedia, eye-catching animation, learning games, video clips, and more! Unlike traditional textbooks, SOS has a diverse mix of text-based instruction and engaging multimedia enrichment. Unsurpassed administrative tools include time-saving features like automatic grading and lesson planning with built-in calendar, customizable lessons, flexible printing options, and message center.</p><p>Winner of Cathy Duffy\'s 100 Top Picks for Homeschool Curriculum, SOS integrates Scripture and a Christian worldview into all subjects. SOS offers the flexibility of an individualized, student-driven approach, helpful text-to-speech option, and diagnostic tests for correct placement. Switched-On Schoolhouse courses are available for 3rd – 12th grade students in Bible, language arts, math, science, and history and geography. Electives are also available for students of all ages. SOS may be purchased as individual subjects or as complete five-subject sets. The program includes all required student and parent materials.</p><h3><strong>Benefits and Features of Switched-On Schoolhouse from Alpha Omega Publications</strong></h3><p class="list_heading">Switched-On Schoolhouse</p><ul><li>interactive, innovative, computer-based curriculum</li><li>offers flexible, engaging learning environment</li><li>contains multimedia tools, educational games, and automatic grading options</li><li>can reduce teacher planning and grading by 20 hours per week</li></ul><h3 class="para-start"><strong>Take a Closer Look at Sonlight Curriculum and Switched-on Schoolhouse</strong></h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">Switched-On Schoolhouse</th><th class="compare">Sonlight Curriculum</th></tr>
</thead>
<tbody>
<tr>
<td>K-12 curriculum options</td>
<td class="tac">3-12th grade</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Teacher-intensive curriculum</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Computer-based curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Interactive, engaging multimedia tools</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Customizable lessons and calendar</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Placement tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Automatic grading and lesson planning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Automatic updated material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Flexible and individualized</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Mastery-based learning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Literature-based learning</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr><th class="features">Cost</th><th class="compare">Alpha Omega Publications</th><th class="compare">Sonlight</th></tr>
</thead>
<tbody>
<tr>
<td>Average yearly cost per grade level**</td>
<td class="tac">$449.95</td>
<td class="tac">$869</td>
</tr>
<tr class="alt">
<td>Average subject cost per grade level**</td>
<td class="tac">$99.95</td>
<td class="tac">$227.78</td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: Sonlight Curriculum versus Alpha Omega Publications</h4>
<ul class="comp_list">
<li><a href="/sonlight-vs-monarch">Sonlight vs. Monarch</a></li>
<li><a href="/sonlight-vs-aoa">Sonlight vs. Alpha Omega Academy</a></li>
<li><a href="/sonlight-vs-horizons">Sonlight vs. Horizons</a></li>
<li><a href="/sonlight-vs-lifepac">Sonlight vs. LIFEPAC</a></li>
<li><a href="/sonlight-vs-sos">Sonlight vs. Switched-On Schoolhouse</a></li>
</ul>
<p>*Sonlight® is the registered trademark of Sonlight Curriculum, LTD. Alpha Omega Publications is not in any way affiliated with Sonlight Curriculum. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by Sonlight.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p><p>**Prices obtained February 2013 from the Sonlight website and from Alpha Omega Publications 2013 Homeschool Catalog. Yearly prices based on comparison between the Switched-On Schoolhouse 3rd Grade Complete grade level set and the complete Sonlight 3rd Grade Multi-subject Package, both of which include all required student and teacher materials for a single 3rd grader. Individual subject prices are based on comparison between the SOS 3rd grade single subject cost and Sonlight 3rd grade science kit D costs. Costs do not reflect any additional fees or shipping and handling charges. Prices for other Sonlight and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'e0d8e08b-92f8-4e94-b1af-de7e0f5e0877';
  $display->content['new-e0d8e08b-92f8-4e94-b1af-de7e0f5e0877'] = $pane;
  $display->panels['left'][0] = 'new-e0d8e08b-92f8-4e94-b1af-de7e0f5e0877';
  $pane = new stdClass();
  $pane->pid = 'new-270f9b30-4321-4ab2-bf15-b21463d25c73';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '270f9b30-4321-4ab2-bf15-b21463d25c73';
  $display->content['new-270f9b30-4321-4ab2-bf15-b21463d25c73'] = $pane;
  $display->panels['right'][0] = 'new-270f9b30-4321-4ab2-bf15-b21463d25c73';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-e0d8e08b-92f8-4e94-b1af-de7e0f5e0877';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
