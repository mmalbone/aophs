<?php
/**
 * @file
 * services_endpoint.aop_user_account.inc
 */

$api = '2.0.0';

$data = $endpoint = new stdClass();
$endpoint->disabled = FALSE; /* Edit this to true to make a default endpoint disabled initially */
$endpoint->api_version = 3;
$endpoint->name = 'aop_user_account';
$endpoint->server = 'rest_server';
$endpoint->path = 'assist/customers';
$endpoint->authentication = array(
  'services_oauth' => array(
    'oauth_context' => 'mule',
    'authorization' => 'muleoauth',
    'credentials' => 'consumer',
  ),
);
$endpoint->server_settings = array(
  'formatters' => array(
    'json' => TRUE,
    'bencode' => FALSE,
    'jsonp' => FALSE,
    'php' => FALSE,
    'xml' => FALSE,
  ),
  'parsers' => array(
    'application/json' => TRUE,
    'application/vnd.php.serialized' => FALSE,
    'application/x-www-form-urlencoded' => FALSE,
    'application/xml' => FALSE,
    'multipart/form-data' => FALSE,
    'text/xml' => FALSE,
  ),
);
$endpoint->resources = array(
  'node' => array(
    'operations' => array(
      'retrieve' => array(
        'enabled' => '1',
        'settings' => array(
          'services_oauth' => array(
            'credentials' => 'consumer',
            'authorization' => 'muleoauth',
          ),
        ),
      ),
      'update' => array(
        'enabled' => '1',
        'settings' => array(
          'services_oauth' => array(
            'credentials' => 'consumer',
            'authorization' => 'muleoauth',
          ),
        ),
      ),
    ),
  ),
  'order' => array(
    'operations' => array(
      'retrieve' => array(
        'enabled' => '1',
        'settings' => array(
          'services_oauth' => array(
            'credentials' => 'consumer',
            'authorization' => 'muleoauth',
          ),
        ),
      ),
      'update' => array(
        'enabled' => '1',
        'settings' => array(
          'services_oauth' => array(
            'credentials' => 'consumer',
            'authorization' => 'muleoauth',
          ),
        ),
      ),
    ),
    'relationships' => array(
      'line-items' => array(
        'enabled' => '1',
        'settings' => array(
          'services_oauth' => array(
            'credentials' => 'consumer',
            'authorization' => 'muleoauth',
          ),
        ),
      ),
    ),
  ),
  'user' => array(
    'operations' => array(
      'retrieve' => array(
        'enabled' => '1',
        'settings' => array(
          'services_oauth' => array(
            'credentials' => 'consumer',
            'authorization' => 'muleoauth',
          ),
        ),
      ),
      'update' => array(
        'enabled' => '1',
        'settings' => array(
          'services_oauth' => array(
            'credentials' => 'consumer',
            'authorization' => 'muleoauth',
          ),
        ),
      ),
    ),
  ),
);
$endpoint->debug = 1;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'services',
);
