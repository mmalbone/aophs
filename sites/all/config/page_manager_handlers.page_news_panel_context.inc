<?php
/**
 * @file
 * page_manager_handlers.page_news_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_news_panel_context';
$handler->task = 'page';
$handler->subtask = 'news';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => 'aop-news-page',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'flexible';
$display->layout_settings = array(
  'items' => array(
    'canvas' => array(
      'type' => 'row',
      'contains' => 'column',
      'children' => array(
        0 => 'main',
      ),
      'parent' => NULL,
    ),
    'main' => array(
      'type' => 'column',
      'width' => 100,
      'width_type' => '%',
      'children' => array(
        0 => 'main-row',
        1 => 1,
      ),
      'parent' => 'canvas',
    ),
    'main-row' => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'center',
      ),
      'parent' => 'main',
    ),
    'center' => array(
      'type' => 'region',
      'title' => 'Center',
      'width' => 100,
      'width_type' => '%',
      'parent' => 'main-row',
    ),
    1 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'left',
        1 => 'right',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    'left' => array(
      'type' => 'region',
      'title' => 'Left',
      'width' => '70.04168670835338',
      'width_type' => '%',
      'parent' => '1',
      'class' => 'aop-news-left',
    ),
    'right' => array(
      'type' => 'region',
      'title' => 'Right',
      'width' => '29.95831329164662',
      'width_type' => '%',
      'parent' => '1',
      'class' => 'aop-news-right',
    ),
  ),
);
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'center' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'b00ef777-1b6d-4ca4-95fe-34e9c00a7fb5';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-d44ed465-77ac-413e-9533-4986f3387b75';
  $pane->panel = 'center';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => '',
    'body' => '<h1>Latest Homeschooling News</h1>
<h2>Get the latest information on what\'s new at Alpha Omega Publications.</h2>',
    'format' => 'markdown',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'aop-news-banner',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'd44ed465-77ac-413e-9533-4986f3387b75';
  $display->content['new-d44ed465-77ac-413e-9533-4986f3387b75'] = $pane;
  $display->panels['center'][0] = 'new-d44ed465-77ac-413e-9533-4986f3387b75';
  $pane = new stdClass();
  $pane->pid = 'new-fb23428b-cc4c-4d37-97e4-4ca33dfb6a54';
  $pane->panel = 'left';
  $pane->type = 'views_panes';
  $pane->subtype = 'aop_blog-panel_pane_3';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'fb23428b-cc4c-4d37-97e4-4ca33dfb6a54';
  $display->content['new-fb23428b-cc4c-4d37-97e4-4ca33dfb6a54'] = $pane;
  $display->panels['left'][0] = 'new-fb23428b-cc4c-4d37-97e4-4ca33dfb6a54';
  $pane = new stdClass();
  $pane->pid = 'new-8cf94a45-8fc7-4008-98b3-738a2dfce429';
  $pane->panel = 'right';
  $pane->type = 'custom';
  $pane->subtype = 'aop_join_facebook';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => '',
    'body' => '',
    'format' => 'filtered_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'aop-news-join-facebook',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '8cf94a45-8fc7-4008-98b3-738a2dfce429';
  $display->content['new-8cf94a45-8fc7-4008-98b3-738a2dfce429'] = $pane;
  $display->panels['right'][0] = 'new-8cf94a45-8fc7-4008-98b3-738a2dfce429';
  $pane = new stdClass();
  $pane->pid = 'new-53ca3652-6c50-4087-a95c-3caedd2f08d1';
  $pane->panel = 'right';
  $pane->type = 'block';
  $pane->subtype = 'block-19';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<span>AOP Newsletter</span>',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '53ca3652-6c50-4087-a95c-3caedd2f08d1';
  $display->content['new-53ca3652-6c50-4087-a95c-3caedd2f08d1'] = $pane;
  $display->panels['right'][1] = 'new-53ca3652-6c50-4087-a95c-3caedd2f08d1';
  $pane = new stdClass();
  $pane->pid = 'new-b8c3f1e3-92aa-4876-97ad-1b378b19e332';
  $pane->panel = 'right';
  $pane->type = 'block';
  $pane->subtype = 'orangebox-enews_signup';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'b8c3f1e3-92aa-4876-97ad-1b378b19e332';
  $display->content['new-b8c3f1e3-92aa-4876-97ad-1b378b19e332'] = $pane;
  $display->panels['right'][2] = 'new-b8c3f1e3-92aa-4876-97ad-1b378b19e332';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = '0';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.aop_blog' => 'views_view.aop_blog',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
