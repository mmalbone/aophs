<?php
/**
 * @file
 * page_manager_handlers.page_new_for_2014_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_new_for_2014_panel_context';
$handler->task = 'page';
$handler->subtask = 'new_for_2014';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'flexible';
$display->layout_settings = array(
  'items' => array(
    'canvas' => array(
      'type' => 'row',
      'contains' => 'column',
      'children' => array(
        0 => 'main',
      ),
      'parent' => NULL,
    ),
    'main' => array(
      'type' => 'column',
      'width' => 100,
      'width_type' => '%',
      'children' => array(
        0 => 'main-row',
      ),
      'parent' => 'canvas',
    ),
    'main-row' => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'center',
        1 => 'right',
      ),
      'parent' => 'main',
    ),
    'center' => array(
      'type' => 'region',
      'title' => 'Center',
      'width' => '70.04168670835338',
      'width_type' => '%',
      'parent' => 'main-row',
    ),
    'right' => array(
      'type' => 'region',
      'title' => 'Right',
      'width' => '29.95831329164662',
      'width_type' => '%',
      'parent' => 'main-row',
      'class' => '',
    ),
  ),
);
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'center' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'cb97aa0f-3498-4594-9336-b85f1b686a65';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-9e72b304-5239-4aa9-9bfe-1ca36997fc81';
  $pane->panel = 'center';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'New for 2014',
    'body' => '<p>See what’s new at Alpha Omega Publications, including redesigned LIFEPAC courses and many new CTE electives for Monarch and Switched-On Schoolhouse.</p>
<h3>Fresh Look for LIFEPAC</h3><p>Newly redesigned courses for 7th, 8th, and 9th grade (with more to come) give LIFEPAC a contemporary appearance for the courses of Bible, history and geography, language arts, math, and science. Featuring the same Christian worldview you’ve come to expect from LIFEPAC, the content is now presented in a more colorful light and modern design to improve comprehension.</p>
<h3>Switched-On Schoolhouse 2014 Edition</h3><p>Voted as the best computer-based curriculum* by homeschoolers, Switched-On Schoolhouse streamlines homeschooling with time-saving features like automatic lesson planning and grading. Plus, SOS is even more engaging with these appealing new features for 2014.</p><ul><li><strong>Scheduling Assistant</strong> - Teachers can schedule recurring events and activities simultaneously.</li><li><strong>Activity Log</strong> - Students can enter their own activities to promote responsibility and time management.</li><li><strong>Enhanced Teacher Reports</strong> - Quickly access daily homework reports and progress reports that show the percentage completed for each course and all the units in each subject that are assigned to your student.</li><li>Enhanced Student Reports - Students can view daily work reports and past due reports for a selected date range.</li></ul><h3 style="margin-top:20px">New Print and Electronic Courses</h3><p>Committed to offering homeschoolers a wide variety of curriculum to meet the needs and interests of every child, AOP has added over 25 new courses in 2014.</p><ul><li>Horizons 6th Grade Penmanship</li><li>LIFEPAC 7th Grade Math</li><li>LIFEPAC 8th Grade Math</li><li>Monarch and SOS Fundamentals of Computer Systems</li><li>Monarch and SOS Fundamentals of Digital Media</li><li>Monarch and SOS Fundamentals of Programming and Software Development</li><li>Monarch and SOS Introduction to Information Technology</li><li>Monarch and SOS Introduction to Network Systems</li><li>Monarch and SOS Introduction to Information Technology Support and Services</li><li>Monarch and SOS New Applications: Web Development in the21st Century</li><li>Monarch and SOS Network System Design</li><li>Monarch and SOS Office Applications I</li><li>Monarch and SOS Office Applications II</li><li>Monarch and SOS Physical Education</li><li>Monarch and SOS Software Development Tools</li></ul><p>*2013 Excellent in Education Awards | The Old Schoolhouse Magazine</p>',
    'format' => 'markdown',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '9e72b304-5239-4aa9-9bfe-1ca36997fc81';
  $display->content['new-9e72b304-5239-4aa9-9bfe-1ca36997fc81'] = $pane;
  $display->panels['center'][0] = 'new-9e72b304-5239-4aa9-9bfe-1ca36997fc81';
  $pane = new stdClass();
  $pane->pid = 'new-369d05d4-e1d4-4747-b47b-b4cf9f1f85ac';
  $pane->panel = 'right';
  $pane->type = 'custom';
  $pane->subtype = 'aop_join_facebook';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => '',
    'body' => '',
    'format' => 'filtered_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '369d05d4-e1d4-4747-b47b-b4cf9f1f85ac';
  $display->content['new-369d05d4-e1d4-4747-b47b-b4cf9f1f85ac'] = $pane;
  $display->panels['right'][0] = 'new-369d05d4-e1d4-4747-b47b-b4cf9f1f85ac';
  $pane = new stdClass();
  $pane->pid = 'new-bb956ce2-5a60-4b85-8df8-185dd18935ce';
  $pane->panel = 'right';
  $pane->type = 'block';
  $pane->subtype = 'block-19';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'AOP Newsletter',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'bb956ce2-5a60-4b85-8df8-185dd18935ce';
  $display->content['new-bb956ce2-5a60-4b85-8df8-185dd18935ce'] = $pane;
  $display->panels['right'][1] = 'new-bb956ce2-5a60-4b85-8df8-185dd18935ce';
  $pane = new stdClass();
  $pane->pid = 'new-07e5a3dc-d1dc-475a-a437-813d150a1bf8';
  $pane->panel = 'right';
  $pane->type = 'block';
  $pane->subtype = 'orangebox-enews_signup';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '07e5a3dc-d1dc-475a-a437-813d150a1bf8';
  $display->content['new-07e5a3dc-d1dc-475a-a437-813d150a1bf8'] = $pane;
  $display->panels['right'][2] = 'new-07e5a3dc-d1dc-475a-a437-813d150a1bf8';
  $pane = new stdClass();
  $pane->pid = 'new-529100e0-1289-486b-838d-5844165874eb';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = '529100e0-1289-486b-838d-5844165874eb';
  $display->content['new-529100e0-1289-486b-838d-5844165874eb'] = $pane;
  $display->panels['right'][3] = 'new-529100e0-1289-486b-838d-5844165874eb';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-9e72b304-5239-4aa9-9bfe-1ca36997fc81';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
