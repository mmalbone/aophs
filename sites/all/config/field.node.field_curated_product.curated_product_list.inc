<?php
/**
 * @file
 * field.node.field_curated_product.curated_product_list.inc
 */

$api = '2.0.0';

$data = array(
  'field_config' => array(
    'active' => '1',
    'cardinality' => '-1',
    'columns' => array(
      'target_id' => array(
        'description' => 'The id of the target entity.',
        'not null' => TRUE,
        'type' => 'int',
        'unsigned' => TRUE,
      ),
    ),
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_curated_product',
    'foreign keys' => array(
      'node' => array(
        'columns' => array(
          'target_id' => 'nid',
        ),
        'table' => 'node',
      ),
    ),
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => '0',
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'direction' => 'ASC',
          'property' => 'title',
          'type' => 'property',
        ),
        'target_bundles' => array(
          'curriculum' => 'curriculum',
          'dvd' => 'dvd',
          'resource' => 'resource',
        ),
      ),
      'target_type' => 'node',
    ),
    'storage' => array(
      'active' => '1',
      'details' => array(
        'sql' => array(
          'FIELD_LOAD_CURRENT' => array(
            'field_data_field_curated_product' => array(
              'target_id' => 'field_curated_product_target_id',
            ),
          ),
          'FIELD_LOAD_REVISION' => array(
            'field_revision_field_curated_product' => array(
              'target_id' => 'field_curated_product_target_id',
            ),
          ),
        ),
      ),
      'module' => 'field_sql_storage',
      'settings' => array(),
      'type' => 'field_sql_storage',
    ),
    'translatable' => '0',
    'type' => 'entityreference',
  ),
  'field_instance' => array(
    'bundle' => 'curated_product_list',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => '',
    'display' => array(
      'aop_subscription_list' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'hidden',
        'module' => 'entityreference',
        'settings' => array(
          'links' => 1,
          'view_mode' => 'node_teaser',
        ),
        'type' => 'entityreference_entity_view',
        'weight' => '0',
      ),
      'product_carousel' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'product_list' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'entityreference',
        'settings' => array(
          'links' => 1,
          'view_mode' => 'node_product_list',
        ),
        'type' => 'entityreference_entity_view',
        'weight' => '0',
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => '',
    'field_name' => 'field_curated_product',
    'label' => 'Curated Product',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference',
      'settings' => array(
        'apply_chosen' => '',
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => '60',
      ),
      'type' => 'entityreference_autocomplete',
      'weight' => '2',
    ),
  ),
);

$dependencies = array(
  'content_type.curated_product_list' => 'content_type.curated_product_list',
);

$optional = array();

$modules = array(
  0 => 'field_sql_storage',
  1 => 'entityreference',
);
