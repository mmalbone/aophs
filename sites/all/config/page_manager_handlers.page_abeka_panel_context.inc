<?php
/**
 * @file
 * page_manager_handlers.page_abeka_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_abeka_panel_context';
$handler->task = 'page';
$handler->subtask = 'abeka';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Landing page',
  'no_blocks' => FALSE,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
    'middle' => NULL,
    'center' => NULL,
    'center_' => NULL,
  ),
);
$display->cache = array();
$display->title = 'Abeka';
$display->uuid = 'dd73f183-6620-421e-9e1e-0d83a08626db';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-17552fb9-613c-4ddf-81f2-ad6b1acc4d3e';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'A Beka vs. Alpha Omega Publications',
    'body' => '<p>Thinking about homeschooling with A Beka? Wondering what the differences are between A Beka and Alpha Omega Publications? You\'re in the right place. Many parents just like you are also searching for a homeschool curriculum and teaching approach that best fits their child\'s academic needs. With so many homeschooling options, resources, and curricula to choose from, selecting homeschool materials for your child can often be a daunting task, but we\'re here to help!</p><h3>A Beka\'s Approach</h3><p>A Beka Books is a popular publisher of Christian-based nursery-12th grade curriculum materials. A Beka offers several curricula and service options for homeschoolers such as a traditional parent-directed textbook option for K-12th graders, as well as a K-12 academy which provides both accredited and non-accredited programs for homeschoolers. Core subjects and electives are available. Parents can purchase single subjects in textbook, DVD, or streaming video format, complete student and teacher curriculum kits, and supplemental materials such as maps, flashcards, and readers.</p><p>One of A Beka\'s strengths is the ability to produce a structured school-at-home approach. Because A Beka materials are developed in a classroom setting, they draw on the experience of classroom teachers as the foundation for their curriculum offerings. However, this classroom approach can also result in making homeschool process more teacher-intensive and less friendly to the homeschool parent. In addition, A Beka\'s structured approach means the curriculum is not easily customized according to student academic needs or individualized according to specific learning styles.</p><h3>Finding Your Approach to Homeschooling</h3><p>Most parents discover that their child\'s educational needs are best met by blending several different homeschool curriculums and teaching approaches. Because there is no one perfect homeschool curriculum, a blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Rather than feeling torn between curricula, parents should be open to mixing and matching materials and resources to customize their child\'s education. Remember, each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3>Curriculum from Alpha Omega Publications</h3><p>Alpha Omega Publications is a Christian-based, PreK-12 publisher that provides engaging, interactive curriculum and educational services to homeschool families. Offering proven, easy-to-teach homeschool curriculum, AOP offers four main curriculum offerings: Internet-based Monarch (3-12), computer-based Switched-On Schoolhouse (3-12), worktext-based LIFEPAC (K-12), and workbook-based Horizons (PreK-12). Main core subjects offered include Bible, language arts, math, science, and history and geography. AOP also offers Alpha Omega Academy, a distance learning academy for students in grades K-12. Understanding that each child learns differently, AOP offers diverse curriculum and services in different formats to fit multiple learning styles, ensuring you can find what fits your child\'s needs.</p><h3>Benefits and Features of Alpha Omega Publications\' Curriculum</h3><p>Monarch</p><ul><li>online interactive lessons with anytime, anywhere access</li><li>compatible with both Windows® and Mac® operating systems</li><li>offers customizable student learning options and time-saving teaching tools</li><li>features dynamic, media-rich lessons; real-time content updates; and data storage</li></ul><p>Switched-On Schoolhouse</p><ul><li>interactive, computer-based curriculum</li><li>offers a flexible, engaging learning environment</li><li>contains multimedia tools, educational games, and automatic grading options</li><li>can reduce teacher planning and grading by 20 hours per week</li></ul><p>LIFEPAC</p><ul><li>full-color, print-based curriculum</li><li>offers progressive, self-paced individual unit worktexts</li><li>promotes concept mastery and cognitive reasoning skills</li><li>encourages academic independence in your children</li></ul><p>Horizons</p><ul><li>brightly illustrated, hands-on, consumable curriculum</li><li>offers fun lessons and manipulative-based activities</li><li>teaches concepts through a spiral learning process</li><li>encourages concept mastery through quick concept introduction, review, and reinforcement</li></ul><h3>Take a Closer Look at A Beka and Alpha Omega Publications</h3><div class="comp_chart"><table><thead><tr><th>Offerings</th><th>Alpha Omega Publications</th><th>A Beka</th></tr></thead><tbody><tr><td>PreK-12 curriculum options</td><td><img alt="Checkmark" src="sites/all/themes/aop/images/check.png"></td><td><img alt="Checkmark" src="sites/all/themes/aop/images/check.png"></td></tr><tr><td>Bible-based</td><td><img alt="Checkmark" src="sites/all/themes/aop/images/check.png"></td><td><img alt="Checkmark" src="sites/all/themes/aop/images/check.png"></td></tr><tr><td>Core subjects and electives available</td><td><img alt="Checkmark" src="sites/all/themes/aop/images/check.png"></td><td><img alt="Checkmark" src="sites/all/themes/aop/images/check.png"></td></tr><tr><td>Flexible and individualized curriculum</td><td><img alt="Checkmark" src="sites/all/themes/aop/images/check.png"></td><td>&nbsp;</td></tr><tr><td>Customizable curriculum</td><td><img alt="Checkmark" src="sites/all/themes/aop/images/check.png"></td><td>&nbsp;</td></tr><tr><td>Teacher-intensive curriculum</td><td>&nbsp;</td><td><img alt="Checkmark" src="sites/all/themes/aop/images/check.png"></td></tr><tr><td>Placement tests available</td><td><img alt="Checkmark" src="sites/all/themes/aop/images/check.png"></td><td>&nbsp;</td></tr><tr><td>Interactive lesson material</td><td><img alt="Checkmark" src="sites/all/themes/aop/images/check.png"></td><td>&nbsp;</td></tr><tr><td>Automatic curriculum updates available</td><td><img alt="Checkmark" src="sites/all/themes/aop/images/check.png"></td><td>&nbsp;</td></tr><tr><td>Automatic grading and lesson planning available</td><td><img alt="Checkmark" src="sites/all/themes/aop/images/check.png"></td><td>&nbsp;</td></tr><tr><td>Appeals to students with multiple learning styles</td><td><img alt="Checkmark" src="sites/all/themes/aop/images/check.png"></td><td>&nbsp;</td></tr></tbody></table><table id="price_chart"><thead><tr><th>Cost</th><th>Alpha Omega Publications</th><th>A Beka</th></tr></thead><tbody><tr><td>Average yearly cost per grade level**</td><td>$449.99 for Monarch</td><td>$995 for video complete DVD option</td></tr><tr><td>Average subject cost per grade level**</td><td>$99.95 for Monarch</td><td>$395 per credit for video complete DVD option</td></tr></tbody></table></div><h4>Compare Offerings: A Beka versus Alpha Omega Publications</h4><p>A Beka Book</p><ul><li><a href="/abeka-book-vs-monarch">A Beka Book vs. Monarch</a></li><li><a href="/abeka-book-vs-horizons">A Beka Book vs. Horizons</a></li><li><a href="/abeka-book-vs-lifepac">A Beka Book vs. LIFEPAC</a></li><li><a href="/abeka-book-vs-aoa">A Beka Book vs. Alpha Omega Academy</a></li><li><a href="/abeka-book-vs-sos">A Beka Book vs. Switched-On Schoolhouse</a></li></ul><p>A Beka Academy (DVD)</p><ul><li><a href="/abeka-dvd-vs-monarch">A Beka Academy (DVD) vs. Monarch</a></li><li><a href="/abeka-dvd-vs-horizons">A Beka Academy (DVD) vs. Horizons</a></li><li><a href="/abeka-dvd-vs-lifepac">A Beka Academy (DVD) vs. LIFEPAC</a></li><li><a href="/abeka-dvd-vs-aoa">A Beka Academy (DVD) vs. Alpha Omega Academy</a></li><li><a href="/abeka-dvd-vs-sos">A Beka Academy (DVD) vs. Switched-On Schoolhouse</a></li></ul><p>A Beka Academy (Parent-Directed)</p><ul><li><a href="/abeka-parent-directed-vs-monarch">A Beka Academy (Parent-Directed) vs. Monarch</a></li><li><a href="abeka-parent-directed-vs-horizons">A Beka Academy (Parent-Directed) vs. Horizons</a></li><li><a href="/abeka-parent-directed-vs-lifepac">A Beka Academy (Parent-Directed) vs. LIFEPAC</a></li><li><a href="/abeka-parent-directed-vs-aoa">A Beka Academy (Parent-Directed) vs. Alpha Omega Academy</a></li><li><a href="/abeka-parent-directed-vs-sos">A Beka Academy (Parent-Directed) vs. Switched-On Schoolhouse</a></li></ul><p>A Beka Video-based Internet Streaming</p><ul><li><a href="/abeka-academy-streaming-video-vs-monarch">A Beka Video-based Internet Streaming vs. Monarch</a></li><li><a href="/abeka-academy-streaming-video-vs-horizons">A Beka Video-based Internet Streaming vs. Horizons</a></li><li><a href="/abeka-academy-streaming-video-vs-lifepac">A Beka Video-based Internet Streaming vs. LIFEPAC</a></li><li><a href="/abeka-academy-streaming-video-vs-aoa">A Beka Video-based Internet Streaming vs. Alpha Omega Academy</a></li><li><a href="/abeka-academy-streaming-video-vs-sos">A Beka Video-based Internet Streaming vs. Switched-On Schoolhouse</a></li></ul><p>*A Beka Book® and A Beka Academy® are the registered trademarks and subsidiaries of Pensacola Christian College. Alpha Omega Publications is not in any way affiliated with A Beka®. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by A Beka Book or A Beka Academy.</p><div class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</div><p>**Prices obtained February 2013 from A Beka 2013 Homeschool Catalog and from Alpha Omega Publications 2011 Homeschool Catalog. Yearly prices based on comparison between Monarch 3rd Grade 5-Subject Set and A Beka 3rd Grade child and parent kits for 3rd grade (totaled). Subject prices based on comparison between Monarch 3rd Grade Math (including all student and teacher material, along with supplements) and A Beka Book 3rd Grade Math (including all student and teacher material, along with supplements). Costs do not reflect any additional fees or shipping and handling charges. Prices for other A Beka and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '17552fb9-613c-4ddf-81f2-ad6b1acc4d3e';
  $display->content['new-17552fb9-613c-4ddf-81f2-ad6b1acc4d3e'] = $pane;
  $display->panels['left'][0] = 'new-17552fb9-613c-4ddf-81f2-ad6b1acc4d3e';
  $pane = new stdClass();
  $pane->pid = 'new-f2b9d55a-0ba0-45ee-a2f5-a7d8ccbc1b0f';
  $pane->panel = 'right';
  $pane->type = 'views';
  $pane->subtype = 'testimonials';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 0,
    'nodes_per_page' => '1',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'default',
    'override_title' => 1,
    'override_title_text' => 'Testimonial',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'f2b9d55a-0ba0-45ee-a2f5-a7d8ccbc1b0f';
  $display->content['new-f2b9d55a-0ba0-45ee-a2f5-a7d8ccbc1b0f'] = $pane;
  $display->panels['right'][0] = 'new-f2b9d55a-0ba0-45ee-a2f5-a7d8ccbc1b0f';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-17552fb9-613c-4ddf-81f2-ad6b1acc4d3e';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
