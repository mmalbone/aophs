<?php
/**
 * @file
 * panels_layout.aop_1_1_1_2_layout.inc
 */

$api = '2.0.0';

$data = $layout = new stdClass();
$layout->disabled = FALSE; /* Edit this to true to make a default layout disabled initially */
$layout->api_version = 1;
$layout->name = 'aop_1_1_1_2_layout';
$layout->admin_title = 'AOP 1-1-1-2 Layout';
$layout->admin_description = '';
$layout->category = 'AOP';
$layout->plugin = 'flexible';
$layout->settings = array(
  'items' => array(
    'canvas' => array(
      'type' => 'row',
      'contains' => 'column',
      'children' => array(
        0 => 'main',
      ),
      'parent' => NULL,
      'class' => '',
      'column_class' => '',
      'row_class' => '',
      'region_class' => '',
      'no_scale' => TRUE,
      'fixed_width' => '',
      'column_separation' => '0.5em',
      'region_separation' => '0.5em',
      'row_separation' => '0.5em',
    ),
    'main' => array(
      'type' => 'column',
      'width' => 100,
      'width_type' => '%',
      'children' => array(
        0 => 2,
        1 => 1,
        2 => 'main-row',
        3 => 3,
      ),
      'parent' => 'canvas',
    ),
    'main-row' => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'center',
      ),
      'parent' => 'main',
    ),
    'center' => array(
      'type' => 'region',
      'title' => 'Center',
      'width' => 100,
      'width_type' => '%',
      'parent' => 'main-row',
    ),
    1 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'center_',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    'center_' => array(
      'type' => 'region',
      'title' => 'Center',
      'width' => 100,
      'width_type' => '%',
      'parent' => '1',
      'class' => '',
    ),
    2 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'center__',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    'center__' => array(
      'type' => 'region',
      'title' => 'Center',
      'width' => 100,
      'width_type' => '%',
      'parent' => '2',
      'class' => '',
    ),
    3 => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        0 => 'left',
        1 => 'right',
      ),
      'parent' => 'main',
      'class' => '',
    ),
    'left' => array(
      'type' => 'region',
      'title' => 'Left',
      'width' => 50,
      'width_type' => '%',
      'parent' => '3',
      'class' => '',
    ),
    'right' => array(
      'type' => 'region',
      'title' => 'Right',
      'width' => 50,
      'width_type' => '%',
      'parent' => '3',
      'class' => '',
    ),
  ),
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'panels',
);
