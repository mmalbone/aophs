<?php
/**
 * @file
 * variable.aop_assist_dev_url.inc
 */

$api = '2.0.0';

$data = $strongarm = new stdClass();
$strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
$strongarm->api_version = 1;
$strongarm->name = 'aop_assist_dev_url';
$strongarm->value = 'https://external-mule-dev.owteam.com/order';


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'strongarm',
  1 => 'ctools',
  2 => 'system',
);
