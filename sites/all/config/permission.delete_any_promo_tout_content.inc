<?php
/**
 * @file
 * permission.delete_any_promo_tout_content.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'delete any promo_tout content',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array(
  'content_type.promo_tout' => 'content_type.promo_tout',
);

$optional = array();

$modules = array(
  0 => 'node',
);
