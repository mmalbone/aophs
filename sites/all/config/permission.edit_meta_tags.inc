<?php
/**
 * @file
 * permission.edit_meta_tags.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'edit meta tags',
  'roles' => array(
    0 => 'administrator',
    1 => 'customer support',
    2 => 'marketing',
    3 => 'sales',
    4 => 'tech support',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'metatag',
);
