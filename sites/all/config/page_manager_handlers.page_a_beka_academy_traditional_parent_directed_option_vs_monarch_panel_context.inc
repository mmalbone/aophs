<?php
/**
 * @file
 * page_manager_handlers.page_a_beka_academy_traditional_parent_directed_option_vs_monarch_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_a_beka_academy_traditional_parent_directed_option_vs_monarch_panel_context';
$handler->task = 'page';
$handler->subtask = 'a_beka_academy_traditional_parent_directed_option_vs_monarch';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'omega_24_twocol_18_6';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'left' => NULL,
    'right' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '27048589-543a-4097-b972-c3f7c648189a';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-d9862775-f6a1-4661-81bb-6882145a03f2';
  $pane->panel = 'left';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'A Beka Academy Traditional Parent-Directed Option vs. Monarch',
    'body' => '<p>Looking for the differences between A Beka Academy Traditional Parent-Directed Option and the online Monarch curriculum from Alpha Omega Publications? Look no further! Find short overviews of each curriculum below, an easy-to-view comparison chart, and additional comparison options!</p><p><strong>A Beka Academy Traditional Parent-Directed Option</strong></p><p>A Beka Academy Traditional Parent-Directed Option for homeschooling families is available for students in grades K5-6 and provides a blending of an accredited (FACCS and SACS CASI) distance learning option with a more traditional parent-directed homeschool program. The A Beka Academy Traditional Parent-Directed Option uses structured, print-based curriculum that was originally written for teachers in a Christian school setting. A Beka Book\'s Traditional Parent-Directed Program requires homeschoolers to follow a highly structured, parent-supervised program with all lessons taught by a parent. Teacher\'s guides include lesson plans that are often more suited to a classroom setting and may at times be difficult to adapt to homeschool situations.</p><p>The A Beka Traditional Parent-Directed Option provides all necessary textbooks, tests, and keys, as well as a parent manual with daily lessons plans that follow a 170-day school year. Students must meet age requirements and placement criterion to be enrolled. A Beka Academy provides progress reports for record keeping, evaluation of student work, a report card for each grading period, and one copy of the transcript. Parental responsibilities include assuring compliance with state laws, showing proof of previously completed work, providing supervision for all work, and sending in all required graded work in accordance with the provided academic calendar. Coursework must be completed within ten months of the assigned start date. All A Beka coursework is Bible-based and provides instruction from a Christian worldview. Science courses are strongly creation-based. Courses are available in all core subjects: Bible, English, mathematics, science, and history and geography. Electives are also available.</p><h3><strong>Finding Your Approach to Homeschooling</strong></h3><p>As you consider print-based homeschool curriculum options, remember there is no one perfect homeschool curriculum. A blended approach to homeschooling allows parents the flexibility of using multiple resources instead of one set curriculum. Instead of feeling torn between curriculum formats, parents should be open to mixing and matching materials and resources to customize their child\'s education. Remember, each child has his own learning style, so it\'s important to choose materials that best fit his needs.</p><h3><strong>Monarch from Alpha Omega Publications</strong></h3><p>Monarch from Alpha Omega Publications is an interactive, Internet-based curriculum for grades 3-12. Alpha Omega Publications, a trusted publisher of high quality educational products, provides Christian curriculum and educational services to homeschool families. Transforming the way students learn, Monarch is an in-depth, online curriculum that makes use of today\'s 21st century technology. This innovative, cutting-edge AOP curriculum presents Christ-centered lessons using eye-catching animations, learning games, video and audio clips, and more! Unlike traditional textbooks, Monarch has a diverse mix of text-based instruction and engaging multimedia enrichment. Time-saving teaching tools provide automatic grading, lesson planning, customizable lessons, a built-in calendar, printing options, message center, text-to-speech, and more!</p><p>Compatible with both Windows® and Macintosh® operating systems, Monarch integrates Scripture and a biblical worldview into all subjects. Monarch offers the flexibility to log on and learn anytime, anywhere with 18-month, single subscriptions using an individualized, student driven approach. Diagnostic tests are available for placement in math and language arts. Core coursework includes Bible (3-12), language arts (3-12), math (3-12), science (3-12), and history and geography (3-12). Over 35 electives are also available. Monarch may be purchased as individual subjects or as complete five-subject sets.</p><h3><strong>Benefits and Features of Monarch from Alpha Omega Publications</strong></h3><p class="list_heading">Monarch</p><ul><li>online interactive lessons with anytime, anywhere access</li><li>compatible with both Windows® and Mac® operating systems</li><li>offers customizable student learning options and time-saving teaching tools</li><li>features dynamic, media-rich lessons; real-time content updates; and data storage</li></ul>
<h3 class="para-start"><strong>Take a Closer Look at A Beka Academy Traditional Parent-Directed Option and Monarch</strong></h3>
<div class="comp_chart">
<table>
<thead>
<tr><th class="features">Offerings</th><th class="compare">Monarch</th><th class="compare">A Beka Academy Traditional Parent-Directed Option (with A Beka Books)</th></tr>
</thead>
<tbody>
<tr>
<td>3-12 curriculum options</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Bible-based</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Accredited program</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Core subjects and electives available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr>
<td>Parent-directed curriculum</td>
<td class="tac">&nbsp;</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
</tr>
<tr class="alt">
<td>Internet-based curriculum</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Interactive, engaging multimedia tools</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Customizable lessons and calendar</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Placement tests available</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Automatic grading and lesson planning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Automatic updated material</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Flexible and individualized</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr>
<td>Mastery-based learning</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
<tr class="alt">
<td>Message center</td>
<td class="tac"><img src="sites/all/themes/aop/images/check.png" alt="Checkmark"></td>
<td class="tac">&nbsp;</td>
</tr>
</tbody>
</table>
<table id="price_chart">
<thead>
<tr><th class="features">Cost</th><th class="compare">Monarch</th><th class="compare">A Beka Academy Traditional Parent-Directed Option (with A Beka Books)</th></tr>
</thead>
<tbody>
<tr>
<td>Average yearly cost per grade level**</td>
<td class="tac">$449.95</td>
<td class="tac">$750</td>
</tr>
<tr class="alt">
<td>Average subject cost per grade level**</td>
<td class="tac">$99.95</td>
<td class="tac">N/A</td>
</tr>
</tbody>
</table>
</div>
<h4>Compare Offerings: A Beka versus Alpha Omega Publications</h4>
<p class="list_heading_links">A Beka Book</p>
<ul>
<li><a href="/abeka-book-vs-monarch">A Beka Book vs. Monarch</a></li>
<li><a href="/abeka-book-vs-horizons">A Beka Book vs. Horizons</a></li>
<li><a href="/abeka-book-vs-lifepac">A Beka Book vs. LIFEPAC</a></li>
<li><a href="/abeka-book-vs-aoa">A Beka Book vs. Alpha Omega Academy</a></li>
<li><a href="/abeka-book-vs-sos">A Beka Book vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">A Beka Academy (DVD)</p>
<ul>
<li><a href="/abeka-dvd-vs-monarch">A Beka Academy (DVD) vs. Monarch</a></li>
<li><a href="/abeka-dvd-vs-horizons">A Beka Academy (DVD) vs. Horizons</a></li>
<li><a href="/abeka-dvd-vs-lifepac">A Beka Academy (DVD) vs. LIFEPAC</a></li>
<li><a href="/abeka-dvd-vs-aoa">A Beka Academy (DVD) vs. Alpha Omega Academy</a></li>
<li><a href="/abeka-dvd-vs-sos">A Beka Academy (DVD) vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">A Beka Academy (Parent-Directed)</p>
<ul>
<li><a href="/abeka-parent-directed-vs-monarch">A Beka Academy (Parent-Directed) vs. Monarch</a></li>
<li><a href="abeka-parent-directed-vs-horizons">A Beka Academy (Parent-Directed) vs. Horizons</a></li>
<li><a href="/abeka-parent-directed-vs-lifepac">A Beka Academy (Parent-Directed) vs. LIFEPAC</a></li>
<li><a href="/abeka-parent-directed-vs-aoa">A Beka Academy (Parent-Directed) vs. Alpha Omega Academy</a></li>
<li><a href="/abeka-parent-directed-vs-sos">A Beka Academy (Parent-Directed) vs. Switched-On Schoolhouse</a></li>
</ul>
<p class="list_heading_links">A Beka Video-based Internet Streaming</p>
<ul>
<li><a href="/abeka-academy-streaming-video-vs-monarch">A Beka Video-based Internet Streaming vs. Monarch</a></li>
<li><a href="/abeka-academy-streaming-video-vs-horizons">A Beka Video-based Internet Streaming vs. Horizons</a></li>
<li><a href="/abeka-academy-streaming-video-vs-lifepac">A Beka Video-based Internet Streaming vs. LIFEPAC</a></li>
<li><a href="/abeka-academy-streaming-video-vs-aoa">A Beka Video-based Internet Streaming vs. Alpha Omega Academy</a></li>
<li><a href="/abeka-academy-streaming-video-vs-sos">A Beka Video-based Internet Streaming vs. Switched-On Schoolhouse</a></li>
</ul>
<p>*A Beka Book® and A Beka Academy® are the registered trademarks and subsidiaries of Pensacola Christian College. Alpha Omega Publications is not in any way affiliated with A Beka®. Alpha Omega Publications\' purpose with this page is to provide information to families who are considering the different aspects of homeschooling curricula or who wish to compare the benefits of using both curricula. Alpha Omega Publications\' products and services are not endorsed or sponsored by A Beka Book or A Beka Academy.</p>
<p class="note"><strong>Note:</strong>&nbsp;Information in this chart was gathered in February 2013 and was obtained from company websites, catalogs, research of rating companies, and publicly filed information. Comparison categories do not encompass entire curriculum features and benefits and may not apply to all situations.</p><p>**Prices obtained February 2013 from A Beka Academy\'s website and from Alpha Omega Publications 2013 Homeschool Catalog. Yearly prices based on comparison between SOS 3rd Grade 5-Subject Set and and A Beka Academy total tuition fee for a full elementary grade. Subject prices based on comparison between SOS 3rd Grade Math (including all student and teacher material, along with supplements) and A Beka Academy does not offer individual courses in the Traditional Parent-Directed Option. Costs do not reflect any additional fees or shipping and handling charges. Prices for other A Beka and Alpha Omega Publications curricula and services can be found on individual comparison pages. All prices are subject to change.</p>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'd9862775-f6a1-4661-81bb-6882145a03f2';
  $display->content['new-d9862775-f6a1-4661-81bb-6882145a03f2'] = $pane;
  $display->panels['left'][0] = 'new-d9862775-f6a1-4661-81bb-6882145a03f2';
  $pane = new stdClass();
  $pane->pid = 'new-ddefb4ef-5e17-4a91-a826-38f72aacbc14';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'testimonials-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'ddefb4ef-5e17-4a91-a826-38f72aacbc14';
  $display->content['new-ddefb4ef-5e17-4a91-a826-38f72aacbc14'] = $pane;
  $display->panels['right'][0] = 'new-ddefb4ef-5e17-4a91-a826-38f72aacbc14';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-d9862775-f6a1-4661-81bb-6882145a03f2';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.testimonials' => 'views_view.testimonials',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
