<?php
/**
 * @file
 * content_type.dvd.inc
 */

$api = '2.0.0';

$data = (object) array(
  'base' => 'node_content',
  'description' => '',
  'has_title' => '1',
  'help' => '',
  'name' => 'Family Entertainment',
  'title_label' => 'Title',
  'type' => 'dvd',
);

$dependencies = array();

$optional = array(
  'field.node.field_product.dvd' => 'field.node.field_product.dvd',
  'field.node.field_search_boost.dvd' => 'field.node.field_search_boost.dvd',
  'field.node.title_field.dvd' => 'field.node.title_field.dvd',
  'permission.create_dvd_content' => 'permission.create_dvd_content',
  'permission.delete_any_dvd_content' => 'permission.delete_any_dvd_content',
  'permission.delete_own_dvd_content' => 'permission.delete_own_dvd_content',
  'permission.edit_any_dvd_content' => 'permission.edit_any_dvd_content',
  'permission.edit_own_dvd_content' => 'permission.edit_own_dvd_content',
);

$modules = array(
  0 => 'node',
);
