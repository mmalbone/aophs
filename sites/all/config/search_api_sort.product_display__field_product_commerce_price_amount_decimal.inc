<?php
/**
 * @file
 * search_api_sort.product_display__field_product_commerce_price_amount_decimal.inc
 */

$api = '2.0.0';

$data = entity_import('search_api_sort', '{
    "index_id" : "product_display",
    "field" : "field_product_commerce_price_amount_decimal_asc",
    "name" : "Price",
    "enabled" : "1",
    "weight" : "0",
    "identifier" : "product_display__field_product_commerce_price_amount_decimal",
    "default_sort" : "0",
    "default_sort_no_terms" : "0",
    "default_order" : "asc",
    "options" : { "field_name" : "Search API ranges (Min)" }
  }');

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'entity',
  1 => 'search_api_sorts',
);
