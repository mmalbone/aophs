
<article<?php print $attributes; ?>>

<?php 
  $sku = strip_tags($content['product:sku']['#markup']);
  $sku = trim(str_replace("SKU:", "", $sku));
?>

  <div class="aop-product-detail-page-wrapper">

   <div class="aop-product-detail-page-header">

      <div class="aop-product-image-wrapper">
        <?php
          $cdn_url   = variable_get('aop_images_url');  
          if (isset($content['product:field_product_image']) && sizeof($content['product:field_product_image']) > 0) { 
             $image_url = $content['product:field_product_image']['#items'][0]['value'];
          } else { 
             $image_url = "/no_image_found.jpg";
          }
          $full_image_url = $cdn_url.$image_url;        
        ?>
        <img src=<?php print $full_image_url; ?> alt=''/>
      </div><!--end of product image wrapper-->

      <div class="aop-product-description-wrapper">

        <div class="aop-product-title">
             <h2><?php print render($content['title_field']); ?></h2>
        </div>
        <div class="aop-product-rate-it">
            <?php print render($content['field_rate_it']); ?>
        </div>
        <div class="aop-product-description">
            <?php print render($content['product:field_hsc_long_description']); ?>
        </div>
        <div class="aop-product-sku">
           PRODUCT ID <?php print $sku; ?> / AVAILABILITY - NOW SHIPPING
        </div>
      </div><!-- end of product description wrapper -->
    </div><!-- end of detail page header-->

    <div class="aop-product-spec-wrapper">

    <div class="single-product-spec-cart clear">
      <div class="single-product-spec clear">
        <div class="single-product-spec-ataglance">
          <h2>At a Glance</h2>
          <?php 
            foreach($content['product:field_feature']['#items'] as $feature) { 
                 $feature_elements = explode(":", $feature['value']);
                 // render the feature 
                 print "<div>";
                 print '<span class="'.trim($feature_elements[0]).'"></span>';
                 print trim($feature_elements[1]);
                 print "</div>";
            }
          ?>
        </div>
        <div class="single-product-spec-calltoaction">

          <?php 
            $text = aop_utility_loadAtAGlanceSection($content['field_product']['#items'][0]['product_id']);
            print $text; 
          ?>
          
          <?php if(isset($content['product:field_has_demo']) && sizeof($content['product:field_has_demo']) > 1) { 
            $has_demo = $content['product:field_has_demo']['#items'][0]['value']; 
          } else { 
            $has_demo = 'no';
          }
          ?>
          <?php if($has_demo == 'yes'): ?>
              <div><span class="presentation-icon"></span><a class="colorbox-load" href="https://monarch.aop.com/api/render_lesson/<?php print variable_get('aop_product_demo_year').'_'.strtoupper($sku); ?>?width=70%25&height=70%25&iframe=true">Sample This Course</a></div>
          <?php endif; ?>        
        </div>
      </div>
      <div class="single-product-cart">
        <div class="single-product-cart-price"><?php print render($content['product:commerce_price']); ?></div>
        <div class="single-product-cart-addtocart"><?php print render($content['field_product']); ?></div>
      </div>
    </div>
   </div><!-- end of product spec wrapper -->
  </div><!-- end of detail page wrapper -->
</article>

<div class="single-product-recommended">
    <div class="single-product-recommended-header clear">
      <span class="single-product-recommended-header-title"><h2>Recommended with these products and tools</h2></span>
      <!-- <span class="single-product-recommended-header-action">SEE MORE RESOURCES</span> -->
    </div>
    <div class="single-product-recommended-carousel clear">
      <?php
        //load the related products view by name and product id
        print views_embed_view('recommended_products', 'recommended_products');
      ?>
    </div>
  </div>
</div>

