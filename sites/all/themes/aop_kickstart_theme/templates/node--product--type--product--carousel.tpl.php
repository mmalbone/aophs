<article<?php print $attributes; ?>>
  <div<?php print $content_attributes; ?>>
   <div class="aop-product-carousel-listing-wrapper">
     <div class="aop-product-carousel-image-wrapper">
        <?php
           $cdn_url   = variable_get('aop_images_url');  
           if (isset($content['product:field_product_image']) && sizeof($content['product:field_product_image']) > 0) { 
              $image_url = $content['product:field_product_image']['#items'][0]['value'];
           } else { 
              $image_url = "/no_image_found.jpg";
           }
           $full_image_url = $cdn_url.$image_url;        
        ?>
        <img src=<?php print $full_image_url; ?> alt=''/>
     </div><!--end of product image wrapper-->
     <div class="aop-product-carousel-item-details-wrapper">
         <div class="aop-product-carousel-item-title">
            <?php print render($content['title_field']); ?>
         </div>
         <div class="aop-product-carousel-item-short-description">
            <?php print render($content['product:field_hsc_short_description']); ?>
         </div>
         <div class="aop-product-carousel-item-sku">
            <?php print render($content['product:sku']); ?>
         </div>
         <div class="aop-product-carousel-item-price">
            <?php print render($content['product:commerce_price']); ?>
         </div>
   </div><!--end of product carousel item wrapper -->
  </div>
 </div>
</article>
