<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */

$titleText = ($view->field['title']->last_render);
$result= str_replace('Monarch - Individual Monthly Subscription', '<em>'.'Purchase Monthly'.'</em><br/><strong>'.'Individual Plan'.'</strong>', $titleText);
$result= str_replace('Monarch - Individual Yearly Subscription', '<em>'.'Purchase 1 - Year'.'</em><br/><strong>'.'Individual Plan'.'</strong>', $result);
$result= str_replace('Monarch - Family Monthly Subscription', '<em>'.'Purchase Monthly'.'</em><br/><strong>'.'Family Plan'.'</strong>', $result);
$result= str_replace('Monarch - Family Yearly Subscription', '<em>'.'Purchase 1 - Year'.'</em><br/><strong>'.'Family Plan'.'</strong>', $result);
print $result;
?>