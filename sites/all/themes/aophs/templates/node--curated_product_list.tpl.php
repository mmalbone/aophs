<article<?php print $attributes; ?>>

<ul class="all-products">

<?php

  if (isset($content['field_curated_product']) && sizeof($content['field_curated_product']) > 0) {

  foreach($content['field_curated_product'] as $item) { 

     if (is_array($item) && isset($item['node'])) { 

       foreach($item['node'] as $product) { 

        if (isset($product['title_field'])) {


          //new dBug($product);
          //die();


        ?>

        <li class='views-row'>
          <div class="aop-product-listing-wrapper clearfix">
            <div class="aop-product-image-wrapper ">
              <?php
                $cdn_url   = variable_get('aop_images_url');  
                if (isset($product['product:field_product_image']) && sizeof($product['product:field_product_image']) > 0) { 
                   $image_url = $product['product:field_product_image']['#items'][0]['value'];
                } else { 
                   $image_url = "/no_image_found.jpg";
                }
                $full_image_url = $cdn_url.$image_url;        
              ?>
              <img src=<?php print $full_image_url; ?> alt=''/>
            </div><!--end of product image wrapper-->

            <div class="aop-product-item-details-wrapper clearfix">
    
              <div class="aop-product-item-details-left">

              <div class="aop-product-item-title">
                <?php print render($product['title_field']); ?>
              </div>

                <div class="aop-product-item-short-description">
                   <?php print render($product['product:field_hsc_short_description']); ?>
                </div>
                <div class="aop-product-item-sku">
                   <?php print render($product['product:sku']); ?>
                </div>
              </div>

              <div class="aop-product-item-details-right">
                <div class="aop-product-item-price">
                    <?php print render($product['product:commerce_price']); ?>
                </div>
              </div>
            </div>
          </div>

        </li>

<?php
        } // end of isset
      } // end of innner for each
    } // end of if array/node
  } // end of foreach
} // end of if
?>

</ul>

</article>
