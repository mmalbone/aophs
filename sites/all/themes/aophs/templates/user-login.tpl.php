<?php

//check if current is set.
if(isset($_GET['current'])) {
	//redirect to checkout
	if($_GET['current'] == "cart") {
	header("Location: /user/login?current=checkout");
	}
}


//Set the form variables.
$login_form = drupal_get_form('user_login_block');
$registration_form = drupal_get_form('user_register_form');
$errors = array();
$errors = form_get_errors();
drupal_get_messages();

?>
<div id="login-register-wrapper">
	<div class="login-block-left">
		<h2>Returning Customers</h2>
		<p>Forgot your password? <a href='/user/password'>Click here</a></p>
		<?php
		
		if(isset($login_form['#validated'])) {
			if(!empty($errors)) {
				print "<div class='user-error-items'>";
				foreach ($errors as $error) {
					print "<div>" . $error . "</div>";
				}
				print "</div>";
			}
		}
		
		//Render login form.
		print drupal_render($login_form);
		
		//print_r($login_form);
		?>
	</div>
	<div class="login-block-right">
		<h2>New Customers</h2>
		<p>Are you a new customer? We'll need a few pieces of information before you can check out.</p>
		<div id="live-validation-errors"></div>
		<?php
		
		if(isset($registration_form['#validated'])) {
			if(!empty($errors)) {
				print "<div class='user-error-items'>";
				foreach ($errors as $error) {
					print "<div>" . $error . "</div>";
				}
				print "</div>";
			}
		}
		
		//Render registration form
		print drupal_render($registration_form);
		?>
	</div>
</div>