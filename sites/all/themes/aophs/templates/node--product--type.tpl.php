<?php
//dpm($content);
?>
<article<?php print $attributes; ?>>

<?php 
  $sku = strip_tags($content['product:sku']['#markup']);
  $sku = trim(str_replace("SKU:", "", $sku));
?>

  <div class="aop-product-detail-page-wrapper clearfix">

   <div class="aop-product-detail-page-header clearfix">

      <div class="aop-product-image-wrapper">
        <?php
          $cdn_url   = variable_get('aop_images_url');  
          if (isset($content['product:field_product_image']) && sizeof($content['product:field_product_image']) > 0) { 
             $image_url = $content['product:field_product_image']['#items'][0]['value'];
          } else { 
             $image_url = "/no_image_found.jpg";
          }
          $full_image_url = $cdn_url.$image_url;        
        ?>
        <img src=<?php print $full_image_url; ?> alt=''/>
      </div><!--end of product image wrapper-->

      <div class="aop-product-description-and-price-wrapper clearfix">
        


        <div class="aop-product-title-rating-reviews">
          <div class="aop-product-title">
            <?php print render($content['title_field']); ?>
          </div>
          <div class="aop-product-rate-it">
            <?php // print render($content['field_rate_it']); ?>
          </div>
        </div>
        <div class="single-product-cart">
          <div class="single-product-cart-price"><?php print render($content['product:commerce_price']); ?></div>
          <div class="aop-product-sku"><b>PRODUCT ID:</b> <?php print $sku; ?> <!-- / AVAILABILITY - NOW SHIPPING --></div>
          <div class="single-product-cart-addtocart"><?php print render($content['field_product']); ?></div>
        </div><!-- end single-product-cart -->

        <div class="aop-product-description-wrapper">
  

  
          <div class="aop-product-description">
              <?php 
                 if (isset($content['body'][0]['#markup']) && 
                    strlen($content['body'][0]['#markup']) > 0) { 
                       print render($content['body']); 
                 } else { 
                   print render($content['product:field_hsc_long_description']); 
                 }
              ?>
          </div>
        </div><!-- end of product description wrapper -->
        
      </div><!-- end aop-product-description-and-price-wrapper -->
    </div><!-- end of detail page header-->

    <div class="aop-product-spec-wrapper">

    <div class="single-product-spec-cart clearfix">
      <?php if(isset($content['product:field_feature']['#items']) && 
              sizeof($content['product:field_feature']['#items']) > 0) { ?>
      <div class="single-product-spec clearfix">
        <div class="single-product-spec-ataglance">
          <h2>At a Glance</h2>
          <?php 
            foreach($content['product:field_feature']['#items'] as $feature) { 
                 $feature_elements = explode(":", $feature['value']);
                 // render the feature 
                 print "<div>";
                 print '<span class="'.trim($feature_elements[0]).'"></span>';
                 if (sizeof($feature_elements) > 0) { 
                    print trim($feature_elements[1]);
                 } 
                 print "</div>";
            }
          ?>
        </div> <!-- end of at a glance -->
        <div class="single-product-spec-calltoaction">

          <?php
/*
            if(module_exists("colorbox")) {
              print "Colorbox Enabled";
            }
*/
            //width=500&height=500&inline=true#
            //[path]?width=500&height=500&iframe=true
            // $content['links']['forward']['#links']['forward_link']['href']
            //print "<a class='colorbox-load' href='/".$content['links']['forward']['#links']['forward_link']['href']."/".$content['links']['forward']['#links']['forward_link']['query']['path']."?width=70%25&height=70%25&iframe=true'>EMAIL</a>";
            print render($content['links']['forward']);
            
            $text = aop_utility_loadAtAGlanceSection($content['field_product']['#items'][0]['product_id']);
            print $text; 
          ?>
          
          <?php 
        
          $has_demo = 'no';

          if(isset($content['product:field_has_demo']) && sizeof($content['product:field_has_demo']) > 1) { 
            if (isset($content['product:field_has_demo']['#items'])) {
              $has_demo = $content['product:field_has_demo']['#items'][0]['value']; 
            }
          } 
          ?>
          <?php if($has_demo == 'yes'): ?>
              <li class="presentation-icon"><a class="colorbox-load" href="https://monarch.aop.com/api/render_lesson/<?php print variable_get('aop_product_demo_year').'_'.strtoupper($sku); ?>?width=70%25&height=70%25&iframe=true">Sample This Course</a></li>
          <?php endif; ?>        
        </div>
      </div>
      <?php } ?>

    </div>
   </div><!-- end of product spec wrapper -->
  </div><!-- end of detail page wrapper -->
</article>

<div class="single-product-recommended">
    <div class="single-product-recommended-header clearfix">
      <span class="single-product-recommended-header-title"><h2>Recommended with these products and tools</h2></span>
      <!-- <span class="single-product-recommended-header-action">SEE MORE RESOURCES</span> -->
    </div>
    <div class="single-product-recommended-carousel clearfix">
      <?php
        //load the related products view by name and product id        
        $block = module_invoke('views', 'block_view', '2a6d22db81fe1a6db7a3b57fab1a66d8');
  			print render ($block);  			
      ?>
    </div>
  </div>
</div>
<div class="single-product-reviews">
  <?php
    //dpm($content['comments']);
    print render($content['comments']);
  ?>
</div>