<?php ?>
<div class="review-panes clearfix">
  <?php 
	foreach ($variables['form']['#data'] as $pane_id => $data):
	$labels = $data['title'];
	//$contents = str_replace(' </label>', ':</label>', $data['data']);
	$contents = $data['data'];
  ?>
  <div class="review-pane <?php print $pane_id; ?>">
	<h3 class="pane-title"><?php print $labels; ?></h3>
		<?php print $contents; ?>
    </div>
  <?php endforeach;?>
</div>
<?php  ?>
