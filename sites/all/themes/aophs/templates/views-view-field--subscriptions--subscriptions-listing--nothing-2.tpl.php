<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */

// cancel = subscriptions/cancel/[title]?destination=user/[field_customer_uid]/subscriptions
// renew  = subscriptions/renew/[title]
?>

<?php

  global $base_url;

  // generate correct link based on conditions of the subscription and subscription type
  // renewal flag of 1 = monthly auto renew
  // renewal flag of 0 = 12 month or 18 month non-autorenew
  // active flag of A = still active product
  // active flag of I = inactive product

  // scenarios as per WEB-1416 spreadsheet
  // Row 2 - status = 0, auto renew = 0 -- action = renew
  // Row 3 - status = 0, auto renew = 1 -- action = renew
  // Row 4 - status = 1, auto renew = 0 -- action - Restart auto billing
  // Row 5 - status = 1, auto renew = 1 -- action nothing

  if (sizeof($row->_field_data['nid']['entity']->field_activation_date['und']) < 1) { 
     print "";
  } else 
    // its a monthly subscription and can be renewed
    if ($row->_field_data['nid']['entity']->field_subscription_renewal_perio['und'][0]['value'] === "1") { 
      // defaul scenario - its active and not expired and not cancelled
      if ($row->_field_data['nid']['entity']->field_renewal_flag['und'][0]['value'] === "1"  &&       
          $row->_field_data['nid']['entity']->field_active_flag['und'][0]['value'] === "A") { 
             $url = '/subscriptions/cancel/'.$row->_field_data['nid']['entity']->title."?destination=/user/".$row->_field_data['nid']['entity']->uid."/subscriptions";
             print "<a href='".$url."'>Cancel Auto Billing</a>";
       } else
       // Row 4 in spread sheet  
       if ($row->_field_data['nid']['entity']->field_active_flag['und'][0]['value'] === "A" &&
           $row->_field_data['nid']['entity']->field_renewal_flag['und'][0]['value'] < 1) { 
              $url = '/subscriptions/restart/'.$row->_field_data['nid']['entity']->title;
              print "<a href='".$url."'>Restart Auto Billing</a>";
       } else  
          // Row 5 in spreadsheet
          if ($row->_field_data['nid']['entity']->field_active_flag['und'][0]['value'] === "A" &&
              $row->_field_data['nid']['entity']->field_renewal_flag['und'][0]['value'] === "1") { 
                 print "";
       } else  
          // row 2 in spreadsheet
          if ($row->_field_data['nid']['entity']->field_active_flag['und'][0]['value'] === "I" &&
              $row->_field_data['nid']['entity']->field_renewal_flag['und'][0]['value'] === "0") { 
                $url = '/subscriptions/renew/'.$row->_field_data['nid']['entity']->title;
                print "<a href='".$url."'>Renew</a>";
       } else 
          // row 3 in spreadsheet 
          if ($row->_field_data['nid']['entity']->field_active_flag['und'][0]['value'] === "I" &&
              $row->_field_data['nid']['entity']->field_renewal_flag['und'][0]['value'] === "1") { 
                $url = '/subscriptions/renew/'.$row->_field_data['nid']['entity']->title;
                print "<a href='".$url."'>Renew</a>";
       }
  } else 
    // its not a monthly subscription - so show renew when appropriate
    if (($row->_field_data['nid']['entity']->field_active_flag['und'][0]['value'] === "I") || 
        ($row->_field_data['nid']['entity']->field_active_flag['und'][0]['value'] === "A")) {
                $url = '/subscriptions/renew/'.$row->_field_data['nid']['entity']->title;
                print "<a href='".$url."'>Renew</a>";
  }

  
  