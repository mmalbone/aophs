<?php

drupal_get_messages('error');

/**
 * @file
 * Customize confirmation screen after successful submission.
 *
 * This file may be renamed "webform-confirmation-[nid].tpl.php" to target a
 * specific webform e-mail on your site. Or you can leave it
 * "webform-confirmation.tpl.php" to affect all webform confirmations on your
 * site.
 *
 * Available variables:
 * - $node: The node object for this webform.
 * - $progressbar: The progress bar 100% filled (if configured). This may not
 *   print out anything if a progress bar is not enabled for this node.
 * - $confirmation_message: The confirmation message input by the webform
 *   author.
 * - $sid: The unique submission ID of this submission.
 */
?>

<div class="news-form-block-wrapper">
	<div class="sidebar-academic-support">
	<h1>Thank You</h1>
	<h3>for your interest in AOP!</h3>
	</div>
	<div class="news-form-block">
		
		<a href="https://glnmedia.s3.amazonaws.com/pdf/brochures/2013_AOP_Brochure.pdf" download="AOP_Brochure.pdf" target="_blank" download>
		<div class="news-form-download-block">
				<div class="news-form-download-block-right">Click to download your <br><strong>FREE BROCHURE</strong></div>
				<div class="news-form-download-block-left">
				<img height="80" width="80" src="https://glnenews.s3.amazonaws.com/download-icon.png">
				</div>
		</div>
		</a>
		
		<div class="new-form-block-divider"></div>
		
	<div class="news-form-ebook-block">
		<div class="news-form-ebook-block-left"><img height="110" width="110" src="https://glnenews.s3.amazonaws.com/homeschooling-ebook-icon.png"></div>
		<div class="news-form-ebook-block-right">
		<h3>Receive a Free Bonus Gift!</h3>
		<p>
		Sign up to receive our free e-book </br><em>Seven Steps to Start Homeschooling</em>.</p>
		</div>
	</div>
	
  	<?php
      $nid = array(19034);
      $niv = node_view(node_load('19034'));
      print drupal_render($niv);
      ?>
	</div>
</div>