<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */

//Set default values.
$total_items = 0;
$total_price = "";
$items_statement = "";

//Set the user values.
if(isset($view->result[0]->field_commerce_total[0]['rendered'])) {
$total_items = intval($view->result[0]->commerce_line_item_field_data_commerce_line_items_quantity);
$total_price = $view->result[0]->field_commerce_total[0]['rendered'];
}

//Calculate the statement to be shown to the user.
if($total_items == 1) {
$items_statement = "1 item";
} elseif ($total_items > 1) {
$items_statement = $total_items . " items";
}

?>
<a href="/cart">
<div class="shopping-cart-summary-container"
	<div class="shopping-cart-icon">
		<img height="26" width="26" src="/sites/all/themes/aophs/images/shopping-cart-icon-white.png">
	</div>
	<?php if($total_items > 0) { ?>
	<div class="shopping-cart-number-items">
		<?php echo $total_items; ?>
	</div>
	<?php } ?>
</div>
</a>
<div class="shopping-cart-summary-dropdown">
	<div class="shopping-cart-summary-details">
	<?php if ($total_items > 0) { ?>
		<div class="shopping-cart-summary-items-count">
		<a href="/cart"><span><?php echo $items_statement; ?></span></a> in your cart.</div>
		<div class="shopping-cart-summary-total">
			<div class="total-headline">Total: </div><div class="total-price">
				<strong><?php echo $total_price; ?></strong>
			</div>
			<div class="clearfix"></div>
		</div>
	<?php } else { ?>
	
		<div class="shopping-cart-summary-empty">There are <strong>no items</strong> in<br> your shopping cart.</div>
	<?php } ?>
	</div>
	<div class="shopping-cart-summary-dropdown-cta">
		<a href="/cart">
		<div class="shopping-cart-summary-dropdown-btn">View CART</div>
		</a>
	</div>
</div>
</div>
