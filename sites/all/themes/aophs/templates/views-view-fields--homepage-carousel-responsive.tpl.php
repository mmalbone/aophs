<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates

 */
 
 //Assign a carousel item id for javascript manipulation.
 $item_id = $fields['field_carousel_item_title']->raw;

?>
<a class="field_carousel_item_url" href="<?php echo $fields['field_carousel_item_url']->content; ?>">
	<div class="homepage-carousel-item">
	
		<!--	Carousel item default state.-->
		<div class="homepage-carousel-item-default-state">
			<div class="homepage-carousel-item-logo">
				<?php echo $fields['field_carousel_item_logo']->content; ?>
			</div>
		</div>
		<!--	/Carousel item default state.-->
		
		<!--	Carousel item hover state.-->
		<div class="homepage-carousel-item-hover-state" style="display:  none;">
			<div class="homepage-carousel-item-logo">
				<?php echo $fields['field_carousel_item_hover_logo']->content; ?>
			</div>
		</div>
		<!--	/Carousel item hover state.-->
		
		<div class="homepage-carousel-item-title">
			<?php echo $fields['field_carousel_item_title']->content; ?>
		</div>
		<div class="homepage-carousel-item-description">
			<?php echo $fields['field_carousel_short_description']->content; ?>
		</div>
		
		<div class="homepage-carousel-item-button">
			<?php echo strtoupper($fields['field_carousel_button_text']->content); ?>
		</div>
	</div>
</a>
