<article<?php print $attributes; ?>>
  <?php print render($title_prefix); ?>
  <?php if (!$page && $title): ?>
  <header>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  </header>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <footer class="submitted">

  <?php

    $blog_user = "";
    $blog_user_name = "";
    $blog_category = "";

    $blog_user = user_load($variables['uid']);

    if (isset($blog_user->field_firstname['und'][0]['value'])) { 
        $blog_user_name = $blog_user->field_firstname['und'][0]['value']." ";    
    }

    if (isset($blog_user->field_lastname['und'][0]['value'])) { 
        $blog_user_name = $blog_user_name.$blog_user->field_lastname['und'][0]['value'];
    }

    if (isset($node->field_blog_category['und'][0]['tid'])) { 
        $blog_category_term = taxonomy_term_load($node->field_blog_category['und'][0]['tid']);
        $blog_category = $blog_category_term->name;
    }

    if (strlen($blog_user_name) > 0) { 
        $submitted = "<div class='author-wrap'>Submitted by&nbsp;".$blog_user_name."<br>";
        $submitted .= date("F j, Y", $node->created);
        if (strlen($blog_category) > 0) { 
          $submitted .= "<a href='/blog-categories/".str_replace(' ', '-', $blog_category)."' alt=$blog_category>&nbsp;in&nbsp;".$blog_category."</a>";
        }
        $submitted .= "</div>";


      print $variables['user_picture'];
      print $submitted; 
    }
          
  ?>

  <!-- src="//glnnewmedia.s3.amazonaws.com/styles/media_thumbnail/s3/Kids-Finger-Paint.jpg?itok=0jrgaT-O"> -->

  <div<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      print render($content);
    ?>
  </div>

  <div class="clearfix">
    <?php if (!empty($content['links'])): ?>
      <nav class="links node-links clearfix"><?php print render($content['links']); ?></nav>
    <?php endif; ?>
    <div class="aop-blog-post-comments">
       <?php print render($content['comments']); ?>
    </div>
  </div>
</article>
