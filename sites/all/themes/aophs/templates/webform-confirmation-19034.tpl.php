<?php

/**
 * @file
 * Customize confirmation screen after successful submission.
 *
 * This file may be renamed "webform-confirmation-[nid].tpl.php" to target a
 * specific webform e-mail on your site. Or you can leave it
 * "webform-confirmation.tpl.php" to affect all webform confirmations on your
 * site.
 *
 * Available variables:
 * - $node: The node object for this webform.
 * - $progressbar: The progress bar 100% filled (if configured). This may not
 *   print out anything if a progress bar is not enabled for this node.
 * - $confirmation_message: The confirmation message input by the webform
 *   author.
 * - $sid: The unique submission ID of this submission.
 */
 
 //https://glnmedia.s3.amazonaws.com/pdf/eBooks/7_Steps_to_Successful_Homeschooling.pdf
?>

<a href="https://glnmedia.s3.amazonaws.com/pdf/eBooks/7_Steps_to_Successful_Homeschooling.pdf" download="7_Steps_to_Successful_Homeschooling.pdf" target="_blank" download>
<div class="news-form-download-block">
		<div class="news-form-download-block-right">Click to download your <br><strong>FREE EBOOK</strong></div>
		<div class="news-form-download-block-left">
		<img height="80" width="80" src="https://glnenews.s3.amazonaws.com/download-icon.png">
		</div>
</div>
</a>