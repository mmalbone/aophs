<?php

/**
 * @file
 * Customize the display of a complete webform.
 *
 * This file may be renamed "webform-form-[nid].tpl.php" to target a specific
 * webform on your site. Or you can leave it "webform-form.tpl.php" to affect
 * all webforms on your site.
 *
 * Available variables:
 * - $form: The complete form array.
 * - $nid: The node ID of the Webform.
 *
 * The $form array contains two main pieces:
 * - $form['submitted']: The main content of the user-created form.
 * - $form['details']: Internal information stored by Webform.
 *
 * If a preview is enabled, these keys will be available on the preview page:
 * - $form['preview_message']: The preview message renderable.
 * - $form['preview']: A renderable representing the entire submission preview.
 */
?>

<?php
  // Print out the progress bar at the top of the page
  print drupal_render($form['progressbar']);
?>
  <!-- ** The main eblast ** -->
  <table cellpadding="0" cellspacing="0" class="backgroundTable"  align="center" width="600">
      <tr>
          <td>
              <table width="550" align="center" cellspacing="0" cellpadding="0" style="margin-bottom: 0px;">
                  <tr align="left" >
                      <td>
                          <table border="0" cellspacing="0" cellpadding="0" style="margin-bottom:0px;">
                              <tr>
                                  <td>
                                      <table width="550" align="center" cellspacing="0" cellpadding="0" style="margin-bottom:0px;" >
                                          <!-- ** Logo ** -->
                                         
                                          <!-- ** Navigation ** -->
                                          <tr >
                                              <td>
                                                  <!-- ** Main teaser image ** -->
                                                  <table border="0" cellspacing="0" cellpadding="0" >
                                           
                                                      <tr>
                                                        <td style="margin-left: auto; margin-right; ">
                                                         <?php

                                                              // Print out the preview message if on the preview page.
                                                              if (isset($form['preview_message'])) {
                                                                print '<div class="messages warning">';
                                                                print drupal_render($form['preview_message']);
                                                                print '</div>';
                                                              }

                                                              // Print out the main part of the form.
                                                              // Feel free to break this up and move the pieces within the array.
                                                              print drupal_render($form['submitted']);

                                                              // Always print out the entire $form. This renders the remaining pieces of the
                                                              // form that haven't yet been rendered above (buttons, hidden elements, etc).
                                                              print drupal_render_children($form);
                                                        ?>
                                                      </td>                                                           
                                                      </tr>
                                        
                                                  </table>
                                              </td>
                                          </tr>
                                      </table>

                                  </td>
                              </tr>
                          </table>
                      </td>
                  </tr>
              </table>
  
          </td>
      </tr>
  </table>
<?php