<?php 
/**
 * @file
 * Alpha's theme implementation to display a single Drupal page.
 */
?>
<div
  <?php print $attributes; ?>>

  <?php if (isset($page['header'])) : ?>
    <?php // print render($page['header']); ?>
  <?php endif; ?>
  
  <?php if (isset($page['content'])) : ?>
    <?php print render($page['content']); ?>
  <?php endif; ?>  
  
  <!-- For checkout page -->
    
    <?php print aophs_checkout_footer(); ?>
</div>