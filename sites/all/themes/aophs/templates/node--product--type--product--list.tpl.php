<article<?php print $attributes; ?>>
  <div<?php print $content_attributes; ?>>
   <div class="aop-product-listing-wrapper clearfix">
   <!--begin product image link-->
   <a href="<?php print "/" . drupal_lookup_path('alias','node/'.$content['title_field']['#object']->nid ); ?>">
     <div class="aop-product-image-wrapper ">
        <?php
           $cdn_url   = variable_get('aop_images_url');  
           if (isset($content['product:field_product_image']) && sizeof($content['product:field_product_image']) > 0) { 
              $image_url = $content['product:field_product_image']['#items'][0]['value'];
           } else { 
              $image_url = "/no_image_found.jpg";
           }
           $full_image_url = $cdn_url.$image_url;        
        ?>
        <img src=<?php print $full_image_url; ?> alt=''/>
     </div><!--end of product image wrapper-->
     </a>
     <!--end product image link-->
     <div class="aop-product-item-details-wrapper clearfix">
       <div class="aop-product-item-details-left">
         <div class="aop-product-item-title">
            <?php print render($content['title_field']); ?>
         </div>
         <div class="aop-product-item-short-description">
            <?php print render($content['product:field_hsc_short_description']); ?>
         </div>
         <div class="aop-product-item-sku">
            <?php print render($content['product:sku']); ?>
         </div>
       </div>
       <div class="aop-product-item-details-right">
         <div class="aop-product-item-price">
            <?php print render($content['product:commerce_price']); ?>
         </div>
       </div>
     </div>
   </div><!--end of product listing wrapper -->
  </div>
</article>
