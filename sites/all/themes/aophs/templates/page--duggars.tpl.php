<?php 
/**
 * @file
 * Alpha's theme implementation to display a single Drupal page.
 */
  drupal_add_css(drupal_get_path('theme', 'aophs') . '/css/duggars.css', array('every_page' => FALSE));
  // Create a variable to hold the full path, in our theme, to the image.
  //   path_to_theme() takes care of creating the correct path for the active theme (which is likely your own custom one)
  $duggars_banner = path_to_theme() . '/images/duggar-header_2.png';
?>
<div
  <?php print $attributes; ?>>
  
  <?php if (isset($page['content'])) : ?>
    <div class="duggar-banner">
      <img src="<?php print $duggars_banner ?>" />
    </div>
    <?php print render($page['content']); ?>

  <?php endif; ?> 
  <?php if (isset($page['footer'])) : ?>
    <?php //print render($page['footer']); ?>
    <div class="footer-wrapper">
      <div class="footer-logo">
        <a href="https://www.aophomeschooling.com/">
          <img src="https://glnmedia.s3.amazonaws.com/landing/duggars/AOP-logo.png">
        </a>
      </div>
      <div class="homeschool-info">
        <p>UNCOMMON HOMESCHOOLING<br>
            800.622.3070<br>
            <a href="https://www.aophomeschooling.com/">www.aophomeschooling.com</a></p>
      </div>
    </div>
  <?php endif; ?> 
    
</div>