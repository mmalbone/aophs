(function($){
	$(document).ready(function(){
		var layouts = [
			{
				name: 'aop_1_1_1_2_layout',
				image: 'sites/all/themes/aophs/images/aoplayouts/aop1112.png'
			},
			{
				name: 'aop_75_25_layout',
				image: 'sites/all/themes/aophs/images/aoplayouts/aop112.png'
			},
			{
				name: 'aop_1_2_layout',
				image: 'sites/all/themes/aophs/images/aoplayouts/aop12.png'
			},
			{
				name: 'aop_1_3_2_layout',
				image: 'sites/all/themes/aophs/images/aoplayouts/aop132.png'
			},
			{
				name: 'aop_1_3_3_1_layout',
				image: 'sites/all/modules/panels/plugins/layouts/flexible/flexible.png'
			},
			{
				name: 'aop_75_25_bottom_row',
				image: 'sites/all/modules/panels/plugins/layouts/flexible/flexible.png'
			},
			{
				name: 'aop_homepage',
				image: 'sites/all/modules/panels/plugins/layouts/flexible/flexible.png'
			}

		];
		
		

		$('#ajax-link').ajaxComplete(function() {
			for (var i=0; i<layouts.length; i++){
				var a_tag = $('div.layout-link').children('a[href$="'+layouts[i].name+'"]');
				a_tag.children('img').attr('src', layouts[i].image);
			}
		});
	});
})(jQuery);