(function ($) {
  // All aop_kickstart_theme jquery script goes here
  Drupal.behaviors.aophs_theme = {
    attach: function (context, settings) {

      // Hide unnessary text after close
      $("#edit-webform-ajax-submit-19034").click(function(){
        $("#webform-ajax-wrapper-19033").find(".links").find("h4").hide();
        $("#node-webform-19034, #node-webform-19033").css("border-bottom","none");
      });
      // Sticky Header version 2
      // Create a clone of the menu, right next to original.
      /*
      $('#zone-menu-wrapper').addClass('original').clone().insertAfter('#zone-menu-wrapper').addClass('cloned fixed-header').removeClass('original');

      scrollIntervalID = setInterval(stickIt, 10);

      function stickIt() {

        var orgElementPos = $('.original').offset();
        orgElementTop = orgElementPos.top;               

        if ($(window).scrollTop() >= (orgElementTop - 34)) {
          // scrolled past the original position; now only show the cloned, sticky element.

          // Cloned element should always have same left position and width as original element.     
          orgElement = $('.original');
          coordsOrgElement = orgElement.offset();
          leftOrgElement = coordsOrgElement.left;  
          widthOrgElement = orgElement.css('width');

          $('.cloned').css('left',leftOrgElement+'px').css('width',widthOrgElement+'px').show();
          $('.original').css('visibility','hidden');
        } else {
          // not scrolled past the menu; only show the original menu.
          $('.cloned').hide();
          $('.original').css('visibility','visible');
        }
      }
      */
      

        // Make the product filter form follow page scroll
        /*
        var headerBbottom = $('#header').position().top+$('#header').outerHeight(true);
        var staticProductListFileter = $('.aop-products-listing-view-filters').offset().top;
        console.log(headerBbottom);
        console.log(staticProductListFileter);
        if( headerBbottom > staticProductListFileter ) {
        $('.aop-products-listing-view-filters').css({position: 'fixed', top: headerBbottom+'px', zIndex: 2000, backgroundColor: '#FFFFFF', borderBottom: '1px solid #9BADB4'});
        } else {
        $('.aop-products-listing-view-filters').css({position: '', top: '', width: '100%'});
        }
        */

    //Sticky Header Version 3
    	//Key elements: zone-user-wrapper, zone-menu-wrapper
    	//Key vars: menuStuck- boolean if menu stuck or unstuck.
    	
    	function initStickyHeader(){
    	
    	//check to see if non-mobile
    	mobile = 0;
    	if ($(window).width() < 1000) { mobile = 1;}
    	
    	
    	if (mobile == 0) {
		    	//Check to see if the there is a promo tout, if not then just add the sticky class.
		    	if($('#zone-menu-wrapper').offset().top < ($('#zone-user-wrapper').height() + 10) && $(window).width() > 1000) {
		    		//Default page, no promo
		    		$('#zone-menu-wrapper').addClass('menu-wrapper-sticky');
		    		$('#section-content').css({'padding-top' : $('#zone-menu-wrapper').height() + 35});
		    	} else {
			    	//Page with large promo
			    	$(window).scroll(function(){
			    			//Sticky header with promo tout functions.
			    			scrollTop = $(this).scrollTop();
			    			if ((scrollTop + $('#zone-user-wrapper').height()) > $('#zone-menu-wrapper').offset().top
			    			 && !$('#zone-menu-wrapper').hasClass('menu-wrapper-sticky')) {
			    			$('#zone-menu-wrapper').addClass('menu-wrapper-sticky').css({top: $('#zone-user-wrapper').height()});
			    			$('#section-content').css({'padding-top' : $('#zone-menu-wrapper').height() + 10});
			    			}
			    			
			    			if((scrollTop + $('#zone-user-wrapper').height()) < $('#section-content').offset().top) {
			    			$('#zone-menu-wrapper').removeClass('menu-wrapper-sticky');
			    			$('#section-content').css({'padding-top' : 0});
			    			}
			    			
			    	});
			    	
		    	}
	    	} else {
	    	//Remove sticky attributes
	    	$('#zone-menu-wrapper').removeClass('menu-wrapper-sticky');
	    	$('#section-content').css({'padding-top' : 0});
	    	$(window).unbind("scroll");
	    	}
    	}
    	
    	initStickyHeader();
    	
    	//Re-initialize sticky functions on window resize.
    	$(window).resize(function(){
    		initStickyHeader();
    	});
        
      ///////////////SHOPPING CART SUMMARY FUNCTIONS///////////////
      
      //On hover, show summary dropdown and add hover affect
      $('.shopping-cart-summary-container, .shopping-cart-summary-dropdown, .shopping-cart-number-items').hover(function(){
      	$('.shopping-cart-summary-dropdown').show();
      	$('.shopping-cart-summary-container').css('background-color', '#6ac1d6');
      }, function(){
      //On mouseleave, hide summary dropdown and set style back to default;
	      $('.shopping-cart-summary-dropdown').hide();
	      $('.shopping-cart-summary-container').css('background-color', 'transparent');
	  });
     
      
      ///////////////Form Focus Functions///////////////
      
      $("#user-login input:text").first().focus();
      
      //All webform focus first input.
      $('.node-webform input:text').first().focus();
      
      $('#user-pass input:text').first().focus();

      
      /////////////Convention Listing Submit////////////////

      $('#show-submit-listing-block').click(function(){
        $(this).hide();
        $('#submit-listing-block').fadeIn(300);
    });

      //////////////Login Form Client Validation////////////
      
      $('#edit-submit--4').click(function(e){
      
	      if(!validateAllLogin()) {
	      //Prevent form from submitting.
	      	e.preventDefault();
	      }
	      //trigger live validation for login form
	      $('#edit-pass--3').keyup(function(){
	      validateLoginPass();
	      });
	      
	      $('#edit-name--3').keyup(function(){
	      validateLoginUser();
	      });
      
      });
      
      
      //Validate all Login
      function validateAllLogin(){
      
	      errors = 0;
	      
	      if(!validateLoginPass()) {
	      errors = 1;
	      $('#edit-pass--3').val("").focus();
	      }
	      
	      if(!validateLoginUser()) {
	      errors = 1;
	      $('#edit-name--3').val("").focus();
	      }
	      
	      if(errors == 0) {
	      //prevent form from submitting.
	      return true;
	      } else {
	      return false;
	      }
      
      }
      
      //Login email
      function validateLoginUser(){
	      username = $('#edit-name--3').val();
	      var userReg = /^[0-9 A-Za-z._'-]+$/;
	      if( !userReg.test( username ) || username.length < 3) {
	      	setValidation('#edit-name--3', 0);
	        return false;
	      } else {
	      	setValidation('#edit-name--3', 1);
	        return true;
	      }
      }
      
      //Password validation function
      function validateLoginPass(){
      	pass = $('#edit-pass--3').val();
          if(pass.length > 5) {
          setValidation('#edit-pass--3', 1);
          return true;
          } else {
          setValidation('#edit-pass--3', 0);
          return false;
          }
      }
      
      
      
      
      //////////////Registration Form Client Validation////////////
      
      //Prevent submit if user information does not match client side validation.
      
      $('#user-register-form').submit(function(e){
      
      if(!validateAll()) {
      //Prevent form from submitting.
      	e.preventDefault();
      	return false;
      }
      
      });
      
      //Replace drupal form styling with our own
      
      $("#login-register-wrapper .error").removeClass('error').addClass('register-input-invalid');
      
      //Email validation function
      function validateEmail() {
      	$email = $('#edit-mail').val();
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if( !emailReg.test( $email ) || $email.length < 1) {
        	setValidation('#edit-mail', 0);
          return false;
        } else {
        	setValidation('#edit-mail', 1);
          return true;
        }
      }
      
      //Username validation function
      function validateUser(){
      	  username = $('#edit-name--4').val();
	      var userReg = /^[0-9 A-Za-z._'-]+$/;
	      if( !userReg.test( username ) || username.length < 3) {
	      	setValidation('#edit-name--4', 0);
	        return false;
	      } else {
	      	setValidation('#edit-name--4', 1);
	        return true;
	      }
      }
      
      //Password validation function
      function validatePass(){
      	pass = $('#edit-pass-pass1').val();
	      if(pass.length > 5) {
	      setValidation('#edit-pass-pass1', 1);
	      return true;
	      } else {
	      setValidation('#edit-pass-pass1', 0);
	      return false;
	      }
      }
      
      //Confirm password validation function
      function validateConfirm(){
      	pass = $('#edit-pass-pass2').val();
      	if(pass == $('#edit-pass-pass1').val() && validatePass(pass)) {
      	setValidation('#edit-pass-pass2', 1);
      	return true;
      	} else {
      	setValidation('#edit-pass-pass2', 0);
      	return false;
      	}
      }
      
      //Live validate username
      function setValidation(field, valid){
      	if(valid == 1) {
      	$(field).removeClass("register-input-invalid").addClass("register-input-valid");
      	} else {
      	$(field).removeClass("register-input-valid").addClass("register-input-invalid");
      	}
      }
      
      
      //Validates all fields
      function validateAll(){
	     errors = 0;
	     
	     $('#live-validation-errors').html("");
	     
	     if(!validateConfirm()) {
	     errors = 1;
	     $('#edit-pass-pass2').val("").focus();
	     $('#live-validation-errors').append("<div class='line-item-error'>Passwords must match.</div>");
	     }
	     
	     if(!validatePass()) {
	     errors = 1;
		 $('#live-validation-errors').append("<div class='line-item-error'>Password must be at least 6 characters.</div>");
	     $('#edit-pass-pass1').val("").focus();
	     }
	     
	     if(!validateEmail()) {
	     errors = 1;
	     $('#live-validation-errors').append("<div class='line-item-error'>Please enter a valid email address.</div>");
	     $('#edit-mail').val("").focus();
	     }
	     
	     if(!validateUser()) {
	     errors = 1;
	     $('#edit-name--4').val("").focus();
	     $('#live-validation-errors').append("<div class='line-item-error'>Please create a username.</div>");
	     }
	     
	     if(errors == 0) {
	     //prevent form from submitting.
	     return true;
	     } else {
	     return false;
	     }
	     
      }
      
      
      
      //Username
       $('#edit-name--4').keyup(function(){
       	validateUser();
       });
      
      //Email live validation.
       $('#edit-mail').keyup(function(){
		validateEmail();
       });
       
       //Password Live Validation
       $('#edit-pass-pass1').keyup(function(){
		validatePass();
       	
       	//Check confirm password manually
       	if(validateConfirm()) {
       	$('#edit-pass-pass2').removeClass("register-input-invalid").addClass("register-input-valid");
       	} else {
       	val = $('#edit-pass-pass2').val();
       		if(val != "") {
       		$('#edit-pass-pass2').removeClass("register-input-valid").addClass("register-input-invalid");
       		} else {
       		$('#edit-pass-pass2').removeClass("register-input-valid").removeClass("register-input-invalid");
       		}
       	}
       });
       
       //Confirm Password Live Validation
       $('#edit-pass-pass2').keyup(function(){
			validateConfirm();
       });
       
  
      
      
      ///////////////HOMEPAGE CAROUSEL FUNCTIONS///////////////
      
      //on hover
      $(".homepage-carousel-item").hover(function(){
      //change carousel item to hover state
      	//hide the default content area
      	$(this).find(".homepage-carousel-item-default-state").hide();
      	//show the hover state area.
      	$(this).find(".homepage-carousel-item-hover-state").show();
      //on hover out
      },function(){
      	//change carousel item to default state
      	//hide the default content area
      	$(this).find(".homepage-carousel-item-default-state").show();
      	//show the hover state area.
      	$(this).find(".homepage-carousel-item-hover-state").hide();
      });
          
      ///////////////END HOMEPAGE CAROUSEL FUNCTIONS/////////////////
      
      // Keep the frontpage top promotouts at equal heights always
      /*
      var equalHeight = function (group) {
         tallest = 0;
         group.each(function() {
            thisHeight = $(this).height();
            if(thisHeight > tallest) {
               tallest = thisHeight;
            }
         });
         group.height(tallest);
      }
      equalHeight($("#zone-home-promo > div"));
        

      // Window resize funtions go here.
      $( window ).resize(function() {
        // Make divs equal heights with window resize
        equalHeight($("#zone-home-promo > div"));            
      });
      */


      // Mega menu mobile theming
      /*
      $(".btn-navbar").click(function() {
        if($(this).is(":visible")) {
          var menu_width = $(".zone-content-wrapper .zone-content").width();
          $(this).next(".nav-collapse").width(menu_width);
          $(this).next(".nav-collapse").css({"margin-top":"24px"});
          $(".tb-megamenu-submenu").hide();
          //$(".tb-megamenu-item.dropdown active .tb-megamenu-submenu").css({display:'none !important'});
        }
      });
      */
	    
  		// Open chat in new window		
  		$('a[href*="https://secure.livechatinc.com/licence/3247562/open_chat.cgi?"]').click(function() {
  			//alert($(this).attr('href'));
  			window.open($(this).attr('href'), 'Welcome to AOP Chat', 'width=520, height=530');
  			return false;
  		});
  		
  		$("#cboxContent").find(".submitted").hide();
      $("#cboxContent").find(".comment-comments").hide();
      
      // Apply page-products class to page-resources page so the the global theming can be inherited
      $("body.page-resources, body.page-family-entertainment").addClass("page-products");
      
      /**************************************************
      * --
      * Product listing page grade level styling and modifications
      * --
      **************************************************/
      // 
      var grade_text = '';
      // The expected values
      var list = ["Pre","K","1st","2nd","3rd","4th","5th","6th","7th","8th","9th","10th","11th","12th"];
      var newList = '';
      
      // Clear the floats lefts for li
      $("ul#facetapi-facet-search-apiproduct-display-block-field-productfield-grade-levelname").addClass("clearfix");        
      $.each(list, function( position, value ) {
        $('ul#facetapi-facet-search-apiproduct-display-block-field-productfield-grade-levelname li').each(function() {

          var str = $(this).text();
          var id = '';
          var clas = '';
          var link = '';
          var rel = '';
          //console.log(str);
          //console.log("TEXT: "+$(this).children('a').text()+"  VALUE: "+value)
          if (str.indexOf(value) > -1) {
            //console.log(value);
            if (value.indexOf('Pre') > -1 || value.indexOf('K') > -1) {
              grade_text = value;
            } else {
              grade_text = value.replace(/[^0-9]/g, '');
            }
            
            id = $(this).children('a').attr('id');
            clas = $(this).children('a').attr('class');
            link = $(this).children('a').attr('href');
            rel = $(this).children('a').attr('rel');
            //console.log("There is something here: "+ grade_text);
            newList += '<li class="leaf"><a id="'+id+'" class="'+clas+'" rel="'+rel+'" href="'+link+'">'+grade_text+'</a></li>';
          }
            
        });                  
      
      });
      // Replace all the li inside the ul with newly generated list
      $("ul#facetapi-facet-search-apiproduct-display-block-field-productfield-grade-levelname").html(newList);
      
      /* Grey out inactive grade levels and terminate the href links
      if ($("ul#facetapi-facet-search-apiproduct-display-block-field-productfield-grade-levelname li a").hasClass("facetapi-active") ){        
        $("ul#facetapi-facet-search-apiproduct-display-block-field-productfield-grade-levelname li a").each(function() {
          if($(this).hasClass("facetapi-inactive")) {
            $(this).addClass("aophs-facetapi-grey-out");
            $(this).bind("click", function() {
                return false;
            });
          }
        });
      }
      */


      // Add reset buttons to product listing page filters
      var reset_grades = '#';
      var reset_subjects = '#';
      var reset_curriculum = '#';
      if ($("ul#facetapi-facet-search-apiproduct-display-block-field-productfield-grade-levelname li a").hasClass("facetapi-active") ){
        // Grade level list reset
        reset_grades = $("ul#facetapi-facet-search-apiproduct-display-block-field-productfield-grade-levelname li a.facetapi-active").attr("href");
        $("#region-sidebar-second .field_product_field_grade_level_name h2.block-title").append("<a href="+reset_grades+"><span class='reset_listing_filter'>Reset</span></a>");
        $(".field_product_field_grade_level_name h2.block-title .reset_listing_filter").addClass("active");
      }
      // Subject list reset
      if ($("ul#facetapi-facet-search-apiproduct-display-block-field-productfield-subjectname li a").hasClass("facetapi-active") ){
        reset_subjects = $("ul#facetapi-facet-search-apiproduct-display-block-field-productfield-subjectname li a.facetapi-active").attr("href");
        $("#region-sidebar-second .field_product_field_subject_name h2.block-title").append("<a href="+reset_subjects+"><span class='reset_listing_filter'>Reset</span></a>");
        $(".field_product_field_subject_name h2.block-title .reset_listing_filter").addClass("active");
      }
      // Curriculum list reset
      if ($("ul#facetapi-facet-search-apiproduct-display-block-field-collection li a").hasClass("facetapi-active") ){
        reset_curriculum = $("ul#facetapi-facet-search-apiproduct-display-block-field-collection li a.facetapi-active").attr("href");
        $("#region-sidebar-second .field_collection h2.block-title").append("<a href="+reset_curriculum+"><span class='reset_listing_filter'>Reset</span></a>");
        $(".field_collection h2.block-title .reset_listing_filter").addClass("active");
      }
      
      // Sort the product listing page filters according to provided list
      // Since facet api does not provide the type of sorting we needed to use jquer to sort it after list is rendered and available
      // We could create a function for this if we need to do more than 2 of these :|
      var subjects_list = ["Bible","History and Geography","Language Arts","Math","Science","Electives","5-Subject Set","Other",];
      var curriculum_list = ["Monarch","Switched-On Schoolhouse","LIFEPAC","Horizons","Weaver"];
      var subjectUL = '';
      var curriculumUL = '';
      var li1 = '';
      var li2 = '';
      
      // Subject list ordering
      $.each(subjects_list, function( position, value ) {
        $("ul#facetapi-facet-search-apiproduct-display-block-field-productfield-subjectname li").each(function() {
          var str1 = $(this).find("a").text();
          if (str1.length > 0 && str1.indexOf(value) > -1) {
            li1 += "<li>"+$(this).html()+"</li>";
          }
        });
     
      });
      $("ul#facetapi-facet-search-apiproduct-display-block-field-productfield-subjectname").html(li1);
      
      // Curriculum list ordering
      $.each(curriculum_list, function( position, value ) {
        $("ul#facetapi-facet-search-apiproduct-display-block-field-collection li").each(function() {
          var str2 = $(this).find("a").text();
          if (str2.length > 0 && str2.indexOf(value) > -1) {
            li2 += "<li>"+$(this).html()+"</li>";
          }
        });
      });
      $("ul#facetapi-facet-search-apiproduct-display-block-field-collection").html(li2);
      
      // Update the sku of a subscription product display
      $(".single-product-cart-addtocart [id^=edit-product-id]").change(function() {
        //alert("Yes");
        var product_id = $(this).val();
        
        $.post( "/aop_utility/update_sku",{product_id:product_id}, function( data ) {

    			if (jQuery.type( data ) === "string") {
            $(".single-product-cart .aop-product-sku").html("<b>PRODUCT ID:</b> "+data);
    			}

    		});
    		
      });

      // Move breadcrumb-total-results element to position after the pseudo breadcrumb element
      $(".current-search-item-results").detach().appendTo('.pseudo-breadcrumb');


    }
  }
})(jQuery);

(function ($) {

  var mapstyle = [{"featureType":"water","stylers":[{"color":"#6ac1d7"},{"visibility":"on"}]},{"featureType":"landscape","stylers":[{"color":"#f2f2f2"}]},{"featureType":"road","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"transit","stylers":[{"visibility":"off"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]}]
  /**
   * Apply styles to a Google map. This function waits until the Drupal GMap
   * module has finished creating the map (when Drupal.gmap.getMap() returns a
   * map instance instead of undefined variable) before applying the styles. If
   * necessary, use window.setInterval to wait for map to be ready.
   *
   * Please note that this is not the best possible solution for styling maps,
   * check back with us later for an improved solution to this issue:
   * http://monkeysatkeyboards.com/making-drupal-gmap-look-pretty
   *
   * @param mapid Map identifier. GMap module outputs it as a class on the map
   *        wrapper div, e.g... 'gmap-auto1map-gmap'. This can be a string with a
   *        single ID or an array with multiple IDs. The function only has any
   *        effect if an element is found in the document with a CSS class that
   *        matches this ID exactly.
   *
   * @param styles JavaScript styles array e.g.. from http://snazzymaps.com
   */
  function styleMaps(mapid, styles) {
    var mapids = (mapid instanceof Array) ? mapid : [ mapid ];
    for (var i in mapids) {
      if ($('.' + mapids[i]).length) {
        var gmap = Drupal.gmap.getMap(mapids[i]);
        if (typeof(gmap.map) != 'undefined') {
          gmap.map.setOptions({styles: styles});
        }
        else {
          var args = {
            gmap: gmap,
            styles: styles
          };
          args.iid = window.setInterval(function(args) {
            if (typeof(args.gmap.map) != 'undefined') {
              window.clearInterval(args.iid);
              args.gmap.map.setOptions({styles: args.styles});
            }
          }, 20, args);
        }
      }
    }
  }

  Drupal.behaviors.site = {
    attach: function (context, settings) {
      var maps = [
        'gmap-auto1map-gmap', // Names used by GMap Location
        'gmap-auto2map-gmap',
        'gmap-auto3map-gmap'
      ];
      styleMaps(maps, mapstyle);
    }
  };
}(jQuery));
