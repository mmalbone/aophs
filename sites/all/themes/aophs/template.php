<?php

/**
 * Preprocess variables for html.tpl.php
 *
 * @see system_elements()
 * @see html.tpl.php
 */
 
 
 
 
function aophs_preprocess_html(&$variables) {
  // Add conditional stylesheets for IE
  drupal_add_css(path_to_theme() . '/css/aophs-alpha-ie-lte-8.css', array('group' => CSS_THEME, 'weight' => 23, 'browsers' => array('IE' => 'lte IE 8', '!IE' => FALSE), 'preprocess' => FALSE));
  drupal_add_css(path_to_theme() . '/css/aophs-alpha-ie-lte-7.css', array('group' => CSS_THEME, 'weight' => 24, 'browsers' => array('IE' => 'lte IE 7', '!IE' => FALSE), 'preprocess' => FALSE));

  // Add external libraries.
  drupal_add_library('commerce_kickstart_theme', 'selectnav');
}

/**
 * Implements hook_library().
 */
function aophs_library() {
  $libraries['selectnav'] = array(
    'title' => 'Selectnav',
    'version' => '',
    'js' => array(
      libraries_get_path('selectnav.js') . '/selectnav.min.js' => array(),
    ),
  );
  return $libraries;
}

/**
 * Override the submitted variable.
 */
function aophs_preprocess_node(&$variables) {
  
  if ($variables['nid'] == 19033 || $variables['nid'] == 19034 || $variables['nid'] == 19038 || $variables['nid'] == 19035){    
    $variables["submitted"] = '';
    $variables["content"]["links"]["comment"] = array(); 
  } 

// if readmore link is set
  if (isset($variables['content']['links']['node']['#links']['node-readmore'])) {
    // make a copy of the old link and change the link's title
    $readmore = $variables['content']['links']['node']['#links']['node-readmore'];
    $readmore['title'] = '<span class="read-more-link">'.t('More')."</span>";
    // remove the old link
    unset($variables['content']['links']['node']['#links']['node-readmore']);
    // creat a new link under a different name
    $variables['content']['links']['node']['#links']['node-readmore-custom'] = $readmore;
  }


}

function aophs_preprocess_page(&$variables) {
  if ( user_is_logged_in() ) {
    $path = drupal_get_path('theme', 'aophs');
    drupal_add_js($path . '/panel_image_src_layout.js');
  }
  // Hide default Search Page Title
  if(arg(0) == 'search' && arg(1) == 'node') {
    $variables['title_hidden'] = 1;
  }

}

function aophs_checkout_footer() {

  $output =
  '<footer id="section-footer" class="section section-footer">
    <div id="zone-footer-wrapper" class="zone-wrapper zone-footer-wrapper clearfix">
      <div id="zone-footer" class="zone-footer clearfix container-24">
        <div id="aop-footer-checkout">
          <span class="grid-5">
            <div>
              <span id="siteseal"><script type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=Srqd6rJMnnBg0Fk9njpLwW2nsqiRBi4uUyj4SFWALHrzDuVhE1Z"></script></span>
            </div>
          </span>
          <span class="grid-5">
            <div>
              <img src="/sites/all/themes/aophs/images/logo_white.png">
            </div>
          </span>
          <span class="grid-14">
            <div>
              <h3>"Educate, inspire, and change lives."</h3>
              Every day, Alpha Omega Publications follows its mission to change education for the glory of God. Driven by a passion to inspire, AOP equips students for the future by promoting academic excellence and Christian values.
            </div>
          </span>
        </div>
      </div>
    </div>
  </footer>';
  return $output;
}


function aophs_form_alter(&$form, &$form_state, $form_id) { 
  if ($form_id == 'search_block_form') {
    $form['search_block_form']['#default_value'] = t('Search'); // Set a default value for the textfield
    // Add extra attributes to the text box
    $form['search_block_form']['#attributes']['onblur'] = "if (this.value == '') {this.value = 'Search';}";
    $form['search_block_form']['#attributes']['onfocus'] = "if (this.value == 'Search') {this.value = '';}";
    // Prevent user from searching the default text
    $form['#attributes']['onsubmit'] = "if(this.search_block_form.value=='Search'){ alert('Please enter a search'); return false; }";
    // Alternative (HTML5) placeholder attribute instead of using the javascript
    $form['search_block_form']['#attributes']['placeholder'] = t('Search');
  }
  if ($form_id == 'search_form') {
    unset($form['advanced']);
    $form['basic'] = array();
  }

  if (in_array($form_id, array('comment_node_curriculum_form','comment_node_resource_form','comment_node_dvd_form'))) {
    $form['comment_body']['und'][0]['#title'] = "Review";
  }
  
  //Registration form override
  if ( TRUE === in_array( $form_id, array( 'user_register_form') ) ) {
    $form['account']['mail']['#attributes']['placeholder'] = t( 'you@email.com' );
    $form['account']['name']['#attributes']['placeholder'] = t( 'Create Username' );
  }

  if ($form_id == 'commerce_checkout_form_checkout') {
    
    //new dBug($form['customer_profile_billing']['commerce_customer_address']['und'][0]['locality_block']['administrative_area']);

    $form['customer_profile_billing']['field_assist_id']['#type'] = 'hidden';
    $form['customer_profile_billing']['commerce_customer_address']['und'][0]['country']['#weight'] = 99;
    $form['customer_profile_shipping']['field_assist_id']['#type'] = 'hidden';
    $form['customer_profile_shipping']['commerce_customer_address']['und'][0]['country']['#weight'] = 99;
    $form['customer_profile_shipping']['field_shipping_phone']['#required'] = FALSE;
    $form['customer_profile_shipping']['field_shipping_phone']['#type'] = 'hidden';

    //new dBug($form['customer_profile_shipping']['field_shipping_phone']['#required']);

  }
}

/**
 * Implements hook_preprocess_comment().
 */
function aophs_preprocess_comment(&$vars) {
  // Change the Permalink to display #1 instead of 'Permalink'
  $time = str_replace("-", "", $vars['created']);
  $date = date("F d, Y", strtotime($time));
  $vars['created'] = $date;

  // Change the author name to use full name if availabe
  $author = aophs_user_full_name($vars['elements']['#comment']->uid);
  $vars['user']->name = $author;
  $vars['author'] = '<a href="/users/'.$vars['elements']['#comment']->registered_name.'" title="View user profile." class="username">'.$author.'</a>';

  if (in_array($vars['elements']['#node']->type, array('curriculum','resource','dvd'))) {
    // Only show links for users with marketing role
    global $user;
    if (!in_array('marketing', array_values($user->roles))) {
      // Remove comment links if the node type is one of the ones in the array above
      unset($vars['content']['links']['comment']['#links']);
    }
  }
}

// Print user full name if the user has filled out his/her user profile or return user name
// int $uid : Drupal User ID
function aophs_user_full_name($uid) {
  global $user;
  $user_fields = user_load($uid);
  $last_name = '';
  $first_name = '';
  $full_name = '';
  
  if (isset($user_fields->field_lastname['und'][0]['value']) and !empty($user_fields->field_lastname['und'][0]['value'])) {
    $last_name = $user_fields->field_lastname['und'][0]['value'];
  }
  if (isset($user_fields->field_firstname['und'][0]['value']) and !empty($user_fields->field_firstname['und'][0]['value'])) {
    $first_name = $user_fields->field_firstname['und'][0]['value'];
  }

  if (!empty($first_name) || !empty($last_name)) {
    $full_name = $first_name.' '.$last_name;
    return trim($full_name);
  } else {
    return $user->name;
  }
}
/*
function aophs_forward_page($variables) {
  //dpm($variables);
  echo "<pre>";
  print_r($variables);
  echo "</pre>";
}
*/

//user login/register

/**
 * Registers overrides for various functions.
 *
 * In this case, overrides three user functions
 */
function aophs_theme() {
  return array(
    'user_login' => array(
      'template' => 'templates/user-login',
      'variables' => array('form' => NULL), ## you may remove this line in this case
    ),
    'user_register' => array(
      'template' => 'user-register',
      'variables' => array('form' => NULL), ## you may remove this line in this case
    ),
  );
}
function aophs_preprocess_user_login(&$variables) {
	$variables['form'] = drupal_build_form('user_login', user_login(array(),$form_state)); ## 
}

function aophs_str_insert($str, $search, $insert) {
  $title_attributes = strpos($str, $search);
  if($title_attributes === false) {
    return $str;
  }
  return substr_replace($str, $search.$insert, $title_attributes, strlen($search));
}
