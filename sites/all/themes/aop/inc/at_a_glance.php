 <?php
function loadAtAGlanceResource($product_id) {
	$product_object = commerce_product_load($product_id);
	$term = taxonomy_term_load($product_object->field_product['und'][0]['tid']);
	$curriculum = strtolower($term->name);

	// Print features
	if(!empty($product_object->field_feature)) {

	}

	// Build array for at a glance lists
	$ataglance = array(
		'ondemand' => 'field_on_demand',
		'sample-pdf' => 'field_sample_pages',
		'supply' => 'field_science_supply',
		'scope' => 'field_scope_sequence',
		'sample-video' => 'field_video_clips',
		'sample-audio' => 'field_audio_clips',
	);

	foreach ($ataglance as $key => $field) {
		if (isset($product_object->$field) && !empty($product_object->$field)) {
			//$data = $product_object->$field['und'][0]['value'];
  		switch($key) {
        case "ondemand":
        case "sample-video":
          $format = "video";
          break;  
        default:
             $output = "pdf";
      }
         
			$data = loadProductXMLString($product_object->$field['und'][0]['value'], $format);
			return $data;
		}
	}
}

function loadProductXMLString($data, $format) {
	$xml = simplexml_load_string($data);
	// echo "<pre>";
	// print_r($xml);
	// echo "</pre>";
	//echo $xml->{@attributes}['description'];
	// $heading = (string)$xml->attributes()['heading'];
	// $description = (string)$xml->attributes()['description'];
	
	// $output = '<strong>'.$heading.'</strong></br>';
	// $output .= $description;

	$output .= "<ul>";
	foreach($xml->resource as $resource) {
	  $output .="<li class='pdf'><a href='http://media.glnsrv.com/pdf/products/scope_and_sequence/mon/".$resource->link."'>".$resource->name."</a> <span>(".$resource->size.")</span></li>";
	}
	$output .= "</ul>";

	if(isset($format)) {
	  if ($format == 'pdf') {
	    $output .= '<p><span class="note">* All files are in PDF file format and require you to have Adobe Reader installed on your computer in order to view them. <a style="color: #999999;" title="Download Adobe Reader" target="_blank" href="http://www.adobe.com/products/acrobat/readstep2.html?">Click here</a> to download Adobe Reader for free.</span>
	    </p>';
	  } elseif($format == 'video') {
	    $output = '<p><span class="note">* All files are in SWF format and require you to have Adobe Flash Player installed on your computer in order to view them. Download <a style="color: #999999;" title="Download Adobe Reader" target="_blank" href="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash">Adobe Flash Player</a> for free to view our media today!.</span>
	    </p>';
	  }
	}
	return $output;
}

function loadColorboxDetails($product_id, $action) {
	return $url = '<a class="colorbox-load" href="/aop_utility/load-at-a-glance/'.$product_id.'/'.$action.'?width=70%25&height=70%25&iframe=true">';
}

function loadAtAGlanceSection($product_id) {
	// Load the commerce product
	$product_object = commerce_product_load($product_id);
	$term = taxonomy_term_load($product_object->field_product['und'][0]['tid']);
	$curriculum = strtolower($term->name);

	// Print features
	if(!empty($product_object->field_feature)) {

	}

	$url = '<a class="colorbox-load" href="/aop_utility/load-at-a-glance/';
	$display_size = '?width=70%25&height=70%25&iframe=true">';

	switch($curriculum) {
    case "monarch":
      $output = "<ul>
      	<li class='mail-icon'>".loadColorboxDetails($product_id,'email')."Email to a Friend</a></li>
		<li class='play-icon'>".loadColorboxDetails($product_id,'sample-video')."View Sample</a></li>
		<li class='play-icon'>".loadColorboxDetails($product_id,'ondemand')."View On-Demand Presentations</a></li>
		<li class='pdf-icon'>".loadColorboxDetails($product_id,'scope')."See Scope and Sequence</a></li>
		<li class='pdf-icon'>".loadColorboxDetails($product_id,'objectives')."See Lesson Objectives</a></li>
		<li class='check-icon'>".loadColorboxDetails($product_id,'requirements')."See System Requirements</a></li>
		<li class='check-icon'>".loadColorboxDetails($product_id,'suply')."Science Supply List – NOTE: These are only available for 3-12 grade Monarch Science courses and Earth Science and Gen. Science III</a></li>";
      break;  
    case "switched-on schoolhouse":
      $output = "<ul>
      	<li class='mail-icon'>".loadColorboxDetails($product_id,'email')."Email to a Friend</a></li>
		<li class='pdf-icon'>".loadColorboxDetails($product_id,'scope')."See Scope and Sequence</a></li>
		<li class='play-icon'>".loadColorboxDetails($product_id,'ondemand')."View On-Demand Presentations</a></li>
		<li class='pdf-icon'>".loadColorboxDetails($product_id,'objectives')."See Lesson Objectives</a></li>
		<li class='check-icon'>".loadColorboxDetails($product_id,'requirements')."See System Requirements</a></li>
		<li class='check-icon'>".loadColorboxDetails($product_id,'suply')."Science Supply List – NOTE: These are only available for 3-12 grade SOS Science courses and Earth Science and Gen. Science III</a></li>";
      break;
    case "lifepac":
      $output = "<ul>
      	<li class='mail-icon'>".loadColorboxDetails($product_id,'email')."Email to a Friend</a></li>
		<li class='pdf-icon'>".loadColorboxDetails($product_id,'scope')."See Scope and Sequence</a></li>
		<li class='pdf-icon'>".loadColorboxDetails($product_id,'sample-pdf')."See Sample Pages</a></li>";
	  break;
    case "horizons":
      $output = "<ul>
      	<li class='mail-icon'>".loadColorboxDetails($product_id,'email')."Email to a Friend</a></li>
		<li class='pdf-icon'>".loadColorboxDetails($product_id,'scope')."See Scope and Sequence</a></li>
		<li class='pdf-icon'>".loadColorboxDetails($product_id,'sample-pdf')."See Sample Pages</a></li>";
	  break;
	case "weaver":
      $output = "<ul>
      	<li class='mail-icon'>".loadColorboxDetails($product_id,'email')."Email to a Friend</a></li>
		<li class='pdf-icon'>".loadColorboxDetails($product_id,'scope')."See Scope and Sequence</a></li>
		<li class='pdf-icon'>".loadColorboxDetails($product_id,'tour')."See the Weaver Tour</a></li>";
	  break;
    default:
         $output = "";   
  }
  return $output;
}

?>