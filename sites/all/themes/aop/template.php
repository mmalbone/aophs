<?php
/**
 * @file
 * Contains the theme's functions to manipulate Drupal's default markup.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728096
 */


/**
 * Override or insert variables into the maintenance page template.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("maintenance_page" in this case.)
 */
/* -- Delete this line if you want to use this function
function aop_preprocess_maintenance_page(&$variables, $hook) {
  // When a variable is manipulated or added in preprocess_html or
  // preprocess_page, that same work is probably needed for the maintenance page
  // as well, so we can just re-use those functions to do that work here.
  aop_preprocess_html($variables, $hook);
  aop_preprocess_page($variables, $hook);
}
// */

/**
 * Override or insert variables into the html templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
/* -- Delete this line if you want to use this function
function aop_preprocess_html(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');

  // The body tag's classes are controlled by the $classes_array variable. To
  // remove a class from $classes_array, use array_diff().
  //$variables['classes_array'] = array_diff($variables['classes_array'], array('class-to-remove'));
}
// */

/**
 * Override or insert variables into the page templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
/* -- Delete this line if you want to use this function
function aop_preprocess_page(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the node templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */
/* -- Delete this line if you want to use this function
function aop_preprocess_node(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');

  // Optionally, run node-type-specific preprocess functions, like
  // aop_preprocess_node_page() or aop_preprocess_node_story().
  $function = __FUNCTION__ . '_' . $variables['node']->type;
  if (function_exists($function)) {
    $function($variables, $hook);
  }
}
// */

/**
 * Override or insert variables into the comment templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
/* -- Delete this line if you want to use this function
function aop_preprocess_comment(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the region templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("region" in this case.)
 */
/* -- Delete this line if you want to use this function
function aop_preprocess_region(&$variables, $hook) {
  // Don't use Zen's region--sidebar.tpl.php template for sidebars.
  //if (strpos($variables['region'], 'sidebar_') === 0) {
  //  $variables['theme_hook_suggestions'] = array_diff($variables['theme_hook_suggestions'], array('region__sidebar'));
  //}
}
// */

/**
 * Override or insert variables into the block templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
/* -- Delete this line if you want to use this function
function aop_preprocess_block(&$variables, $hook) {
  // Add a count to all the blocks in the region.
  // $variables['classes_array'][] = 'count-' . $variables['block_id'];

  // By default, Zen will use the block--no-wrapper.tpl.php for the main
  // content. This optional bit of code undoes that:
  //if ($variables['block_html_id'] == 'block-system-main') {
  //  $variables['theme_hook_suggestions'] = array_diff($variables['theme_hook_suggestions'], array('block__no_wrapper'));
  //}
}
// */



function aop_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'search_block_form') {
  
    unset($form['search_block_form']['#title']);
    
    $form['search_block_form']['#title_display'] = 'invisible';
    $form_default = t('Search');
    $form['search_block_form']['#default_value'] = $form_default;
    $form['actions']['submit'] = array('#type' => 'image_button', '#src' => base_path() . path_to_theme() . '/images/search_icon.png');
    
	$form['search_block_form']['#attributes'] = array('onblur' => "if (this.value == '') {this.value = '{$form_default}';}", 'onfocus' => "if (this.value == '{$form_default}') {this.value = '';}" );
  }
  if (arg(0) == 'admin') {
    $path = drupal_get_path('theme', 'aop');
    $form['#attached']['js'][] = $path . '/panel_image_src_layout.js';
  }
}

/* modify the breadcrum list to replace ">" with "/" */
function aop_breadcrumb($variables) {
	//drupal_set_message( '<pre>'.print_r($variables, TRUE). '</pre>');
	$breadcrumb = $variables['breadcrumb'];
  if (!empty($breadcrumb)) {
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';

    $output .= '<div class="breadcrumb">' . implode('  /  ', $breadcrumb) . '</div>';
    return $output;
  }
}

/**
 * Themes the optional checkout review page data.
 */
function aop_commerce_checkout_review($variables) {
  $form = $variables['form'];

  // Turn the review data array into table rows.
  $rows = array();

  foreach ($form['#data'] as $pane_id => $data) {
    // First add a row for the title.
    $rows[] = array(
      'data' => array(
        array('data' => $data['title'], 'colspan' => 2),
      ),
      'class' => array('pane-title', 'odd'),
    );

    // Next, add the data for this particular section.
    if (is_array($data['data'])) {
      // If it's an array, treat each key / value pair as a 2 column row.
      foreach ($data['data'] as $key => $value) {
        $rows[] = array(
          'data' => array(
            array('data' => $key .':', 'class' => array('pane-data-key')),
            array('data' => $value, 'class' => array('pane-data-value')),
          ),
          'class' => array('pane-data', 'even'),
        );
      }
    }
    else {
      // Otherwise treat it as a block of text in its own row.
      $rows[] = array(
        'data' => array(
          array('data' => $data['data'], 'colspan' => 2, 'class' => array('pane-data-full')),
        ),
        'class' => array('pane-data', 'even'),
      );
    }
  }

  return theme('table', array('rows' => $rows, 'attributes' => array('class' => array('checkout-review'))));
}


function aop_preprocess_page(&$variables) {
	//dpm($variables['page']);
	if ( user_is_logged_in() ) {
    $path = drupal_get_path('theme', 'aop');
    drupal_add_js($path . '/panel_image_src_layout.js');
    if ( $variables['page']['secondary_nav']['menu_block_2']['#content'][1255]['#href'] == 'user' ) {
			global $user;
			$variables['page']['secondary_nav']['menu_block_2']['#content'][1255]['#title'] = 'Hi, '.aop_loggedin_username();
		}
/*
		if ( $variables['page']['secondary_nav']['menu_block_2']['#content'][1274]['#title'] == 'Sign Up' ) {
			$variables['page']['secondary_nav']['menu_block_2']['#content'][1274]['#title'] = 'Hi, '.$user->name;
		}
		if ( $variables['page']['secondary_nav']['menu_block_2']['#content'][1274]['#title'] == 'Log In' ) {
			$variables['page']['secondary_nav']['menu_block_2']['#content'][1255]['#title'] = 'Hi, '.$user->name;
		}
*/
	}
}

function aop_preprocess_node(&$variables) {
	//dpm($variables['view_mode']);
}

function aop_loggedin_username() {
  if ( user_is_logged_in() ) {
    global $user;
    
    $user_profile = user_load($user->uid);
    
    //dpm($user_profile);
    $print_name = ucfirst($user->name);
    
    if (isset($user_profile->field_user_nickname) && !empty($user_profile->field_user_nickname['und'][0]['value'])) {
      $print_name = $user_profile->field_user_nickname['und'][0]['value'];
    } elseif(isset($user_profile->field_user_first_name) && !empty($user_profile->field_user_first_name['und'][0]['value'])) {
      $print_name = $user_profile->field_user_first_name['und'][0]['value'];    
    }
    
    //dpm($print_name);
    return $print_name;

  } else {
    return '';
  }
}

function aop_checkout_footer() {

  $output =  '<div id="aop-footer-checkout">
        <span>
          <div>
            <img src="/sites/all/themes/aop/images/logo_white.png">
          </div>
        </span>
        <span>
          <div>
            <h3>"Educate, inspire, and change lives."</h3>
            Every day, Alpha Omega Publications follows its mission to change education for the glory of God. Driven by a passion to inspire, AOP equips students for the future by promoting academic excellence and Christian values.
          </div>
        </span>
        <span>
          <div>
            <span id="siteseal">
              <span id="siteseal"><script type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=Srqd6rJMnnBg0Fk9njpLwW2nsqiRBi4uUyj4SFWALHrzDuVhE1Z"></script></span>
            </span>
          </div>
        </span>
      </div>';
  return $output;
}
function aop_str_insert($str, $search, $insert) {
  $title_attributes = strpos($str, $search);
  if($title_attributes === false) {
    return $str;
  }
  return substr_replace($str, $search.$insert, $title_attributes, strlen($search));
}