<!DOCTYPE html>
<html lang="">
<head>
  <meta charset="utf-8">
	<title></title>
	<meta name="description" content="" />
  	<meta name="keywords" content="" />
	<meta name="robots" content="" />
</head>
<body>
	<div>
		<div class="top_banner">
		</div>
		<div class="header_nav_and_search">
			<span class="logo">LOGO</span>
			<span class="nav"></span>
			<span class="phone_search">
				<div class="search">SEARCH</div>
				<div class="call_search_cart">
					<span class="call">Call us 800.622.3070</span>
					<span class="sign_in_join">Sign in/Join</span>
					<span class="message">Mssg</span>
					<span class="cart">Crt</span>
				</div>
			</span>
		</div>
		<div class="front_slide_show">
			<span class="front_main_image">
				<img src="" width="" height=""/>
			</span>
			<span class="front_featured">
				<div class="front_pink_featured"></div>
				<div class="front_orange_blue_featured">
					<span class="front_orange"></span>
					<span class="front_blue"></span>
				</div>
			</span>
		</div>
		<div class="front_carousel"></div>
		<div class="front_stories_and_articles"></div>
		<div class="front_call_out"></div>
		<div class="front_academy">
			<span class="academy"></span>
			<span class="school"></span>
		</div>
		<div class="front_footer">
			
		</div>
	</div>
</body>
</html>