(function ($) {
  Drupal.behaviors.aop_theme = {
    attach: function (context, settings) {

		 var stickyHeaderTop = $('#header').offset().top;
		 
		 $(window).scroll(function(event){
			event.preventDefault();
			var windowTop = $(window).scrollTop();
	        if( windowTop > stickyHeaderTop ) {
	            $('#header').css({position: 'fixed', top: '0px', zIndex: 2000, backgroundColor: '#FFFFFF', width: '100%', borderBottom: '1px solid #9BADB4', maxWidth: '940px'});
	        } else {
	            $('#header').css({position: 'static', top: '0px', width: '100%'});
	        }
	     // Make the product filter form follow page scroll  
/*
			 var headerBbottom = $('#header').position().top+$('#header').outerHeight(true);
			 var staticProductListFileter = $('.aop-products-listing-view-filters').offset().top;
			 console.log(headerBbottom);
			 console.log(staticProductListFileter);
			 if( headerBbottom > staticProductListFileter ) {
            $('.aop-products-listing-view-filters').css({position: 'fixed', top: headerBbottom+'px', zIndex: 2000, backgroundColor: '#FFFFFF', borderBottom: '1px solid #9BADB4'});
        } else {
            $('.aop-products-listing-view-filters').css({position: '', top: '', width: '100%'});
        }
*/
	        
	    });
		
  		// Open chat in new window		
  		$('a[href*="https://secure.livechatinc.com/licence/3247562/open_chat.cgi?"]').click(function() {
  			//alert($(this).attr('href'));
  			window.open($(this).attr('href'), 'Welcome to AOP Chat', 'width=520, height=530');
  			return false;
  		});
  
  		// Update the shoping chart icon with number of items in cart
  		$.post( "/aop_utility/get_cart_item_count", function( data ) {
  			if (data > 0) {
  				$('.menu__item a[href="/cart"]').html("<span class='cart_items_counter'>"+data+"</span>");
  			}
  		});
  		
  		
      // AOP Search block
      if ($('#edit-custom-search-types option:selected').text() == "-Any-") {
        $('#edit-custom-search-types option:selected').text("All");
      }
      
      var text_size = $.trim($('#edit-custom-search-types option:selected').text()).length + 3;
      //alert($('#edit-custom-search-types option:selected').text());
      // playing css width
      size = text_size*2; // average width of a char
      $("form #edit-custom-search-types").css('width',size*4);
      var used_width = $("form #edit-custom-search-types").width() + $("form #edit-actions").width();
      var wrapper_width = $("#search-block-form").width();
      $("form .form-item.form-item-search-block-form").css('width', $("#search-block-form").width() - used_width );
      
      $("form #edit-custom-search-types").change(function() {
  
        if ($('#edit-custom-search-types option:selected').text() == "-Any-") {
          $('#edit-custom-search-types option:selected').text("All");
        }
        //var parent_width = $(this).parent('div').width();
        var text_size = $.trim($('#edit-custom-search-types option:selected').text()).length + 3;
        //alert($('#edit-custom-search-types option:selected').text());
        // playing css width
        size = text_size*2; // average width of a char
        $(this).css('width',size*4);
        var used_width = $("form #edit-custom-search-types").width() + $("form #edit-actions").width();
        var wrapper_width = $("#search-block-form").width();
        $("form .form-item.form-item-search-block-form").css('width', $("#search-block-form").width() - used_width );
      });
      
      
    }
  };
})(jQuery);



