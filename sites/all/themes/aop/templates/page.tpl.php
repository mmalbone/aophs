<?php
/**
 * @file
 * Returns the HTML for a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728148
 */
?>

<div id="page">

	<header class="header" id="header" role="banner">
	
		<div id="header_wrapper">	  
			<?php if ($logo): ?>
			<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="header__logo" id="logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" class="header__logo-image" /></a>
			<?php endif; ?>
			
			<div id="aop-nav-search-wrapper">
      <?php if (strpos(current_path(),'checkout') !== false ): ?>
        <span class="aop-checkout-nav-text">Complete the required details below then review your order.</span>
        <span class="aop-checkout-nav-text"><a href="/user">Hi, <?php print aop_loggedin_username(); ?></a></span>
      <?php else: ?>

        <div id="aop-search">
          <?php print render($page['aop_search']); ?>
        </div>
        <div id="navigation" class="clearfix">
          <div id="primary-nav">
            <?php print render($page['primary_nav']); ?>
          </div>
          <div id="secondary-nav">
            <?php print render($page['secondary_nav']); ?>
          </div>
        </div>

      <?php endif; ?>

			</div>
		
		</div><!-- end header wrapper -->
	</header>

  <div id="main">

    <div id="content" class="column" role="main">
      
      <?php if (strpos(current_path(),'checkout') == false ): ?>

        <?php print render($page['highlighted']); ?>
        <?php print $breadcrumb; ?>

      <?php endif; ?>
      
      <a id="main-content"></a>
      <?php print render($title_prefix); ?>
      <?php if ($title): ?>
        <h1 class="page__title title" id="page-title"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php print $messages; ?>
      <?php print render($tabs); ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
      <?php print $feed_icons; ?>
    </div>

  </div>

</div>

<div id="aop-footer">
<?php //dpm(current_path()); ?>
<?php //dpm(drupal_lookup_path('alias',current_path())); ?>
  
   <div id="aop-footer-col-wrapper" class="clearfix">
   <!-- For checkout page -->
   <?php if (strpos(current_path(),'checkout') !== false ): ?>
    
    <?php print aop_checkout_footer(); ?>

  <?php else: ?>

       <div id="aop-footer-col-1">
         <?php print render($page['aop_footer_col_1']); ?>
       </div>

       <div id="aop-footer-col-2">
         <?php print render($page['aop_footer_col_2']); ?>
       </div>

       <div id="aop-footer-col-3">
         <?php print render($page['aop_footer_col_3']); ?>
       </div>

       <div id="aop-footer-col-4">
         <?php print render($page['aop_footer_col_4']); ?>
       </div>

      </div>

      <div id="aop-footer-notices">
        <?php print render($page['aop_footer_notices']); ?>
      </div>

  <?php endif; ?>
</div>

<?php print render($page['bottom']); ?>
