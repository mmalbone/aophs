<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
 //dpm($row->field_field_bundled_product);
?>


<?php
/*
//dpm($product);
//print "Hello";

$product_image_url = variable_get('aop_images_url');

if (!empty($row->field_field_bundled_product) && count($row->field_field_bundled_product > 0)) {
	foreach($row->field_field_bundled_product as $bundle) {
		foreach($bundle['rendered']['commerce_product'] as $item) {
			$item['field_product_image']['#items'][0]['value'] = $product_image_url.$item['field_product_image']['#items'][0]['value'];
			$item['field_product_image']['#items'][0]['safe_value'] = $product_image_url.$item['field_product_image']['#items'][0]['safe_value'];
			$item['field_product_image'][0]['#markup'] = '<img width="150px" height="100px" src="'.$product_image_url.$item['field_product_image'][0]['#markup'].'" alt="No Image">';
			//dpm($item);
			//print $item['field_product_image'][0]['#markup'];
		}
	}
}
*/
?>
<?php print $output; ?>
