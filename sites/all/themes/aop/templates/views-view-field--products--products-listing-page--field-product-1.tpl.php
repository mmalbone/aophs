<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>
<?php //dpm($field); ?>
<?php //dpm($row); ?>
<?php //print $output; ?>


<?php

switch($field->original_value) {
  case "Monarch":
    $output = "<div class='monarc_curriculum_banner'>";
    $output .= "<h1>".$field->original_value."</h1>";
    $output .= "<p>Compatible with Windows and Macintosh, Monarch is a Christian online curriculum for grades 3-12.</p>";
    $output .= "</div>";
  break;  
  case "Switched-On Schoolhouse":
    $output = "<div class='switchedon_curriculum_banner'>";
    $output .= "<h1>".$field->original_value."</h1>";
    $output .= "<p>The #1 choice of homeschoolers for computer-based curriculum, Switched-On Schoolhouse (SOS) is a computer-based curriculum for grades 3-12.</p>";
    $output .= "</div>";
  break;
  case "LIFEPAC":
    $output = "<div class='lifepac_curriculum_banner'>";
    $output .= "<h1>".$field->original_value."</h1>";
    $output .= "<p>AOP’s first and most time-tested curriculum, LIFEPAC is a print curriculum for grades K-12 in which students master concepts before moving to new content.</p>";
    $output .= "</div>";
  break;  
  case "Horizons":
    $output = "<div class='horizon_curriculum_banner'>";
    $output .= "<h1>".$field->original_value."</h1>";
    $output .= "<p>A homeschool favorite, Horizons is a workbook curriculum based on the spiral learning process of introduction, review, and reinforcement.</p>";
    $output .= "</div>";
  break;
  case "Weaver":
    $output = "<div class='weaver_curriculum_banner'>";
    $output .= "<h1>".$field->original_value."</h1>";
    $output .= "<p>The Weaver Curriculum is a traditional, unit-based curriculum for grades PreK-12. Weaver offers a flexible, topical approach to simultaneously teach students of multiple ages.</p>";
    $output .= "</div>";
  break;
  default:
       $output = "";
      
}
print $output;
?>