<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
//dpm($fields);
?>

		<?php
		  //dpm($fields['field_feature']->content);
			if(isset($fields['field_feature']->content) && !empty($fields['field_feature']->content)) {
				$features = preg_replace('#<[^>]+>#', '', explode(";", $fields['field_feature']->content));
				//dpm($features);
			  $print_feature = '';
				$width = 'at_a_glance_full_width';
				if(!empty($features[0])) {
  				foreach ($features as $feature) {
  					// Switch case for feature images
  					list($feature_image, $feature_text) = explode(':', $feature);
  					//print '<li class="'.$feature_image.'">'.$feature_text.'</li>';
  					$width = 'at_a_glance_half_width';
  					$print_feature .= '<div><span class="'.$feature_image.'"></span><span>'.$feature_text.'</span></div>';
  				}  				
				}
			}
		?>
<div class="single-product-spec clear">
  <?php if(!empty($print_feature)) : ?>
	<div class="single-product-spec-ataglance">
		<h2>At a Glance</h2>
		<?php print $print_feature; ?>
	</div>
	<?php endif; ?>
	<div class="single-product-spec-calltoaction <?php print $width; ?>">
	  <?php if(empty($print_feature)) : ?>
	  <h2>At a Glance</h2>
	  <?php endif; ?>
		<ul>
		<?php
			print "<li class='mail-icon'>".aop_utility_loadColorboxDetails($fields['product_id']->raw,'email')."Email to a Friend</a></li>";

			if(isset($fields['field_video_clips'])) {
			  $video = trim(preg_replace('#<[^>]+>#', '', $fields['field_video_clips']->content));
			  if (!empty($video)) {
  				print "<li class='play-icon'>".aop_utility_loadColorboxDetails($fields['product_id']->raw,'sample-video')."View Sample</a></li>";
			  }
			}
			if(isset($fields['field_audio_clips'])) {
			  $audio = trim(preg_replace('#<[^>]+>#', '', $fields['field_audio_clips']->content));
			  if (!empty($audio)) {
  				print "<li class='play-icon'>".aop_utility_loadColorboxDetails($fields['product_id']->raw,'sample-audio')."View Sample</a></li>";
        } 
			}
			if(isset($fields['field_on_demand'])) {
			  $ondemand = trim(preg_replace('#<[^>]+>#', '', $fields['field_on_demand']->content));
			  if (!empty($ondemand)) {
  				print "<li class='play-icon'>".aop_utility_loadColorboxDetails($fields['product_id']->raw,'ondemand')."View On-Demand Presentations</a></li>";
        }
			}
			if(isset($fields['field_scope_sequence'])) {
			  $scope = trim(preg_replace('#<[^>]+>#', '', $fields['field_scope_sequence']->content));
			  if (!empty($scope)) {
  				print "<li class='pdf-icon'>".aop_utility_loadColorboxDetails($fields['product_id']->raw,'scope')."See Scope and Sequence</a></li>";
        }
			}
			if(isset($fields['field_sample_pages'])) {
			  $pages = trim(preg_replace('#<[^>]+>#', '', $fields['field_sample_pages']->content));
			  if (!empty($pages)) {
  				print "<li class='pdf-icon'>".aop_utility_loadColorboxDetails($fields['product_id']->raw,'sample-pdf')."See Sample Pages</a></li>";
        }
			}
			if(isset($fields['field_science_supply'])) {
        $supply = trim(preg_replace('#<[^>]+>#', '', $fields['field_science_supply']->content));
			  if (!empty($supply)) {
  				print "<li class='check-icon'>".aop_utility_loadColorboxDetails($fields['product_id']->raw,'supply')."Science Supply List</a></li></br><i><small>– NOTE: These are only available for 3-12 grade Monarch Science courses and Earth Science and Gen. Science III</small></i>";
        }
			}
		?>			
		</ul>
	</div>
</div>