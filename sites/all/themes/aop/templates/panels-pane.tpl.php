<?php
/**
 * @file panels-pane.tpl.php
 * Main panel pane template
 *
 * Variables available:
 * - $pane->type: the content type inside this pane
 * - $pane->subtype: The subtype, if applicable. If a view it will be the
 *   view name; if a node it will be the nid, etc.
 * - $title: The title of the content
 * - $content: The actual content
 * - $links: Any links associated with the content
 * - $more: An optional 'more' link (destination only)
 * - $admin_links: Administrative links associated with the content
 * - $feeds: Any feed icons or associated with the content
 * - $display: The complete panels display object containing all kinds of
 *   data including the contexts and all of the other panes being displayed.
 */
?>
<?php if ($pane_prefix): ?>
  <?php print $pane_prefix; ?>
<?php endif; ?>
<div class="<?php print $classes; ?>" <?php print $id; ?> <?php print $attributes; ?>>
  <?php if ($admin_links): ?>
    <?php print $admin_links; ?>
  <?php endif; ?>

<?php 

  
  //dpm($content);
  $htag = 'h2';
  $hspan = NULL;
  $hspend = NULL;
  if ($variables['pane']->type === "fieldable_panels_pane" && isset($content['#fieldable_panels_pane']->field_title_type['und'][0]['value'])) {
    if ($content['#fieldable_panels_pane']->field_title_type['und'][0]['value']=='h1') {
      $htag = 'h1';
    } elseif ($content['#fieldable_panels_pane']->field_title_type['und'][0]['value']=='h2') {
      $htag = 'h2';
    } elseif ($content['#fieldable_panels_pane']->field_title_type['und'][0]['value']=='h3') {
      $htag = 'h3';
    } elseif ($content['#fieldable_panels_pane']->field_title_type['und'][0]['value']=='h4') {
      $htag = 'h4';
    } elseif ($content['#fieldable_panels_pane']->field_title_type['und'][0]['value']=='h4b') {
      $htag = 'h4'; $title_attributes = ' '. glow_str_insert('class="pane-title"', 'class="', 'blue ');
    }
  }

?>

  <?php print render($title_prefix); ?>
  <?php if ($title): ?>
    <<?php print $htag; ?><?php print $title_attributes; ?>><?php print $hspan; ?><?php print $title; ?><?php print $hspend; ?></<?php print $htag; ?>>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php if ($feeds): ?>
    <div class="feed">
      <?php print $feeds; ?>
    </div>
  <?php endif; ?>

  <div class="pane-content">
    <?php print render($content); ?>
  </div>

  <?php if ($links): ?>
    <div class="links">
      <?php print $links; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <div class="more-link">
      <?php print $more; ?>
    </div>
  <?php endif; ?>
</div>
<?php if ($pane_suffix): ?>
  <?php print $pane_suffix; ?>
<?php endif; ?>
