<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>
<?php 
  //dpm($field);
  //dpm($row); 
  //print $output;
  //dpm($field->original_value);
?>

<?php
//print $row->field_field_product_1[0]['rendered']['#markup'];
//if(isset($field->original_value)) {
  $grade = '<div class="aop-products-listing-view-grades-group">';
  switch($field->original_value) {
    case "Pre":
      $grade .= $row->field_field_product_1[0]['rendered']['#markup'].' '.$output;
      break;  
    case "K":
      $grade .= $row->field_field_product_1[0]['rendered']['#markup'].' '.$output;
      break;
    case "1":
      $grade .= $row->field_field_product_1[0]['rendered']['#markup'].' '.$output."st Grade ";
      break;
    case "4":
    case "5":
    case "6":
    case "7":
    case "8":
    case "9":
    case "10":
    case "11":
    case "12":
      $grade .= $row->field_field_product_1[0]['rendered']['#markup'].' '.$output."th Grade ";
      break;  
    case "2":
      $grade .= $row->field_field_product_1[0]['rendered']['#markup'].' '.$output."nd Grade ";
      break;
    case "3":
      $grade .= $row->field_field_product_1[0]['rendered']['#markup'].' '.$output."rd Grade ";
      break;
    default:
         $grade .= "";   
  }
  $grade .='</div>';
//}
print $grade;
?>