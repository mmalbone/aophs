<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */

//print $image;
?>
<div id="related-product-block" class="clear">
<?php
$product_image_url = variable_get('aop_images_url');

$product_image = preg_replace('#<[^>]+>#', '', $fields['field_product_image']->content);

$image = $product_image_url.$product_image;

//dpm($fields);	
?>
	<div class="related_product_image">
		<img width="150px" height="100px" alt="No Image" src="<?php print $image; ?>">
	</div>

	<div class="related_product_description">
		<div class="title"><?php print $fields['title']->content; ?></div>
		<?php print $fields['field_hsc_short_description']->content; ?>
		<div class="product-id"><?php print 'PRODUCT ID: '.$fields['sku']->content; ?></div>
		<div class="product-price"><?php print $fields['commerce_price']->content; ?></div>
		<?php print $fields['add_to_cart_form']->content; ?>
	</div>
</div>
<!--
Commerce Product: Product Image (Product Image)
Commerce Product: Product (Product)
Commerce Product: Description (Description)
Commerce Product: Product ID (Product ID)
Commerce Product: Price (Price)
Commerce Product: Add to Cart form (Add to Cart form) 
-->
