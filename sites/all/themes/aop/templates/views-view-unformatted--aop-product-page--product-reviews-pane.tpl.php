<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<div class="product-rating-wrapper clear">
  <div class="product-rating-totals">
    <div class="product-rating-percentage"><!-- 50% positive --></div>
    <div class="product-rating-count"><!-- 2 reviews --></div>
  </div>
  <div class="product-rating-reviews">
  <?php if (!empty($title)): ?>
    <h3><?php print $title; ?></h3>
  <?php endif; ?>
  <?php foreach ($rows as $id => $row): ?>
    <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
      <?php print $row; ?>
    </div>
  <?php endforeach; ?>
  </div>
</div>