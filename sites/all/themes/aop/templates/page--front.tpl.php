<?php
/**
 * @file
 * Returns the HTML for a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728148
 */
?>

<div id="homepage-promo">
  <div id="homepage-promo-first">
     <?php print render($page['homepage_promo_1']); ?>
  </div>
  <div id="homepage-promo-second">
     <?php print render($page['homepage_promo_2']); ?>
  </div>
</div>

<div id="page">

	<header class="header" id="header" role="banner">
	
		<div id="header_wrapper">	  
			<?php if ($logo): ?>
			<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="header__logo" id="logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" class="header__logo-image" /></a>
			<?php endif; ?>
			
			<div id="aop-nav-search-wrapper">
			
				<div id="aop-search">
					<?php print render($page['aop_search']); ?>
				</div>
				<div id="navigation" class="clearfix">
					<div id="primary-nav">
						<?php print render($page['primary_nav']); ?>
					</div>
					<div id="secondary-nav">
						<?php print render($page['secondary_nav']); ?>
					</div>
				</div>
			</div>
		
		</div><!-- end header wrapper -->
	</header>

  <div id="main">

<!--
    <div id="navigation">
		<div id="primary-nav">
		  <?php print render($page['primary_nav']); ?>
		</div>
		<div id="secondary-nav">
		  <?php print render($page['secondary_nav']); ?>
	  	</div>
    </div>	
-->

    <div id="content" class="column" role="main">
      <?php print $breadcrumb; ?>
      <a id="main-content"></a>
      <?php print $messages; ?>
      <?php print render($tabs); ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
    </div>

  </div>

</div>

<div id="aop-footer">

	<div id="aop-footer-col-wrapper" class="clearfix">
	
		<div id="aop-footer-col-1">
		<?php print render($page['aop_footer_col_1']); ?>
		</div>
		
		<div id="aop-footer-col-2">
		<?php print render($page['aop_footer_col_2']); ?>
		</div>
		
		<div id="aop-footer-col-3">
		<?php print render($page['aop_footer_col_3']); ?>
		</div>
		
		<div id="aop-footer-col-4">
		<?php print render($page['aop_footer_col_4']); ?>
		</div>
	
	</div>

    <div id="aop-footer-notices" class="clear">
      <?php print render($page['aop_footer_notices']); ?>
    </div>

</div>


<?php print render($page['bottom']); ?>
