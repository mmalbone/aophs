<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
$product_image_url = variable_get('aop_images_url');
//dpm($fields);
//$product_id = $fields['product_id']->raw;
//$product_object = commerce_product_load($fields['product_id']->raw);

//dpm($product_object);
//$product_image = preg_replace('#<[^>]+>#', '', '/sites/default/files'.$fields['field_product_image']->content);
//$image = $product_image_url.(string)$product_image;
$product_image = preg_replace('#<[^>]+>#', '', $fields['field_product_image']->content);
$image = $product_image_url.$product_image;

//check if this product has a demo
if(isset($fields['field_has_demo'])):
  $has_demo = strip_tags($fields['field_has_demo']->content);
else:
  $has_demo = 'no';
endif;


//print $image;
?>
<div id="single-product-page">
	<div class="single-product-image-description clear">
		<div class="single-product-image">
			<img width="150px" height="100px" alt="No Image" src="<?php echo $image; ?>">
		</div>
		<div class="single-product-detail">
			<div class="single-product-title"><?php print $fields['title']->content; ?></div>
			<div class="single-product-description"><?php print $fields['field_hsc_long_description']->content; ?></div>
			<div class="single-product-id-availability"><?php print 'PRODUCT ID '.$fields['sku_1']->content.' / '.'AVAILABILITY - NOW SHIPPING'; ?></div>
		</div>
	</div>
	<div class="single-product-spec-cart clear">
		<div class="single-product-spec clear">
			<div class="single-product-spec-ataglance">
				<h2>At a Glance</h2>
				<div><span class="book-icon"></span>52 Unit Workbooks</div>
				<div><span class="video-icon"></span>11 Video Resources</div>
				<div><span class="folder-icon"></span>12 Suplemental Tools</div>
				<div><span class="cross-icon"></span>1 Million Sold</div>
				<div><span class="monitor-icon"></span>Access to online Resources</div>
				<div><span class="thumbs-icon"></span>125 Positive Reviews</div>
			</div>
			<div class="single-product-spec-calltoaction">
				<?php //include ($directory."/inc/at_a_glance.php"); ?>
				
				<?php 
					$text = aop_utility_loadAtAGlanceSection($fields['product_id']->raw);
					print $text; 
				?>
				<?php if($has_demo == 'yes'): ?>
				<div><span class="presentation-icon"></span><a class="colorbox-load" href="https://monarch.aop.com/api/render_lesson/<?php print variable_get('aop_product_demo_year').'_'.strtoupper($fields['sku_1']->raw) ?>?width=70%25&height=70%25&iframe=true">Sample This Course</a></div>
				<?php endif; ?>
				
			</div>
		</div>
		<div class="single-product-cart">
			<div class="single-product-cart-price"><?php print $fields['commerce_price']->content; ?></div>
			<div class="single-product-cart-addtocart"><?php print $fields['add_to_cart_form']->content; ?></div>
			<div class="single-product-cart-saveforlater"><span class="icon-star"></span> Save for later</div>
		</div>
	</div>
<!--
	<div class="single-product-inthebox">
		<table border="0" bordercolor="" style="background-color:" width="100%" cellpadding="3" cellspacing="3">
			<tr>
				<th>In the Box</th>
				<th>Sample Pages and Resources</th>
				<th>Related Downloads</th>
			</tr>
			<tr>
				<td>
					<div class="div-inline">Subject</div>
					<div class="div-inline clearfix">
						<ul>
							<li>Sub-Item 1</li>
							<li>Sub-Item 2</li>
							<li>Sub-Item 3</li>
							<li>Sub-Item 4</li>
							<li>Sub-Item 5</li>
							<li>Sub-Item 6</li>
							<li>Sub-Item 7</li>
						</ul>
					</div>
				</td>
				<td>
					<div><a href="#">Sample Video</a></div>
					<div><a href="#">Sample Worksheet</a></div>
					<div><a href="#">Sample Unit Chapter</a></div>
					<div><a href="#">Sample Worksheet</a></div>
					<div><a href="#">Sample Worksheet</a></div>
				</td>
				<td>
					<div><a href="#">Instruction Video</a></div>
					<div><a href="#">Worksheet</a></div>
					<div><a href="#">Chapter Review</a></div>
					<div><a href="#">Worksheet</a></div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="div-inline">Subject</div>
					<div class="div-inline clearfix">
						<ul>
							<li>Sub-Item 1</li>
							<li>Sub-Item 2</li>
							<li>Sub-Item 3</li>
							<li>Sub-Item 4</li>
							<li>Sub-Item 5</li>
							<li>Sub-Item 6</li>
							<li>Sub-Item 7</li>
						</ul>
					</div>
				</td>
				<td>
					<div><a href="#">Sample Video</a></div>
					<div><a href="#">Sample Worksheet</a></div>
					<div><a href="#">Sample Unit Chapter</a></div>
					<div><a href="#">Sample Worksheet</a></div>
					<div><a href="#">Sample Worksheet</a></div>
				</td>
				<td>
					<div><a href="#">Instruction Video</a></div>
					<div><a href="#">Worksheet</a></div>
					<div><a href="#">Chapter Review</a></div>
					<div><a href="#">Worksheet</a></div>
				</td>
			</tr>
		</table>
	</div>
-->
	<div class="single-product-recommended">
		<div class="single-product-recommended-header clear">
			<span class="single-product-recommended-header-title">Recommended with these products and tools</span>
			<!-- <span class="single-product-recommended-header-action">SEE MORE RESOURCES</span> -->
		</div>
		<div class="single-product-recommended-carousel clear">
			<?php
				//load the related products view by name and product id
				print views_embed_view('aop_product_page', 'related_products_block',arg(1));
			?>
		</div>
	</div>
</div>
