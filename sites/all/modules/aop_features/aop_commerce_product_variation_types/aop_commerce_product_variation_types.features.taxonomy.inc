<?php
/**
 * @file
 * aop_commerce_product_variation_types.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function aop_commerce_product_variation_types_taxonomy_default_vocabularies() {
  return array(
    'avatax_tax_codes' => array(
      'name' => 'AvaTax Tax codes',
      'machine_name' => 'avatax_tax_codes',
      'description' => NULL,
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'awards' => array(
      'name' => 'Awards',
      'machine_name' => 'awards',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'category' => array(
      'name' => 'Subject',
      'machine_name' => 'category',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'curriculum' => array(
      'name' => 'Homeschool Curriculum',
      'machine_name' => 'curriculum',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'dvd_categories' => array(
      'name' => 'DVD Categories',
      'machine_name' => 'dvd_categories',
      'description' => 'Categories for Family Entertainment DVDs',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'gender' => array(
      'name' => 'Grade Level',
      'machine_name' => 'gender',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
  );
}
