<?php
/**
 * @file
 * aop_commerce_product_variation_types.features.inc
 */

/**
 * Implements hook_commerce_product_default_types().
 */
function aop_commerce_product_variation_types_commerce_product_default_types() {
  $items = array(
    'curriculum' => array(
      'type' => 'curriculum',
      'name' => 'Curriculum',
      'description' => '',
      'help' => '',
      'revision' => '1',
    ),
    'dvd' => array(
      'type' => 'dvd',
      'name' => 'Family Entertainment',
      'description' => '',
      'help' => '',
      'revision' => '1',
    ),
    'resource' => array(
      'type' => 'resource',
      'name' => 'Resource',
      'description' => '',
      'help' => '',
      'revision' => '1',
    ),
  );
  return $items;
}
