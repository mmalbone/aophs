<?php
/**
 * @file
 * aop_curriculum_blocks.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function aop_curriculum_blocks_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-about_the_author'] = array(
    'cache' => -1,
    'custom' => '0',
    'machine_name' => 'about_the_author',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'blog/creating-love-learning
blog/my-5-goals-start-school-year',
    'roles' => array(),
    'themes' => array(
      'aophs' => array(
        'region' => 'sidebar_second',
        'status' => '1',
        'theme' => 'aophs',
        'weight' => '-66',
      ),
      'commerce_kickstart_admin' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'commerce_kickstart_admin',
        'weight' => '0',
      ),
    ),
    'title' => 'About the Author',
    'visibility' => '1',
  );

  $export['block-countact_us_menu'] = array(
    'cache' => -1,
    'custom' => '0',
    'machine_name' => 'countact_us_menu',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'aophs' => array(
        'region' => 'menu',
        'status' => '1',
        'theme' => 'aophs',
        'weight' => '0',
      ),
      'commerce_kickstart_admin' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'commerce_kickstart_admin',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['block-curriculum_comparison'] = array(
    'cache' => -1,
    'custom' => '0',
    'machine_name' => 'curriculum_comparison',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'aophs' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'aophs',
        'weight' => '0',
      ),
      'commerce_kickstart_admin' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'commerce_kickstart_admin',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['block-curriculum_horizons_block'] = array(
    'cache' => -1,
    'custom' => '0',
    'machine_name' => 'curriculum_horizons_block',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'aophs' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'aophs',
        'weight' => '0',
      ),
      'commerce_kickstart_admin' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'commerce_kickstart_admin',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['block-curriculum_lifepac_block'] = array(
    'cache' => -1,
    'custom' => '0',
    'machine_name' => 'curriculum_lifepac_block',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'aophs' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'aophs',
        'weight' => '0',
      ),
      'commerce_kickstart_admin' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'commerce_kickstart_admin',
        'weight' => '0',
      ),
    ),
    'title' => 'Curriculum LifePac Block',
    'visibility' => '0',
  );

  $export['block-curriculum_monarch_block'] = array(
    'cache' => -1,
    'custom' => '0',
    'machine_name' => 'curriculum_monarch_block',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'aophs' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'aophs',
        'weight' => '0',
      ),
      'commerce_kickstart_admin' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'commerce_kickstart_admin',
        'weight' => '0',
      ),
    ),
    'title' => 'Curriculum Monarch Block',
    'visibility' => '0',
  );

  $export['block-curriculum_switched_on_block'] = array(
    'cache' => -1,
    'custom' => '0',
    'machine_name' => 'curriculum_switched_on_block',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'aophs' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'aophs',
        'weight' => '0',
      ),
      'commerce_kickstart_admin' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'commerce_kickstart_admin',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['block-curriculum_weaver_block'] = array(
    'cache' => -1,
    'custom' => '0',
    'machine_name' => 'curriculum_weaver_block',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'aophs' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'aophs',
        'weight' => '0',
      ),
      'commerce_kickstart_admin' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'commerce_kickstart_admin',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['block-footer_copyright'] = array(
    'cache' => -1,
    'custom' => '0',
    'machine_name' => 'footer_copyright',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'aophs' => array(
        'region' => 'footer_bottom',
        'status' => '1',
        'theme' => 'aophs',
        'weight' => '0',
      ),
      'commerce_kickstart_admin' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'commerce_kickstart_admin',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['block-footer_social_icons'] = array(
    'cache' => -1,
    'custom' => '0',
    'machine_name' => 'footer_social_icons',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'aophs' => array(
        'region' => 'footer_second',
        'status' => '1',
        'theme' => 'aophs',
        'weight' => '-58',
      ),
      'commerce_kickstart_admin' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'commerce_kickstart_admin',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['block-front_promo_tout_right'] = array(
    'cache' => -1,
    'custom' => '0',
    'machine_name' => 'front_promo_tout_right',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'aophs' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'aophs',
        'weight' => '0',
      ),
      'commerce_kickstart_admin' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'commerce_kickstart_admin',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['block-homepage_hero_left'] = array(
    'cache' => -1,
    'custom' => '0',
    'machine_name' => 'homepage_hero_left',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'aophs' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'aophs',
        'weight' => '0',
      ),
      'commerce_kickstart_admin' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'commerce_kickstart_admin',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['block-homepage_hero_right'] = array(
    'cache' => -1,
    'custom' => '0',
    'machine_name' => 'homepage_hero_right',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'aophs' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'aophs',
        'weight' => '0',
      ),
      'commerce_kickstart_admin' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'commerce_kickstart_admin',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['block-horizons_listing_header'] = array(
    'cache' => -1,
    'custom' => '0',
    'machine_name' => 'horizons_listing_header',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'aophs' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'aophs',
        'weight' => '0',
      ),
      'commerce_kickstart_admin' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'commerce_kickstart_admin',
        'weight' => '0',
      ),
    ),
    'title' => 'Horizons',
    'visibility' => '0',
  );

  $export['block-let_us_help_you'] = array(
    'cache' => -1,
    'custom' => '0',
    'machine_name' => 'let_us_help_you',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'products
resources
family-entertainment',
    'roles' => array(),
    'themes' => array(
      'aophs' => array(
        'region' => 'sidebar_second',
        'status' => '1',
        'theme' => 'aophs',
        'weight' => '-70',
      ),
      'commerce_kickstart_admin' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'commerce_kickstart_admin',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '1',
  );

  $export['block-lifepac_listing_header'] = array(
    'cache' => -1,
    'custom' => '0',
    'machine_name' => 'lifepac_listing_header',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'aophs' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'aophs',
        'weight' => '0',
      ),
      'commerce_kickstart_admin' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'commerce_kickstart_admin',
        'weight' => '0',
      ),
    ),
    'title' => 'LifePac',
    'visibility' => '0',
  );

  $export['block-monarch_listing_header'] = array(
    'cache' => -1,
    'custom' => '0',
    'machine_name' => 'monarch_listing_header',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'aophs' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'aophs',
        'weight' => '0',
      ),
      'commerce_kickstart_admin' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'commerce_kickstart_admin',
        'weight' => '0',
      ),
    ),
    'title' => 'Monarch',
    'visibility' => '0',
  );

  $export['block-switched_on_listing_header'] = array(
    'cache' => -1,
    'custom' => '0',
    'machine_name' => 'switched_on_listing_header',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'aophs' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'aophs',
        'weight' => '0',
      ),
      'commerce_kickstart_admin' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'commerce_kickstart_admin',
        'weight' => '0',
      ),
    ),
    'title' => 'Switched-On Schoolhouse',
    'visibility' => '0',
  );

  $export['block-weaver_listing_header'] = array(
    'cache' => -1,
    'custom' => '0',
    'machine_name' => 'weaver_listing_header',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'aophs' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'aophs',
        'weight' => '0',
      ),
      'commerce_kickstart_admin' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'commerce_kickstart_admin',
        'weight' => '0',
      ),
    ),
    'title' => 'Weaver',
    'visibility' => '0',
  );

  return $export;
}
