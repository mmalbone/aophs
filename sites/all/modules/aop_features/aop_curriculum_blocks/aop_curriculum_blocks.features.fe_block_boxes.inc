<?php
/**
 * @file
 * aop_curriculum_blocks.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function aop_curriculum_blocks_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'About the Author';
  $fe_block_boxes->format = 'plain_text';
  $fe_block_boxes->machine_name = 'about_the_author';
  $fe_block_boxes->body = 'Text and picture goes here.';

  $export['about_the_author'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Contact us menu for call and chat';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'countact_us_menu';
  $fe_block_boxes->body = '<ul id="contact_us_menu"><li><span class="number"><a href="tel://1-800-622-3070">Call Us 1.800.622.3070</a></span></li><li class="contact_us_menu_chat_button"><span class="chat_with_us" title="Chat with us">Chat</span><ul class="contact_us_menu_links"><li class="contact_us_menu_chat"><a href="https://secure.livechatinc.com/licence/3247562/open_chat.cgi?groups=10" target="_blank" title="Chat with us">Chat with us from 7 a.m. – 5 p.m. (CT)</a></li><li class="contact_us_menu_call"><a href="/support/schedule-a-callback" title="Call Us">Have a curriculum expert call you.</a></li><li class="contact_us_menu_email"><a href="/contact" title="Email Us">Contact us</a></li></ul></li></ul>';

  $export['countact_us_menu'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Block linking to curriculum comparison page';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'curriculum_comparison';
  $fe_block_boxes->body = '<div class="curriculum_comparison_block"><div class="text">Use our quick comparison checklist to review your options side by side.</div><div><a class="button-gray-background" href="/find-a-curriculum/curriculum-comparision"><em>Compare</em> <strong>PRODUCTS</strong></a></div></div>';

  $export['curriculum_comparison'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Curriculum Horizons Block';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'curriculum_horizons_block';
  $fe_block_boxes->body = '<div id="curriculum-block-wrapper-horizons" class="horizon-curriculum clearfix">
  
<div class="curriculum_close_detail">X</div>

<div class="curriculum-more-info curriculum-more-info-horizons" id="horizons-more-info">&nbsp;</div>

<div class="clearfix">

  <div class="curriculum-detail-left">

    <div class="curriculum-detail-logo"><img alt="SOS 12th Grade 5-Subject Set" src="/sites/default/files/curriculum-logo-Horizons.png" style="height:80px; width:80px" /></div>  
    
    <div class="curriculum-title">Horizons</div>
    
    <div class="curriculum-grade">Grades PreK-12</div>
    
    <div class="curriculum-media">PRINT / TEACHER-LED / PLANNED LESSONS</div>
    
    <div class="curriculum-more-info-summary" id="horizons-more-info-summary">Brightly illustrated and teacher-led print curriculum</div>
 </div>
 
  <div class="curriculum-detail-image"><img alt="SOS 12th Grade 5-Subject Set" src="/sites/default/files/curriculum-detail-Horizons.png" style="height:300px; width:300px" /></div>

</div>

<div class="curriculum-full-description horizon-full-description">
<p>A homeschool favorite, Horizons is a workbook curriculum based on the spiral learning process of introduction, review, and reinforcement. Horizons offers courses in Preschool, Math (K-8), Phonics &amp; Reading (K-3), Penmanship (1-6), Spelling and Vocabulary (1-3), Health (K-8), and Physical Education (PreK-12).</p>
<a class="button-blue-background" href="/products?f%255B0%255D=field_collection:10&f[0]=field_collection:75">Shop HORIZONS</a></div>
</div>
';

  $export['curriculum_horizons_block'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Curriculum LifePac Block';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'curriculum_lifepac_block';
  $fe_block_boxes->body = '<div id="curriculum-block-wrapper-lifepack" class="lifepac-curriculum clearfix">
  
<div class="curriculum_close_detail">X</div>

<div class="curriculum-more-info curriculum-more-info-lifepack" id="lifepack-more-info">&nbsp;</div>

<div class="clearfix">

  <div class="curriculum-detail-left">

    <div class="curriculum-detail-logo"><img alt="SOS 12th Grade 5-Subject Set" src="/sites/default/files/curriculum-logo-LIFEPAC.png" style="height:80px; width:80px" /></div>
    
    
    <div class="curriculum-title">LIFEPAC ®</div>
    
    <div class="curriculum-grade">Grades K-12</div>
    
    <div class="curriculum-media">PRINT / STUDENT PACED / CUSTOMIZED LESSONS</div>
        
    <div class="curriculum-more-info-summary" id="lifepack-more-info-summary">Self-paced print curriculum</div>

  </div>
  
    <div class="curriculum-detail-image"><img alt="SOS 12th Grade 5-Subject Set" src="/sites/default/files/curriculum-detail-LIFEPAC.png" style="height:300px; width:300px" /></div>

</div>

<div class="curriculum-full-description lifepack-full-description">
<p>AOP’s first and most time-tested curriculum, LIFEPAC is organized into self-directed units called worktexts that can be completed in 3-4 weeks. LIFEPAC offers integrated lesson plans, a mastery-based format in which students master concepts before moving to new content, five main subjects, and 15 electives.</p>
<a class="button-blue-background" href="/products?f%255B0%255D=field_collection:10&f[0]=field_collection:10">Shop LIFEPAC</a></div>
</div>
';

  $export['curriculum_lifepac_block'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Curriculum Monarch Block';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'curriculum_monarch_block';
  $fe_block_boxes->body = '<div id="curriculum-block-wrapper-monarch" class="monarch-curriculum clearfix">
  
<div class="curriculum_close_detail">X</div>

<div class="curriculum-more-info curriculum-more-info-monarch" id="monarch-more-info">&nbsp;</div>


<div class="clearfix">

  <div class="curriculum-detail-left">

    <div class="curriculum-detail-logo"><img alt="SOS 12th Grade 5-Subject Set" src="/sites/default/files/curriculum-logo-Monarch.png" style="height:80px; width:80px" /></div>
    
    
    <div class="curriculum-title">Monarch</div>
    
    <div class="curriculum-grade">Grades 3-12</div>
    
    <div class="curriculum-media">DIGITAL / ONLINE CURRICULUM / NO ENROLLMENT</div>
        
    <div class="curriculum-more-info-summary" id="monarch-more-info-summary">Media-rich, online homeschooling curriculum</div>

  </div>
  
  <div class="curriculum-detail-image"><img alt="SOS 12th Grade 5-Subject Set" src="/sites/default/files/curriculum-detail-Monarch.png" style="height:300px; width:300px" /></div>

</div>

<div class="curriculum-full-description monarc-full-description">
<p>Compatible with Windows and Macintosh, Monarch is filled with interactive games and movie clips, as well as automatic grading and recordkeeping, Monarch is an Excellence in Education award-winning curriculum that offers five main subjects and over 45 electives.</p>
<a class="button-blue-background" href="/products?f%255B0%255D=field_collection:10&f[0]=field_collection:9">Shop MONARCH</a></div>
</div>
';

  $export['curriculum_monarch_block'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Curriculum Switched-On Block';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'curriculum_switched_on_block';
  $fe_block_boxes->body = '<div id="curriculum-block-wrapper-switchedon" class="switch-on-curriculum clearfix">
  
<div class="curriculum_close_detail">X</div>

<div class="curriculum-more-info curriculum-more-info-switchedon" id="switchedon-more-info">&nbsp;</div>


<div class="clearfix">

  <div class="curriculum-detail-left">
    <div class="curriculum-detail-logo"><img alt="SOS 12th Grade 5-Subject Set" src="/sites/default/files/curriculum-logo-SOS.png" style="height:80px; width:80px" /></div>
    
    
    <div class="curriculum-title">Switched-On</div>
    
    <div class="curriculum-grade">Grades 3-12</div>
    
    <div class="curriculum-media">DIGITAL / NOT ONLINE CURRICULUM</div>
        
    <div class="curriculum-more-info-summary" id="switchedon-more-info-summary">Computer curriculum on CD-ROM</div>

  </div>

  <div class="curriculum-detail-image"><img alt="SOS 12th Grade 5-Subject Set" src="/sites/default/files/curriculum-detail-SOS.png" style="height:300px; width:300px" /></div>

</div>
<div class="curriculum-full-description switcheon-full-description">
<p>The #1 choice of homeschoolers for computer-based curriculum, Switched-On Schoolhouse (SOS) is available on CD-ROM for Windows operating systems. SOS offers dynamic lessons packed with multimedia elements, automatic grading and recordkeeping, five main subjects, and over 45 electives.</p>
<a class="button-blue-background" href="/products?f%255B0%255D=field_collection:10&f[0]=field_collection:8">Shop SWITCHED-ON</a></div>
</div>
';

  $export['curriculum_switched_on_block'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Curriculum The Weaver Block';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'curriculum_weaver_block';
  $fe_block_boxes->body = '<div id="curriculum-block-wrapper-weaver" class="the-weaver-curriculum clearfix">
  
<div class="curriculum_close_detail">X</div>

<div class="curriculum-more-info curriculum-more-info-weavercurriculum" id="weavercurriculum-more-info">&nbsp;</div>


<div class="clearfix">

  <div class="curriculum-detail-left">
  
    <div class="curriculum-detail-logo"><img alt="SOS 12th Grade 5-Subject Set" src="/sites/default/files/curriculum-logo-Weaver.png" style="height:80px; width:80px" /></div>
    
    
    <div class="curriculum-title">Weaver</div>
    
    <div class="curriculum-grade">Grades PreK-12</div>
    
    <div class="curriculum-media">PRINT / TEACHER-LED / CUSTOMIZED LESSONS</div>
        
    <div class="curriculum-more-info-summary" id="weavercurriculum-more-info-summary">Unit-study, hands-on curriculum for teaching multiple ages at once</div>

  </div>
  
  <div class="curriculum-detail-image"><img alt="SOS 12th Grade 5-Subject Set" src="/sites/default/files/curriculum-detail-Weaver.png" style="height:300px; width:300px" /></div>

</div>

<div class="curriculum-full-description weaver-full-description">
<p>The Weaver Curriculum is a traditional, unit-based curriculum for grades PreK-12. Weaver offers a flexible, topical approach to simultaneously teach students of multiple ages. This hands-on program features Old and New Testament lessons that weave Scripture through language arts, history, science, and more.</p>
<a class="button-blue-background" href="/products?f%255B0%255D=field_collection:10&f[0]=field_collection:11">Shop THE WEAVER</a></div>
</div>
';

  $export['curriculum_weaver_block'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Footer Copyright';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'footer_copyright';
  $fe_block_boxes->body = '<div class="copyright"><ul><li><a href="/return-policy">Return Policy</a></li><li><a href="/legal_info">Terms &amp; Conditions</a></li><li><a href="http://www.aoppartners.com/" target="_blank">Partner Info</a></li></ul></div>';

  $export['footer_copyright'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Footer Social Icons';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'footer_social_icons';
  $fe_block_boxes->body = '<div class="footer-social-icons clear"><div class="footer-social-icon"><a class="facebook" href="https://www.facebook.com/aophomeschooling" title="Facebook"><img alt="Facebook" src="/sites/all/themes/aophs/images/facebook.png" style="height: 30px; width: 30px;"> </a></div><div class="footer-social-icon"><a class="google-plus" href="https://plus.google.com/b/112967472975789780547/#112967472975789780547/posts" title="Google+"><img alt="Google +" src="/sites/all/themes/aophs/images/google.png" style="height: 30px; width: 30px;"> </a></div><div class="footer-social-icon"><a class="twitter" href="https://twitter.com/homeschoolers" title="Twitter"><img alt="Twitter" src="/sites/all/themes/aophs/images/twitter.png" style="height: 30px; width: 30px;"> </a></div><div class="footer-social-icon"><a class="pinterest" href="http://www.pinterest.com/aophomeschool/" title="Pinterest"><img alt="Pinterest" src="/sites/all/themes/aophs/images/pinterest.png" style="height: 30px; width: 30px;"> </a></div><div class="footer-social-icon"><a class="instagram" href="http://instagram.com/aophomeschooling" title="Instagram"><img alt="Instagram" src="/sites/all/themes/aophs/images/instagram.png" style="height: 30px; width: 30px;"> </a></div></div><p>&nbsp;</p>';

  $export['footer_social_icons'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Promo Tout Right';
  $fe_block_boxes->format = 'markdown';
  $fe_block_boxes->machine_name = 'front_promo_tout_right';
  $fe_block_boxes->body = '<span><a class="tout" href="/find-a-curriculum">Not sure which curriculum to use? We can help.</a></span>';

  $export['front_promo_tout_right'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Homepage Hero Left';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'homepage_hero_left';
  $fe_block_boxes->body = '<div class="content-title">Homeschooling made for you.</div><p><a class="button-blue-background" href="/find-a-curriculum" title="Homeschooling made for you"><em>Shop all</em> <strong> PRODUCTS</strong> </a></p>';

  $export['homepage_hero_left'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Homepage Hero Right';
  $fe_block_boxes->format = 'markdown';
  $fe_block_boxes->machine_name = 'homepage_hero_right';
  $fe_block_boxes->body = '<div class="hero-right-top"><h2>We Are Uncommon</h2><a class="button-blue-background" href="/uncommon-homeschooling" title="See why we are uncommon"><em>See</em><strong> WHY</strong></a></div><div class="hero-right-bottom"><div class="hero-right-bottom-left"><div><em>New to AOP?</em><br><a class="colorbox-node" href="webform/new-alpha-omega-publications" data-href="webform/new-alpha-omega-publications?width=650&height=800">GET A FREE CURRICULUM BROCHURE</a></div></div><div class="hero-right-bottom-right"><div><a href="/blog-categories/homeschool-view"><em>Discover</em><br>WHAT\'S TRENDING NOW</a></div></div></div><p>&nbsp;</p>';

  $export['homepage_hero_right'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Curriculum Listing Header Horizons';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'horizons_listing_header';
  $fe_block_boxes->body = '<p>Horizons is a brightly illustrated, engaging workbook curriculum filled with consumable lessons and hands-on activities.</p>';

  $export['horizons_listing_header'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Not sure what you are looking for, let us help you';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'let_us_help_you';
  $fe_block_boxes->body = '<div class="foryourfamily"><p><a href="/find-a-curriculum">Not sure what you\'re looking for?&nbsp; Let us help.</a></p></div>';

  $export['let_us_help_you'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Curriculum Listing Header LifePac';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'lifepac_listing_header';
  $fe_block_boxes->body = '<p>A Christian homeshool curriculum for grades K-12. Designed by a team of accomplished educators, LifePac is based on the principle of mastering learning.</p>';

  $export['lifepac_listing_header'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Curriculum Listing Header Monarch';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'monarch_listing_header';
  $fe_block_boxes->body = '<p>Compatible with Windows and Macintosh, Monarch is filled with interactive games and movie clips, as well as automatic grading and recordkeeping, Monarch is an Excellence in Education award-winning curriculum that offers five main subjects and over 45 electives.</p>';

  $export['monarch_listing_header'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Curriculum Listing Header Switched-On Schoolhouse';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'switched_on_listing_header';
  $fe_block_boxes->body = '<p>The #1 choice of homeschoolers for computer-based curriculum, Switched-On Schoolhouse (SOS) is available on CD-ROM for Windows operating systems. SOS offers dynamic lessons packed with multimedia elements, automatic grading and recordkeeping, five main subjects, and over 45 electives.</p>';

  $export['switched_on_listing_header'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Curriculum Listing Header Weaver';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'weaver_listing_header';
  $fe_block_boxes->body = '<div id="curriculum-block-wrapper-weaver" class="clearfix">
<div class="curriculum_close_detail">X</div>

<div class="curriculum-more-info curriculum-more-info-weavercurriculum" id="weavercurriculum-more-info">&nbsp;</div>

<div class="curriculum-detail-logo"><img alt="SOS 12th Grade 5-Subject Set" src="/sites/default/files/curriculum-logo-Weaver.png" style="height:80px; width:80px" /></div>

<div class="curriculum-detail-image"><img alt="SOS 12th Grade 5-Subject Set" src="/sites/default/files/curriculum-detail-Weaver.png" style="height:300px; width:300px" /></div>

<div class="curriculum-title">Weaver</div>

<div class="curriculum-grade">Grades PreK-12</div>

<div class="curriculum-more-info-summary" id="weavercurriculum-more-info-summary">Unit-study, hands-on curriculum for teaching multiple ages at once</div>

<div class="curriculum-full-description weaver-full-description">
<p>The Weaver Curriculum is a traditional, unit-based curriculum for grades PreK-12. Weaver offers a flexible, topical approach to simultaneously teach students of multiple ages. This hands-on program features Old and New Testament lessons that weave Scripture through language arts, history, science, and more.</p>
<a class="button-blue-background" href="/products?f%255B0%255D=field_collection:10&f[0]=field_collection:11">Shop THE WEAVER</a></div>
</div>
';

  $export['weaver_listing_header'] = $fe_block_boxes;

  return $export;
}
