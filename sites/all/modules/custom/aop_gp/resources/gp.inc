<?php

/**
 * @file
 * Contains the resource callbacks for Subscriptions.
 */

/**
 * Returns a single subscription.
 *
 * @see aop_gp_retrieve_entity()
 */

function aop_gp_order_retrieve($date) {

   return aop_gp_load_order($date);

}


function aop_gp_load_order($date) { 

  $orders = new stdClass;

  if (strlen($date) > 0) { 

    $update_date = strtotime($date);
    watchdog("test", "Update date is: ".$update_date);

    $query = new EntityFieldQuery();

    $query
      ->entityCondition('entity_type', 'commerce_order', '=')
      ->propertyCondition('changed', $update_date, '>');

    $result = $query->execute();

  }

  return $result;

}


