<?php

/********
 * Implements custom events for errors that are used by Drupal's rules module
 *
 * Implements hooK_rules_event_info()
 */

function aop_assist_rules_event_info() { 

  $items = array(
    'aop_assist_mondis_error' => array(
      'group' => t('AOP Assist'),      
      'label' => t('Unable to connect to Mondis'),
      'module' => 'aop_assist',
      'arguments' => array(
         'error_code' => array(
            'type' => 'string',
            'label' => 'Error code',
          ),
        ),
      ),
    'aop_assist_monarch_error' => array(
      'group' => t('AOP Assist'),      
      'label' => t('Unable to connect to Monarch'),
      'module' => 'aop_assist',
      'arguments' => array(
         'error_code' => array(
            'type' => 'string',
            'label' => 'Error code',
          ),
        ),
      ),
    'aop_assist_mule_error' => array(
      'group' => t('AOP Assist'),      
      'label' => t('Unable to connect to Mule/Assist'),
      'module' => 'aop_assist',
      'arguments' => array(
         'error_code' => array(
            'type' => 'string',
            'label' => 'Error code',
          ),
        ),
    ),        
  );

  return $items;

}