function update_filter(){
  grade_opts = "";
  subject_opts = "";
  curriculum_opts = "";
  media_opts = "";
  category_opts = "";

  if( jQuery(".facets_desktop").is(":visible") ){
    jQuery("input[name=check_g]:checked").each( function(){
      grade_opts += this.value;
      jQuery( this ).parent().addClass("checked");
    } );
    jQuery("input[name=check_s]:checked").each( function(){
      subject_opts += this.value;
      jQuery( this ).parent().addClass("checked");
    } );
    jQuery("input[name=check_c]:checked").each( function(){
      curriculum_opts += this.value;
      jQuery( this ).parent().addClass("checked");
    } );
    jQuery("input[name=check_d]:checked").each( function(){
      category_opts += this.value;
      jQuery( this ).parent().addClass("checked");
    } );
    jQuery("input[name=check_m]:checked").each( function(){
      media_opts += this.value;
      jQuery( this ).parent().addClass("checked");
    } );
  } else {
    grade_opts = jQuery("select[name=check_g]").val() || "";
    subject_opts = jQuery("select[name=check_s]").val() || "";
    curriculum_opts = jQuery("select[name=check_c]").val() || "";
    category_opts = jQuery("select[name=check_d]").val() || "";
    media_opts = jQuery("select[name=check_m]").val() || "";
  }

  base_location = document.location.href.replace( /\/[gscmd]\/[^\/]+/gi, '' ); /* REMOVE OLD ITEMS FROM URL */
  base_location = base_location.replace( /\?.*/gi, '' );
  base_query = document.location.href.replace( /http.*\??/gi, '' );
  if( base_query != '' ){ base_query = "?" + base_query; }

  document.location = base_location + grade_opts + subject_opts + curriculum_opts + category_opts + media_opts + base_query ;
}

function reset_filter( which ){
  if( which == "g" ){
    jQuery("input[name=check_g]:checked").each( function(){
      this.checked=false
    } );
  } 
  if( which == "s" ){
    jQuery("input[name=check_s]:checked").each( function(){
      this.checked=false;
    } );
  } 
  if( which == "c" ){
    jQuery("input[name=check_c]:checked").each( function(){
      this.checked=false;
    } );
  }
   if( which == "d" ){
    jQuery("input[name=check_d]:checked").each( function(){
      this.checked=false;
    } );
  }
   if( which == "m" ){
    jQuery("input[name=check_m]:checked").each( function(){
      this.checked=false;
    } );
  } 
  update_filter();
}

function populate_facets(){
  var facet_groups = ['g','s','c','d','m'];
  var facet_values = {
    g:[
      ['Pre','check_g_p'],
      ['K','check_g_k'],
      ['1st','check_g_1'],
      ['2nd','check_g_2'],
      ['3rd','check_g_3'],
      ['4th','check_g_4'],
      ['5th','check_g_5'],
      ['6th','check_g_6'],
      ['7th','check_g_7'],
      ['8th','check_g_8'],
      ['9th','check_g_9'],
      ['10th','check_g_10'],
      ['11th','check_g_11'],
      ['12th','check_g_12']
    ],
    s:[
      ['Bible','check_s_bible'],
      ['History','check_s_history_and_geography'],
      ['Language','check_s_language_arts'],
      ['Math','check_s_math'],
      ['Science','check_s_science'],
      ['Electives','check_s_electives'],
      ['Subject','check_s_subject_sets'],
      ['Other','check_s_other']
    ],
    c:[
      ['LIFEPAC','check_c_lifepac'],
      ['Monarch','check_c_monarch'],
      ['Switched-On','check_c_sos'],
      ['Horizons','check_c_horizons'],
      ['Weaver','check_c_weaver']
    ],
    d:[
      ['childrens-111','check_d_childrens'],
      ['documentaries-113','check_d_documentaries'],
      ['drama-112','check_d_drama'],
      ['educational-teaching-114','check_d_educational'],
      ['inspirational-115','check_d_inspirational']
    ],
    m:[
      ['dvd','check_m_dvd'],
      ['cd','check_m_cd']
    ]
  };

  var facet_checks = document.location.pathname;

  jQuery.each( facet_groups, function( gix, gval ){

    jQuery.each( facet_values[ gval ], function( vix, vval ){
      var facet_path = vval[0];
      var facet_id = vval[1];
      var facet_str = "/" + gval + "/" + facet_path;
      var facet_regex = new RegExp( "/" + gval + "/" + facet_path, "ig");

      if( facet_checks.search( facet_regex ) != -1 ){
        facet_checks=facet_checks.replace( facet_regex, '');
        jQuery( '#'+ facet_id ).attr("checked","checked");
        jQuery( '#'+ facet_id ).parent().addClass("checked");

        // The "+1" is to accomidate option 0 (nothing) on these dropdowns 
        jQuery( "#check_" + gval + "s option:eq(" + ( vix + 1 ) + ")" ).attr("selected", "selected");
      }
    });
  });

	//Product Listing Responsive
	jQuery( ".facets_mobile" ).prependTo( ".all-products" );
};

(function ($) {
  Drupal.behaviors.custom_product_facets = {
    attach: function (context, settings) {
      jQuery( "[name='check_g']" ).change( update_filter );
      jQuery( "[name='check_s']" ).change( update_filter );
      jQuery( "[name='check_c']" ).change( update_filter );
      jQuery( "[name='check_d']" ).change( update_filter );
      jQuery( "[name='check_m']" ).change( update_filter );

      populate_facets()
    }
  };
})(jQuery);

