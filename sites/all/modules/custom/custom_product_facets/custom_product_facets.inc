<?php

/*

CONTENT FOR CUSTOM FACET AND PRODUCT LISTING BLOCKS

*/


/**
 *  The Facet chooser block content DVD CATEGORIES
 */

function facets_category_block_code(){
  $facet_block_code =<<<EOT
  <div class="facets_desktop" >

    <h2 class="block-title title-underline">
      <span>Filter by Category</span>
      <a href="javascript:;" onclick="reset_filter('d');" class="reset_listing_filter">Reset</a>
    </h2>

    <div class="content clearfix">
      <div class="item-list">
        <ul class="clearfix check_d facet_check">
          <li>
              <input type="checkbox" id="check_d_childrens" name="check_d" class="check_d" value="/d/childrens-111" >
              <label for="check_d_childrens"><div class="checkbox-proxy"></div> Children's</label>
          </li>
          <li>
              <input type="checkbox" id="check_d_documentaries" name="check_d" class="check_d" value="/d/documentaries-113" >
              <label for="check_d_documentaries"><div class="checkbox-proxy"></div> Documentaries</label>
          </li>
          <li>
              <input type="checkbox" id="check_d_drama" name="check_d" class="check_d" value="/d/drama-112" >
              <label for="check_d_drama"><div class="checkbox-proxy"></div> Drama</label>
          </li>
          <li>
            <input type="checkbox" id="check_d_educational" name="check_d" class="check_d" value="/d/educational-teaching-114" >
            <label for="check_d_educational"><div class="checkbox-proxy"></div> Educational - Teaching</label>
          </li>
          <li>
              <input type="checkbox" id="check_d_inspirational" name="check_d" class="check_d" value="/d/inspirational-115" >
              <label for="check_d_inspirational"><div class="checkbox-proxy"></div> Inspirational</label>
          </li>

        </ul>
      </div>
    </div>

    <h2 class="block-title title-underline">
      <span>Filter by Media</span>
      <a href="javascript:;" onclick="reset_filter('m');" class="reset_listing_filter">Reset</a>
    </h2>

    <div class="content clearfix">
      <div class="item-list">
        <ul class="clearfix check_m facet_check">
          <li>
              <input type="checkbox" id="check_m_cd" name="check_m" class="check_m" value="/m/cd" >
              <label for="check_m_cd"><div class="checkbox-proxy"></div> CD</label>
          </li>
          <li>
              <input type="checkbox" id="check_m_dvd" name="check_m" class="check_m" value="/m/dvd" >
              <label for="check_m_dvd"><div class="checkbox-proxy"></div> DVD</label>
          </li>

        </ul>
      </div>
    </div>

  </div>

  <div class="facets_mobile">
    <h2 class="block-title">
      <span>Filter by Category</span>
      <a href="javascript:;" onclick="reset_filter('d');" class="reset_listing_filter">Reset</a>
    </h2>

    <div class="facet_select_div facet_select_div_subject">
      <select class="facet_select facet_select_category" name="check_d" id="check_ds" >
        <option value="" >--Choose Category--</option>
        <option id="check_d_childrens_o" value="/d/Childrens" >Children's</option>
        <option id="check_d_documentaries_o" value="/d/Documentaries" >Documentaries</option>
        <option id="check_d_drama_o" value="/d/Drama" >Drama</option>
        <option id="check_d_educational_o" value="/d/Educational%20-%20Teaching" >Educational - Teaching</option>
        <option id="check_d_inspirational_o" value="/d/Inspirational" >Inspirational</option>
      </select>
    </div>

    <h2 class="block-title">
      <span>Filter by Media</span>
      <a href="javascript:;" onclick="reset_filter('m');" class="reset_listing_filter">Reset</a>
    </h2>

    <div class="facet_select_div facet_select_div_media">
      <select class="facet_select facet_select_media" name="check_m" id="check_ms" >
        <option value="" >--Choose Media--</option>
        <option id="check_d_childrens_o" value="/m/cd" >CD</option>
        <option id="check_d_documentaries_o" value="/m/dvd" >DVD</option>
      </select>
    </div>
  </div>

EOT;

  return $facet_block_code;
}

/**
 *  The Facet chooser block content SUBJECT ONLY
 */

function facets_subject_block_code(){
  $facet_block_code =<<<EOT
  <div class="facets_desktop" >
    <h2 class="block-title title-underline">
      <span>Filter by Subject</span>
      <a href="javascript:;" onclick="reset_filter('s');" class="reset_listing_filter">Reset</a>
    </h2>

    <div class="content clearfix">
      <div class="item-list">
        <ul class="clearfix check_s facet_check">
          <li>
              <input type="checkbox" id="check_s_bible" name="check_s" class="check_s" value="/s/Bible" >
              <label for="check_s_bible"><div class="checkbox-proxy"></div> Bible</label>
          </li>
          <li>
              <input type="checkbox" id="check_s_history_and_geography" name="check_s" class="check_s" value="/s/History%20and%20Geography" >
              <label for="check_s_history_and_geography"><div class="checkbox-proxy"></div> History & Geography</label>
          </li>
          <li>
              <input type="checkbox" id="check_s_language_arts" name="check_s" class="check_s" value="/s/Language%20Arts" >
              <label for="check_s_language_arts"><div class="checkbox-proxy"></div> Language Arts</label>
          </li>
          <li>
            <input type="checkbox" id="check_s_math" name="check_s" class="check_s" value="/s/Math" >
            <label for="check_s_math"><div class="checkbox-proxy"></div> Math</label>
          </li>
          <li>
              <input type="checkbox" id="check_s_science" name="check_s" class="check_s" value="/s/Science" >
              <label for="check_s_science"><div class="checkbox-proxy"></div> Science</label>
          </li>
          <li>
              <input type="checkbox" id="check_s_electives" name="check_s" class="check_s" value="/s/Electives" >
              <label for="check_s_electives"><div class="checkbox-proxy"></div> Electives</label>
          </li>
          <li>
              <input type="checkbox" id="check_s_subject_sets" name="check_s" class="check_s" value="/s/Subject%20Sets" >
              <label for="check_s_subject_sets"><div class="checkbox-proxy"></div> Subject Sets</label>
          </li>
          <li>
            <input type="checkbox" id="check_s_other" name="check_s" class="check_s" value="/s/Other" >
            <label for="check_s_other"><div class="checkbox-proxy"></div> Other</label>
          </li>
        </ul>
      </div>
    </div>
  </div>

  <div class="facets_mobile">
    <h2 class="block-title">
      <span>Filter by Subject</span>
      <a href="javascript:;" onclick="reset_filter('s');" class="reset_listing_filter">Reset</a>
    </h2>

    <div class="facet_select_div facet_select_div_subject">
      <select class="facet_select facet_select_subject" name="check_s" id="check_ss" >
        <option value="" >--Choose Subject--</option>
        <option id="check_s_bible_o" value="/s/Bible" >Bible</option>
        <option id="check_s_history_and_geography_o" value="/s/History%20and%20Geography" >History and Geography</option>
        <option id="check_s_language_arts_o" value="/s/Language%20Arts" >Language Arts</option>
        <option id="check_s_math_o" value="/s/Math" >Math</option>
        <option id="check_s_science_o" value="/s/Science" >Science</option>
        <option id="check_s_electives_o" value="/s/Electives" >Electives</option>
        <option id="check_s_subject_sets_o" value="/s/Subject%20Sets" >Subject Sets</option>
        <option id="check_s_other_o" value="/s/Other" >Other</option>
      </select>
    </div>
  </div>

EOT;

  return $facet_block_code;
}

function facets_all_block_code(){
  $facet_block_code =<<<EOT
  <div class="facets_desktop" >
    <h2 class="block-title">
      <span>Filter by Grade</span>
      <a href="javascript:;" onclick="reset_filter('g');" class="reset_listing_filter">Reset</a>
    </h2>

    <div class="content clearfix">
      <div class="item-list">
        <ul class="clearfix check_g facet_check">
          <li class="leaf">
            <input type="checkbox" name="check_g" class="check_g" id="check_g_p" value="/g/Pre">
            <label for="check_g_p">Pre</label>
          </li>
          <li class="leaf">
            <input type="checkbox" name="check_g" class="check_g" id="check_g_k" value="/g/K">
            <label for="check_g_k">K</label>
          </li>
          <li class="leaf">
            <input type="checkbox" name="check_g" class="check_g" id="check_g_1" value="/g/1st%20Grade">
            <label for="check_g_1">1</label>
          </li>
          <li class="leaf">
            <input type="checkbox" name="check_g" class="check_g" id="check_g_2" value="/g/2nd%20Grade">
            <label for="check_g_2">2</label>
          </li>
          <li class="leaf leaf-last">
            <input type="checkbox" name="check_g" class="check_g" id="check_g_3" value="/g/3rd%20Grade">
            <label for="check_g_3">3</label>
          </li>
          <li class="leaf">
            <input type="checkbox" name="check_g" class="check_g" id="check_g_4" value="/g/4th%20Grade">
            <label for="check_g_4">4</label>
          </li>
          <li class="leaf">
            <input type="checkbox" name="check_g" class="check_g" id="check_g_5" value="/g/5th%20Grade">
            <label for="check_g_5">5</label>
          </li>
          <li class="leaf">
            <input type="checkbox" name="check_g" class="check_g" id="check_g_6" value="/g/6th%20Grade">
            <label for="check_g_6">6</label>
          </li>
          <li class="leaf">
            <input type="checkbox" name="check_g" class="check_g" id="check_g_7" value="/g/7th%20Grade">
            <label for="check_g_7">7</label>
          </li>
          <li class="leaf leaf-last">
            <input type="checkbox" name="check_g" class="check_g" id="check_g_8" value="/g/8th%20Grade">
            <label for="check_g_8">8</label>
          </li>
          <li class="leaf">
            <input type="checkbox" name="check_g" class="check_g" id="check_g_9" value="/g/9th%20Grade">
            <label for="check_g_9">9</label>
          </li>
          <li class="leaf">
            <input type="checkbox" name="check_g" class="check_g" id="check_g_10" value="/g/10th%20Grade">
            <label for="check_g_10">10</label>
          </li>
          <li class="leaf">
            <input type="checkbox" name="check_g" class="check_g" id="check_g_11" value="/g/11th%20Grade">
            <label for="check_g_11">11</label>
          </li>
          <li class="leaf">
            <input type="checkbox" name="check_g" class="check_g" id="check_g_12" value="/g/12th%20Grade">
            <label for="check_g_12">12</label>
          </li>
        </ul>
      </div>
    </div>

    <h2 class="block-title title-underline">
      <span>Filter by Subject</span>
      <a href="javascript:;" onclick="reset_filter('s');" class="reset_listing_filter">Reset</a>
    </h2>

    <div class="content clearfix">
      <div class="item-list">
        <ul class="clearfix check_s facet_check">
          <li>
              <input type="checkbox" id="check_s_bible" name="check_s" class="check_s" value="/s/Bible" >
              <label for="check_s_bible"><div class="checkbox-proxy"></div> Bible</label>
          </li>
          <li>
              <input type="checkbox" id="check_s_history_and_geography" name="check_s" class="check_s" value="/s/History%20and%20Geography" >
              <label for="check_s_history_and_geography"><div class="checkbox-proxy"></div> History & Geography</label>
          </li>
          <li>
              <input type="checkbox" id="check_s_language_arts" name="check_s" class="check_s" value="/s/Language%20Arts" >
              <label for="check_s_language_arts"><div class="checkbox-proxy"></div> Language Arts</label>
          </li>
          <li>
            <input type="checkbox" id="check_s_math" name="check_s" class="check_s" value="/s/Math" >
            <label for="check_s_math"><div class="checkbox-proxy"></div> Math</label>
          </li>
          <li>
              <input type="checkbox" id="check_s_science" name="check_s" class="check_s" value="/s/Science" >
              <label for="check_s_science"><div class="checkbox-proxy"></div> Science</label>
          </li>
          <li>
              <input type="checkbox" id="check_s_electives" name="check_s" class="check_s" value="/s/Electives" >
              <label for="check_s_electives"><div class="checkbox-proxy"></div> Electives</label>
          </li>
          <li>
              <input type="checkbox" id="check_s_subject_sets" name="check_s" class="check_s" value="/s/Subject%20Sets" >
              <label for="check_s_subject_sets"><div class="checkbox-proxy"></div>Subject Sets</label>
          </li>
          <li>
            <input type="checkbox" id="check_s_other" name="check_s" class="check_s" value="/s/Other" >
            <label for="check_s_other"><div class="checkbox-proxy"></div> Other</label>
          </li>
        </ul>
      </div>
    </div>

    <h2 class="block-title title-underline">
      Filter by Curriculum
      <a href="javascript:;" onclick="reset_filter('c');" class="reset_listing_filter">Reset</a>
    </h2>

    <div class="content clearfix">
      <div class="item-list">
        <ul class="clearfix check_c facet_check">
          <li class="leaf first">
            <input type="checkbox" id="check_c_monarch" name="check_c" class="check_c" value="/c/Monarch" >
            <label for="check_c_monarch"><div class="checkbox-proxy"></div> Monarch</label>
          </li>
          
          <li class="leaf first">
            <input type="checkbox" id="check_c_sos" name="check_c" class="check_c" value="/c/Switched-On%20Schoolhouse" class="facet_check" >
            <label for="check_c_sos"><div class="checkbox-proxy"></div> Switched-On Schoolhouse</label>
          </li>
          
          <li class="leaf first">
            <input type="checkbox" id="check_c_lifepac" name="check_c" class="check_c" value="/c/LIFEPAC" >
            <label for="check_c_lifepac"><div class="checkbox-proxy"></div> LIFEPAC</label>
          </li>

          <li class="leaf first">
            <input type="checkbox" id="check_c_horizons" name="check_c" class="check_c" value="/c/Horizons" class="facet_check" >
            <label for="check_c_horizons"><div class="checkbox-proxy"></div> Horizons</label>
          </li>
          
          <li class="leaf first">
            <input type="checkbox" id="check_c_weaver" name="check_c" class="check_c" value="/c/Weaver" class="facet_check" >
            <label for="check_c_weaver"><div class="checkbox-proxy"></div> Weaver</label>
          </li>
        </ul>
      </div>
    </div>
  </div>

  <div class="facets_mobile">
    <h2 class="block-title">
      <span>Filter by Grade</span>
      <a href="javascript:;" onclick="reset_filter('g');" class="reset_listing_filter">Reset</a>
    </h2>

    <div class="facet_select_div facet_select_div_grade">
      <select class="facet_select facet_select_grade" name="check_g" id="check_gs" >
        <option id="check_g_p_o" value="">--Choose a Grade--</option>
        <option id="check_g_p_o" value="/g/Pre">Pre-School</option>
        <option id="check_g_k_o" value="/g/K">Kindergarten</option>
        <option id="check_g_1_o" value="/g/1st%20Grade">1st Grade</option>
        <option id="check_g_2_o" value="/g/2nd%20Grade">2nd Grade</option>
        <option id="check_g_3_o" value="/g/3rd%20Grade">3rd Grade</option>
        <option id="check_g_4_o" value="/g/4th%20Grade">4th Grade</option>
        <option id="check_g_5_o" value="/g/5th%20Grade">5th Grade</option>
        <option id="check_g_6_o" value="/g/6th%20Grade">6th Grade</option>
        <option id="check_g_7_o" value="/g/7th%20Grade">7th Grade</option>
        <option id="check_g_8_o" value="/g/8th%20Grade">8th Grade</option>
        <option id="check_g_9_o" value="/g/9th%20Grade">9th Grade</option>
        <option id="check_g_10_o" value="/g/10th%20Grade">10th Grade</option>
        <option id="check_g_11_o" value="/g/11th%20Grade">11th Grade</option>
        <option id="check_g_12_o" value="/g/12th%20Grade">12th Grade</option>
      </select>
    </div>

    <h2 class="block-title">
      <span>Filter by Subject</span>
      <a href="javascript:;" onclick="reset_filter('s');" class="reset_listing_filter">Reset</a>
    </h2>

    <div class="facet_select_div facet_select_div_subject">
      <select class="facet_select facet_select_subject" name="check_s" id="check_ss" >
        <option value="" >--Choose Subject--</option>
        <option id="check_s_bible_o" value="/s/Bible" >Bible</option>
        <option id="check_s_history_and_geography_o" value="/s/History%20and%20Geography" >History and Geography</option>
        <option id="check_s_language_arts_o" value="/s/Language%20Arts" >Language Arts</option>
        <option id="check_s_math_o" value="/s/Math" >Math</option>
        <option id="check_s_science_o" value="/s/Science" >Science</option>
        <option id="check_s_electives_o" value="/s/Electives" >Electives</option>
        <option id="check_s_subject_sets_o" value="/s/Subject%20Sets" >Subject Sets</option>
        <option id="check_s_other_o" value="/s/Other" >Other</option>
      </select>
    </div>


    <h2 class="block-title">
      Filter by Curriculum
      <a href="javascript:;" onclick="reset_filter('c');" class="reset_listing_filter">Reset</a>
    </h2>

    <div class="facet_select_div facet_select_div_curriculum">
      <select class="facet_select facet_select_curriculum"  name="check_c" id="check_cs" >
        <option value="" >--Choose Curriculum--</option>
        <option id="check_c_monarch_o" value="/c/Monarch" >Monarch</option>
        <option id="check_c_sos_o" value="/c/Switched-On%20Schoolhouse" >Switched-On Schoolhouse</option>
        <option id="check_c_lifepac_o" value="/c/LIFEPAC" >LIFEPAC</option>
        <option id="check_c_horizons_o" value="/c/Horizons" >Horizons</option>
        <option id="check_c_weaver_o" value="/c/Weaver" >Weaver</option>
      </select>
    </div>
  </div>

EOT;

  return $facet_block_code;
}

function facets_results_header_code(){
  $header_view_name = 'promo_touts';
  $header_display = 'product_list_headers';
  unset($header_banner);
  $facet_args = arg();

  $header_banner = '';

  if( $facet_args[0] == 'products' ||  $facet_args[0] == 'resources' ){
    array_shift( $facet_args );

    //CHECK FOR CURRICULUM
    $count_curriculum = 0;
    foreach( $facet_args as $aix ){
      if( $aix == 'c' ){
        $count_curriculum++;
      }
    }

    if( $count_curriculum == 1 ){
      for( $aix=0; $aix<count( $facet_args ); $aix+=2 ){
        if( $facet_args[$aix] == 'c' ) {
          $header_banner = $facet_args[ $aix+1 ] . " Header";
        }
      }
    } else {

      //NO MATCHING CURRICULUM, CHECK FOR OTHER SUBJECT HEADERS
      $count_subject = 0;
      foreach( $facet_args as $aix ){
        if( $aix == 's' ){
          $count_subject++;
        }
      }

      if( $count_subject == 1 ){
        for( $aix=0; $aix<=count( $facet_args ); $aix+=2 ){
          if( $facet_args[$aix] == 's' ) {
            $header_banner = $facet_args[ $aix+1 ] . " Header";
          }
        }
      }
    }
  }

  $view = views_get_view( $header_view_name );
  $view->set_display( $header_display );
  $view->set_arguments( array( $header_banner ) );
  $view->pre_execute();
  $view->execute();
  $header_count = count( $view->result );

  if( $header_count == 0 ){ $header_banner = 'General Header'; }

  $return_code = views_embed_view( $header_view_name, $header_display, $header_banner );

  return $return_code;
}

function facets_results_targeted_ads_code(){
  $header_view_name = 'promo_touts';
  $header_display = 'marketing_product_listing_tout';
  unset($ad_banner);
  $facet_args = arg();

  $ad_banner = '';

  if( $facet_args[0] == 'products' ||  $facet_args[0] == 'resources' ){
    array_shift( $facet_args );

    //CHECK FOR CURRICULUM
    $count_curriculum = 0;
    foreach( $facet_args as $aix ){
      if( $aix == 'c' ){
        $count_curriculum++;
      }
    }

    if( $count_curriculum == 1 ){
      for( $aix=0; $aix<count( $facet_args ); $aix+=2 ){
        if( $facet_args[$aix] == 'c' ) {
          $ad_banner = $facet_args[ $aix+1 ] . " Ad";
        }
      }
    } else if( $count_curriculum > 1 ){
      for( $aix=0; $aix<count( $facet_args ); $aix+=2 ){
        if( $facet_args[$aix] == 'c' && strtolower( $facet_args[ $aix+1 ] ) == 'monarch' ) {
          $ad_banner = $facet_args[ $aix+1 ] . " Ad";
        }
      }
    }
  }

  $view = views_get_view( $header_view_name );
  $view->set_display( $header_display );
  $view->set_arguments( array( $ad_banner ) );
  $view->pre_execute();
  $view->execute();
  $header_count = count( $view->result );

  if( $count_curriculum == 0 ){ $ad_banner = 'Monarch Ad'; }

  $return_code = views_embed_view( $header_view_name, $header_display, $ad_banner );

  return $return_code;
}

function facets_results_curated_code(){
  $curated_view_name = 'curated_product_list';
  $curated_display = 'curated_list';
  unset($curated_curriculum);
  $facet_args = arg();

  if( $facet_args[0] == 'products' ){
    array_shift( $facet_args );

    $count_curriculum = 0;
    foreach( $facet_args as $aix ){
      if( $aix == 'c' ){
        $count_curriculum++;
      }
    }

    if( $count_curriculum == 1 ){
      for( $aix=0; $aix<=count( $facet_args ); $aix+=2 ){
        if( $facet_args[$aix] == 'c' ) {
          $curated_curriculum = $facet_args[ $aix+1 ];
        }
      }
    }
  }

  $return_code = views_embed_view( $curated_view_name, $curated_display, $curated_curriculum );

  return $return_code;
}