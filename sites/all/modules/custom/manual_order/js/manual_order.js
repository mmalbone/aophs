(function($){
  //Executes on create customer form submit.
  Drupal.behaviors.manualOrder = {
    attach: function (context) {
      //Manual Order js goes here.
      ////JQUERY CUSTOM FUNCTIONS

      //Delay and show new product in cart.
      $('.line-item-new').fadeIn(400, function(){
        $(this).removeClass('line-item-new');
      });

      //Shows the create customer block.
      function showCreateCustomer(){
        //Set all fields to empty.
        $('#create-customer-block input[type=text], textarea, select').val("").removeClass('error').removeClass('valid');
        //Remove description
        $('#create-customer-block .description').hide();
        $('#create-customer-block').fadeIn(200);
        $('#search-customer-block').hide();
        //Populate customer fields
        populateCustomerFields();
        //Focus on the appropriate input.
        $('#create-customer-block input[type=text], textarea').each(function() {
          if ( this.value === '' ) {
            this.focus();
            return false;
          }
        });
      }

      //Splits a string into first name last name
      function splitName(theName){
        var items = theName.split(' '),
          lastName = items[items.length-1],
          firstName = "";

        for (var i = 0; i < items.length - 1; i++) {
          if (i > 0) {
            firstName += ' ';
          }
          firstName += items[i];
        }
        return [capitalizeString(firstName), capitalizeString(lastName)];
      }

      //Capitalizes the first letter in a string
      function capitalizeString(txt){
        return txt.substring(0, 1).toUpperCase() + txt.substring(1);
      }

      //parses an input and prefills create customer fields
      function populateCustomerFields(){
        inputVal = $('input[name=search_customer]').val().replace(/\}/g, '').replace(/\{/g, '');
        var inputSet = 0;
        if(inputVal) {
          //The user has entered text.
          //Check if input is an email address.
          var emailReg = /@/;
          if(emailReg.test(inputVal)) {
            //The input is an email.
            $('input[name=email]').val(inputVal);
            inputSet = 1;
          }

          //Check if input is a phone number.
          var phoneReg = /^[ ()+]*([0-9][ ()+]*){10}$/;
          if(phoneReg.test(inputVal)) {
            //The input is an email.
            $('input[name=phone]').val(inputVal);
            inputSet = 1;
          }
          //If the input hasn't been set, return 
          if(inputSet == 0) {
            //The input is a name
            nameArr = splitName(inputVal);
            if(nameArr[0]) { $('input[name=fname]').val(nameArr[0]); }
            if(nameArr[1]) { $('input[name=lname]').val(nameArr[1]); }

		     		// if(nameArr[0] == "") {
		     		// 	$('#edit-fname').val(nameArr[1]);
		     		// 	$('#edit-lname').val("");
		     		// }
          }
        }
      }

////////////////////////INITIALIZE/////////////////////////////
      //showSearchCustomer();
      //Autocomplete js hack
      /**
       * Positions the suggestions popup and starts a search.
       */
      Drupal.jsAC.prototype.populatePopup = function () {
        var $input = $(this.input);
        var position = $input.position();
        // Show popup.
        if (this.popup) {
          $(this.popup).remove();
        }
        this.selected = false;
        this.popup = $('<div id="autocomplete"></div><div id="create-customer-button" class="create-customer">Create New Customer</div>')[0];
        this.popup.owner = this;
        $(this.popup).css({
          top: parseInt(position.top + this.input.offsetHeight, 10) + 'px',
          left: parseInt(position.left, 10) + 'px',
          width: $input.innerWidth() + 'px',
          display: 'none'
        });
        $input.after(this.popup);

        // Do search.
        this.db.owner = this;
        this.db.search(this.input.value);
      };

      //Submit form on enter
      $(".search-form input").keypress(function(event) {
        if (event.which == 13) {
          event.preventDefault();
          $(this).blur();
        }
      });

      //Hide edit customer.
      $(".cancel-edit-customer").click(function(){
        $("#edit-customer-block").hide();
        $("#customer-profile-block").fadeIn(200);
      });


      //create customer js
      $('#show-create-customer').click(function(){
        showCreateCustomer();
      });

      //Show Customer Edit Profile
      $('.edit-customer-profile').click(function(){
        $('#customer-profile-block').hide();
        $('#edit-customer-block').fadeIn(300);
      });

      //Close customer profile.
      $('.close-profile, #show-search-customer, #create-customer-cancel').click(function(){
        $('#customer-profile-block').hide();
        $('input[name=search_customer]').val("");
        $('#search-customer-block').fadeIn(200);
        $('#create-customer-block').hide();
        $('#billing-shipping-address-block').hide();
        $('#customer-profile-block .pane-content').html("<div id='customer-profile-block-wrapper' class='customer-profile-wrapper ajax-loading'><img src='https://glnenews.s3.amazonaws.com/aophs/large-loader.GIF'/></div>");
        $('input[name=search_customer]').focus();
        //Ajax call to remove product from order.
        $.post( "/unassign_customer_callback", function( data ) {
            //The user is removed from the order.
            $('.receipt-profile').html("").hide();
        });
      });

      //Form info focus
      $(".forminfo").focus(function(){
        //Remove form info class
        $(this).removeClass("forminfo");
      });

      //Show and hide the shipping form
      $("input[name='billing_same[same_billing]']").change(function(){
        if(!this.checked) {
          $('#shipping-address-wrapper').fadeIn(300);
          $('#shipping-address-wrapper input:text').each(function(){
            //$(this).val("");
          });
          $('#shipping-address-wrapper input:text').first().focus();
        } else {
          $('#shipping-address-wrapper').fadeOut(100);
        }
      });

      //On change, trigger add product.
      $('input[name=search_product]').blur(function(){
        str = $(this).val();
        //show input success
        if (/{{/i.test(str)) {
          //Assign the customer to order.
          $('#search-promo-block').fadeIn(300);
          $(this).css({ "color" : "white" }).addClass('valid');
          //Show next step.
          $('.manual-order-next-step').fadeIn(300);
        }
      });

      //On click, remove product
      $('.remove-item').click(function(){
        //The product to remove from the cart.
        lineItem = $(this).attr("data-line-item");

        //Because this product uses a callback, add a class of "loading" to it.
        $(".order_receipt").addClass('ajax-loading');

        //Fade out line item.
        $(this).parent().fadeOut(300, function(){
          //Remove product from order.
          $.post( "/unassign_product_callback", { line_item_id : lineItem }, function( data ) {
            //The line item is removed from the order.
            //Update the receipt total div.
            if(!data.items) {
              unassign_promo_code(false);
              $('#search-promo-block').fadeOut(300);
              $('.manual-order-next-step').fadeOut(300);
            }
            $(".receipt_total").html(data.content);
            $(".order_receipt").removeClass('ajax-loading');
          }, "json");
        });
      });

      //On change, trigger new customer
      $('input[name=search_customer]').blur(function(){
        str = $(this).val();

        //Test if the entry is a new customer
        if(/{{{/i.test(str)) {
          showCreateCustomer();
        } else {
          //The input is a current customer
          if (/{{/i.test(str)) {
            //Assign the customer to order.
            $('#search-customer-block').hide();
            $('#autocomplete').hide();
            $('#customer-profile-block').show();
          }
        }
      });

      //On click, remove sales rep initials
      $('#sales-rep-initials-display .button-close-initials').click(function(){
        $('#sales-rep-initials-display').hide();
        $('#sales-rep-initials-input').fadeIn(300);
        $('input[name=sales_rep_initials]').val("").focus();
      });

      //Submit Sales Rep Initials on enter.
      $('input[name=sales_rep_initials]').keypress(function (e) {
        if (event.which == 13) {
          event.preventDefault();
          $(this).blur();
        }
      });


      //Proceed to payment event.
      $( "body" ).bind( "proceedToPayment", function() {
        //Scroll to the top of the page.
        $("html, body").animate({ scrollTop: 0 }, "slow");
        //Hide the close buttons.
        $('.line-item-wrapper .button-close').fadeOut(100);
        //Show the small customer profile.
        $('.order_receipt .customer-profile').fadeIn(300);
        //Hide remove product button.
        $('.product_list .button-close').hide();
        return false;
      });

        //highlight and choose shipping options
        $('.form-item-shipping-service').click(function(){
            $('.form-item-shipping-service').removeClass("active");
            //Set shipping option to "Checked"
            $(this).find("[type=radio]").attr('checked', 'checked');
            //Get the value of the shipping option.
            optionVal = $(this).find("[type=radio]").val();
            $(this).addClass("active");
            //Update order info.
            $('.loading-block').fadeIn(300);
            update_manual_order_shipping(optionVal)
        });


        function update_manual_order_shipping(optionVal){
        //Update the order total.
        $(".order_receipt").addClass('ajax-loading');
          $.post( "/shipping_option_select_callback", { option : optionVal }, function(data) {
                $(".receipt_total").html(data.content);
                $(".order_receipt").removeClass('ajax-loading');
                $('.loading-block').fadeOut(300);
          }, "json");
        }

      //Updates the order total at any time during the order process.

      function update_manual_order_total(){

      //Update the order total.
      $(".order_receipt").addClass('ajax-loading');
      $.post( "/order_total_update_callback", function(data) {
            $(".receipt_total").html(data.content);
            $(".order_receipt").removeClass('ajax-loading');
      }, "json");

    }


      $('form input[type=radio]:checked').parent(".form-item-shipping-service").addClass("active");

      //Hide promo form on page load
      function unassign_promo_code(updateTotal){
        $('#discount-item-box').hide().remove();
        $('input[name=search_promo]').val("").focus();
        $('#promo-search-form').fadeIn(200);
        $(".order_receipt").addClass('ajax-loading');
        $.post( "/unassign_promo_callback", function( data ) {
          //The promo is removed from the order.
          if(updateTotal) $(".receipt_total").html(data);
          $(".order_receipt").removeClass('ajax-loading');
        });
      }

      //Close promo code.
      $('#button-close-promo').click(function(){
        unassign_promo_code(true);
      });

      //Submit promo code on enter.
      $('input[name=search_promo]').keypress(function (e) {
        if (e.which == 13){
          $('#promo-search-form input[name=op]').mousedown();
          return false;
        }
      });






      $('input[name=activation_input]').keypress(function(e){
        if(e.which == 13){
          $.post( "/output_order_callback",function(data){
            $("#activation-wrapper").append(data);
          });
          return false;
        }
      });

    }
  };
})(jQuery);