<?php

/**
 * Define the form to be returned.
 *
 * Note that drupal passes in two parameters,
 * $form - which holds all of the elements of the form
 * $form_state - a special array of settings & values needed by Drupal
 */
function activation_form($form, &$form_state){
        $form['#prefix'] = "<div id='activation-wrapper'>";
        $form['#suffix'] = "</div>";

        //Apply the activation code
        $form['activation_input'] = array(
            '#type' => 'textfield',
            '#prefix' => "<div id='activation-form'>",
            '#attributes' => array(
                'placeholder' => t('Enter the existing activation code.')
            ),
            '#ajax' => array(
                'callback' => 'activation_callback',
                'wrapper' => 'activation-form',
                'progress' => array('type' => 'throbber', 'message' => ''),
            ),
        );

        return $form;
}

function activation_callback($form, &$form_state){

    unset($form['#prefix']);
    unset($form['#suffix']);

    $form_value = $form['activation_input']['#value'];
    $form_value = isset($form_value)? $form_value: '';

    if($order = manual_order_get_order()){
        // Get the TID for the curriculum taxonomy term
        $monarchTID = manual_order_get_tid('Monarch', 'curriculum');

        $monarch_products = array();

        // Loop through all the line items in the order
        foreach($order->commerce_line_items['und'] as $order_line_item){
            $order_item = entity_load_single('commerce_line_item', $order_line_item['line_item_id']);
            if($order_item->type === 'product'){
                $order_item_product = entity_load_single('commerce_product', $order_item->commerce_product['und'][0]['product_id']);

                // if its a monarch product then we need to process a subscription
                if ((sizeof($order_item_product->field_curriculum) > 0) && $order_item_product->field_curriculum['und'][0]['tid'] == $monarchTID){
                    $monarch_products[] = $order_item_product;
                }
            }
        }

        if(count($monarch_products) == 1){
            print_r($monarchTID);
        }

        // Set monarch activation code on the order
        //manual_order_add_activation($order, 'monsku', $form_value);
    }
    $commands[] = ajax_command_html('#activation-wrapper', drupal_render($form));
    return array('#type' => 'ajax', '#commands' => $commands);
}