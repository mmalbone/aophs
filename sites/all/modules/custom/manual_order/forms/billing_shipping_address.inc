<?php

/**
 * Define the form to be returned.
 *
 * Note that drupal passes in two parameters,
 * $form - which holds all of the elements of the form
 * $form_state - a special array of settings & values needed by Drupal
 */
function billing_shipping_address_form($form, &$form_state) {
        // Now the fun begins.
        // You can add as much or as little to this form as
        // needed.  See the forms api for all possible elements.

        $form['#prefix'] = '<div id="billing-shipping-address-wrapper">';
        $form['#suffix'] = '</div>';

        if(isset($_SESSION['customerId']) && $_SESSION['customerId'] != 0) {

            //Load the current order's customer.
            $customer = user_load($_SESSION['customerId']);

            //////////////////BILLING FORM///////////////////////

            $markup = "<h3>Billing Address</h3><p>Enter the billing address associated with the customer's credit card.</p>";

            //Begin Billing Shipping Form Array
            $form['billing_description'] = array(
              '#markup' => $markup,
            );

            //Billing Messages

            $form['billing_messages'] = array(
              '#prefix' => "<div id='billing-address-message' class='manual-order-messages'>",
              '#suffix' => "</div>",
            );


                            //First Name
            $form['billing_fname'] = array(
                    '#type' => 'textfield',
                    '#title' => t("First Name"),
                    '#required' => TRUE,
                    '#default_value' => $customer->field_firstname['und'][0]['value'],
                    '#prefix' => '<div class="input-group"><div class="input-item">',
                    '#suffix' => "</div>",

            );

            //Last Name
            $form['billing_lname'] = array(
                    '#type' => 'textfield',
                    '#title' => t("Last Name"),
                    '#required' => TRUE,
                    '#default_value' => $customer->field_lastname['und'][0]['value'],
                    '#prefix' => '<div class="input-item">',
                    '#suffix' => "</div></div>",
            );

            //Phone number
            //if !isset, display the phone number array input and make it required.
            if(!isset($customer->field_phone_number['und'][0]['value']) || strlen($customer->field_phone_number['und'][0]['value']) < 6) {

                $form['phone_number']['#type'] = 'textfield';
                $form['phone_number']['#title'] = t('Phone Number');
                $form['phone_number']['#required'] = TRUE;
            }

            //Create Address
            $form['billing_address'] = array(
                    '#type' => 'textfield',
                    '#required' => TRUE,
                    '#default_value' => $customer->field_billing_address['und'][0]['value'],
                    '#title' => t("Street Address"),
            );

            //City
            $form['billing_city'] = array(
                    '#type' => 'textfield',
                    '#required' => TRUE,
                    '#default_value' => $customer->field_billing_city['und'][0]['value'],
                    '#title' => t("City"),
            );

            //Select State
            $form['billing_us_state'] = array(
                   '#type' => 'select',
                   '#required' => TRUE,
                   '#title' => t('State'),
                   '#options' => array(
                    '' => 'Choose',
                    'AL' => 'Alabama',
                    'AK' => 'Alaska',
                    'AZ' => 'Arizona',
                    'AR' => 'Arkansas',
                    'CA' => 'California',
                    'CO' => 'Colorado',
                    'CT' => 'Connecticut',
                    'DE' => 'Delaware',
                    'DC' => 'District Of Columbia',
                    'FL' => 'Florida',
                    'GA' => 'Georgia',
                    'HI' => 'Hawaii',
                    'ID' => 'Idaho',
                    'IL' => 'Illinois',
                    'IN' => 'Indiana',
                    'IA' => 'Iowa',
                    'KS' => 'Kansas',
                    'KY' => 'Kentucky',
                    'LA' => 'Louisiana',
                    'ME' => 'Maine',
                    'MD' => 'Maryland',
                    'MA' => 'Massachusetts',
                    'MI' => 'Michigan',
                    'MN' => 'Minnesota',
                    'MS' => 'Mississippi',
                    'MO' => 'Missouri',
                    'MT' => 'Montana',
                    'NE' => 'Nebraska',
                    'NV' => 'Nevada',
                    'NH' => 'New Hampshire',
                    'NJ' => 'New Jersey',
                    'NM' => 'New Mexico',
                    'NY' => 'New York',
                    'NC' => 'North Carolina',
                    'ND' => 'North Dakota',
                    'OH' => 'Ohio',
                    'OK' => 'Oklahoma',
                    'OR' => 'Oregon',
                    'PA' => 'Pennsylvania',
                    'RI' => 'Rhode Island',
                    'SC' => 'South Carolina',
                    'SD' => 'South Dakota',
                    'TN' => 'Tennessee',
                    'TX' => 'Texas',
                    'UT' => 'Utah',
                    'VT' => 'Vermont',
                    'VA' => 'Virginia',
                    'WA' => 'Washington',
                    'WV' => 'West Virginia',
                    'WI' => 'Wisconsin',
                    'WY' => 'Wyoming',
                   ),
                   '#default_value' => $customer->field_billing_state['und'][0]['value'],
                    '#prefix' => '<div class="input-group"><div class="input-item">',
                    '#suffix' => "</div>",
            );

            //Create ZIP
            $form['billing_zip'] = array(
                    '#type' => 'textfield',
                    '#required' => TRUE,
                    '#title' => t("ZIP/Postal Code"),
                    '#prefix' => '<div class="input-item">',
                    '#suffix' => "</div></div>",
                    '#default_value' => $customer->field_postal_code['und'][0]['value'],
            );

            $markup = "<h3 style='padding-top: 100px;'>Shipping Address</h3>";

            //Begin Shipping Form Array
            $form['shipping_description'] = array(
              '#markup' => $markup,
            );

            //Checkbox indicating that the shipping address is the same as the billing.
            $form['billing_same'] = array(
              '#type' => 'checkboxes',
              '#options' => array('same_billing' => t('Same as billing address.')),
              '#default_value' => array('same_billing'),
              '#suffix' => "<div id='shipping-address-wrapper' style='display: none;'>",
            );



            //////////////////SHIPPING FORM///////////////////////

            $form['shipping_messages'] = array(
              '#prefix' => "<div id='shipping-address-message' class='manual-order-messages'>",
              '#suffix' => "</div>",
            );

            //First Name
            $form['shipping_fname'] = array(
                    '#type' => 'textfield',
                    '#title' => t("First Name"),
                    '#prefix' => '<div class="input-group"><div class="input-item">',
                    '#suffix' => "</div>",

            );

            //Last Name
            $form['shipping_lname'] = array(
                    '#type' => 'textfield',
                    '#title' => t("Last Name"),
                    '#prefix' => '<div class="input-item">',
                    '#suffix' => "</div></div>",
            );

            //Create Address
            $form['shipping_address'] = array(
                    '#type' => 'textfield',
                    '#title' => t("Street Address"),
            );

            //City
            $form['shipping_city'] = array(
                    '#type' => 'textfield',
                    '#title' => t("City"),
            );

            //Select State
            $form['shipping_us_state'] = array(
                   '#type' => 'select',
                   '#title' => t('State'),
                   '#options' => array(
                    '' => 'Choose',
                    'AL' => 'Alabama',
                    'AK' => 'Alaska',
                    'AZ' => 'Arizona',
                    'AR' => 'Arkansas',
                    'CA' => 'California',
                    'CO' => 'Colorado',
                    'CT' => 'Connecticut',
                    'DE' => 'Delaware',
                    'DC' => 'District Of Columbia',
                    'FL' => 'Florida',
                    'GA' => 'Georgia',
                    'HI' => 'Hawaii',
                    'ID' => 'Idaho',
                    'IL' => 'Illinois',
                    'IN' => 'Indiana',
                    'IA' => 'Iowa',
                    'KS' => 'Kansas',
                    'KY' => 'Kentucky',
                    'LA' => 'Louisiana',
                    'ME' => 'Maine',
                    'MD' => 'Maryland',
                    'MA' => 'Massachusetts',
                    'MI' => 'Michigan',
                    'MN' => 'Minnesota',
                    'MS' => 'Mississippi',
                    'MO' => 'Missouri',
                    'MT' => 'Montana',
                    'NE' => 'Nebraska',
                    'NV' => 'Nevada',
                    'NH' => 'New Hampshire',
                    'NJ' => 'New Jersey',
                    'NM' => 'New Mexico',
                    'NY' => 'New York',
                    'NC' => 'North Carolina',
                    'ND' => 'North Dakota',
                    'OH' => 'Ohio',
                    'OK' => 'Oklahoma',
                    'OR' => 'Oregon',
                    'PA' => 'Pennsylvania',
                    'RI' => 'Rhode Island',
                    'SC' => 'South Carolina',
                    'SD' => 'South Dakota',
                    'TN' => 'Tennessee',
                    'TX' => 'Texas',
                    'UT' => 'Utah',
                    'VT' => 'Vermont',
                    'VA' => 'Virginia',
                    'WA' => 'Washington',
                    'WV' => 'West Virginia',
                    'WI' => 'Wisconsin',
                    'WY' => 'Wyoming',
                   ),
                   '#default_value' => "",
                    '#prefix' => '<div class="input-group"><div class="input-item">',
                    '#suffix' => "</div>",
            );

            //Create ZIP
            $form['shipping_zip'] = array(
                    '#type' => 'textfield',
                    '#title' => t("ZIP/Postal Code"),
                    '#prefix' => '<div class="input-item">',
                    '#suffix' => "</div></div></div>",
            );


            $order = commerce_order_load($_SESSION['orderId']);
            //Get the order object.
            $orderTotal = manual_order_total_array($order);

            $displayNextStep = "display: none";

            if(isset($orderTotal['base_price']) && $orderTotal['base_price'] > 0) {
                $displayNextStep = "";
            }

              //Submit Button 
            $form['submit_billing_shipping'] = array(
                    '#type' => 'submit',
                    '#value' => t('Proceed to Payment'),
                    '#prefix' => "<div class='manual-order-next-step' style='{$displayNextStep}'>",
                    '#ajax' => array(
                        'callback' => 'billing_shipping_address_callback',
                        'wrapper' => 'billing-shipping-address-wrapper',
                        'progress' => array('type' => 'throbber', 'message' => ''),
                    ),
            );

            //Cancel button (markup manipulated by Javascript)
            $form['cancel_button'] = array(
                    '#markup' => '<div id="billing-shipping-cancel" class="btn-gray-background close-profile">Cancel</div></div>',


            );
        }
 
        return $form;
}

//Ajax callback for form submission.
function billing_shipping_address_callback($form, &$form_state){

    $form_state['rebuild'] = TRUE;

    //Create the ajax commands array.
    $commands   = array();

    //Array of addresses
    $addresses = array();

    //Create the comparison array to check the difference between input and response
    $comparisonArr = array('Line1'=> '_address', 'City' => '_city', 
                                'Region' => '_us_state', 'PostalCode' => '_zip');



    //PHONE NUMBER
    //Update the user's profile with the validated phone number.
    //Load the current order's customer.
    $customer = user_load($_SESSION['customerId']);
    //Update the phone number to 
    if(isset($customer->uid) && isset($form_state['values']['phone_number'])) {
        //die();
        //Replace the customer's phone number with the new value.
        $customer->field_phone_number['und']['0']['value'] = $form_state['values']['phone_number'];
        //the first parameter is the user so a new user is not created
        $customer = user_save($customer);
    }

    $phoneNumber = $customer->field_phone_number['und']['0']['value'];


    //SHIPPING FORM

    //Check if same as billing.
    if($form_state['values']['billing_same']['same_billing'] != TRUE) {
        //Show the shipping form.
        $form['billing_same']['#suffix'] = "<div id='shipping-address-wrapper' style=''>";

        //Set fields required.
        $shippingFields = array('shipping_fname', 'shipping_lname', 'shipping_address','shipping_city', 'shipping_us_state', 'shipping_zip');
        foreach($shippingFields as $field) {
            if(empty($form_state['values'][$field])) {
                 form_set_error($field);
                 $form['shipping_messages']['#markup'] = "<div class='manual-order-message invalid-message'>Please enter a valid shipping address.</div>";
            }
        }

        //Create the shipping address array.
        $addressShipping = array('line1' => $form_state['values']['shipping_address'],'line2' => "",'city' => $form_state['values']['shipping_city'],'state' => $form_state['values']['shipping_us_state'], 'postal_code' => $form_state['values']['shipping_zip'],
            'first_name' => $form_state['values']['shipping_fname'], 'last_name' => $form_state['values']['shipping_lname'],
            'phone_number' => $customer->field_phone_number['und']['0']['value']);

        $comparisonArrShipping = array();

        //Add the shipping address to the addresses array.
        $addresses["shipping"] = $addressShipping;
    }


    ///BILLING FORM

    //Build the billing address array to send to avatax.
    $addressBilling = array('line1' => $form_state['values']['billing_address'],'line2' => "",'city' => $form_state['values']['billing_city'],
                                'state' => $form_state['values']['billing_us_state'],'postal_code' => $form_state['values']['billing_zip'],
                                'first_name' => $form_state['values']['billing_fname'], 'last_name' => $form_state['values']['billing_lname'],
                                'phone_number' => $customer->field_phone_number['und']['0']['value']);

    //Add the billing address to the addresses array.
    $addresses["billing"] = $addressBilling;

    //Get billing errors.
    $errors = form_get_errors();

    //Set an array to track differences.
    $unconfirmedArr = array();

    //Validate the available addresses.
    foreach($addresses as $key => $address) {

        //Validate address.
        if(!$errors && $response = commerce_avatax_validate_address($address)){
            //A valid address has been found.

            //Loop through available fields and perform comparison operations to the array.
            foreach($comparisonArr as $responseKey => $inputName) {

                //append the key to the input name.
                $inputName = $key . $inputName;

                if(isset($response[$responseKey]) && $form[$inputName]['#value'] != $response[$responseKey] && stripos($response[$responseKey], $form[$inputName]['#value']) !== 0) {
                    //Leave a note in the description saying what has been changed.
                    $form[$inputName]['#description'] = "Changed from <span>" . $form[$inputName]['#value'] . "</span>";
                    //Add a class to alert the user of the change.
                    $form[$inputName]['#attributes'] = array('class' => array('forminfo'));

                    $unconfirmedArr[] = $inputName;
                } else {
                    //Alert the user that the input is valid
                    $form[$inputName]['#attributes'] = array('class' => array('valid'));
                } 

                //Set the form input's value to the response.
                $form[$inputName]['#value'] = $response[$responseKey];  
            }

        } else {

            //The address could not be validated.

            $unconfirmedArr[] = $key;

            if(!$errors) {

                //Add an invalid class to each of the inputs.
                foreach($comparisonArr as $inputName) {
                        $inputName = $key . $inputName;
                        $form[$inputName]['#attributes'] = array('class' => array('error'));
                }

                //Set message.
                $form[$key . '_messages']['#markup'] = "<div class='manual-order-message invalid-message'>Please enter a valid {$key} address.</div>";

            }
        }

    }

    if(empty($unconfirmedArr)){

        //The addresses have been validated.

        //Create new billing user profile.
        $billingProfileId  = manual_order_profile_resource_create($addresses["billing"], "billing");

        //Set the shipping array key.
        $shippingKey = "billing";

        if(isset($addresses['shipping'])) {
        //If shipping is the same as billing, create shipping profile with identical info.
        $shippingKey = "shipping";
        }

        //Create new shipping user profile.
        $shippingProfileId = manual_order_profile_resource_create($addresses[$shippingKey], "shipping");

        //create the right array for the save controller
        $billing_profile_id_object = array ( 
        'und' => array ( array ( 'profile_id' => $billingProfileId , ) , ) , ); 
        $shipping_profile_id_object = array ( 
        'und' => array ( array ( 'profile_id' => $shippingProfileId , ) , ) , ); 

        $order = commerce_order_load($_SESSION['orderId']);

        $order->commerce_customer_billing  = $billing_profile_id_object;
        $order->commerce_customer_shipping = $shipping_profile_id_object;

        //All addresses have been validated.
        $commands[] = ajax_command_invoke('.manual-order-form-block', 'hide');

        //Set the order status to checkout shipping.
        commerce_order_status_update($order, "checkout_shipping");

        //Apply tax to the order
        commerce_avatax_calculate_sales_tax($order);

        //Update the order total.
        $commands[] = ajax_command_invoke(".receipt_total", "html", array(manual_order_total_basic()));

        //Replace the shipping options form.
        $new_state = array();
        $new_state['build_info'] = $form_state['build_info'];
        $new_state['rebuild'] = TRUE;
        $new_state['values'] = array();
        $new_state += form_state_defaults();
        $new_form_array = drupal_rebuild_form('shipping_options_form', $new_state);
        $new_form = drupal_render($new_form_array);
        $commands[] = ajax_command_invoke('#shipping-options-block-wrapper', 'replaceWith' , array($new_form));
        $commands[] = ajax_command_invoke('#shipping-options-block', 'fadeIn' , array("300"));

        //Show the receipt profile.
        $commands[] = ajax_command_invoke('.receipt-profile', 'html' , array(customer_profile_receipt($addresses["billing"])));

        //Call proceed to payment event.
        $commands[] = ajax_command_invoke('body', 'trigger' , array("proceedToPayment"));
    }

    //Replace the form.
    $commands[] = ajax_command_invoke('#billing-shipping-address-wrapper', 'replaceWith' , array(drupal_render($form)));

    //Show next step button.
    $commands[] = ajax_command_invoke('.manual-order-next-step', 'show');

    //Clear drupal form errors.
    $errors = form_get_errors();

    //Return the ajax commands array.
    return array('#type' => 'ajax', '#commands' => $commands);
}

/*
 * Define a validation function that drupal will
 * automatically call when the submit button is pressed.
 */
function billing_shipping_address_form_validate($form, &$form_state) {
drupal_get_messages('error');
}
 
/*
 * Define a submit function that drupal will
 * automatically call when submit is pressed (and all validators pass)
 */
function billing_shipping_address_form_submit($form, &$form_state) {

}
