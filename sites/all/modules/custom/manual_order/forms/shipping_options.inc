<?php

/**
 * Define the form to be returned.
 *
 * Note that drupal passes in two parameters,
 * $form - which holds all of the elements of the form
 * $form_state - a special array of settings & values needed by Drupal
 */

//formats the shipping options for styling reasons and returns an options array.
function format_shipping_options($options){

    foreach($options as $key => $option){

        //split the price from the option.

        $priceParts    = explode("$", $option);

        //Extract a description.
        $description   = $priceParts[0];

        //Get the price.
        $price         = "$" . $priceParts[1];

        //Find the shipping type.
        $descriptionParts    = explode(": ", $description);
        $shortDescription = $descriptionParts[0];
        $serviceParts     = explode(" ", $shortDescription);
        $service = strtolower($serviceParts[0]);

        $template      = "<div class='shipping-option-item'><div class='shipping-option-logo'><img src='https://glnenews.s3.amazonaws.com/aophs/manual-order/{$service}-logo.png'></div><div class='shipping-option-description'>{$shortDescription}</div><div class='shipping-option-price'><strong>{$price}</strong></div></div>";
        $options[$key] = $template;
    }

    return $options;
}

function shipping_options_form($form, &$form_state){


//Set the wrapper for the form.
$form['#prefix'] = "<div id='shipping-options-block-wrapper'>";
$form['#suffix'] = "</div>";

if(isset($_SESSION['customerId']) && $_SESSION['customerId'] > 0) {
// Collect the available shipping rates for this order.
  $order = commerce_order_load($_SESSION['orderId']);
  commerce_shipping_collect_rates($order);
  // Create an options array based on the rated services.
  $options = format_shipping_options(commerce_shipping_service_rate_options($order));


  $markup = "<h3>Shipping Options</h3><p>Choose a shipping option below.</p>";

  //Begin Create Customer Form Array
  $form['description_text'] = array(
    '#markup' => $markup,
  );


  $form['shipping_rates'] = array(
    '#type' => 'value',
    '#value' => $order->shipping_rates,
  );

  $form['shipping_service'] = array(
    '#type' => 'radios',
    '#options' => $options,
    '#prefix' =>  "<div class='shipping-options-wrapper'><div class='loading-block'></div>",
    //'#default_value' => key($options),
    '#suffix' => '</div>',
  );

}

/*  $form['custom_rate']['shipping_service'] = array(
    '#type' => 'select',
    '#title' => t('Shipping service'),
    '#options' => commerce_shipping_service_options_list(),
  );*/

  return $form;
}


function shipping_options_callback($form, &$form_state){
    
}



