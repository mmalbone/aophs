<?php

/**
 * Define the form to be returned.
 *
 * Note that drupal passes in two parameters,
 * $form - which holds all of the elements of the form
 * $form_state - a special array of settings & values needed by Drupal
 */
function edit_customer_form($form, &$form_state) {
        // Now the fun begins.
        // You can add as much or as little to this form as
        // needed.  See the forms api for all possible elements.


        $form['#prefix'] = '<div id="edit-customer-wrapper">';
        $form['#suffix'] = '</div>';

        if(isset($_SESSION['customerId']) && $_SESSION['customerId'] != 0) {

        //Load the current order's customer.
        $customer = user_load($_SESSION['customerId']);

        $markup = "<h3>Edit Customer</h3><p>Enter the customer's new information or <span class='dummy-link cancel-edit-customer'>cancel your edit</span>.</p>";

        //Begin Create Customer Form Array
        $form['description_text'] = array(
          '#markup' => $markup,
        );

        //First Name
        $form['fname'] = array(
                '#type' => 'textfield',
                '#required' => TRUE,
                '#title' => t("First Name"),
                '#default_value' => $customer->field_firstname['und'][0]['value'],
                '#prefix' => '<div class="input-group"><div class="input-item">',
                '#suffix' => "</div>",

        );

        //Last Name
        $form['lname'] = array(
                '#type' => 'textfield',
                '#required' => TRUE,
                '#title' => t("Last Name"),
                '#default_value' => $customer->field_lastname['und'][0]['value'],
                '#prefix' => '<div class="input-item">',
                '#suffix' => "</div></div>",
        );

        //Email Address
        $form['email'] = array(
                '#type' => 'textfield',
                '#required' => TRUE,
                '#title' => t("Email Address"),
                '#default_value' => $customer->mail,
                '#ajax' => array(
                    'callback' => 'edit_customer_email_callback',
                    'wrapper' => 'edit-customer-email-wrapper',
                    'progress' => array('type' => 'throbber', 'message' => ''),
                ),
        );
        $form['email']['#prefix'] = '<div id="edit-customer-email-wrapper">';
        $form['email']['#suffix'] = '</div>';

        //Phone
        $form['phone'] = array(
                '#type' => 'textfield',
                '#required' => TRUE,
                '#title' => t("Phone Number"),
                '#default_value' => $customer->field_phone_number['und'][0]['value'],
        );


        //Create Address
        $form['address'] = array(
                '#type' => 'textfield',
                '#required' => TRUE,
                '#title' => t("Street Address"),
                '#default_value' => $customer->field_billing_address['und'][0]['value'],

        );

        //City
        $form['city'] = array(
                '#type' => 'textfield',
                '#required' => TRUE,
                '#title' => t("City"),
                '#default_value' => $customer->field_billing_city['und'][0]['value'],
        );

        //Select State
        $form['us_state'] = array(
               '#type' => 'select',
               '#required' => TRUE,
               '#title' => t('State'),
               '#options' => array(
                '' => 'Choose',
                'AL' => 'Alabama',
                'AK' => 'Alaska',
                'AZ' => 'Arizona',
                'AR' => 'Arkansas',
                'CA' => 'California',
                'CO' => 'Colorado',
                'CT' => 'Connecticut',
                'DE' => 'Delaware',
                'DC' => 'District Of Columbia',
                'FL' => 'Florida',
                'GA' => 'Georgia',
                'HI' => 'Hawaii',
                'ID' => 'Idaho',
                'IL' => 'Illinois',
                'IN' => 'Indiana',
                'IA' => 'Iowa',
                'KS' => 'Kansas',
                'KY' => 'Kentucky',
                'LA' => 'Louisiana',
                'ME' => 'Maine',
                'MD' => 'Maryland',
                'MA' => 'Massachusetts',
                'MI' => 'Michigan',
                'MN' => 'Minnesota',
                'MS' => 'Mississippi',
                'MO' => 'Missouri',
                'MT' => 'Montana',
                'NE' => 'Nebraska',
                'NV' => 'Nevada',
                'NH' => 'New Hampshire',
                'NJ' => 'New Jersey',
                'NM' => 'New Mexico',
                'NY' => 'New York',
                'NC' => 'North Carolina',
                'ND' => 'North Dakota',
                'OH' => 'Ohio',
                'OK' => 'Oklahoma',
                'OR' => 'Oregon',
                'PA' => 'Pennsylvania',
                'RI' => 'Rhode Island',
                'SC' => 'South Carolina',
                'SD' => 'South Dakota',
                'TN' => 'Tennessee',
                'TX' => 'Texas',
                'UT' => 'Utah',
                'VT' => 'Vermont',
                'VA' => 'Virginia',
                'WA' => 'Washington',
                'WV' => 'West Virginia',
                'WI' => 'Wisconsin',
                'WY' => 'Wyoming',
               ),
               '#default_value' => $customer->field_billing_state['und'][0]['value'],
                '#prefix' => '<div class="input-group"><div class="input-item">',
                '#suffix' => "</div>",
        );

        //Create ZIP
        $form['zip'] = array(
                '#type' => 'textfield',
                '#required' => TRUE,
                '#title' => t("ZIP/Postal Code"),
                '#default_value' => $customer->field_postal_code['und'][0]['value'],
                '#prefix' => '<div class="input-item">',
                '#suffix' => "</div></div>",
        );


        //Submit Button 
        $form['submit_customer'] = array(
                '#type' => 'submit',
                '#value' => t('Save Customer'),
                '#ajax' => array(
                    'callback' => 'edit_customer_callback',
                    'wrapper' => 'edit-customer-wrapper',
                    'progress' => array('type' => 'throbber', 'message' => ''),
                ),
        );

        //Cancel button (markup manipulated by Javascript)
        $form['cancel_button'] = array(
                '#markup' => '<div class="btn-gray-background cancel-edit-customer">Cancel</div>',

        );
    }
 
    return $form;
}

//Ajax callback for form submission.
function edit_customer_callback($form, &$form_state){

        //Initiate AJAX Commands array
        $commands = array();
        $new_state = array();
        $new_state['build_info'] = $form_state['build_info'];
        $new_state['rebuild'] = TRUE;
        $new_state['values'] = array();
        $new_state += form_state_defaults();
        $new_form_array = drupal_rebuild_form('edit_customer_form', $new_state);
        $new_form = drupal_render($new_form_array);


        $errors = form_get_errors();
        if(!$errors) {
            //The form has no errors and has been submitted.
            $profile    = customer_profile_basic($_SESSION['customerId']);
            $commands[] = ajax_command_invoke("#customer-profile-block", "fadeIn");
            $commands[] = ajax_command_invoke("#edit-customer-block", "hide");
            $commands[] = ajax_command_invoke('#customer-profile-block-wrapper', 'replaceWith' , array($profile));
            
            //Replace the billing shipping form.
            $billingShippingFormArray = drupal_rebuild_form('billing_shipping_address_form', $new_state);
            $commands[] = ajax_command_invoke('#billing-shipping-address-wrapper', 'replaceWith' , array(drupal_render($billingShippingFormArray)));
            //Replace the edit customer form.
            $commands[] = ajax_command_html('#edit-customer-wrapper', $new_form);

        } else {
            //The form has errors.
            $commands[] = ajax_command_html('#edit-customer-wrapper', drupal_render($form));
        }

        return array('#type' => 'ajax', '#commands' => $commands);
}


//Ajax callback for the email field
function edit_customer_email_callback($form, &$form_state){

    //Load the current order's customer.
    $customer = user_load($_SESSION['customerId']);

    //Check if the email address
    if($form['email']['#value'] && $user = user_load_by_mail($form['email']['#value'])){
        //if the returned user isn't equal to our customer throw an error.
        if($user->mail != $customer->mail) {
            $form['email']['#description'] = "<strong>". $form['email']['#value'] ."</strong> already exists. Please try a different email address";
            $form['email']['#attributes']['class'] = array('error');

            //Reset the value to the user's default email address.
            $form['email']['#value'] = $customer->mail;
        }
    } else {
        if($form['email']['#value'] && valid_email_address($form['email']['#value'])) {
            //email address is unique
            $form['email']['#attributes']['class'] = array('valid');
        }
    }

    return $form['email'];
}

/*
 * Define a validation function that drupal will
 * automatically call when the submit button is pressed.
 */
function edit_customer_form_validate($form, &$form_state) {
        // Drupal stows away all of the form elements into
        // $form_state['values'].  We find our input
        // element and assign it to a variable for easy
        // reference.
        $email = $form_state['values']['email'];
        // If it's not a valid email, set an error.
        if(valid_email_address($email) == 0) {
                // form_set_error() tells drupal that it should not proceed.
                // The first parameter is the form element that didn't pass
                // validation.  The second is the message to tell the user.
        form_set_error('email', t('Not a valid email address'));
        }
        //Check if an email is in use.
        if($user = user_load_by_mail($form_state['values']['email'])){

            //form_set_error('email', t('User already exists! Please try a different email address.'));
        }
}
 
/*
 * Define a submit function that drupal will
 * automatically call when submit is pressed (and all validators pass)
 */
function edit_customer_form_submit($form, &$form_state) {

        //Load the current order's customer.
        $customer = user_load($_SESSION['customerId']);

        //Create the User Array
        $userArr                          = array();
        $userArr['field_firstname']       = $form_state['values']['fname'];
        $userArr['field_lastname']        = $form_state['values']['lname'];
        $userArr['field_phone_number']    = $form_state['values']['phone'];
        $userArr['field_billing_address'] = $form_state['values']['address'];
        $userArr['field_billing_city']    = $form_state['values']['city'];
        $userArr['field_billing_state']   = $form_state['values']['us_state'];
        $userArr['field_postal_code']     = $form_state['values']['zip'];

        $errors = array();

          //set up the email field
        $fields = array(
            'status' => 1,
            'mail'   => $form_state['values']['email'],
        );

          //Loop through user Array and assign values.
          foreach ($userArr as $key => $value) {
            $fields[$key] =  array(
                LANGUAGE_NONE => array(
                  0 => array(
                    'value' => $userArr[$key]
                  )
                )
            );
          }

         //if there are no errors, save the user.
         if(empty($errors)) {
          //the first parameter is the user so a new user is not created
          $account = user_save($customer, $fields);
         } 
}

