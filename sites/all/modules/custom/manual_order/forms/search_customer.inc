<?php

/**
 * Define the form to be returned.
 *
 * Note that drupal passes in two parameters,
 * $form - which holds all of the elements of the form
 * $form_state - a special array of settings & values needed by Drupal
 */

function search_customer_form($form, &$form_state) {
        // Now the fun begins.
        // You can add as much or as little to this form as
        // needed.  See the forms api for all possible elements.
        $form['#prefix'] = "<div id='search-customer-wrapper'>";
        $form['#suffix'] = "</div>";

        $markup = "<h3>Search Customer</h3><p>Enter the customer's name, phone number, or email address. If they do not have an account you can create a <span id='show-create-customer' class='dummy-link'>new customer</span>.</p>";

        $order = commerce_order_load($_SESSION['orderId']);

       // $markup = print_r($order, true);

        //Begin Search Customer Form Array
        $form['description_text'] = array(
                '#markup' => $markup,
        );

        //Search for the customer
        $form['search_customer'] = array(
                '#type' => 'textfield',
                '#prefix' => '<div class="search-form">',
                '#suffix' => "</div>",
                '#autocomplete_path' => "search_customer_autocomplete_callback",
                '#ajax' => array(
                    'callback' => 'search_customer_callback',
                    'wrapper' => 'search-customer-wrapper',
                    'progress' => array('type' => 'throbber', 'message' => ''),
                    'method' => 'html',
                ),
        );
        
        return $form;
}

function search_customer_callback($form, &$form_state){

        //Initiate AJAX Commands array
        $commands = array();
        $new_state = array();
        $new_state['build_info'] = $form_state['build_info'];
        $new_state['rebuild'] = TRUE;
        $new_state['values'] = array();
        $new_state += form_state_defaults();
        $new_form_array = drupal_rebuild_form('search_customer_form', $new_state);
        $new_form = drupal_render($new_form_array);
        //Replace invalid characters
        $customerId = preg_replace('/[^A-Za-z0-9\-]/', '', $form['search_customer']['#value']);
        //Check if customer is found.
        if($customerId != "" && $profile = customer_profile_basic($customerId)){
            //The customer has been found.
            manual_order_assign_customer($customerId);
            
            //Replace the customer profile wrapper.
            $commands[] = ajax_command_invoke('#customer-profile-block-wrapper', 'replaceWith' , array($profile));
            //Replace the edit customer form.
            $editCustomerArray = drupal_rebuild_form('edit_customer_form', $new_state);
            $commands[] = ajax_command_invoke('#edit-customer-wrapper', 'replaceWith' , array(drupal_render($editCustomerArray)));
            //Replace the billing shipping form.
            $billingShippingFormArray = drupal_rebuild_form('billing_shipping_address_form', $new_state);
            $commands[] = ajax_command_invoke('#billing-shipping-address-wrapper', 'replaceWith' , array(drupal_render($billingShippingFormArray)));
            $commands[] = ajax_command_invoke('#billing-shipping-address-block', 'fadeIn' , array("300"));

            $commands[] = ajax_command_invoke('.receipt-profile', 'show' , array(300));


        }

        $commands[] = ajax_command_html('#search-customer-wrapper', $new_form);
        return array('#type' => 'ajax', '#commands' => $commands);
}

//The autocomplete function for search product
function search_customer_autocomplete_callback($string = ""){

    //Remove spaces from beginning and end of string.
    $string = trim($string);

    $matches = array();
    $view = views_get_view('user_lookup');
    $view->set_display('views_data_export_1');

    $view->is_cacheable = FALSE;  

    $filter = $view->get_item( 'views_data_export_1', 'filter', 'combine');

    $filter['value'] = $string;

    $view->set_item('views_data_export_1', 'filter', 'combine', $filter);

    $view->pre_execute();
    $view->execute();
    if($results = $view->result) {

    foreach ($results as $result) {
        if($user = user_load_by_name($result->users_name)) {
            $output = "<div class='dropdown-results-item'>";

            //Output user name.
            if(isset($user->field_firstname['und'][0]['value'])) {
            $output .= "<strong>" . $user->field_firstname['und'][0]['value'] . " " .
             $user->field_lastname['und']['0']['value'] . "</strong><br>";
            }

            //Output user email
            $output .= $user->mail . "<br>";

            //Output user address
            if(isset($user->field_billing_address['und'][0]['value'])) {
            $output .= $user->field_billing_address['und'][0]['value'] . " " . $user->field_billing_city['und']['0']['value'] . ", " .  
            $user->field_billing_state['und'][0]['value'] . " " . $user->field_postal_code['und']['0']['value'];
            }

            //Customer Phone
            if(isset($user->field_phone_number['und'][0]['value'])){
            $output .= "<br>" . $user->field_phone_number['und'][0]['value'];
            }
            $output .= "</div>";

            if (user_has_role('customer support') || user_has_role('sales')) {
                $matches["{{".$user->uid . "}}"] = $output;
            }
        }
    }
    }

  $matches["{{{". $string ."}}}"] = "<div id='create-customer-button' class='create-customer'>Create New Customer</div>";

  drupal_json_output($matches);
}

function search_customer_form_validate($form, &$form_state){

    $form_state['rebuild'] = TRUE;
    $form_state['flag'] = 1;
}
 
/*
 * Define a submit function that drupal will
 * automatically call when submit is pressed (and all validators pass)
 */
function search_customer_form_submit($form, &$form_state) {

}