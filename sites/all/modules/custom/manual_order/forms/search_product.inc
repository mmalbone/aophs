<?php

/**
 * Define the form to be returned.
 *
 * Note that drupal passes in two parameters,
 * $form - which holds all of the elements of the form
 * $form_state - a special array of settings & values needed by Drupal
 */
function search_product_form($form, &$form_state) {
        // Now the fun begins.
        // You can add as much or as little to this form as
        // needed.  See the forms api for all possible elements.

        $markup = "<h3>Add Products</h3><p>Enter the title or SKU of the product the customer wants to buy.</p>";

        //Begin Search Customer Form Array
        $form['description_text'] = array(
                '#markup' => $markup,
        );

        //Search for the customer
        $form['search_product'] = array(
                '#type' => 'textfield',
                '#prefix' => '<div id="product-search-form" class="search-form">',
                '#suffix' => "</div>",
                '#autocomplete_path' => "search_product_autocomplete_callback",
                '#ajax' => array(
                    'callback' => 'search_product_callback',
                    'wrapper' => 'product-search-form',
                    'progress' => array('type' => 'throbber', 'message' => ''),
                ),
        );

        return $form;
}

//
function search_product_callback($form, &$form_state){

    //Set the product id
    $productId = preg_replace('/[^A-Za-z0-9\-]/', '', $form['search_product']['#value']);
    //Unset the search value
    unset($form['search_product']['#value']);

     //Initiate AJAX Commands array
    $commands = array();

    if($product = commerce_product_load($productId)) {

        //Assign product to order
        $lineItem = manual_order_assign_product($product);

        //The product has been found.
        //Render the line item template.
        $line_item = "<li class='line-item-new'>" . search_product_line_item_basic($product) . "<div class='button-close remove-item' data-line-item='" . $lineItem . "'></div></li>";
        $commands[] = ajax_command_invoke(".product_list", "append", array($line_item));
        $commands[] = ajax_command_invoke(".receipt_total", "html", array(manual_order_total_basic()));
    }

    $new_form = drupal_render($form['search_product']);
    $commands[] = ajax_command_html('#product-search-form', $new_form);

    return array('#type' => 'ajax', '#commands' => $commands);
}

//Takes an input of a product object and renders a line item template.
function search_product_line_item_basic($product, $description = ""){

    //Image path
    $image = variable_get('aop_images_url') . $product->field_product_image['und'][0]['value'];

    //Price
    $price =  commerce_currency_format($product->commerce_price['und'][0]['amount'], 'USD');

    if($description != "") {
            //Create product description
    $parts = explode(". ", $product->field_hsc_short_description['und'][0]['value']);
    //Remove questions and exclamations.

    $description = substr($parts[0], 0, 140) . ".<br>";
    }


    $markup = "<div class='line-item-wrapper'><div class='line-item-image'><img src='{$image}' /></div><div class='line-item-description'>" 
    . "<div class='line-item-title'>" . $product->title . "</div>{$description}<span class='line-item-sku'>" . $product->sku . "</span></div><div class='line-item-price'>{$price}</div></div>";

    return $markup;
}

//The autocomplete function for search product
function search_product_autocomplete_callback($string = ""){

    //Sanitize string.
    $string = str_ireplace("lifepac", "LIFEPAC®", $string);
    $string = str_ireplace("5 s", "5-s", $string);
    $string = str_ireplace("switched on schoolhouse", "sos", $string);
    $string = str_ireplace("switched-on schoolhouse", "sos", $string);

   // DB table which holds products
    $query = db_select('commerce_product', 'p');

    $or = db_or()->condition('p.title', '%' . db_like($string) . '%', 'LIKE')->condition('p.sku', '%' . db_like($string) . '%', 'LIKE');

    // Select rows that match the string
    $results = $query
    ->fields('p', array('title', 'product_id'))
    ->condition($or)
    ->range(0, 6)
    ->execute();

    $matches = array();

    // add matches to $matches
    foreach ($results as $item) {
    $product =  commerce_product_load($item->product_id);
    $matches["{{" . $item->product_id . "}}"] =
    "<div class='dropdown-results-item product'>" . search_product_line_item_basic($product, 1) . "</div>";    
    }

    drupal_json_output($matches);
}
 
/*
 * Define a validation function that drupal will
 * automatically call when the submit button is pressed.
 */
function search_product_form_validate($form, &$form_state) {

}
 
/*
 * Define a submit function that drupal will
 * automatically call when submit is pressed (and all validators pass)
 */
function search_product_form_submit($form, &$form_state) {

}