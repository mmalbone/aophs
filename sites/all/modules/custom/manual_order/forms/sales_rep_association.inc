 <?php

/**
 * Define the form to be returned.
 *
 * Note that drupal passes in two parameters,
 * $form - which holds all of the elements of the form
 * $form_state - a special array of settings & values needed by Drupal
 */
function sales_rep_association_form($form, &$form_state) {

    //Load in CSA
    global $user;

    if(isset($_SESSION['orderId'])) {
        //Get the order object.
        $order = commerce_order_load($_SESSION['orderId']);

        //Get the user object.
        $user  = user_load($user->uid);

        //Set the sales rep initials variable
        if(isset($order->field_sales_rep_id['und'][0]['value'])) {
        $salesRepInitials = $order->field_sales_rep_id['und'][0]['value'];
        } else {
        $salesRepInitials = "";
        }


        $showHideDisplayInititals = "";

        //Check if the sales rep has their user id set.
        if(!isset($user->field_user_sales_rep_id['und'][0]['value'])) {
            $showHideDisplayInititals = "display: none;";
            $showHideInputInititals   = "";
        } else {
            $showHideDisplayInititals = "";
            $showHideInputInititals   = "display: none;";
        }

        //Set Markup Variable for Sales Rep Initials Display
        $markup = "<div style='{$showHideDisplayInititals}' id='sales-rep-initials-display' class='discount-item'>Salesperson Initials: <span id='sales-rep-initials-span'>{$salesRepInitials}</span><div class='button-close-initials'></div></div>";

        $form['#prefix'] = "<div id='sales-rep-initials'>";
        $form['#suffix'] = "</div>";

        //Begin Associate Sales Rep Form Array
        $form['description_text'] = array(
          '#markup' => $markup,
        );

        $prefix = "<div id='sales-rep-initials-input' style='{$showHideInputInititals}'>";
        $suffix = "</div>";

        //Sales Rep
        $form['sales_rep_initials'] = array(
            '#type' => 'textfield',
            '#required' => FALSE,
            '#title' => t("Salesperson Initials"),
            '#maxlength' => 6,
            '#default_value' => $salesRepInitials,
            '#ajax' => array(
                        'callback' => 'sales_rep_association_callback',
                        'progress' => array('type' => 'throbber', 'message' => ''),
            ),
            '#prefix' => $prefix,
            '#suffix' => $suffix,
        );

    }

    return $form;

}


//Returns a response to the salesperson id form.
function sales_rep_association_callback($form, &$form_state){

$initials = $form_state['values']['sales_rep_initials'];

//Create commands away.
$commands = array();

$commands[] = ajax_command_invoke('#sales-rep-initials', 'replaceWith' , array(drupal_render($form)));

if(!empty($initials )){
    //Get the order object.
    $order = commerce_order_load($_SESSION['orderId']);
    //Set the order object's id to the value in the input field.
    $order->field_sales_rep_id['und'][0]['value'] = strtoupper($initials);
    //Save the order object
    commerce_order_save($order);
    $commands[] = ajax_command_invoke('#sales-rep-initials-span', 'html', array($initials));
    $commands[] = ajax_command_invoke('#sales-rep-initials-input', 'hide');
    $commands[] = ajax_command_invoke('#sales-rep-initials-display', 'show');
} else {
    $commands[] = ajax_command_invoke('#sales-rep-initials-display', 'hide');
}


return array('#type' => 'ajax', '#commands' => $commands);

}

