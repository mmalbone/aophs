<?php

/**
 * Define the form to be returned.
 *
 * Note that drupal passes in two parameters,
 * $form - which holds all of the elements of the form
 * $form_state - a special array of settings & values needed by Drupal
 */
function search_promo_form($form, &$form_state){

        $form['#prefix'] = "<div id='search-promo-wrapper'>";
        $form['#suffix'] = "</div>";

        //Begin Search Promo Code Form Array
        $form['description_text'] = array(
            '#markup' => "<h3>Promo Code</h3><p>If the customer has a promo code enter it below.</p>",
        );

        //Search for the customer
        $form['search_promo'] = array(
            '#type' => 'textfield',
            '#prefix' => "<div id='promo-search-form'>",
            '#ajax' => array(
                //'callback' => 'search_promo_callback',
                'wrapper' => 'promo-search-form',
                'progress' => array('type' => 'throbber', 'message' => ''),
            ),
        );

        $form['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Apply Discount'),
            '#suffix' => "</div>",
            '#ajax' => array(
                'callback' => 'search_promo_callback',
                'wrapper' => 'promo-search-form',
                'progress' => array('type' => 'throbber', 'message' => ''),
            ),
        );

        return $form;
}

function search_promo_callback($form, &$form_state){

    $code = $form['search_promo']['#value'];

    //Initiate AJAX Commands array
    $commands = array();

    //Unset the description text from getting re-rendered.
    unset($form['description_text']);
    //unset($form['#prefix']);
    //unset($form['#suffix']);

    if($order = manual_order_get_order()){
        //Check if coupon code exists.
        if(commerce_coupon_code_exists($code)){
            $totalArray = manual_order_total_array($order);
            if($totalArray['base_price']){

                //Assign the promo code to the order and return true or false.
                if($coupon = manual_order_assign_promo($code)){
                    $discount = manual_order_load_discount_label($coupon->commerce_discount_reference['und'][0]['target_id']);

                    $template = "<div class='discount-item' id='discount-item-box'>
                                    <div class='button-close close-discount' id='button-close-promo'></div>
                                    <div id='discount-item-text'>".$discount."</div>
                                </div>";
                    $commands[] = ajax_command_invoke("#promo-search-form", "hide");
                    $commands[] = ajax_command_invoke("#search-promo-wrapper", "append", array($template));
                    $commands[] = ajax_command_invoke(".receipt_total", "html", array(manual_order_total_basic()));
                    $commands[] = ajax_command_invoke("#discount-total", "fadeIn", array(300));
                } else {
                    $error = "Coupon is already applied to this order.";
                }
            } else {
                $error = "Please add a product to the cart in order to apply a promo code.";
            }
        } else {
            if(!empty($form['search_promo']['#value'])){
                //If the input isn't empty
                $error = t("Please try a different promo code.");
            } else {
                $error = t('Please enter a promo code.');
            }
        }
    }

    $form['search_promo']['#description'] = $error? $error: null;

    $commands[] = ajax_command_html('#promo-search-form', drupal_render($form));
    return array('#type' => 'ajax', '#commands' => $commands);
}

/*
 * Define a validation function that drupal will
 * automatically call when the submit button is pressed.
 */
function search_promo_form_validate($form, &$form_state) {

}
 
/*
 * Define a submit function that drupal will
 * automatically call when submit is pressed (and all validators pass)
 */
function search_promo_form_submit($form, &$form_state) {

}