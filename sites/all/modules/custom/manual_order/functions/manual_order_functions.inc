<?php 

//MAIN FUNCTIONS FOR MANUAL ORDER

//Get the current order based off the Session.
function manual_order_get_order(){
	return isset($_SESSION['orderId'])? commerce_order_load($_SESSION['orderId']): NULL;
}

//Update the manual order.
function manual_order_refresh($order, $save = TRUE){
	if($save){
		commerce_order_save($order);
	}
	commerce_cart_order_refresh($order);
}

//Return an array of the order totl components
function manual_order_total_array($order){

	$orderTotalArr = array();

	$components = $order->commerce_order_total['und'][0]['data']['components'];

	foreach($components as $key => $component) {
		//Reformat discount name.
		if(strpos($component['name'], "discount") === 0) {
			$component['name'] = "discount";
		}
		//Add to manual order total array.
		$orderTotalArr[$component['name']] = $component['price']['amount'];
	}

	return $orderTotalArr;
}

//Updates shipping option based on user selection
function shipping_option_select_callback(){

	$json = array();

	if($order = manual_order_get_order()) {

		//Remove Shipping Option
		commerce_shipping_delete_shipping_line_items($order);

		//Add Shipping option.

		$service = $_POST['option'];

		// Load the full shipping service info array.
		$shipping_service = commerce_shipping_service_load($service);

		 // If the service specifies a rate callback...
		if ($callback = commerce_shipping_service_callback($shipping_service, 'rate')) {
			// Get the base rate price for the shipping service.
			$price = $callback($shipping_service, $order);

			// If we got a base price...
			if ($price) {
				// Create a calculated shipping line item out of it.
				$line_item = commerce_shipping_service_rate_calculate($service, $price, $order->order_id);
				$line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

				// Add the rate to the order as long as it doesn't have a NULL price amount.
				if (!is_null($line_item_wrapper->commerce_unit_price->amount->value())) {
					// Include a weight property on the line item object from the shipping
					// service for sorting rates.
					$line_item->weight = empty($shipping_service['weight']) ? 0 : $shipping_service['weight'];
					$order->shipping_rates[$service] = $line_item;
					commerce_shipping_add_shipping_line_item($line_item, $order);
				}
			}
		}

		//commerce_order_save($order);

		//Recalculate Tax
		commerce_avatax_calculate_sales_tax($order);
		$json['content'] = manual_order_total_basic();
	}

	print json_encode($json);
}

//Returns the manual order total formatted.
function manual_order_total_update_callback(){

	$json = array();
	if($order = manual_order_get_order()){
		$json['content'] = manual_order_total_basic();
	}

	print json_encode($json);
}

//Creates a new manual order.
function manual_order_create_new(){

	if(!isset($_SESSION['customerId'])) {
		$_SESSION['customerId'] = 0;
	}

    // Create the new order with the customer's uid and the cart order status.
    $order = commerce_order_new(0, 'cart');
    $order->log = t('Created as a shopping cart order.');

    global $user;

    //Load in logged in user.
    $user = user_load($user->uid);

    //Set the default sales rep iniitals.

    //$emptyInitials = 'Your Profile is missing Sales Rep Initials';
    if (isset($user->field_user_sales_rep_id['und'][0]['value'])) {
    	$order->field_sales_rep_id['und'][0]['value'] = $user->field_user_sales_rep_id['und'][0]['value'];
    }
    //} else {
    //	$order->field_sales_rep_id['und'][0]['value'] = "";
    //};

    // Save it so it gets an order ID and return the full object.
    commerce_order_save($order);
    //Assign the session order ID.
    $_SESSION['orderId'] = $order->order_id;
}

//Assigns a customer to the order.
function manual_order_assign_customer($customerId){

	//Set the customer ID variable
	$_SESSION['customerId'] = $customerId;

	if($order = manual_order_get_order()){
		//The order exists, just update the order with the new customer.
		$order->uid = $_SESSION['customerId'];
		manual_order_refresh($order);
	} else {
		manual_order_create_new();
	}

}

//Unassigns a customer from an order.
function manual_order_unassign_customer_callback(){
	if($order = manual_order_get_order()){
		$order->uid = 0;
		manual_order_refresh($order);
		$_SESSION['customerId'] = 0;
	}
}

//Assigns a product to the manual order.
function manual_order_assign_product($product) {
	//Set Customer Variable
	if(!isset($_SESSION['customerId'])) {
		$_SESSION['customerId'] = 0;
	}

	//Set the order variable
	if(!isset($_SESSION['orderId'])) {
		manual_order_create_new();
	} else {
		$order = commerce_order_load($_SESSION['orderId']);
	}

	// Load whatever product represents the item the customer will be
	// paying for and create a line item for it.
	$line_item = commerce_product_line_item_new($product, 1, $order->order_id);

	// Save the line item to get its ID.
	commerce_line_item_save($line_item);

	// Add the line item to the order.
	$order_wrapper = entity_metadata_wrapper('commerce_order', $order);
	$order_wrapper->commerce_line_items[] = $line_item;

	// Save the order again to update its line item reference field.
	manual_order_refresh($order);

	return $line_item->line_item_id;
}

//Unassigns a product from the manual order.
function manual_order_unassign_product_callback(){
	//If the post variable is set, unassign the item from the order. 
	if(isset($_POST['line_item_id'])) {
		//Load the order.
		if($order = manual_order_get_order()){
			commerce_cart_order_product_line_item_delete($order, $_POST['line_item_id']);
			manual_order_refresh($order);
			$totalArray = manual_order_total_array($order);
		}
	}

	$json = array('items' => $totalArray['base_price'], 'content'=> manual_order_total_basic());
	print json_encode($json);
}


//Assigns a promo code to an order.
function manual_order_assign_promo($promo){
	if($order = manual_order_get_order()){
		$error = '';

		$order_wrapper = entity_metadata_wrapper('commerce_order', $order);

		$coupon = commerce_coupon_redeem_coupon_code($promo, $order, $error);
		manual_order_refresh($order);

		return $coupon;
	}
}

function manual_order_load_discount_label($discountId) {
	$query = db_select('commerce_discount', 'd');

	if(!empty($discountId)) {
	    // Select rows that match the string
	    $results = $query
	    ->condition('discount_id', $discountId,'=')
	    ->fields('d', array('component_title'))
	    ->execute();

	    return $results->fetchField();
	}
}

//Unassigns a promo code to an order.
function manual_order_unassign_promo_callback(){
	if($order = manual_order_get_order()){
		$order_wrapper = entity_metadata_wrapper('commerce_order', $order);
		$coupon = $order_wrapper->commerce_coupons->value();

		//Remove coupon from order hook
		$save = TRUE;
		if(isset($coupon[0])) {
			commerce_coupon_remove_coupon_from_order($order, $coupon[0], $save);
			manual_order_refresh($order);
		}

		print manual_order_total_basic();
	}
}

//Creates and assigns a new customer profile
function manual_order_profile_resource_create($address) {
	//Set the profiles array.
	$profiles = array();

	//Get the customer
	$customer = user_load($customer->uid);

	//Set the new profile variables.
	$new_profile = commerce_customer_profile_new("billing");
	$wrapper = entity_metadata_wrapper('commerce_customer_profile', $new_profile);
	$wrapper->uid = $_SESSION['customerId'];
	$wrapper->commerce_customer_address->country = "US";
	$wrapper->commerce_customer_address->name_line = $address['first_name'] . " " . $address['last_name'];
	$wrapper->commerce_customer_address->first_name = $address['first_name'];
	$wrapper->commerce_customer_address->last_name = $address['last_name'];
	$wrapper->commerce_customer_address->organisation_name = "";
	$wrapper->commerce_customer_address->administrative_area = $address['state'];
	$wrapper->commerce_customer_address->sub_administrative_area = "";
	$wrapper->commerce_customer_address->locality = $address['city'];
	$wrapper->commerce_customer_address->dependent_locality = "";
	$wrapper->commerce_customer_address->postal_code = $address['postal_code'];
	$wrapper->commerce_customer_address->thoroughfare = $address['line1'];
	$wrapper->field_billing_phone = $address['phone_number'];

	//save the new profile
	commerce_customer_profile_save($new_profile);

	return $new_profile->profile_id;
}


// 
function manual_order_get_tid($specific, $type){
	// Get the TID for the curriculum taxonomy term
    $term_array = taxonomy_get_term_by_name($specific, $type);
    foreach($term_array as $ids) {
        $TID = $ids->tid;
    }
    return $TID;
}


// Add Monarch Activation fields to orders
function manual_order_add_activation($order, $sku, $ac){
	// Create a new field collection
    $collection = entity_create('field_collection_item', array('field_name' => 'field_activation_codes'));
    // Set the host entity to the order
    $collection->setHostEntity('commerce_order', $order);
    // Set the field collection metadata wrapper
    $fc_wrapper = entity_metadata_wrapper('field_collection_item', $collection);
    // Set the fields
    $fc_wrapper->field_monarch_sku->set($sku);
    $fc_wrapper->field_activation_code_on_order->set($ac);
    // Save field collection item
    $fc_wrapper->save(TRUE);
}

// For testing purposes
function manual_order_output_order_callback(){
	$order = manual_order_get_order();

	$manual_order_activation_codes = array();

    //if (isset($order->field_activation_codes) && sizeof($order->field_activation_codes['und']) > 0) { 
    foreach($order->field_activation_codes['und'] as $ac) {
        $activation_code = field_collection_item_load($ac['value']);
        $sku = $activation_code->field_monarch_sku['und'][0]['value'];
        $act_code = $activation_code->field_activation_code_on_order['und'][0]['value'];
        $manual_order_activation_codes[$sku] = $act_code;
    }
    //}

	print_r($order->field_activation_codes['und']);
}
