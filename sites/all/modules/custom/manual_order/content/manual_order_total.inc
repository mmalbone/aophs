<?php

/**
 * Define the form to be returned.
 *
 * Note that drupal passes in two parameters,
 * $form - which holds all of the elements of the form
 * $form_state - a special array of settings & values needed by Drupal
 */

function manual_order_total_basic() {
    $orderSubtotal     = 0;
    $orderTotal        = 0;

    if($order = manual_order_get_order()){
        // Get the order wrapper
        $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
        
        // Order total.
        $orderTotal = $order_wrapper->commerce_order_total->value();

        //Define the total array.
        $totalArray = manual_order_total_array($order);
        
        //Set the order subtotal.
        $orderSubtotal = $totalArray['base_price'];

        //Set the default promo field display.
        $displayPromo = "display: none";
        $priceOff = "";
        if($totalArray['discount']){
            $displayPromo = "";
            $priceOff = commerce_currency_format($totalArray['discount'], 'USD');
        }

        //Set the default sales tax display
        $displayTax   = "display: none";
        $priceTax = "";
        if($totalArray['avatax']) {
            $displayTax   = "";
            $priceTax = commerce_currency_format($totalArray['avatax'], 'USD');
        }

        //Set the default shipping display.
        $displayShipping = "display: none";
        if($totalArray['shipping']) {
            $displayShipping   = "";
            $priceShipping = commerce_currency_format($totalArray['shipping'], 'USD');
        }

    }

    //Convert the currency format.
    $orderSubtotal  = commerce_currency_format($orderSubtotal, 'USD');
    $orderTotal     = commerce_currency_format($orderTotal['amount'], 'USD');

    $markup =   "<div class='item-total'>
                    <div class='item-total-val ajax-content' id='subtotal-total'>{$orderSubtotal}</div>
                    <div class='item-total-label'>Subtotal:</div>
                </div>
                <div class='item-total' id='discount-total' style='{$displayPromo}'>
                    <div class='item-total-val'>{$priceOff}</div>
                    <div class='item-total-label'>Discount:</div>
                </div>
                <div class='item-total' id='tax-total' style='{$displayTax}'>
                    <div class='item-total-val ajax-content' id='item-tax-val'>{$priceTax}</div>
                    <div class='item-total-label'>Sales Tax:</div>
                </div>
                <div class='item-total' style='{$displayShipping}'>
                    <div class='item-total-val ajax-content'>{$priceShipping}</div>
                    <div class='item-total-label'>Shipping:</div>
                </div>
                <div class='item-total final-total'>
                    <div class='item-total-val order-total-amt ajax-content'>{$orderTotal}</div>
                    <div class='item-total-label'>Total:</div>
                </div>";

    return $markup;
}