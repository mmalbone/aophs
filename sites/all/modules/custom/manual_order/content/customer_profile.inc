<?php

/**
 * Define the form to be returned.
 *
 * Note that drupal passes in two parameters,
 * $form - which holds all of the elements of the form
 * $form_state - a special array of settings & values needed by Drupal
 */

function customer_profile_basic($userId = 0) {



        ////Customer Variable 

        if($userId != 0 && $customer = user_load($userId)){

        //Set the order customer variable.
        $_SESSION['customerId'] = $customer->uid;

        $name    = $customer->field_firstname['und']['0']['value'] . " " .  
                   $customer->field_lastname['und']['0']['value'];
        $email   = $customer->mail;
        $address = $customer->field_billing_address['und']['0']['value'] . " " . $customer->field_billing_city['und']['0']['value'] . ", " .  
        $customer->field_billing_state['und']['0']['value'] . " " . $customer->field_postal_code['und']['0']['value'];


        $phone   = $customer->field_phone_number['und']['0']['value'];


        $template =     "<div id='customer-profile-block-wrapper' class='customer-profile-wrapper'>
                        <div class='button-close close-profile' id='button-close-profile'></div>
                        <h3>{$name}</h3>
                        <h4>{$email}</h4>
                        <p>{$address}<br>
                        {$phone}
                        </p>
                        <p><span class='dummy-link edit-customer-profile'>Edit Profile</span></p>
                    </div>";
        
        return $template;
        } else {
        
            $markup = "<div id='customer-profile-block-wrapper' class='customer-profile-wrapper ajax-loading'><img src='https://glnenews.s3.amazonaws.com/aophs/large-loader.GIF'/></div>";
            return $markup;
        }
}


//Create profile receipt section from a billing address array.
function customer_profile_receipt($address) {

        $name    = $address['first_name'] . " " .  
                   $address['last_name'];
        $addressItem = $address['line1'] . " " . $address['city'] . ", " .  
        $address['state'] . " " . $address['postal_code'];
        $phone   = $address['phone_number'];
        $template = "<div class='receipt-profile-wrapper'><strong>{$name}</strong><br>{$addressItem}<br>{$phone}</div>";
        
        return $template;
}