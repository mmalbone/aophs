<?php

/**
 * Define the form to be returned.
 *
 * Note that drupal passes in two parameters,
 * $form - which holds all of the elements of the form
 * $form_state - a special array of settings & values needed by Drupal
 */

function manual_order_receipt_basic() {

    //For Testing
    //$_SESSION['newCustomer'] = 1;

    if(isset($_SESSION['orderId'])) {

        $orderNumber = $_SESSION['orderId'];
    }

    if(isset($_SESSION['newCustomer'])) {
        $newCustomerText = module_invoke('block', 'block_view', '40');
        if (!empty($newCustomerText['content'])) {
            $newCustomerText = render($newCustomerText['content']);
        } else {
            $newCustomerText = "<p>An email will be sent to undefined asking them to complete the customer account creation process.</p>
                                <p>Completing the account process is optional but encouraged for account management purposes.</p>";
        }
        unset($_SESSION['newCustomer']);
    }

        $thankyouBlock = module_invoke('block', 'block_view', '39');
        if (!empty($thankyouBlock['content'])) {
            $thankyouBlock = render($thankyouBlock['content']);
        } else {
            $thankyouBlock = "Thank you for homeschooling with us!";
        }
        
        

    $markup = " <h3>Order #{$orderNumber} Submitted!</h3>

                {$thankyouBlock}
                
                {$newCustomerText}
                    
                <p><a class='dummy-link'>Create a new order</a></p>";

    return $markup;
}
