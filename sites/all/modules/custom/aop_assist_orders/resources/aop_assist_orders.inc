<?php

/**
 * @file
 * Contains the resource callbacks for AOP Assist Orders.
 */

function aop_assist_orders_load_order($assist_order_id) {
    $entityType = 'commerce_order';
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', $entityType)
        ->fieldCondition('field_aop_order_number', 'value', $assist_order_id, '=');
    $result = $query->execute();
    $order = new stdClass;
    if (isset($result[$entityType])) {
        $order_stubs = array_values($result[$entityType]);
        if (count($order_stubs) === 1) {
            //$entities = entity_load($entityType, $order_ids);
            $order = $order_stubs[0];
        } else {
            $order->error = "Expecting one single commerce order for Assist Order ID $assist_order_id, got " . count($order_ids);
        }
    } else {
        $order->error = "No orders found for Assist Order ID $assist_order_id";
    }
    return $order;
}


function aop_assist_orders_retrieve($assist_order_id) {
    $order = aop_assist_orders_load_order($assist_order_id);
    if (isset($order->error) && $order->error == 404) {
        return "404";
    }
    return $order;
}


function aop_assist_orders_update($assist_order_id, $status) {
   $order = aop_assist_orders_load_order($assist_order_id);
   if (isset($order->error) && $order->error == 404) {
     return "404";
   }
   $order->status = $status;
   entity_save('commerce_order', $order);

   return 200;

}

