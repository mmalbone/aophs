<?php
$states = array( 'AL' => 'Alabama', 'AK' => 'Alaska', 'AZ' => 'Arizona', 'AR' => 'Arkansas', 'CA' => 'California', 'CO' => 'Colorado', 'CT' => 'Connecticut', 'DE' => 'Delaware', 'DC' => 'District Of Columbia', 'FL' => 'Florida', 'GA' => 'Georgia', 'HI' => 'Hawaii', 'ID' => 'Idaho', 'IL' => 'Illinois', 'IN' => 'Indiana', 'IA' => 'Iowa', 'KS' => 'Kansas', 'KY' => 'Kentucky', 'LA' => 'Louisiana', 'ME' => 'Maine', 'MD' => 'Maryland', 'MA' => 'Massachusetts', 'MI' => 'Michigan', 'MN' => 'Minnesota', 'MS' => 'Mississippi', 'MO' => 'Missouri', 'MT' => 'Montana', 'NE' => 'Nebraska', 'NV' => 'Nevada', 'NH' => 'New Hampshire', 'NJ' => 'New Jersey', 'NM' => 'New Mexico', 'NY' => 'New York', 'NC' => 'North Carolina', 'ND' => 'North Dakota', 'OH' => 'Ohio', 'OK' => 'Oklahoma', 'OR' => 'Oregon', 'PA' => 'Pennsylvania', 'RI' => 'Rhode Island', 'SC' => 'South Carolina', 'SD' => 'South Dakota', 'TN' => 'Tennessee', 'TX' => 'Texas', 'UT' => 'Utah', 'VT' => 'Vermont', 'VA' => 'Virginia', 'WA' => 'Washington', 'WV' => 'West Virginia', 'WI' => 'Wisconsin', 'WY' => 'Wyoming');
?>
<div class="conf-detail hatch-bg">
  <div class="conf-top">
    <a href="/events-and-training">Events</a> > <a href="/<?php echo $root_url; ?>"> OWLS </a> > <?php echo $conference['CITY']; ?>
  </div>

  <div class="conf-block_left">
    <h4 class="red">Sign up now for the 2014-2015</h4>
    <h4 class="red">ODYSSEYWARE Regional Learning Summit!</h4>
    <h4 class="conf-city"><?php echo $conference['DISPLAY_CITY'].", ".$conference['STATE'].", ". date("F j", strtotime($conference['DATE'])).""; ?></h4>
    <div class="conf-address">
      <?php if($conference['URL_DIRECTION'] != "" || $conference['URL_DIRECTION'] != NULL): ?>
      	<a href="<?php echo $conference['URL_DIRECTION']; ?>" target="_blank"><?php echo $conference['PLACE']; ?></a><br />
      <?php else: ?>
      	<?php echo $conference['PLACE']; ?><br />
      <?php endif ?>
      <?php echo $conference['ADDRESS']; ?><br />
      <?php echo $conference['CITY'].", ".$conference['STATE']." ".$conference['ZIP']; ?><br />
      Phone: <?php echo $conference['PHONE']; ?>
    </div>

    <p>The <?php echo date('Y'); ?>-<?php echo date('Y')+1; ?> ODYSSEYWARE Regional Learning Summit promises to be an exciting and information-packed event. Participate in stimulating discussions, learn practical information, and take advantage of professional development opportunities that will leave you inspired and energized.</p>

    <p>Submit the form below to complete your registration or call <strong>877-795-8904, option 2</strong> for more information.</p>
  </div>

  <div class="conf-block_right gray-bg">
    <h4 class="red">EARLY BIRD SAVINGS!</h4>
    Register prior to <?php echo date('F d, Y', strtotime($conference['EARLY_REG_END_DATE'])); ?>
    <div class="conf-rate">
      <h4 class="red"><span class="conf-bold">$<?php echo $conference['EARLY_RATE']?></span><sub>/person</sub></h4>
    </div>
    <div class="conf-group">
      <span class="conf-bold">Group Rates</span><br />
      $<?php echo $conference['GROUP_RATE']; ?> for up to five attendees.<br />
      <span class="conf-italic">Each person thereafter is $<?php echo $conference['EARLY_RATE']?>.</span>
    </div>

    <div class="conf-reg">
      <h4>REGULAR PRICE</h4>
      <span class="conf-bold">$<?php echo $conference['REG_RATE']; ?></span>/person
      <br />
      <br />
      <span class="conf-bold">$<?php echo $conference['GROUP_RATE']?></span> for up to five attendees<br />
      <span class="conf-italic">Lunch* is included.</span><br />
      <span class="conf-small">*A 72-hour advance registration is required.</span><br />
      <span class="conf-small">Cancellation Policy: 100% refund if reservation is cancelled up to 24 hours before the start of the summit.</span>
    </div>
  </div>

  <div class="clear"></div>

  <?php if($conference['END_DATE'] != NULL): ?>
    <h3 class="fancy">DAY 1</h3>
    <h5 class="orangebox-centered">Attend the Blended Learning Track</h5>
    <p>Join the discussions about Blended Learning on day one and discover what Blended Learning is and the misconceptions that surround it. Learn about the different Blended Learning Models and strategies to successfully implement them.</p>
    <h3 class="fancy">DAY 2</h3>
    <h5 class="orangebox-centered">Half-Day Workshop</h5>
    <p>Get ready to roll up your sleeves for the half-day workshop. Choose a Blended Learning Model that is right for you. Learn how Odysseyware easily integrates into each model and build an implementation plan that you can take back to your school.</p>
  <?php endif ?>

  <h3 id="conf-register-header" class="fancy">REGISTER</h3>
    <a name="errors_occurred"></a>
    <div id="error_box"></div>

  <div class="conf-block conf-block-left">
    <form method="post" name="frm_info" id="frm_info" >

    <h4 class="conf-block-header blue">Your Information</h4>

      <input type="hidden" name='user_conference_id' value='<?php echo $conference['ID']; ?>' />

  		<input type="hidden" name="description" value="<?php echo $conference['DISPLAY_CITY'] . ' - ' . $conference['PLACE'] . ' - ' . date('l, F j, Y', strtotime($conference['DATE'])); ?> "/>

      <input type="hidden" name="slug" value="<?php echo $conference['SLUG'] ?>" />

      <div class="conf-reg-row">
        <input type="text" name="first_name" id="first_name" placeholder="First Name" />
  		</div>
  		<div class="conf-reg-row">
  			<input type="text" name="last_name" id="ff_last_name" placeholder="Last Name" />
  		</div>
  		<div class="conf-reg-row">
  			<input type="text" name="title" id="ff_title" placeholder="Position/Title" />
  		</div>
  		<div class="conf-reg-row">
  			<input type="text" name="email" id="ff_email" placeholder="Email" />
  		</div>
  		<div class="conf-reg-row">
  			<input type="text" name="school_name" id="ff_organization" placeholder="School Name" />
  		</div>
  		
  		<div class="conf-reg-row">
  			<input type="text" name="school_district" id="ff_district" placeholder="School District" />
  		</div>

  		<div id="additional-attendee" class="registration-row">
      </div>
  		<div id="add-attendee">
  			<a class="add-attendee" href="javascript:" onclick="add_another_attendee();"><span class="conf-add-attendee">+</span> Add Another Attendee</a>
  			<input type="hidden" id="attendee_count" name="attendee_count" value="0" />
  			<input type="hidden" id="attendee_row" name="attendee_row" value="0" />
  		</div>
      <div class="pb20"></div>
  </div>


  <div class="conf-block conf-block-right">
  	<h4 class="conf-block-header blue">Billing Address</h4>

  	<div class="conf-reg-row">
  		<input type="text" name="organization" id="ff_cc_org" placeholder="Organization" />
  	</div>

  	<div class="conf-reg-row">
  		<input type="text" name="billing_first_name" id="ff_cc_first_name" placeholder="First Name" />
  	</div>

  	<div class="conf-reg-row">
  		<input type="text" name="billing_last_name" id="ff_cc_last_name" placeholder="Last Name" />
  	</div>

  	<div class="conf-reg-row">
  		<input type="text" name="billing_address" id="ff_address1" placeholder="Street" />
  	</div>

  	<div class="conf-reg-row">
  		<input type="text" name="billing_city" id="ff_city" placeholder="City" />
  	</div class="registration-row">

  	<div class="conf-reg-row">
  		<select name="billing_state" id="ff_state" class="chosen-select-no-single">
  			<option value="" selected="selected">Select State</option>
  			<option disabled="disabled"></option>
  			<?php foreach($states as $key=>$val) : ?>
                      <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                       <?php endforeach; ?>
  		</select>
  	</div>

  	<div class="conf-reg-row">
  		<input type="text" name="billing_zip" id="ff_postal_code" placeholder="Zip Code" />
  	</div>

  	<input type="hidden" name="country" value="US" />
  	<div class="conf-reg-row">
  		<input type="text" name="billing_phone" id="ff_work_phone" placeholder="Phone" />
  	</div>
  </div>
  <div class="clear"></div>

  <div class="conf-block conf-block-left">
  	<h4 class="conf-block-header blue">Payment Options</h4>

	<div class="conf-reg-row">
		<select name="payment_type" id="ff_payment_option" class="chosen-select-no-single">
			<option selected="selected">SELECT PAYMENT TYPE</option>
			<option value="PO">Purchase Order</option>
			<option value="CC">Credit Card</option>
		</select>
	</div>
	

	<div id="credit" style="display:none">
		<div>
			<span class="conf-small">
			  Once you have completed and submitted your registration, please call customer service to make your payment with a credit card at 877-795-8904 Option 6.
			</span>
  	</div>
  </div>


  <div id="po" style="display:block;" class="conf-reg-row">
	  <div>
  		<input type="text" name="po_number" id="ff_po_number" placeholder="Purchase Order No."/>
  	</div>
	  <div class="clear"></div>
	<div>

	<span class="conf-small">
	  If paying by purchase order, please fax a copy to 866-465-1954. Paperwork must be received no later than 3 days after registration.
	</span>
	</div>
</div>
  			<div class="clear"></div>
  			<div class="conf-total">
    			<?php if(date('Y-m-d') <= $conference['EARLY_REG_END_DATE']): ?>
    		    Total: <strong><span id="total_cost_show">$<?php echo number_format($conference['EARLY_RATE'], 2);?></span></strong>
    		    <input type="hidden" id="total_cost" name="cost" value="<?php echo number_format($conference['EARLY_RATE'], 2); ?>" />
    		  <?php else: ?>
    		    Total: <strong><span id="total_cost_show">$<?php echo number_format($conference['REG_RATE'], 2);?></span></strong>
    		    <input type="hidden" id="total_cost" name="cost" value="<?php echo number_format($conference['REG_RATE'], 2); ?>" />
    		  <?php endif ?>
    		</div>
  </div>
  <div class="conf-block conf-block-right">
  	<input type="submit" alt="Register Now!" id="register" class="conf-button-blue" value="REGISTER"/>
  </div>
  <div class="clear"></div>

  <a href="#errors_occurred" id="error_link"></a>
  </form>
</div>

<script>

jQuery(document).ready(function($){
  $('#po').hide();
  $('#credit').hide();
	$('#ff_payment_option').change(function(){
  	if($(this).val() == 'CC'){
  		$('#po').hide();
  		$('#credit').show();
  	} else {
  		$('#credit').hide();
  		$('#po').show();
  	}
  })

	$('.chosen-select-no-single').chosen('disable_search_threshold', 10);
})

var val_array = [{
    name: 'first_name',
    display: 'First Name',    
    rules: 'required'
}, {
    name: 'last_name',
    display: 'Last Name',    
    rules: 'required'
}, {
    name: 'title',
    display: 'Position Title',    
    rules: 'required'
}, {
    name: 'email',
    display: 'Email',
    rules: 'required|valid_email'
}, {
    name: 'school_name',
    display: 'School Name',
    rules: 'required'
}, {
    name: 'school_district',
    display: 'School District',
}, {
    name: 'organization',
    display: 'Billing Organization',
    rules: 'required'
}, {
    name: 'billing_first_name',
    display: 'Billing First Name',
    rules: 'required'
}, {
    name: 'billing_last_name',
    display: 'Billing Last Name',
    rules: 'required'
}, {
    name: 'billing_address',
    display: 'Billing Street',
    rules: 'required'
}, {
    name: 'billing_city',
    display: 'Billing City',
    rules: 'required'
}, {
    name: 'billing_state',
    display: 'Billing State',
    rules: 'required'
}, {
    name: 'billing_zip',
    display: 'Billing Zip Code',
    rules: 'required'
}, {
    name: 'billing_phone',
    display: 'Billing Phone',
    rules: 'required'
}];

var validator = new FormValidator('frm_info', val_array , function(errors, event) {

  var error = false;

  var has_attendee_message = false;
  if (errors.length > 0) {        
    var errorString = '';
    for (var i = 0, errorLength = errors.length; i < errorLength; i++) {
      document.getElementById(errors[i].id).className = 'error';
      document.getElementById(errors[i].id).setAttribute("placeholder", errors[i].message);
        errorString += errors[i].message + '<br />';
    }
    error = true;                                                  
  }

  //var form_data = jQuery('#frm_info').serialize();
  if(error == false){
    
    jQuery.post('/events/learning-summits/submit', jQuery('#frm_info').serialize(), function(data){
      window.location ='/events/learning-summits';
    });
    
    alert('Thank You! You have been successfully registered for the conference.');
    
    //
    // jQuery.ajax({
    //   type: "POST",
    //   url: "/events/learning-summits/submit",
    //   data: jQuery('#frm_info').serialize(),
    //   success: function(data){
    //     console.log(data);
    //     if(data == "OK"){
    //       window.location('/events/learning-summits');
    //     }
    //   }
    // });
    return false;
  } else {
        // do nothing
  }                                               
      return !error;
});


//attendee math stuffs

var attendee_count = 1;

jQuery(document).ready( function($){   


  $("rate_type_single").bind('click', function(){





    document.getElementById("groupattendees").style.display = 'none';


    $("total_cost").value = "<?php echo number_format($conference['REG_RATE'], 2); ?>";


    document.getElementById('total_cost_show').innerHTML = '<?php echo number_format($conference['REG_RATE'], 2); ?>';


  });





  $("rate_type_group").bind('click', function(){            


    document.getElementById("groupattendees").style.display = 'block';


    $("total_cost").value = "<?php echo number_format($conference['GROUP_RATE'], 2); ?>";            


    document.getElementById('total_cost_show').innerHTML = '<?php echo number_format($conference['GROUP_RATE'], 2); ?>';


  })


});





function add_another_attendee() {


  row = document.getElementById('attendee_row').value = parseInt(document.getElementById('attendee_row').value) +1;


  attendee_rows = document.getElementById('attendee_count').value = parseInt(document.getElementById('attendee_count').value) +1;


  var wrap = document.getElementById('additional-attendee');


  var newdiv = document.createElement('div');


  var divIdName = 'rowitemattendee'+row;


  var divClassName = 'attendeerow';


  newdiv.setAttribute('id',divIdName);


  newdiv.setAttribute('class', divClassName);


  


  var content = '<div style="margin:0">';	


                                


  content += '<div class="clear"></div>';


                            


  content += '<input name="attendees[]" id="attendee_' + row + '" type="text" value="" placeholder="First Name / Last Name" />';


  content += '<div class="remove-attendee-wrapper">';


                          


  content += '<a class="remove-attendee" href="javascript:" onclick="remove_attendee(\'rowitemattendee' + row + '\');"><span class="conf-remove-attendee">-</span> Remove</a>';


  content += '</div>';


  content += '<div class="clear"></div>';


  content += '</div>';





  newdiv.innerHTML = content;


  wrap.appendChild(newdiv);





  var field = { name: 'attendees[]',


			id: 'attendee_' + row,


      display: 'Every selected attendee name',


      rules: 'required'};





  calculate_total(attendee_rows);





}





function remove_attendee(obj) {


  row = document.getElementById('attendee_row').value;	


  var d = document.getElementById('additional-attendee');


  var olddiv = document.getElementById(obj);


  d.removeChild(olddiv);


  attendee_rows = document.getElementById('attendee_count').value = parseInt(document.getElementById('attendee_count').value) - 1;





  var rownum = obj.replace("rowitemattendee", "");


	


  calculate_total(attendee_rows);





}





function calculate_total(attendee_count)


{





  <?php


    //calculate conference rates here, in case early bird end date was crossed


    if(date('Y-m-d') <= $conference['EARLY_REG_END_DATE'])


    {


      //early rate


      $single_rate = $conference['EARLY_RATE'];


      $rate_type = 'early';


    }


    else


    {


      //regular rate


      $single_rate = $conference['REG_RATE'];


      $rate_type = 'regular';


    }





    $group_rate = $conference['GROUP_RATE'];





    //5 people or above get the group price; so, this would be 4 additional attendees for 5 attendees total


    $attendee_threshold = 4;





  ?>


  if(attendee_count < <?php echo $attendee_threshold; ?>)


  {


    //use single rate


    total = parseFloat(<?php echo number_format($single_rate, 2); ?> + (<?php echo number_format($single_rate, 2); ?> * attendee_count));


  }


  else


  {


    per_group = <?php echo number_format($group_rate, 2); ?>;


    if((attendee_count + 1) > 5)


    {


      //per_person = parseFloat(per_group/5).toFixed(2);


      //total = parseFloat(per_person * (attendee_count + 1));


                        


      per_person = <?php echo number_format($single_rate, 2); ?>;


      total = parseFloat(per_group+(per_person*((attendee_count + 1)- 5)));


    }


    else


    {


      total = per_group;


    }


  }





  jQuery("#total_cost").val(parseFloat(total).toFixed(2));





  jQuery('#total_cost_show').html(parseFloat(total).toFixed(2));





} 


                  


</script>

