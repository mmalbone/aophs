<?php
/**
 * THIS TEMPLATE SHOWS THE WEBINAR SERIES TITLE AND LONG DESCRIPTION
 * @param $series array An array containing the series information
 * @author Steven Goodman
 */
?>
<h3><?php echo $series['title']; ?></h3>
<p><?php echo $series['description']; ?></p>
