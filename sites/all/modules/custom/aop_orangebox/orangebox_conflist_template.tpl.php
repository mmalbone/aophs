<?php

/**
 * @file
 * 
 *
 * Available variables:
 * - $content: All aggregator content.
 * - $page: Pager links rendered through theme_pager().
 *
 * @see template_preprocess()
 * @see template_preprocess_aggregator_wrapper()
 *
 * @ingroup themeable
 */

?>

<?php if ( count($conf_current) ) : ?>
<h3 class="fancy"><?php echo date('Y'); ?></h3>
 <table border="0" class="orangebox-conf-table">
    <tbody>
      <?php foreach ($conf_current as $conference) : ?>
        <tr>
          <td class="orangebox-conf-table-location"><?php echo $conference[ 'DISPLAY_CITY' ]; ?>, <?php echo $conference['STATE']; ?></td>
          <td class="orangebox-conf-table-date"><?php echo date("F j", strtotime($conference['DATE'])); ?><?php echo ($conference['END_DATE'] != NULL ? ' & '.date("j", strtotime($conference['END_DATE'])) : ''); ?></td>
          <td class="orangebox-conf-table-special"><?php echo ($conference['END_DATE'] != NULL ? '<span class="red">Two Day Conference!</span> ' : ''); ?></td>                 
          <td class="orangebox-conf-table-reg"><a href="<?php echo '/' . $root_url . '/' .$ext . '/' . $conference[ 'SLUG' ]; ?>">Register</a></td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else : ?>
  <p>Currently no <?php echo date('Y'); ?> user conferences are scheduled. Please check back since conferences are added regularly.</p>
<?php endif; ?>


<?php if ( count($conf_next) ) : ?>
<h3 class="fancy"><?php echo date('Y')+1; ?></h3>
 <table border="0" cellspacing="0" cellpadding="0" class="orangebox-conf-table">
    <tbody>
      <?php foreach ($conf_next as $conference) : ?>
        <tr>
          <td class="orangebox-conf-table-location"><?php echo $conference[ 'DISPLAY_CITY' ]; ?>, <?php echo $conference['STATE']; ?></td>
          <td class="orangebox-conf-table-date"><?php echo date("F j", strtotime($conference['DATE'])); ?></td>          
          <td class="orangebox-conf-table-special"><?php echo ($conference['END_DATE'] != NULL ? 'and '.date("F j", strtotime($conference['END_DATE'])) : ''); ?></td>                 
          <td class="orangebox-conf-table-reg"><a href="<?php echo '/' . $root_url . '/' .$ext . '/' . $conference[ 'SLUG' ]; ?>">Register</a></td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else : ?>
  <p>Currently no <?php echo date('Y')+1; ?> user conferences are scheduled. Please check back since conferences are added regularly.</p>
<?php endif; ?>
