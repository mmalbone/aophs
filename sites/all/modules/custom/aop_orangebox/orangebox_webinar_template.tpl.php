<?php

/**
 * @file
 * 
 *
 * Available variables:
 * - $content: All aggregator content.
 * - $page: Pager links rendered through theme_pager().
 *
 * @see template_preprocess()
 * @see template_preprocess_aggregator_wrapper()
 *
 * @ingroup themeable
 */

//print_r($itemlist);
?>

<?php if ( count($itemlist) ) : ?>

<h1 class="red-bg pane-title">Upcoming Webinars</h1>
 <table class="orangebox-conf-table" border="0">
    <tbody>
      <?php foreach ( $itemlist as $item ) : ?>
      
        <tr align="left">
        <?php if($item['TITLE'] != '' || $item['TITLE'] != NULL): ?>
          <td class="orangebox-conf-table-location"><?php echo $item[ 'TITLE' ]; ?></td>
          <?php endif ?>
          <td><?php echo date("D F j, Y - ga", strtotime($item['WEBINAR_DATE'])); ?></td>
          <td class="orangebox-conf-table-reg"><a href="<?php echo $item[ 'ACCESS_URL' ]; ?>" target="_blank">Register</a></td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
  
  <div style="height: 25px"></div>
  
  <?php if (isset($archives) && count($archives) > 0) : ?>
  <h3 class="fancy">Webinar Archives</h3>
  <table class="orangebox-conf-table" id="sortabletable" border="0" cellspacing="0" cellpadding="0" width="100%">

    <tbody>
    <?php foreach ( $archives as $item ) : ?>

    <tr align="left">
    <?php if($item['TITLE'] != '' || $item['TITLE'] != NULL): ?>
      <td class="orangebox-conf-table-location"><?php echo $item[ 'TITLE' ]; ?></td>
    <?php endif ?>
      <td><?php echo date("F j, Y", strtotime($item['WEBINAR_DATE'])); ?></td>
      <td class="orangebox-conf-table-reg"><a href="<?php echo $item[ 'ACCESS_URL' ]; ?>" target="_blank">Play</a></td>
    </tr>
  <?php endforeach; ?>
    </tbody>
  </table>
  <?php endif; ?>
  
  
<?php else: ?>

<h1 class="red-bg pane-title">Upcoming Webinars</h1>
<p>There are no webinars currently scheduled</p>
  <div style="height: 10px"></div>
  
  <?php if (isset($archives) && count($archives) > 0) : ?>
  <h3 class="fancy">Webinar Archives</h3>
  <table class="orangebox-conf-table" id="sortabletable" border="0" cellspacing="0" cellpadding="0" width="100%">

    <tbody>
    <?php foreach ( $archives as $item ) : ?>

    <tr align="left">
    <?php if($item['TITLE'] != '' || $item['TITLE'] !== NULL): ?>
      <td class="orangebox-conf-table-location"><?php echo $item[ 'TITLE' ]; ?></td>
    <?php endif ?>
      <td><?php echo date("F j, Y", strtotime($item['WEBINAR_DATE'])); ?></td>
      <td class="orangebox-conf-table-reg"><a href="<?php echo $item[ 'ACCESS_URL' ]; ?>" target="_blank">Play</a></td>
    </tr>
  <?php endforeach; ?>
    </tbody>
  </table>
  <?php endif; ?>
<?php endif; ?>

