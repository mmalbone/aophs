 (function($) {
    Drupal.ajax.prototype.commands.afterAddressValidation = function(ajax, response, status) {
        $('.error.message').remove();
        $('.error').removeClass('error');

        // Delete both shipping and billing phone number errors from validate package, then if the error object is empty, delete it as well.
        if (response.errors) delete response.errors['customer_profile_shipping][field_shipping_phone][und][0'];
        if (response.errors) delete response.errors['customer_profile_billing][field_billing_phone][und][0'];
        if( jQuery.isEmptyObject( response.errors ) ) delete response.errors;

        if (response.errors) {
            var continue_btn = $('input.checkout-continue.checkout-processed');
            $(continue_btn[0]).show().attr('disabled', false);
            $(continue_btn[1]).remove();
            $('span.checkout-processing').addClass('element-invisible');
            commerce_avatax_handle_errors(response.errors);
            return;
        }
        if (response.validation_result === false) {
            // This should never happen!
            var continue_btn = $('input.checkout-continue.checkout-processed');
            $(continue_btn[0]).show().attr('disabled', false);
            $(continue_btn[1]).remove();
            $('span.checkout-processing').addClass('element-invisible');
            commerce_avatax_attach_events();
            return;
        }
        if (response.validation_result.result != 'valid') {
            var continue_btn = $('input.checkout-continue.checkout-processed');
            $(continue_btn[0]).show().attr('disabled', false);
            $(continue_btn[1]).remove();
            $('span.checkout-processing').addClass('element-invisible');
            var buttons = [];
            if (response.validation_result.result == 'needs correction') {
                buttons = [
                    {
                        text: "Use recommended",
                        click: function() {
                            $('form.commerce_checkout_form').removeClass('commerce_avatax_revalidate_addr');
                            var $continue_btn = $('input.checkout-continue');
                            $continue_btn.clone().insertAfter($continue_btn).attr('disabled', true).next().removeClass('element-invisible');
                            $continue_btn.hide();
                            var selected = jQuery('#address_validation_wrapper .form-type-radios.form-item-addresses input[name="addresses"]').val();
                            var address = response.validation_result.suggestions[selected];
                            $('fieldset.' + Drupal.settings.commerce_avatax.commerce_avatax_address_validation_profile  + ' select.country').val(address.country);
                            $('fieldset.' + Drupal.settings.commerce_avatax.commerce_avatax_address_validation_profile  + ' input.thoroughfare').val(address.line1);
                            $('fieldset.' + Drupal.settings.commerce_avatax.commerce_avatax_address_validation_profile  + ' input.permise').val(address.line2);
                            $('fieldset.' + Drupal.settings.commerce_avatax.commerce_avatax_address_validation_profile  + ' input.locality').val(address.city);
                            if ($('fieldset.' + Drupal.settings.commerce_avatax.commerce_avatax_address_validation_profile  + ' select.administrative-area')) {
                                $('fieldset.' + Drupal.settings.commerce_avatax.commerce_avatax_address_validation_profile  + ' select.administrative-area').val(address.state);
                            }
                            $('fieldset.' + Drupal.settings.commerce_avatax.commerce_avatax_address_validation_profile  + ' input.postal-code').val(address.postal_code);
                            $(this).dialog("close");
                            $('form.commerce_checkout_form').submit();
                        }
                    },
                    {
                        text: "Use as entered",
                        click: function() {
                            $('form.commerce_checkout_form').removeClass('commerce_avatax_revalidate_addr');
                            var $continue_btn = $('input.checkout-continue');
                            $continue_btn.clone().insertAfter($continue_btn).attr('disabled', true).next().removeClass('element-invisible');
                            $continue_btn.hide();
                            $(this).dialog('close');
                            $('form.commerce_checkout_form').submit();
                        }
                    },
                    {
                        text: "Enter again",
                        click: function() {
                            $(this).dialog('close');
                            $('input.checkout-continue').unbind('click').click(function() {
                                var $this = $(this);
                                $this.clone().insertAfter($this).attr('disabled', true).next().removeClass('element-invisible');
                                $this.hide();
                            });
                            commerce_avatax_attach_events();
                        }
                    }
                ]
            }
            else if (response.validation_result.result == 'invalid') {
                buttons = [
                    {
                        text: "Let me change the address",
                        click: function() {
                            $(this).dialog("close");
                            $('input.checkout-continue').unbind('click').click(function() {
                                var $this = $(this);
                                $this.clone().insertAfter($this).attr('disabled', true).next().removeClass('element-invisible');
                                $this.hide();
                                commerce_avatax_attach_events();
                            });
                        }
                    },
                    {
                        text: "Use the address anyway",
                        click: function() {
                            $('form.commerce_checkout_form').removeClass('commerce_avatax_revalidate_addr');
                            var $continue_btn = $('input.checkout-continue');
                            $continue_btn.clone().insertAfter($continue_btn).attr('disabled', true).next().removeClass('element-invisible');
                            $continue_btn.hide();
                            $('form.commerce_checkout_form').submit();
                        }
                    }
                ]
            }
            $("#address_validation_wrapper").html(response.validation_result.msg);
            $("#address_validation_wrapper").dialog({
                height: 500,
                width: 800,
                modal: true,
                title: Drupal.t('Confirm your shipping address'),
                resizable: false,
                draggable: false,
                buttons: buttons,
                dialogClass: 'no-close',
                closeOnEscape: false
            });
            if (!$("#address_validation_wrapper").dialog('isOpen')) {
                $("#address_validation_wrapper").dialog('open');
            }
        }
        else {
            if (Drupal.settings.commerce_avatax.commerce_avatax_autocomplete_postal_code) {
                var address = response.validation_result.suggestions[0];
                $('fieldset.' + Drupal.settings.commerce_avatax.commerce_avatax_address_validation_profile  + ' input.postal-code').val(address.postal_code);
            }
            $('form.commerce_checkout_form').submit();
        }
    };
}(jQuery));
