(function ($) {
  Drupal.behaviors.aop_curriculum_guide = {
    attach: function (context, settings) {
  // Add class curriculum-block-wrapper to all blocks to use as block selection later.  
  $('.monarch-curriculum, .switch-on-curriculum, .lifepac-curriculum, .horizon-curriculum, .the-weaver-curriculum').addClass("curriculum-block-wrapper");
  
  //Compute the desired width for blocks (used for responsiveness)//////////////
  
  var fullBlockSize = '61%';
  var smallBlockSize = '29%';
  
  function setBlockSize(){
	  if($(window).width() < 998) {
	  	fullBlockSize  = '90%';
	  	smallBlockSize = '45%';
	  	
	  	if($(window).width() < 695) {
	  	smallBlockSize = '93%';
	  	}
	  } else {
	  fullBlockSize = '61%';
	  smallBlockSize = '29%';
	  }
  }
  
  //Call function block size.
  
  setBlockSize();
  
  ///////////////////////////////////////
  
  var resetAllCurriculum = function() {
  	// We need to reload the curriculum finder to its original state
     $('.monarch-curriculum, .switch-on-curriculum, .lifepac-curriculum, .horizon-curriculum, .the-weaver-curriculum').removeClass('not_possible_selection');
     setBlockSize();
  }
    
	var resetCurriculumList = function() {
	setBlockSize();
		// Reset all curriculum boxes to default
		$('.monarch-curriculum, .lifepac-curriculum, .horizon-curriculum, .switch-on-curriculum, .the-weaver-curriculum').css({width: smallBlockSize,backgroundImage:'', border:"none"});
		$('.curriculum-title,.curriculum-grade,.curriculum-more-info-summary').removeClass("color_black").css({margin:0,fontStyle: "normal", color:"#FFFFFF"});
		$(".curriculum-full-description").hide();
		$('.curriculum-media').hide();
		$('.curriculum_close_detail, .curriculum-detail-logo, .curriculum-detail-image').hide();
		$('.curriculum-detail-left').css({width: "100%"});
		$('.curriculum-title').css({paddingTop: "40%"});
	}
    
    // Automatically expand the detailed view of the last possible selection
  var notPosibleArrayCount = 0;
  var checkedClasses = [];
  var expandSelected = '';
  
  var autoExpandDetails = function() {
    
    var defaultClassesArray = [".monarch-curriculum", ".switch-on-curriculum", ".lifepac-curriculum", ".horizon-curriculum", ".the-weaver-curriculum"];
    
    $.each(defaultClassesArray, function( index, value ) {
      if ($(value).hasClass('not_possible_selection')) {
    
      	if( $.inArray( value, checkedClasses ) ==-1 ) {
    
    	    notPosibleArrayCount++;
    	    checkedClasses.push(value);
    	    
        	if ($.inArray( value, checkedClasses ) !==-1){
    
    		    if (notPosibleArrayCount == 4) {
    		    	var selectedClass = $(defaultClassesArray).not(checkedClasses).get();
    
    					if (!$(selectedClass[0]).hasClass("not_possible_selection")) {
    					    resetCurriculumList();
    						
    						// Apply effect to selected item
    						$(selectedClass[0]).animate({width:'60%'}, 'slow').css({backgroundImage:'none', border:"6px solid #6ac1d6", });
    						$(selectedClass[0]).find('.curriculum-full-description, .curriculum_close_detail, .curriculum-detail-logo, .curriculum-detail-image').show();
    						$(selectedClass[0]).find('.curriculum-detail-left').css({width: "60%"});		
    						$(selectedClass[0]).find('.curriculum-media').show();		
    						$(selectedClass[0]).find('.curriculum-title,.curriculum-grade,.curriculum-more-info-summary').addClass("color_black").css({marginLeft: "30px", color:"#222222"});
    						$(selectedClass[0]).find('.curriculum-title').css({paddingTop: "165px"});
    						$(selectedClass[0]).find('.curriculum-more-info-summary').css({fontStyle: "italic"});åå
                return false;
    					}
    		    }
        	}
    		}
      }
    });
		
  }
  

  // Open a selected block and show more information
	$('.curriculum-block-wrapper').click(function() {
		if ($(this).hasClass("curriculum-block-wrapper-open")) {
		$(this).removeClass("curriculum-block-wrapper-open");
		
		
		} else {
  		$(".curriculum-block-wrapper").removeClass("curriculum-block-wrapper-open");
  		// Reset all to default
  		resetCurriculumList();
  		// Apply effect to selected item
  		$(this).css({backgroundImage:'none', border:"6px solid #6ac1d6", width:fullBlockSize});
  		$(this).find('.curriculum-full-description, .curriculum_close_detail, .curriculum-detail-logo, .curriculum-detail-image').fadeIn(400);			
  		$(this).find('.curriculum-detail-left').css({width: "60%"});			
  		$(this).find('.curriculum-title,.curriculum-grade,.curriculum-more-info-summary').addClass("color_black").css({marginLeft: "30px", color:"#222222"});
  		$(this).find('.curriculum-media').show();
  		$(this).find('.curriculum-title').css({paddingTop: "165px"});
  		$(this).find('.curriculum-more-info-summary').css({fontStyle: "italic"});
  		$(this).addClass("curriculum-block-wrapper-open");
  		
  		// Clear all transparent overlays
      $('.monarch-curriculum, .switch-on-curriculum, .lifepac-curriculum, .horizon-curriculum, .the-weaver-curriculum').removeClass('not_possible_selection');  		
		}

	});
	
	$('.curriculum_close_detail').click(function() {
	
		// Reset all to default
		resetCurriculumList();
		resetAllCurriculum();
	});

// Start curriculum finder

//on start call function 
resetCurriculumList();
resetAllCurriculum();

//on window resize call function
var width = $(window).width();
$(window).resize(function(){
if($(window).width() != width){
	setBlockSize();
	resetCurriculumList();
	resetAllCurriculum();
	width = $(window).width();
}
});
	
	//Shows a block
	$.fn.showFacBlock = function() {
	this.fadeIn(100);
	return this;
	}
	
	//Hides a block
	$.fn.hideFacBlock = function(callback) {
		        // Do something to each element here.
		this.fadeOut(200, function() {
			//Callback function
			if (typeof callback == 'function') { // make sure the callback is a function
			        callback.call(this); // brings the scope to the callback
			}
		});
	return this;
	}
	
	//Makes a past block active
	$.fn.makeFacActive = function(choiceText, choiceTarget, callback) {
	this.addClass('fac-step-block-active');
	this.find('.fac-step-choice').hide().html(choiceText).fadeIn(200);
		this.animate({'border-right' : '4px solid #E1E6E8'}, 200, function() {
			//Callback function
			if (typeof callback == 'function') { // make sure the callback is a function
			        callback.call(this); // brings the scope to the callback
			}
		});
	//set data attribute
	$(this).data('data-choice-target', choiceTarget);
	return this;
	}
	
	//Makes a block inactive
	$.fn.makeFacInactive = function(callback) {
	this.removeClass('fac-step-block-active');
	this.find('.fac-step-choice').fadeOut(200, function(){
		$(this).html('');
	});
		this.animate({'border-right' : '0px'}, 200, function() {
			//Callback function
			if (typeof callback == 'function') { // make sure the callback is a function
			        callback.call(this); // brings the scope to the callback
			}
		});
	return this;
	}
	
	function mobile_button_show(){
	$('.fac-mobile-reset').fadeIn();
	}
	
	function fac_get_started(){
	$('#fac-step-one').removeClass('fac-step-block-active');
	$('.fac-get-started-text').fadeOut(300);
	$('.fac-get-started-btn').fadeOut(200);
	$('.fac-get-started-block').animate({width: '0px', 'padding-left' : '0px', 'padding-right' : '0px', 'border-right-width' : '0px'}, 500, function(){
	$('#fac-choice-print-digital').showFacBlock();
	});
	$('#fac-step-one').removeClass('fac-step-block-get-started');
	}
	
	
	//Moves the resulting product block to the top of the list.
	
	function productToBeginning(blockId){
		//Prepend the block to the beginning.
		
		innerDiv = $('.fac-products-wrapper').children();
		$( '#' + blockId + '-product-block').prependTo(innerDiv);
	}
	
	//Choice functions
	
	$('#fac-step-one').click(function(){
		
		if($(this).hasClass('fac-step-block-get-started')) {
		fac_get_started();
		}
		
		if($(this).hasClass('fac-step-block-active')) {
		//Block is active
		dataChoiceTarget = $(this).data('data-choice-target');
		
		$('.fac-choice-block:visible, .fac-result-block:visible, #fac-step-two:visible, #fac-step-three:visible').fadeOut(200, function(){
			$('#fac-step-one').makeFacInactive(function(){
				$('#' + dataChoiceTarget).showFacBlock();
				$('#fac-step-two, #fac-step-three').makeFacInactive();
			});
		});
		}
		
		//Hide Mobile Button
		$('.fac-mobile-reset').fadeOut();
	});
	
	$('#fac-step-two').click(function(){
		if($(this).hasClass('fac-step-block-active')) {
		//Block is active
		dataChoiceTarget = $(this).data('data-choice-target');
		
		$('.fac-choice-block:visible, .fac-result-block:visible, #fac-step-three:visible').fadeOut(200, function(){
			$('#fac-step-two').makeFacInactive(function(){
				$('#' + dataChoiceTarget).showFacBlock();
				$('#fac-step-three').makeFacInactive();
			});
		});
		}
	});
	
	$('#fac-step-three').click(function(){
		if($(this).hasClass('fac-step-block-active')) {
		//Block is active
		dataChoiceTarget = $(this).data('data-choice-target');
		
		$('.fac-choice-block:visible, .fac-result-block:visible').fadeOut(300, function(){
			$('#fac-step-three').makeFacInactive(function(){
				$('#' + dataChoiceTarget).showFacBlock();
				$('#fac-step-three').makeFacInactive();
			});
		});
		}
	});
	
	
	//Choice Button Functions
	

	//Click get started.
	$('.fac-get-started-block').click(function(){
	fac_get_started();
	
	});
	
	
	//Choose Print
	$('#choice-option-print').click(function(){
		$('.fac-choice-block:visible').hideFacBlock(function(){
			$('#fac-step-one').makeFacActive('Print', 'fac-choice-print-digital', function(){
				$('#fac-step-two').showFacBlock();
				$('#fac-choice-teacher-student').showFacBlock();
			});
		});
		
		//mobile button
		mobile_button_show();
	});
	
	//Choose Digital
	$('#choice-option-digital').click(function(){
	$('.fac-choice-block:visible').hideFacBlock();
		$('#fac-step-one').makeFacActive('Digital', 'fac-choice-print-digital', function(){
			$('#fac-step-two').showFacBlock();
			$('#fac-choice-online').showFacBlock();
		});
		
		//mobile button
		$('.fac-mobile-reset').fadeIn();
	});
	
	//Choose Teacher-led
	$('#choice-option-teacher-led').click(function(){
		$('.fac-choice-block:visible').hideFacBlock(function(){
			$('#fac-step-two').makeFacActive('Teacher-led', 'fac-choice-teacher-student', function(){
				$('#fac-step-three').showFacBlock();
				$('#fac-choice-unit-studies').showFacBlock();
			});
		});
	});
	
	//Choose Student Paced
	$('#choice-option-student-paced').click(function(){
		$('.fac-choice-block:visible').hideFacBlock(function(){
			$('#fac-step-two').makeFacActive('Student Paced', 'fac-choice-teacher-student', function(){
				//The user has chosen LIFEPAC
				$('#fac-result-lifepac').showFacBlock();
				$('.lifepac-curriculum').click();
				productToBeginning('lifepac');
				
			});
		});
	});
	
	//Choose Unit Studies Yes
	$('#choice-option-unit-yes').click(function(){
		$('.fac-choice-block:visible').hideFacBlock(function(){
			$('#fac-step-three').makeFacActive('Customized Lessons', 'fac-choice-unit-studies', function(){
				//The user has chosen Weaver
				$('#fac-result-weaver').showFacBlock();
				$('.the-weaver-curriculum').click();
				productToBeginning('weaver');
			});
		});
	});
	
	//Choose Unit Studies No
	$('#choice-option-unit-no').click(function(){
		$('.fac-choice-block:visible').hideFacBlock(function(){
			$('#fac-step-three').makeFacActive('Planned Lessons', 'fac-choice-unit-studies', function(){
				//The user has chosen Horizons
				$('#fac-result-horizons').showFacBlock();
				$('.horizon-curriculum').click();
				productToBeginning('horizons');
			});
		});
	});
	
	
	//Choose Online Yes
	$('#choice-option-online-yes').click(function(){
		$('.fac-choice-block:visible').hideFacBlock(function(){
			$('#fac-step-two').makeFacActive('Online Curriculum', 'fac-choice-online', function(){
				$('#fac-step-three').showFacBlock();
				$('#fac-choice-academy').showFacBlock();
			});
		});
	});
	
	//Choose Online No
	$('#choice-option-online-no').click(function(){
		$('.fac-choice-block:visible').hideFacBlock(function(){
			$('#fac-step-two').makeFacActive('Curriculum Not Online', 'fac-choice-online', function(){
				//The user has chosen SOS
				$('#fac-result-sos').showFacBlock();
				$('.switch-on-curriculum').click();
				productToBeginning('sos');
			});
		});
	});
	
	//Choose Academy Yes
	$('#choice-option-academy-yes').click(function(){
		$('.fac-choice-block:visible').hideFacBlock(function(){
			$('#fac-step-three').makeFacActive('Enroll Online', 'fac-choice-academy', function(){
				//The user has chosen AOA
				$('#fac-result-aoa').showFacBlock();
			});
		});
	});
	
	//Choose Academy No
	$('#choice-option-academy-no').click(function(){
		$('.fac-choice-block:visible').hideFacBlock(function(){
			$('#fac-step-three').makeFacActive('No Enrollment', 'fac-choice-academy', function(){
				//The user has chosen Monarch
				$('#fac-result-monarch').showFacBlock();
				$('.monarch-curriculum').click();
				productToBeginning('monarch');
			});
		});
	});
	
	//Mobile Button
	
	$('.fac-mobile-reset').click(function(){
		//Reset the curriculum finder.
		$('#fac-step-one').click();
	
	});
	
	
	/////////////////////////////////////

    }
  };
})(jQuery);