/**
 * @file custom_module.js
 * Custom Module main JavaScript file.
 *
 * You will need to get rid of the comments.
 * For how to add this file to your module, see https://drupal.org/node/304255
 */
(function ($) {
$(document).ready(function() {
	
	//Focus on first input
	$("#edit-create-username").focus();
	
	
	//Login user
	function validateFinishUser(){
	    username = $('#edit-create-username').val();
	    var userReg = /^[0-9 A-Za-z._'-]+$/;
	    if( !userReg.test( username ) || username.length < 3) {
	    	setValidation('#edit-create-username', 0);
	      return false;
	    } else {
	    	setValidation('#edit-create-username', 1);
	      return true;
	    }
	}
	
	//Password validation function
	function validateFinishPass(){
		pass = $('#edit-create-password').val();
	    if(pass.length > 5) {
	    setValidation('#edit-create-password', 1);
	    return true;
	    } else {
	    setValidation('#edit-create-password', 0);
	    return false;
	    }
	}
	
	//Confirm password validation function
	function validateFinishConfirm(){
		pass = $('#edit-confirm-password').val();
		
		
		
		if(pass == $('#edit-create-password').val() && validateFinishPass(pass)) {
		setValidation('#edit-confirm-password', 1);
		return true;
		} else {
			if(pass != "") {
			setValidation('#edit-confirm-password', 0);
			return false;
			} else {
			removeValidation('#edit-confirm-password');
			return false;
			}
		}
	}
	
	//Username
	 $('#edit-create-username').keyup(function(){
	 	validateFinishUser();
	 });
	 
	 //Password
	  $('#edit-create-password').keyup(function(){
	  	validateFinishConfirm();
	  	validateFinishPass();
	  });
	  
	  $('#edit-confirm-password').keyup(function(){
	  	validateFinishPass();
	  	validateFinishConfirm();
	  });
	  
	  
	  //Live validate username
	  function setValidation(field, valid){
	  	if(valid == 1) {
	  	$(field).removeClass("register-input-invalid").addClass("register-input-valid");
	  	} else {
	  	$(field).removeClass("register-input-valid").addClass("register-input-invalid");
	  	}
	  }
	  
	  function removeValidation(field){
	  //remove visual validation
	  $(field).removeClass("register-input-invalid").removeClass("register-input-valid");
	  }


}); 
})(jQuery);