<?php
/*
///////
CUSTOM FINISH ACCOUNT CLASS
///////
*/

//Validates that the user is returning from the email link.
class FinishAccount {

	//creates the temporary user and sends the email
	public static function createTempCustomer($userArr){
	
		$errors = array();

		//Check if an email is in use.
		if($user = user_load_by_mail($userArr['email'])){
			$errors[] = "User already exists";
		}
		  //set up the email field
		  $fields = array(
		    'name' => self::generateHash($userArr['email']),
		    'mail' => $userArr['email'],
		    'pass' => self::generateHash("aop" . $userArr['email'] . "aop123"),
		    'status' => 1,
    		'init' => 'email address',
    		'roles' => array(
      			DRUPAL_AUTHENTICATED_RID => 'authenticated user',
    			),
		    );

		  //Remove email from user array.
		  $email = $userArr['email'];
		  unset($userArr['email']);

		  //Loop through user Array and assign values.
		  foreach ($userArr as $key => $value) {
		  	$fields[$key] =  array(
				LANGUAGE_NONE => array(
				  0 => array(
				    'value' => $userArr[$key]
				  )
				)
			);
		  }


		 //if there are no errors, save the user.
		 if(empty($errors) && self::sendFinishEmail($email, $userArr['field_firstname'])) {
		  //the first parameter is left blank so a new user is created
		  $account = user_save('', $fields);
		  return $account;
		 } else {
		 	return false;
		 }
	}

	//sends a finish your account email
	public static function sendFinishEmail($email="", $fname=""){
		$pet = pet_load('finish_account_template');
		//replace the tag with url
		$pet->mail_body = str_replace("{{finish_account_url}}", self::generateHashUrl($email), $pet->mail_body);
		$pet->mail_body = str_replace("{{fname}}", $fname, $pet->mail_body);
		$params['pet_to'] = $email;
		$params['pet_from'] = 'noreply@aophomeschooling.com';
		
		//send the email.
		//if(pet_send_one_mail($pet, $params)){
		return true;
		//} else {
		//return false;
		//}
	}
    
    //Returns an array of user validation errors
    public static function validateAll(){
    
    	//Set array variable.
    	$urlErrors = array();
	    //check URL
	    if(!self::checkUrl()) {
	    	$urlErrors[] = "url parameters are not set";
	    }
	    

	    //check user activation code
	    if(self::generateHash($_GET['email']) != $_GET['activation']) {
	    	//The activation code was invalid
	    	$urlErrors[] = "there is an invalid activation code";
	    }
	    
	    
	    //check if user exists.
	    if(!$user = user_load_by_name($_GET['activation'])){
	        // User doesn't exist
	        $urlErrors[] = "no temporary user exists associated with your email address";
	    }
	    
	 
	 return $urlErrors; 
	    
    
    }
    
    //returns a human-readable explanation of error
    public static function validationErrorHuman(){
    	$errors = self::validateAll();
	    $humanText = "The 'Finish Your Account' process failed because ";
	    
	    if(count($errors) < 2) {
    		$humanText .= join($errors, " and ");
	    } else {
	    	//take off the last value and add it to the end.
	    	$lastError = array_pop($errors);
	    	$humanText .= join($errors, ", ") . " and " . $lastError;
	    }
    	
    	return  $humanText;
    }
    
    //generates the link sent to user
    public static function generateHashUrl($email){
    global $base_url;
    $url = $base_url . "/finish-account?email=" . $email . "&activation=" . self::generateHash($email);
    return $url;
    }
    
    //generates an activation hash
    public static function generateHash($input =""){
    	return md5("xuKq9X" . $input . "EhApZ");
    }
   	
	
	//checks the URL string to verify email link. Returns true or false.
    protected static function checkUrl() {
    	//Check if URL params are set
	    if(isset($_GET['email']) && isset($_GET['activation'])) {
	    return true;
	    } else {
	    return false;
	    }
    }
}