<?php

/**
 * @file
 * Contains the resource callbacks for Subscriptions.
 */

/**
 * Returns a single subscription.
 *
 * @see aop_subscriptions_retrieve_entity()
 */

function aop_subscriptions_services_subscription_retrieve($subscription_id, $activation_code, $expand_entities, $flatten_fields) {

   return aop_subscriptions_services_load_subscription($subscription_id, $activation_code);

}


function aop_subscriptions_services_load_subscription($subscription_id = NULL, $activation_code) { 

  $subscription = new stdClass;

  $query = new EntityFieldQuery();

  if (strlen($activation_code) > 0) { 
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
       ->entityCondition('bundle', 'aop_subscription')
       ->propertyCondition('title', $activation_code);
    $result = $query->execute();
    if (isset($result['node'])) {
      $subscription_nids = array_keys($result['node']); 
      $subscriptions = entity_load('node', $subscription_nids);      
      foreach($subscriptions as $sub) { 
        $subscription->activation_code = $sub->title;
        $subscription->subscription_id = $sub->field_subscription_id['und'][0]['value'];
        $subscription->upc = $sub->field_monarch_product_id['und'][0]['value'];
        $subscription->renewal_flag = $sub->field_renewal_flag['und'][0]['value'];
        $subscription->expiration_date = $sub->field_subscription_expiration_da['und'][0]['value'];
      }
    } else { 
      $subscription->error = "No subscription found for activation code $activation_code";
    }
  } else if (!is_null($subscription_id)) { 
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
       ->entityCondition('bundle', 'aop_subscription')
       ->fieldCondition('field_subscription_id', 'value', $subscription_id, '=');
    $result = $query->execute();
    if (isset($result['node'])) {
      $subscription_nids = array_keys($result['node']); 
      $subscriptions = entity_load('node', $subscription_nids);      
      foreach($subscriptions as $sub) { 
        $subscription->activation_code = $sub->title;
        $subscription->subscription_id = $sub->field_subscription_id['und'][0]['value'];
        $subscription->upc = $sub->field_monarch_product_id['und'][0]['value'];
        $subscription->renewal_flag = $sub->field_renewal_flag['und'][0]['value'];
        $subscription->expiration_date = $sub->field_subscription_expiration_da['und'][0]['value'];
      }
    } else { 
      $subscription->error = "No subscription found for subscription id $subscription_id";
    }

  } 

  return $subscription;

}

function aop_subscriptions_services_update($subscription_id, $activation_code, $data) { 

   $subscription = aop_subscriptions_services_get_subscription($subscription_id, $activation_code);

   if (isset($subscription->error) && $subscription->error == 404) { 
     return "404";
   } else { 
     $node = node_load($subscription->vid);
   }

   foreach($data as $key => $value) {

     switch($key) { 

        case 'field_renewal_flag':
           $renewal_flag = $value;
           break;

        case 'field_activation_date':
           $activation_date = $value;
           break;

        case 'field_subscription_expiration_da':
           $expiration_date = $value;
           break;

        case 'field_subscription_id':
           $new_subscription_id = $value;
           break;

        case 'field_activation_code':
           $new_activation_code = $value;
           break;

        default:
           break;

     }

   }

   if (isset($renewal_flag)) { 
     $node->field_renewal_flag['und'][0]['value'] = $renewal_flag;
     $node->field_renewal_flag['und'][0]['safe']  = $renewal_flag;  
   }

   if (isset($activation_date)) { 
     $node->field_activation_date['und'][0]['value'] = $activation_date." 00:00:00";
   }

   if (isset($expiration_date)) { 
     $node->field_subscription_expiration_da['und'][0]['value'] = $expiration_date." 00:00:00";
   }

   if (isset($new_subscription_id)) { 
     $node->field_subscription_id['und'][0]['value'] = $new_subscription_id;
   }

   if (isset($new_activation_code)) { 
     $node->title = $new_activation_code;
   }

   $node->field_last_update_from_lms['und'][0]['value'] = date('Y-m-d H:i:s');

   node_save($node);

   return 200;

}


function aop_subscriptions_services_get_subscription($subscription_id, $activation_code) { 
 
  $subscription = new stdClass;

  $query = new EntityFieldQuery();

  if (strlen($activation_code) > 0) { 
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
       ->entityCondition('bundle', 'aop_subscription')
       ->propertyCondition('title', $activation_code);
    $result = $query->execute();
    if (isset($result['node'])) {
      $subscription_nids = array_keys($result['node']); 
      $subscriptions = entity_load('node', $subscription_nids);      
      foreach($subscriptions as $sub) { 
        $subscription->vid = $sub->vid;
        $subscription->activation_code = $sub->title;
        $subscription->subscription_id = $sub->field_subscription_id['und'][0]['value'];
        $subscription->upc = $sub->field_monarch_product_id['und'][0]['value'];
        $subscription->renewal_flag = $sub->field_renewal_flag['und'][0]['value'];
        $subscription->expiration_date = $sub->field_subscription_expiration_da['und'][0]['value'];
      }
    } else { 
      $subscription->error = "404";
      return $subscription;
    }
  } else if (!is_null($subscription_id)) { 
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
       ->entityCondition('bundle', 'aop_subscription')
       ->fieldCondition('field_subscription_id', 'value', $subscription_id, '=');
    $result = $query->execute();
    if (isset($result['node'])) {
      $subscription_nids = array_keys($result['node']); 
      $subscriptions = entity_load('node', $subscription_nids);      
      foreach($subscriptions as $sub) { 
        $subscription->vid = $sub->vid;
        $subscription->activation_code = $sub->title;
        $subscription->subscription_id = $sub->field_subscription_id['und'][0]['value'];
        $subscription->upc = $sub->field_monarch_product_id['und'][0]['value'];
        $subscription->renewal_flag = $sub->field_renewal_flag['und'][0]['value'];
        $subscription->expiration_date = $sub->field_subscription_expiration_da['und'][0]['value'];
      }
    } else { 
      $subscription->error = "404";
    }
  }

    return $subscription;

}
