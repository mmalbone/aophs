<?php

/**
 * @file
 * Implements the various hooks exposed by Drupal commerce completing the
 * commerce_return entity type.
 */

/**
 * Implements hook_commerce_customer_profile_type_info_alter().
 *
 * Using the hook alter because we want to ensure that profile is not attached
 * to commerce_order entity type.
 */
function commerce_return_commerce_customer_profile_type_info_alter(&$profile_types){
  $profile_types['rma'] = commerce_return_customer_profile();
}

/**
 * Implements hook_commerce_checkout_pane_info_alter().
 *
 * Remove the checkout pane related to RMA customer profile implemented
 * by commerce_customer_commerce_checkout_pane_info function.
 */
function commerce_return_commerce_checkout_pane_info_alter(&$checkout_panes) {
  if (isset($checkout_panes['customer_profile_rma'])) {
    unset($checkout_panes['customer_profile_rma']);
  }
}
