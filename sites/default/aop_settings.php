<?php

ini_set('memory_limit', '512M');

// AOP specific changes to settings.php

// Modify server variables to be compatible with the OAuth.php library
$_SERVER['SERVER_NAME'] = $_SERVER['HTTP_HOST'];
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") {
  $_SERVER['SERVER_PORT'] = 443;
}
else {
  $_SERVER['SERVER_PORT'] = 80;
}
$_SERVER['QUERY_STRING'] = preg_replace("/&{0,1}q=[^&]*&{0,1}/i", "", $_SERVER['QUERY_STRING']);


// Require SSL.
if (isset($_SERVER['PANTHEON_ENVIRONMENT']) &&
  $_SERVER['PANTHEON_ENVIRONMENT'] === 'live') {
  if (!isset($_SERVER['HTTP_X_SSL']) ||
    (isset($_SERVER['HTTP_X_SSL']) && $_SERVER['HTTP_X_SSL'] != 'ON')) {
    header('HTTP/1.0 301 Moved Permanently');
    header('Location: https://pantheon.aophomeschooling.com'. $_SERVER['REQUEST_URI']);
    exit();
  }
}

// Redis configutation
// All Pantheon Environments.
if (isset($_SERVER['PANTHEON_ENVIRONMENT']) &&
    (($_SERVER['PANTHEON_ENVIRONMENT'] === 'live') ||
    ($_SERVER['PANTHEON_ENVIRONMENT'] === 'test'))) {  // Use Redis for caching.
  
  $conf['redis_client_interface'] = 'PhpRedis';
  $conf['cache_backends'][] = 'sites/all/modules/redis/redis.autoload.inc';
  $conf['cache_default_class'] = 'Redis_Cache';
  $conf['cache_prefix'] = array('default' => 'pantheon-redis');
  // Do not use Redis for cache_form (no performance difference).
  $conf['cache_class_cache_form'] = 'DrupalDatabaseCache';
  // Use Redis for Drupal locks (semaphore).
  $conf['lock_inc'] = 'sites/all/modules/redis/redis.lock.inc';
}
