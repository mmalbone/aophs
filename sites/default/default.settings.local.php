<?php

/**
 * The settings.local.php can be used to override variables set in settings.php
 * or in the database.
 * There are a large number of commonly hijacked variables available for your
 * use: https://www.drupal.org/node/1525472
*/

$conf['file_temporary_path'] = '/tmp';

